//
//  AddressController.swift
//  Tookan
//
//  Created by Rakesh Kumar on 3/11/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

protocol AddressDelegate {
    func setLocation(_ location:CLLocation, address:String)
}


class AddressController: UIViewController, NavigationDelegate, GMSMapViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var pinPointer: UIImageView!
    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var pickLocationButton: UIButton!
    @IBOutlet var addressFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet var pickLocationBottomConstraint: NSLayoutConstraint!
    
    var createTaskAddressField:UITextField!
    var createTaskAddressView:UITextView!
    let searchRadius: Double = 100
    var addressArray = NSMutableArray()
    var placeIDArray = NSMutableArray()
    var selectedLocation:CLLocation!
    var camera:GMSCameraPosition!
    var delegate:AddressDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickLocationButton.setTitle("Pick this location".localized, for: UIControlState())
        pickLocationButton.backgroundColor = COLOR.themeForegroundColor
        pickLocationButton.layer.cornerRadius = 4.0
        
        self.pinPointer.image = #imageLiteral(resourceName: "pin_pointer").withRenderingMode(.alwaysTemplate)
        self.pinPointer.tintColor = COLOR.themeForegroundColor

        self.setNavigationBar()
        /*--------------- UITapGesture -------------*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddressController.backgroundTouch))
        tap.numberOfTapsRequired = 1
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)

        self.addressFieldTopConstraint.constant = self.addressFieldTopConstraint.constant + Singleton.sharedInstance.getTopInsetofSafeArea()
        
        self.pickLocationBottomConstraint.constant = self.pickLocationBottomConstraint.constant + Singleton.sharedInstance.getBottomInsetofSafeArea()
        
        
    }
    
    override func loadView() {
        Bundle.main.loadNibNamed("AddressController", owner: self, options: nil)
        self.view.frame = UIScreen.main.bounds
    }
    
    //MARK: UIStatusBarStyle
    override var preferredStatusBarStyle:UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        googleMapView.delegate = self
        addressField.delegate = self
        addressField.placeholder = "Getting address...".localized
        self.pickLocationButton.isEnabled = false
        self.addressTableView.delegate = self
        self.addressTableView.dataSource = self
        self.addressTableView.isHidden = true
        self.addressTableView.tableFooterView = UIView(frame: CGRect.zero)

        if(LocationTracker.sharedInstance().myLocation != nil) {
            selectedLocation = LocationTracker.sharedInstance().myLocation
            if(selectedLocation != nil) {
                camera = GMSCameraPosition.camera(withLatitude: selectedLocation.coordinate.latitude, longitude: selectedLocation.coordinate.longitude, zoom: 16)
                googleMapView.camera = camera
                googleMapView.isMyLocationEnabled = true
            }
            googleMapView.settings.myLocationButton = true
            googleMapView.padding = UIEdgeInsetsMake(0, 0, 60 + Singleton.sharedInstance.getBottomInsetofSafeArea(), 0)
            
            CLGeocoder().reverseGeocodeLocation(LocationTracker.sharedInstance().myLocation, completionHandler: { (placemarks, error) -> Void in
                if error != nil {
                  //  print(error)
                    return
                }
                else {
                    let pm = placemarks![0]
                    if pm.addressDictionary != nil {
                        self.addressField.text = (pm.addressDictionary!["FormattedAddressLines"] as! NSArray).componentsJoined(by: ", ")
                        self.pickLocationButton.isEnabled = true
                    }
                }
            })
            
            if self.createTaskAddressView != nil {
                self.addressField.text = self.createTaskAddressView.text
            }
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
        CATransaction.begin()
        CATransaction.setValue(NSNumber(value: 0.5), forKey: kCATransactionAnimationDuration)
        self.googleMapView.animate(toViewingAngle: 45)
        CATransaction.commit()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func backgroundTouch() {
        if(self.addressArray.count == 0) {
            self.addressTableView.isHidden = true
        }
        self.view.endEditing(true)
    }

    
    //MARK: Navigation Bar
    func setNavigationBar() {
        let navigation = UINib(nibName: "navigationBar", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = "Get Address".localized
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.titleLabel.textColor = COLOR.TEXT_COLOR
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        navigation.backButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: UIControlState())
        navigation.backButton.tintColor = COLOR.TEXT_COLOR
        navigation.bottomLine.isHidden = false
        navigation.editButtonTrailingConstraint.constant = 15.0
        self.view.addSubview(navigation)
    }
    
    
    //MARK: NavigationDelegate Methods
    func backAction() {
        if self.createTaskAddressField != nil {
            self.createTaskAddressField.text = ""
            self.createTaskAddressField.placeholder = TEXT.ADDRESS
        }
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pickLocationAction(_ sender: AnyObject) {
        if(selectedLocation != nil) {
            if self.createTaskAddressField != nil {
                self.createTaskAddressField.text = self.addressField.text
            }
            
            if self.createTaskAddressView != nil {
                self.createTaskAddressView.text = self.addressField.text
            }
            
            delegate.setLocation(selectedLocation, address: self.addressField.text!)
            self.view.endEditing(true)
            self.dismiss(animated: true, completion: nil)
        } else {
            Singleton.sharedInstance.showErrorMessage(error: "Invalid Address".localized, isError: .error)
        }
    }
    //MARK: GOOGLE MAP DELEGATE
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.addressTableView.isHidden = true
        selectedLocation = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        
//        NetworkingHelper.sharedInstance.getAddressFromLatLong("\(position.target.latitude),\(position.target.longitude)") { (succeeded, response) -> () in
//            if(succeeded == true) {
//                print(response)
//            }
//        }
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: position.target.latitude, longitude: position.target.longitude), completionHandler: { (placemarks, error) -> Void in
            if error != nil {
               // print(error)
                return
            }
            else {
                let pm = placemarks![0]
                self.addressField.text = (pm.addressDictionary!["FormattedAddressLines"] as! NSArray).componentsJoined(by: ", ")
                self.pickLocationButton.isEnabled = true
            }
        })
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.addressField.text = ""
        self.addressField.placeholder = "Getting address...".localized
        self.pickLocationButton.isEnabled = false
    }
    
    
    //UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedText = textField.text!
        var updatedString:NSString = "\(updatedText)" as NSString
        updatedString = updatedString.replacingCharacters(in: range, with: string) as NSString
        let searchKey = updatedString
        NetworkingHelper.sharedInstance.autoCompleteSearch(searchKey as String) {[unowned self] (succeeded, response) -> () in
            if(succeeded == true) {
                DispatchQueue.main.async(execute: { () -> Void in
                    let predictions = response["predictions"] as! NSArray
                    let predictionDictionary = ["prediction":predictions]
                    self.addressArray = NSMutableArray(array: predictionDictionary["prediction"]?.value(forKey: "description") as! [AnyObject])
                    if(self.addressArray.count > 0) {
                        self.placeIDArray = NSMutableArray(array: predictionDictionary["prediction"]?.value(forKey: "place_id") as! [AnyObject])
                    } else {
                        self.addressArray.removeAllObjects()
                        self.placeIDArray.removeAllObjects()
                        self.tableBackgroundView()
                    }
                    
                    self.addressTableView.isHidden = false
                    self.addressTableView.reloadData()
                })
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.addressArray.removeAllObjects()
                    self.placeIDArray.removeAllObjects()
                    self.addressTableView.isHidden = false
                    self.tableBackgroundView()
                    self.addressTableView.reloadData()
                })
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.addressTableView.isHidden = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.addressTableView.isHidden = true
        return true
    }
    
    //MARK: UITableViewDelegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(addressArray.count > 0) {
            addressTableView.backgroundView = nil
        }
        return addressArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel?.text = addressArray.object(at: (indexPath as NSIndexPath).row) as? String
        cell.textLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pickLocationButton.isEnabled = true
        self.addressField.text = addressArray.object(at: (indexPath as NSIndexPath).row) as? String
        self.addressTableView.isHidden = true
        NetworkingHelper.sharedInstance.getLatLongFromPlaceId((placeIDArray.object(at: (indexPath as NSIndexPath).row) as? String)!) { (succeeded, response) -> () in
            if(succeeded == true) {
                DispatchQueue.main.async(execute: { () -> Void in
                    let results = response["result"] as! [String:AnyObject]
                    let locationArray = results["geometry"]?["location"] as! [String:AnyObject]
                    self.selectedLocation = CLLocation(latitude: locationArray["lat"] as! Double, longitude: locationArray["lng"] as! Double)
                    CATransaction.begin()
                    CATransaction.setValue(NSNumber(value: 1.0), forKey: kCATransactionAnimationDuration)
                    self.googleMapView.animate(toLocation: self.selectedLocation.coordinate)
                    CATransaction.commit()
                })
            }
        }
    }

    
    func tableBackgroundView() {
        selectedLocation = nil
        addressTableView.backgroundView = nil
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: addressTableView.frame.width, height: addressTableView.frame.height)
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 50, width: view.frame.width, height: 40)
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.darkGray
        label.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        label.text = "No address found".localized
        view.addSubview(label)
        addressTableView.backgroundView = view
    }
    
}
