//
//  TemplateController.swift
//  SearchTemplate
//
//  Created by CL-Macmini-110 on 7/27/17.
//  Copyright © 2017 CL-Macmini-110. All rights reserved.
//

import UIKit

protocol TemplateControllerDelegate {
    func selectedValue(value: String, tag:Int, isDirectDismiss: Bool)
//    func closeButtonTapped()
//    func dismissTemplateController()
}

class TemplateController: UIViewController {

    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var templateSearchBar: UISearchBar!
    @IBOutlet weak var templateTableView: UITableView!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var crossButtonOutlet: UIButton!
    
    let defaultValueOfBottomConstraint:CGFloat = 50.0
    var itemArray = [String]()
    var filteredData = [String]()
    let footerHeight:CGFloat = 50.0
    var isSearching = false
    var delegate:TemplateControllerDelegate!
    var isKeyboardOn  = false
    var placeholderValue : String?
    
    override func loadView() {
        Bundle.main.loadNibNamed(NIB_NAME.templateController, owner: self, options: nil)
        self.view.frame = UIScreen.main.bounds
        self.backgroundView.layer.cornerRadius = 14.0
        self.backgroundView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTableView()
        self.setSearchBar()

        /*===============   Setting Bottom line   ====================*/
        self.bottomLine.backgroundColor = COLOR.LINE_COLOR
        self.backgroundView.transform = CGAffineTransform(translationX: 0, y: SCREEN_SIZE.height - HEIGHT.navigationHeight)
        
        /*===============   Setting Close Button   ====================*/
        self.crossButtonOutlet.setImage(#imageLiteral(resourceName: "close_profile").withRenderingMode(.alwaysTemplate), for: .normal)
        self.crossButtonOutlet.tintColor = COLOR.TEXT_COLOR
        
        self.tapGesture()
    }

    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.backgroundView.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setTableView() {
        /*===============   Setting Table View   ====================*/
        self.templateTableView.tableFooterView = UIView()
        self.templateTableView.delegate = self
        self.templateTableView.dataSource = self
        self.templateTableView.register(UINib(nibName: NIB_NAME.templateTableViewCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.templateTableViewCell)
    }
    
    func setSearchBar() {
        /*===============   Setting Search Bar   ====================*/
        self.templateSearchBar.delegate = self
        self.templateSearchBar.backgroundImage = UIImage()
        self.templateSearchBar.returnKeyType = .search
        self.templateSearchBar.placeholder = "\(TEXT.SEARCH) \(placeholderValue ?? TEXT.DROPDOWN)"
        self.templateSearchBar.setImage(UIImage(), for: .clear, state: .normal)
    }
    
    //MARK: Tap Gesture
    func tapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    //MARK: Close Action
    @IBAction func crossButtonAction(_ sender: Any) {
        //self.delegate.selectedValue(value: "", tag:self.view.tag, isDirectDismiss: false)
        self.dismissView(value: "", isDirectDismiss: false)
    }
    
    func backgroundTouch() {
        self.dismissView(value: "", isDirectDismiss: true)
    }
    func dismissView(value: String, isDirectDismiss: Bool) {
        if self.isKeyboardOn == false {
            UIView.animate(withDuration: 0.3, animations: {
                self.backgroundView.transform = CGAffineTransform(translationX: 0, y: self.backgroundView.frame.height)
                self.delegate.selectedValue(value: value, tag:self.view.tag, isDirectDismiss: isDirectDismiss)
            }, completion: { finished in
                self.dismiss(animated: false, completion: nil)
            })
        } else {
            self.view.endEditing(true)
        }
    }
    
    //MARK: KeyBoard Observers
    func keyboardWillShow(_ notification: Foundation.Notification) {
        self.isKeyboardOn = true
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = value.cgRectValue.size
        self.tableBottomConstraint.constant = keyboardSize.height + defaultValueOfBottomConstraint
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.isKeyboardOn = false
        self.tableBottomConstraint.constant = defaultValueOfBottomConstraint
    }
}

//MARK: Table Delegate Methods
extension TemplateController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard isSearching == false else {
            return filteredData.count
        }
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.templateTableViewCell) as! TemplateTableViewCell
        guard isSearching == false else {
            cell.templateLabel.text = filteredData[indexPath.row]
            return cell
        }
        cell.templateLabel.text = itemArray[indexPath.row].trimText
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        var selectedValue = ""
        if isSearching == true {
            selectedValue = filteredData[indexPath.row]
        } else {
           selectedValue = itemArray[indexPath.row]
        }
        self.dismissView(value: selectedValue, isDirectDismiss: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//MARK: UISearchBarDelegate Methods
extension TemplateController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" || searchBar.text == nil{
            self.isSearching = false
        } else {
            self.isSearching = true
            self.filteredData = itemArray.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: searchText, options: .caseInsensitive)
                return range.location != NSNotFound
            })
        }
        self.templateTableView.reloadData()
    }
}

//MARK: UIGestureRecognizerDelegate Methods
extension TemplateController:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view != nil && touch.view!.isDescendant(of: self.backgroundView)  {
            return false
        }
        
        if self.isKeyboardOn == true {
            self.view.endEditing(true)
            return false
        }
        return true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer : UIGestureRecognizer)->Bool {
        return true
    }
}
