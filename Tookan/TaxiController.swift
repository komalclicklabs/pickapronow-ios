//
//  TaxiController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 28/12/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import MessageUI
import Crashlytics
import AVFoundation
import FirebaseAnalytics
import MapKit

class TaxiController: UIViewController, NoInternetConnectionDelegate, AddressDelegate {

    @IBOutlet var googleMapView: GMSMapView!
    @IBOutlet var currentLocationButton: UIButton!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var timeDetailView: UIView!
    @IBOutlet var distanceHeader: UILabel!
    @IBOutlet var timeHeader: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var dotView: UIView!
    @IBOutlet var destinationLabelInTimeDetail: UILabel!
    @IBOutlet var innerRectView: UIView!
    @IBOutlet var bottomLine: UIView!
    @IBOutlet var timeDetailViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var currentLocationBottomConstraint: NSLayoutConstraint!
    
    //var bottomView:BottomButtonView!
    var acceptTitle:String!
    var rejectTitle:String!
    var viewShowStatus:Int!
    var stateTitle:String!
    var loadingTitle:String!
    var positiveStatusColor:UIColor!
    var hideLeftButton = false
    
    //var navigationWidth:CGFloat = 50.0
    var accessToken: String!
    var dictionary = [String: String]()
    var taskUpdatingStatus = ""
    var hitProgessCount = 0
    var locationUpdateCount = 0
    //var model = TaskDetailModel()
    var internetToast:NoInternetConnectionView!
    var endPointMarker:GMSMarker? = GMSMarker()
    var currentMarker:GMSMarker? = GMSMarker()
    var mapCurrentZoomLevel:Float = 16
    var updatedConstraintValue:CGFloat = 15.0
    var previousLocation:CLLocation!
    var rideTimer:Timer!

    var initialCalculationOfRide = false

    var myCurrentLocation:CLLocation!
    //var lastDestinationCooridnates:CLLocationCoordinate2D!
    var taxiModel:TaxiModel?
    var panDirection:SwipeDirection!
    var taxiDetailsView:TaxiDetailView!
    var navigation:navigationBar!
    var directionButton:UIButton!
    var slider:CustomSlider!
    var minimizeOrigin:CGFloat = getAspectRatioValue(value: SCREEN_SIZE.height - getAspectRatioValue(value: 288))//SCREEN_SIZE.height - 288
    let sliderHeight:CGFloat = 70.0
    let jobOrigin:CGFloat = getAspectRatioValue(value: 150.0)//177.0
    let maxTranslatingPointToMinimize:CGFloat = getAspectRatioValue(value: 100.0)//150
    let leftRightMargin:CGFloat = 5.0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.taxiModel = TaxiModel()
        self.minimizeOrigin = SCREEN_SIZE.height - jobOrigin - self.sliderHeight - getAspectRatioValue(value: 130)
        Singleton.sharedInstance.isTaxiTask = true
        self.setNavigationBar()
        self.setTaxiDetailView()
        self.setTimeDistanceFareSummaryView()
        ActivityIndicator.sharedInstance.hideActivityIndicator()
        self.setSliderButton()
        self.setCurrentLocationButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.accessToken = Singleton.sharedInstance.getAccessToken()
        /*-------------- Set Job Status Bottom View -------------*/
        self.setValuesForTaxiFromCustomField()
       // self.setJobStatusAndButtonTitle()
        self.setMarkersImage()
        self.setGoogleMap()
        if self.taxiDetailsView.transform.ty == SCREEN_SIZE.height {
           // self.taxiDetailsView.alpha = 1
            //self.translateViewToPoint(point: 0, translatingView: self.taxiDetailsView)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func setGoogleMap() {
        /*============ Google Map ===============*/
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                self.googleMapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("The style definition could not be loaded: \(error)")
        }
        
        let coordinate = self.taxiModel?.getDestinationCoordinates()
        self.googleMapView.animate(toZoom: 16)
        self.googleMapView.isMyLocationEnabled = true
        self.setMarker(coordinate!, marker: self.endPointMarker!)
        self.googleMapView.animate(toLocation: coordinate!)
        if Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.started {
            self.drawPathFromCurrentToDestination()
        } else {
            self.updateMarkerForTaskDetail()
        }
    }
    
    func updateMarkerForTaskDetail() {
        DispatchQueue.main.async {
            guard UIApplication.shared.applicationState == UIApplicationState.active else {
                return
            }
            let destinationCoordinate = self.taxiModel?.getDestinationCoordinates()
            guard destinationCoordinate != nil else {
                self.previousLocation = LocationTracker.sharedInstance().getLastLocation()
                self.googleMapView.animate(with: GMSCameraUpdate.setTarget(self.previousLocation.coordinate, zoom: 16.0))
                self.googleMapView.padding = UIEdgeInsetsMake(40, 0, SCREEN_SIZE.height/1.8, 0)
                return
            }
            
            guard destinationCoordinate?.latitude != 0.0 else {
                self.previousLocation = LocationTracker.sharedInstance().getLastLocation()
                self.googleMapView.animate(with: GMSCameraUpdate.setTarget(self.previousLocation.coordinate, zoom: 16.0))
                self.googleMapView.padding = UIEdgeInsetsMake(40, 0, SCREEN_SIZE.height/1.8, 0)
                return
            }
            
            self.googleMapView.animate(with: GMSCameraUpdate.setTarget(destinationCoordinate!, zoom: 16.0))
            self.googleMapView.padding = UIEdgeInsetsMake(40, 0, SCREEN_SIZE.height/1.8, 0)
        }
    }
    
    func drawPathFromCurrentToDestination() {
        let originCoordinate = LocationTracker.sharedInstance().getLatestLocationForForegroundMode()
        let destinationCoordinate = self.taxiModel?.getDestinationCoordinates()
       
        guard destinationCoordinate != nil else {
            self.googleMapView.clear()
            self.previousLocation = LocationTracker.sharedInstance().getLastLocation()
            self.animationForCameraLocation(coordinate: self.previousLocation.coordinate)
            return
        }
        
        guard destinationCoordinate?.latitude != 0.0 else {
            self.googleMapView.clear()
            self.previousLocation = LocationTracker.sharedInstance().getLastLocation()
            self.animationForCameraLocation(coordinate: self.previousLocation.coordinate)
            return
        }
        
        if myCurrentLocation == nil {
            self.myCurrentLocation = LocationTracker.sharedInstance().getLatestLocationForForegroundMode()
        }
//        else {
//            if lastDestinationCooridnates != nil {
//                
//            } else {
//                guard Double((originCoordinate?.distance(from: self.myCurrentLocation))!) > 200.0 else {
//                    googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
//                    CATransaction.begin()
//                    CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
//                    let bounds = GMSCoordinateBounds(coordinate: (originCoordinate?.coordinate)!, coordinate: destinationCoordinate!)
//                    let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(0, 20, self.jobOrigin + 20, 20))
//                    googleMapView.moveCamera(update)
//                    //googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
//                    self.googleMapView.animate(toViewingAngle: 45)
//                    CATransaction.commit()
//                    return
//                }
//
//            }
//        }
        self.myCurrentLocation = originCoordinate
        NetworkingHelper.sharedInstance.fetchPathPoints((originCoordinate?.coordinate)!, to: destinationCoordinate!) { (optionalRoute) in
            if let jsonRoutes = optionalRoute,
                let routes = jsonRoutes["routes"] as? NSArray{
                if routes.count > 0 {
                    if let shortestRoute = routes[0] as? [String: AnyObject],
                        let legs = shortestRoute["legs"] as? Array<[String: AnyObject]>,
                        let distanceDict = legs[0]["distance"] as? [String: AnyObject],
                        let distance = distanceDict["value"] as? NSNumber,
                        let polyline = shortestRoute["overview_polyline"] as? [String: String],
                        let points = polyline["points"]
                        , distance.doubleValue >= 0 {
                        self.drawPath(points, originCoordinate:(originCoordinate?.coordinate)!, destinationCoordinate:destinationCoordinate!, minOrigin:self.jobOrigin + 20)
                    }
                }else{
                    self.setMarker((originCoordinate?.coordinate)!, destinationCoordinate: destinationCoordinate!, minOrigin:self.jobOrigin + 20)
                }
            } else {
                DispatchQueue.main.async {
                    guard UIApplication.shared.applicationState == UIApplicationState.active else {
                        return
                    }
                    self.googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
                    CATransaction.begin()
                    CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
                    let bounds = GMSCoordinateBounds(coordinate: (originCoordinate?.coordinate)!, coordinate: destinationCoordinate!)
                    let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(0, 20, self.jobOrigin + 20, 20))
                    self.googleMapView.moveCamera(update)
                    //googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
                    self.googleMapView.animate(toViewingAngle: 90)
                    CATransaction.commit()
                }
            }
        }
    }
    
    func drawPath(_ encodedPathString: String, originCoordinate:CLLocationCoordinate2D, destinationCoordinate:CLLocationCoordinate2D, minOrigin:CGFloat) -> Void {
        DispatchQueue.main.async {
            guard UIApplication.shared.applicationState == UIApplicationState.active else {
                return
            }
            self.googleMapView.clear()
            CATransaction.begin()
            CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
            let path = GMSPath(fromEncodedPath: encodedPathString)
            let line = GMSPolyline(path: path)
            line.strokeWidth = 4.0
            line.strokeColor = COLOR.mapStrokeColor
            line.isTappable = true
            line.map = self.googleMapView
            self.setMarker(originCoordinate, destinationCoordinate: destinationCoordinate, minOrigin: minOrigin)
            // change the camera, set the zoom, whatever.  Just make sure to call the animate* method.
            self.googleMapView.animate(toViewingAngle: 45)
            CATransaction.commit()
        }
    }
    
    func setMarker(_ originCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D, minOrigin:CGFloat) {
        googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        let destinationLocationMarker = GMSMarker(position: destinationCoordinate)
        destinationLocationMarker.icon = #imageLiteral(resourceName: "selectedIcon").withRenderingMode(.alwaysTemplate)//self.jobModel.getSelectedMarker(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus)
        destinationLocationMarker.map = googleMapView
        
        let northEastCoordinate = CLLocationCoordinate2D(latitude: max(originCoordinate.latitude, destinationCoordinate.latitude), longitude: max(originCoordinate.longitude, destinationCoordinate.longitude))
        let southWestCoordinate = CLLocationCoordinate2D(latitude: min(originCoordinate.latitude, destinationCoordinate.latitude), longitude: min(originCoordinate.longitude, destinationCoordinate.longitude))
        
        _ = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: (northEastCoordinate.latitude + southWestCoordinate.latitude)/2, longitude: (northEastCoordinate.longitude + southWestCoordinate.longitude)/2), zoom: 12, bearing: 0, viewingAngle: 0)
        
        //        googleMapView.animateToCameraPosition(cameraPosition)
        
        let bounds = GMSCoordinateBounds(coordinate: originCoordinate, coordinate: destinationCoordinate)
        let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(0, 20, minOrigin, 20))
        googleMapView.moveCamera(update)
    }


    func setCurrentLocationButton() {
        self.currentLocationButton.layer.cornerRadius = 25
        self.currentLocationBottomConstraint.constant = getAspectRatioValue(value: 218.0)
    }
    
    func setDirectionButton() {
        if directionButton == nil {
            directionButton = UIButton()
            directionButton.alpha = 0
            directionButton.frame = CGRect(x: SCREEN_SIZE.width - 75, y:jobOrigin + minimizeOrigin - 25, width: 50, height: 50)
            directionButton.backgroundColor = COLOR.themeForegroundColor
            directionButton.setImage(#imageLiteral(resourceName: "getDirectionsLarge"), for: .normal)
            directionButton.layer.cornerRadius = 25
            directionButton.layer.masksToBounds = true
            directionButton.addTarget(self, action: #selector(self.directionAction), for: .touchUpInside)
            self.view.addSubview(directionButton)
        }
    }
    
    func directionAction() {
        self.directionButton.isEnabled = false
        self.perform(#selector(self.enableDirectionButton), with: nil, afterDelay: 0.5)
        //self.model.tappedOnMapNavigation()
        self.navigationAction()
    }
    
    func enableDirectionButton() {
        self.directionButton.isEnabled = true
    }
    
    
    //MARK: SET JOB DETAIL VIEW
    func setTaxiDetailView() {
        if self.taxiDetailsView == nil {
            self.taxiDetailsView = UINib(nibName: NIB_NAME.taxiDetailView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TaxiDetailView
            self.taxiDetailsView.delegate = self
            self.taxiDetailsView.model = self.taxiModel
            self.taxiDetailsView.setDetailTable()
            self.taxiDetailsView.frame = CGRect(x: leftRightMargin, y: jobOrigin, width: SCREEN_SIZE.width - (leftRightMargin * 2), height: SCREEN_SIZE.height - jobOrigin + 20)
            self.taxiDetailsView.layer.cornerRadius = 14.0
            self.taxiDetailsView.layer.masksToBounds = true
           // self.taxiDetailsView.alpha = 0
            self.translateViewToPoint(point: SCREEN_SIZE.height, translatingView: self.taxiDetailsView)
            self.view.addSubview(taxiDetailsView)
        }
        
        self.setDirectionButton()
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.valueChanged(_:)))
        panGesture.delegate = self
        self.taxiDetailsView.addGestureRecognizer(panGesture)
    }
    
    
    //MARK: Animate Card
    func translateViewToPoint(point:CGFloat, translatingView:UIView) {
        self.taxiDetailsView.detailsTable.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.curveLinear, animations: {
            if point == 0 {
                self.taxiDetailsView.showListCell = false
                self.taxiDetailsView.detailsTable.reloadData()
                self.directionButton.alpha = 0.0
                translatingView.transform = CGAffineTransform.identity
                self.taxiDetailsView.slideButton.transform = CGAffineTransform(rotationAngle: .pi)
                self.updateMarkerForTaskDetail()
            } else {
                self.taxiDetailsView.showListCell = true
                self.taxiDetailsView.detailsTable.reloadData()
                translatingView.transform = CGAffineTransform(translationX: 0, y: point)
                self.taxiDetailsView.slideButton.transform = CGAffineTransform.identity
                if self.taxiDetailsView.transform.ty != SCREEN_SIZE.height {
                   self.drawPathFromCurrentToDestination()
                    if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false {
                        let destinationCoordinate = self.taxiModel?.getDestinationCoordinates()
                        guard destinationCoordinate != nil else {
                            return
                        }
                        
                        guard destinationCoordinate?.latitude != 0.0 else {
                            return
                        }
                        self.directionButton.alpha = 1.0
                    }
                }
            }
        }, completion: { finished in
            self.changeTableViewState(status: point == 0 ? true : false, jobView: translatingView)
        })
    }
    
    //MARK: Get Pan Direction
    func getJobViewDirection(jobView:UIView) -> SwipeDirection {
        if jobView.transform.ty == 0 {
            return .topToBottom
        } else {
            self.taxiDetailsView.showListCell = false
            self.taxiDetailsView.detailsTable.reloadData()
            return .bottomToTop
        }
    }
    
    //MARK: Set Table Properties
    func changeTableViewState(status:Bool, jobView:UIView) {
        if jobView == self.taxiDetailsView {
            self.taxiDetailsView.detailsTable.isScrollEnabled = status
        }
    }

    
    func updateTaskAfterNotifcation() {
        if Singleton.sharedInstance.selectedTaskDetails.jobId == NetworkingHelper.sharedInstance.notificationJobId {
            switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
            case JOB_STATUS.canceled, JOB_STATUS.failed, JOB_STATUS.successful, JOB_STATUS.declined:
                self.stopTimer()
                self.resetTaxiStatus()
                _ = self.navigationController?.popViewController(animated: true)
                self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
                return
            default:
                break
            }
        }
        ActivityIndicator.sharedInstance.showActivityIndicator(self)
        self.setValuesForTaxiFromCustomField()
        self.setJobStatusAndButtonTitle()
        self.setMarkersImage()

        ActivityIndicator.sharedInstance.hideActivityIndicator()
    }

    @IBAction func backAction(_ sender: Any) {
        self.stopTimer()
        Singleton.sharedInstance.isTaxiTask = false
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: TIME DISTANCE SUMMARY VIEW
    func setTimeDistanceFareSummaryView() {
        self.dotView.layer.cornerRadius = 5.0
        
        self.destinationLabelInTimeDetail.textColor = COLOR.TEXT_COLOR
        self.timeHeader.text = TEXT.RIDE_TIME
        self.timeHeader.textColor = COLOR.LIGHT_COLOR
        self.timeHeader.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.timeHeader.setLetterSpacing(value: 1.0)
        self.distanceHeader.text = TEXT.DISTANCE
        self.distanceHeader.textColor = COLOR.LIGHT_COLOR
        self.distanceHeader.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.distanceHeader.setLetterSpacing(value: 1.0)

        self.timeLabel.text = "00:00:00"
        self.timeLabel.textColor = COLOR.TEXT_COLOR
        self.timeLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        self.distanceLabel.text = "0.0"
        self.distanceLabel.textColor = COLOR.TEXT_COLOR
        self.distanceLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        self.timeDetailView.layer.shadowOffset = CGSize(width: 2, height: -3)
        self.timeDetailView.layer.shadowOpacity = 0.7
        self.timeDetailView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        self.timeDetailView.layer.cornerRadius = 14.0
        self.timeDetailView.layer.masksToBounds = true
        
        self.innerRectView.layer.cornerRadius = 5.0
        self.innerRectView.layer.borderWidth = 1.0
        self.innerRectView.layer.borderColor = COLOR.LINE_COLOR.cgColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.gotoAddressController))
        tap.numberOfTapsRequired = 1
        self.destinationLabelInTimeDetail.addGestureRecognizer(tap)
        
        self.timeDetailView.transform = CGAffineTransform(translationX: 0, y: self.timeDetailView.frame.height)
    }
    
    //MARK: ADDRESS
    func gotoAddressController() {
        //if Singleton.sharedInstance.selectedTaskDetails.jobType == JOB_STATUS.started {
            let addressController = AddressController()
            addressController.view.frame = self.view.frame
            addressController.delegate = self
            self.present(addressController, animated: true, completion: nil)
       // }
    }
    
    //ADDRESS DELEGATE
    func setLocation(_ location: CLLocation, address:String) {
       // self.lastDestinationCooridnates = self.taxiModel?.getDestinationCoordinates()
        self.taxiModel?.destinationAddress = address
        self.taxiModel?.destinationLatitude = location.coordinate.latitude
        self.taxiModel?.destinationLongitude = location.coordinate.longitude
        self.taxiModel?.updateValuesForTaxiFromCustomField(location: location, address: address)
        self.sendRequestForCustomField(TAXI_LABEL.destinationAddress, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, customData: address, customFieldIndex: 0, location: location)
        self.taxiDetailsView.detailsTable.reloadData()
        
        if Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.started {
            self.drawPathFromCurrentToDestination()
            let destinationCoordinate = self.taxiModel?.getDestinationCoordinates()
            guard destinationCoordinate != nil else {
                return
            }
            
            guard destinationCoordinate?.latitude != 0.0 else {
                return
            }
            self.directionButton.frame = CGRect(x: SCREEN_SIZE.width - 75, y:self.timeDetailView.frame.origin.y - 25, width: 50, height: 50)
            self.directionButton.alpha = 1.0
        } else {
            self.googleMapView.clear()
            let coordinate = self.taxiModel?.getDestinationCoordinates()
             self.setMarker(coordinate!, marker: self.endPointMarker!)
            self.updateMarkerForTaskDetail()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func currentLocationAction(_ sender: Any) {
        CATransaction.begin()
        CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
        if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
            self.googleMapView.animate(toZoom: 16)
            self.googleMapView.animate(toLocation: location.coordinate)
        }
        self.googleMapView.animate(toViewingAngle: 45)
        CATransaction.commit()
    }
    
    //MARK: Setting Job Status and title
    func setJobStatusAndButtonTitle() {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return
        }
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus! {
        case JOB_STATUS.assigned:
            switch (Singleton.sharedInstance.selectedTaskDetails.acceptOptionalField.value)! {
            case ACCEPT_TYPE.acknowledge:
                self.updateNavigationBar(hideBackButton: false)
                self.updateNavigationTitle(title: "")
                self.updateViewsAfterStatus(isTableHidden: false, isSummaryViewHidden: true, isSliderHidden: false)
                viewShowStatus = SHOW_HIDE.showPartial
                acceptTitle = BUTTON_TITLE.acknowledge
                rejectTitle = ""
                self.stateTitle = TEXT.ACKNOWLEDGED
                self.loadingTitle = TEXT.ACKNOWLEDGING
                self.positiveStatusColor = COLOR.ACCEPTED_COLOR
                self.hideLeftButton = false
                self.stopTimer()
                break
                
            case ACCEPT_TYPE.acceptDecline:
                self.updateNavigationBar(hideBackButton: false)
                self.updateNavigationTitle(title: "")
                self.updateViewsAfterStatus(isTableHidden: false, isSummaryViewHidden: true, isSliderHidden: false)
                viewShowStatus = SHOW_HIDE.showComplete
                acceptTitle = BUTTON_TITLE.acceptRide
                rejectTitle = BUTTON_TITLE.declineRide
                self.stateTitle = TEXT.ACCEPTED
                self.loadingTitle = TEXT.ACCEPTING
                self.positiveStatusColor = COLOR.ACCEPTED_COLOR
                self.hideLeftButton = false
                self.stopTimer()
                break
                
            case ACCEPT_TYPE.startCancel:
                self.updateNavigationBar(hideBackButton: true)
                self.updateNavigationTitle(title: TEXT.PICK)
                self.updateViewsAfterStatus(isTableHidden: false, isSummaryViewHidden: true, isSliderHidden: false)
                if(Singleton.sharedInstance.selectedTaskDetails.hideCancelOptionalField.value == 1) {
                    viewShowStatus = SHOW_HIDE.showPartial
                    self.hideLeftButton = true
                } else {
                    viewShowStatus = SHOW_HIDE.showComplete
                    self.hideLeftButton = false
                }
                acceptTitle = BUTTON_TITLE.arrived
                rejectTitle = BUTTON_TITLE.cancelRide
                self.stopTimer()
                self.stateTitle = TEXT.ARRIVED
                self.loadingTitle = TEXT.ARRIVING
                self.positiveStatusColor = COLOR.STARTED_COLOR
                break
            default:
                break
            }
            break
            
        case JOB_STATUS.accepted:
            self.updateNavigationBar(hideBackButton: true)
            self.updateNavigationTitle(title: TEXT.PICK)
            self.updateViewsAfterStatus(isTableHidden: false, isSummaryViewHidden: true, isSliderHidden: false)
            if(Singleton.sharedInstance.selectedTaskDetails.hideCancelOptionalField.value == 1) {
                viewShowStatus = SHOW_HIDE.showPartial
                self.hideLeftButton = true
            } else {
                viewShowStatus = SHOW_HIDE.showComplete
                self.hideLeftButton = false
            }
            acceptTitle = BUTTON_TITLE.arrived
            rejectTitle = BUTTON_TITLE.cancelRide
            self.stateTitle = TEXT.ARRIVED
            self.loadingTitle = TEXT.ARRIVING
            self.positiveStatusColor = COLOR.ARRIVED_COLOR
            self.stopTimer()
            break
            
        case JOB_STATUS.started:
            self.updateNavigationBar(hideBackButton: true)
//            if(Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value == 1) {
//                self.updateNavigationTitle(title: TEXT.PICK)
//                self.updateViewsAfterStatus(isTableHidden: false, isSummaryViewHidden: true, isSliderHidden: false)
//                if(Singleton.sharedInstance.selectedTaskDetails.hideCancelOptionalField.value == 1) {
//                    viewShowStatus = SHOW_HIDE.showPartial
//                    self.hideLeftButton = true
//                } else {
//                    viewShowStatus = SHOW_HIDE.showComplete
//                    self.hideLeftButton = false
//                }
//                acceptTitle = BUTTON_TITLE.startRide
//                rejectTitle = BUTTON_TITLE.cancelRide
//                self.stateTitle = TEXT.STARTED
//                self.loadingTitle = TEXT.STARTING
//                self.positiveStatusColor = COLOR.STARTED_COLOR
//                self.stopTimer()
//            } else {
                self.updateNavigationTitle(title: TEXT.DROP)
                self.updateViewsAfterStatus(isTableHidden: true, isSummaryViewHidden: false, isSliderHidden: false)
                acceptTitle = BUTTON_TITLE.endRide
                rejectTitle = BUTTON_TITLE.failed
                self.stateTitle = TEXT.SUCCESSFUL
                self.loadingTitle = TEXT.COMPLETING
                self.positiveStatusColor = COLOR.SUCCESSFUL_COLOR
                if(Singleton.sharedInstance.selectedTaskDetails.hideFailedOptionalField.value == 1) {
                    viewShowStatus = SHOW_HIDE.showPartial
                    self.hideLeftButton = true
                } else {
                    viewShowStatus = SHOW_HIDE.showComplete
                    self.hideLeftButton = false
                }
                self.startTimer()
          //  }
            break
            
        case JOB_STATUS.successful:
            self.updateNavigationBar(hideBackButton: false)
            self.updateNavigationTitle(title: "")
            self.updateViewsAfterStatus(isTableHidden: true, isSummaryViewHidden: true, isSliderHidden: true)
            if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)
            {
                viewShowStatus = SHOW_HIDE.hidePartial
                acceptTitle = BUTTON_TITLE.gotoDelivery
                rejectTitle = ""
            } else {
                viewShowStatus = SHOW_HIDE.hideComplete
                acceptTitle = ""
                rejectTitle = ""
            }
            self.stopTimer()
            break
            
        case JOB_STATUS.failed:
            self.updateNavigationBar(hideBackButton: false)
            self.updateNavigationTitle(title: "")
            self.updateViewsAfterStatus(isTableHidden: true, isSummaryViewHidden: true, isSliderHidden: true)
            viewShowStatus = SHOW_HIDE.hideComplete
            acceptTitle = ""
            rejectTitle = ""
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.failed
            self.stopTimer()
            break
            
        case JOB_STATUS.arrived:
            self.updateNavigationBar(hideBackButton: true)
            self.updateNavigationTitle(title:TEXT.PICK)
            self.updateViewsAfterStatus(isTableHidden: false, isSummaryViewHidden: true, isSliderHidden: false)
            if(Singleton.sharedInstance.selectedTaskDetails.hideFailedOptionalField.value == 1) {
                viewShowStatus = SHOW_HIDE.showPartial
                self.hideLeftButton = true
            } else {
                viewShowStatus = SHOW_HIDE.showComplete
                self.hideLeftButton = false
            }
            acceptTitle = BUTTON_TITLE.startRide
            rejectTitle = BUTTON_TITLE.cancelRide
            self.stateTitle = TEXT.STARTED
            self.loadingTitle = TEXT.STARTING
            self.positiveStatusColor = COLOR.SUCCESSFUL_COLOR
             self.stopTimer()
            break
            
        case JOB_STATUS.declined:
            self.updateNavigationBar(hideBackButton: false)
            self.updateNavigationTitle(title: "")
            self.updateViewsAfterStatus(isTableHidden: true, isSummaryViewHidden: true, isSliderHidden: true)
            viewShowStatus = SHOW_HIDE.hideComplete
            acceptTitle = ""
            rejectTitle = ""
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.declined
            self.stopTimer()
            break
            
        case JOB_STATUS.canceled:
            self.updateNavigationBar(hideBackButton: false)
            self.updateNavigationTitle(title:"")
            self.updateViewsAfterStatus(isTableHidden: true, isSummaryViewHidden: true, isSliderHidden: true)
            viewShowStatus = SHOW_HIDE.hideComplete
            acceptTitle = ""
            rejectTitle = ""
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.canceled
            self.stopTimer()
            break
            
        default:
            break
        }
        
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return
        }
        
        //if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
                self.slider.isHidden = true
                return
            }
            self.updateSliderButton()
       // }
    }
    
    func updateViewsAfterStatus(isTableHidden:Bool, isSummaryViewHidden:Bool, isSliderHidden:Bool) {
        if isTableHidden == true {
            self.translateViewToPoint(point: self.taxiDetailsView.frame.height, translatingView: self.taxiDetailsView)
        } else {
            self.translateViewToPoint(point: 0, translatingView: self.taxiDetailsView)
        }
        
        if isSummaryViewHidden == true {
            self.animateTimeDetailView(point: self.timeDetailView.frame.height)
        } else {
            self.directionButton.alpha = 0.0
            self.animateTimeDetailView(point: 0)
        }
        
        if isSliderHidden == true {
            self.slider.isHidden = true
            self.bottomConstraint.constant = 0
            self.timeDetailViewBottomConstraint.constant = 0
            self.taxiDetailsView.isHidden = true
            self.timeDetailView.isHidden = true
        }
    }
    
    func animateTimeDetailView(point:CGFloat) {
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.curveLinear, animations: {
            if point == 0 {
                self.timeDetailView.isHidden = false
                self.timeDetailView.transform = CGAffineTransform.identity
            } else {
                self.timeDetailView.transform = CGAffineTransform(translationX: 0, y: point)
            }
        }, completion: { finished in
            if point == 0 {
                DispatchQueue.main.async {
                    self.currentLocationBottomConstraint.constant = self.timeDetailView.frame.height + self.currentLocationButton.frame.height + 20
                    if (self.taxiModel?.destinationLatitude)! > 0.0 {
                        self.directionButton.alpha = 1.0
                        self.directionButton.frame = CGRect(x: SCREEN_SIZE.width - 75, y:self.timeDetailView.frame.origin.y - 25, width: 50, height: 50)
                    } else {
                        self.directionButton.alpha = 0.0
                    }
                }
            } else {
                self.timeDetailView.isHidden = true
            }
        })
    }
    
    func animationForBottomView() {
        if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            self.slider.resetView()
        }
    }
    
    func dismissPartial() {
       // if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            if self.slider.direction == PAN_GESTURE_DIRECTION.right {
                self.slider.centerLabel.text = self.slider.loadingText
            } else {
                self.slider.centerLabel.text = TEXT.CANCELING
            }
        //}
    }
    
    func dismissComplete() {
        if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            self.slider.isHidden = true
        }
    }
    
    func dismissConstroller() {
        self.backAction()
    }
    
    func acceptTask(){
        self.dismissPartial()
        
        guard IJReachability.isConnectedToNetwork() == true else {
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            self.setJobStatusAndButtonTitle()
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            return
        }
        
        
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
        case JOB_STATUS.accepted:
             if self.checkForMinimumDistance() == true {
                self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.arrived, reason: "")
                self.dictionary["description"] = "\(TEXT.ARRIVED) \(TEXT.AT)"
                self.taskUpdatingStatus = "Arrived"
             } else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                self.setJobStatusAndButtonTitle()
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.CAN_NOT_START_TASK, isError: .error)
            }
        case JOB_STATUS.started:
//                if(Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value == 1) {
//                    self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.arrived, reason: "")
//                    self.dictionary["description"] = TEXT.ARRIVED + " " + TEXT.AT
//                    self.taskUpdatingStatus = "Arrived"
//                } else {
                    self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.successful, reason: "")
                    guard IJReachability.isConnectedToNetwork() == true else {
                        self.dictionary["creation_datetime"] = Date().formattedWith("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                        return
                    }
                    self.dictionary["description"] = "\(TEXT.SUCCESSFUL) \(TEXT.AT)"//TEXT.SUCCESSFUL + " " + TEXT.AT
                    self.taskUpdatingStatus = "Successful"
                //}
            
        case JOB_STATUS.arrived:
            self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.started, reason: "")
            guard IJReachability.isConnectedToNetwork() == true else {
                self.dictionary["creation_datetime"] = Date().formattedWith("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                return
            }
            
            self.dictionary["description"] = "\(TEXT.STARTED) \(TEXT.AT)"//TEXT.STARTED + " " + TEXT.AT
            self.taskUpdatingStatus = "Successful"
            
        case JOB_STATUS.successful:
            if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
                if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)
                {
                    self.viewShowStatus = SHOW_HIDE.hidePartial
                    self.acceptTitle = BUTTON_TITLE.gotoDelivery
                    self.rejectTitle = ""
                } else {
                    self.viewShowStatus = SHOW_HIDE.hideComplete
                    _ = self.navigationController?.popViewController(animated: true)
                    return
                }
                
            }
            
        case JOB_STATUS.assigned:
            switch (Singleton.sharedInstance.selectedTaskDetails.acceptOptionalField.value)! {
            case ACCEPT_TYPE.acknowledge:
                self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.accepted, reason: "")
                self.dictionary["description"] = "\(TEXT.ACKNOWLEDGED) \(TEXT.AT)"//TEXT.ACKNOWLEDGED + " " + TEXT.AT
                self.taskUpdatingStatus = "Acknowledged"
                break
                
            case ACCEPT_TYPE.acceptDecline:
                self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.accepted, reason: "")
                self.dictionary["description"] = "\(TEXT.ACCEPTED) \(TEXT.AT)"//TEXT.ACCEPTED + " " + TEXT.AT
                self.taskUpdatingStatus = "Accepted"
                break
                
            case ACCEPT_TYPE.startCancel:
                if self.checkForMinimumDistance() == true {
                    self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.arrived, reason: "")
                    self.dictionary["description"] = "\(TEXT.ARRIVED) \(TEXT.AT)"//TEXT.ARRIVED + " " + TEXT.AT
                    self.taskUpdatingStatus = "Arrived"
                } else {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    self.setJobStatusAndButtonTitle()
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.CAN_NOT_START_TASK, isError: .error)
                }
                break
                
            default:
                break
            }
            break
        default:
            break
        }
        self.dictionary["creation_datetime"] = Date().formattedWith("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    }
    
    func changeJobStatus(_ accessToken:String, jobId:Int, jobStatus:Int, reason:String) {
        ActivityIndicator.sharedInstance.showActivityIndicator(self)
        /*------------- Firebase ---------------*/
       // FIRAnalytics.logEvent(withName: ANALYTICS_KEY.CHANGE_JOB_STATUS, parameters: ["Status":jobStatus as NSObject])
      //  Answers.logCustomEvent(withName: "Change Job Status", customAttributes: ["Status":jobStatus])
        hitProgessCount = 0
        locationUpdateCount = 0
        self.checkLocationHitInProgress(jobStatus, reason: reason)
    }
    
    func checkLocationHitInProgress(_ jobStatus:Int, reason:String) {
        if IJReachability.isConnectedToNetwork() == true {
            self.hitProgessCount = self.hitProgessCount + 1
            if(UserDefaults.standard.bool(forKey: USER_DEFAULT.isHitInProgress) == false) {
                self.locationUpdateCount = self.locationUpdateCount + 1
                var locationArray = [Any]()
                if let array = UserDefaults.standard.object(forKey: USER_DEFAULT.locationArray) as? [Any]{
                    locationArray = array
                }
                
                if(self.locationUpdateCount > 5) {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SERVER_NOT_RESPONDING, isError: .error)
                    self.setJobStatusAndButtonTitle()
                } else {
                    if(locationArray.count > 0) {
                        UserDefaults.standard.set(true, forKey: USER_DEFAULT.isHitInProgress)
                        UIApplication.shared.isNetworkActivityIndicatorVisible = true
                        NetworkingHelper.sharedInstance.updatingFleetLocation(locationArray.jsonString, status: "ChangeJobStatus", locationUpdateMode: UserDefaults.standard.value(forKey: USER_DEFAULT.applicationMode) as! String, getLocationLatitude: 0.0, getLocationLongitude: 0.0, getLocationAccuracy: 0.0, receivedResponse: { (succeeded, response) -> () in
                            if(succeeded == true) {
                                DispatchQueue.main.async(execute: { () -> Void in
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                    if let _ = response["data"] as? NSDictionary {
                                        switch(response["status"] as! Int) {
                                        case STATUS_CODES.SHOW_DATA:
                                            var sentLocationArray = [Any]()
                                            if let sentLocationString = response["location"] as? String {
                                                sentLocationArray = sentLocationString.jsonObjectArray
                                            }
                                            for i in (0..<sentLocationArray.count) {
                                                let sendDictionaryObject = sentLocationArray[i] as! [String:Any]
                                                if let sendTimeStamp = sendDictionaryObject["tm_stmp"] as? String {
                                                    for j in (0..<locationArray.count) {
                                                        let locationDictionaryObject = locationArray[j] as! [String:Any]
                                                        if let locationTimeStamp = locationDictionaryObject["tm_stmp"] as? String {
                                                            if(sendTimeStamp == locationTimeStamp) {
                                                                locationArray.remove(at: j)
                                                                break
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                            UserDefaults.standard.set(locationArray, forKey: USER_DEFAULT.locationArray)
                                            self.checkUnsyncedTask(jobStatus, reason: reason)
                                            break
                                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                            ActivityIndicator.sharedInstance.hideActivityIndicator()
                                            UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                            if let message = response["message"] as? String {
                                                self.showInvalidAccessTokenPopup(message)
                                            } else {
                                                self.showInvalidAccessTokenPopup(ERROR_MESSAGE.INVALID_ACCESS_TOKEN)
                                            }
                                            break
                                        default:
                                            Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                                            ActivityIndicator.sharedInstance.hideActivityIndicator()
                                            self.setJobStatusAndButtonTitle()
                                            break
                                        }
                                    }
                                })
                            } else {
                                //Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                                sleep(1)
                                self.checkLocationHitInProgress(jobStatus, reason: reason)
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                            }
                        })
                    } else {
                        self.checkUnsyncedTask(jobStatus, reason: reason)
                    }
                }
            } else {
                if(self.hitProgessCount > 15) {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SERVER_NOT_RESPONDING, isError: .error)
                    self.setJobStatusAndButtonTitle()
                } else {
                    sleep(1)
                    self.checkLocationHitInProgress(jobStatus, reason: reason)
                }
            }
        } else {
            UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            self.checkUnsyncedTask(jobStatus, reason: reason)
        }
    }

    func checkUnsyncedTask(_ jobStatus:Int, reason:String) {
        ActivityIndicator.sharedInstance.hideActivityIndicator()
        if IJReachability.isConnectedToNetwork() == true {
            self.changeJobStatusServerHit(jobStatus, reason: reason)
        } else {
            self.showInternetToast()
        }
    }
    
    func changeJobStatusServerHit(_ jobStatus:Int, reason:String) {
        self.taxiModel?.requestToChangeJobStatus(accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: jobStatus, reason: reason) { (succeeded, response) -> () in
            if(succeeded == true) {
                DispatchQueue.main.async(execute: { () -> Void in
                    if (response["data"] as? [String:Any]) != nil {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            self.setUpdatedJobStatus(jobStatus)
                            break
                            
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            if let message = response["message"] as? String {
                                self.showInvalidAccessTokenPopup(message)
                            } else {
                                self.showInvalidAccessTokenPopup(ERROR_MESSAGE.INVALID_ACCESS_TOKEN)
                            }
                            break
                        default:
                            Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                            break
                        }
                    }
                    self.setJobStatusAndButtonTitle()
                })
            } else {
                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                self.setJobStatusAndButtonTitle()
            }
        }
    }
    
    func sendRequestForCustomField(_ customLabel:String, jobId:Int, customData:String, customFieldIndex:Int, location:CLLocation) {
        NetworkingHelper.sharedInstance.updateCustomFieldWithOutImage(customLabel, job_id: jobId, data: customData, status: "", imageName: "", collectionIndex: 0) { (succeeded, response) -> () in
            if(succeeded == true) {
                NetworkingHelper.sharedInstance.updateCustomFieldWithOutImage(TAXI_LABEL.destinationLongitude, job_id: jobId, data: "\(location.coordinate.longitude)", status: "", imageName: "", collectionIndex: 0) { (succeeded, response) -> () in
                    if(succeeded == true) {
                        NetworkingHelper.sharedInstance.updateCustomFieldWithOutImage(TAXI_LABEL.destinationLatitude, job_id: jobId, data: "\(location.coordinate.latitude)", status: "", imageName: "", collectionIndex: 0) { (succeeded, response) -> () in
                            if(succeeded == true) {
                                
                            } else {
                                switch(response["status"] as! Int) {
                                case STATUS_CODES.SLOW_INTERNET_CONNECTION:
                                    self.showInternetToast()
                                    break
                                default:
                                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                                    break
                                }
                            }
                        }
                    } else {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SLOW_INTERNET_CONNECTION:
                            self.showInternetToast()
                            break
                        default:
                            Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                            break
                        }
                    }
                }
            } else {
                switch(response["status"] as! Int) {
                case STATUS_CODES.SLOW_INTERNET_CONNECTION:
                    self.showInternetToast()
                    break
                default:
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    break
                }
            }
        }
     }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        self.stopTimer()
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.stopTimer()
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func setUpdatedJobStatus(_ jobStatus:Int) {
        Singleton.sharedInstance.selectedTaskDetails.taskHistoryArray?.append(TaskHistory(json: self.dictionary as NSDictionary))
        switch jobStatus {
        case JOB_STATUS.accepted:
            switch (Singleton.sharedInstance.selectedTaskDetails.acceptOptionalField.value)! {
            case ACCEPT_TYPE.acknowledge, ACCEPT_TYPE.acceptDecline:
                if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0 && Singleton.sharedInstance.selectedDeliveryJobDetails != nil) {
                    Singleton.sharedInstance.selectedDeliveryJobDetails.jobStatus = JOB_STATUS.accepted
                }
                
                if(Singleton.sharedInstance.selectedTaskDetails.jobType == JOB_TYPE.delivery && Singleton.sharedInstance.selectedTaskDetails.pickupJobId > 0 && Singleton.sharedInstance.selectedPickupJobDetails != nil) {
                    Singleton.sharedInstance.selectedPickupJobDetails.jobStatus = JOB_STATUS.accepted
                }
                
                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.accepted
                break
            case ACCEPT_TYPE.startCancel:
                UserDefaults.standard.setValue([Any](), forKey: "SavedLocations")
                UserDefaults.standard.setValue([Any](), forKey: USER_DEFAULT.updatingLocationPathArray)
                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.arrived
                break
            default:
                break
            }
            break
            
        case JOB_STATUS.started:
            UserDefaults.standard.setValue([Any](), forKey: "SavedLocations")
            UserDefaults.standard.setValue([Any](), forKey: USER_DEFAULT.updatingLocationPathArray)
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.started
            break
            
        case JOB_STATUS.successful:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.successful
          //  Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            break
            
        case JOB_STATUS.failed:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.failed
           // Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            self.resetTaxiStatus()
            _ = self.navigationController?.popViewController(animated: true)
            self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
           // NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
            return
            
        case JOB_STATUS.arrived:
           // if(Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value == 1) {
                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.arrived
//            } else {
//                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.successful
//                //Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
//            }
            break
            
        case JOB_STATUS.declined:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.declined
            //Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            self.resetTaxiStatus()
            _ = self.navigationController?.popViewController(animated: true)
            self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
           // NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
            return
            
        case JOB_STATUS.canceled:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.canceled
            //Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            self.resetTaxiStatus()
            _ = self.navigationController?.popViewController(animated: true)
            self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
           // NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
            return
            
        default:
            break
        }
        
        if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
            self.viewShowStatus = SHOW_HIDE.hideComplete
            self.stopTimer()
            if(Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.successful && Singleton.sharedInstance.fleetDetails.has_invoicing_module == 1 && Singleton.sharedInstance.selectedTaskDetails.invoiceHtml.length > 0) {
                self.getInvoiceHTML()
            } else {
                self.resetTaxiStatus()
                _ = self.navigationController?.popViewController(animated: true)
                self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
                //NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
                return
            }
         }
    }

    func getInvoiceHTML() {
        let invoiceModel = InvoiceModel()
        _ = invoiceModel.getInvoiceHTML(completion: {(result) in
            switch result {
            case .success:
                let invoiceController = InvoiceController()
                invoiceController.jobId = Singleton.sharedInstance.selectedTaskDetails.jobId
                invoiceController.htmlString = Singleton.sharedInstance.selectedTaskDetails.invoiceHtml
                self.navigationController?.pushViewController(invoiceController, animated: true)
                break
            case .failed, .ignore:
                self.resetTaxiStatus()
                _ = self.navigationController?.popViewController(animated: true)
                self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
                break
            case .resend:
                self.perform(#selector(self.getInvoiceHTML), with: nil, afterDelay: 1.0)
                break
            case .error(let message):
                Singleton.sharedInstance.showErrorMessage(error: message, isError: .error)
            }
        })
    }
    
    func rejectTask(_ oldJobStatus : Int, reason : String ){
        self.dismissPartial()
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus{
        case JOB_STATUS.declined:
            self.dictionary["description"] = "Declined at".localized
            self.taskUpdatingStatus = "Declined"
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = oldJobStatus
            self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.declined, reason: "")
            
        case JOB_STATUS.failed, JOB_STATUS.canceled:
            self.discardAlertPopup(oldJobStatus, reason: reason)
            break
            
        default:
            break
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        self.dictionary["creation_datetime"] = dateFormatter.string(from: Date())
    }
    
    func discardAlertPopup(_ oldJobStatus : Int, reason : String) {
        let alert = UIAlertController(title: "Add a reason".localized, message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = reason
        })
        alert.addAction(UIAlertAction(title: "Submit".localized, style: .default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            if !(textField.text!.blank(textField.text!)){
                self.viewShowStatus = SHOW_HIDE.hidePartial
                self.dismissPartial()
                if Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.failed {
                    self.dictionary["description"] = "Failed at".localized
                    self.taskUpdatingStatus = "Failed"
                    Singleton.sharedInstance.selectedTaskDetails.jobStatus = oldJobStatus
                    /*------------- Firebase ---------------*/
                   // FIRAnalytics.logEvent(withName: ANALYTICS_KEY.TASK_FAILED_SUBMITTED, parameters: ["Status":"Failed" as NSObject])
                    self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.failed, reason: textField.text!)
                } else {
                    /*------------- Firebase ---------------*/
                   // FIRAnalytics.logEvent(withName: ANALYTICS_KEY.TASK_FAILED_SUBMITTED, parameters: ["Status":"Cancelled" as NSObject])
                    Singleton.sharedInstance.selectedTaskDetails.jobStatus = oldJobStatus
                    self.dictionary["description"] = "Cancelled at".localized
                    self.taskUpdatingStatus = "Cancelled"
                    self.changeJobStatus(self.accessToken, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: JOB_STATUS.canceled, reason: textField.text!)
                }
            } else{
                self.rejectTask(oldJobStatus, reason: "Giving a reason is Mandatory.".localized)
            }
        }))
        alert.addAction(UIAlertAction(title: "Discard".localized, style: .cancel, handler: { (action) -> Void in
            /*------------- Firebase ---------------*/
           // FIRAnalytics.logEvent(withName: ANALYTICS_KEY.TASK_FAILED_DISCARD, parameters: ["Status":Singleton.sharedInstance.selectedTaskDetails.jobStatus as NSObject])
            self.viewShowStatus = SHOW_HIDE.showComplete
            self.animationForBottomView()
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = oldJobStatus
        }))
        if ((self.presentedViewController) != nil) {
            self.presentedViewController?.dismiss(animated: false, completion: nil)
        }
        self.present(alert, animated: true, completion: nil)
    }

    
    //MARK: NoInternetConnectionView
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: "NoInternetConnectionView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:self.navigation.frame.height, width: self.view.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.navigation.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(self.internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
    
    //MARK: GOOGLE MAPS:
    func animationForCameraLocation(coordinate:CLLocationCoordinate2D) {
        DispatchQueue.main.async {
            guard UIApplication.shared.applicationState == UIApplicationState.active else {
                return
            }
            self.googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            CATransaction.begin()
            CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: self.googleMapView.camera.zoom)
            self.googleMapView.animate(to: camera)
            CATransaction.commit()
            self.mapCurrentZoomLevel = self.googleMapView.camera.zoom
        }
    }
    
    func setMarker(_ originCoordinate: CLLocationCoordinate2D, marker:GMSMarker) {
        marker.position = originCoordinate
        marker.map = googleMapView
    }
    
    func setMarkersImage() {
        self.endPointMarker?.icon = #imageLiteral(resourceName: "selectedIcon")
    }
    
    //MARK: UPDATE BOTTOM CONSTRAINTS
    func updateBottomConstraints(value:CGFloat){
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
        case JOB_STATUS.started:
            if(Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value == 0) {
                self.timeDetailViewBottomConstraint.constant = value
                self.bottomConstraint.constant = value + self.timeDetailView.frame.height
            } else {
                self.bottomConstraint.constant = value
            }
            break
        case JOB_STATUS.arrived:
            self.timeDetailViewBottomConstraint.constant = value
            self.bottomConstraint.constant = value + self.timeDetailView.frame.height
            break
        case JOB_STATUS.successful:
            self.timeDetailViewBottomConstraint.constant = 0
            //self.navigationButton.isHidden = true
            self.bottomConstraint.constant = value
            break
        default:
            self.bottomConstraint.constant = value
            break
        }
    }

    func startTimer() {
        if rideTimer != nil {
            rideTimer.invalidate()
            rideTimer = nil
        }
        self.initialCalculationOfRide = true
        self.rideTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateRideSummary), userInfo: nil, repeats: true)
        
    }
    
    func stopTimer() {
        if rideTimer != nil {
            rideTimer.invalidate()
            rideTimer = nil
            self.initialCalculationOfRide = true
            self.timeLabel.text = "00:00:00"
            self.distanceLabel.text = "0.0"
            Singleton.sharedInstance.selectedTaskDetails.travelledTime = 0.0
            Singleton.sharedInstance.selectedTaskDetails.travelledDistance = 0.0
            if Singleton.sharedInstance.selectedTaskDetails.jobStatus != JOB_STATUS.successful {
                self.timeDetailView.isHidden = true
            }
        }
    }
    
    
    func updateDistance() {
        guard LocationTracker.sharedInstance().getLastLocation() != nil else {
            return
        }
        
        guard self.previousLocation != nil else {
            self.previousLocation = LocationTracker.sharedInstance().getLastLocation()
            self.distanceLabel.text = self.taxiModel?.getDistance(previousLocation: self.previousLocation, distanceType: (self.taxiModel?.distanceUnit)!, lastDistance: self.distanceLabel.text!, initialCalculation: self.initialCalculationOfRide)
            return
        }
        self.distanceLabel.text = self.taxiModel?.getDistance(previousLocation: self.previousLocation, distanceType: (self.taxiModel?.distanceUnit)!, lastDistance: self.distanceLabel.text!, initialCalculation: self.initialCalculationOfRide)
        self.previousLocation = LocationTracker.sharedInstance().getLastLocation()
        self.animationForCameraLocation(coordinate: self.previousLocation.coordinate)
    }
    
//    func updateFare() {
//        let distance = self.distanceLabel.text!.components(separatedBy: " ")[0]
//        var remainingDistance = 0.0
//
//        if distance.length > 0 {
//            if Double(distance) != nil {
//                remainingDistance = Double(distance)! - (self.taxiModel?.thresholdDistance)!
//            }
//            if remainingDistance < 0 {
//                remainingDistance = 0.0
//            }
//        }
//        
//        let totalDistanceFare = ceil(remainingDistance * (self.taxiModel?.distanceFare)!)
//        
//        let totalMinutes = self.model.convertTimeIntoMinutes(currentTime: self.timeLabel.text!)
//        var remainingMinutes = totalMinutes - Int((self.taxiModel?.thresholdTime)!)
//        if remainingMinutes < 0 {
//            remainingMinutes = 0
//        }
//        
//        let totalTimeFare = remainingMinutes * Int((self.taxiModel?.timeFare)!)
//        
//        var totalRideFare = (self.taxiModel?.baseFare + totalDistanceFare + Double(totalTimeFare))
//        
//        if self.taxiModel?.surgeFactor > 0 {
//            totalRideFare = totalRideFare * self.surgeFactor
//        }
//        
//        self.fareLabel.text = String.init(format: "%@%.2f", self.currencySymbol, ceil(totalRideFare))
//    }
    
    func updateRideSummary() {
        if (self.taxiModel?.destinationAddress?.length)! > 0 {
            self.destinationLabelInTimeDetail.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
            self.destinationLabelInTimeDetail.text = self.taxiModel?.destinationAddress
            self.destinationLabelInTimeDetail.textColor = COLOR.TEXT_COLOR
            self.dotView.backgroundColor = UIColor.black
            self.bottomLine.isHidden = true

        } else {
            self.destinationLabelInTimeDetail.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)
            self.destinationLabelInTimeDetail.text = TEXT.ENTER_DESTINATION
            self.destinationLabelInTimeDetail.textColor = UIColor.red
            self.dotView.backgroundColor = UIColor.red
            self.bottomLine.isHidden = false
        }
        self.timeLabel.text = self.taxiModel?.convertIntoSeconds(currentTime: self.timeLabel.text!, initialCalculation: self.initialCalculationOfRide)
        self.updateDistance()
        //self.updateFare()
        self.initialCalculationOfRide = false
    }
    
    func setValuesForTaxiFromCustomField() {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            self.resetTaxiStatus()
            _ = self.navigationController?.popViewController(animated: true)
            self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
            return
        }
        
        guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray != nil && (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > 0 else {
            self.resetTaxiStatus()
            _ = self.navigationController?.popViewController(animated: true)
            self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
            return
        }
        
        self.taxiModel?.setTaxiCustomField()
      //  self.lastDestinationCooridnates = self.taxiModel?.getDestinationCoordinates()
    }
    
    func postNotification() {
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
        Singleton.sharedInstance.selectedTaskDetails = nil
    }
    
    func resetTaxiStatus() {
        Singleton.sharedInstance.isTaxiTask = false
    }
    
    func checkForMinimumDistance() -> Bool {
        var pickupLocationCoordinate:CLLocation!
        var latitude = CLLocationDegrees()
        var longitude = CLLocationDegrees()
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        guard Singleton.sharedInstance.selectedTaskDetails.jobPickupLatitude != nil else {
            return false
        }
        
        guard Singleton.sharedInstance.selectedTaskDetails.jobPickupLongitude != nil else {
            return false
        }
        
        if let value = numberFormatter.number(from: Singleton.sharedInstance.selectedTaskDetails.jobPickupLatitude)?.doubleValue {
            latitude = value
        }
        
        if let value = numberFormatter.number(from: Singleton.sharedInstance.selectedTaskDetails.jobPickupLongitude)?.doubleValue {
            longitude = value
        }
        
        
        pickupLocationCoordinate = CLLocation(latitude: latitude, longitude: longitude)
        
        guard LocationTracker.sharedInstance().getLastLocation() != nil else {
            return false
        }
        
        let distance = LocationTracker.sharedInstance().getLastLocation().distance(from: pickupLocationCoordinate)
        if distance > (self.taxiModel?.minimumPickupDistance)! {
            return false
        }
        return true
    }
}

extension TaxiController:NavigationDelegate {
    func setNavigationBar() {
        if navigation == nil {
            navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        }
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = "\(Singleton.sharedInstance.selectedTaskDetails.jobPickupName!)"
        navigation.titleLabel.textAlignment = NSTextAlignment.left
        navigation.backButton.isHidden = true
        navigation.hamburgerButton.isHidden = false
        
        /*================= Blur effect ===================*/
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.navigation.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.navigation.addSubview(blurEffectView)
        self.navigation.sendSubview(toBack: blurEffectView)
        
        self.view.addSubview(navigation)
        self.navigation.changeHamburgerButton()
    }
    
    func backAction() {
        UIView.animate(withDuration: 0.1, animations: {
            //self.jobDetailsView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
        }, completion: { finished in
            _ = self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: false, completion: { finished in
                //self.callback?("Hi")
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
            })
            
        })
    }
    
    func updateNavigationBar(hideBackButton:Bool) {
        if hideBackButton == true {
            self.navigation.hamburgerButton.isHidden = true
            self.navigation.titleLabel.transform = CGAffineTransform(translationX: -25, y: 0)
        } else {
            self.navigation.backButton.isHidden = false
            self.navigation.titleLabel.transform = CGAffineTransform.identity
        }
    }
    
    func updateNavigationTitle(title:String) {
        if title.length > 0 {
            self.navigation.titleLabel.text = "\(title) \(Singleton.sharedInstance.selectedTaskDetails.jobPickupName!)"
        } else {
            self.navigation.titleLabel.text = "\(Singleton.sharedInstance.selectedTaskDetails.jobPickupName!)"
        }
    }
}
extension TaxiController:CustomSliderDelegate {
    func setSliderButton() {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            animationForBottomView()
            return
        }
        
        guard self.slider == nil else {
            self.setJobStatusAndButtonTitle()
//            if(self.taxiDetailsView != nil) {
//                UIView.setAnimationsEnabled(false)
//                self.taxiDetailsView.detailsTable.reloadData()
//                UIView.setAnimationsEnabled(true)
//            }
            return
        }
        
        self.slider = UINib(nibName: NIB_NAME.customSlider, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomSlider
        self.slider.frame = CGRect(x: CGFloat(0), y: SCREEN_SIZE.height - self.sliderHeight, width: SCREEN_SIZE.width, height: self.sliderHeight)
        self.slider.delegate = self
        self.view.addSubview(self.slider)
        self.slider.setShimmerView()
        
        /*============= Shadow =============*/
        self.slider.layer.masksToBounds = false
        self.slider.layer.shadowOffset = CGSize(width: 2, height: -3)
        self.slider.layer.shadowOpacity = 0.7
        self.slider.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        
        self.setJobStatusAndButtonTitle()
//        if(self.taxiDetailsView != nil) {
//            UIView.setAnimationsEnabled(false)
//            self.taxiDetailsView.detailsTable.reloadData()
//            UIView.setAnimationsEnabled(true)
//        }
    }
    
    func updateSliderButton() {
        self.taxiModel?.setSectionsAndRows()
        if(self.taxiDetailsView != nil) {
            UIView.setAnimationsEnabled(false)
            self.taxiDetailsView.detailsTable.reloadData()
            UIView.setAnimationsEnabled(true)
        }
        if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
            self.slider.isHidden = true
        } else {
            self.slider.isHidden = false
        }
        //self.jobDetailsView.bottomConstraintOfDetailTable.constant = self.sliderHeight + 20
        var holdDuration = 0.0
        if self.slider.statusText != nil {
            self.slider.centerLabel.text = self.slider.statusText
            holdDuration = 1.0
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + holdDuration) {
            self.slider.hideLeftButton = self.hideLeftButton
            self.slider.setThumbSliderAndMaxLimit()
            self.slider.centerLabel.alpha = 0.0
            self.slider.setLabeltext(left: self.rejectTitle, right: self.acceptTitle, center: self.stateTitle, loadingText: self.loadingTitle)
            let rgbColour = COLOR.SUCCESSFUL_COLOR.cgColor //self.statusModel.positiveStatusColor!.cgColor
            let rgbColours = rgbColour.components
            self.slider.setPositiveSlidingColor(r: (rgbColours?[0])!, g:  (rgbColours?[1])!, b:  (rgbColours?[2])!)
            
            let negativeColor = COLOR.FAILED_COLOR.cgColor
            let rgbValues = negativeColor.components
            self.slider.setNegativeSlidingColor(r: (rgbValues?[0])!, g:  (rgbValues?[1])!, b:  (rgbValues?[2])!)
            
            self.slider.resetView()
        }
    }
    
    func sliderPositiveAction() {
        self.dismissPartial()
        self.acceptTask()
    }
    
    func sliderNegativeAction() {
        guard IJReachability.isConnectedToNetwork() == true else {
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            self.setJobStatusAndButtonTitle()
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            return
        }
        let oldJobStatus = Singleton.sharedInstance.selectedTaskDetails.jobStatus
        switch self.rejectTitle {
        case BUTTON_TITLE.decline:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.declined
            break
        case BUTTON_TITLE.cancelRide:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.canceled
            break
        case BUTTON_TITLE.failed:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.failed
            break
        default:
            break
        }
        
        self.rejectTask(oldJobStatus!, reason :TEXT.ADD_REASON)
    }
}

extension TaxiController: UIGestureRecognizerDelegate {
    //MARK: PanGesture Delegates
    func valueChanged(_ pan:UIPanGestureRecognizer) {
        let translation  = pan.translation(in: pan.view?.superview)
        var theTransform = pan.view?.transform
        if self.taxiDetailsView.detailsTable.contentOffset.y == 0 {
            if pan.state == UIGestureRecognizerState.began {
                panDirection = self.getJobViewDirection(jobView: pan.view!)
            } else if(pan.state == UIGestureRecognizerState.changed) {
                if panDirection == nil {
                    panDirection = self.getJobViewDirection(jobView: pan.view!)
                }
                switch panDirection! {
                case .topToBottom:
                    if translation.y > 0 {
                        theTransform?.ty = translation.y
                        pan.view?.transform = theTransform!
                    }
                case .bottomToTop:
                    if translation.y < 0 && Double((theTransform?.ty)!) >= 0.0 {
                        theTransform?.ty = minimizeOrigin + translation.y
                        pan.view?.transform = theTransform!
                        self.directionButton.alpha = 0
                    }
                }
            } else if(pan.state == UIGestureRecognizerState.ended) {
                switch panDirection! {
                case .topToBottom:
                    if translation.y > maxTranslatingPointToMinimize {
                        self.translateViewToPoint(point: minimizeOrigin, translatingView: pan.view!)
                    } else {
                        self.translateViewToPoint(point: 0, translatingView: pan.view!)
                    }
                case .bottomToTop:
                    if translation.y > 0 {
                        self.translateViewToPoint(point: minimizeOrigin, translatingView: pan.view!)
                    } else {
                        if translation.y < 0 {
                            self.translateViewToPoint(point: 0, translatingView: pan.view!)
                        }
                    }
                }
            }
        } else {
            //            if panDirection == nil {
            //                panDirection = self.getJobViewDirection(jobView: pan.view!)
            //            }
            //            if(pan.state == UIGestureRecognizerState.ended) {
            //                switch panDirection! {
            //                case .topToBottom:
            //                  if  Double((theTransform?.ty)!) < 0.0 {
            //                        self.translateViewToPoint(point: 0, translatingView: pan.view!)
            //                  }
            //                   break
            //                case .bottomToTop:
            //              //    if Double((theTransform?.ty)!) > 0.0 {
            //                        self.translateViewToPoint(point: minimizeOrigin, translatingView: pan.view!)
            //              //    }
            //                }
            //                if self.signatureView != nil {
            //                    self.hideSignatureView()
            //                }
            //            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

//MARK: TaxiViewDelegate
extension TaxiController:TaxiViewDelegate, MFMessageComposeViewControllerDelegate {

    //MARK: Message Button Action
    func openMessageController(){
        guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
            return
        }
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.TASK_DETAIL_MESSAGE, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.TASK_DETAIL_MESSAGE, customAttributes: [:])
        let messageController: MFMessageComposeViewController = MFMessageComposeViewController()
        if let phoneNumber = Singleton.sharedInstance.selectedTaskDetails.customerPhone {
            if !MFMessageComposeViewController.canSendText() {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NOT_SUPPORT_SMS, isError: .error)
                return
            }
            let recipents: [String] = [phoneNumber]
            messageController.messageComposeDelegate = self
            messageController.recipients = (recipents)
            DispatchQueue.main.async(execute: {
                self.present(messageController, animated: true, completion: nil)
            })
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func delegateForAddressController() {
        self.gotoAddressController()
    }
    
    func slideButtonAction() {
        if self.taxiDetailsView.transform.ty > 0 {
            self.directionButton.alpha = 0
            self.translateViewToPoint(point: 0, translatingView: self.taxiDetailsView)
        } else {
            self.translateViewToPoint(point: minimizeOrigin, translatingView: self.taxiDetailsView)
        }
    }
    
    func navigationAction() {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_START, parameters: [:])
        Answers.logCustomEvent(withName: "Map Start Navigation", customAttributes: [:])
        let destinationCoordinate = self.taxiModel?.getDestinationCoordinates()

        self.directionButton.isEnabled = false
        self.perform(#selector(self.enableDirectionButton), with: nil, afterDelay: 0.5)
        guard destinationCoordinate != nil else {
            return
        }
        
        guard destinationCoordinate?.latitude != 0.0 else {
            return
        }
        
        
        var originCoordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        if(LocationTracker.sharedInstance().myLocation != nil) {
            originCoordinate = LocationTracker.sharedInstance().myLocation.coordinate
        }
        
        if let navigationMap = UserDefaults.standard.value(forKey: USER_DEFAULT.navigationMap) as? Int {
            if navigationMap == NAVIGATION_MAP.appleMap {
                self.openAppleMap(destinationCoordinate: destinationCoordinate!)
            } else {
                let appName = Singleton.sharedInstance.getNavigationAppName()
                let directionMode = Singleton.sharedInstance.getDirectionMode()
                let address = Singleton.sharedInstance.getNavigationAddress(destinationCoordinate: destinationCoordinate!, directionMode: directionMode)
                if(UIApplication.shared.canOpenURL(URL(string: address)!)) {
                    UIApplication.shared.openURL(URL(string: address)!)
                } else{
                    self.navigationErrorMessage(appName: appName, directionMode:directionMode, origin:originCoordinate, destination:destinationCoordinate!)
                }
            }
        }
    }
    
    func openAppleMap(destinationCoordinate:CLLocationCoordinate2D) {
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: destinationCoordinate, addressDictionary:nil))
        mapItem.name = "Destination Address"
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    
    func navigationErrorMessage(appName: String, directionMode:String, origin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D) {
        let alertController = UIAlertController(title: "", message: appName + ERROR_MESSAGE.NAVIGATION_ERROR_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
        let continueAction = UIAlertAction(title: TEXT.CONTINUE, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            let addr = "http://maps.google.com/maps?saddr=\(origin.latitude),\(origin.longitude)&daddr=\(destination.latitude),\(destination.longitude)&mode=\(directionMode)"
            UIApplication.shared.openURL(URL(string: addr)!)
        })
        
        let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(continueAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func callAction() {
        if(Singleton.sharedInstance.selectedTaskDetails.maskingOptionalField.value == 1) {
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.TASK_DETAIL_MASKING_CALL, parameters: [:])
            Answers.logCustomEvent(withName: "Task Detail Masking Call", customAttributes: [:])
            let selectedJobId = Singleton.sharedInstance.selectedTaskDetails.jobId!
            var params:[String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
            params["job_id"] = selectedJobId
//            let params = [
//                "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//                "job_id":selectedJobId
//                ] as [String : Any]
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.masking, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    if(isSucceeded == true) {
                        if ((response["data"] as? [String:Any]) != nil) {
                            switch(response["status"] as! Int) {
                            case STATUS_CODES.SHOW_DATA:
                                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .success)
                                break
                                
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                self.showInvalidAccessTokenPopup(response["message"] as! String)
                                break
                            default:
                                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                                break
                            }
                        }
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                }
            })
        } else if let phoneNumber = Singleton.sharedInstance.selectedTaskDetails.customerPhone{
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.TASK_DETAIL_CALL, parameters: [:])
            Answers.logCustomEvent(withName: "Task Detail Call", customAttributes: [:])
            let phone = "tel://\(phoneNumber.replacingOccurrences(of: " ", with: ""))"
            if let url:URL = URL(string:phone) {
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                alert.addAction(UIAlertAction(title: "Call".localized, style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) in
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.openURL(url)
                    })
                }))
                alert.addAction(UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                Singleton.sharedInstance.showErrorMessage(error: TEXT.CONNECTION_ERROR, isError: .error)
            }
            
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.CALLING_NOT_AVAILABLE, isError: .error)
        }

    }
}

