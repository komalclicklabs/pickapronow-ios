//
//  DayCollectionCell.swift
//  
//
//  Created by cl-macmini-45 on 13/10/16.
//
//

import UIKit

class DayCollectionCell: UICollectionViewCell {

    @IBOutlet var dayLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.dayLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
        // Initialization code
    }

}
