//
//  SaveCancelButton.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

protocol SaveCancelDelegate {
    func saveSchedule()
    func cancelSchedule()
}


class SaveCancelButton: UIView {

    var delegate:SaveCancelDelegate!
    let buttonHeight:CGFloat = 50.0
    let cancelColor = UIColor(red: 141/255, green: 141/255, blue: 141/255, alpha: 1.0)
    let saveColor = COLOR.themeForegroundColor
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    override func awakeFromNib() {
        cancelButton.layer.cornerRadius = 4.0
        saveButton.layer.cornerRadius = 4.0
        
        cancelButton.setTitle("Reset".localized, for: UIControlState.normal)
        saveButton.setTitle("Save".localized, for: UIControlState.normal)
        
        cancelButton.backgroundColor = cancelColor
        saveButton.backgroundColor = saveColor
    }
    
    @IBAction func cancelAction(_ sender: AnyObject) {
        delegate.cancelSchedule()
    }
    
    @IBAction func saveAction(_ sender: AnyObject) {
        delegate.saveSchedule()
    }

}
