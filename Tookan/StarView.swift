//
//  StarView.swift
//  RatingsScreen
//
//  Created by CL-Macmini-110 on 6/29/17.
//  Copyright © 2017 CL-Macmini-110. All rights reserved.
//

import UIKit

enum STAR_FILLING:Int {
    case full
    case half
    case partial
}

@objc protocol RatingDelegate {
    @objc optional func finalRatings(rating:Int)
}


class StarView: UIView {

    var starsArray = [Star]()
    @IBInspectable var totalStars:Int = 5
    @IBInspectable var filledStars:Int = 0
    var starSize:CGFloat = 70.0
    var starSpacings:CGFloat!
    var totalWidthOfStarAndSpacing:CGFloat!
    var starFillingStyle:STAR_FILLING = .full
    @IBInspectable var lineWidth: CGFloat = 1.0
    @IBInspectable var fillColor:UIColor = UIColor.yellow
    @IBInspectable var strokeColor:UIColor = UIColor.yellow
    @IBInspectable var emptyColor:UIColor = UIColor.white
    var delegate:RatingDelegate?
    
    override func awakeFromNib() {
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        self.addGestureRecognizer(panRecognizer)
    }
    
    func setStars(widthOfStar: CGFloat? = nil, spacingBetweenStars: CGFloat? = nil)  {
        if let size = widthOfStar {
            self.starSize = size
        }
        if let spacing = spacingBetweenStars {
            self.starSpacings = spacing
        } else if let spacing = self.starSpacings {
            self.starSpacings = spacing
        } else {
            self.starSpacings = self.getDefaultSpace()
        }
        
        self.totalWidthOfStarAndSpacing = self.totalWidthOfStars() + self.totalWidthOfSpacing()
        var xPos = getXPosition()
        switch starFillingStyle {
        case STAR_FILLING.full:
            for i in 0..<self.totalStars{
                var starView:Star!
                if i != 0 {
                    xPos = xPos + self.starSize + self.starSpacings
                }

                
                starView = Star(xPos: xPos, yPos: 0, size : self.starSize,color : self.emptyColor)
                starView.center = CGPoint(x: starView.center.x, y: self.frame.height/2)
                starView.strokeColor = self.strokeColor.cgColor
                starView.lineWidth = self.lineWidth
                if i < self.filledStars {
                    starView.fillColor = self.fillColor.cgColor
                } else {
                    starView.fillColor = self.emptyColor.cgColor
                }
                
                starView.setPath()
                self.addSubview(starView)
                self.starsArray.append(starView)
            }
        default:
            break
        }
    }
    
    func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: self)
        panGesture.setTranslation(panGesture.location(in: self), in: self)
        if panGesture.state == UIGestureRecognizerState.changed {
            self.setStarsVisual(xAxis: translation.x)
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        switch  0 {
        case STAR_FILLING.full.rawValue:
            let touch = touches.first!
            let location = touch.location(in: self)
            self.setStarsVisual(xAxis: location.x)
        default:
            break
        }
    }
    
    func getXPosition() -> CGFloat {
        while self.frame.width < self.totalWidthOfStarAndSpacing {
            self.totalStars -= 1
            self.totalWidthOfStarAndSpacing = self.totalWidthOfStars() + self.totalWidthOfSpacing()
        }
        return (self.frame.width - self.totalWidthOfStarAndSpacing) / 2
    }
    
    func getDefaultSpace() -> CGFloat {
        return (self.frame.width - starSize * CGFloat(totalStars)) / CGFloat(totalStars + 1)
    }
    
    func totalWidthOfStars() -> CGFloat {
        return self.starSize * CGFloat(self.totalStars)
    }
    
    func totalWidthOfSpacing() -> CGFloat {
        return self.starSpacings * CGFloat(self.totalStars - 1)
    }
    
    func setStarsVisual(xAxis:CGFloat) {
        for i in 0..<self.starsArray.count {
            let star = self.starsArray[i]
            let starXAxis = star.frame.origin.x
            if xAxis < starXAxis {
                self.updateStarView(star: star, color:self.emptyColor)
                if i == 0 {
                    self.filledStars = 0
                }
            } else {
                self.updateStarView(star: star, color:self.fillColor)
                self.filledStars = i + 1
            }
        }
        self.delegate?.finalRatings!(rating: self.filledStars)
    }
    
    func updateStarView(star:Star, color:UIColor) {
        star.shapeLayer.fillColor = color.cgColor
    }
}
