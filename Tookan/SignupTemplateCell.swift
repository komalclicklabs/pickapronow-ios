//
//  SignupTemplateCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 02/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol SignupTemplateDelegate {
    func customFieldShouldBeginEditing(textView:UITextView) -> Bool
    func customFieldShouldReturn(textView:UITextView) -> Bool
    func updateTableHeight(descriptionText: String)
    func customFieldDidEndEditing(textView:UITextView)
}

class SignupTemplateCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet var bottomLine: UIView!
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var detailTextView: UITextView!
    @IBOutlet var placeholderLabel: UILabel!
    var delegate:SignupTemplateDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*------------- Placeholder label -----------*/
        placeholderLabel.textColor = COLOR.LIGHT_COLOR
        placeholderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        
        /*---------- Text View -------------*/
        detailTextView.textColor = COLOR.TEXT_COLOR
        detailTextView.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        detailTextView.delegate = self
        detailTextView.tintColor = COLOR.themeForegroundColor
        
        self.bottomLine.backgroundColor = COLOR.LIGHT_COLOR
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: UITEXTVIEW DELEGATE
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty == true {
            self.tranlatePlaceholderLabel(status: false)
        }else{
            self.tranlatePlaceholderLabel(status: true)
        }
        delegate?.updateTableHeight(descriptionText: textView.text)
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty == true{
            self.tranlatePlaceholderLabel(status:false)
        }
        delegate?.customFieldDidEndEditing(textView: textView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            return delegate!.customFieldShouldReturn(textView: textView)
        }
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return delegate!.customFieldShouldBeginEditing(textView: textView)
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return delegate!.customFieldShouldReturn(textView: textView)
    }

    //MARK: Moving PlaceHolder
    func tranlatePlaceholderLabel(status:Bool) {
        if status == true {
            if self.placeholderLabel.transform.ty != 0 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.placeholderLabel.transform = CGAffineTransform.identity
                })
            }
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                if self.placeholderLabel.transform.ty == 0 {
                    self.placeholderLabel.transform = CGAffineTransform(translationX: 0, y: self.detailTextView.center.y - 21)
                }
            })
        }
    }
}
