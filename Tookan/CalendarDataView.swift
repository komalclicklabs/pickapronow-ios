//
//  CalendarDataVIew.swift
//  Tookan
//
//  Created by Click Labs on 7/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit
import Foundation

class CalendarDataView: NSObject {
    
    var currentMonthArray = [String]()
    var nextMonthArray = [String]()
    var previousMonthArray = [String]()
    
    var currentMonthName = String()
    var nextMonthName = String()
    var previousMonthName = String()
    
    var firstDayCurrentMonth = String()
    var firstDayNextMonth = String()
    var firstDayPreviousMonth = String()
    
    let calendar = NSCalendar.current
    var dateComponent = DateComponents()
   // var midMonthDate:NSDate!
    
    
    func currentMonth(_ multiplier: Int) -> [String:AnyObject] {
       
        currentMonthArray = []
        var cells = 0
        var currentMonthDict = [String:AnyObject]()
        let midMonthDate = self.getMidMonthDate(1 * multiplier)
        currentMonthName = "\(self.getMonthName(midMonthDate)) \(Auxillary.getCurrentYear(midMonthDate, format: "yyyy"))"
        firstDayCurrentMonth = self.getFirstWeekday(midMonthDate)
        cells = Auxillary.getNumberOfDaysInCurrentMonth(midMonthDate)
        for i in 1...cells {
            currentMonthArray.append(String(i))
        }
        let paddingCells = WeekDay(firstDayCurrentMonth)
        if (paddingCells != 0){
            for _ in 0...(paddingCells-1){
                currentMonthArray.insert("", at: 0)
            }
            for _ in 0...(42 - cells - paddingCells-1){
                currentMonthArray.append("")
            }
        }
        else {
            for _ in 0...(42 - cells - 1){
                currentMonthArray.append("")
            }
        }
        currentMonthDict["dates"] = currentMonthArray as AnyObject
        currentMonthDict["monthYear"] = currentMonthName as AnyObject
        currentMonthDict["month"] = "\(Auxillary.getCurrentMonthInNumeric(midMonthDate))" as AnyObject
        currentMonthDict["year"] = "\(Auxillary.getCurrentYear(midMonthDate, format: "yyyy"))" as AnyObject
        return currentMonthDict
    }
    
    func nextMonth(_ multiplier : Int) -> [String] {
        nextMonthArray = []
        var cells = 0
        let midMonthDate = self.getMidMonthDate(1 * multiplier)
        nextMonthName = "\(self.getMonthName(midMonthDate)) \(Auxillary.getCurrentYear(midMonthDate, format: "yyyy"))"
        firstDayNextMonth = self.getFirstWeekday(midMonthDate)
        cells = Auxillary.getNumberOfDaysInCurrentMonth(midMonthDate)
        for i in 1...cells {
            nextMonthArray.append(String(i))
        }
        let paddingCells = WeekDay(firstDayNextMonth)
        if (paddingCells != 0){
            for _ in 0...(paddingCells-1){
                nextMonthArray.insert("", at: 0)
            }
            for _ in 0...(42 - cells - paddingCells-1){
                nextMonthArray.append("")
            }
        }
        else {
            for _ in 0...(42 - cells - 1){
                nextMonthArray.append("")
            }
        }
        return nextMonthArray
    }
    
    func previousMonth(_ multiplier : Int) -> [String]{
    
        previousMonthArray = []
        var cells = 0
        let newMultiplier = -1 * (multiplier * -1)
        let midMonthDate = self.getMidMonthDate(newMultiplier)
        previousMonthName = "\(self.getMonthName(midMonthDate)) \(Auxillary.getCurrentYear(midMonthDate, format: "yyyy"))"
        firstDayPreviousMonth = self.getFirstWeekday(midMonthDate)
        cells = Auxillary.getNumberOfDaysInCurrentMonth(midMonthDate)
        for i in 1...cells {
            previousMonthArray.append(String(i))
        }
        let paddingCells = WeekDay(firstDayPreviousMonth)
        if (paddingCells != 0){
            for _ in 0...(paddingCells-1){
                previousMonthArray.insert("", at: 0)
            }
            for _ in 0...(42 - cells - paddingCells-1){
                previousMonthArray.append("")
            }
        }
        else {
            for _ in 0...(42 - cells - 1){
                previousMonthArray.append("")
            }
        }

        
        return previousMonthArray
    }
    
    func WeekDay(_ day: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!
        let weekdayArray = dateFormatter.weekdaySymbols
        var index: Int!
        switch day {
        case (weekdayArray?[0])!:
            index = 0
            break
        case (weekdayArray?[1])!:
            index = 1
            break
        case (weekdayArray?[2])!:
            index = 2
            break
        case (weekdayArray?[3])!:
            index = 3
            break
        case (weekdayArray?[4])!:
            index = 4
            break
        case (weekdayArray?[5])!:
            index = 5
            break
        case (weekdayArray?[6])!:
            index = 6
            break
        default :
            index = 0
            break
        }
        return index
    }
    
    func getMidMonthDate(_ multiplier:Int) -> Date {
        let date = NetworkingHelper.sharedInstance.currentDate.asDateFormattedWith("yyyy-MM-dd")!//Date()
        dateComponent.month = multiplier
        return (calendar as NSCalendar).date(byAdding: dateComponent, to: date, options: [])!
    }
    
    func getFirstWeekday(_ midMonthDate:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "dd"
        let currentDate = dateFormatter.string(from: midMonthDate)
        var decrementor = (Int(currentDate)!) - 1
        decrementor = -decrementor
        let newDate:Date = (calendar as NSCalendar).date(byAdding: .day, value: decrementor, to: midMonthDate, options: [])!
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!//NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: newDate)
    }
    
    func getMonthName(_ midMonthDate:Date) -> String {
        let monthFormatter = DateFormatter()
        monthFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!
        monthFormatter.dateFormat = "MMMM"
        return monthFormatter.string(from: midMonthDate)  //day name contains current month name
    }
    
    
    
}
