//
//  TodayButton.swift
//  Tookan
//
//  Created by cl-macmini-45 on 11/11/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
protocol TodayButtonDelegate {
    func todayTaskAction()
}

class TodayButton: UIView {

    @IBOutlet var icon: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var todayLabel: UILabel!
    var delegate:TodayButtonDelegate!
    
    override func awakeFromNib() {
         self.backgroundColor = COLOR.navigationBackgroundColor
        /*=================== Tap Gesture ===============*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.buttonTapped))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
       
        /*============= Today Label ===========*/
        self.todayLabel.textColor = COLOR.themeForegroundColor
        self.todayLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.todayLabel.text = TEXT.TODAY
        
        /*============= Date Label ============*/
        self.dateLabel.textColor = COLOR.themeForegroundColor
        self.dateLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small - 2)
        
        /*============= icon ================*/
        self.icon.image = #imageLiteral(resourceName: "iconTodayTap").withRenderingMode(.alwaysTemplate)
        self.icon.tintColor = COLOR.themeForegroundColor
    }
    
    func updateDate() {
        let dateFormatter = DateFormatter()
        let currentDate = Auxillary.currentDate()
        dateFormatter.timeZone = TimeZone(abbreviation:"UTC")
        dateFormatter.dateFormat = "dd"
        dateLabel.text = dateFormatter.string(from: currentDate)
    }
    
    func buttonTapped() {
        delegate.todayTaskAction()
    }

}
