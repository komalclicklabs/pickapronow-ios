//
//  BarGraphCell.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 6/19/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class BarGraphCell: UICollectionViewCell, CAAnimationDelegate {

    @IBOutlet weak var daysButton: UIButton!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var valueLabelBottomOutlet: NSLayoutConstraint!
    let model = AvailabilityModel()
    var widthOfCell : CGFloat!
    var heightOfBar: CGFloat! = 50.0
    var noOfLines = 5
    var outlineShape = CAShapeLayer()
    var shadowShape = CAShapeLayer()
    var filledShape = CAShapeLayer()
    var shadowLayer = CALayer()
    var line = CAShapeLayer()
    var linePath = UIBezierPath()
    
    var drawPath = UIBezierPath()
    var shadowPath = UIBezierPath()
    
    //let animDuration = TimeInterval(2.0) // the duration of one 'segment' of the animation
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.daysButton.titleLabel?.numberOfLines = 2
        self.daysButton.isEnabled = false
    }

    
    func resetCellData() {
        self.drawPath.removeAllPoints()
        self.shadowPath.removeAllPoints()
        self.shadowLayer.removeFromSuperlayer()
        self.shadowLayer.shadowPath = nil
        self.filledShape.removeFromSuperlayer()
        self.filledShape.path = nil
        self.outlineShape.removeFromSuperlayer()
        self.outlineShape.path = nil
        self.shadowShape.removeFromSuperlayer()
        self.shadowShape.path = nil
        self.line.removeFromSuperlayer()
        self.line.path = nil
        self.linePath.removeAllPoints()
        line = CAShapeLayer()
        linePath = UIBezierPath()
        drawPath = UIBezierPath()
        shadowPath = UIBezierPath()
        
        outlineShape = CAShapeLayer()
        shadowShape = CAShapeLayer()
        filledShape = CAShapeLayer()
        shadowLayer = CALayer()
        
        self.setNeedsDisplay()
        
        self.daysButton.setAttributedTitle(NSMutableAttributedString(string: ""), for: .normal)
    }
    
    func setValueOfRupeesLabel(_ valueOfBar:Float, heightOfCell:CGFloat) {
        self.bringSubview(toFront: valueLabel)
        if valueOfBar == 0.0 {
            self.valueLabel.text = ""
        } else {
            self.valueLabel.text = "\(valueOfBar)"
        }
        self.valueLabelBottomOutlet.constant = heightOfBar + heightOfCell + FONT_SIZE.extraSmall
        self.valueLabel.textColor = COLOR.LITTLE_LIGHT_COLOR//UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        self.valueLabel.font = UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.extraSmall)
        
    }
    

    func horizontalLine(index:Int, maxHeightOfBar:CGFloat) {
        
        let decrement:CGFloat = (maxHeightOfBar / CGFloat(noOfLines - 1))
        print(decrement)
        var yPos = frame.height - FONT_SIZE.extraSmall / 2
        for _ in 1...noOfLines{
            self.line.removeFromSuperlayer()
            if index == 0{
                linePath.move(to: CGPoint(x:widthOfCell / 4, y:yPos-decrement))
            } else {
                linePath.move(to: CGPoint(x:0, y:yPos-decrement))
            }
            if index == 6 {
                linePath.addLine(to: CGPoint(x:(widthOfCell / 4) * 3, y:yPos-decrement))
            } else {
                linePath.addLine(to: CGPoint(x:widthOfCell, y:yPos-decrement))
            }

            line.path = linePath.cgPath
            line.strokeColor = COLOR.LINE_COLOR.cgColor//UIColor(red: 245/255, green: 247/255, blue: 248/255, alpha: 1).cgColor
            line.lineWidth = 0.5
            line.lineJoin = kCALineJoinRound
            self.layer.insertSublayer(line, at: 0)
            
            yPos -= decrement
        }
    }
    
    func setButton(currentDay:String, currentDate:String, textColor:UIColor)   {
        let attributedTitle = NSMutableAttributedString(string: "\(currentDay)\n", attributes: [NSForegroundColorAttributeName:textColor, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.medium)!])
        
        let subAttributedTitle = NSMutableAttributedString(string: "\(currentDate)", attributes: [NSForegroundColorAttributeName:textColor, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: getAspectRatioValue(value: 10.0))!])
        
        attributedTitle.append(subAttributedTitle)
        self.daysButton.setAttributedTitle(attributedTitle, for: .normal)
        self.daysButton.titleLabel?.textAlignment = .center
    }
    
    func setRectWithCornerRadius(ratio:CGFloat, graphColor:UIColor, shadowColor:UIColor,selectedIndex: Int, maxHeightOfBar:CGFloat, heightOfCell:CGFloat) {
        
        heightOfBar = maxHeightOfBar * ratio
        let xPos = widthOfCell/8
        let yPos = frame.height - heightOfBar - heightOfCell - FONT_SIZE.extraSmall / 2
        let widthOfBar = (widthOfCell/4)*3
        
      
        self.drawPath = UIBezierPath(roundedRect: CGRect(x: xPos, y: yPos, width: widthOfBar, height: heightOfBar), cornerRadius: 2.0)
        self.filledShape.path = drawPath.cgPath
        self.filledShape.fillColor = graphColor.cgColor//UIColor(red: 70/255, green: 149/255, blue: 246/255, alpha: 1).cgColor
        self.layer.addSublayer(filledShape)

        self.shadowPath = UIBezierPath(roundedRect: CGRect(x: xPos-4, y: yPos-2, width: widthOfBar, height: heightOfBar+2), cornerRadius: 2.0)
        self.shadowLayer.shadowPath = shadowPath.cgPath
        self.shadowLayer.shadowOffset = CGSize(width: 0, height: 0)
        self.shadowLayer.shadowRadius = 2
        self.shadowLayer.shadowOpacity = 0.2
        self.shadowLayer.shadowColor = shadowColor.cgColor//UIColor(red: 70/255, green: 149/255, blue: 246/255, alpha: 1).cgColor
        self.layer.addSublayer(shadowLayer)
        
        if selectedIndex == -1 {
            self.outlineShape.path = CGPath(rect: drawPath.bounds, transform: nil)
            self.filledShape.mask = outlineShape
            
            self.shadowShape.path = CGPath(rect: shadowPath.bounds, transform: nil)
            self.shadowLayer.mask = shadowShape
            
            self.startAnimation()
        }
        
    }

    func startAnimation() {
        let animateOutlineFromBottom = CABasicAnimation(keyPath: "position")
        animateOutlineFromBottom.fromValue = NSValue(cgPoint: CGPoint(x: 0, y: heightOfBar))
        animateOutlineFromBottom.toValue = NSValue(cgPoint:CGPoint(x: 0, y: 0))
        animateOutlineFromBottom.duration = 0.35
        animateOutlineFromBottom.fillMode = kCAFillModeForwards
        animateOutlineFromBottom.isRemovedOnCompletion = false
        self.outlineShape.add(animateOutlineFromBottom, forKey:"bounds.size.height")
        self.shadowShape.add(animateOutlineFromBottom, forKey:"bounds.size.height")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {
            self.valueLabel.isHidden = false
        }
    }
    
}
