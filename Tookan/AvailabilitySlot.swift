//
//  AvailabilitySlot.swift
//  Tookan
//
//  Created by cl-macmini-45 on 17/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class AvailabilitySlot: NSObject {

    var slotTiming = String()
    var availabilityStatus:Int? = 0
    var lastAvailabilityStatus:Int? = 0
    var slotStartTime = String()
    var slotEndTime = String()
    var jobs = [Any]()
    
    required init(coder aDecoder: NSCoder) {
        availabilityStatus = aDecoder.decodeObject(forKey: "availabilityStatus") as? Int
        lastAvailabilityStatus = aDecoder.decodeObject(forKey: "lastAvailabilityStatus") as? Int
        slotTiming = aDecoder.decodeObject(forKey: "slotTiming") as! String
        slotStartTime = aDecoder.decodeObject(forKey: "slotStartTime") as! String
        slotEndTime = aDecoder.decodeObject(forKey: "slotEndTime") as! String
        jobs = aDecoder.decodeObject(forKey: "jobs") as! [Any]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(availabilityStatus, forKey: "availabilityStatus")
        aCoder.encode(availabilityStatus, forKey: "lastAvailabilityStatus")
        aCoder.encode(slotTiming, forKey: "slotTiming")
        aCoder.encode(slotStartTime, forKey: "slotStartTime")
        aCoder.encode(slotEndTime, forKey: "slotEndTime")
        aCoder.encode(jobs, forKey: "jobs")
    }
    
    
    init(json: [String:Any]) {
        if let status = json["available_status"] as? Int {
            self.availabilityStatus = status
            self.lastAvailabilityStatus = status
        } else {
            self.availabilityStatus = 0
            self.lastAvailabilityStatus = 0
        }
        
        if let timing = json["slot_timming"] as? String {
            self.slotTiming = timing
        }
        
        if let start = json["slot_start_time"] as? String {
            self.slotStartTime = start
        }
        
        if let end = json["slot_end_time"] as? String {
            self.slotEndTime = end
        }
        
        if let items = json["jobs"] as? [Any]{
            self.jobs = items
        }
    }
}
