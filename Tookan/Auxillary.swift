//
//  Auxillary.swift
//  Tookan
//
//  Created by Click Labs on 8/1/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import CoreTelephony
import CoreData
import AVFoundation
import SDKDemo1
class Auxillary: NSObject {
    
    
    var newArray = [String]()
    
    func tasksAssignedDatesFilter(_ dataArray: Array<MonthTasksList>) -> [String] {
        for item in dataArray {
            
            if item.totalTasks != 0 {
                let dateFormatter = DateFormatter()
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
                dateFormatter.dateFormat = "yyyy-MM-dd"
                if let date = dateFormatter.date(from: item.date) {
                    dateFormatter.dateFormat = "d"
                    let newDate = dateFormatter.string(from: date)
                    newArray.append(newDate)
                }
            }
        }
        return newArray
    }
    
    class func validateEmail(_ candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    class func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        let urlRegex = "((http|https)://)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        return NSPredicate(format: "SELF MATCHES %@", urlRegex).evaluate(with: urlString)
    }
    
    class func isHtmlString(htmlString:String) -> Bool {
        let urlRegex = "<([a-z][a-z0-9]*)[^>]*(.*?)</([a-z][a-z0-9]*)>"
        return NSPredicate(format: "SELF MATCHES %@", urlRegex).evaluate(with: htmlString)
    }
    
    class func setTaskStatusFields(_ taskStatusLabel:UILabel, jobStatus :Int, appOptionalField : Int){
        switch jobStatus{
        case 0:
            taskStatusLabel.text = "Assigned".localized
            taskStatusLabel.backgroundColor = UIColor(red: 252/255, green: 131/255, blue: 68/255, alpha: 1)
        case 1:
            taskStatusLabel.text = "Started".localized
            taskStatusLabel.backgroundColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
        case 4:
            taskStatusLabel.text = "Arrived".localized
            taskStatusLabel.backgroundColor = UIColor(red: 63/255, green: 81/255, blue: 181/255, alpha: 1)
        case 2:
            taskStatusLabel.text = "Successful".localized
            taskStatusLabel.backgroundColor = UIColor(red: 44/255, green: 159/255, blue: 44/255, alpha: 1)
        case 3:
            taskStatusLabel.text = "Failed".localized
            taskStatusLabel.backgroundColor = UIColor(red: 229/255, green: 57/255, blue: 53/255, alpha: 1)
        case 9:
            taskStatusLabel.text = "Cancelled".localized
            taskStatusLabel.backgroundColor = UIColor(red: 229/255, green: 57/255, blue: 53/255, alpha: 1)
        case 7:
            if appOptionalField == 1{
                taskStatusLabel.text = "Accepted".localized
            }else{
                taskStatusLabel.text = "Acknowledged".localized
            }
            taskStatusLabel.backgroundColor = UIColor(red: 186/255, green: 104/255, blue: 200/255, alpha: 1)
        case 8:
            taskStatusLabel.text = "Declined".localized
            taskStatusLabel.backgroundColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
        default:
            break
        }
    }

    class func getMonthName(_ month: Int) -> String {
        switch month {
        case 1:
            return "Jan".localized
        case 2:
            return "Feb".localized
        case 3:
            return "Mar".localized
        case 4:
            return "Apr".localized
        case 5:
            return "May".localized
        case 6:
            return "Jun".localized
        case 7:
            return "Jul".localized
        case 8:
            return "Aug".localized
        case 9:
            return "Sep".localized
        case 10:
            return "Oct".localized
        case 11:
            return "Nov".localized
        case 12 :
            return "Dec".localized
        default:
        break
        }
        return ""
    }
    
    class func getTimeFromTimeAndDate(_ dateReceived: String, onlyTime:Bool, onlyDate:Bool) -> String{
        var timeToSend = String()
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss" 
        let time1 = formatter.date(from: dateReceived)
        if(time1 != nil) {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           // print(dateReceived)
            let todayDate = formatter.date(from: dateReceived)!
            if(onlyTime) {
                formatter.dateFormat = "hh:mm a"
            } else if(onlyDate == true) {
                formatter.dateFormat = "dd MMM, yyyy"
            } else {
                formatter.dateFormat = "hh:mm a, dd MMM"
            }
            timeToSend = formatter.string(from: todayDate)
        } else {
            timeToSend = "N/A"
        }
        
        return timeToSend
    }

    class func convertStringToDate(_ dateString:String) -> Date {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.timeZone = TimeZone(abbreviation:  "UTC")
        styler.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFromServer = styler.date(from: dateString)
        //NSTimeZone(name: "UTC")
        if(dateFromServer != nil) {
            styler.timeZone = TimeZone.autoupdatingCurrent
            let utcDateString = styler.string(from: dateFromServer!)
            styler.timeZone = TimeZone(abbreviation:  "UTC")
            return styler.date(from: utcDateString)!
        } else {
            return Date()
        }
    }
    
    class func convertUTCStringToLocalString(_ utcDate:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation:  "UTC")
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: utcDate)
        dateFormatter.timeZone = TimeZone.current
        guard date != nil else {
            return ""
        }
        return dateFormatter.string(from: date!)
    }
    
    class func convertDateInto12HoursString(withFormat:String, dateToConvert:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation:  "UTC")
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = withFormat
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: dateToConvert)
    }
    
    class func getLocalDateString() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    class func getOnlyLocalDate() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    
    class func getUTCDateString() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation:  "UTC")
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    
    class func convertDateToString() -> String {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.timeZone = TimeZone(abbreviation:  "UTC")
        styler.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        return styler.string(from: Date())
    }
    
    class func convert24HoursTimeInto12HoursFormat(timeFormat24:String) -> String {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!
        styler.timeZone = TimeZone(abbreviation:  "UTC")
        styler.dateFormat = "HH:mm"
        let time = styler.date(from: timeFormat24)
        styler.dateFormat = "hh:mm a"
        let timeFormat12 = styler.string(from: time!)
        return timeFormat12
    }
    
    class func currentDate() -> Date {
       let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.timeZone = TimeZone.autoupdatingCurrent
        styler.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        let currentDateString = styler.string(from: Date())
         styler.timeZone = TimeZone(abbreviation:  "UTC")
        return (styler.date(from: currentDateString))!
    }
    
    class func getCurrentMonthInNumeric(_ midMonthDate:Date) -> Int {
        let monthDateFormatter = DateFormatter()
        monthDateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        monthDateFormatter.dateFormat = "M"
        return Int(monthDateFormatter.string(from: midMonthDate))!
    }
    
    class func getCurrentYear(_ midMonthDate:Date, format:String) -> Int {
        let yearFormatter = DateFormatter()
        yearFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        yearFormatter.dateFormat = format
        return Int(yearFormatter.string(from: midMonthDate))!
    }
    
    class func getCurrentDay(_ midMonthDate:Date) -> Int {
        let yearFormatter = DateFormatter()
        yearFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        yearFormatter.dateFormat = "dd"
        return Int(yearFormatter.string(from: midMonthDate))!
    }
    
    class func getNextMonth(currentDate:Date, format:String) -> String {
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
        let newDate:Date = (calendar as NSCalendar).date(byAdding: .month, value: 1, to: currentDate, options: [])!
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!//NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: newDate)
    }
    
    class func getNextMonthYear(currentDate:Date) -> String {
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
        let newDate:Date = (calendar as NSCalendar).date(byAdding: .month, value: 1, to: currentDate, options: [])!
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!//NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yy"
        return dateFormatter.string(from: newDate)
    }
    
    class func getNumberOfDaysInCurrentMonth(_ midMonthDate:Date) -> Int {
        var dateComponents = DateComponents()
        dateComponents.year = self.getCurrentYear(midMonthDate, format: "yyyy")
        dateComponents.month = self.getCurrentMonthInNumeric(midMonthDate)
        let newCalendar = NSCalendar.current
        let newDate = newCalendar.date(from: dateComponents)!
        let range = (newCalendar as NSCalendar).range(of: .day, in: .month, for: newDate)
        return range.length
    }

    
    class func showAlert(_ message:String) {
        DispatchQueue.main.async(execute: { () -> Void in
            let alertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK".localized)
            alertView.show()
        })
    }
    
    class func currentDialingCode() -> String {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
            // Get carrier name
        return (carrier?.isoCountryCode) != nil ? (carrier?.isoCountryCode)! : ""
    }
    
    class func logoutFromDevice() {
        Singleton.sharedInstance.cancelAllURLRequest()
        OperationQueue.main.cancelAllOperations()
        DatabaseManager.sharedInstance.deleteNotificationDataFromDatabase()
        DatabaseManager.sharedInstance.deleteUserInfoFromDatabase(DatabaseManager.sharedInstance.managedObjectContext)
        DatabaseManager.sharedInstance.deletePolylineFromDatabase(DatabaseManager.sharedInstance.managedObjectContext)
        DatabaseManager.sharedInstance.deleteAllImagesFromDatabase()
        DatabaseManager.sharedInstance.deleteAllUnsyncedTaskFromDatabase()
        DatabaseManager.sharedInstance.deleteAssignedTaskFromDatabase(DatabaseManager.sharedInstance.managedObjectContext)
        
        Singleton.sharedInstance.selectedTaskDetails = nil
        Singleton.sharedInstance.selectedPickupJobDetails = nil
        Singleton.sharedInstance.selectedDeliveryJobDetails = nil
        UserDefaults.standard.removeObject(forKey: "accessToken")
        UserDefaults.standard.removeObject(forKey: "BatteryLog")
        NetworkingHelper.sharedInstance.dayTasksListArray.removeAll(keepingCapacity: false)
        if Singleton.sharedInstance.selectedTaskRelatedArray != nil {
            Singleton.sharedInstance.selectedTaskRelatedArray.removeAll(keepingCapacity: false)
        }
        NetworkingHelper.sharedInstance.newTaskListArray.removeAll(keepingCapacity: false)
        NetworkingHelper.sharedInstance.notificationCount = 0
        NetworkingHelper.sharedInstance.allNotificationDataArray.removeAll(keepingCapacity: false)
        NetworkingHelper.sharedInstance.notificationArray.removeAll(keepingCapacity: false)
        UserDefaults.standard.set([Any](), forKey: USER_DEFAULT.locationArray)
        UserDefaults.standard.setValue([Any](), forKey: "SavedLocations")
        UserDefaults.standard.setValue([Any](), forKey: "SocketLocations")
        UserDefaults.standard.setValue([Any](), forKey: USER_DEFAULT.updatingLocationPathArray)
        UserDefaults.standard.setValue(nil, forKey: USER_DEFAULT.lastAccurateUserLocation)
        UserDefaults.standard.setValue(nil, forKey: USER_DEFAULT.filterDictionary)
        UserDefaults.standard.setValue(nil, forKey: USER_DEFAULT.sortingDictionary)
        UserDefaults.standard.setValue(nil, forKey: USER_DEFAULT.templateDictionary)
        UserDefaults.standard.setValue("", forKey: USER_DEFAULT.searchTextForFilter)
        NotificationCenter.default.removeObserver(self)
        LocationTracker.sharedInstance().stopLocationTracking()
        LocationTracker.sharedInstance().sendFirstTimeLocation = true
        FuguConfig.shared.clearFuguUserData()
        self.resetFilterValue()
        Singleton.sharedInstance.logoutFromDevice()
    }
    
    class func drawSingleLine(_ xOrigin:CGFloat, yOrigin:CGFloat, lineWidth:CGFloat, lineHeight:CGFloat) -> UIView {
        let line = UIView()
        line.frame = CGRect(x: xOrigin, y: yOrigin, width: lineWidth, height: lineHeight)
        line.backgroundColor = UIColor().lineColor
        return line
    }
    
    class func saveImageToDocumentDirectory(_ imageName:String, image:UIImage) -> String {
        let filePath = self.createPath(imageName)
        let pngImageData = UIImageJPEGRepresentation(image, 0.1)
        let fileManager = FileManager.default
        if(fileManager.fileExists(atPath: filePath) == true) {
            do {
                try fileManager.removeItem(atPath: filePath)
                NetworkingHelper.sharedInstance.allImagesCache.removeObject(forKey: "\(imageName)" as NSString)
            } catch let error as NSError {
                print("Error in removing Path = \(error.description)")
            }

        }
        try? pngImageData!.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
        return imageName
    }
    
    class func deleteAllFilesFromDocumentDirectory(){
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let imagePath = URL(fileURLWithPath: dirPath).appendingPathComponent("Image")
        let fileManager = FileManager.default
        do {
            let directoryContents:[URL] = try fileManager.contentsOfDirectory(at: imagePath, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles) //fileManager.contentsOfDirectoryAtPath(tempDirPath, error: error)?
            for path in directoryContents {
                //print(path)
                let fullPath = imagePath.appendingPathComponent((path).path)//dirPath.stringByAppendingString(path as! String)
                do {
                    //print(fullPath)
                    try fileManager.removeItem(at: fullPath)
                } catch let error as NSError {
                    print("Error in removing Path = \(error.description)")
                }
            }
        } catch let error as NSError {
           print("Error in removing Path = \(error.description)")
        }
    }
    
    class func deleteImageFromDocumentDirectory(_ imagePath:String) {
       // print(imagePath)
        let fileManager = FileManager.default
        let filePath = self.createPath(imagePath)
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print("Error in removing Path = \(error.description)")
        }
    }
    
    class func createPath(_ imageName:String) -> String {
        let fileManager = FileManager.default
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        let imageDirUrl = url.appendingPathComponent("Image")
        var isDirectory:ObjCBool = false
        if fileManager.fileExists(atPath: imageDirUrl.path, isDirectory: &isDirectory) == false {
            do {
                try FileManager.default.createDirectory(atPath: imageDirUrl.path, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        let filePath = imageDirUrl.appendingPathComponent(imageName).path
        return filePath
    }
    
    class func isFileExistAtPath(_ path:String) -> Bool {
      //  print(path)
        let filePath = self.createPath(path)
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: filePath)
    }
    
    // Reset FilterDictionary and Sorting Dictionary after logout
    class func resetFilterValue() {
        Singleton.sharedInstance.filterStatusArray = [FilterStatus]()
        Singleton.sharedInstance.templateDictionary = [FilterStatus]()
        Singleton.sharedInstance.sortingDictionary = [FilterStatus]()
      //  UserDefaults.standard.set(NetworkingHelper.sharedInstance.filterDictionary, forKey: USER_DEFAULT.filterDictionary)
      //  UserDefaults.standard.set(NetworkingHelper.sharedInstance.templateDictionary, forKey: USER_DEFAULT.templateDictionary)
      //  UserDefaults.standard.set(NetworkingHelper.sharedInstance.sortingDictionary, forKey: USER_DEFAULT.sortingDictionary)
    }
    
    class func isAppSyncingEnable() -> Bool {
        switch UserDefaults.standard.integer(forKey: USER_DEFAULT.appCaching) {
        case APP_CACHING.ON:
            return true
        default:
            return false
        }
    }
    
    class func locationWithBearing(_ bearing:Double, distanceMeters:Double, origin:CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let distRadians = distanceMeters / (6372797.6)
        let rbearing = bearing * .pi / 180.0
        let lat1 = origin.latitude * .pi / 180
        let lon1 = origin.longitude * .pi / 180
        let lat2 = asin(sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(rbearing))
        let lon2 = lon1 + atan2(sin(rbearing) * sin(distRadians) * cos(lat1), cos(distRadians) - sin(lat1) * sin(lat2))
        return CLLocationCoordinate2D(latitude: lat2 * 180 / .pi, longitude: lon2 * 180 / .pi)
    }
}
