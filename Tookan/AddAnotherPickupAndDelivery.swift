//
//  AddAnotherPickupAndDelivery.swift
//  Tookan
//
//  Created by cl-macmini-150 on 13/04/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class AddAnotherPickupAndDelivery: UITableViewCell {

    @IBOutlet weak var addAnotherTaskLabel: UILabel!
    @IBOutlet weak var addIcon: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var borderOfAddButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        /*===============   Setting Left Dots   ========================*/
        self.topView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "iconPattern"))
        self.bottomView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "iconPattern"))

        /*===============   Setting font Of Label   ========================*/
        self.addAnotherTaskLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.addAnotherTaskLabel.textColor = COLOR.themeForegroundColor
        
        /*===============   Setting Add Button   ========================*/
        borderOfAddButton.layer.cornerRadius = borderOfAddButton.frame.size.width/2
        borderOfAddButton.clipsToBounds = true
        borderOfAddButton.layer.borderWidth = 1
        borderOfAddButton.layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.1).cgColor
        borderOfAddButton.setImage(#imageLiteral(resourceName: "plus").withRenderingMode(.alwaysTemplate), for: .normal)
        borderOfAddButton.tintColor = COLOR.themeForegroundColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
