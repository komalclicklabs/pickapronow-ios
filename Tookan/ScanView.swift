//
//  ScanView.swift
//  RKScanner
//
//  Created by Rakesh Kumar on 20/07/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import AVFoundation
public protocol ScannerDelegate {
    func decodedOutput(_ value:String)
    func dismissScannerView()
}

open class ScanView: UIView, AVCaptureMetadataOutputObjectsDelegate {
    open var delegate:ScannerDelegate!
    let session = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var ratio:CGFloat = 0.208
    var ratioHeight:CGFloat!
    var scanningLine = UIImageView()
    let manualTextView = UITextView()
    let placeHolderLabel = UILabel()
    let doneButton = UIButton()
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    let heightOfTextView:CGFloat = 50.0
    let ypos:CGFloat = SCREEN_SIZE.height - 50.0 - Singleton.sharedInstance.getBottomInsetofSafeArea()
    var tap = UITapGestureRecognizer()
    
    open func setScanView() {
        ratioHeight = self.frame.height * ratio
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let inputDevice:AVCaptureDeviceInput!
        do {
            inputDevice =  try AVCaptureDeviceInput(device: captureDevice)  //AVCaptureDeviceInput(device: captureDevice, error: &error)
            if let inp = inputDevice {
                session.addInput(inp)
            } else {
                print("error")
            }
        } catch {
            print("error")
        }
        
        addPreviewLayer()
        
        /* Check for metadata */
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        output.metadataObjectTypes = output.availableMetadataObjectTypes
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        session.startRunning()
        
        /*------------- Camera Layer -------------*/
        let scannerLayer = UIImageView(frame: self.frame)
        scannerLayer.image = UIImage(named: "scanner")
        self.addSubview(scannerLayer)
        
        /*------------- Back Button ---------------*/
        let dismissButton = UIButton()
        dismissButton.frame = CGRect(x: self.frame.width - 50, y: 20 + Singleton.sharedInstance.getTopInsetofSafeArea(), width: 50, height: 50)
        dismissButton.setImage(UIImage(named: "close"), for: UIControlState())
        dismissButton.addTarget(self, action: #selector(ScanView.dismissScanView), for: UIControlEvents.touchUpInside)
        self.addSubview(dismissButton)
        
        /*----------- Scanning line -----------*/
        self.scanningLine.frame = CGRect(x: 50, y: 0, width: self.frame.width - 100, height: 2)
        self.scanningLine.center = CGPoint(x: self.center.x, y: self.center.y - self.ratioHeight)
        self.scanningLine.image = UIImage(named: "scanner_line")
        self.addSubview(self.scanningLine)
        
        UIView.animate(withDuration: 1.5, delay:0, options: [.repeat, .autoreverse], animations: {
            self.scanningLine.frame = CGRect(x: self.scanningLine.frame.origin.x, y: self.scanningLine.frame.origin.y + self.ratioHeight * 2, width: self.scanningLine.frame.width, height: self.scanningLine.frame.height)
            }, completion: nil)
    }
    
    func setManualScanField() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.manualTextView.frame = CGRect(x: 0, y: self.ypos, width: SCREEN_SIZE.width - self.heightOfTextView , height: self.heightOfTextView )
        self.manualTextView.backgroundColor = COLOR.imagePreviewBackgroundColor
        self.manualTextView.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.manualTextView.textColor = UIColor.white
        self.manualTextView.textContainerInset = UIEdgeInsetsMake(17, 10, 10, self.heightOfTextView - 2)
        self.manualTextView.delegate = self

        self.placeHolderLabel.frame = CGRect(x: 13, y: 0, width: self.manualTextView.frame.width, height: self.heightOfTextView)
        self.placeHolderLabel.text = TEXT.ENTER_MANUALLY_HERE
        self.placeHolderLabel.textColor = UIColor.lightGray
        self.placeHolderLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        
        self.doneButton.frame = CGRect(x: SCREEN_SIZE.width - 50.0, y: self.ypos, width: self.heightOfTextView , height: self.heightOfTextView)
        self.doneButton.setTitle(TEXT.DONE, for: .normal)
        self.doneButton.titleLabel?.textColor = UIColor.white
        self.doneButton.backgroundColor = COLOR.imagePreviewBackgroundColor
        self.doneButton.addTarget(self, action: #selector(self.doneAction), for: .touchUpInside)
        self.doneButton.isEnabled = true
        
        self.manualTextView.addSubview(self.placeHolderLabel)
        self.addSubview(self.manualTextView)
        self.addSubview(self.doneButton)
    }
    
    func doneAction() {
        guard self.manualTextView.text.trimText != "" else {
            return
        }
        self.endEditing(true)
        self.delegate.decodedOutput(self.manualTextView.text.trimText)
        self.dismissScanView()
    }
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    //MARK: Keyboard Observer
    func keyboardWillShow(_ notification : Foundation.Notification){
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = value.cgRectValue.size
        self.manualTextView.frame.origin.y = self.ypos - keyboardSize.height
        self.doneButton.frame.origin.y = self.ypos - keyboardSize.height
        self.hideKeyboardWhenTappedAround()
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        self.manualTextView.frame.origin.y = self.ypos
        self.doneButton.frame.origin.y = self.ypos
        self.tap.removeTarget(self, action: #selector(self.dismissKeyboard))
    }
    
    func hideKeyboardWhenTappedAround() {
        self.tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.tap.delegate = self
        self.addGestureRecognizer(self.tap)
    }
    
    /* Add the preview layer here */
    func addPreviewLayer() {
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer?.bounds = self.bounds
        previewLayer?.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        self.layer.addSublayer(previewLayer!)
    }
    
    open func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        self.endEditing(true)
        for data in metadataObjects {
            let metaData = data as! AVMetadataObject
            let transformed = previewLayer?.transformedMetadataObject(for: metaData) as? AVMetadataMachineReadableCodeObject
            if let unwraped = transformed {
                if let decodeValue = unwraped.stringValue {
                    delegate.decodedOutput(decodeValue)
                    self.scanningLine.layer.removeAllAnimations()
                    self.session.stopRunning()
                    let systemSoundID: SystemSoundID = 1114
                    AudioServicesPlaySystemSound (systemSoundID)
                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                    Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ScanView.dismissScanView), userInfo: nil, repeats: false)
                    return
                }
            }
        }
    }
    
    func dismissScanView() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        session.stopRunning()
        self.removeFromSuperview()
        delegate.dismissScannerView()
    }
}

//MARK: Functions of UITextViewDelegate
extension ScanView : UITextViewDelegate {
    public func textViewDidBeginEditing(_ textView: UITextView) {
        print("yup")
    }
    
    @available(iOS 10.0, *)
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("characterRange")
        return true
    }
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        //self.manualTextView.becomeFirstResponder()
        return true
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        if textView.text.length > 0{
            self.placeHolderLabel.isHidden = true
        } else {
            self.placeHolderLabel.isHidden = false
        }
    }
}

//MARK: Functions of UIGestureRecognizerDelegate
extension ScanView: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view != nil && touch.view!.isDescendant(of: self.doneButton)){
            return false
        }
        return true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,shouldRecognizeSimultaneouslyWith otherGestureRecognizer : UIGestureRecognizer)->Bool{
        return true
    }
}
