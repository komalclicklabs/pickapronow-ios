//
//  UserDetailCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 23/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class UserDetailCell: UITableViewCell {

    @IBOutlet var bottomPattern: UIView!
    @IBOutlet var dot: UIView!
    @IBOutlet var topPattern: UIView!
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var line: UIView!
    @IBOutlet var messageButton: UIButton!
    @IBOutlet var trainlingConstraint: NSLayoutConstraint!
    let trailingConstraintWithBothButtons:CGFloat = 55.0
    let trailingConstraintWithSingleButton:CGFloat = 10.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*=================== detail Label ===================*/
        self.detailLabel.textColor = COLOR.TEXT_COLOR
        
        /*=================== Line ==================*/
        self.line.backgroundColor = COLOR.LINE_COLOR
        
        self.dot.isHidden = true
        self.topPattern.isHidden = true
        self.bottomPattern.isHidden = true
        
        dot.backgroundColor = UIColor.black
        self.dot.layer.cornerRadius = 5.0
        topPattern.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "iconPattern"))
        bottomPattern.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "iconPattern"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
