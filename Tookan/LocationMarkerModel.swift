//
//  LocationMarkerModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 07/03/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class LocationMarkerModel: NSObject {
    var location:CLLocationCoordinate2D = CLLocationCoordinate2D()
    var locationMarker: GMSMarker = GMSMarker()
    var jobStatus:Int = 0
    var section = 0
    var row = 0
}
