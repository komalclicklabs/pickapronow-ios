//
//  PendingImages+CoreDataProperties.swift
//  
//
//  Created by CL-macmini45 on 5/25/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PendingImages {

    @NSManaged var job_id: NSNumber?
    @NSManaged var custom_label: String?
    @NSManaged var image: String?
    @NSManaged var type: NSNumber?
    @NSManaged var caption: String?
}
