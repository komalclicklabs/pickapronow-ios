//
//  AvailabilityDay.swift
//  Tookan
//
//  Created by cl-macmini-45 on 17/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class AvailabilityDay: NSObject {
    var dayId:Int? = 0
    var dateTime = String()
    var isDayBlocked:Int? = 0
    var lastStatusOfDayBlocked:Int? = 0
    var slots = Array<AvailabilitySlot>()
    
    required init(coder aDecoder: NSCoder) {
        dayId = aDecoder.decodeObject(forKey: "dayId") as? Int
        isDayBlocked = aDecoder.decodeObject(forKey: "isDayBlocked") as? Int
        lastStatusOfDayBlocked = aDecoder.decodeObject(forKey: "lastStatusOfDayBlocked") as? Int
        dateTime = aDecoder.decodeObject(forKey: "dateTime") as! String
        slots = (aDecoder.decodeObject(forKey: "slots") as? Array<AvailabilitySlot>)!
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(dayId, forKey: "dayId")
        aCoder.encode(dateTime, forKey: "dateTime")
        aCoder.encode(slots, forKey: "slots")
        aCoder.encode(isDayBlocked,forKey: "isDayBlocked")
        aCoder.encode(isDayBlocked,forKey: "lastStatusOfDayBlocked")
    }
    
    
    init(json: [String:Any]) {
        if let id = json["day_id"] as? Int {
            self.dayId = id
        } else {
            self.dayId = 0
        }
        
        if let blocked = json["is_day_blocked"] as? Int {
            self.isDayBlocked = blocked
            self.lastStatusOfDayBlocked = blocked
        }
        
        if let date = json["date_time"] as? String {
            self.dateTime = date
        }
        
        if let items = json["slots"] as? [Any]{
            self.slots = [AvailabilitySlot]()
            for item in items{
                if let slot = AvailabilitySlot(json: item as! [String:Any]) as AvailabilitySlot! {
                    slots.append(slot)
                }
            }
        }
    }

}
