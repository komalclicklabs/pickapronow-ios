//
//  SplashViewController.swift
//  Tookan
//
//  Created by Click Labs on 8/18/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit
import CoreLocation
import SDKDemo1

class SplashViewController: UIViewController {
    
    @IBOutlet var tagLine: UILabel!
    @IBOutlet var logo: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var location: CLLocation!
    var flag = true

    var alertView: UIAlertView!
    var accessToken: String!
    let date = Date()
    var getLocationTimer:Timer!
    var isUserLocationGet = false
    let userInfoObjectContext = DatabaseManager.sharedInstance.managedObjectContext
    var userInfoFromDatabase:FleetInfoDetails!
    var locationAlert:UIAlertController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationTracker.sharedInstance().restartLocationUpdates()
        self.view.backgroundColor = COLOR.themeBackgroundColor
        /*================= Tag Line =====================*/
        self.tagLine.textColor = COLOR.TEXT_COLOR
        self.tagLine.text = TAG_LINE
        self.tagLine.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)
        /*================= Status =====================*/
        statusLabel.textColor = COLOR.TEXT_COLOR
        statusLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        statusLabel.text = TEXT.GETTING_LOCATION

        self.setFestivalSplash()
        getLocationTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(SplashViewController.checkGetLocation), userInfo: nil, repeats: true)
    }
    override var preferredStatusBarStyle:UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.activityIndicator.startAnimating()
        
        if UIApplication.shared.backgroundRefreshStatus == UIBackgroundRefreshStatus.denied {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.DISABLE_BACKGROUND_APP_REFRESH, isError: .error)
        } else if UIApplication.shared.backgroundRefreshStatus == UIBackgroundRefreshStatus.restricted {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.DISABLE_BACKGROUND_APP_REFRESH, isError: .error)
        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.userLocationCoordinates(_:)), name: NSNotification.Name(rawValue: NotifRequestingResponse.currentLocationOfUser.rawValue) , object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.locationServiceDisabledAlert), name: NSNotification.Name(rawValue: NotifRequestingResponse.locationServicesDisabled.rawValue), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotifRequestingResponse.currentLocationOfUser.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotifRequestingResponse.locationServicesDisabled.rawValue), object: nil)
        if self.locationAlert != nil {
            self.locationAlert.dismiss(animated: true, completion: { finished in
                self.locationAlert = nil
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setFestivalSplash() {
        /*================= Festival Splash =====================*/
        switch Auxillary.getOnlyLocalDate() {
        case FESTIVE_SEASON.DIWALI:
            COLOR.navigationBackgroundColor = COLOR.diwaliNavigationColor
            COLOR.lowerNavigationBackgroundColor = COLOR.diwaliLowerBarColor
            self.logo.isHidden = true
            break
        case FESTIVE_SEASON.HALLOWEEN:
            COLOR.navigationBackgroundColor = COLOR.halloweenNavigationBackgroundColor
            COLOR.lowerNavigationBackgroundColor = COLOR.halloweenLowerBarColor
            self.logo.isHidden = true
            break
        default:
            self.logo.isHidden = false
            break
        }
        /*========================================================*/
    }
    
    //APP Update Popup
    func updateForceballyWithPopUpDict(_ popUpDict : NSDictionary){
        var titleForPopUp = String()
        var updateTitleForPopUp = ""
        if let title = popUpDict["title"] as? String{
            titleForPopUp = title
        }
        if let updateTitle = popUpDict["text"] as? String{
            updateTitleForPopUp = updateTitle
        }
        let alert = UIAlertController(title: titleForPopUp, message: updateTitleForPopUp, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default){
            UIAlertAction in
            let openUrl = popUpDict["app_url"] as! String
            UIApplication.shared.openURL(URL(string: openUrl)!)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateWithPopUpDict(_ popUpDict: NSDictionary, loginResponse: FleetInfoDetails){
        var titleForPopUp = String()
        var updateTitleForPopUp = ""
        if let title = popUpDict["title"] as? String{
            titleForPopUp = title
        }
        if let updateTitle = popUpDict["text"] as? String{
            updateTitleForPopUp = updateTitle
        }
        let alert = UIAlertController(title: titleForPopUp, message: updateTitleForPopUp, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default){
            UIAlertAction in
            let openUrl = popUpDict["app_url"] as! String
            UIApplication.shared.openURL(URL(string: openUrl)!)
        }
        let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.destructive){
            UIAlertAction in
            self.setValuesForNextController(loginResponse)
            DatabaseManager.sharedInstance.saveUserInfoInDatabase(loginResponse, syncStatus: 1, userManagedContext: self.userInfoObjectContext)
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
         self.present(alert, animated: true, completion: nil)
    }
    
    func setValuesForNextController(_ loginResponse: FleetInfoDetails) {
        accessToken = loginResponse.accessToken
        Singleton.sharedInstance.fleetDetails = loginResponse
        Singleton.sharedInstance.availabilityEditStatus = loginResponse.calendarEditStatus!
        Singleton.sharedInstance.availabilityViewStatus = loginResponse.calendarViewStatus!
        UserDefaults.standard.set(accessToken, forKey: "accessToken")
        UserDefaults.standard.set(loginResponse.batteryUsage, forKey: USER_DEFAULT.batteryUsage)
        if let teams = loginResponse.teams {
            UserDefaults.standard.set(teams.teamId, forKey: USER_DEFAULT.teamId)
        }
        
        NetworkingHelper.sharedInstance.currentDate = Date().formattedWith("yyyy-MM-dd")
        
        switch Auxillary.getOnlyLocalDate() {
        case FESTIVE_SEASON.DIWALI:
            self.perform(#selector(self.gotoNextController), with: nil, afterDelay: 2.0)
            break
        case FESTIVE_SEASON.HALLOWEEN:
            self.perform(#selector(self.gotoNextController), with: nil, afterDelay: 2.0)
            break
        default:
            self.gotoNextController()
            break
        }
    }
    
    func gotoNextController() {
        LocationTracker.sharedInstance().stopLocationTracking()
        let driverState = DRIVER_STATE(rawValue: Singleton.sharedInstance.fleetDetails.registrationStatus!)
        switch driverState! {
        case DRIVER_STATE.acknowledged:
            Singleton.sharedInstance.gotoHomeStoryboard()
        case DRIVER_STATE.otp_pending:
            self.gotoOTPController()
        default:
            self.gotoVerificationStateController()
            break
        }
        
    }
    
    func gotoOTPController() {
        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.otpController) as! OTPController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoVerificationStateController() {
        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.verificationStateController) as! VerificationStateController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func locationServiceDisabledAlert(){
        if(locationAlert == nil) {
            locationAlert = UIAlertController(
                title: ERROR_MESSAGE.BACKGROUND_LOCATION_DISABLED,
                message:ERROR_MESSAGE.LOCATION_ACCESS_PATH,
                preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: .cancel, handler: { (action) in
                self.locationAlert = nil
            })
            locationAlert.addAction(cancelAction)
        
            let openAction = UIAlertAction(title: TEXT.OPEN_SETTINGS, style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                    self.locationAlert = nil
                }
            }
            locationAlert.addAction(openAction)
            self.present(locationAlert, animated: true, completion: nil)
        }
    }
    
    // MARK: - LocationObserver
    func checkGetLocation() {
        if(isUserLocationGet == false) {
            if CLLocationManager.locationServicesEnabled() == false {
                self.locationServiceDisabledAlert()
                activityIndicator.stopAnimating()
                statusLabel.text = /*ERROR_MESSAGE.LOCATION_SERVICE_DISABLED +*/ "\(ERROR_MESSAGE.LOCATION_SERVICE_DISABLED)\n\(ERROR_MESSAGE.ALL_LOCATION_DISABLED)"// + ERROR_MESSAGE.ALL_LOCATION_DISABLED
            } else {
                let authorizationStatus = CLLocationManager.authorizationStatus()
                if authorizationStatus == CLAuthorizationStatus.denied || authorizationStatus == CLAuthorizationStatus.restricted {
                    statusLabel.text = /*ERROR_MESSAGE.BACKGROUND_LOCATION_DISABLED + */"\(ERROR_MESSAGE.BACKGROUND_LOCATION_DISABLED)\n\(ERROR_MESSAGE.LOCATION_ACCESS_PATH)"// + ERROR_MESSAGE.LOCATION_ACCESS_PATH
                    activityIndicator.stopAnimating()
                    self.locationServiceDisabledAlert()
                } else {
                    statusLabel.text =  TEXT.GETTING_LOCATION
                    activityIndicator.startAnimating()
                    LocationTracker.sharedInstance().startLocationTracking()
                }
            }
        }
    }
    
    
    func userLocationCoordinates(_ notification : Foundation.Notification){
        isUserLocationGet = true
        if(self.getLocationTimer != nil) {
            self.getLocationTimer.invalidate()
            self.getLocationTimer = nil
        }
        if IJReachability.isConnectedToNetwork() {
            self.loginUser(notification)
        } else {
            self.userInfoFromDatabase = DatabaseManager.sharedInstance.fetchUserInfoFromDatabase(self.userInfoObjectContext)
            if(Auxillary.isAppSyncingEnable() == false || self.userInfoFromDatabase == nil) {
                activityIndicator.stopAnimating()
                statusLabel.text = ERROR_MESSAGE.NO_INTERNET_CONNECTION_SHORT
                let alertController = UIAlertController(title: TEXT.CONNECTION_ERROR, message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, preferredStyle: UIAlertControllerStyle.alert)
                let alertAction = UIAlertAction(title: TEXT.TRY_AGAIN, style: UIAlertActionStyle.default, handler: { (finished) -> Void in
                    if IJReachability.isConnectedToNetwork() == false {
                        self.present(alertController, animated: true, completion: nil)
                    } else {
                        self.dismiss(animated: true, completion: nil)
                        self.loginUser(notification)
                    }
                })
                alertController.addAction(alertAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                self.setValuesForNextController(self.userInfoFromDatabase)
                DatabaseManager.sharedInstance.saveUserInfoInDatabase(self.userInfoFromDatabase, syncStatus: 0, userManagedContext: self.userInfoObjectContext)
            }
        }
    }
    
    fileprivate func loginUser(_ notification : Foundation.Notification) {
        activityIndicator.startAnimating()
        statusLabel.text = TEXT.CONNECTING_SERVER
        if let response = (notification as NSNotification).userInfo, let newlocation = response["userLocationCoordinate"] as? CLLocation {
            location = newlocation
            if flag {
                if Singleton.sharedInstance.isWhiteLabelUserWithDeviceTokenCheck() == true {
                    Singleton.sharedInstance.isNotificationEnabled(completion: { (isEnabled) in
                        if isEnabled == false {
                            let alert = UIAlertController(title: "Error", message: TEXT.NOTIFICATIONS_DISABLE, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK".localized, style: UIAlertActionStyle.default){
                                UIAlertAction in
                                
                                Singleton.sharedInstance.isNotificationEnabled(completion: { (enable) in
                                    if enable == true {
                                        self.loginUser(notification)
                                    } else {
                                        if let url = URL(string:UIApplicationOpenSettingsURLString) {
                                            UIApplication.shared.openURL(url)
                                        }
                                    }
                                })
                                
                                
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                            return
                        }
                    })
                }
                let deviceToken =  UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) != nil ? UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) as? String : "No deviceToken"

                if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as? String {
                    flag = false
                    var param:[String:Any] = ["device_type": DEVICE_TYPE]
                    param["access_token"] = accessToken
                    param["device_token"] = "\(deviceToken!)"
                    param["latitude"] = "\(location.coordinate.latitude)"
                    param["longitude"] = "\(location.coordinate.longitude)"
                    param["device_os"] = "iOS"
                    param["store_version"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
                    param["device_desc"] = "\(DeviceInfo.oSVersion) \(DeviceInfo.deviceType) \(DeviceInfo.deviceName as String)"// + DeviceInfo.deviceType + " " + (DeviceInfo.deviceName as String)
                    param["app_version"] = app_version
                    param["bat_lvl"] = "\(UIDevice.current.batteryLevel * 100)"
//                    let param = [
//                        "access_token"      : accessToken,
//                        "device_type"       : DEVICE_TYPE,
//                        "device_token"      : "\(deviceToken!)",
//                        "latitude"          : "\(location.coordinate.latitude)",
//                        "longitude"         : "\(location.coordinate.longitude)",
//                        "device_os"         : "iOS",
//                        "store_version"     : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String,
//                        "device_desc"       : "\(DeviceInfo.oSVersion) " + DeviceInfo.deviceType + " " + (DeviceInfo.deviceName as String),
//                        "app_version"       : app_version,
//                        "bat_lvl" : "\(UIDevice.current.batteryLevel * 100)"
//                    ]
                    NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.access_token_login, params: param as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                        DispatchQueue.main.async(execute: { () -> Void in
                            if isSucceeded == true {
                                print(response)
                                var loginResponse: FleetInfoDetails!
                                self.activityIndicator.stopAnimating()
                                self.statusLabel.text = ""
                                if let responseData = response["data"] as? [String:AnyObject] {
                                    switch(response["status"] as! Int) {
                                    case STATUS_CODES.SHOW_DATA:
                                        if let items = responseData["fleet_info"] as? NSArray {
                                            for item in items {
                                                if let fleetInfo = item as? NSDictionary{
                                                    loginResponse = FleetInfoDetails(json: fleetInfo)
                                                    
                                                }
                                            }
                                        }
                                        
                                        if let notificationCenterInfo = responseData["notification_center"] {
                                            let notificationCenter:[String:AnyObject] = notificationCenterInfo as! [String : AnyObject]
                                            if let count = notificationCenter["count"] as? Int {
                                                NetworkingHelper.sharedInstance.notificationCount = count
                                            }
                                            
                                            if let serverTime = notificationCenter["server_time"] as? String {
                                                NetworkingHelper.sharedInstance.serverTime = serverTime
                                            }
                                            
                                            if let data = notificationCenter["data"] as? NSArray {
                                                for item in data {
                                                    NetworkingHelper.sharedInstance.notificationArray.append(NotificationDataObject(json: item as! [String:Any]))
                                                }
                                            }
                                            
                                            //NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotificationCount), object: self)
                                        }
                                        
                                        if let popUp = responseData["popup"] as? NSDictionary{
                                            if popUp.count > 0{
                                                if let is_force = popUp["is_force"] as? String{
                                                    if is_force == "1" {
                                                        // Code here for popUp
                                                        self.updateForceballyWithPopUpDict(popUp)
                                                        return
                                                    }else{
                                                        self.updateWithPopUpDict(popUp,loginResponse: loginResponse)
                                                        return
                                                    }
                                                }
                                                
                                                if let is_force = popUp["is_force"] as? Int{
                                                    if is_force == 1{
                                                        self.updateForceballyWithPopUpDict(popUp)
                                                        return
                                                    }else{
                                                        self.updateWithPopUpDict(popUp, loginResponse: loginResponse)
                                                        return
                                                    }
                                                }
                                            }
                                        }
                                        
                                        self.setValuesForNextController(loginResponse)
                                        DatabaseManager.sharedInstance.saveUserInfoInDatabase(loginResponse, syncStatus: 1, userManagedContext: self.userInfoObjectContext)
                                        break
                                        
                                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                        NotificationCenter.default.removeObserver(self)
                                        Auxillary.logoutFromDevice()
                                        self.flag = true
                                        let alert = UIAlertController(title: "", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                                        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                                            DispatchQueue.main.async(execute: { () -> Void in
                                                let loginViewContoller  = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.signinViewController) as! SigninViewController
                                                self.navigationController?.pushViewController(loginViewContoller, animated: true)
                                            })
                                        })
                                        alert.addAction(actionPickup)
                                        self.present(alert, animated: true, completion: nil)
                                        break
                                    default:
                                        self.flag = true
                                        Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                                        break
                                    }
                                }
                            } else {
                                let message = (response["message"] as? String)!
                                self.activityIndicator.stopAnimating()
                                self.statusLabel.text = message
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                let action = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                                    self.loginUser(notification)
                                })
                                alert.addAction(action)
                                self.present(alert, animated: true, completion: nil)
                                self.flag = true
                            }
                        
                        })
                    })
                } else {
                    let loginViewContoller  = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.signinViewController) as! SigninViewController
                    self.navigationController?.pushViewController(loginViewContoller, animated: true)
                    flag = true
                }
            }
        }
    }
}
