//
//  AvailabilityView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 13/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

protocol AvailabilityDelegate {
    func selectedDay(selectedDate:String,availabilityStatus:Bool,weekday:Int)
    func backAction()
}

class AvailabilityView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var backButton: UIButton!
    @IBOutlet var availabilityTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var availabilityWidthConstraint: NSLayoutConstraint!
    @IBOutlet var availabilityScrollView: UIScrollView!
    @IBOutlet var timeCollectionView: UICollectionView!
    @IBOutlet var dayCollectionView: UICollectionView!
    @IBOutlet var availabilityCollectionView: UICollectionView!
    let model = AvailabilityModel()
    var maximumDays = 7
    var maximumTimeSlots = 24
    var dayCellWidth:CGFloat = 52
    var dayCellHeight:CGFloat = 56
    var timeCellWidth:CGFloat = 60
    var timeCellHeight:CGFloat = 55
    var availabilityCellWidth:CGFloat = 52
    var availabilityCellHeight:CGFloat = 55
    var currentScrollingView = 0
    var numberOfDots = 4
    var delegate:AvailabilityDelegate!
    
    struct MonthColor {
        static let availableColor = UIColor.white
        static let unAvailableColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
    } 
    
    struct SCROLL_VIEW {
        static let dayScrollView = 1
        static let timeScrollView = 2
        static let availabilityScrollView = 3
    }
    
    struct CURRENT_SCROLLING {
        static let day = 1
        static let time = 2
        static let availability = 3
        static let scroll = 4
    }
    
    
    override func awakeFromNib() {
        
    }
    
    func setTableView() {
        dayCollectionView.register(UINib(nibName: NIB_NAME.dayCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.dayCollectionCell)
        dayCollectionView.delegate = self
        dayCollectionView.dataSource = self
        dayCollectionView.tag = 1
        
        timeCollectionView.register(UINib(nibName: NIB_NAME.timeCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.timeCollectionCell)
        timeCollectionView.delegate = self
        timeCollectionView.dataSource = self
        timeCollectionView.tag = 2
        
        availabilityCollectionView.register(UINib(nibName: NIB_NAME.availabilityDotCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.availabilityDotCell)
        availabilityCollectionView.delegate = self
        availabilityCollectionView.dataSource = self
        availabilityCollectionView.tag = 3
        
        self.availabilityScrollView.delegate = self
//        if Singleton.sharedInstance.fleetDetails != nil {
//            self.maximumDays = Singleton.sharedInstance.fleetDetails.availability_days_limit!
//        }
    }
    
    
    func setDayCollectionView() {
       availabilityWidthConstraint.constant = availabilityCellWidth * CGFloat(maximumDays)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.delegate.backAction()
    }
    
    
    //MARK: UICollection View Delegate...
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case dayCollectionView:
           return maximumDays
        case timeCollectionView:
            return maximumTimeSlots
        default:
            return maximumDays * maximumTimeSlots
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case dayCollectionView:
            return CGSize(width: dayCellWidth, height: dayCellHeight)
        case timeCollectionView:
            return CGSize(width: timeCellWidth, height: timeCellHeight)
        default:
            return CGSize(width: availabilityCellWidth, height: availabilityCellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case dayCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.dayCollectionCell, for: indexPath) as! DayCollectionCell
            cell.dayLabel.text = model.getDateSlot(indexPath: indexPath.item)
            if model.isGivenDayBlocked(day: indexPath.item) == true {
                cell.dayLabel.textColor = MonthColor.unAvailableColor
            } else {
                cell.dayLabel.textColor = MonthColor.availableColor
            }
            return cell
        case timeCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.timeCollectionCell, for: indexPath) as! TimeCollectionCell
            cell.timeLabel.text = model.getTimeSlot(indexPath: indexPath.item, totalTimeSlots: maximumTimeSlots)
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.availabilityDotCell, for: indexPath) as! AvailabilityDotCell
            for i in 0..<cell.numberOfDots {
                let currentNodeStatus = model.getAvailabilityStatus(indexPath: indexPath.item, maximumDays: Singleton.sharedInstance.availabilityData.count, numberOfDots:cell.numberOfDots, row:i)
                let currentNodeColor = model.currentStatusColor(currentNodeStatus: currentNodeStatus)
                cell.dotCollection[i].backgroundColor = currentNodeColor
            }
            
//            if currentNodeStatus == AVAILABILITY.busy {
//                if model.isTopMarginViewEligibleForCombineView(indexPath: indexPath.item, maximumDays: Singleton.sharedInstance.availabilityData.count, currentNodeStatus: currentNodeStatus) == true {
//                    cell.topMarginView.isHidden = false
//                    cell.topMarginView.backgroundColor = currentNodeColor
//                } else {
//                    cell.topMarginView.isHidden = true
//                }
//                
//                if model.isBottomMarginViewEligibleForCombineView(indexPath: indexPath.item, maximumDays: Singleton.sharedInstance.availabilityData.count, currentNodeStatus: currentNodeStatus, maximumSlots: self.maximumTimeSlots) {
//                    cell.bottomMarginView.isHidden = false
//                    cell.bottomMarginView.backgroundColor = currentNodeColor
//                } else {
//                    cell.bottomMarginView.isHidden = true
//                }
//                if cell.topMarginView.isHidden == true {
//                    cell.lock.isHidden = false
//                }
//            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        guard Singleton.sharedInstance.availabilityEditStatus == 1 else {
            return 
        }
        
        switch collectionView {
        case availabilityCollectionView:
            let currentNodeStatus = model.getAvailabilityStatus(indexPath: indexPath.item, maximumDays: self.maximumDays, numberOfDots:self.numberOfDots, row:0)
            if currentNodeStatus != AVAILABILITY.busy {
                let day = indexPath.item % maximumDays
                delegate.selectedDay(selectedDate: model.getDateSlotForEditSchedule(indexPath: day),availabilityStatus:!model.isGivenDayBlocked(day: day),weekday:day)
            }
            break
        case dayCollectionView:
            delegate.selectedDay(selectedDate: model.getDateSlotForEditSchedule(indexPath: indexPath.item),availabilityStatus:!model.isGivenDayBlocked(day: indexPath.item),weekday:indexPath.item)
        break
        default:
            break
            
        }
    }
    
    //MARK: SCROLL VIEW DELEGATE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch currentScrollingView {
        case CURRENT_SCROLLING.scroll:
            self.dayCollectionView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: self.dayCollectionView.contentOffset.y) , animated: false)
            break
        case CURRENT_SCROLLING.day:
            self.availabilityScrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: self.availabilityScrollView.contentOffset.y) , animated: false)
            break
        case CURRENT_SCROLLING.time:
            self.availabilityCollectionView.setContentOffset(CGPoint(x: self.availabilityCollectionView.contentOffset.x, y: scrollView.contentOffset.y) , animated: false)
            break
        case CURRENT_SCROLLING.availability:
            self.timeCollectionView.setContentOffset(CGPoint(x: self.timeCollectionView.contentOffset.x, y: scrollView.contentOffset.y) , animated: false)
            break
        default:
            break
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.resetCurrentScrollView(scrollView: scrollView)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.resetCurrentScrollView(scrollView: scrollView)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.setCurrentScrollView(scrollView: scrollView)
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        self.setCurrentScrollView(scrollView: scrollView)
    }
    
    func setCurrentScrollView(scrollView:UIScrollView) {
        switch scrollView {
        case availabilityScrollView:
            self.currentScrollingView = CURRENT_SCROLLING.scroll
            break
        default:
            switch scrollView.tag {
            case SCROLL_VIEW.dayScrollView:
                self.currentScrollingView = CURRENT_SCROLLING.day
                self.timeCollectionView.isScrollEnabled = false
                self.availabilityScrollView.isScrollEnabled = false
                self.availabilityCollectionView.isScrollEnabled = false
                break
            case SCROLL_VIEW.timeScrollView:
                self.currentScrollingView = CURRENT_SCROLLING.time
                self.dayCollectionView.isScrollEnabled = false
                self.availabilityScrollView.isScrollEnabled = false
                break
            case SCROLL_VIEW.availabilityScrollView:
                self.currentScrollingView = CURRENT_SCROLLING.availability
                self.dayCollectionView.isScrollEnabled = false
                break
            default:
                break
            }
            break
        }
    }
    
    func resetCurrentScrollView(scrollView:UIScrollView) {
        switch scrollView {
        case availabilityScrollView:
            break
        default:
            switch scrollView.tag {
            case SCROLL_VIEW.dayScrollView:
                self.timeCollectionView.isScrollEnabled = true
                self.availabilityScrollView.isScrollEnabled = true
                self.availabilityCollectionView.isScrollEnabled = true
                break
            case SCROLL_VIEW.timeScrollView:
                self.dayCollectionView.isScrollEnabled = true
                self.availabilityScrollView.isScrollEnabled = true
                break
            case SCROLL_VIEW.availabilityScrollView:
                self.dayCollectionView.isScrollEnabled = true
                break
            default:
                break
            }
            break
        }
    }

}
