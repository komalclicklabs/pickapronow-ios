//
//  UnavailableMarkView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
protocol UnavailableDelegate {
    func tappedOnUnavailableView()
}

class UnavailableMarkView: UIView {

    var delegate:UnavailableDelegate!
    @IBOutlet var topLabel: UILabel!
    @IBOutlet var bottomLabel: UILabel!
    @IBOutlet var topArrow: UIImageView!
    
    override func awakeFromNib() {
        topLabel.text = "Mark yourself as available to update your diary".localized
        let firstLine = "You have marked yourself unavailable for this day.".localized
        let secondLine = "Please confirm below".localized
        bottomLabel.text = "\(firstLine)\n\(secondLine)"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    func backgroundTouch() {
        delegate.tappedOnUnavailableView()
    }
}
