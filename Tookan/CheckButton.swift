//
//  CheckButton.swift
//  Tookan
//
//  Created by cl-macmini-45 on 22/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit


class CheckButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(ovalIn: rect)
        COLOR.SUCCESSFUL_VERIFICATION_COLOR.setFill()
        path.fill()
        let plusHeight: CGFloat = 7.0
        let plusWidth: CGFloat = min(bounds.width, bounds.height) * 0.5
        let plusPath = UIBezierPath()
        plusPath.lineWidth = plusHeight
        
        plusPath.move(to: CGPoint(
            x:bounds.width/2 - plusWidth/2 + 0.5,
            y:bounds.height/2 + 0.5))
        
        //add a point to the path at the end of the stroke
        plusPath.addLine(to: CGPoint(
            x:bounds.width/2 - plusWidth/5 + 0.5,
            y:bounds.height/2 + 15.0 + 0.5))
        plusPath.addLine(to: CGPoint(
            x:bounds.width/2 + plusWidth/2 + 0.5,
            y:bounds.height/2 - 13.0 + 0.5))
        
        UIColor.white.setStroke()
        plusPath.stroke()
    }

}
