//
//  CompletedTaskEarningController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 22/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class CompletedTaskEarningController: UIViewController {

    @IBOutlet var backround: UIView!
    @IBOutlet var checkButton: CheckButton!
    @IBOutlet var completedLabel: UILabel!
    @IBOutlet var distanceView: UIView!
    @IBOutlet var durationView: UIView!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var earningsDetailsButton: UIButton!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet weak var centreView: UIView!
    @IBOutlet weak var centreLabel: UILabel!
    @IBOutlet weak var ratingView: StarView!
    @IBOutlet weak var pricingDetailsButton: UIButton!
//    @IBOutlet weak var earningingDetailTrailingConstraint: NSLayoutConstraint!
    //@IBOutlet var distanceViewBottomConstraint: NSLayoutConstraint!
    
    //@IBOutlet var bottomConstraintForEarningDetails: NSLayoutConstraint!
    let defaultConstraintValueForBottomConstraint:CGFloat = 140.0
    var jobId:String!
    var totalDistance:String!
    var totalDuration:String!
    var earningsFormulaFields = [FORMULA_FIELDS]()
    var pricingFormulaFields = [FORMULA_FIELDS]()
    var dismissCallback:(() -> Void)?
    var completedTask = false
    var finalRating : Float = 0.0
    var filledStars = 0
    var isRatingAvailable = true
    var isEarningsAvailable = true
    var isPricingAvailable = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.checkButton.setShadow()
        self.setView()
        self.setLabels()
        self.setButtons()
        self.setCenterView()
        self.setStarView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.perform(#selector(self.updateStatusBar), with: nil, afterDelay: 1.0)
        self.settingViewAsPerAddOn()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                UIApplication.shared.statusBarStyle = .lightContent
            } else {
                UIApplication.shared.statusBarStyle = .default
            }
        }
    }
    
    func setStarView() {
        self.ratingView.delegate = self
        self.ratingView.strokeColor = COLOR.STAR_COLOR
        self.ratingView.fillColor = COLOR.STAR_COLOR
        self.ratingView.emptyColor = UIColor.white
        self.ratingView.totalStars = 5
        self.ratingView.filledStars = self.filledStars
        self.ratingView.lineWidth = 2.0
        self.ratingView.starSize = 37.0
        self.ratingView.starSpacings = 15.0
        self.ratingView.setStars()
        guard self.completedTask == false else {
            self.ratingView.isUserInteractionEnabled = false
            return
        }
    }
    
    func setCenterView() {
        self.centreView.backgroundColor = COLOR.LINE_COLOR
        self.centreLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.centreLabel.textColor = COLOR.LIGHT_COLOR
        self.centreLabel.textAlignment = .left
        self.centreLabel.text  = "\(TEXT.RATE_THE_CUSTOMER)"
        self.centreLabel.setLetterSpacing(value: 1.8)
    }
    
    func updateStatusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func setView() {
        self.backround.layer.cornerRadius = 15.0
        
        self.distanceView.layer.cornerRadius = 2.0
        self.distanceView.layer.borderWidth = 1.0
        self.distanceView.layer.borderColor = COLOR.LINE_COLOR.cgColor
        
        self.durationView.layer.cornerRadius = 2.0
        self.durationView.layer.borderWidth = 1.0
        self.durationView.layer.borderColor = COLOR.LINE_COLOR.cgColor
    }
    
    func setLabels() {
        self.completedLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.completedLabel.textColor = COLOR.LIGHT_COLOR
        self.completedLabel.text = "\(TEXT.COMPLETED_TASK) \(self.jobId!)"
        
        self.distanceLabel.attributedText = self.getAttributedTotalText(fieldLabel: TEXT.DISTANCE_SMALL, fieldValue: "\(totalDistance!)")
        self.durationLabel.attributedText = self.getAttributedTotalText(fieldLabel: TEXT.DURATION, fieldValue: "\(totalDuration!)")
    }
    
    func setButtons() {
        self.continueButton.titleLabel?.font = UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.regular)
        self.continueButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.continueButton.layer.cornerRadius = 2.0
        self.continueButton.backgroundColor = COLOR.themeForegroundColor
        self.continueButton.setTitle(TEXT.CONTINUE, for: .normal)
        self.continueButton.setShadow()
        
        self.earningsDetailsButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.earningsDetailsButton.setTitleColor(COLOR.LIGHT_COLOR, for: .normal)
        self.earningsDetailsButton.layer.cornerRadius = 2.0
        self.earningsDetailsButton.backgroundColor = UIColor.clear
        self.earningsDetailsButton.setTitle(TEXT.EARNING_DETAILS, for: .normal)
        self.earningsDetailsButton.titleLabel?.setLetterSpacing(value: 0.5)
        self.earningsDetailsButton.titleLabel?.numberOfLines = 0
        
        self.pricingDetailsButton.setTitle(TEXT.PRICING_DETAILS, for: .normal)
        self.pricingDetailsButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.pricingDetailsButton.setTitleColor(COLOR.LIGHT_COLOR, for: .normal)
        self.pricingDetailsButton.layer.cornerRadius = 2.0
        self.pricingDetailsButton.backgroundColor = UIColor.clear
        self.pricingDetailsButton.titleLabel?.setLetterSpacing(value: 0.5)
        self.pricingDetailsButton.titleLabel?.numberOfLines = 0
    }
    
    func settingViewAsPerAddOn() {
        if isRatingAvailable == false {
            self.ratingView.isHidden = true
            self.centreView.isHidden = true
            self.ratingView.isUserInteractionEnabled = false
//            if SCREEN_SIZE.height > 568.0 {
//                self.distanceViewBottomConstraint.constant = 100.0
//                self.bottomConstraintForEarningDetails.constant = 100.0
//            } else {
//                self.distanceViewBottomConstraint.constant = 80.0
//                self.bottomConstraintForEarningDetails.constant = 80.0
//            }
        }
        
        switch (isEarningsAvailable, isPricingAvailable) {
        case (false,false):
            self.earningsDetailsButton.isHidden = true
            self.pricingDetailsButton.isHidden = true
        case (true,false), (false,true):
            self.earningsDetailsButton.isHidden = !self.isEarningsAvailable
            self.pricingDetailsButton.isHidden = !self.isPricingAvailable
            self.earningsDetailsButton.isUserInteractionEnabled = self.isEarningsAvailable
            self.pricingDetailsButton.isUserInteractionEnabled = self.isPricingAvailable
//            self.earningingDetailTrailingConstraint.constant = -(SCREEN_SIZE.width - 70)
        default:
            break
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendRequestForRating() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        var params:[String : Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
        params["rating"] = self.finalRating
        params["comment"] = ""
        params["job_id"] = self.jobId
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.add_ratings, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                print(response)
                
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .success)
                        Singleton.sharedInstance.selectedTaskDetails.customerRating = Int(self.finalRating)
                        self.dismissViewController()
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    default:
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        })
    }
    
    func dismissViewController() {
        if self.completedTask == true {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: false, completion: { finished in
                self.dismissCallback!()
            })
        }
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                NotificationCenter.default.removeObserver(self)
                Auxillary.logoutFromDevice()
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    

    @IBAction func continueAction(_ sender: Any) {
        //self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        guard self.isRatingAvailable == false else {
            guard self.completedTask == false else {
                self.dismissViewController()
                return
            }
            self.sendRequestForRating()
            return
        }
        self.dismissViewController()
    }
    
    @IBAction func showEarningDetailsAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.earningDetailController) as! EarningDetailController
        controller.forCompletedTask = true
        controller.forEarningDetails = true
        controller.formulaFields = self.earningsFormulaFields
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func showPricingDetailsAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.earningDetailController) as! EarningDetailController
        controller.forCompletedTask = true
        controller.forEarningDetails = false
        controller.formulaFields = self.pricingFormulaFields
        self.present(controller, animated: true, completion: nil)
    }
    
    func getAttributedTotalText(fieldLabel:String,fieldValue:String) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        attributedString = NSMutableAttributedString(string: "\(fieldLabel)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)!])
        
        let sum = NSMutableAttributedString(string: "\n\(fieldValue)", attributes: [NSForegroundColorAttributeName:COLOR.themeForegroundColor, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.regular)!])
        attributedString.append(sum)
        
        return attributedString
    }
    
}

//MARK: Navigation Bar
extension CompletedTaskEarningController:NavigationDelegate {
    func setNavigationBar() {
        let navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = self.jobId!
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.view.addSubview(navigation)
    }
    
    func backAction() {
       self.dismiss(animated: true, completion: nil)
    }
    
    func editAction() {
        
    }
}

extension CompletedTaskEarningController:RatingDelegate {
    func finalRatings(rating: Int) {
        self.finalRating = Float(rating)
    }
}
