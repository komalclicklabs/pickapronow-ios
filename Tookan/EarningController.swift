//
//  EarningController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 19/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

class EarningController: UIViewController {

    @IBOutlet var earningTable: UITableView!
    var offset = 0
    var currentDate = ""
    var weekTasks = [[WEEK_JOBS]]()
    var weekReport = [WEEK_REPORT]()
    var weekTotal = ""
    var weekDistance = ""
    var weekDuration = ""
    let model = EarningModel()
    let availabilityModel = AvailabilityModel()
    let headerHeight:CGFloat = 353.0
    let footerHeight:CGFloat = 75.0
    var headerView:EarningHeaderView!
    var activityIndicator:DGActivityIndicatorView!
    var maxPriceValue:Float = 0
    let minSlotValue = 100
    var currentWeekDate:Date!
    var currentDateIndex = 0
    var numberOfDays = 7
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableView()
        self.setNavigationBar()
        self.perform(#selector(self.setEarningDetailsTableAndGraph), with: nil, afterDelay: 0.5)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.perform(#selector(self.updateStatusBar), with: nil, afterDelay: 1.0)
    }
    
    func setEarningDetailsTableAndGraph() {
        self.currentWeekDate = Date().startOfWeek
        self.currentDate = (self.currentWeekDate.formattedWith("yyyy-MM-dd"))
        self.currentWeekDate = self.currentDate.asDateFormattedWith("yyyy-MM-dd")
        if offset != -1 {
            self.getAllEarnings(startDate: self.currentDate)
        }
    }
    
    func updateStatusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTableView() {
        self.earningTable.delegate = self
        self.earningTable.dataSource = self
        self.earningTable.register(UINib(nibName: NIB_NAME.earningCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.earningCell)
        self.earningTable.rowHeight = UITableViewAutomaticDimension
        self.earningTable.layer.cornerRadius = 15.0
        self.earningTable.layer.masksToBounds = true
        self.setFooterView()
    }
    
    func setHeaderView() {
        if headerView == nil {
            headerView = UINib(nibName: NIB_NAME.earningHeaderView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as!  EarningHeaderView
            headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width - 10, height: headerHeight)
            self.earningTable.tableHeaderView = headerView
            self.setCollectionsView()
            self.headerView.leftButton.addTarget(self, action: #selector(self.leftButtonAction), for: UIControlEvents.touchUpInside)
            self.headerView.rightButton.addTarget(self, action: #selector(self.rightButtonAction), for: UIControlEvents.touchUpInside)

        }
        headerView.totalPayoutButton.setAttributedTitle(self.model.getAttributedPayout(totalPayout: self.weekTotal), for: .normal)
        self.headerView.barGraphCollectionView.reloadData()
        self.headerView.rupeesScaleCollectionView.reloadData()
    }
    
    func setCollectionsView() {
        self.headerView.barGraphCollectionView.delegate = self
        self.headerView.barGraphCollectionView.dataSource = self
        self.headerView.rupeesScaleCollectionView.delegate = self
        self.headerView.rupeesScaleCollectionView.dataSource = self
        
        // Do any additional setup after loading the view.
        self.headerView.barGraphCollectionView.register(UINib(nibName: NIB_NAME.barGraphCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.barGraphCell)
        self.headerView.rupeesScaleCollectionView.register(UINib(nibName: NIB_NAME.rupeesScaleCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.rupeesScaleCollectionCell)
        
//        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.minimumInteritemSpacing = 0
//        layout.minimumLineSpacing = 0
    }
    
    func setFooterView() {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: footerHeight)
        activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballClipRotate, tintColor: COLOR.themeForegroundColor, size: 30.0)
        activityIndicator?.frame = CGRect(x: 0, y: 10, width: 40, height: 40)
        activityIndicator?.center = CGPoint(x: self.view.center.x, y: (activityIndicator?.center.y)!)
        activityIndicator?.clipsToBounds = true
        view.addSubview(activityIndicator)
        self.earningTable.tableFooterView = view
    }
    
    func tableBackgroundView(_ frame:CGRect) -> UIView {
        let backgroundView = UIView()
        backgroundView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        let label = UILabel(frame: CGRect(x: 0,y: frame.origin.y, width: frame.width, height: frame.height - frame.origin.y))
        label.text = ERROR_MESSAGE.NO_DATA_FOUND
        label.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        backgroundView.addSubview(label)
        return backgroundView
    }
    
    
    func dataSetWhenNoDataFound(){
        if(self.weekTasks.count == 0) {
            self.earningTable.reloadData()
//            self.setHeaderView()
//            self.headerView.setCenterLabel(startDate: (self.weekReport.first?.date)!, endDate: (self.weekReport.last?.date)!)
            var originY:CGFloat = 0
            if headerView != nil {
                originY = self.headerHeight
            }
            self.earningTable.backgroundView = self.tableBackgroundView(CGRect(x: 0, y:originY , width: self.earningTable.frame.width, height: self.earningTable.frame.height))
        } else {
            self.earningTable.backgroundView = nil
        }
    }
    
    
    
    func getAllEarnings(startDate:String) {
        if IJReachability.isConnectedToNetwork() == false {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            self.dataSetWhenNoDataFound()
        } else {
            if offset == 0 {
                let loaderView = UINib(nibName: NIB_NAME.loadingTaskView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! LoadingTaskView
                loaderView.frame = self.earningTable.frame
                self.earningTable.backgroundView = loaderView
            }
            var params:[String : Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
            params["start_date"] = startDate
            params["offset"] = self.offset
            params["num_of_days"] = self.numberOfDays
            print(params)
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.fetch_fleet_weekly_job_report, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    if self.activityIndicator != nil {
                        self.activityIndicator.stopAnimating()
                    }
                    print(response)
                    if isSucceeded == true {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            if let responseData = response["data"] as? NSDictionary {
                                
                                if self.offset == 0 {
                                    self.weekTasks.removeAll()
                                }
                                
                                if self.offset == 0 {
                                    if let value = responseData["week_total"] as? NSNumber {
                                        self.weekTotal = "\(value)"
                                    } else if let value = responseData["week_total"] as? String {
                                        self.weekTotal = value
                                    }
                                    
                                    if let value = responseData["week_distance"] as? NSNumber {
                                        self.weekDistance = "\(value)"
                                    } else if let value = responseData["week_distance"] as? String {
                                        self.weekDistance = value
                                    }
                                    
                                    if let value = responseData["week_duration"] as? NSNumber {
                                        self.weekDuration = "\(value)"
                                    } else if let value = responseData["week_duration"] as? String {
                                        self.weekDuration = value
                                    }
                                    
                                    if self.numberOfDays == 7 {
                                        if let weekReports = responseData["week_report"] as? [[String:Any]] {
                                            for report in weekReports {
                                                let weekReportData = WEEK_REPORT(json: report)
                                                self.weekReport.append(weekReportData)
                                                if Float(weekReportData.total)! > self.maxPriceValue {
                                                    self.maxPriceValue = Float(weekReportData.total)!
                                                    let mod =   Int(self.maxPriceValue) % self.minSlotValue
                                                    let remainder = self.minSlotValue - mod
                                                    self.maxPriceValue = self.maxPriceValue + Float(remainder)
                                                }
                                            }
                                            if self.maxPriceValue <= 0 {
                                                self.maxPriceValue = 200.0
                                            }
                                        }
                                    }
                                }
                                
                                if let offsetCount = responseData["offset"] as? NSNumber {
                                    self.offset = Int(offsetCount)
                                } else if let offsetCount = responseData["offset"] as? String {
                                    self.offset = Int(offsetCount)!
                                }
                                
                                
                                
                                
                                if let weekData = responseData["week_tasks"] as? [[[String:Any]]] {
                                    for sections in weekData {
                                        var sectionJobs = [WEEK_JOBS]()
                                        for rows in sections {
                                            let job = WEEK_JOBS(json: rows)
                                            sectionJobs.append(job)
                                        }
                                        self.weekTasks.append(sectionJobs)
                                    }
                                }
                                self.earningTable.reloadData()
                                self.setHeaderView()
                                self.headerView.currentWeekDate = self.currentWeekDate
                                self.headerView.setCenterLabel(startDate: (self.weekReport.first?.date)!, endDate: (self.weekReport.last?.date)!, currentDateIndex:self.currentDateIndex)
                            }
                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.showInvalidAccessTokenPopup(response["message"] as! String)
                            break
                        default:
                            break
                        }
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        if self.numberOfDays == 1 {
                            self.headerView.selectedGraphIndex = -1
                            self.numberOfDays = 7
                            self.offset = 0
                            self.earningTable.reloadData()
                            self.setHeaderView()
                        }
                    }
                    self.dataSetWhenNoDataFound()
                    guard self.headerView != nil else {
                        return
                    }
                    
                    if self.headerView.barGraphCollectionView != nil {
                        self.headerView.barGraphCollectionView.isUserInteractionEnabled = true
                    }
                }
            })
        }
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    func gotoEarningDetailController(job:WEEK_JOBS) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.earningDetailController) as! EarningDetailController
        controller.taskJob = job
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func updateServerHit() {
        self.offset = 0
        self.maxPriceValue = 0
        self.weekTotal = ""
        self.weekDistance = ""
        self.weekDuration = ""
        self.weekTasks.removeAll()
        self.weekReport.removeAll()
        self.currentDate = self.headerView.getDateSlotForNextPrevious(currentDateIndex:self.currentDateIndex)
        self.earningTable.tableHeaderView = nil
        self.headerView = nil
        self.earningTable.reloadData()
        print(self.currentDate)
        self.getAllEarnings(startDate: self.currentDate)
    }
    
    
    func leftButtonAction() {
        self.currentDateIndex -= 7
        self.headerView.selectedGraphIndex = -1
        self.numberOfDays = 7
        self.updateServerHit()
    }
    
    func rightButtonAction() {
        self.currentDateIndex += 7
        self.headerView.selectedGraphIndex = -1
        self.numberOfDays = 7
        self.updateServerHit()
    }

}

extension EarningController:UITableViewDelegate, UITableViewDataSource {
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.weekTasks.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.weekTasks[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.earningCell) as! EarningCell
        let task = self.weekTasks[indexPath.section][indexPath.row]
        cell.setDotAndPatternLine(indexPath: indexPath, sectionCount: self.weekTasks[indexPath.section].count)
        cell.firstLine.text = task.date
        cell.amountLabel.text = "\(task.total)"
        cell.secondLine.attributedText = self.model.getAttributedText(weekTask: task)
        if indexPath.row == self.weekTasks[indexPath.section].count - 1 {
            cell.separatorInset = UIEdgeInsetsMake(0, 40, 0, 0)
        } else {
            cell.separatorInset = UIEdgeInsetsMake(0, SCREEN_SIZE.width, 0, 0)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == self.weekTasks.count - 1 {
            if self.activityIndicator != nil {
                guard self.activityIndicator.animating == false else {
                    return
                }
                if self.offset != -1 {
                    self.activityIndicator.startAnimating()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        self.getAllEarnings(startDate: self.currentDate)
                    })
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = self.weekTasks[indexPath.section][indexPath.row]
        self.gotoEarningDetailController(job: task)
    }
}

//MARK: Navigation Bar
extension EarningController:NavigationDelegate {
    func setNavigationBar() {
        let navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = TEXT.EARNINGS_CAPS
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.view.addSubview(navigation)
    }
    
    func backAction() {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
    func editAction() {
        
    }
}

extension EarningController:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case self.headerView.barGraphCollectionView:
            return self.weekReport.count
        default:
            return self.headerView.numberOfPriceSlots + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.headerView.barGraphCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.barGraphCell, for: indexPath) as? BarGraphCell
            cell?.widthOfCell = self.headerView.ceilValueForGraphWidth
            cell?.resetCellData()
            var graphColor:UIColor!
            var shadowColor:UIColor!
            var textColor:UIColor!
            let maxHeightOfBar:CGFloat = (self.headerView.heightForCollectionView / CGFloat(self.headerView.numberOfPriceSlots + 1)) * CGFloat(self.headerView.numberOfPriceSlots - 1)
            if self.headerView.selectedGraphIndex == -1{
                graphColor = COLOR.themeForegroundColor
                shadowColor = COLOR.themeForegroundColor
                if self.headerView.getDateSlotForNextPrevious(currentDateIndex: self.currentDateIndex + indexPath.item) == self.availabilityModel.getDateSlotForRepeatPopup(indexPath: 0, format: "yyyy-MM-dd"){
                    textColor = COLOR.TEXT_COLOR
                } else {
                    textColor = COLOR.LITTLE_LIGHT_COLOR
                }
                cell?.valueLabel.isHidden = true
            } else if self.headerView.selectedGraphIndex == indexPath.item{
                graphColor = COLOR.themeForegroundColor
                shadowColor = COLOR.themeForegroundColor
                textColor = COLOR.themeForegroundColor
                cell?.valueLabel.isHidden = false
            } else {
                graphColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
                shadowColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
                textColor = COLOR.LITTLE_LIGHT_COLOR
                cell?.valueLabel.isHidden = false
            }
            
            let graphValue = Float(self.weekReport[indexPath.item].total)!
            
            cell?.setButton(currentDay:self.weekReport[indexPath.item].day, currentDate:self.weekReport[indexPath.item].date, textColor:textColor)
            if graphValue != 0.0{
                cell?.setRectWithCornerRadius(ratio: CGFloat(graphValue / Float(self.maxPriceValue)),graphColor:graphColor, shadowColor:shadowColor,selectedIndex: self.headerView.selectedGraphIndex, maxHeightOfBar: maxHeightOfBar, heightOfCell:(self.headerView.heightForCollectionView / CGFloat(self.headerView.numberOfPriceSlots + 1)))
            }
            cell?.setValueOfRupeesLabel(graphValue, heightOfCell:(self.headerView.heightForCollectionView / CGFloat(self.headerView.numberOfPriceSlots + 1)))
            cell?.horizontalLine(index:indexPath.item, maxHeightOfBar: maxHeightOfBar)
            
            return cell!
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.rupeesScaleCollectionCell, for: indexPath) as? rupeesScaleCollectionCell
            let decrement = (self.maxPriceValue - self.headerView.minPriceValue) / Float(self.headerView.numberOfPriceSlots - 1)
            if indexPath.item < self.headerView.numberOfPriceSlots {
                cell?.rupeeLabel.font = UIFont(name: UIFont().MontserratRegular, size: getAspectRatioValue(value: FONT_SIZE.extraSmall))
                cell?.rupeeLabel.textColor = COLOR.LIGHT_COLOR
                cell?.rupeeLabel.text = "\(Int(self.maxPriceValue) - Int(decrement) * indexPath.item)"
            } else {
                let currentMonth = self.availabilityModel.getDateSlotForRepeatPopup(indexPath: self.currentDateIndex, format: "MMM")
                let nextMonth = self.availabilityModel.getDateSlotForRepeatPopup(indexPath: self.currentDateIndex + 6, format: "MMM")
                if currentMonth == nextMonth{
                    cell?.rupeeLabel.text = "\(currentMonth)"
                } else {
                    cell?.rupeeLabel.text = "\(currentMonth)/\n\(nextMonth)"
                }
                cell?.rupeeLabel.font = UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.small)
                cell?.rupeeLabel.textColor = COLOR.LITTLE_LIGHT_COLOR
            }
            return cell!
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if collectionView == self.headerView.barGraphCollectionView {
            self.headerView.barGraphCollectionView.isUserInteractionEnabled = false
            self.offset = 0
            self.weekTasks.removeAll()
            if self.headerView.selectedGraphIndex == indexPath.item {
                self.headerView.selectedGraphIndex = -1
                self.numberOfDays = 7
                //self.headerView.barGraphCollectionView.reloadData()
                self.getAllEarnings(startDate: self.currentDate)
            } else {
                self.headerView.selectedGraphIndex = indexPath.item
                self.numberOfDays = 1
                let selectedDate = self.headerView.getDateSlotForNextPrevious(currentDateIndex:self.currentDateIndex + indexPath.item)
                self.getAllEarnings(startDate: selectedDate)
                self.headerView.barGraphCollectionView.reloadData()
            }
            self.earningTable.reloadData()

        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.headerView.ceilValueForGraphWidth = ceil((SCREEN_SIZE.width - self.headerView.leadingForCollectionView - self.headerView.trailingForCollectionView) / 7)
        if collectionView == self.headerView.barGraphCollectionView {
            return CGSize(width: self.headerView.ceilValueForGraphWidth, height: self.headerView.heightForCollectionView)
        } else {
            let sizeOfCell = (self.headerView.rupeesScaleCollectionView.frame.height) / CGFloat(self.headerView.numberOfPriceSlots + 1)
            
            if indexPath.item < self.headerView.numberOfPriceSlots {
                return CGSize(width: 40.0, height: sizeOfCell)
            } else {
                let currentMonth = self.availabilityModel.getDateSlotForRepeatPopup(indexPath: self.currentDateIndex, format: "MMM")
                let nextMonth = self.availabilityModel.getDateSlotForRepeatPopup(indexPath: self.currentDateIndex + 6, format: "MMM")
                if currentMonth == nextMonth{
                    return CGSize(width: 40.0, height: 30.0)
                } else {
                    return CGSize(width: 40.0, height: 35.0)
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}
