//
//  JobDescriptionCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 23/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
protocol JobDescriptionDelegate {
    func imageTappedForTaskDescription(imageType:ImageType, index:Int)
}

class JobDescriptionCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var imageCollectionView: UICollectionView!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    let bottomConstraintWithCollection:CGFloat = 70.0
    let bottomConstraintWithoutCollection:CGFloat = 20.0
    var delegate:JobDescriptionDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.icon.image = #imageLiteral(resourceName: "taskDetails")
        self.detailLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.detailLabel.textColor = COLOR.TEXT_COLOR
        self.setImageCollectionCell()
    }

    func setImageCollectionCell() {
        self.imageCollectionView.register(UINib(nibName: NIB_NAME.imageCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.imageCollectionCell)
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
    }
    
    //MARK: UICollection View Delegate...
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Singleton.sharedInstance.selectedTaskDetails.referenceImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width:CGFloat = 52
        let height:CGFloat = 52
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.imageCollectionCell, for: indexPath) as! ImageCollectionCell
        cell.imageView.setImageUrl(urlString: Singleton.sharedInstance.selectedTaskDetails.referenceImageArray[indexPath.item] as! String, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"), indexPath: indexPath, view:collectionView)
        cell.transLayer.isHidden = true
        cell.removeButton.isHidden = true
        cell.imageView.contentMode = UIViewContentMode.scaleAspectFill
        cell.imageView.layer.cornerRadius = 20
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.imageTappedForTaskDescription(imageType: .taskDescription, index: indexPath.row)
//        if(imagesPreview == nil) {
//            imagesPreview = UINib(nibName: "ImagesPreview", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ImagesPreview
//            imagesPreview.frame = UIScreen.main.bounds
//            imagesPreview.imageType = ImageType.taskDescription.hashValue
//            imagesPreview.imageCollectionView.frame = CGRect(x: imagesPreview.imageCollectionView.frame.origin.x, y: imagesPreview.imageCollectionView.frame.origin.y, width: self.view.frame.width, height: imagesPreview.imageCollectionView.frame.height)
//            imagesPreview.delegate = self
//            imagesPreview.imageArray = NSMutableArray(array: Singleton.sharedInstance.selectedTaskDetails.referenceImageArray)
//            // imagesPreview.completeStatus = Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus
//            imagesPreview.backgroundColor = UIColor.clear
//            imagesPreview.setCollectionViewWithPage((indexPath as NSIndexPath).item)
//            self.view.addSubview(imagesPreview)
//            imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
//            
//            UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
//                self.imagesPreview.transform = CGAffineTransform.identity
//            }, completion: { finished in
//                self.imagesPreview.backgroundColor = COLOR.imagePreviewBackgroundColor
//            })
//        }
        
    }
//
//    //MARK: ImagePreviewDelegate Methods
//    func dismissImagePreview() {
//        imagesPreview.backgroundColor = UIColor.clear
//        UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
//            self.imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
//        }, completion: { finished in
//            self.imagesPreview.removeFromSuperview()
//            self.imagesPreview = nil
//        })
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
