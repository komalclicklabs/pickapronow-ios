//
//  UnsyncedTask+CoreDataProperties.swift
//  
//
//  Created by CL-macmini45 on 5/23/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UnsyncedTask {

    @NSManaged var job_id: NSNumber?
    @NSManaged var latitude: String?
    @NSManaged var longitude: String?
    @NSManaged var reason: String?
    @NSManaged var job_status: NSNumber?
    @NSManaged var timestamp: String?
    @NSManaged var status:String?
}
