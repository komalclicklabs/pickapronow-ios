//
//  AvailabilityDotCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 19/07/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class AvailabilityDotCell: UICollectionViewCell {

    let numberOfDots = 4
   // let margin:CGFloat = 5.0
    let dotSize:CGFloat = 7.0
    //var shapeLayer = [CAShapeLayer]()
    
    @IBOutlet var dotCollection: [UIView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.setDots()
        for dot in dotCollection {
            dot.layer.cornerRadius = dotSize / 2
        }
    }
    
//    func setDots(){
//        let xOrigin:CGFloat = self.center.x - dotSize/2
//        var yOrigin:CGFloat = 6
//        if self.shapeLayer.count >= numberOfDots {
//            self.shapeLayer.removeAll()
//        }
//        for _ in 0..<numberOfDots {
//            let drawPath = UIBezierPath(ovalIn: CGRect(x: xOrigin, y: yOrigin, width: dotSize, height: dotSize))
//            let filledShape = CAShapeLayer()
//            filledShape.path = drawPath.cgPath
//            filledShape.fillColor = UIColor.red.cgColor
//            self.layer.addSublayer(filledShape)
//            self.shapeLayer.append(filledShape)
//            yOrigin = yOrigin + dotSize + margin
//        }
//    }
}
