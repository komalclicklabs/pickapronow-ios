//
//  Task+CoreDataProperties.swift
//  
//
//  Created by CL-macmini45 on 5/23/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Task {

    @NSManaged var job_id: NSNumber?
    @NSManaged var sync: NSNumber?
    @NSManaged var task_object: NSObject?
    @NSManaged var timestamp: String?

}
