                 //
//  FilterModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 14/04/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class FilterModel: NSObject {

    //Mark : Reset Status count
    
    func setFilterStatus() {
        self.initFilterStatus()
    }
    
    func initFilterStatus() {
        guard Singleton.sharedInstance.filterStatusArray.count == 0 else {
            return
        }
        Singleton.sharedInstance.filterStatusArray = [FilterStatus(title: TEXT.ASSIGNED, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
                                                      FilterStatus(title: "\(TEXT.ACCEPTED) / \(TEXT.ACKNOWLEDGED)", count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
                                                      FilterStatus(title: TEXT.STARTED, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
                                                      FilterStatus(title: TEXT.ARRIVED, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
                                                      FilterStatus(title: TEXT.SUCCESSFUL, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
                                                      FilterStatus(title: TEXT.FAILED, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
                                                      FilterStatus(title: TEXT.CANCELED, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
                                                      FilterStatus(title: TEXT.DECLINE, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType))]
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: Singleton.sharedInstance.filterStatusArray), forKey: USER_DEFAULT.filterDictionary)

    }
    
    func saveStatusInUserDefaults() {
        
        guard Singleton.sharedInstance.filterStatusArray.count > 0 else {
            return
        }
        
        Singleton.sharedInstance.filterStatusArray =
            [FilterStatus(title: TEXT.ASSIGNED, count: Singleton.sharedInstance.filterStatusArray[0].count!, isSelected: Singleton.sharedInstance.filterStatusArray[0].isSelected!, isLocallySelected: Singleton.sharedInstance.filterStatusArray[0].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
             FilterStatus(title: "\(TEXT.ACCEPTED) / \(TEXT.ACKNOWLEDGED)", count: Singleton.sharedInstance.filterStatusArray[1].count!, isSelected: Singleton.sharedInstance.filterStatusArray[1].isSelected!, isLocallySelected: Singleton.sharedInstance.filterStatusArray[1].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
             FilterStatus(title: TEXT.STARTED, count: Singleton.sharedInstance.filterStatusArray[2].count!, isSelected: Singleton.sharedInstance.filterStatusArray[2].isSelected!, isLocallySelected: Singleton.sharedInstance.filterStatusArray[2].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
             FilterStatus(title: TEXT.ARRIVED, count: Singleton.sharedInstance.filterStatusArray[3].count!, isSelected: Singleton.sharedInstance.filterStatusArray[3].isSelected!, isLocallySelected: Singleton.sharedInstance.filterStatusArray[3].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
             FilterStatus(title: TEXT.SUCCESSFUL, count: Singleton.sharedInstance.filterStatusArray[4].count!, isSelected: Singleton.sharedInstance.filterStatusArray[4].isSelected!, isLocallySelected: Singleton.sharedInstance.filterStatusArray[4].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
             FilterStatus(title: TEXT.FAILED, count: Singleton.sharedInstance.filterStatusArray[5].count!, isSelected: Singleton.sharedInstance.filterStatusArray[5].isSelected!, isLocallySelected: Singleton.sharedInstance.filterStatusArray[5].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
             FilterStatus(title: TEXT.CANCELED, count: Singleton.sharedInstance.filterStatusArray[6].count!, isSelected: Singleton.sharedInstance.filterStatusArray[6].isSelected!, isLocallySelected: Singleton.sharedInstance.filterStatusArray[6].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
             FilterStatus(title: TEXT.DECLINE, count: Singleton.sharedInstance.filterStatusArray[7].count!, isSelected: Singleton.sharedInstance.filterStatusArray[7].isSelected!, isLocallySelected: Singleton.sharedInstance.filterStatusArray[7].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType))]
        
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: Singleton.sharedInstance.filterStatusArray), forKey: USER_DEFAULT.filterDictionary)
        
    }
    
    func setSortingDictionary() {
        self.initSortingDictionary()
    }
    
    func initSortingDictionary() {
        guard Singleton.sharedInstance.sortingDictionary.count == 0 else {
            return
        }
        Singleton.sharedInstance.sortingDictionary = [FilterStatus(title: TEXT.TIME_R, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
                                                      FilterStatus(title: TEXT.DISTANCE_SMALL, count: 0, isSelected: false, isLocallySelected: false,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType))]
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: Singleton.sharedInstance.sortingDictionary), forKey: USER_DEFAULT.sortingDictionary)
    }
    
    func saveSoringInUserDefaults() {
        guard Singleton.sharedInstance.sortingDictionary.count > 0 else {
            return
        }
        
        Singleton.sharedInstance.sortingDictionary =
            [FilterStatus(title: TEXT.TIME_R, count: Singleton.sharedInstance.sortingDictionary[0].count!, isSelected: Singleton.sharedInstance.sortingDictionary[0].isSelected!, isLocallySelected: Singleton.sharedInstance.sortingDictionary[0].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)),
             FilterStatus(title: TEXT.DISTANCE_SMALL, count: Singleton.sharedInstance.sortingDictionary[1].count!, isSelected: Singleton.sharedInstance.sortingDictionary[1].isSelected!, isLocallySelected: Singleton.sharedInstance.sortingDictionary[1].isLocallySelected!,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType))]
        
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: Singleton.sharedInstance.sortingDictionary), forKey: USER_DEFAULT.sortingDictionary)
    }
    
    func setTemplateDictionary(keys:[String]) {
        self.initTemplateDictionary(keys: keys)
    }
    
    func initTemplateDictionary(keys:[String]) {
        var templateStatusArray = [FilterStatus]()
        for i in 0..<keys.count {
            let previousSelection = self.getPreviousTemplateStatus(index: i, title: keys[i])
            let templateStatus = FilterStatus(title: keys[i], count: 0, isSelected: previousSelection, isLocallySelected: previousSelection,layoutType: UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType))
            templateStatusArray.append(templateStatus)
        }
        Singleton.sharedInstance.templateDictionary.removeAll()
        Singleton.sharedInstance.templateDictionary = templateStatusArray
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: Singleton.sharedInstance.templateDictionary), forKey: USER_DEFAULT.templateDictionary)
    }
    
    func getPreviousTemplateStatus(index:Int, title:String) -> Bool {
        guard index < Singleton.sharedInstance.templateDictionary.count else {
            return false
        }
        
        let template = Singleton.sharedInstance.templateDictionary[index]
        if template.title == title {
            return template.isSelected!
        } else {
            for template in Singleton.sharedInstance.templateDictionary {
                if template.title == title {
                    return template.isSelected!
                }
            }
        }
        return false
    }
    
    func resetStatusCount(){
        for  i in 0..<Singleton.sharedInstance.filterStatusArray.count{
            Singleton.sharedInstance.filterStatusArray[i].count = 0
        }
    }
    
    func resetTemplateCount(){
        for  i in 0..<Singleton.sharedInstance.templateDictionary.count{
            Singleton.sharedInstance.templateDictionary[i].count = 0
            Singleton.sharedInstance.templateDictionary[i].isSelected = false
            Singleton.sharedInstance.templateDictionary[i].isLocallySelected = false
        }
    }
    
    func resetSortingCount(){
        for  i in 0..<Singleton.sharedInstance.sortingDictionary.count{
            Singleton.sharedInstance.sortingDictionary[i].count = 0
        }
    }
    
    func checkStatusSelection() -> Bool {
        for i in Singleton.sharedInstance.filterStatusArray{
            if i.isLocallySelected != i.isSelected{
                return true
            }
        }
        return false
    }
    
    func checkTemplateSelection() -> Bool {
        for i in Singleton.sharedInstance.templateDictionary {
            if i.isLocallySelected != i.isSelected{
                return true
            }
        }
        return false
    }
    
    func checkSortingSelection() -> Bool {
        for i in Singleton.sharedInstance.sortingDictionary{
            if i.isLocallySelected != i.isSelected{
                return true
            }
        }
        return false
    }
    
    func isAnyStatusFieldSelected() -> Bool {
        for filterStatus in Singleton.sharedInstance.filterStatusArray{
            if filterStatus.isLocallySelected == true {
                return true
            }
        }
        return false
    }
    
    func isAnyTemplateFieldSelected() -> Bool {
        for filterStatus in Singleton.sharedInstance.templateDictionary{
            if filterStatus.isLocallySelected == true {
                return true
            }
        }
        return false
    }
    
    func isAnySortingFieldSelected() -> Bool {
        for filterStatus in Singleton.sharedInstance.sortingDictionary{
            if filterStatus.isLocallySelected == true {
                return true
            }
        }
        return false
    }
    
    func updateLocalStatusWithGlobal() {
        for i in 0..<Singleton.sharedInstance.filterStatusArray.count {
            Singleton.sharedInstance.filterStatusArray[i].isLocallySelected = Singleton.sharedInstance.filterStatusArray[i].isSelected!
        }
    }
    
    func updateGlobalStatusWithLocal() {
        for i in 0..<Singleton.sharedInstance.filterStatusArray.count {
            Singleton.sharedInstance.filterStatusArray[i].isSelected = Singleton.sharedInstance.filterStatusArray[i].isLocallySelected
        }
    }
    
    func updateLocalTemplateWithGlobal() {
        for i in 0..<Singleton.sharedInstance.templateDictionary.count {
            Singleton.sharedInstance.templateDictionary[i].isLocallySelected = Singleton.sharedInstance.templateDictionary[i].isSelected!
        }
    }
    
    func updateGlobalTemplateWithLocal() {
        for i in 0..<Singleton.sharedInstance.templateDictionary.count {
            Singleton.sharedInstance.templateDictionary[i].isSelected = Singleton.sharedInstance.templateDictionary[i].isLocallySelected
        }
    }
    
    func updateLocalSortingWithGlobal() {
        for i in 0..<Singleton.sharedInstance.sortingDictionary.count {
            Singleton.sharedInstance.sortingDictionary[i].isLocallySelected = Singleton.sharedInstance.sortingDictionary[i].isSelected!
        }
    }
    
    func updateGlobalSortingWithLocal() {
        for i in 0..<Singleton.sharedInstance.sortingDictionary.count {
            Singleton.sharedInstance.sortingDictionary[i].isSelected = Singleton.sharedInstance.sortingDictionary[i].isLocallySelected
        }
    }
    
    
    func setFilterCount() {
        for i in (0..<NetworkingHelper.sharedInstance.dayTasksListArray.count){
            let innerArray = NetworkingHelper.sharedInstance.dayTasksListArray[i]
            for task in innerArray {
                self.setStatusCount(status: task.jobStatus)
            }
         }
    }
    
    //Mark: Setting count in statusArray
    func  setStatusCount(status:Int) {
        switch status {
        case JOB_STATUS.assigned:
            Singleton.sharedInstance.filterStatusArray[0].count! += 1
        case JOB_STATUS.accepted:
            Singleton.sharedInstance.filterStatusArray[1].count! += 1
        case JOB_STATUS.started:
            Singleton.sharedInstance.filterStatusArray[2].count! += 1
        case JOB_STATUS.arrived:
            Singleton.sharedInstance.filterStatusArray[3].count! += 1
        case JOB_STATUS.successful:
            Singleton.sharedInstance.filterStatusArray[4].count! += 1
        case JOB_STATUS.failed:
            Singleton.sharedInstance.filterStatusArray[5].count! += 1
        case JOB_STATUS.canceled:
            Singleton.sharedInstance.filterStatusArray[6].count! += 1
        case JOB_STATUS.declined:
            Singleton.sharedInstance.filterStatusArray[7].count! += 1
        default:
            break
        }
    }

}
