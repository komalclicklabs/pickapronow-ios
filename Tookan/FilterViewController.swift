//
//  FilterViewController.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 4/5/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import Crashlytics
import FirebaseAnalytics

class FilterViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lineBelowFilterTask: UIView!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var filterTable: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var applyButtonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var heightOfTopView: NSLayoutConstraint!
    var filterTaskBy = [TEXT.KEYWORD,TEXT.STATUS,TEXT.SORTING, TEXT.TEMPLATE]
    var templateArray = [String]()
    var statusSelected  = false
    var templateSelected = false
    var sortingSelected = false
    var model = FilterModel()
    var isTemplateHitInProgress = true
    //var keywordValue = ""
    var previousSearchedText = Singleton.sharedInstance.searchedText
    
    
    let filter_section_tag = 1000
    
    enum Filter_Task:Int{
        case KEYWORD
        case STATUS
        case SORTING
        case TEMPLATE
    }
    var filterTask : Filter_Task!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.model.setFilterStatus()
        self.model.resetStatusCount()
        self.model.setFilterCount()
        
        self.model.setSortingDictionary()
        self.model.resetSortingCount()
        
        /*============= Setting Label ====================*/
        let attributedString = NSMutableAttributedString(string: TEXT.FILTER_TASK_BY)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.5), range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttributes([NSFontAttributeName: UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)!], range: NSRange(location: 0, length: attributedString.length))
        filterLabel.textColor = COLOR.TEXT_COLOR
        filterLabel.attributedText = attributedString
        
        /*============= Setting Close Button ===============*/
        closeButton.setImage(#imageLiteral(resourceName: "close.png").withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = COLOR.TEXT_COLOR
        self.lineBelowFilterTask.backgroundColor = COLOR.LINE_COLOR
        
        //*================= Filter Table =================*/
        self.filterTable.register(UINib(nibName: NIB_NAME.filterViewCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.filterViewCell)
        self.filterTable.delegate = self
        self.filterTable.dataSource = self
        self.filterTable.transform = CGAffineTransform(translationX: 0, y: -SCREEN_SIZE.height/2)
        self.filterTable.rowHeight = UITableViewAutomaticDimension
        self.filterTable.tag = 99
        self.filterTable.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        //self.filterTable.estimatedRowHeight = 50
        /*============= Setting apply Button ===============*/
        self.applyButton.backgroundColor = COLOR.themeForegroundColor
        self.applyButton.setTitle(TEXT.APPLY, for: .normal)
        self.applyButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.applyButton.transform = CGAffineTransform(translationX: 0, y: SCREEN_SIZE.height)
        self.applyButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.topView.transform = CGAffineTransform(translationX: 0, y: -SCREEN_SIZE.height/2)
        
        /*============= Tap Gesture =================*/
        self.view.tag = 99
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTapAction))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        tap.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tap)
        
        self.heightOfTopView.constant = self.heightOfTopView.constant + Singleton.sharedInstance.getTopInsetofSafeArea()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isTemplateHitInProgress = true
        let fleetId = Singleton.sharedInstance.fleetDetails.fleetId!
        var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["fleet_id"] = fleetId
        params["layout_type"] = UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)
        //            "layout_type":UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)
//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//            "fleet_id":fleetId,
//            "layout_type":UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)
//            ] as [String : Any]
        if IJReachability.isConnectedToNetwork() == true {
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getTemplates, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
                DispatchQueue.main.async {
                    self.isTemplateHitInProgress = false
                    if(isSucceeded == true) {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            if let array = response["data"] as? [String] {
                                self.templateArray = array
                                self.model.setTemplateDictionary(keys: self.templateArray)
                               // self.disabledTemplateButton()
                                self.filterTable.reloadData()
                            }
                            break
                        default:
                            break
                        }
                    }
                }
            }
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.filterTable.transform = CGAffineTransform.identity
            self.topView.transform = CGAffineTransform.identity
        }, completion: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                UIApplication.shared.statusBarStyle = .lightContent
            } else {
                UIApplication.shared.statusBarStyle = .default
            }
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //Mark: Background Tap action
    func backgroundTapAction() {
        Singleton.sharedInstance.searchedText = self.previousSearchedText
        self.dismissView()
    }
    
    
    //Mark: CLose Button Action
    @IBAction func closeButtonAction(_ sender: Any) {
        Singleton.sharedInstance.searchedText = self.previousSearchedText
        self.dismissView()
    }
    
    //Mark: Apply Button Action
    @IBAction func applyAction(_ sender: Any) {
        self.previousSearchedText = Singleton.sharedInstance.searchedText
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.FILTER_APPLY, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.FILTER_APPLY, customAttributes: [:])
        self.model.updateGlobalStatusWithLocal()
        self.model.updateGlobalTemplateWithLocal()
        self.model.updateGlobalSortingWithLocal()
        self.applyButton.transform = CGAffineTransform(translationX: 0, y: self.applyButton.frame.height + Singleton.sharedInstance.getBottomInsetofSafeArea())
        self.model.saveStatusInUserDefaults()
        self.model.saveSoringInUserDefaults()
//        if templateArray.count > 0 {
//            Singleton.sharedInstance.templateDictionary.removeAll()
//        }
        
        self.model.setTemplateDictionary(keys: self.templateArray)
        UserDefaults.standard.set(Singleton.sharedInstance.searchedText, forKey: USER_DEFAULT.searchTextForFilter)
       // UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: Singleton.sharedInstance.filterStatusArray), forKey: USER_DEFAULT.filterDictionary)
      //  UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: Singleton.sharedInstance.templateDictionary), forKey: USER_DEFAULT.templateDictionary)
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
        self.dismissView()
    }
    
    
    //Mark: TableView Delegate and DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterTaskBy.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      filterTask = FilterViewController.Filter_Task(rawValue: section)
        switch filterTask! {
        case .STATUS:
            if(self.statusSelected == true){
               return Singleton.sharedInstance.filterStatusArray.count
            }else{
                return 0
            }
        case .TEMPLATE:
            if(self.templateSelected == true){
               return self.templateArray.count
            }else{
                return 0
            }
        case .KEYWORD:
            return 0
        case .SORTING:
            if(self.sortingSelected == true){
                return Singleton.sharedInstance.sortingDictionary.count
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.filterViewCell) as! FilterViewCell
        cell.backgroundColor = COLOR.TABLE_BACKGROUND_COLOR
        cell.selectionStyle = .none
        cell.filterName.text = ""
        filterTask = FilterViewController.Filter_Task(rawValue: indexPath.section)
        switch filterTask! {
        case .STATUS:
            if self.statusSelected == true {
                cell.filterName.text = Singleton.sharedInstance.filterStatusArray[indexPath.row].title
                cell.filterNumber.isHidden = false
                cell.filterNumber.text = "\(Singleton.sharedInstance.filterStatusArray[indexPath.row].count ?? 0)"
                if Singleton.sharedInstance.filterStatusArray[indexPath.row].isLocallySelected == true{
                    cell.filterName.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
                    cell.filterCellIcon.isHidden = false
                }else{
                    cell.filterName.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
                    cell.filterCellIcon.isHidden = true
                }
            }
        case .TEMPLATE:
           if self.templateSelected == true {
                cell.filterNumber.isHidden = true
                cell.filterName.text = Singleton.sharedInstance.templateDictionary[indexPath.row].title
                if Singleton.sharedInstance.templateDictionary[indexPath.row].isLocallySelected == true{
                    cell.filterName.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
                    cell.filterCellIcon.isHidden = false
                }else{
                    cell.filterName.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
                    cell.filterCellIcon.isHidden = true
                }
            }
        case .KEYWORD:
            break
        case .SORTING:
            
            if self.sortingSelected == true {
                cell.filterNumber.isHidden = true
                cell.filterName.text = Singleton.sharedInstance.sortingDictionary[indexPath.row].title
                if Singleton.sharedInstance.sortingDictionary[indexPath.row].isLocallySelected == true{
                    cell.filterName.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
                    cell.filterCellIcon.isHidden = false
                }else{
                    cell.filterName.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
                    cell.filterCellIcon.isHidden = true
                }
            }
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       let header =  UINib(nibName: NIB_NAME.filterSectionHeader, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! FilterSectionHeader
        filterTask = FilterViewController.Filter_Task(rawValue: section)
        header.filterHeaderIcon.transform = CGAffineTransform.identity
        
        switch filterTask! {
        case .STATUS:
            if (statusSelected == true){
                UIView.animate(withDuration: 0.5, animations: {
                    header.filterHeaderIcon.transform = CGAffineTransform(rotationAngle: .pi)
                })
            } else {
                UIView.animate(withDuration: 0.5, animations: {
                    header.filterHeaderIcon.transform = CGAffineTransform.identity
                })
            }
            header.clearButton.isHidden = !self.model.isAnyStatusFieldSelected()
        
        case .TEMPLATE:
            if isTemplateHitInProgress == true {
                header.activityIndicator.startAnimating()
            } else {
                header.activityIndicator.stopAnimating()
            }
            
            
            if (templateSelected == true){
                UIView.animate(withDuration: 0.5, animations: {
                    header.filterHeaderIcon.transform = CGAffineTransform(rotationAngle: .pi)
                })
            }else{
                //header.filterHeaderIcon.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                UIView.animate(withDuration: 0.5, animations: {
                    header.filterHeaderIcon.transform = CGAffineTransform.identity
                })
            }
            header.clearButton.isHidden = !self.model.isAnyTemplateFieldSelected()
        case .KEYWORD:
            let keywordHeader = UINib(nibName: NIB_NAME.keyWordSectionHeader, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! KeyWordSectionHeader
            keywordHeader.delegate = self
            keywordHeader.keywordTextField.text = Singleton.sharedInstance.searchedText
            return keywordHeader
        case .SORTING:
            if (self.sortingSelected == true){
                UIView.animate(withDuration: 0.5, animations: {
                    header.filterHeaderIcon.transform = CGAffineTransform(rotationAngle: .pi)
                })
            }else{
                //header.filterHeaderIcon.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                UIView.animate(withDuration: 0.5, animations: {
                    header.filterHeaderIcon.transform = CGAffineTransform.identity
                })
            }
            header.clearButton.isHidden = !self.model.isAnySortingFieldSelected()
        }
        
        header.filterHeaderLabel.text = self.filterTaskBy[section]
        header.tag = section + filter_section_tag
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.headerSelected(tap: )))
        tapRecognizer.delegate = self
        tapRecognizer.numberOfTapsRequired = 1
        header.addGestureRecognizer(tapRecognizer)
        header.clearButton.tag = section
        header.clearButton.addTarget(self, action: #selector(self.clearSelection(button:)), for: .touchUpInside)
        return header
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getAspectRatioValue(value: 70)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let header = self.filterTable.viewWithTag(indexPath.section + filter_section_tag) as? FilterSectionHeader
        filterTask = FilterViewController.Filter_Task(rawValue: indexPath.section)
        switch filterTask! {
        case .STATUS:
            Analytics.logEvent(ANALYTICS_KEY.FILTER_STATUS, parameters: [ANALYTICS_KEY.FILTER_STATUS : Singleton.sharedInstance.filterStatusArray[indexPath.row].title as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.FILTER_STATUS, customAttributes: [ANALYTICS_KEY.FILTER_STATUS : Singleton.sharedInstance.filterStatusArray[indexPath.row].title])
            if Singleton.sharedInstance.filterStatusArray[indexPath.row].isLocallySelected == true {
                Singleton.sharedInstance.filterStatusArray[indexPath.row].isLocallySelected = false
            } else {
                Singleton.sharedInstance.filterStatusArray[indexPath.row].isLocallySelected = true
            }
             header?.clearButton.isHidden = !self.model.isAnyStatusFieldSelected()
            break
        case .TEMPLATE:
            Analytics.logEvent(ANALYTICS_KEY.FILTER_TEMPLATE, parameters: [ANALYTICS_KEY.FILTER_TEMPLATE : Singleton.sharedInstance.templateDictionary[indexPath.row].title as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.FILTER_TEMPLATE, customAttributes: [ANALYTICS_KEY.FILTER_TEMPLATE : Singleton.sharedInstance.templateDictionary[indexPath.row].title])
            if Singleton.sharedInstance.templateDictionary[indexPath.row].isLocallySelected == true {
                Singleton.sharedInstance.templateDictionary[indexPath.row].isLocallySelected = false
            } else {
                Singleton.sharedInstance.templateDictionary[indexPath.row].isLocallySelected = true
            }
            header?.clearButton.isHidden = !self.model.isAnyTemplateFieldSelected()
           break
        case .KEYWORD:
            Analytics.logEvent(ANALYTICS_KEY.FILTER_KEYWORD, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.FILTER_KEYWORD, customAttributes: [:])
            break
        case .SORTING:
            Analytics.logEvent(ANALYTICS_KEY.FILTER_SORTING, parameters: [ANALYTICS_KEY.FILTER_SORTING : Singleton.sharedInstance.sortingDictionary[indexPath.row].title as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.FILTER_SORTING, customAttributes: [ANALYTICS_KEY.FILTER_SORTING : Singleton.sharedInstance.sortingDictionary[indexPath.row].title])
            if indexPath.row == 0 {
                if Singleton.sharedInstance.sortingDictionary[0].isLocallySelected == true {
                    Singleton.sharedInstance.sortingDictionary[0].isLocallySelected = false
                } else {
                    Singleton.sharedInstance.sortingDictionary[0].isLocallySelected = true
                    Singleton.sharedInstance.sortingDictionary[1].isLocallySelected = false
                }
            } else {
                if Singleton.sharedInstance.sortingDictionary[1].isLocallySelected == true {
                    Singleton.sharedInstance.sortingDictionary[1].isLocallySelected = false
                } else {
                    Singleton.sharedInstance.sortingDictionary[1].isLocallySelected = true
                    Singleton.sharedInstance.sortingDictionary[0].isLocallySelected = false
                }
            }
            
            header?.clearButton.isHidden = !self.model.isAnySortingFieldSelected()
            self.filterTable.reloadSections(IndexSet(integer: 2), with: UITableViewRowAnimation.none)
            break
        }
        
        self.updateApplyButton()
        self.filterTable.reloadRows(at: [indexPath], with: .none)
    }
    
    func getDictionary(keys:[String])->[String:Bool]{
        var dict = [String:Bool]()
        for i in 0..<keys.count{
            dict.updateValue(false, forKey: keys[i])
        }
        return dict
    }
    
    func headerSelected(tap : UITapGestureRecognizer){
        self.view.endEditing(true)
        let section = (tap.view?.tag)!  % filter_section_tag
       
        if(section == 1){
            self.statusSelected = !self.statusSelected
        } else if section == 2 {
            self.sortingSelected = !self.sortingSelected
        } else {
            guard isTemplateHitInProgress == false else {
                return
            }
            
            guard self.templateArray.count > 0 else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_TEMPLATE_FOUNDS, isError: .error)
                return
            }
            self.templateSelected = !self.templateSelected
        }
        self.filterTable.reloadSections(IndexSet(integer: section), with: .automatic)
    }
    
    func clearSelection(button : UIButton){
        Analytics.logEvent(ANALYTICS_KEY.FILTER_CLEAR, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.FILTER_CLEAR, customAttributes: [:])
        filterTask = FilterViewController.Filter_Task(rawValue: button.tag)
        switch filterTask! {
        case .STATUS:
            for  i in 0..<Singleton.sharedInstance.filterStatusArray.count{
                Singleton.sharedInstance.filterStatusArray[i].isLocallySelected = false
            }
        case .TEMPLATE:
            for  i in 0..<Singleton.sharedInstance.templateDictionary.count{
                Singleton.sharedInstance.templateDictionary[i].isLocallySelected = false
            }
        case .KEYWORD:
            break
        case .SORTING:
            for  i in 0..<Singleton.sharedInstance.sortingDictionary.count{
                Singleton.sharedInstance.sortingDictionary[i].isLocallySelected = false
            }
        }
        
        self.filterTable.reloadSections(IndexSet(integer: (button.tag)), with: .none)
        self.updateApplyButton()
    }
    //Mark: Show Apply Button with Animation
    func updateApplyButton() {
        guard self.model.checkStatusSelection() == false else {
            self.showApply()
            return
        }
        guard self.model.checkTemplateSelection() == false else {
            self.showApply()
            return
        }
        
        guard self.model.checkSortingSelection() == false else {
            self.showApply()
            return
        }
        
        
        guard Singleton.sharedInstance.searchedText == self.previousSearchedText else {
            self.showApply()
            return
        }
        self.hideApply()
    }
    
    func showApply(){
        self.bottomConstraint.constant = self.applyButton.frame.height
        self.applyButton.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.applyButton.transform = CGAffineTransform.identity
        }
    }
    //Mark: Hide Apply Button with Animation
    func hideApply(){
        self.bottomConstraint.constant = 0.0
        UIView.animate(withDuration: 0.5) { 
            self.applyButton.transform = CGAffineTransform(translationX: 0, y: self.applyButton.frame.height + Singleton.sharedInstance.getBottomInsetofSafeArea())
        }
    }
    //Mark Dismiss View
    func dismissView(){
        UIView.animate(withDuration:0.35, animations: {
            self.filterTable.transform = CGAffineTransform(translationX: 0, y: -SCREEN_SIZE.height)
            self.topView.transform = CGAffineTransform(translationX: 0, y: -SCREEN_SIZE.height)
        }) { finished in
            self.model.updateLocalStatusWithGlobal()
            self.model.updateLocalTemplateWithGlobal()
            self.model.updateLocalSortingWithGlobal()
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    //MARK: Handling Keyboard Toggle
    func keyboardWillShow(notification: NSNotification) {
        //self.applyButtonBottomConstraint.constant = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as!
       self.view.setNeedsLayout()
        self.bottomConstraint.constant = (((notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue)?.height)!
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
            self.view.layoutIfNeeded()
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: false)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        self.view.setNeedsLayout()
        if self.applyButton.transform.ty == 0 {
            bottomConstraint.constant = self.applyButton.frame.height
        } else {
            bottomConstraint.constant = 0.0
        }
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
            self.view.layoutIfNeeded()
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }
    
}
extension FilterViewController : KeyWordSectionHeaderDelegate{
    
    func setApplyButton() {
        self.updateApplyButton()
    }
    
    func hideApplyButton(textField :UITextField){
        self.updateApplyButton()
    }
}

//MARK: UIGestureRecognizerDelegate method
extension FilterViewController:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view != nil && touch.view!.isDescendant(of: self.filterTable) {
            if touch.view?.tag == 99 || touch.view?.tag == 1001 || touch.view?.tag == 1002 || touch.view?.tag == 1003 {
                return true
            }
            return false
        }
        return true
    }
 }
