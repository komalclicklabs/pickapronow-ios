//
//  DetailHeaderView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class DetailHeaderView: UIView {
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var addButton: UIButton!
    @IBOutlet var requiredLabel: UILabel!
    @IBOutlet var topLine: UIView!
    @IBOutlet var bottomLine: UIView!
    @IBOutlet var buttonWidthConstraint: NSLayoutConstraint!
    @IBOutlet var addButtonTrailing: NSLayoutConstraint!
    @IBOutlet var headerLabelLeading: NSLayoutConstraint!
    var defaultButtonWidth:CGFloat = 35.0

    class func getHeaderView() -> DetailHeaderView {
        let headerView = UINib(nibName: NIB_NAME.detailHeaderView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! DetailHeaderView
        headerView.backgroundColor = COLOR.TABLE_HEADER_COLOR
        headerView.addButton.transform = CGAffineTransform.identity
        headerView.addButton.isUserInteractionEnabled = true
        return headerView
    }
    
    override func awakeFromNib() {
        self.topLine.backgroundColor = COLOR.LINE_COLOR
        self.bottomLine.backgroundColor = COLOR.LINE_COLOR
        
        self.headerLabel.textColor = COLOR.TEXT_COLOR
        self.headerLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.headerLabel.setLetterSpacing(value: 1.8)
        
        self.requiredLabel.textColor = COLOR.REQUIRED_COLOR
        self.requiredLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.requiredLabel.setLetterSpacing(value: 1.8)
    }
    
    func setForNotes() {
        self.headerLabel.text = TEXT.NOTES
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "add").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
    }
    
    func setForSignature() {
        self.headerLabel.text = TEXT.SIGNATURE
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "add").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
    }

    func setForImage() {
        self.headerLabel.text = TEXT.IMAGES
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "add").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
        self.addButton.tag = IMAGE_TAG
    }
    
    func setForBarcode() {
        self.headerLabel.text = TEXT.BARCODE
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "add").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
    }
    
    func setForCustomBarcode(index:Int) {
        self.headerLabel.text = Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[index].displayLabelName
        self.headerLabel.setLetterSpacing(value: 1.8)
        let fleetData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index])
        if fleetData.length > 0 {
            self.addButton.isHidden = true
        } else {
            self.addButton.setImage(#imageLiteral(resourceName: "add").withRenderingMode(.alwaysTemplate), for: .normal)
            self.addButton.tintColor = COLOR.themeForegroundColor
        }
    }
    
    func setForCustomImage(index:Int) {
        self.headerLabel.text = Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[index].displayLabelName
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "add").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
        self.addButton.tag = index
    }
    
    func setForDefaultSection(index:Int) {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return
        }
        guard index < (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)!  else {
            return
        }
        self.headerLabel.text = Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[index].displayLabelName
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.isHidden = true
    }
    
    func setForCheckbox(index:Int) {
        self.headerLabel.text = Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[index].displayLabelName
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "checkboxEmpty"), for: .normal)
        self.addButton.setImage(#imageLiteral(resourceName: "checkboxTicked"), for: .selected)
        self.addButton.tintColor = COLOR.themeForegroundColor
        if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].data == "0" || Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].data == "false" || Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].data == "") {
            self.addButton.isSelected = false
        } else {
            self.addButton.isSelected = true
        }
    }
    
    func setForTableChecklist(index:Int) {
        self.headerLabel.text = Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[index].displayLabelName
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "largeRigthArrow").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
        self.addButton.tag = index
    }
    
    func setForChecklist(index:Int) {
        self.headerLabel.text = Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[index].displayLabelName
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "dropdownDown").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
        self.addButton.tag = index
    }
    
    func setForInvoice(index:Int) {
        self.headerLabel.text = TEXT.INVOICE
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "largeRigthArrow").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
        self.addButton.tag = index
    }
    
    func setForEarnings(index:Int) {
        self.headerLabel.text = "\(TEXT.MORE_CAP) \(TEXT.DETAILS)" //"\(TEXT.EARNINGS_CAPS): \(Singleton.sharedInstance.selectedTaskDetails.driverJobTotal!)"
        self.headerLabel.setLetterSpacing(value: 1.8)
        self.addButton.setImage(#imageLiteral(resourceName: "largeRigthArrow").withRenderingMode(.alwaysTemplate), for: .normal)
        self.addButton.tintColor = COLOR.themeForegroundColor
        self.addButton.tag = index
    }
    
    func updateAddButton(sections:[DETAIL_SECTION], section:Int, countBeforeCustomFields:Int) {
        if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
            switch sections[section] {
            case .checkbox:
                self.unhideAddButton()
                self.addButton.isUserInteractionEnabled = false
                self.setCheckboxSelection(section: section, countBeforeCustomFields: countBeforeCustomFields)
            case .checklist, .table, .invoice, .earnings:
                self.unhideAddButton()
            default:
                self.hideAddButton()
            }
        } else if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
            switch sections[section] {
            case .notes:
                let count = Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count
                if count! > 0 {
                    if (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[count! - 1].taskDescription.length)! == 0 {
                        self.hideAddButton()
                    } else {
                        self.unhideAddButton()
                    }
                } else {
                    self.unhideAddButton()
                }
            case .barcode:
                let count = Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count
                if count! > 0 {
                    if (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?[count! - 1].taskDescription.length)! == 0 {
                        self.hideAddButton()
                    } else {
                        self.unhideAddButton()
                    }
                } else {
                    self.unhideAddButton()
                }
            case .checkbox:
                self.unhideAddButton()
                self.setCheckboxSelection(section: section, countBeforeCustomFields: countBeforeCustomFields)
            default:
                self.unhideAddButton()
            }
        } else {
            switch sections[section] {
            case .checkbox:
                self.unhideAddButton()
                self.addButton.isUserInteractionEnabled = false
                self.setCheckboxSelection(section: section, countBeforeCustomFields: countBeforeCustomFields)
            case .checklist, .table, .invoice, .earnings:
                self.unhideAddButton()
            default:
                self.hideAddButton()
            }
        }
        switch sections[section] {
        case .barcode, .signature, .image, .jobDetails, .notes, .checklist, .checkbox, .table, .customImage, .invoice, .earnings:
            break
        default:
            self.hideAddButton()
        }
    }
    
    func unhideAddButton() {
        self.addButton.isHidden = false
        self.buttonWidthConstraint.constant = self.defaultButtonWidth
        self.addButton.isUserInteractionEnabled = true
    }
    
    func hideAddButton() {
        self.addButton.isHidden = true
        self.buttonWidthConstraint.constant = 0.0
    }
    
    func setCheckboxSelection(section:Int, countBeforeCustomFields:Int) {
        if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - countBeforeCustomFields].data == "0" || Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - countBeforeCustomFields].data == "false" || Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - countBeforeCustomFields].data == "") {
            self.addButton.isSelected = false
        } else {
            self.addButton.isSelected = true
        }
    }
}
