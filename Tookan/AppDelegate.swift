
//
//  AppDelegate.swift
//  Tookan
//
//  Created by Click Labs on 7/6/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import Fabric
import Crashlytics
import AudioToolbox
import CoreData
import Firebase
import UserNotifications
import SDKDemo1

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var locationTracker: LocationTracker!
    var navigationController:UINavigationController!
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        navigationController = self.window?.rootViewController as! UINavigationController
        /*------------ Google -------------*/
        GMSServices.provideAPIKey(APIKeyForGoogleMaps)
        
        /*------------- Flurry ------------*/
        /*Flurry.startSession(FLURRY_KEY)
         Flurry.setCrashReportingEnabled(true)
         Flurry.logEvent("Start Application")*/
        
        /*------------ Fabric/Crashlytics ----------*/
        Fabric.with([Crashlytics.self()])
        
        /*---------- FireBase --------------*/
        FirebaseApp.configure()
        
        /*------------ NewRelic ------------*/
        //NewRelicAgent.startWithApplicationToken("AA4b88847059348c5ac4ef3475327826d994ee630a")
        
        
        /*=========== Set Default Value of Application =========*/
        Singleton.sharedInstance.setDefaultValuesOfApplication()
        
        /*============ Set Location Tracker ============*/
        self.locationTracker = LocationTracker.sharedInstance()
        if(launchOptions != nil) {
            self.locationTracker.checkAuthorizationStatus(launchOptions!)
        } else {
            self.locationTracker.checkAuthorizationStatus(nil)
        }
        
        /*======= Monitor Battery ==========*/
        UIDevice.current.isBatteryMonitoringEnabled = true
        /*========= White Status Bar ==========*/
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        /*===========Clear Badge Icon ============*/
        UIApplication.shared.applicationIconBadgeNumber = 0
        /*======== Cancel all local Notification =========*/
        UIApplication.shared.cancelAllLocalNotifications()
        
        /*=========== Register Push Notification =============*/
        if #available(iOS 10.0, *) {
            if #available(iOS 11.0, *) {
                UNUserNotificationCenter.current().delegate = self
            }
            // request permissions
            UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) {
                (granted, error) in
                if (granted) {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        } else { // Fallback on earlier versions
            DispatchQueue.main.async {
                let pushSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound],categories: nil)
                UIApplication.shared.registerUserNotificationSettings(pushSettings)
                application.registerUserNotificationSettings(pushSettings)
                application.registerForRemoteNotifications()
            }
        }
    
        Singleton.sharedInstance.setFuguTheme()
        if let lauchOptionInfo = launchOptions, let _ = lauchOptionInfo[UIApplicationLaunchOptionsKey.remoteNotification] as? [String: Any] {
            Singleton.sharedInstance.fromPush = true
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        DispatchQueue.main.async {
            application.registerForRemoteNotifications()
        }
    }
    
    @available(iOS 10.0, *)
    func registerForPushNotificationsButtons() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            guard granted else {
                return
            }
            let accept = UNNotificationAction(identifier: NOTIFICATION.action.accept, title: BUTTON_TITLE.accept, options: [.destructive])
            let decline = UNNotificationAction(identifier: NOTIFICATION.action.decline, title: BUTTON_TITLE.decline, options: [.destructive])
            let category1 = UNNotificationCategory(identifier: NOTIFICATION.category.newTaskAcceptDecline, actions: [accept,decline], intentIdentifiers: [], options: [])
            
            let okAction = UNNotificationAction(identifier: NOTIFICATION.action.ok, title: TEXT.OK, options: [.destructive])
            let category2 = UNNotificationCategory(identifier: NOTIFICATION.category.newTaskNone, actions: [okAction], intentIdentifiers: [], options: [])
            
            let acknowledgeAction = UNNotificationAction(identifier: NOTIFICATION.action.acknowledge, title: BUTTON_TITLE.acknowledge, options: [.destructive])
            let category3 = UNNotificationCategory(identifier: NOTIFICATION.category.newTaskAcknowledge, actions: [acknowledgeAction], intentIdentifiers: [], options: [])
            
            let openAction = UNNotificationAction(identifier: NOTIFICATION.action.open, title: TEXT.OPEN, options: [.foreground])
            let category4 = UNNotificationCategory(identifier: NOTIFICATION.category.newTaskOpen, actions: [openAction,decline], intentIdentifiers: [], options: [])
            
            
            UNUserNotificationCenter.current().setNotificationCategories([category1, category2, category3, category4])
            UNUserNotificationCenter.current().delegate = self
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = NSString(format: "%@", deviceToken as CVarArg) as String
        let characterSet: CharacterSet = CharacterSet( charactersIn: "<>" )
        let finalToken: String = (deviceTokenString)
            .trimmingCharacters( in: characterSet )
            .replacingOccurrences( of: " ", with: "" ) as String
        UserDefaults.standard.set(finalToken, forKey: USER_DEFAULT.deviceToken)
        FuguConfig.shared.registerDeviceToken(deviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error){
        UserDefaults.standard.set("No deviceToken", forKey: USER_DEFAULT.deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
        self.didReceivedRemoteNotificationData(userInfo: userInfo)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        self.didReceivedRemoteNotificationData(userInfo: notification.request.content.userInfo)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let userInfo = response.notification.request.content.userInfo as? [String: Any] {
            if FuguConfig.shared.isFuguNotification(userInfo: userInfo) {
                FuguConfig.shared.handleRemoteNotification(userInfo: userInfo)
                return
            }
        }

        
        let identifier = response.actionIdentifier
        let userInfo = response.notification.request.content.userInfo
        let notificationInfo = userInfo as! [String:AnyObject]
        let notificationData = NotificationDataObject(json: notificationInfo)
        
        switch(notificationData.flag!) {
        case FLAG_TYPE.newTask:
            Singleton.sharedInstance.handlingNewTaskPush(notificationData: notificationData, fromAcceptTaskNotification:true)
            break
        default:
            break
        }
        guard Singleton.sharedInstance.getAccessToken().length > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INVALID_ACCESS_TOKEN, isError: .error)
            return
        }
        
        guard IJReachability.isConnectedToNetwork() == true else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            return
        }
        
        guard NetworkingHelper.sharedInstance.newTaskListArray.count > 0 else {
            return
        }
        
        let task = NetworkingHelper.sharedInstance.newTaskListArray[0][0]
        var jobStatus = 0
        switch identifier {
        case NOTIFICATION.action.accept, NOTIFICATION.action.acknowledge, NOTIFICATION.action.open:
            jobStatus = JOB_STATUS.accepted
            break
        case NOTIFICATION.action.decline:
            jobStatus = JOB_STATUS.declined
            break
        case NOTIFICATION.action.ok:
            jobStatus = JOB_STATUS.okAcceptStatus
            break
        default:
            break
        }
        
        self.sendRequestToAcceptDeclineJobStatus(jobId: task.jobId, status: jobStatus, vertical: task.vertical!)
    }
    
    func sendRequestToAcceptDeclineJobStatus(jobId:Int, status:Int, vertical:Int) {
        NetworkingHelper.sharedInstance.newTaskListArray.remove(at: 0)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var params:[String:Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["job_id"] = jobId
        params["job_status"] = status
        params["reason"] = ""
        params["lat"] = lat
        params["lng"] = lng
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.changeJobStatus, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    let statusCode = response["status"] as! Int
                    switch statusCode {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        NotificationCenter.default.removeObserver(self)
                        break
                        
                    case STATUS_CODES.SHOW_DATA:
                        break
                    default:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func sendNotificationThroughUDP(notification:[String:Any]){
        guard Singleton.sharedInstance.fleetDetails != nil else {
            return
        }
        guard Singleton.sharedInstance.fleetDetails.isUDPEnabled == 1 else {
            return
        }
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil) {
            print(notification)
            let udpData = ["flag":0, "data":notification, "fleet_id":Singleton.sharedInstance.fleetDetails.fleetId!, "device_type":DEVICE_TYPE] as [String : Any]
            let client:UDPClient = UDPClient(addr: IP_ADDRESS, port: PORT)
            _ = client.send(str: udpData.jsonString)
            _ = client.close()
        }
    }
    
    func didReceivedRemoteNotificationData(userInfo:[AnyHashable : Any]) {
        let pushInfo = (userInfo as? [String : Any]) ?? [:]
        guard let _ = Singleton.sharedInstance.fleetDetails else {
            Singleton.sharedInstance.pushDataOfFugu = pushInfo
            return
        }
//
//        guard let pushData = pushInfo else {
//            Singleton.sharedInstance.showErrorMessage(error:ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
//            return
//        }
        
        self.sendNotificationThroughUDP(notification: userInfo as! [String:Any])
        DispatchQueue.main.async {
            
            if FuguConfig.shared.isFuguNotification(userInfo: pushInfo) {
                if let notificationInfo = userInfo as? [String:AnyObject] {
                        NetworkingHelper.sharedInstance.notificationArray.removeAll()
                        let notificationData = NotificationDataObject(json: notificationInfo)
                        NetworkingHelper.sharedInstance.notificationArray.append(notificationData)
                        if let aps = notificationInfo["aps"] as? NSDictionary{
                            if let alert = aps["alert"] as? [String: Any]{
                                if let title = alert["title"] as? String {
                                    notificationData.fuguTitle = title
                                }
                                
                                if let body = alert["body"] as? String {
                                    notificationData.fuguBody = body
                                }
                            }
                        }
                        guard NetworkingHelper.sharedInstance.notificationArray.count > 0 else {
                            return
                        }
                        Singleton.sharedInstance.showPushBannerforFugu(notification: NetworkingHelper.sharedInstance.notificationArray[0])
                }
                return
            }
            
            if(UIApplication.shared.applicationState == UIApplicationState.inactive || UIApplication.shared.applicationState == UIApplicationState.background) {
                let aps = (userInfo as NSDictionary)["aps"] as! NSDictionary
                if(aps["content-available"] != nil) {
                    if(aps["content-available"] as! Int == 1) {
                        if(self.locationTracker == nil) {
                            self.locationTracker = LocationTracker.sharedInstance()
                        }
                        let notificationInfo = userInfo as! [String:Any]
                        if let value = notificationInfo["battery_usage"] as? Int {
                            UserDefaults.standard.setValue(value, forKey: USER_DEFAULT.batteryUsage)
                        } else if let value = notificationInfo["battery_usage"] as? String {
                            UserDefaults.standard.setValue(Int(value)!, forKey: USER_DEFAULT.batteryUsage)
                        }
                        
                        /*======= SET MODE ==========*/
                        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
                        case BATTERY_USAGE.high, BATTERY_USAGE.low, BATTERY_USAGE.medium:
                            self.locationTracker.updateLastSavedLocationOnServer()
                            break
                        default:
                            self.locationTracker.restartLocationUpdates()
                            break
                        }
                        /*==========================================*/
                    }
                }
                
            } else {
                self.handlePushNotificationGeneralCase(userInfo as [NSObject : AnyObject])
            }
        }
    }
    
    func handlePushNotificationGeneralCase(_ userInfo: [NSObject : AnyObject]){
        print(userInfo)
        if let notificationInfo = userInfo as? [String:AnyObject] {
            let notificationData = NotificationDataObject(json: notificationInfo)
            switch(notificationData.flag!) {
            case FLAG_TYPE.none:
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotification), object: self)
                break
            case FLAG_TYPE.silent:
                /*======= SET MODE ==========*/
                switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
                case BATTERY_USAGE.high, BATTERY_USAGE.low, BATTERY_USAGE.medium:
                    self.locationTracker?.updateLastSavedLocationOnServer()
                    break
                default:
                    break
                }
                /*==========================================*/
                break
            case FLAG_TYPE.updateLocation:
                break
            case FLAG_TYPE.putOffDuty:
                guard Singleton.sharedInstance.fleetDetails != nil else {
                    return
                }
                if notificationData.isAvailable == 1 {
                    Singleton.sharedInstance.fleetDetails.isAvailable = DutyStatus.onDuty
                    UserDefaults.standard.set(DutyStatus.onDuty, forKey: USER_DEFAULT.userAvailableStatus)
                } else {
                    Singleton.sharedInstance.fleetDetails.isAvailable = DutyStatus.offDuty
                    UserDefaults.standard.set(DutyStatus.offDuty, forKey: USER_DEFAULT.userAvailableStatus)
                }
                Singleton.sharedInstance.showErrorMessage(error: notificationData.message!, isError: .message)
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.putOffDuty), object: nil)
                break
                
            case FLAG_TYPE.unableToUploadLocation:
                Singleton.sharedInstance.showErrorMessage(error: notificationData.message!, isError: .error)
                break
            case FLAG_TYPE.batteryUsage:
                UserDefaults.standard.setValue(notificationData.battery_usage, forKey: USER_DEFAULT.batteryUsage)
                locationTracker.restartLocationUpdates()
                break
                
            case FLAG_TYPE.deleteTask,
                 FLAG_TYPE.reschedule,
                 FLAG_TYPE.taskUpdatedStatus,
                 FLAG_TYPE.upcoming,
                 FLAG_TYPE.alertMessage:
                self.handlePushForBanner(notificationData: notificationData)
                break
            case FLAG_TYPE.newTask:
                switch notificationData.accept_button! {
                case ACCEPT_TYPE.startCancel:
                    self.handlePushForBanner(notificationData: notificationData)
                    break
                default:
                    Singleton.sharedInstance.handlingNewTaskPush(notificationData: notificationData, fromAcceptTaskNotification:false)
                }
                break
            case FLAG_TYPE.signupVerificationCode:
                if Singleton.sharedInstance.fleetDetails == nil {
                    Singleton.sharedInstance.fleetDetails = FleetInfoDetails(json: [:])
                }
                Singleton.sharedInstance.fleetDetails.registrationStatus = notificationData.registrationStatus
                Singleton.sharedInstance.fleetDetails.admin_action_message = notificationData.admin_action_message
                let vibrationsEnabled = UserDefaults.standard.value(forKey: USER_DEFAULT.vibration) as! Bool
                if vibrationsEnabled{
                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                }
                NetworkingHelper.sharedInstance.serverTime = "0"
                notificationData.d = Auxillary.getUTCDateString()
                NetworkingHelper.sharedInstance.notificationArray.append(notificationData)
                Singleton.sharedInstance.lastNotificationFlagType = notificationData.flag!
                Singleton.sharedInstance.showPushBanner(notification: NetworkingHelper.sharedInstance.notificationArray[0])
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.signupVerificationPushReceived), object: self)
                break
            default:
                break
            }
        }
    }
    
    func handlePushForBanner(notificationData:NotificationDataObject) {
        let vibrationsEnabled = UserDefaults.standard.value(forKey: USER_DEFAULT.vibration) as! Bool
        if vibrationsEnabled{
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        }
        NetworkingHelper.sharedInstance.serverTime = "0"
        notificationData.d = Auxillary.getUTCDateString()
        NetworkingHelper.sharedInstance.notificationArray.append(notificationData)
        NetworkingHelper.sharedInstance.notificationJobId = notificationData.job_id!
        Singleton.sharedInstance.lastNotificationFlagType = notificationData.flag!
        
        if(NetworkingHelper.sharedInstance.syncView == nil) {
            NetworkingHelper.sharedInstance.notificationCount += 1
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotificationCount), object: self)
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotification), object: self)
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.getAllNotification), object: self)
        }
        guard NetworkingHelper.sharedInstance.notificationArray.count > 0 else {
            return
        }
        Singleton.sharedInstance.showPushBanner(notification: NetworkingHelper.sharedInstance.notificationArray[0])
    }
    
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        print("applicationDidReceiveMemoryWarning")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        //self.locationTracker.appEnterInBackground()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        UserDefaults.standard.setValue("Background", forKey: USER_DEFAULT.applicationMode)
        self.locationTracker.appEnterInBackground()
        self.window?.endEditing(true)
        Singleton.sharedInstance.shouldPerformActionFor()
    }
    
    
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
        guard DEVICE_TYPE == "1" || DEVICE_TYPE == "3" else {
            Singleton.sharedInstance.isNotificationEnabled(completion: { (isEnabled) in
                if isEnabled == true {
                    if Singleton.sharedInstance.getDeviceToken() == "NoDeviceToken" {
                        DispatchQueue.main.async(execute: { () -> Void in
                            Auxillary.logoutFromDevice()
                            NotificationCenter.default.removeObserver(self)
                        })
                    } else {
                        self.enteringInForeground()
                    }
                } else {
                    self.enteringInForeground()
                }
            })
            return
        }
        self.enteringInForeground()
    }
    
    func enteringInForeground() {
        self.window?.endEditing(true)
        UIApplication.shared.applicationIconBadgeNumber = 0
        UIApplication.shared.cancelAllLocalNotifications()
        UserDefaults.standard.setValue("Foreground", forKey: USER_DEFAULT.applicationMode)
        self.locationTracker.enterInForegroundFromBackground()
        self.getAllNotification()
    }
    

    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        guard self.locationTracker != nil else {
            self.locationTracker = LocationTracker.sharedInstance()
            return
        }
        if(self.locationTracker.locationManager == nil) {
            self.locationTracker?.restartLocationUpdates()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //   triggerLocalNotification(CGFloat(300))
        UserDefaults.standard.setValue("Terminate", forKey: USER_DEFAULT.applicationMode)
        if(self.locationTracker == nil) {
            self.locationTracker = LocationTracker.sharedInstance()
        }
        self.locationTracker.appEnterInTerminateState()
    }
    
    func getAllNotification() {
        guard Singleton.sharedInstance.fleetDetails != nil else {
            return
        }
        
        if let controller = self.navigationController?.visibleViewController {
            let controller = NSStringFromClass(controller.classForCoder).components(separatedBy: ".").last!
            switch controller {
            case STORYBOARD_ID.signupViewController, STORYBOARD_ID.otpController, STORYBOARD_ID.verificationStateController, STORYBOARD_ID.signupTemplateController:
                break
            default:
                NetworkingHelper.sharedInstance.notificationArray.removeAll()
                if let accessToken = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as? String {
                    //if self.navigationController.visibleViewController == STORYBOARD_ID.detailController {
                    ActivityIndicator.sharedInstance.showActivityIndicator()
                    //}
                    let params = [
                        "access_token":accessToken,
                        "fleet_id":Singleton.sharedInstance.fleetDetails.fleetId!,
                        "flag":"unread"
                        ] as [String : Any]
                    NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.viewAllNotification, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                        DispatchQueue.main.async {
                            if isSucceeded == true {
                                switch(response["status"] as! Int) {
                                case STATUS_CODES.SHOW_DATA:
                                    if let responseData = response["data"] as? NSDictionary {
                                        if let count = responseData["count"] as? Int {
                                            NetworkingHelper.sharedInstance.notificationCount = count
                                            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotificationCount), object: self)
                                        }
                                        if let serverTime = responseData["server_time"] as? String {
                                            NetworkingHelper.sharedInstance.serverTime = serverTime
                                        }
                                        if let notificationData = responseData["data"] as? NSArray {
                                            for item in notificationData {
                                                NetworkingHelper.sharedInstance.notificationArray.append(NotificationDataObject(json: item as! [String:Any]))
                                            }
                                            DispatchQueue.main.async(execute: {
                                                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotification), object: self)
                                                Singleton.sharedInstance.handlingPushAfterGettingAllNotification()
                                            })
                                        } else {
                                            ActivityIndicator.sharedInstance.hideActivityIndicator()
                                        }
                                    } else {
                                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                                    }
                                    break
                                case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                                    NotificationCenter.default.removeObserver(self)
                                    Auxillary.logoutFromDevice()
                                    break
                                default:
                                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                                    break
                                }
                            } else {
                                ActivityIndicator.sharedInstance.hideActivityIndicator()
                                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                            }
                        }
                    })
                }
                break
            }
        }
    }
    
    //MARK: Keyboard Functions
    func keyboardWillShow(_ notification : Foundation.Notification){
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue //info.value(forKey:
        Singleton.sharedInstance.keyboardSize = value.cgRectValue.size
        Singleton.sharedInstance.translateErrorMessage(toBottom: false)
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        Singleton.sharedInstance.keyboardSize = CGSize(width: 0.0, height: 0.0)
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        Singleton.sharedInstance.forceTouch = true
        Singleton.sharedInstance.shouldPerformActionFor()
        completionHandler(true)
    }
}

