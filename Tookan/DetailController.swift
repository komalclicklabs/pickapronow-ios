//
//  DetailController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 22/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Crashlytics
import AVFoundation
import MessageUI
import DGActivityIndicatorView
import MessageUI
import AssetsLibrary

protocol DetailControllerDelegate {
    func updateForceTouch()
//    func dismissDetailVC()
}

class DetailController: UIViewController, MFMailComposeViewControllerDelegate {
    var delegate:DetailControllerDelegate!
    var jobDetailsView:JobDetailView!
    var signatureView:SignatureView!
    var panDirection:SwipeDirection!
    var navigation:navigationBar!
    var internetToast:NoInternetConnectionView!
    var model:DetailModel!
    let offlineModel = OfflineDataModel()
    let statusModel = StatusModel()
    var imagesPreview:ImagesPreview!
    var scanView:ScanView!
    var customPicker:CustomDatePicker!
    var drowDownWithSearch:TemplateController!
    let getImagePicker = GetImageFromPicker()
    var imageCaptionViewController:ImageCaptionViewController!
    var slider:CustomSlider!
    
    var callback : ((String) -> Void)?
    var drawPathClosuer:((String) -> Void)?
    var updateMarkerClosure:((String) -> Void)?
    
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    let leftRightMargin:CGFloat = 5.0
    let jobOrigin:CGFloat = 75.0//getAspectRatioValue(value: 80.0)//177.0
    let maxTranslatingPointToMinimize:CGFloat = getAspectRatioValue(value: 100.0)//150
    var minimizeOrigin:CGFloat = getAspectRatioValue(value: SCREEN_SIZE.height - getAspectRatioValue(value: 288))//SCREEN_SIZE.height - 288
    let keyboardMargin:CGFloat = 30.0
    let defaultBottomConstraint:CGFloat = 70.0//200.0
    let defaultHeightOfButtons:CGFloat = 60.0
    var signatureDetailViewOrigin:CGFloat = SCREEN_SIZE.height
    let unsyncedObjectContext = DatabaseManager.sharedInstance.managedObjectContext
    var activityIndicator:DGActivityIndicatorView!
    var lastContentOffset:CGPoint = CGPoint(x: 0, y: 0)
    var jobId:Int!
    var pickupDeliveryRelationship:String!
    var directionButton:UIButton!
    let sliderHeight:CGFloat = 70.0
    var fromForceTouch = false
    
    @IBOutlet var transLayer: UIView!
    @IBOutlet var positiveStatusButton: UIButton!
    @IBOutlet var negativeStatusButton: UIButton!
    @IBOutlet var moreOptionButton: UIButton!
    @IBOutlet var waitingView: UIView!
    @IBOutlet var waitingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.transLayer.alpha = 0.0
        self.signatureDetailViewOrigin = SCREEN_SIZE.height - jobOrigin - 40
        if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
            self.minimizeOrigin = SCREEN_SIZE.height - jobOrigin - self.defaultHeightOfButtons - getAspectRatioValue(value: 50)
        } else {
            self.minimizeOrigin = SCREEN_SIZE.height - jobOrigin - self.defaultHeightOfButtons - getAspectRatioValue(value: 150)
        }
        self.model = DetailModel()
        self.model.notesDelegate = self
        self.statusModel.delegate = self
        self.setNavigationBar()
        self.setJobDetailView()
        
        /*========= Tap Gesture ===========*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.moreOptionAction(_:)))
        tap.numberOfTapsRequired = 1
        self.transLayer.addGestureRecognizer(tap)
        
        guard Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 else {
            return
        }
        guard Singleton.sharedInstance.selectedTaskDetails.hideCancelOptionalField.value == 0 else {
            return
        }
        self.showStatusButtonsOnCountBased()
     }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusButton()
        if self.fromForceTouch == false {
            if self.jobDetailsView.transform.ty == SCREEN_SIZE.height {
                self.jobDetailsView.alpha = 1
                self.translateViewToPoint(point: 0, translatingView: self.jobDetailsView)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadImageSection), name: NSNotification.Name(rawValue: OBSERVER.reloadTaskTableForImageSection), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDetailTable), name: NSNotification.Name(rawValue: OBSERVER.reloadDetailTable), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.reloadTaskTableForImageSection), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.reloadDetailTable), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func showStatusButtonsOnCountBased() {
        /*======================== Show Status Button on Count based ==============================*/
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
        case JOB_STATUS.accepted, JOB_STATUS.arrived, JOB_STATUS.assigned, JOB_STATUS.started:
            let count = UserDefaults.standard.integer(forKey: USER_DEFAULT.buttonCountOnTaskDetailToShow)
            if count <= 10 {
                self.perform(#selector(self.moreOptionAction(_:)), with: nil, afterDelay: 0.1)
                self.perform(#selector(self.hideTransLayer), with: nil, afterDelay: 2.5)
                UserDefaults.standard.set(count + 1, forKey: USER_DEFAULT.buttonCountOnTaskDetailToShow)
            }
        default:
            break
        }
    }
    
    func reloadImageSection() {
        DispatchQueue.main.async {
            UIView.setAnimationsEnabled(false)
            if let section = self.model.sections.index(of: DETAIL_SECTION.image) {
                guard self.jobDetailsView.detailsTable.isSectionExist(section: section) == true else {
                    return
                }
                if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row: 0, section: section)) as? ImageTableCell {
                    cell.imageCollectionView.reloadData()
                    guard (Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.count)! > 0 else {
                        self.jobDetailsView.detailsTable.reloadSections(IndexSet(integer: section), with: .none)
                        return
                    }
                    cell.imageCollectionView.scrollToItem(at: IndexPath(item:Singleton.sharedInstance.selectedTaskDetails.addedImagesArray!.count - 1,section: 0), at: UICollectionViewScrollPosition.right, animated: true)
                } else {
                    if let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: section) as Int? {
                        if numberOfRows == 0 {
                            self.jobDetailsView.detailsTable.reloadSections(IndexSet(integer: section), with: .none)
                        }
                    }
                }
            }
            UIView.setAnimationsEnabled(true)
        }
    }
    
    func reloadDetailTable() {
        self.setNavigationBar()
        self.navigation.alpha = 1
        self.navigation.changeHamburgerButton()
        self.setStatusButton()
        self.directionButton.alpha = 0.0
        self.translateViewToPoint(point: 0, translatingView: self.jobDetailsView)
    }

    public func updateView() {
        if Singleton.sharedInstance.selectedTaskDetails.vertical == VERTICAL.TAXI.rawValue {
            guard Singleton.sharedInstance.selectedTaskDetails != nil else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                return
            }
            guard Singleton.sharedInstance.selectedTaskDetails.vertical == VERTICAL.TAXI.rawValue else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                return
            }
            guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray != nil && (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > 0 else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                return
            }
            
            self.gotoTaxiController(index: 0)
        } else {
            self.setStatusButton()
        }
    }
    
    func receivedDeleteTaskNotification() {
        if let presented = self.presentedViewController {
            presented.dismiss(animated: false, completion: { finished in
                self.backAction()
            })
        } else {
            self.backAction()
        }
    }
    
    func setDirectionButton() {
        if directionButton == nil {
            directionButton = UIButton()
            directionButton.alpha = 0
            directionButton.frame = CGRect(x: SCREEN_SIZE.width - 75, y:jobOrigin + minimizeOrigin - 25 + Singleton.sharedInstance.getTopInsetofSafeArea(), width: 50, height: 50)
            directionButton.backgroundColor = COLOR.themeForegroundColor
            directionButton.setImage(#imageLiteral(resourceName: "getDirectionsLarge"), for: .normal)
            directionButton.layer.cornerRadius = 25
            directionButton.layer.masksToBounds = true
            directionButton.addTarget(self, action: #selector(self.directionAction), for: .touchUpInside)
            self.view.addSubview(directionButton)
        }
    }
    
    func directionAction() {
        self.directionButton.isEnabled = false
        self.perform(#selector(self.enableDirectionButton), with: nil, afterDelay: 0.5)
        self.model.tappedOnMapNavigation()
    }
    
    func enableDirectionButton() {
        self.directionButton.isEnabled = true
    }
    
    func setStatusButton() {
        if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            self.setSliderButton()
        } else {
            self.view.bringSubview(toFront: self.transLayer)
            self.view.bringSubview(toFront: self.positiveStatusButton)
            self.view.bringSubview(toFront: self.negativeStatusButton)
            self.view.bringSubview(toFront: self.moreOptionButton)
            self.view.bringSubview(toFront: self.waitingView)
            self.view.bringSubview(toFront: self.waitingLabel)
            
            self.positiveStatusButton.backgroundColor = COLOR.themeForegroundColor
            self.positiveStatusButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
            self.positiveStatusButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
            self.negativeStatusButton.backgroundColor = COLOR.negativeButtonBackgroundColor
            self.negativeStatusButton.setTitleColor(COLOR.negativeButtonTitleColor, for: .normal)
            self.negativeStatusButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
            self.waitingLabel.text = TEXT.PLEASE_WAIT
            self.waitingLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
            self.waitingLabel.setLetterSpacing(value: 1.8)
            self.waitingLabel.textColor = COLOR.TEXT_COLOR
            self.activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballPulse, tintColor: COLOR.TEXT_COLOR, size: 30.0)
            self.activityIndicator.clipsToBounds = true
            self.waitingView.addSubview(self.activityIndicator)
            
            self.statusModel.setJobStatusAndButtonTitle()
            if(self.jobDetailsView != nil) {
                self.jobDetailsView.detailsTable.reloadData()
            }
        }
    }
    
    @IBAction func positiveAction(_ sender: UIButton) {
        if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
            self.acceptTask()
        } else {
            if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
                if self.statusModel.showMandatoryFieldsAlert(Singleton.sharedInstance.selectedTaskDetails) == true {
                    self.acceptTask()
                } else {
                    if self.jobDetailsView.transform.ty > 0 {
                        self.translateViewToPoint(point: 0, translatingView: self.jobDetailsView)
                    }
                    self.jobDetailsView.detailsTable.reloadData()
                }
            } else {
                self.acceptTask()
            }
        }
    }
    
    func acceptTask() {
        self.dismissPartial()
        self.statusModel.positiveStatusAction()
    }
    
    @IBAction func negativeAction(_ sender: UIButton) {
        if self.statusModel.negativeStatusAction((sender.titleLabel?.text)!) == false {
            self.animationForBottomView()
            if self.jobDetailsView.transform.ty > 0 {
                self.directionButton.alpha = 0.0
                self.translateViewToPoint(point: 0, translatingView: self.jobDetailsView)
            }
        }
    }
    
    @IBAction func moreOptionAction(_ sender: Any) {
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
            if self.transLayer.alpha == 1.0 {
                self.transLayer.alpha = 0.0
                self.positiveStatusButton.transform = CGAffineTransform.identity
                self.negativeStatusButton.transform = CGAffineTransform.identity
                self.moreOptionButton.transform = CGAffineTransform.identity
            } else {
                self.transLayer.alpha = 1.0
                self.positiveStatusButton.transform = CGAffineTransform(translationX: 0, y: -self.positiveStatusButton.frame.height)
                self.negativeStatusButton.transform = CGAffineTransform(translationX: 0, y: -self.positiveStatusButton.frame.height)
                self.moreOptionButton.transform = CGAffineTransform(translationX: 0, y: -self.positiveStatusButton.frame.height)
            }
        }, completion: { finished in })
    }
    
    func hideTransLayer() {
        if self.transLayer.alpha == 1.0 {
            self.moreOptionAction(self.moreOptionButton)
        }
    }
    
    func gotoTaxiController(index:Int) {
        let taxiController = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.taxiController) as! TaxiController
        self.navigationController?.pushViewController(taxiController, animated: true)
        Singleton.sharedInstance.removeExtraControllerFromStack()
    }
    
    //MARK: SET JOB DETAIL VIEW
    func setJobDetailView() {
        if jobDetailsView == nil {
            self.jobDetailsView = UINib(nibName: NIB_NAME.jobDetailView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! JobDetailView
            self.jobDetailsView.delegate = self
            self.jobDetailsView.model = self.model
            self.jobDetailsView.setDetailTable()
            let yPos = HEIGHT.navigationHeight + 10
            self.jobDetailsView.frame = CGRect(x: leftRightMargin, y: yPos, width: SCREEN_SIZE.width - (leftRightMargin * 2), height: SCREEN_SIZE.height - yPos + 20 - Singleton.sharedInstance.getBottomInsetofSafeArea())
            self.jobDetailsView.layer.cornerRadius = 14.0
            self.jobDetailsView.layer.masksToBounds = true
            if self.fromForceTouch == false {
                self.jobDetailsView.alpha = 0
                self.translateViewToPoint(point: SCREEN_SIZE.height, translatingView: self.jobDetailsView)
            } else {
                self.jobDetailsView.slideButton.transform = CGAffineTransform(rotationAngle: .pi)
            }
            self.view.addSubview(jobDetailsView)
        }
        self.setDirectionButton()
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.valueChanged(_:)))
        panGesture.delegate = self
        self.jobDetailsView.addGestureRecognizer(panGesture)
    }
    
    //MARK: Animate Card
    func translateViewToPoint(point:CGFloat, translatingView:UIView) {
        self.jobDetailsView.detailsTable.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.curveLinear, animations: {
            if point == 0 {
                self.model.showListCell = false
                self.jobDetailsView.detailsTable.reloadData()
                self.directionButton.alpha = 0.0
                translatingView.transform = CGAffineTransform.identity
                self.jobDetailsView.slideButton.transform = CGAffineTransform(rotationAngle: .pi)
            } else {
                self.model.showListCell = true
                self.jobDetailsView.detailsTable.reloadData()
                translatingView.transform = CGAffineTransform(translationX: 0, y: point)
                self.jobDetailsView.slideButton.transform = CGAffineTransform.identity
                if self.jobDetailsView.transform.ty != SCREEN_SIZE.height {
                    self.drawPathClosuer?("DrawPath")
                    if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false {
                        if self.signatureView == nil {
                            self.directionButton.alpha = 1.0
                        }
                    }
                }
            }
        }, completion: { finished in
            self.changeTableViewState(status: point == 0 ? true : false, jobView: translatingView)
        })
    }
    
    //MARK: Get Pan Direction
    func getJobViewDirection(jobView:UIView) -> SwipeDirection {
        if jobView.transform.ty == 0 {
            return .topToBottom
        } else {
            self.model.showListCell = false
            self.jobDetailsView.detailsTable.reloadData()
            return .bottomToTop
        }
    }
    
    //MARK: Set Table Properties
    func changeTableViewState(status:Bool, jobView:UIView) {
        if jobView == self.jobDetailsView {
            self.jobDetailsView.detailsTable.isScrollEnabled = status
        }
    }
    
    //MARK: Upload Customfield Image
    func uploadCustomFieldImageToServer(_ tag:Int,imagePath:String, image:UIImage,caption:String) {
        if IJReachability.isConnectedToNetwork() == true {
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].imageArray.add(imagePath)
            self.model.reloadParticularSection(index: tag + self.model.countBeforeCustomFields)
            var dict = ["image_url":imagePath]
            dict["caption_data"] = caption
            let captionObject = Caption(json: dict)
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].captionArray?.append(captionObject)
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].fleetData = "Uploading"
            if(self.offlineModel.uploadUnsyncedTaskToServer() == false) {
                if(offlineModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                    self.showInvalidAccessTokenPopup(offlineModel.hitFailMessage)
                } else {
                    self.alertForSyncingFailed("", textToSend: "", type: .custom_image_added, image: image, imagePath: imagePath,caption:caption, tag: tag)
                }
            } else {
                self.sendRequestToUploadCustomFieldImage(tag, imagePath: imagePath, image: image, caption:caption)
            }
        } else {
            guard Singleton.sharedInstance.isAppSyncingEnable() == true else {
                return
            }
            self.saveOfflineDataForCustomFieldImage(imagePath, tag: tag, image: image, caption: caption)
        }
    }
    
    func sendRequestToUploadCustomFieldImage(_ tag:Int,imagePath:String, image:UIImage, caption:String) {
        NetworkingHelper.sharedInstance.updateCustomFieldWithImage(UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String, custom_field_label: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].label, job_id: Singleton.sharedInstance.selectedTaskDetails.jobId!, data: "", image: image, caption:caption, customFieldValue: tag, imageArray: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].imageArray,imagePath: imagePath) { (succeeded, response) -> () in
            if(succeeded == true) {
                DispatchQueue.main.async {
                    if let jobId = response["jobId"] as? Int {
                        guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                            return
                        }
                    }
                    let index = response["Index"] as! Int
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].fleetData = "ImageAdded"
                    let json = response["json"] as! [String:AnyObject]
                    if let dataFromServer:AnyObject = json["data"]  {
                        if(dataFromServer.isKind(of: NSString.classForCoder()) == true) {
                            let storedImagePath = response["imagePath"] as! String
                            if let indexItem = self.model.getCustomImageIndexFromImageName(storedImagePath, imageArray: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].imageArray) {
                                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].imageArray.replaceObject(at: indexItem, with: dataFromServer)
                                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].captionArray![indexItem].caption_data = caption
                                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].captionArray![indexItem].image_url = dataFromServer as? String
                                NetworkingHelper.sharedInstance.allImagesCache.setObject(image, forKey: "\(dataFromServer)" as NSString)
                                if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row: 0, section: index + self.model.countBeforeCustomFields)) as? ImageTableCell {
                                    cell.imageCollectionView.reloadData()
                                    cell.imageCollectionView.scrollToItem(at: IndexPath(item:Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].imageArray.count - 1,section: 0), at: UICollectionViewScrollPosition.right, animated: true)
                                    Auxillary.deleteImageFromDocumentDirectory(imagePath)
                                }
                            }
                        }
                        _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                    }
                }
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SLOW_INTERNET_CONNECTION:
                        if let jobId = response["jobId"] as? Int {
                            guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                                return
                            }
                        }
                        self.saveOfflineDataForCustomFieldImage(imagePath, tag: tag, image: image, caption: caption)
                        break
                    default:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        if let jobId = response["jobId"] as? Int {
                            guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                                return
                            }
                        }
                        let index = response["Index"] as! Int
                        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].fleetData = ""
                        let storedImagePath = response["imagePath"] as! String
                        Auxillary.deleteImageFromDocumentDirectory(storedImagePath)
                        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row: 0, section: index + self.model.countBeforeCustomFields)) as? ImageTableCell {
                            cell.imageCollectionView.reloadData()
                            cell.imageCollectionView.scrollToItem(at: IndexPath(item:Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].imageArray.count - 1,section: 0), at: UICollectionViewScrollPosition.right, animated: true)
                        }
                        break
                    }
                })
            }
        }
    }
    
    //MARK: CustomField Request
    func sendRequestToServerForCustomField(_ customLabel:String, jobId:Int, customData:String, section:Int, row:Int, customFieldType:String) {
        if IJReachability.isConnectedToNetwork() == true {
            if(self.offlineModel.uploadUnsyncedTaskToServer() == false) {
                if(self.offlineModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                    self.showInvalidAccessTokenPopup(self.offlineModel.hitFailMessage)
                } else {
                    self.alertForSyncingFailedForCustomField(customLabel, jobId: jobId, customData: customData, section:section, row:row, customFieldType:customFieldType)
                }
            } else {
                self.model.sendOnlineRequestForCustomField(customLabel, jobId: jobId, customData: customData, section:section, row:row, customFieldType:customFieldType)
            }
        } else {
            self.model.saveOfflineCustomFieldData(customLabel, jobId: jobId, customData: customData, section:section, row:row, customFieldType:customFieldType)
        }
    }
    
    func sendRequestForAddTaskDetail(_ timestamp:String, textToSend:String, addedDetailType:ADDED_DETAILS_TYPE, selectedImage:UIImage, imagePath:String, caption:String) {
        NetworkingHelper.sharedInstance.addTaskDetails(Singleton.sharedInstance.getAccessToken(), text: textToSend, type: addedDetailType.rawValue, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId!, image: selectedImage,caption:caption, timeStamp:timestamp, imagePath:imagePath,  receivedResponse: { (succeeded, response) -> () in
            if(succeeded == true) {
                DispatchQueue.main.async(execute: { () -> Void in
                    let responseData = response["responseData"] as! NSDictionary
                    switch(responseData.value(forKey: "status") as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(responseData["message"] as! String)
                        break
                    default:
                        if let jobId = response["jobId"] as? Int {
                            guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                                return
                            }
                        }
                        if let type  = response["type"] as? String{
                            switch type {
                            case ADDED_DETAILS_TYPE.text_added.rawValue, ADDED_DETAILS_TYPE.text_updated.rawValue:
                                if let data  = responseData["data"] as? NSDictionary{
                                    _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                                    if let indexItem = self.model.getNoteFromDescription(response["text"] as! String, timeStamp: response["timeStamp"] as! String) {
                                        Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![indexItem].IDs = data["insertedID"] as! Int
                                        _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                                        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getNoteSectionIndex(index:indexItem)) as? NotesCell {
                                            cell.activityIndicator.stopAnimating()
                                            cell.actionButton.isHidden = false
                                            cell.descriptionView.isUserInteractionEnabled = true
                                        }
                                    }
                                }
                                break
                            case ADDED_DETAILS_TYPE.barcode_added.rawValue, ADDED_DETAILS_TYPE.barcode_updated.rawValue:
                                if let data  = responseData["data"] as? NSDictionary{
                                    _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                                    if let indexItem = self.model.getCodeFromDescription(response["text"] as! String, timeStamp: response["timeStamp"] as! String) {
                                        Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![indexItem].IDs = data["insertedID"] as! Int
                                        _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                                        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getBarcodeSectionIndex(index:indexItem)) as? NotesCell {
                                            cell.activityIndicator.stopAnimating()
                                            cell.actionButton.isHidden = false
                                            cell.descriptionView.isUserInteractionEnabled = true
                                        }
                                    }
                                }
                                break
                            case ADDED_DETAILS_TYPE.signature_image_added.rawValue:
                                ActivityIndicator.sharedInstance.hideActivityIndicator()
                                _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId,  taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                                self.hideSignatureView()
                                break
                            case ADDED_DETAILS_TYPE.image_added.rawValue:
                                Singleton.sharedInstance.selectedTaskDetails.imageUploadingStatus = false
                                var imageID = Int()
                                let data = responseData["data"] as! NSDictionary
                                imageID = data["insertedID"] as! Int
                                let storedImagePath = response["imagePath"] as! String
                                Singleton.sharedInstance.deleteImageFromDocumentDirectory(imagePath)
                                NetworkingHelper.sharedInstance.allImagesCache.removeObject(forKey: storedImagePath as NSString)
                                NetworkingHelper.sharedInstance.allImagesCache.setObject(selectedImage, forKey: "\(storedImagePath)" as NSString)
                                if let indexItem = self.model.getImageIndexFromImageName(storedImagePath,selectedTaskDetails:Singleton.sharedInstance.selectedTaskDetails) {
                                    if((Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.count)! > indexItem) {
                                        Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![indexItem].IDs = imageID
                                        self.reloadImageSection()
                                        Singleton.sharedInstance.selectedTaskDetails.addedCaptionArray?[indexItem].image_url = storedImagePath
                                    }
                                }
                                
                                //self.delegate?.updateProgressBar()
                                _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                                break
                            default:
                                break
                            }
                        }
                        break
                    }
                })
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SLOW_INTERNET_CONNECTION:
                        if let jobId = response["jobId"] as? Int {
                            guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                                return
                            }
                        }
                        if let type  = response["type"] as? String{
                            switch type {
                            case ADDED_DETAILS_TYPE.text_added.rawValue, ADDED_DETAILS_TYPE.text_updated.rawValue, ADDED_DETAILS_TYPE.barcode_added.rawValue, ADDED_DETAILS_TYPE.barcode_updated.rawValue:
                                self.saveOfflineData(type:addedDetailType)
                                break
                            case ADDED_DETAILS_TYPE.signature_image_added.rawValue:
                                self.saveOfflineData(imagePath, label:"signature_image_added",caption:"")
                            case ADDED_DETAILS_TYPE.image_added.rawValue:
                                self.saveOfflineData(imagePath, label:"",caption:caption)
                                if let indexItem = self.model.getImageIndexFromImageName(imagePath,selectedTaskDetails:Singleton.sharedInstance.selectedTaskDetails) {
                                    if((Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.count)! > indexItem) {
                                        self.reloadImageSection()
                                    }
                                }
                                break
                            default:
                                break
                            }
                        }
                        break
                    default:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        if let jobId = response["jobId"] as? Int {
                            guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                                return
                            }
                        }
                        if let type  = response["type"] as? String{
                            switch type {
                            case ADDED_DETAILS_TYPE.text_added.rawValue, ADDED_DETAILS_TYPE.text_updated.rawValue:
                                if let indexItem = self.model.getNoteFromDescription(response["text"] as! String, timeStamp: response["timeStamp"] as! String) {
                                    //Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.remove(at: indexItem)
                                    let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: self.model.sections.index(of: .notes)!)
                                    if(numberOfRows != (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)!) {
                                        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getNoteSectionIndex(index:indexItem)) as? NotesCell {
                                            cell.activityIndicator.stopAnimating()
                                            cell.actionButton.isHidden = false
                                            self.model.setAddButtonOfNotesHeaderView(status: true)
                                        }
                                    }
                                }
                                break
                            case ADDED_DETAILS_TYPE.barcode_added.rawValue, ADDED_DETAILS_TYPE.barcode_updated.rawValue:
                                if let indexItem = self.model.getCodeFromDescription(response["text"] as! String, timeStamp: response["timeStamp"] as! String) {
                                    //Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.remove(at: indexItem)
                                    let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: self.model.sections.index(of: .barcode)!)
                                    if(numberOfRows != (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)!) {
                                        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getBarcodeSectionIndex(index:indexItem)) as? NotesCell {
                                            cell.activityIndicator.stopAnimating()
                                            cell.actionButton.isHidden = false
                                            self.model.setAddButtonOfBarcodeHeaderView(status: true)
                                        }
                                    }
                                }
                                break
                            case ADDED_DETAILS_TYPE.signature_image_added.rawValue:
                                //Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                                break
                            case ADDED_DETAILS_TYPE.image_added.rawValue:
                                let storedImagePath = response["imagePath"] as! String
                                if let indexItem = self.model.getImageIndexFromImageName(storedImagePath,selectedTaskDetails:Singleton.sharedInstance.selectedTaskDetails) {
                                    if((Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.count)! > indexItem) {
                                        Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.remove(at: indexItem)
                                        self.reloadImageSection()
                                    }
                                    
                                    if (Singleton.sharedInstance.selectedTaskDetails.addedCaptionArray?.count)! > indexItem {
                                        Singleton.sharedInstance.selectedTaskDetails.addedCaptionArray?.remove(at: indexItem)
                                    }
                                }
                                Singleton.sharedInstance.selectedTaskDetails.imageUploadingStatus = false
                                break
                            default:
                                break
                            }
                        }
                        break
                    }
                })
            }
        })
    }
    
    func sendRequestToDeleteCustomFieldImageOnline(_ indexItem:Int, collectionViewIndex:Int) {
        NetworkingHelper.sharedInstance.updateCustomFieldWithOutImage(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].label, job_id: Singleton.sharedInstance.selectedTaskDetails.jobId, data: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].imageArray.object(at: indexItem) as! String, status: "delete", imageName:Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].imageArray.object(at: indexItem) as! String, collectionIndex: collectionViewIndex, receivedResponse: { (succeeded, response) -> () in
            if(succeeded == true) {
                if let jobId = response["jobId"] as? Int {
                    guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                        return
                    }
                }
                let collectionIndex = response["collectionIndex"] as! Int
                let storedImagePath = response["imagePath"] as! String
                Auxillary.deleteImageFromDocumentDirectory(storedImagePath)
                if let storedImageIndex = self.model.getCustomImageIndexFromImageName(storedImagePath, imageArray: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionIndex].imageArray) {
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionIndex].imageArray.removeObject(at: storedImageIndex)
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[collectionIndex].captionArray?.remove(at: storedImageIndex)
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionIndex].fleetData = ""
                    self.model.reloadParticularSection(index: collectionViewIndex + self.model.countBeforeCustomFields)
                }
            } else {
                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                if let jobId = response["jobId"] as? Int {
                    guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                        return
                    }
                }
                let collectionIndex = response["collectionIndex"] as! Int
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionIndex].fleetData = ""
            }
        })
    }
    
    func saveOfflineDataForDeletingCustomFieldImage(_ indexItem:Int, collectionViewIndex:Int) {
        if(Auxillary.isAppSyncingEnable() == true) {
            if(DatabaseManager.sharedInstance.deleteImageFromDatabase(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].imageArray.object(at: indexItem) as! String, imageManagedContext: self.unsyncedObjectContext) == true) {
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].imageArray.removeObject(at: indexItem)
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[collectionViewIndex].captionArray?.remove(at: indexItem)
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].fleetData = ""
                self.model.reloadParticularSection(index: collectionViewIndex + self.model.countBeforeCustomFields)
            } else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].fleetData = ""
            }
        }
    }
    
    func saveOfflineData(type:ADDED_DETAILS_TYPE) {
        if(Singleton.sharedInstance.isAppSyncingEnable() == true) {
            if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                switch type {
                case ADDED_DETAILS_TYPE.text_added, ADDED_DETAILS_TYPE.text_updated:
                    if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getNoteSectionIndex(index:(Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! - 1)) as? NotesCell {
                        cell.activityIndicator.stopAnimating()
                        cell.actionButton.isHidden = false
                    }
                    break
                case ADDED_DETAILS_TYPE.barcode_added, ADDED_DETAILS_TYPE.barcode_updated:
                    if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getBarcodeSectionIndex(index:(Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! - 1)) as? NotesCell {
                        cell.activityIndicator.stopAnimating()
                        cell.actionButton.isHidden = false
                    }
                    break
                default:
                    break
                }
            } else {
                if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                    switch type {
                    case ADDED_DETAILS_TYPE.text_added, ADDED_DETAILS_TYPE.text_updated:
                        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getNoteSectionIndex(index:(Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! - 1)) as? NotesCell {
                            cell.activityIndicator.stopAnimating()
                        }
                        break
                    case ADDED_DETAILS_TYPE.barcode_added, ADDED_DETAILS_TYPE.barcode_updated:
                        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getBarcodeSectionIndex(index:(Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! - 1)) as? NotesCell {
                            cell.activityIndicator.stopAnimating()
                        }
                        break
                    default:
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                    switch type {
                    case ADDED_DETAILS_TYPE.text_added, ADDED_DETAILS_TYPE.text_updated:
                        Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.remove(at: (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! - 1)
                        let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: self.model.sections.index(of: .notes)!)
                        if(numberOfRows != (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)!) {
                            if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getNoteSectionIndex(index:(Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! - 1)) as? NotesCell {
                                cell.activityIndicator.stopAnimating()
                                cell.actionButton.isHidden = false
                                self.model.setAddButtonOfNotesHeaderView(status: true)
                            }
                        }
                        break
                    case ADDED_DETAILS_TYPE.barcode_added, ADDED_DETAILS_TYPE.barcode_updated:
                        Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.remove(at: (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! - 1)
                        let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: self.model.sections.index(of: .barcode)!)
                        if(numberOfRows != (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)!) {
                            if let cell = self.jobDetailsView.detailsTable.cellForRow(at: self.model.getBarcodeSectionIndex(index:(Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! - 1)) as? NotesCell {
                                cell.activityIndicator.stopAnimating()
                                cell.actionButton.isHidden = false
                                self.model.setAddButtonOfBarcodeHeaderView(status: true)
                            }
                        }
                        break
                    default:
                        break
                    }
                }
            }
        }
    }
    
    func saveOfflineDataForCustomFieldImage(_ imagePath:String,tag:Int, image:UIImage, caption:String) {
        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].fleetData = ""
        if(Auxillary.isAppSyncingEnable() == true) {
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].imageArray.add(imagePath)
            self.model.reloadParticularSection(index: tag + self.model.countBeforeCustomFields)
            var dict = ["image_url":imagePath]
            dict["caption_data"] = caption
            let captionObject = Caption(json: dict)
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].captionArray?.append(captionObject)
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].fleetData = "ImageAdded"
            if(DatabaseManager.sharedInstance.saveOfflineImagesToDatabase(Singleton.sharedInstance.selectedTaskDetails.jobId, image: imagePath, caption: caption, label: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].label, imageManagedContext: self.unsyncedObjectContext, type: DatabaseImageType.customField) == true) {
                if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].fleetData = "ImageAdded"
                } else {
                    if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                    }
                }
                NetworkingHelper.sharedInstance.allImagesCache.setObject(image, forKey: "\(imagePath)" as NSString)
                if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row: 0, section: tag + self.model.countBeforeCustomFields)) as? ImageTableCell {
                    cell.imageCollectionView.reloadData()
                    cell.imageCollectionView.scrollToItem(at: IndexPath(item:Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].imageArray.count - 1,section: 0), at: UICollectionViewScrollPosition.right, animated: true)
                }
            } else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .message)
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].fleetData = ""
                if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row: 0, section: tag + self.model.countBeforeCustomFields)) as? ImageTableCell {
                    cell.imageCollectionView.reloadData()
                }
            }
        }
    }
    
    func saveOfflineData(_ imagePath:String, label:String, caption:String) {
        ActivityIndicator.sharedInstance.hideActivityIndicator()
        if(Singleton.sharedInstance.isAppSyncingEnable() == true) {
            DispatchQueue.main.async(execute: { () -> Void in
                var type = 0
                if label == ADDED_DETAILS_TYPE.signature_image_added.rawValue {
                    type = DatabaseImageType.signature
                } else {
                    type = DatabaseImageType.image
                }
                if(DatabaseManager.sharedInstance.saveOfflineImagesToDatabase(Singleton.sharedInstance.selectedTaskDetails.jobId, image:imagePath, caption: caption , label: label, imageManagedContext: self.unsyncedObjectContext, type: type) == true) {
                    if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                        if label == ADDED_DETAILS_TYPE.signature_image_added.rawValue {
                            self.hideSignatureView()
                        }
                        //_ = self.navigationController?.popViewController(animated: true)
                    } else {
                        if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                            _ = self.navigationController?.popViewController(animated: true)
                        } else {
                            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                        }
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                }
            })
            
        }
    }
    
    func alertForSyncingFailed(_ timestamp:String, textToSend:String, type:ADDED_DETAILS_TYPE, image:UIImage, imagePath:String,caption:String, tag:Int) {
        let alert = UIAlertController(title: TEXT.ERROR, message: ERROR_MESSAGE.SYNC_FAILED_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
        let tryAgain = UIAlertAction(title: TEXT.TRY_AGAIN, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                switch type {
                case ADDED_DETAILS_TYPE.text_added, .text_updated, .signature_image_added, .image_added, .barcode_added, .barcode_updated:
                    self.sendRequestForAddTaskDetail(timestamp, textToSend: textToSend, addedDetailType: type, selectedImage: image, imagePath:imagePath, caption: caption)
                    break
                case  ADDED_DETAILS_TYPE.custom_image_added:
                    self.uploadCustomFieldImageToServer(tag, imagePath: imagePath, image: image, caption: caption)
                    break
                default:
                    break
                }
            })
        })
        alert.addAction(tryAgain)
        
        let offlineData = UIAlertAction(title: TEXT.CONTINUE_OFFLINE, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                switch type {
                case ADDED_DETAILS_TYPE.text_added, ADDED_DETAILS_TYPE.text_updated, .barcode_updated, .barcode_added:
                    self.saveOfflineData(type:type)
                    break
                case ADDED_DETAILS_TYPE.signature_image_added:
                    self.saveOfflineData(imagePath, label: ADDED_DETAILS_TYPE.signature_image_added.rawValue, caption:"")
                case ADDED_DETAILS_TYPE.image_added:
                    Singleton.sharedInstance.selectedTaskDetails.imageUploadingStatus = false
                    NetworkingHelper.sharedInstance.allImagesCache.setObject(image, forKey: "\(imagePath)" as NSString)
                    self.saveOfflineData(imagePath, label: ADDED_DETAILS_TYPE.image_added.rawValue, caption:caption)
                    break
                case  ADDED_DETAILS_TYPE.custom_image_added:
                    break
                default:
                    break
                }
            })
        })
        alert.addAction(offlineData)
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertForSyncingFailedForDeletingImage(_ indexItem:Int, collectionViewIndex:Int) {
        let alert = UIAlertController(title: TEXT.ERROR, message: ERROR_MESSAGE.SYNC_FAILED_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
        let tryAgain = UIAlertAction(title: TEXT.TRY_AGAIN, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.deleteImageFromCustomField(indexItem, collectionViewIndex: collectionViewIndex)
            })
        })
        alert.addAction(tryAgain)
        
        let offlineData = UIAlertAction(title: TEXT.CONTINUE_OFFLINE, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.saveOfflineDataForDeletingCustomFieldImage(indexItem, collectionViewIndex: collectionViewIndex)
            })
        })
        alert.addAction(offlineData)
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertForSyncingFailedForCustomField(_ customLabel:String, jobId:Int, customData:String, section:Int, row:Int, customFieldType:String){
        let alert = UIAlertController(title: TEXT.ERROR, message: ERROR_MESSAGE.SYNC_FAILED_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
        let tryAgain = UIAlertAction(title: TEXT.TRY_AGAIN, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.sendRequestToServerForCustomField(customLabel, jobId: jobId, customData: customData, section:section, row:row, customFieldType:customFieldType)
            })
        })
        alert.addAction(tryAgain)
        
        let offlineData = UIAlertAction(title: TEXT.CONTINUE_OFFLINE, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.model.saveOfflineCustomFieldData(customLabel, jobId: jobId, customData: customData, section:section, row:row, customFieldType:customFieldType)
            })
        })
        alert.addAction(offlineData)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                NotificationCenter.default.removeObserver(self)
                Auxillary.logoutFromDevice()
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }

    //MARK: Keyboard Functions
    func keyboardWillShow(_ notification : Foundation.Notification){
        if self.jobDetailsView.currentIndexPath.count > 0 {
            let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
            Singleton.sharedInstance.keyboardSize = value.cgRectValue.size
            keyboardSize = value.cgRectValue.size
            self.jobDetailsView.bottomConstraintOfDetailTable.constant = 40 + keyboardSize.height
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                if self.jobDetailsView.currentIndexPath.section != self.model.sections.count-1 {
                    if self.jobDetailsView.detailsTable.isIndexPathExist(indexPath: self.jobDetailsView.currentIndexPath) {
                        self.jobDetailsView.detailsTable.scrollToRow(at: self.jobDetailsView.currentIndexPath, at: .middle, animated: true)
                    }
                }
            })
            Singleton.sharedInstance.translateErrorMessage(toBottom: false)
        }
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        var duration = 0.3
        var animation = UIViewAnimationOptions.curveLinear
        if let value  = (notification as NSNotification).userInfo![UIKeyboardAnimationDurationUserInfoKey] as? Double {
            duration = value
        }
        
        if let value = (notification as NSNotification).userInfo![UIKeyboardAnimationCurveUserInfoKey] as? UInt {
            animation = UIViewAnimationOptions(rawValue: value)
        }
        
        if Singleton.sharedInstance.selectedTaskDetails != nil {
            if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value != nil {
                if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
                    self.jobDetailsView.bottomConstraintOfDetailTable.constant = self.sliderHeight + 20//
                } else {
                    self.jobDetailsView.bottomConstraintOfDetailTable.constant = self.defaultBottomConstraint
                }
            } else {
                self.jobDetailsView.bottomConstraintOfDetailTable.constant = self.sliderHeight + 20
            }
        } else {
            self.jobDetailsView.bottomConstraintOfDetailTable.constant = self.sliderHeight + 20
        }
        
        self.view.setNeedsUpdateConstraints()
        Singleton.sharedInstance.keyboardSize = CGSize(width: 0.0, height: 0.0)
        keyboardSize = CGSize(width: 0.0, height: 0.0)
        UIView.animate(withDuration: duration, delay: 0.0, options: animation, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }
}

extension DetailController: UIGestureRecognizerDelegate {
    //MARK: PanGesture Delegates
    func valueChanged(_ pan:UIPanGestureRecognizer) {
        let translation  = pan.translation(in: pan.view?.superview)
        var theTransform = pan.view?.transform
        if self.jobDetailsView.detailsTable.contentOffset.y == 0 {
            if pan.state == UIGestureRecognizerState.began {
                panDirection = self.getJobViewDirection(jobView: pan.view!)
            } else if(pan.state == UIGestureRecognizerState.changed) {
                if panDirection == nil {
                    panDirection = self.getJobViewDirection(jobView: pan.view!)
                }
                switch panDirection! {
                case .topToBottom:
                    if translation.y > 0 {
                        theTransform?.ty = translation.y
                        pan.view?.transform = theTransform!
                    }
                case .bottomToTop:
                    if translation.y < 0 && Double((theTransform?.ty)!) >= 0.0 {
                        theTransform?.ty = minimizeOrigin + translation.y
                        pan.view?.transform = theTransform!
                        self.directionButton.alpha = 0
                    }
                }
            } else if(pan.state == UIGestureRecognizerState.ended) {
                if panDirection == nil {
                    panDirection = self.getJobViewDirection(jobView: pan.view!)
                }
                switch panDirection! {
                case .topToBottom:
                    if translation.y > maxTranslatingPointToMinimize {
                        self.translateViewToPoint(point: minimizeOrigin, translatingView: pan.view!)
                    } else {
                        self.translateViewToPoint(point: 0, translatingView: pan.view!)
                    }
                case .bottomToTop:
                    if translation.y > 0 {
                        self.translateViewToPoint(point: minimizeOrigin, translatingView: pan.view!)
                    } else {
                        if translation.y < 0 {
                            self.translateViewToPoint(point: 0, translatingView: pan.view!)
                        }
                    }
                    if self.signatureView != nil {
                        self.hideSignatureView()
                    }
                }
            }
        } else {
//            if panDirection == nil {
//                panDirection = self.getJobViewDirection(jobView: pan.view!)
//            }
//            if(pan.state == UIGestureRecognizerState.ended) {
//                switch panDirection! {
//                case .topToBottom:
//                  if  Double((theTransform?.ty)!) < 0.0 {
//                        self.translateViewToPoint(point: 0, translatingView: pan.view!)
//                  }
//                   break
//                case .bottomToTop:
//              //    if Double((theTransform?.ty)!) > 0.0 {
//                        self.translateViewToPoint(point: minimizeOrigin, translatingView: pan.view!)
//              //    }
//                }
//                if self.signatureView != nil {
//                    self.hideSignatureView()
//                }
//            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

//MARK: JobDetailsDelegate
extension DetailController:JobDetailsDelegate {
    func updateChecklistCustomField(index: Int, row:Int) {
        sendRequestToServerForCustomField(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].label, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, customData: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].fleetData, section:index + self.model.countBeforeCustomFields, row:row, customFieldType:CUSTOM_FIELD_DATA_TYPE.checklist)
    }
    
    func addSignatureAction() {
        self.setSignatureView()
        self.showSignatureView()
        self.lastContentOffset = self.jobDetailsView.detailsTable.contentOffset
    }
    
    func slideButtonAction() {
        if self.jobDetailsView.transform.ty > 0 {
            if self.signatureView != nil {
                self.hideSignatureView()
            }
            self.directionButton.alpha = 0
            self.translateViewToPoint(point: 0, translatingView: self.jobDetailsView)
        } else {
            self.translateViewToPoint(point: self.minimizeOrigin, translatingView: self.jobDetailsView)
        }
    }
    
    func tableAction(index:Int) {
        let taskTableController = storyboard?.instantiateViewController(withIdentifier: "TaskTableController") as! TaskTableController
        taskTableController.selectedIndex = index
        taskTableController.jobId = Singleton.sharedInstance.selectedTaskDetails.jobId
        taskTableController.jobStatus = Singleton.sharedInstance.selectedTaskDetails.jobStatus
        taskTableController.appOptionalField = Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value
        taskTableController.taskStatus = Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus)
        self.present(taskTableController, animated: true, completion: nil)
    }
    
    func checkboxAction(tag: Int, isSelected: Bool) {
        Analytics.logEvent(ANALYTICS_KEY.CUSTOM_FIELD_CHECKBOX, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.CUSTOM_FIELD_CHECKBOX, customAttributes: [:])
        if(isSelected == true) {
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].data = "true"
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].fleetData = "true"
        } else {
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].data = "false"
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].fleetData = "false"
        }
        
        self.model.reloadParticularSection(index: tag + self.model.countBeforeCustomFields)
        sendRequestToServerForCustomField(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].label, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, customData: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].fleetData, section: tag, row:0, customFieldType:CUSTOM_FIELD_DATA_TYPE.checkbox)
    }
    
    func addImageAction(imageType:ImageType, tag:Int) {
        if IJReachability.isConnectedToNetwork() == false && Singleton.sharedInstance.isAppSyncingEnable() == false {
            self.showInternetToast()
        } else {
            getImagePicker.setImagePicker(imagePickerType:getImagePicker.getPickerTypeForCustomTemplate(tag:tag), tag: tag)
            getImagePicker.imageCallBack = { [weak self] (result) in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let image, let index):
                        ActivityIndicator.sharedInstance.showActivityIndicator()
                        if Singleton.sharedInstance.selectedTaskDetails.captionOptionalField.value == 1 {
                            self?.gotoCaptionScreen(image: image, tag: index)
                        } else {
                            self?.setImage(caption: "", image: image, pickerTag:index)
                            ActivityIndicator.sharedInstance.hideActivityIndicator()
                        }
                    case .error(let message):
                        Singleton.sharedInstance.showErrorMessage(error: message, isError: .error)
                    }
                }
            }
        }
    }
    
    func deleteOptionalImage(index:Int) {
        Answers.logCustomEvent(withName: "Delete Image", customAttributes: [:])
        if(index >= Singleton.sharedInstance.selectedTaskDetails.addedImagesArray!.count) {
            return
        }
        
        if(Singleton.sharedInstance.selectedTaskDetails.imageUploadingStatus == false) {
            if(Singleton.sharedInstance.selectedTaskDetails.imageDeletingStatus == false) {
                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                let deleteAction = UIAlertAction(title: TEXT.DELETE, style: UIAlertActionStyle.destructive){
                    UIAlertAction in
                    DispatchQueue.main.async(execute: { () -> Void in
                        if self.imagesPreview != nil {
                            self.imagesPreview.confirmationDoneForDeletion()
                        }
                        if IJReachability.isConnectedToNetwork() == true {
                            self.model.requestToDeleteImage(Singleton.sharedInstance.selectedTaskDetails.jobId, imageId: Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![index].IDs, imagePath:Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![index].taskDescription , selectedTaskDetails: Singleton.sharedInstance.selectedTaskDetails)
                        } else {
                            if(DatabaseManager.sharedInstance.deleteImageFromDatabase(Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![index].taskDescription, imageManagedContext: self.unsyncedObjectContext) == true) {
                                Singleton.sharedInstance.deleteImageFromDocumentDirectory(Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![index].taskDescription)
                                Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.remove(at: index)
                                Singleton.sharedInstance.selectedTaskDetails.addedCaptionArray?.remove(at: index)
                                if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.reloadTaskTableForImageSection), object: nil)
                                }
                            } else {
                                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .message)
                            }
                        }
                    })
                }
                let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.cancel){
                    UIAlertAction in
                }
                alertController.addAction(deleteAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_DELETING, isError: .message)
            }
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_UPLOADING, isError: .message)
        }
    }
    
    func deleteImageFromCustomField(_ indexItem:Int, collectionViewIndex:Int) {
        guard collectionViewIndex < (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! else {
            return
        }
        
        guard indexItem < Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].imageArray.count else {
            return
        }
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.CUSTOM_FIELD_IMAGE_DELETE, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.CUSTOM_FIELD_IMAGE_DELETE, customAttributes: [:])
        if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].fleetData == "Uploading") {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_UPLOADING, isError: .message)
        } else if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].fleetData == "Deleting") {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_DELETING, isError: .message)
        } else if((Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].imageArray.object(at: indexItem) as AnyObject).isKind(of: UIImage.classForCoder()) == true) {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.PLEASE_TRY_AGAIN, isError: .error)
        }else {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            let deleteAction = UIAlertAction(title: TEXT.DELETE, style: UIAlertActionStyle.destructive){
                UIAlertAction in
                DispatchQueue.main.async(execute: { () -> Void in
                    if self.imagesPreview != nil {
                        self.imagesPreview.confirmationDoneForDeletion()
                    }
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionViewIndex].fleetData = "Deleting"
                    if IJReachability.isConnectedToNetwork() == true {
                        if(self.offlineModel.uploadUnsyncedTaskToServer() == false) {
                            if(self.offlineModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                                self.showInvalidAccessTokenPopup(self.offlineModel.hitFailMessage)
                            } else {
                                self.alertForSyncingFailedForDeletingImage(indexItem, collectionViewIndex: collectionViewIndex)
                            }
                        } else {
                            self.sendRequestToDeleteCustomFieldImageOnline(indexItem, collectionViewIndex: collectionViewIndex)
                        }
                    } else {
                        self.saveOfflineDataForDeletingCustomFieldImage(indexItem, collectionViewIndex: collectionViewIndex)
                    }
                })
            }
            let cancelAction = UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.cancel){
                UIAlertAction in
            }
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func noInternetConnectionAvailable() {
        self.showInternetToast()
    }
    
    func showCustomDatePicker(customField:CustomFields, tag:Int) {
        if customPicker == nil {
            customPicker = UINib(nibName: NIB_NAME.customDatePicker, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomDatePicker
            customPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            customPicker.delegate = self
            customPicker.tag = tag
            self.view.addSubview(customPicker)
            customPicker.setDatePicker()
            customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
            switch(customField.dataType) {
            case CUSTOM_FIELD_DATA_TYPE.date:
                customPicker.datePicker.datePickerMode = UIDatePickerMode.date
                break
            case CUSTOM_FIELD_DATA_TYPE.datePast:
                customPicker.datePicker.datePickerMode = UIDatePickerMode.date
                customPicker.datePicker.maximumDate = Date()
                break
            case CUSTOM_FIELD_DATA_TYPE.dateFuture:
                customPicker.datePicker.datePickerMode = UIDatePickerMode.date
                customPicker.datePicker.minimumDate = Date()
                break
            case CUSTOM_FIELD_DATA_TYPE.dateTime:
                customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
                break
            case CUSTOM_FIELD_DATA_TYPE.dateTimeFuture:
                customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
                customPicker.datePicker.minimumDate = Date()
                break
            case CUSTOM_FIELD_DATA_TYPE.dateTimePast:
                customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
                customPicker.datePicker.maximumDate = Date()
                break
            default:
                break
            }
        }
    }
    
    func showDropdown(customField: CustomFields, tag: Int) {
        self.view.endEditing(true)
        guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].dropdown.count > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
            return
        }
        self.drowDownWithSearch = TemplateController()
        self.drowDownWithSearch.itemArray = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].dropdown
        self.drowDownWithSearch.placeholderValue = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tag].displayLabelName
        self.drowDownWithSearch.delegate = self
        self.drowDownWithSearch.modalPresentationStyle = .overCurrentContext
        self.drowDownWithSearch.view.tag = tag
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.present(self.drowDownWithSearch, animated: false, completion: nil)
    }
    
    func showEmailController(urlText:String) {
        let mailComposeViewController = configuredMailComposeViewController(urlText)
        if MFMailComposeViewController.canSendMail() {
            DispatchQueue.main.async(execute: {
                self.present(mailComposeViewController, animated: true, completion: nil)
            })
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NOT_SUPPORT_EMAIL, isError: .message)
        }
    }
    
    func configuredMailComposeViewController(_ recipientMail:String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients([recipientMail])
        return mailComposerVC
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    internal func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func checkCustomFieldValidation(index: Int, enteredText: String, row:Int, customFieldType:String){
        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row:0, section: index + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
            let message = self.model.checkCustomFieldsValidation(index: index, enteredText: enteredText)
            if(message.length == 0) {
                self.view.endEditing(true)
                if IJReachability.isConnectedToNetwork() == false && Singleton.sharedInstance.isAppSyncingEnable() == false {
                    self.showInternetToast()
                } else {
                    if enteredText != Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].fleetData.trimText {
                        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].data = enteredText
                        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].fleetData = enteredText
                        self.sendRequestToServerForCustomField(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].label, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, customData: enteredText,section: index, row:row, customFieldType: customFieldType)
                    }
                }
                cell.detailView.isEditable = true
                cell.actionButton.isSelected = true
            } else {
                Singleton.sharedInstance.showErrorMessage(error: message, isError: .error)
                cell.actionButton.isSelected = false
            }
        }
    }
    
    func checkCustomFieldValidationForBarcode(index: Int, enteredText: String) {
        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row:0, section: index + self.model.countBeforeCustomFields)) as? NotesCell {
            let message = self.model.checkCustomFieldsValidation(index: index, enteredText: enteredText)
            if(message.length == 0) {
                self.view.endEditing(true)
                if IJReachability.isConnectedToNetwork() == false && Singleton.sharedInstance.isAppSyncingEnable() == false {
                    self.showInternetToast()
                } else {
                    if enteredText != Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].fleetData.trimText {
                        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].data = enteredText
                        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].fleetData = enteredText
                        self.sendRequestToServerForCustomField(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].label, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, customData: enteredText,section: index, row:0, customFieldType:CUSTOM_FIELD_DATA_TYPE.barcode)
                    }
                }
                cell.descriptionView.isEditable = true
            } else {
                Singleton.sharedInstance.showErrorMessage(error: message, isError: .error)
            }
        }
    }
    
    func showCalling(phoneNumber:String) {
        let phone = "tel://\(phoneNumber.replacingOccurrences(of: " ", with: ""))"
        if let url:URL = URL(string:phone) {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            alert.addAction(UIAlertAction(title: TEXT.CALL, style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) in
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.openURL(url)
                })
            }))
            
            alert.addAction(UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            Singleton.sharedInstance.showErrorMessage(error: "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.PHONE_NUMBER)", isError: .error)
        }
    }
    
    func viewInvoiceController() {
        self.gotoInvoiceController()
    }
    
    func showEarningController() {
        let ratingValue = Singleton.sharedInstance.selectedTaskDetails.isRatingEnabled == true ? 1 : 0
        guard Singleton.sharedInstance.selectedTaskDetails.driverJobTotal?.length == 0 else {
            self.serverHitForRatings(ratingValue: ratingValue)
            return
        }
        guard Singleton.sharedInstance.selectedTaskDetails.customerJobTotal?.length == 0 else {
            self.serverHitForRatings(ratingValue: ratingValue)
            return
        }
        self.gotoEarningController(earningFormulaFields: [], pricingFormulaFields: [], totalDistane: "-", totalDuration: "-", hasTaskCompleted: true, isRatingAvailable: ratingValue)
    }
    
    func serverHitForRatings(ratingValue : Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        var params:[String : Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
        params["job_id"] = self.jobId
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.fetch_fleet_job_report, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                print(response)
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        var earningFormulaFields = [FORMULA_FIELDS]()
                        var pricingFormulaFields = [FORMULA_FIELDS]()
                        //let ratingValue = Singleton.sharedInstance.selectedTaskDetails.isRatingEnabled == true ? 1 : 0
                        
                        if let responseData = response["data"] as? [String:Any] {
                            if let fields = responseData["formula_fields"] as? [[String:Any]] {
                                for field in fields {
                                    let formulaField = FORMULA_FIELDS(json: field)
                                    earningFormulaFields.append(formulaField)
                                }
                            }
                            
                            if let fields = responseData["customer_formula_fields"] as? [[String:Any]] {
                                for field in fields {
                                    let formulaField = FORMULA_FIELDS(json: field)
                                    pricingFormulaFields.append(formulaField)
                                }
                            }
                            
                            var totalDistance = ""
                            var totalDuration = ""
                            
                            if let value = responseData["total_distance"] as? NSNumber {
                                totalDistance = "\(value)"
                            } else if let value = responseData["total_distance"] as? String {
                                totalDistance = value
                            }
                            
                            if let value = responseData["total_duration"] as? NSNumber {
                                totalDuration = "\(value)"
                            } else if let value = responseData["total_duration"] as? String {
                                totalDuration = value
                            }
                            
                            self.gotoEarningController(earningFormulaFields: earningFormulaFields, pricingFormulaFields: pricingFormulaFields, totalDistane: totalDistance, totalDuration: totalDuration, hasTaskCompleted: true, isRatingAvailable: ratingValue)
                        }
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    default:
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        })
    }
}

//MARK: TemplateControllerDelegate Methods
extension DetailController:TemplateControllerDelegate {
    func selectedValue(value: String, tag:Int, isDirectDismiss: Bool) {
        
        if isDirectDismiss == false{
            self.updateValue(value, tagValue: tag)
        }
        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row: 0, section: tag + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
            cell.detailView.isUserInteractionEnabled = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.drowDownWithSearch = nil
    }
    
    func updateValue(_ updatedValue:String, tagValue:Int) {
        if IJReachability.isConnectedToNetwork() == false && Auxillary.isAppSyncingEnable() == false {
            self.showInternetToast()
        } else {
            if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row: 0, section: tagValue + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    cell.detailView.text! = updatedValue
                }
                cell.detailView.isUserInteractionEnabled = true
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tagValue].data = updatedValue
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tagValue].fleetData = updatedValue
                self.sendRequestToServerForCustomField(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![tagValue].label, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, customData: updatedValue, section: tagValue, row:0, customFieldType:CUSTOM_FIELD_DATA_TYPE.dropDown)
                if updatedValue.length > 0 {
                    cell.placeholderLabel.isHidden = true
                } else {
                    cell.placeholderLabel.isHidden = false
                }
                
                self.model.reloadParticularSection(index: tagValue + self.model.countBeforeCustomFields)
            }
        }
    }
}

//MARK: CustomDatePicker Delegate Methods
extension DetailController:CustomPickerDelegate {
    func dismissDatePicker() {
        self.updateValueForDatePicker("",customPickerTag: customPicker.tag)
    }
    
    func selectedDateFromPicker(_ selectedDate: String) {
        guard customPicker != nil else {
            return
        }
        Analytics.logEvent(ANALYTICS_KEY.CUSTOM_FIELD_DATE, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.CUSTOM_FIELD_DATE, customAttributes: [:])
        self.view.endEditing(true)
        self.updateValueForDatePicker(selectedDate, customPickerTag: customPicker.tag)
    }
    
    func updateValueForDatePicker(_ updatedDate:String, customPickerTag:Int) {
        if IJReachability.isConnectedToNetwork() == false && Auxillary.isAppSyncingEnable() == false {
            self.showInternetToast()
        } else {
            if let cell = self.jobDetailsView.detailsTable.cellForRow(at: IndexPath(row: 0, section: customPickerTag + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
                if updatedDate.length > 0 {
                    cell.placeholderLabel.isHidden = true
                } else {
                    cell.placeholderLabel.isHidden = false
                }
                switch Singleton.sharedInstance.selectedTaskDetails.customFieldArray![customPicker.tag].dataType {
                case CUSTOM_FIELD_DATA_TYPE.date, CUSTOM_FIELD_DATA_TYPE.datePast, CUSTOM_FIELD_DATA_TYPE.dateFuture:
                    cell.detailView.text! = updatedDate.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd", output: "dd MMM yyyy")
                    break
                default:
                    cell.detailView.text! = updatedDate.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd hh:mm a", output: "dd MMM yyyy hh:mm a")
                    break
                }
                cell.detailView.isUserInteractionEnabled = true
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![customPicker.tag].data = updatedDate
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![customPicker.tag].fleetData = updatedDate
                self.sendRequestToServerForCustomField(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![customPicker.tag].label, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, customData: updatedDate,section: customPicker.tag, row:0, customFieldType:CUSTOM_FIELD_DATA_TYPE.date)
                
                self.model.reloadParticularSection(index: customPickerTag + self.model.countBeforeCustomFields)
            }
        }
        customPicker = nil
    }
}

//MARK: CaptionController
extension DetailController: ImageCaptionViewControllerDelegate {
    func gotoCaptionScreen(image:UIImage, tag:Int) {
        self.imageCaptionViewController = ImageCaptionViewController()
        self.imageCaptionViewController.delegate = self
        DispatchQueue.main.async {
            self.present(self.imageCaptionViewController, animated: true, completion: nil)
            self.imageCaptionViewController.imagePreview.image = image
            self.imageCaptionViewController.pickerTag = tag
            ActivityIndicator.sharedInstance.hideActivityIndicator()
        }
    }
    
    func setImage(caption: String,image: UIImage, pickerTag: Int) {
        let currentDateTime = Date().getDateStringWithUnderscore
        if(pickerTag == IMAGE_TAG) {
            let imagePath = Singleton.sharedInstance.saveImageToDocumentDirectory("\(Singleton.sharedInstance.selectedTaskDetails.jobId!)_\(currentDateTime).png", image: image)
            let dict = ["description":imagePath]
            let taskHistory = TaskHistory(json: dict as NSDictionary)
            var captionDict = ["image_url":imagePath]
            captionDict["caption_data"] = caption
            let captionObject = Caption(json: captionDict)
            if IJReachability.isConnectedToNetwork() == true {
                Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.append(taskHistory)
                Singleton.sharedInstance.selectedTaskDetails.addedCaptionArray?.append(captionObject)
                self.reloadImageSection()
                Singleton.sharedInstance.selectedTaskDetails.imageUploadingStatus = true
                if(self.offlineModel.uploadUnsyncedTaskToServer() == false) {
                    if(self.offlineModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                        self.showInvalidAccessTokenPopup(self.offlineModel.hitFailMessage)
                    } else {
                        self.alertForSyncingFailed(currentDateTime, textToSend: "", type: .image_added, image: image, imagePath: imagePath,caption:caption, tag:0)
                    }
                } else {
                    self.sendRequestForAddTaskDetail(currentDateTime, textToSend: "", addedDetailType: .image_added, selectedImage: image, imagePath: imagePath, caption: caption)
                }
            } else {
                guard Singleton.sharedInstance.isAppSyncingEnable() == true else {
                    return
                }
                Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.append(taskHistory)
                Singleton.sharedInstance.selectedTaskDetails.addedCaptionArray?.append(captionObject)
                self.reloadImageSection()
                Singleton.sharedInstance.selectedTaskDetails.imageUploadingStatus = false
                NetworkingHelper.sharedInstance.allImagesCache.setObject(image, forKey: "\(imagePath)" as NSString)
                self.saveOfflineData(imagePath, label: "",caption:caption)
            }
        } else {
            let imagePath = Auxillary.saveImageToDocumentDirectory("\(Singleton.sharedInstance.selectedTaskDetails.jobId!)_\(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![pickerTag].label)_\(currentDateTime).png", image: image)
            self.uploadCustomFieldImageToServer(pickerTag, imagePath: imagePath, image: image, caption: caption)
        }

    }
}

//MARK: Navigation Bar
extension DetailController:NavigationDelegate {
    func setNavigationBar() {
        if navigation == nil {
            navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        }
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        self.jobId = Singleton.sharedInstance.selectedTaskDetails.jobId!
        self.pickupDeliveryRelationship = Singleton.sharedInstance.selectedTaskDetails.pickupDeliveryRelationship
        navigation.titleLabel.text = "\(self.jobId!)"
        navigation.titleLabel.textAlignment = NSTextAlignment.left
        navigation.backButton.isHidden = true
        navigation.hamburgerButton.isHidden = false
        navigation.editButtonTrailingConstraint.constant = 15.0
        navigation.editButton.addTarget(self, action: #selector(self.editAction), for: .touchUpInside)
        
        switch Singleton.sharedInstance.selectedTaskDetails.related_job_type! {
        case RELATED_JOB_TYPE.SINGLE:
            navigation.editButton.isHidden = true
            break
        case RELATED_JOB_TYPE.GOTO_PICKUP:	
            navigation.editButton.isHidden = false
            navigation.editButton.setTitle(TEXT.GOTO_PICKUP, for: .normal)
            break
        case RELATED_JOB_TYPE.GOTO_DELIVERY:
            navigation.editButton.isHidden = false
            navigation.editButton.setTitle(BUTTON_TITLE.gotoDelivery, for: .normal)
            break
        case RELATED_JOB_TYPE.GOTO_MULTIPLE:
            navigation.editButton.isHidden = false
            navigation.editButton.setTitle(TEXT.GOTO_RELATED_TASK, for: .normal)
            break
        default:
            break
        }
        navigation.alpha = 0
        self.view.addSubview(navigation)
        self.navigation.changeHamburgerButton()
        self.updateNavigationBar()
    }
    
    func updateNavigationBar() {
        var themeColor:UIColor = UIColor.white
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                themeColor = UIColor.white
                self.navigation.blurEffectView.frame.size.height = HEIGHT.navigationHeight
                self.navigation.addSubview(self.navigation.blurEffectView)
                self.navigation.sendSubview(toBack: self.navigation.blurEffectView)
                UIApplication.shared.statusBarStyle = .lightContent
                navigation.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
            } else {
                themeColor = UIColor.black
                self.navigation.blurEffectView.removeFromSuperview()
                UIApplication.shared.statusBarStyle = .default
                self.navigation.backgroundColor = UIColor.clear
            }
            
            self.navigation.hamburgerButton.color = themeColor
            self.navigation.editButton.tintColor = themeColor
            self.navigation.titleLabel.textColor = themeColor
            self.navigation.editButton.setTitleColor(themeColor, for: .normal)
        }
    }
    
    func backAction() {
        Singleton.sharedInstance.getCountOfInProgressHit(receivedCount: { (count) in
            DispatchQueue.main.async {
                if count > 0 {
                    let alert = UIAlertController(title: "", message: ERROR_MESSAGE.DATA_UPLOAD_IN_PROGRESS, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: TEXT.YES, style: .default, handler: { finished in
                        Singleton.sharedInstance.cancelAllURLRequest()
                        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotification), object: self)
                        self.gotoHome()
                    }))
                    alert.addAction(UIAlertAction(title: TEXT.NO, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.gotoHome()
                }
            }
        })
    }
    
    func gotoHome() {
        UIView.animate(withDuration: 0.1, animations: {
            self.delegate.updateForceTouch()
            self.jobDetailsView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
        }, completion: { finished in
            self.dismiss(animated: false, completion: { finished in
                self.callback?("Hi")
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
            })
        })
        
    }
    
    func editAction() {
        Singleton.sharedInstance.getCountOfInProgressHit(receivedCount: { (count) in
            DispatchQueue.main.async {
                if count > 0 {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.DATA_UPLOADING, isError: .error)
                } else {
                    self.gotoRelatedTaskAction()
                }
            }
        })
    }
    
    func gotoRelatedTaskAction() {
        self.navigation.editButton.isEnabled = false
        self.perform(#selector(self.enableEditButton), with: nil, afterDelay: 2.0)
        self.dismissPartial()
        self.translateViewToPoint(point: minimizeOrigin, translatingView: self.jobDetailsView)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let isHitRequired = self.model.isHitRequiredForRelatedTask()
            if Singleton.sharedInstance.selectedTaskDetails.related_job_count! > 2 {
                if isHitRequired == false {
                    self.gotoRelatedTask()
                } else {
                    self.serverHitForRelatedTask()
                }
            } else {
                if isHitRequired == false {
                    for i in 0..<Singleton.sharedInstance.selectedTaskRelatedArray.count {
                        if Singleton.sharedInstance.selectedTaskRelatedArray[i].jobId != self.jobId {
                            Singleton.sharedInstance.selectedTaskDetails = Singleton.sharedInstance.selectedTaskRelatedArray[i]
                            break
                        }
                    }
                    self.reloadDetailTable()
                } else {
                    self.serverHitForRelatedTask()
                }
            }
        }
    }
    
    func enableEditButton() {
        self.navigation.editButton.isEnabled = true
    }
    
    func serverHitForRelatedTask() {
        var params:[String:Any] = ["access_token": Singleton.sharedInstance.getAccessToken()]
        params["pickup_delivery_relationship"] = self.pickupDeliveryRelationship
        params["job_id"] = self.jobId
        params["version"] = VERSION
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getRelatedTask, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                print(response)
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    default:
                        if Singleton.sharedInstance.selectedTaskDetails.related_job_count! > 2 {
                            Singleton.sharedInstance.selectedTaskRelatedArray.removeAll(keepingCapacity: false)
                            if let data = response["data"] as? [Any] {
                                for item in data {
                                    let taskDetails = TasksAssignedToDate(json: item as! [String : Any])
                                    Singleton.sharedInstance.selectedTaskRelatedArray.append(taskDetails)
                                }
                            }
                            self.gotoRelatedTask()
                        } else {
                            Singleton.sharedInstance.selectedTaskRelatedArray.removeAll(keepingCapacity: false)
                            if let data = response["data"] as? [Any] {
                                for item in data {
                                    let taskDetails = TasksAssignedToDate(json: item as! [String : Any])
                                    Singleton.sharedInstance.selectedTaskRelatedArray.append(taskDetails)
                                    if taskDetails.jobId != self.jobId {
                                        Singleton.sharedInstance.selectedTaskDetails = taskDetails
                                        break
                                    }
                                }
                                self.reloadDetailTable()
                            }
                        }
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func gotoRelatedTask() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.relatedTaskController) as! RelatedTaskController
        self.present(controller, animated: true, completion: nil)
    }
}

//MARK: NOTES DELEGATE
extension DetailController:NotesModelDelegate, MFMessageComposeViewControllerDelegate {
    func navigationErrorMessage(appName: String, directionMode:String, origin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D) {
        let alertController = UIAlertController(title: "", message: appName + ERROR_MESSAGE.NAVIGATION_ERROR_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
        let continueAction = UIAlertAction(title: TEXT.CONTINUE, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            let addr = "http://maps.google.com/maps?saddr=\(origin.latitude),\(origin.longitude)&daddr=\(destination.latitude),\(destination.longitude)&mode=\(directionMode)"
            UIApplication.shared.openURL(URL(string: addr)!)
        })
        
        let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(continueAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func invalidAccessToken(message:String) {
        self.showInvalidAccessTokenPopup(message)
    }
    
    func callFunction(url:URL) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: TEXT.CALL, style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) in
            DispatchQueue.main.async(execute: {
                UIApplication.shared.openURL(url)
            })
        }))
        alert.addAction(UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Message Button Action
    func openMessageController(){
        guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
            return
        }
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.TASK_DETAIL_MESSAGE, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.TASK_DETAIL_MESSAGE, customAttributes: [:])
        let messageController: MFMessageComposeViewController = MFMessageComposeViewController()
        if let phoneNumber = Singleton.sharedInstance.selectedTaskDetails.customerPhone {
            if !MFMessageComposeViewController.canSendText() {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NOT_SUPPORT_SMS, isError: .error)
                return
            }
            let recipents: [String] = [phoneNumber]
            messageController.messageComposeDelegate = self
            messageController.recipients = (recipents)
            DispatchQueue.main.async(execute: {
                self.present(messageController, animated: true, completion: nil)
            })
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func sendRequestToAddNewNotes(enteredNote:String, index:Int, timeStamp:String, type:ADDED_DETAILS_TYPE) {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.NOTE_ADD, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.NOTE_ADD, customAttributes: [:])
        let offlineModel = OfflineDataModel()
        if IJReachability.isConnectedToNetwork() == true {
            if(offlineModel.uploadUnsyncedTaskToServer() == false) {
                if(offlineModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                    self.showInvalidAccessTokenPopup(offlineModel.hitFailMessage)
                } else {
                    self.alertForSyncingFailed(timeStamp, textToSend:enteredNote, type: type, image:UIImage(), imagePath:"", caption:"", tag:0)
                }
            } else {
                self.sendRequestForAddTaskDetail(timeStamp, textToSend: enteredNote, addedDetailType: type, selectedImage: UIImage(), imagePath: "", caption: "")
            }
        } else {
            self.saveOfflineData(type:type)
        }
    }
    
    func deleteNoteAction(index:Int, type:ADDED_DETAILS_TYPE) {
        var noteId:Int!
        switch type {
        case .text_deleted:
            Analytics.logEvent(ANALYTICS_KEY.NOTE_DELETED, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.NOTE_DELETED, customAttributes: [:])
            noteId = Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[index].IDs!
            guard noteId != 0 else {
                let index = (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! - 1
                Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.remove(at: index)
                let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: self.model.sections.index(of: .notes)!)
                if(numberOfRows != (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)!) {
                    self.jobDetailsView.detailsTable.deleteRows(at: [self.model.getNoteSectionIndex(index: index)], with: .automatic)
                }
            
                UIView.setAnimationsEnabled(false)
                self.jobDetailsView.detailsTable.reloadData()
                UIView.setAnimationsEnabled(true)
                return
            }
            break
        case .barcode_deleted:
            Analytics.logEvent(ANALYTICS_KEY.BARCODE_DELETE, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.BARCODE_DELETE, customAttributes: [:])
            noteId = Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?[index].IDs!
            break
        default:
            break
        }
        
        if IJReachability.isConnectedToNetwork() == true {
            var lat:String = ""
            var lng:String = ""
            /*======= SET MODE ==========*/
            switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
            case BATTERY_USAGE.foreground:
                if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                    lat = "\(location.coordinate.latitude)"
                    lng = "\(location.coordinate.longitude)"
                }
                break
            default:
                break
            }
            /*==========================================*/
            let jobId = Singleton.sharedInstance.selectedTaskDetails.jobId!
            
            var param:[String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
            param["type"] = type.rawValue
            param["job_id"] = "\(jobId)"
            param["id"] = "\(noteId!)"
            param["lat"] = lat
            param["lng"] = lng

            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.delete_task_detail, params: param as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    if isSucceeded == true {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.showInvalidAccessTokenPopup(response["message"] as! String)
                            break
                        default:
                            switch type {
                            case .text_deleted:
                                if let data  = response["data"] as? NSDictionary{
                                    if let deletedId = data["insertedID"] as? NSString {
                                        if let indexItem = self.model.getNoteFromID(deletedId.integerValue) {
                                            Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.remove(at: indexItem)
                                            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                                            UIView.setAnimationsEnabled(false)
                                            self.jobDetailsView.detailsTable.reloadData()
                                            UIView.setAnimationsEnabled(true)
                                        }
                                    }
                                }
                                break
                            case .barcode_deleted:
                                if let data  = response["data"] as? NSDictionary{
                                    if let deletedId = data["insertedID"] as? NSString {
                                        if let indexItem = self.model.getCodeFromID(deletedId.integerValue) {
                                            Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.remove(at: indexItem)
                                            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                                            let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: self.model.sections.index(of: .barcode)!)
                                            if(numberOfRows != (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)!) {
                                                self.jobDetailsView.detailsTable.deleteRows(at: [self.model.getBarcodeSectionIndex(index: indexItem)], with: .automatic)
                                            }
                                            UIView.setAnimationsEnabled(false)
                                            self.jobDetailsView.detailsTable.reloadData()
                                            UIView.setAnimationsEnabled(true)
                                        }
                                    }
                                }
                                break
                            default:
                                break
                            }
                            
                            break
                        }
                    } else {
                        UIView.setAnimationsEnabled(false)
                        self.jobDetailsView.detailsTable.reloadData()
                        UIView.setAnimationsEnabled(true)
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                }
            })
        } else {
           
            switch type {
            case .text_deleted:
                if(noteId != 0) {
                    Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![index].type = "text_deleted"
                    Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![index].creationDatetime = Singleton.sharedInstance.getUTCDateString()
                    Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![index].latitude = LocationTracker.sharedInstance().myLocation.coordinate.latitude
                    Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![index].longitude = LocationTracker.sharedInstance().myLocation.coordinate.longitude
                    Singleton.sharedInstance.selectedTaskDetails.deletedNotesArray.append(Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![index])
                }
                Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.remove(at: index)
                if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                    let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: self.model.sections.index(of: .notes)!)
                    if(numberOfRows != (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)!) {
                        self.jobDetailsView.detailsTable.deleteRows(at: [self.model.getNoteSectionIndex(index: index)], with: .automatic)
                    }
                    UIView.setAnimationsEnabled(false)
                    self.jobDetailsView.detailsTable.reloadData()
                    UIView.setAnimationsEnabled(true)
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                }
                break
            case .barcode_deleted:
                if(noteId != 0) {
                    Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![index].type = type.rawValue
                    Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![index].creationDatetime = Singleton.sharedInstance.getUTCDateString()
                    Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![index].latitude = LocationTracker.sharedInstance().myLocation.coordinate.latitude
                    Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![index].longitude = LocationTracker.sharedInstance().myLocation.coordinate.longitude
                    Singleton.sharedInstance.selectedTaskDetails.deletedScannerArray.append(Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![index])
                }
                Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.remove(at: index)
                if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                    let numberOfRows = self.jobDetailsView.detailsTable.numberOfRows(inSection: self.model.sections.index(of: .barcode)!)
                    if(numberOfRows != (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)!) {
                        self.jobDetailsView.detailsTable.deleteRows(at: [self.model.getBarcodeSectionIndex(index: index)], with: .automatic)
                    }
                    UIView.setAnimationsEnabled(false)
                    self.jobDetailsView.detailsTable.reloadData()
                    UIView.setAnimationsEnabled(true)
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                }
                break
            default:
                break
            }
        }
    }
    
    func noInternetConnection() {
        self.showInternetToast()
    }
}

//MARK: SCANNER
extension DetailController:ScannerDelegate {
    func scannerAction(index: Int) {
        self.view.endEditing(true)
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
            self.startScanning()
        } else {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                DispatchQueue.main.async {
                    if granted == true {
                        self.startScanning()
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.REJECTED_CAMERA_SUPPORT, isError: .error)
                    }
                }
            });
        }
    }
    
    func startScanning() {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.BARCODE_ADD_SCAN, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.BARCODE_ADD_SCAN, customAttributes: [:])
        guard self.scanView == nil else {
            return
        }
        scanView = ScanView(frame: self.view.frame)
        scanView.delegate = self
        self.view.addSubview(scanView)
        scanView.setScanView()
    }
    
    //MARK: Scanner Delegate
    func decodedOutput(_ value: String) {
        let indexPath = IndexPath(row: self.model.currentIndex, section: self.model.currentSection)
        if let cell = self.jobDetailsView.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            cell.placeholderLabel.isHidden = true
            cell.descriptionView.text = value
            cell.descriptionView.resignFirstResponder()
            if self.model.currentSection == self.model.sections.index(of: .customBarcode) {
                self.checkCustomFieldValidationForBarcode(index: self.model.currentSection - self.model.countBeforeCustomFields, enteredText: value.trimText)
            } else {
                cell.actionButton.isSelected = true
                cell.actionButton.isHidden = true
                cell.activityIndicator.startAnimating()
                self.model.sendRequestForBarcode(index: self.model.currentIndex, barcodeText: value.trimText)
            }
        }
    }
    
    func dismissScannerView() {
        guard scanView != nil else {
            return
        }
        self.scanView.removeFromSuperview()
        self.scanView = nil
    }
}

//MARK: NoInternetConnectionView
extension DetailController:NoInternetConnectionDelegate {
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: NIB_NAME.noInternetConnectionView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:20, width: self.view.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.view.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
}

//MARK: Signature Delegate
extension DetailController:SignatureDelegate {
    func setSignatureView() {
        self.view.endEditing(true)
        if signatureView == nil {
            self.signatureView = UINib(nibName: NIB_NAME.signatureView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! SignatureView
            self.signatureView.delegate = self
            self.signatureView.alpha = 0
            self.signatureView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height)
            self.signatureView.setSignatureImageView()
            self.view.addSubview(self.signatureView)
            self.view.sendSubview(toBack: self.signatureView)
        }
        self.showSignatureView()
    }
    
    func showSignatureView() {
        UIApplication.shared.setStatusBarHidden(true, with: .fade)
        self.directionButton.alpha = 0.0
        UIView.animate(withDuration: 0.5) {
            self.signatureView.alpha = 1.0
            self.navigation.alpha = 0.0
            self.positiveStatusButton.transform = CGAffineTransform(translationX: 0, y: self.positiveStatusButton.frame.height)
            self.negativeStatusButton.transform = CGAffineTransform(translationX: 0, y: self.positiveStatusButton.frame.height)
            self.moreOptionButton.transform = CGAffineTransform(translationX: 0, y: self.positiveStatusButton.frame.height)
            if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
                self.slider.transform = CGAffineTransform(translationX: 0, y: self.sliderHeight + Singleton.sharedInstance.getBottomInsetofSafeArea())
            }
        }
        self.translateViewToPoint(point: signatureDetailViewOrigin - Singleton.sharedInstance.getBottomInsetofSafeArea(), translatingView: self.jobDetailsView)
    }
    
    func hideSignatureView() {
        UIApplication.shared.setStatusBarHidden(false, with: .fade)
        self.jobDetailsView.detailsTable.reloadData()
        if self.jobDetailsView.transform.ty == self.signatureDetailViewOrigin - Singleton.sharedInstance.getBottomInsetofSafeArea(){
            self.translateViewToPoint(point: 0, translatingView: self.jobDetailsView)
        }
        self.jobDetailsView.detailsTable.setContentOffset(self.lastContentOffset, animated: true)
        UIView.animate(withDuration: 0.5, animations: {
            if self.signatureView != nil {
                self.signatureView.alpha = 0.0
            }
            self.navigation.alpha = 1.0
            self.positiveStatusButton.transform = CGAffineTransform.identity
            self.negativeStatusButton.transform = CGAffineTransform.identity
            self.moreOptionButton.transform = CGAffineTransform.identity
            if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
                self.slider.transform = CGAffineTransform.identity
            }
        }, completion: { finished in
            guard self.signatureView != nil else {
                return
            }
            self.signatureView.removeFromSuperview()
            self.signatureView = nil
        })
    }
    
    func addSignatureAction(imagePath:String, image:UIImage) {
        if IJReachability.isConnectedToNetwork() == false && Singleton.sharedInstance.isAppSyncingEnable() == false {
            self.showInternetToast()
        } else {
            ActivityIndicator.sharedInstance.showActivityIndicator(self)
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.SIGNATURE_DONE, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.SIGNATURE_DONE, customAttributes: [:])
            let dict = [
                "description"    :      imagePath
            ]
            let taskHistory = TaskHistory(json: dict as NSDictionary)
            if((Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.count)! > 0) {
                _ = DatabaseManager.sharedInstance.deleteImageFromDatabase(imagePath, imageManagedContext: self.unsyncedObjectContext)
                Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray!.removeLast()
            }
            Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray!.append(taskHistory)
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                if IJReachability.isConnectedToNetwork() == true {
                    if(self.offlineModel.uploadUnsyncedTaskToServer() == false) {
                        if(self.offlineModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                            self.showInvalidAccessTokenPopup(self.offlineModel.hitFailMessage)
                        } else {
                            self.alertForSyncingFailed("", textToSend: "", type: .signature_image_added, image:image, imagePath:imagePath,caption:"", tag:0)
                        }
                    } else {
                        self.sendRequestForAddTaskDetail("", textToSend: "", addedDetailType: .signature_image_added, selectedImage: image, imagePath:imagePath, caption: "")
                    }
                } else {
                    self.saveOfflineData(imagePath, label: ADDED_DETAILS_TYPE.signature_image_added.rawValue, caption:"")
                }
            }
        }
    }
}
//MARK: ImagePreviewDelegate Methods

extension DetailController:ImagePreviewDelegate {
    func showImagesPreview(_ indexItem:Int, collectionTag:Int, imageType:ImageType) {
        self.view.endEditing(true)
        guard self.keyboardSize.height == 0.0 else {
            return
        }
        
        if(imagesPreview == nil) {
            imagesPreview = UINib(nibName: NIB_NAME.imagesPreview, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ImagesPreview
            imagesPreview.frame = UIScreen.main.bounds
            imagesPreview.imageCollectionView.frame = CGRect(x: imagesPreview.imageCollectionView.frame.origin.x, y: imagesPreview.imageCollectionView.frame.origin.y, width: self.view.frame.width, height: imagesPreview.imageCollectionView.frame.height)
            
            imagesPreview.delegate = self
            switch imageType {
            case .image:
                for i in (0..<Singleton.sharedInstance.selectedTaskDetails.addedImagesArray!.count) {
                    imagesPreview.imageArray.add(Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![i].taskDescription)
                }
                imagesPreview.imageType = ImageType.image.hashValue
                imagesPreview.captionArray = Singleton.sharedInstance.selectedTaskDetails.addedCaptionArray!
            case .customFieldImage:
                imagesPreview.imageArray = NSMutableArray(array: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionTag].imageArray)
                imagesPreview.imageType = ImageType.customFieldImage.hashValue
                imagesPreview.collectionViewTag = collectionTag
                imagesPreview.captionArray = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionTag].captionArray!
            case .taskDescription:
                imagesPreview.imageArray = NSMutableArray(array: Singleton.sharedInstance.selectedTaskDetails.referenceImageArray)
                imagesPreview.imageType = ImageType.taskDescription.hashValue
                imagesPreview.captionArray = Singleton.sharedInstance.selectedTaskDetails.referenceImagesCaptionArray!
            default:
                break
            }

            imagesPreview.backgroundColor = UIColor.clear
            self.imagesPreview.setCollectionViewWithPage(indexItem)
            self.view.addSubview(imagesPreview)
            
            imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                self.imagesPreview.transform = CGAffineTransform.identity
            }, completion: { finished in
                self.imagesPreview.backgroundColor = COLOR.imagePreviewBackgroundColor
            })
        }
    }
    
    func dismissImagePreview() {
        imagesPreview.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: { finished in
            self.imagesPreview.removeFromSuperview()
            self.imagesPreview = nil
        })
    }
    
    func deleteImageActionFromImages(_ index:Int) {
        self.deleteOptionalImage(index: index)
    }
    
    func deleteImageActionFromCustomField(_ indexItem: Int, collectionViewIndex: Int) {
        self.deleteImageFromCustomField(indexItem, collectionViewIndex: collectionViewIndex)
    }
}

extension DetailController:StatusModelDelegate {
    //MARK: Set Bottom Button View
    func setBottomButtonView() {
        self.model.setSectionsAndRows()
        guard self.jobDetailsView != nil else {
            return
        }
        
        guard self.jobDetailsView.detailsTable != nil else {
            return
        }
        self.jobDetailsView.detailsTable.reloadData()
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return
        }

        if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
                self.slider.isHidden = true
                self.jobDetailsView.bottomConstraintOfDetailTable.constant = 20.0
                return
            }
            self.updateSliderButton()
        } else {
            guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
                self.positiveStatusButton.isHidden = true
                self.negativeStatusButton.isHidden = true
                self.moreOptionButton.isHidden = true
                self.jobDetailsView.bottomConstraintOfDetailTable.constant = 20.0
                self.waitingView.isHidden = true
                self.waitingView.transform = CGAffineTransform.identity
                self.waitingLabel.transform = CGAffineTransform.identity
                self.activityIndicator.transform = CGAffineTransform.identity
                self.transLayer.alpha = 0.0
                return
            }
            
            self.positiveStatusButton.isHidden = false
            self.negativeStatusButton.isHidden = false
            self.moreOptionButton.isHidden = false
            self.positiveStatusButton.setTitle(self.statusModel.acceptTitle, for: .normal)
            self.negativeStatusButton.setTitle(self.statusModel.rejectTitle, for: .normal)
            animationForBottomView()
        }
    }
    
    func animationForBottomView() {
        if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            self.slider.resetView()
            self.jobDetailsView.bottomConstraintOfDetailTable.constant = self.sliderHeight
        } else {
            self.activityIndicator.stopAnimating()
            self.jobDetailsView.bottomConstraintOfDetailTable.constant = getAspectRatioValue(value: 70.0)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                self.waitingView.isHidden = true
                self.waitingView.transform = CGAffineTransform.identity
                self.waitingLabel.transform = CGAffineTransform.identity
                self.activityIndicator.transform = CGAffineTransform.identity
                self.view.updateConstraintsIfNeeded()
                switch(self.statusModel.viewShowStatus) {
                case SHOW_HIDE.showPartial,SHOW_HIDE.hidePartial:
                    self.transLayer.alpha = 0.0
                    self.positiveStatusButton.transform = CGAffineTransform.identity
                    self.negativeStatusButton.transform = CGAffineTransform.identity
                    self.moreOptionButton.transform = CGAffineTransform.identity
                    self.moreOptionButton.isHidden = true
                case SHOW_HIDE.showComplete:
                    self.moreOptionButton.isHidden = false
                    self.transLayer.alpha = 0.0
                    self.positiveStatusButton.transform = CGAffineTransform.identity
                    self.negativeStatusButton.transform = CGAffineTransform.identity
                    self.moreOptionButton.transform = CGAffineTransform.identity
                case SHOW_HIDE.hideComplete:
                    self.transLayer.alpha = 0.0
                    self.positiveStatusButton.transform = CGAffineTransform(translationX: 0, y: self.positiveStatusButton.frame.height)
                default:
                    break
                }
            }, completion: nil)

        }
    }
    
    func discardAlertPopup(_ oldJobStatus : Int, reason : String) {
        let alert = UIAlertController(title: TEXT.ADD_REASON, message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = reason
        })
        alert.addAction(UIAlertAction(title: TEXT.SUBMIT, style: .default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            if !(textField.text!.blank(textField.text!)){
                self.statusModel.viewShowStatus = SHOW_HIDE.hidePartial
                self.dismissPartial()
                if Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.failed {
                    self.statusModel.dictionary["description"] = "\(TEXT.FAILED) \(TEXT.AT)"// + TEXT.AT//"Failed at".localized
                    self.statusModel.taskUpdatingStatus = "Failed"
                    Singleton.sharedInstance.selectedTaskDetails.jobStatus = oldJobStatus
                    /*------------- Firebase ---------------*/
                    Analytics.logEvent(ANALYTICS_KEY.TASK_FAILED_SUBMITTED, parameters: ["Status":"Failed" as NSObject])
                    self.statusModel.changeJobStatus(jobStatus: JOB_STATUS.failed, reason: textField.text!)
                } else {
                    /*------------- Firebase ---------------*/
                    Analytics.logEvent(ANALYTICS_KEY.TASK_FAILED_SUBMITTED, parameters: ["Status":"Cancelled" as NSObject])
                    Singleton.sharedInstance.selectedTaskDetails.jobStatus = oldJobStatus
                    self.statusModel.dictionary["description"] = "\(TEXT.CANCELED) \(TEXT.AT)"//TEXT.CANCELED + " " + TEXT.AT //"Cancelled at".localized
                    self.statusModel.taskUpdatingStatus = "Cancelled"
                    self.statusModel.changeJobStatus(jobStatus: JOB_STATUS.canceled, reason: textField.text!)
                }
            } else{
                self.statusModel.rejectTask(oldJobStatus, reason: ERROR_MESSAGE.GIVING_REASON_MANDATORY)
            }
        }))
        alert.addAction(UIAlertAction(title: TEXT.DISCARD, style: .cancel, handler: { (action) -> Void in
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.TASK_FAILED_DISCARD, parameters: ["Status":Singleton.sharedInstance.selectedTaskDetails.jobStatus as NSObject])
            self.statusModel.viewShowStatus = SHOW_HIDE.showComplete
            self.animationForBottomView()
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = oldJobStatus
        }))
        if ((self.presentedViewController) != nil) {
            self.presentedViewController?.dismiss(animated: false, completion: nil)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func dismissPartial() {
        if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            if self.slider.direction == PAN_GESTURE_DIRECTION.right {
                self.slider.centerLabel.text = self.slider.loadingText
            } else {
                self.slider.centerLabel.text = TEXT.CANCELING
            }
        } else {
            self.jobDetailsView.bottomConstraintOfDetailTable.constant = getAspectRatioValue(value: 70.0)
            self.waitingView.isHidden = false
            self.waitingView.layer.masksToBounds = false
            self.waitingView.layer.shadowOffset = CGSize(width: 2, height: -3)
            self.waitingView.layer.shadowOpacity = 0.7
            self.waitingView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            self.activityIndicator.frame = CGRect(x: self.waitingLabel.frame.origin.x + self.waitingLabel.frame.width + 10, y: 0, width: 40, height: defaultHeightOfButtons)
            self.view.bringSubview(toFront: self.activityIndicator)
            self.activityIndicator.startAnimating()
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                self.transLayer.alpha = 1.0
                self.waitingView.transform = CGAffineTransform(translationX: 0, y: -self.positiveStatusButton.frame.height)
                self.positiveStatusButton.transform = CGAffineTransform(translationX: 0, y: self.positiveStatusButton.frame.height)
                self.waitingLabel.transform = CGAffineTransform(translationX: 0, y: -self.positiveStatusButton.frame.height)
            }, completion: nil)
        }
    }
    
    func dismissComplete() {
        if Singleton.sharedInstance.selectedTaskDetails.sliderOptionalField.value == 1 {
            self.jobDetailsView.bottomConstraintOfDetailTable.constant = 20.0
            self.slider.isHidden = true
        } else {
            self.activityIndicator.stopAnimating()
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                self.waitingView.transform = CGAffineTransform.identity
                self.positiveStatusButton.transform = CGAffineTransform(translationX: 0, y: self.positiveStatusButton.frame.height)
            }, completion: nil)

        }
    }

    func dismissConstroller() {
        self.backAction()
    }
    
    func updateJobIdTitle() {
        self.model.setSectionsAndRows()
        self.dismissPartial()
        self.translateViewToPoint(point: minimizeOrigin, translatingView: self.jobDetailsView)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.reloadDetailTable()
        }
    }
    func alertForSyncingFailed(_ jobStatus:Int, reason:String) {
        let alert = UIAlertController(title: TEXT.ERROR, message: ERROR_MESSAGE.SYNC_FAILED_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
        let tryAgain = UIAlertAction(title: TEXT.TRY_AGAIN, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.statusModel.checkUnsyncedTask(jobStatus, reason: reason)
            })
        })
        alert.addAction(tryAgain)
        
        let offlineData = UIAlertAction(title: TEXT.CONTINUE_OFFLINE, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.statusModel.saveOfflineData(jobStatus, reason: reason)
            })
        })
        alert.addAction(offlineData)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showGotoPickupTask(_ message:String, jobId:Int,pickupTaskDetails:TasksAssignedToDate) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: nil)
        let actionPickup = UIAlertAction(title: TEXT.GOTO_PICKUP, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.statusModel.gotoPickupTaskAction(pickupJobId: jobId)
        })
        alert.addAction(action)
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    func gotoInvoiceController() {
        self.setBottomButtonView()
        let invoiceController = InvoiceController()
        invoiceController.jobId = Singleton.sharedInstance.selectedTaskDetails.jobId
        invoiceController.htmlString = Singleton.sharedInstance.selectedTaskDetails.invoiceHtml
        self.present(invoiceController, animated: true, completion: nil)
    }
    
    func gotoEarningController(earningFormulaFields:[FORMULA_FIELDS],pricingFormulaFields:[FORMULA_FIELDS], totalDistane:String, totalDuration:String, hasTaskCompleted:Bool, isRatingAvailable:Int) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.completedTaskEarningController) as! CompletedTaskEarningController
        controller.earningsFormulaFields = earningFormulaFields
        controller.pricingFormulaFields = pricingFormulaFields
        controller.totalDuration = totalDuration
        controller.totalDistance = totalDistane
        controller.isRatingAvailable = isRatingAvailable == 1 ? true : false
        controller.isPricingAvailable = pricingFormulaFields.count > 0 ? true : false
        controller.isEarningsAvailable = earningFormulaFields.count > 0 ? true : false
        controller.jobId = "\(self.jobId!)"
        controller.completedTask = hasTaskCompleted
        if hasTaskCompleted == false {
            controller.dismissCallback = {
                self.backAction()
            }
        } else {
            controller.filledStars = Singleton.sharedInstance.selectedTaskDetails.customerRating!
        }
        self.present(controller, animated: true, completion: nil)
    }
}

extension DetailController:CustomSliderDelegate {
    func setSliderButton() {
        self.positiveStatusButton.isHidden = true
        self.negativeStatusButton.isHidden = true
        self.moreOptionButton.isHidden = true
        
        guard self.slider == nil else {
            self.statusModel.setJobStatusAndButtonTitle()
            if(self.jobDetailsView != nil) {
                UIView.setAnimationsEnabled(false)
                self.jobDetailsView.detailsTable.reloadData()
                UIView.setAnimationsEnabled(true)
            }
            return
        }
        
        self.slider = UINib(nibName: NIB_NAME.customSlider, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomSlider
        self.slider.frame = CGRect(x: CGFloat(0), y: SCREEN_SIZE.height - self.sliderHeight - Singleton.sharedInstance.getBottomInsetofSafeArea(), width: SCREEN_SIZE.width, height: self.sliderHeight)
        self.slider.delegate = self
        
        let safeAreaView = UIView(frame: CGRect(x: 0, y: SCREEN_SIZE.height - Singleton.sharedInstance.getBottomInsetofSafeArea(), width: SCREEN_SIZE.width, height: self.sliderHeight))
        safeAreaView.backgroundColor = UIColor.black
        self.view.addSubview(safeAreaView)
        self.view.addSubview(self.slider)
        self.slider.setShimmerView()
        
        /*============= Shadow =============*/
        self.slider.layer.masksToBounds = false
        self.slider.layer.shadowOffset = CGSize(width: 2, height: -3)
        self.slider.layer.shadowOpacity = 0.7
        self.slider.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        
        self.statusModel.setJobStatusAndButtonTitle()
        if(self.jobDetailsView != nil) {
            UIView.setAnimationsEnabled(false)
            self.jobDetailsView.detailsTable.reloadData()
            UIView.setAnimationsEnabled(true)
        }
    }
    
    func updateSliderButton() {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return
        }
        if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
            self.slider.isHidden = true
        } else {
            self.slider.isHidden = false
        }
        self.jobDetailsView.bottomConstraintOfDetailTable.constant = self.sliderHeight + 20
        var holdDuration = 0.0
        if self.slider.statusText != nil {
            self.slider.centerLabel.text = self.slider.statusText
            holdDuration = 1.0
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + holdDuration) {
            self.slider.hideLeftButton = self.statusModel.hideLeftButton
            self.slider.setThumbSliderAndMaxLimit()
            self.slider.centerLabel.alpha = 0.0
            self.slider.setLabeltext(left: self.statusModel.rejectTitle, right: self.statusModel.acceptTitle, center: self.statusModel.stateTitle, loadingText: self.statusModel.loadingTitle)
            let rgbColour = COLOR.SUCCESSFUL_COLOR.cgColor //self.statusModel.positiveStatusColor!.cgColor
            let rgbColours = rgbColour.components
            self.slider.setPositiveSlidingColor(r: (rgbColours?[0])!, g:  (rgbColours?[1])!, b:  (rgbColours?[2])!)
            
            let negativeColor = COLOR.FAILED_COLOR.cgColor
            let rgbValues = negativeColor.components
            self.slider.setNegativeSlidingColor(r: (rgbValues?[0])!, g:  (rgbValues?[1])!, b:  (rgbValues?[2])!)
            
            self.slider.resetView()
        }
    }
    
    func sliderPositiveAction() {
        self.dismissPartial()
        if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
            self.acceptTask()
        } else {
            if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
                if self.statusModel.showMandatoryFieldsAlert(Singleton.sharedInstance.selectedTaskDetails) == true {
                    self.acceptTask()
                } else {
                    self.animationForBottomView()
                    if self.jobDetailsView.transform.ty > 0 {
                        self.directionButton.alpha = 0.0
                        self.translateViewToPoint(point: 0, translatingView: self.jobDetailsView)
                    }
                    self.jobDetailsView.detailsTable.reloadData()
                }
            } else {
                self.acceptTask()
            }
        }
    }
    
    func sliderNegativeAction() {
        if self.statusModel.negativeStatusAction(self.statusModel.rejectTitle) == false {
            self.animationForBottomView()
            if self.jobDetailsView.transform.ty > 0 {
                self.directionButton.alpha = 0.0
                self.translateViewToPoint(point: 0, translatingView: self.jobDetailsView)
            }
        }
    }
}
