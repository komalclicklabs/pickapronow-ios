//
//  DatabaseManager.swift
//  Tookan
//
//  Created by Rakesh Kumar on 5/16/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import CoreData

class DatabaseManager: NSObject {

    static let sharedInstance = DatabaseManager()
    
    fileprivate override init() {
    }

    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.clicklabs.CoreDataSwift2" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Tookan", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Tookan.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [NSMigratePersistentStoresAutomaticallyOption: true,
                NSInferMappingModelAutomaticallyOption: true])
        } catch let error as NSError {
            // Report any error we got.
            var dict = [String: String]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error.localizedDescription //as Error
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    //MARK: FleetInfoDetails Data
    func fetchUserInfoFromDatabase(_ userManagedContext:NSManagedObjectContext)-> FleetInfoDetails!{
        
        let request: NSFetchRequest<UserInfo>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = UserInfo.fetchRequest() as! NSFetchRequest<UserInfo>
        } else {
            request = NSFetchRequest(entityName: "UserInfo")
        }
        do {
            let results = try userManagedContext.fetch(request)
            if results.count > 0 {
                let userData = NSData(data: results.first?.value(forKey: "info") as! Data) as Data
                let userDetails = NSKeyedUnarchiver.unarchiveObject(with: userData) as! FleetInfoDetails
                return userDetails
            }else{
                print("0 results or potential error")
            }
        } catch let error {
            print(error.localizedDescription)
        }
        
        
//        if #available(iOS 10.0, *) {
//            let request:NSFetchRequest<NSFetchRequestResult> = UserInfo.fetchRequest()
//            do {
//                if let results = try userManagedContext.fetch(request) as? [UserInfo] {
//                    if results.count > 0 {
//                        let userData = NSData(data: results.first?.value(forKey: "info") as! Data) as Data
//                        let userDetails = NSKeyedUnarchiver.unarchiveObject(with: userData) as! FleetInfoDetails
//                        return userDetails
//                    }else{
//                        print("0 results or potential error")
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        }
        
        
        
        
        return nil
    }
    
    func saveUserInfoInDatabase(_ userDetails: FleetInfoDetails, syncStatus:Int, userManagedContext:NSManagedObjectContext) {
        self.deleteUserInfoFromDatabase(userManagedContext)
        let entityDescription = NSEntityDescription.entity(forEntityName: "UserInfo", in: userManagedContext)
        let data = UserInfo(entity: entityDescription!, insertInto: userManagedContext)
        data.info = NSKeyedArchiver.archivedData(withRootObject: userDetails) as NSObject
        data.sync = NSNumber(value: syncStatus) //NSNumber(syncStatus)
        do {
            try userManagedContext.save()
        } catch {
            Singleton.sharedInstance.showErrorMessage(error: "Failure to save context: \(error.localizedDescription)", isError: .error)
        }
    }
    
    func deleteUserInfoFromDatabase(_ userManagedContext:NSManagedObjectContext) {
        let request: NSFetchRequest<UserInfo>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = UserInfo.fetchRequest() as! NSFetchRequest<UserInfo>
        } else {
            request = NSFetchRequest(entityName: "UserInfo")
        }
        
        if #available(iOS 9.0, *) {
            do {
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
                try persistentStoreCoordinator.execute(deleteRequest, with: userManagedContext)
                do {
                    try userManagedContext.save()
                } catch {
                    let saveError = error as NSError
                    print(saveError)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
                // Fallback on earlier versions
                do {
                    
                    let results = try userManagedContext.fetch(request)
                    if results.count > 0 {
                        userManagedContext.delete((results.first)!)
                        do {
                            try userManagedContext.save()
                        } catch {
                            let saveError = error as NSError
                            print(saveError)
                        }
                    }else{
                        print("0 results or potential error")
                    }
                } catch let error {
                     print(error.localizedDescription)
                }
            }
        
        
//        if #available(iOS 9.0, *) {
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: userManagedContext)
//                do {
//                    try userManagedContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            do {
//                if let results = try userManagedContext.fetch(fetchRequest) as? [UserInfo] {
//                    if results.count > 0 {
//                        userManagedContext.delete((results.first)!)
//                        do {
//                            try userManagedContext.save()
//                        } catch {
//                            let saveError = error as NSError
//                            print(saveError)
//                        }
//                    }else{
//                        print("0 results or potential error")
//                    }
//
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        }
    }
    
    func deleteNotificationDataFromDatabase() {
        let request: NSFetchRequest<Notification>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = Notification.fetchRequest() as! NSFetchRequest<Notification>
        } else {
            request = NSFetchRequest(entityName: "Notification")
        }
        
        let entity = ["Notification"]
        for entityName in entity {
            request.entity = NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)
            request.includesPropertyValues = false
            do {
                let notificationDataFromDatabase = try managedObjectContext.fetch(request)
                for managedObj: NSManagedObject in notificationDataFromDatabase {
                    managedObjectContext.delete(managedObj)
                }
                do {
                    try managedObjectContext.save()
                } catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
//        if #available(iOS 10.0, *) {
//            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = Notification.fetchRequest()
//            let entity = ["Notification"]
//            for entityName in entity {
//                fetchRequest.entity = NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)
//                fetchRequest.includesPropertyValues = false
//                do {
//                    if let notificationDataFromDatabase = try managedObjectContext.fetch(fetchRequest) as? [Notification] {
//                        for managedObj: NSManagedObject in notificationDataFromDatabase
//                        {
//                            managedObjectContext.delete(managedObj)
//                        }
//                        do {
//                            try managedObjectContext.save()
//                        } catch {
//                            let saveError = error as NSError
//                            print(saveError)
//                        }
//                    }
//                } catch {
//                    fatalError("Failed to fetch person: \(error)")
//                }
//            }
//        } else {
//            // Fallback on earlier versions
//        }
//        let fetchRequest = NSFetchRequest()
//        let entity = ["Notification"]
        
    }
    
    
    //MARK: Assigned Task Data
    func fetchAssignedTaskFromDatabase(_ taskManagedContext:NSManagedObjectContext, selectedDate:String)-> [Task]!{
        
        let request: NSFetchRequest<Task>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = Task.fetchRequest() as! NSFetchRequest<Task>
        } else {
            request = NSFetchRequest(entityName: "Task")
        }
        request.returnsObjectsAsFaults=false
        request.predicate = NSPredicate(format:"timestamp = %@", selectedDate)
        do {
            let results = try taskManagedContext.fetch(request)
            return results
        } catch let error {
            print(error.localizedDescription)
        }

        
//        if #available(iOS 10.0, *) {
//            let request:NSFetchRequest<NSFetchRequestResult> = Task.fetchRequest()
//            request.returnsObjectsAsFaults=false
//            request.predicate = NSPredicate(format:"timestamp = %@", selectedDate)
//            do {
//                let results = try taskManagedContext.fetch(request) as? [Task]
//                return results
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//
//        } else {
//            // Fallback on earlier versions
//        }
        //let request = NSFetchRequest(entityName: "Task")
        return nil
    }
    
    func fetchAssignedTaskFromDatabase(_ taskManagedContext:NSManagedObjectContext, jobId:Int)-> Task!{
       // let request = NSFetchRequest(entityName: "Task")
        let request: NSFetchRequest<Task>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = Task.fetchRequest() as! NSFetchRequest<Task>
        } else {
            request = NSFetchRequest(entityName: "Task")
        }
        request.returnsObjectsAsFaults=false
        request.predicate = NSPredicate(format:"job_id = %d", jobId)
        do {
            let results = try taskManagedContext.fetch(request)
            return results.first
        } catch let error {
            print(error.localizedDescription)
        }
        
        
//        if #available(iOS 10.0, *) {
//            let request:NSFetchRequest<NSFetchRequestResult> = Task.fetchRequest()
//            request.returnsObjectsAsFaults=false
//            request.predicate = NSPredicate(format:"job_id = %d", jobId)
//            do {
//                let results = try taskManagedContext.fetch(request) as? [Task]
//                return results?.first
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//
//        } else {
//            // Fallback on earlier versions
//        }
                return nil
    }
    
    func fetchAllUnsyncTaskFromDatabase(_ taskManagedContext:NSManagedObjectContext) -> [Task]! {
        
        let request: NSFetchRequest<Task>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = Task.fetchRequest() as! NSFetchRequest<Task>
        } else {
            request = NSFetchRequest(entityName: "Task")
        }
        request.returnsObjectsAsFaults=false
        request.predicate = NSPredicate(format:"sync = %d", 0)
        do {
            let results = try taskManagedContext.fetch(request)
            return results
        } catch let error {
            print(error.localizedDescription)
        }
        
        
//        if #available(iOS 10.0, *) {
//            let request:NSFetchRequest<NSFetchRequestResult> = Task.fetchRequest()
//            request.returnsObjectsAsFaults=false
//            request.predicate = NSPredicate(format:"sync = %d", 0)
//            do {
//                let results = try taskManagedContext.fetch(request) as? [Task]
//                return results
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
//        let request = NSFetchRequest(entityName: "Task")
       
        return nil
    }
    
    func updateAssignedTaskOnDatabase(_ taskManagedContext:NSManagedObjectContext, jobId:Int, taskDetails: TasksAssignedToDate, syncStatus:Int) -> Bool {
       // let request = NSFetchRequest(entityName: "Task")
        let request: NSFetchRequest<Task>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = Task.fetchRequest() as! NSFetchRequest<Task>
        } else {
            request = NSFetchRequest(entityName: "Task")
        }
        request.predicate = NSPredicate(format:"job_id = %d", jobId)
        do {
            let results = try taskManagedContext.fetch(request)
            if(results.count > 0) {
                let data = results[0]
                data.task_object = NSKeyedArchiver.archivedData(withRootObject: taskDetails) as NSObject
                data.sync = NSNumber(value:syncStatus)
                do {
                    try taskManagedContext.save()
                    return true
                } catch {
                    Singleton.sharedInstance.showErrorMessage(error: "Failure to save context: \(error.localizedDescription)", isError: .error)
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        
        
        
//        if #available(iOS 10.0, *) {
//            let request:NSFetchRequest<NSFetchRequestResult> = Task.fetchRequest()
//            request.predicate = NSPredicate(format:"job_id = %d", jobId)
//            do {
//                if let results = try taskManagedContext.fetch(request) as? [Task] {
//                    if(results.count > 0) {
//                        let data = results[0]
//                        data.task_object = NSKeyedArchiver.archivedData(withRootObject: taskDetails) as NSObject
//                        data.sync = NSNumber(value:syncStatus)
//                        do {
//                            try taskManagedContext.save()
//                            return true
//                        } catch {
//                            fatalError("Failure to save context: \(error)")
//                        }
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
        
        return false
    }
    
    func saveAssignedTaskToDatabase(_ taskDetails: TasksAssignedToDate, jobId:Int, timeStamp:String, syncStatus:Int, taskManagedContext:NSManagedObjectContext) -> Bool {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Task", in: taskManagedContext)
        let data = Task(entity: entityDescription!, insertInto: taskManagedContext)
        data.task_object = NSKeyedArchiver.archivedData(withRootObject: taskDetails) as NSObject
        data.job_id = NSNumber(value: jobId)
        data.timestamp = timeStamp
        data.sync = NSNumber(value:syncStatus)
        do {
            try taskManagedContext.save()
            return true
        } catch {
            Singleton.sharedInstance.showErrorMessage(error: "Failure to save context: \(error.localizedDescription)", isError: .error)
        }
        return false
    }
    
    func deleteAssignedTaskFromDatabase(_ taskManagedContext:NSManagedObjectContext) {
       // let fetchRequest = NSFetchRequest(entityName: "Task")
        let request: NSFetchRequest<Task>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = Task.fetchRequest() as! NSFetchRequest<Task>
        } else {
            request = NSFetchRequest(entityName: "Task")
        }
        
        if #available(iOS 9.0, *) {
            do {
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
                try persistentStoreCoordinator.execute(deleteRequest, with: taskManagedContext)
                do {
                    try taskManagedContext.save()
                } catch {
                    let saveError = error as NSError
                    print(saveError)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            // Fallback on earlier versions
            do {
                let results = try taskManagedContext.fetch(request)
                if results.count > 0 {
                    for taskData in results {
                        taskManagedContext.delete(taskData)
                        do {
                            try taskManagedContext.save()
                        } catch {
                        let saveError = error as NSError
                            print(saveError)
                        }
                    }
                } else{
                    print("0 results or potential error")
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        
//        if #available(iOS 10.0, *) {
//            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = Task.fetchRequest()
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: taskManagedContext)
//                do {
//                    try taskManagedContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
        
        
//        if #available(iOS 9.0, *) {
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: taskManagedContext)
//                do {
//                    try taskManagedContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            do {
//                if let results = try taskManagedContext.fetch(fetchRequest) as? [Task] {
//                    if results.count > 0 {
//                        for taskData in results {
//                            taskManagedContext.delete(taskData)
//                            do {
//                                try taskManagedContext.save()
//                            } catch {
//                                let saveError = error as NSError
//                                print(saveError)
//                            }
//                        }
//                    }else{
//                        print("0 results or potential error")
//                    }
//
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        }
    }
    
    //MARK: Polyline Table
    func fetchPolylineFromDatabase(_ userManagedContext:NSManagedObjectContext, selectedDate:String)-> String!{
       // let request = NSFetchRequest(entityName: "Polyline")
        let request: NSFetchRequest<Polyline>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = Polyline.fetchRequest() as! NSFetchRequest<Polyline>
        } else {
            request = NSFetchRequest(entityName: "Polyline")
        }
        request.returnsObjectsAsFaults=false
        request.predicate = NSPredicate(format:"date=%@", selectedDate)
        do {
            let results = try userManagedContext.fetch(request)
            if results.count > 0 {
                return results.first?.value(forKey: "polyline") as! String
            }else{
                print("0 results or potential error")
            }
        } catch let error {
            print(error.localizedDescription)
        }
        
//        if #available(iOS 10.0, *) {
//            let request:NSFetchRequest<NSFetchRequestResult> = Polyline.fetchRequest()
//            request.returnsObjectsAsFaults=false
//            request.predicate = NSPredicate(format:"date=%@", selectedDate)
//            do {
//                if let results = try userManagedContext.fetch(request) as? [Polyline] {
//                    if results.count > 0 {
//                        return results.first?.value(forKey: "polyline") as! String
//                    }else{
//                        print("0 results or potential error")
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
        return nil
    }
    
    func savePolylineToDatabase(_ polyline: String, timeStamp:String, syncStatus:Int, userManagedContext:NSManagedObjectContext) {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Polyline", in: userManagedContext)
        let data = Polyline(entity: entityDescription!, insertInto: userManagedContext)
        data.polyline = polyline
        data.date = timeStamp
        data.sync = NSNumber(value:syncStatus)
        do {
            try userManagedContext.save()
        } catch {
            Singleton.sharedInstance.showErrorMessage(error: "Failure to save context: \(error.localizedDescription)", isError: .error)
        }
    }
    
    func deletePolylineFromDatabase(_ userManagedContext:NSManagedObjectContext) {
        let request: NSFetchRequest<Polyline>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = Polyline.fetchRequest() as! NSFetchRequest<Polyline>
        } else {
            request = NSFetchRequest(entityName: "Polyline")
        }
        
        if #available(iOS 9.0, *) {
            do {
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
                try persistentStoreCoordinator.execute(deleteRequest, with: userManagedContext)
                do {
                    try userManagedContext.save()
                } catch {
                    let saveError = error as NSError
                    print(saveError)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            // Fallback on earlier versions
            do {
                let results = try userManagedContext.fetch(request)
                if results.count > 0 {
                    for taskData in results {
                        userManagedContext.delete(taskData)
                        do {
                            try userManagedContext.save()
                        } catch {
                            let saveError = error as NSError
                            print(saveError)
                        }
                    }
                } else{
                    print("0 results or potential error")
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        
//        if #available(iOS 10.0, *) {
//            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = Polyline.fetchRequest()
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: userManagedContext)
//                do {
//                    try userManagedContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
        //let fetchRequest = NSFetchRequest(entityName: "Polyline")
//        if #available(iOS 9.0, *) {
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: userManagedContext)
//                do {
//                    try userManagedContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            do {
//                if let results = try userManagedContext.fetch(fetchRequest) as? [Polyline] {
//                    if results.count > 0 {
//                        for taskData in results {
//                            userManagedContext.delete(taskData)
//                            do {
//                                try userManagedContext.save()
//                            } catch {
//                                let saveError = error as NSError
//                                print(saveError)
//                            }
//                        }
//                    } else {
//                        print("0 results or potential error")
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        }
    }
    
    //MARK: Unsynced Task Table
    func fetchUnsyncedTaskFromDatabase(_ userManagedContext:NSManagedObjectContext, jobID:Int) -> [UnsyncedTask]!{
        //let request = NSFetchRequest(entityName: "UnsyncedTask")
        
        let request: NSFetchRequest<UnsyncedTask>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = UnsyncedTask.fetchRequest() as! NSFetchRequest<UnsyncedTask>
        } else {
            request = NSFetchRequest(entityName: "UnsyncedTask")
        }
        request.returnsObjectsAsFaults=false
        request.predicate = NSPredicate(format:"job_id = %d", jobID)
        do {
            let results = try userManagedContext.fetch(request)
            if results.count > 0 {
                return results
            }else{
                print("0 results or potential error")
            }
        } catch let error {
            print(error.localizedDescription)
        }
        
        
//        let request: NSFetchRequest<UnsyncedTask>
//        if #available(iOS 10.0, *) {
//            request = UnsyncedTask.fetchRequest() as! NSFetchRequest<UnsyncedTask>
//            request.returnsObjectsAsFaults=false
//            request.predicate = NSPredicate(format:"job_id = %d", jobID)
//            do {
//                let results = try userManagedContext.fetch(request) {
//                
//                    if results.count > 0 {
//                    return results
//                }else{
//                    print("0 results or potential error")
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//             request = NSFetchRequest(entityName: "UnsyncedTask")
//        }
        
        return nil
    }
    
    func saveStatusForUnsyncedTaskToDatabase(_ jobId: Int, jobStatus:Int, latitude:String, longitude:String, reason:String, userManagedContext:NSManagedObjectContext, status:String) -> Bool {

        let entityDescription = NSEntityDescription.entity(forEntityName: "UnsyncedTask", in: userManagedContext)
        let data = UnsyncedTask(entity: entityDescription!, insertInto: userManagedContext)
        data.job_id = NSNumber(value: jobId)
        data.status = status
        data.job_status = NSNumber(value:jobStatus)
        data.timestamp = Auxillary.getLocalDateString()
        data.latitude = latitude
        data.longitude = longitude
        data.reason = reason
        do {
            try userManagedContext.save()
            return true
        } catch {
            Singleton.sharedInstance.showErrorMessage(error: "Failure to save context: \(error.localizedDescription)", isError: .error)
        }
        return false
    }
    
    func deleteUnsyncedTaskFromDatabase(_ jobId:Int, userManagedContext:NSManagedObjectContext) {
       // let fetchRequest = NSFetchRequest(entityName: "UnsyncedTask")
        let request: NSFetchRequest<UnsyncedTask>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = UnsyncedTask.fetchRequest() as! NSFetchRequest<UnsyncedTask>
        } else {
            request = NSFetchRequest(entityName: "UnsyncedTask")
        }
        
        request.returnsObjectsAsFaults=false
        request.predicate = NSPredicate(format:"job_id = %d", jobId)
        
        if #available(iOS 9.0, *) {
            do {
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
                try persistentStoreCoordinator.execute(deleteRequest, with: userManagedContext)
                do {
                    try userManagedContext.save()
                } catch {
                    let saveError = error as NSError
                    print(saveError)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            // Fallback on earlier versions
            do {
                let results = try userManagedContext.fetch(request)
                if results.count > 0 {
                    for taskData in results {
                        userManagedContext.delete(taskData)
                        do {
                            try userManagedContext.save()
                        } catch {
                            let saveError = error as NSError
                            print(saveError)
                        }
                    }
                } else{
                    print("0 results or potential error")
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        
        
        
//        if #available(iOS 10.0, *) {
//            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = UnsyncedTask.fetchRequest()
//            fetchRequest.returnsObjectsAsFaults=false
//            fetchRequest.predicate = NSPredicate(format:"job_id = %d", jobId)
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: userManagedContext)
//                do {
//                    try userManagedContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
//        fetchRequest.returnsObjectsAsFaults=false
//        fetchRequest.predicate = NSPredicate(format:"job_id = %d", jobId)
//        if #available(iOS 9.0, *) {
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: userManagedContext)
//                do {
//                    try userManagedContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            do {
//                if let results = try userManagedContext.fetch(fetchRequest) as? [UnsyncedTask] {
//                    if results.count > 0 {
//                        for result in results {
//                            userManagedContext.delete(result)
//                            do {
//                                try userManagedContext.save()
//                            } catch {
//                                let saveError = error as NSError
//                                print(saveError)
//                            }
//                        }
//                    }else{
//                        print("0 results or potential error")
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        }
    }
    
    func deleteAllUnsyncedTaskFromDatabase() {
        //let fetchRequest = NSFetchRequest(entityName: "UnsyncedTask")
        let request: NSFetchRequest<UnsyncedTask>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = UnsyncedTask.fetchRequest() as! NSFetchRequest<UnsyncedTask>
        } else {
            request = NSFetchRequest(entityName: "UnsyncedTask")
        }
        
        
        if #available(iOS 9.0, *) {
            do {
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
                try persistentStoreCoordinator.execute(deleteRequest, with: self.managedObjectContext)
                do {
                    try self.managedObjectContext.save()
                } catch {
                    let saveError = error as NSError
                    print(saveError)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            // Fallback on earlier versions
            do {
                let results = try self.managedObjectContext.fetch(request)
                if results.count > 0 {
                    for taskData in results {
                        self.managedObjectContext.delete(taskData)
                        do {
                            try self.managedObjectContext.save()
                        } catch {
                            let saveError = error as NSError
                            print(saveError)
                        }
                    }
                } else{
                    print("0 results or potential error")
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }

        
        
        
//        if #available(iOS 10.0, *) {
//            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = UnsyncedTask.fetchRequest()
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: self.managedObjectContext)
//                do {
//                    try self.managedObjectContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
//        if #available(iOS 9.0, *) {
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: self.managedObjectContext)
//                do {
//                    try self.managedObjectContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            do {
//                if let results = try self.managedObjectContext.fetch(fetchRequest) as? [Task] {
//                    if results.count > 0 {
//                        for taskData in results {
//                            self.managedObjectContext.delete(taskData)
//                            do {
//                                try self.managedObjectContext.save()
//                            } catch {
//                                let saveError = error as NSError
//                                print(saveError)
//                            }
//                        }
//                    } else {
//                        print("0 results or potential error")
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        }
    }
    
    
    
    //MARK: Image Table
    func fetchImagesFromDatabase(_ imagesManagedContext:NSManagedObjectContext) -> [PendingImages]!{
       // let request = NSFetchRequest(entityName: "PendingImages")
        let request: NSFetchRequest<PendingImages>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = PendingImages.fetchRequest() as! NSFetchRequest<PendingImages>
        } else {
            request = NSFetchRequest(entityName: "PendingImages")
        }
        do {
            let results = try imagesManagedContext.fetch(request)
            if results.count > 0 {
                return results
            }else{
                print("0 results or potential error")
            }
        } catch let error {
            print(error.localizedDescription)
        }

        
        
        
        
//        if #available(iOS 10.0, *) {
//            let request:NSFetchRequest<NSFetchRequestResult> = PendingImages.fetchRequest()
//            do {
//                if let results = try imagesManagedContext.fetch(request) as? [PendingImages] {
//                    if results.count > 0 {
//                        return results
//                    }else{
//                        print("0 results or potential error")
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
        return nil
    }
    
    func saveOfflineImagesToDatabase(_ jobId: Int, image:String,caption:String, label:String, imageManagedContext:NSManagedObjectContext, type:Int) -> Bool {
        let entityDescription = NSEntityDescription.entity(forEntityName: "PendingImages", in: imageManagedContext)
        let data = PendingImages(entity: entityDescription!, insertInto: imageManagedContext)
        data.job_id = NSNumber(value:jobId)
        data.image = image
        data.custom_label = label
        data.type = NSNumber(value:type)
        data.caption = caption
        do {
            try imageManagedContext.save()
            return true
        } catch {
            Singleton.sharedInstance.showErrorMessage(error: "Failure to save context: \(error.localizedDescription)", isError: .error)
        }
        return false
    }
    
    func deleteImageFromDatabase(_ deletingImage:String, imageManagedContext:NSManagedObjectContext) -> Bool {
        //let request = NSFetchRequest(entityName: "PendingImages")
        let request: NSFetchRequest<PendingImages>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = PendingImages.fetchRequest() as! NSFetchRequest<PendingImages>
        } else {
            request = NSFetchRequest(entityName: "PendingImages")
        }
        
        request.returnsObjectsAsFaults=false
        request.predicate = NSPredicate(format:"image = %@", deletingImage)
        
            do {
                let results = try imageManagedContext.fetch(request)
                if(results.count > 0) {
                    imageManagedContext.delete(results[0])
                    do {
                        try imageManagedContext.save()
                        return true
                    } catch {
                        let saveError = error as NSError
                    print(saveError)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        
        
//        if #available(iOS 10.0, *) {
//            let request:NSFetchRequest<NSFetchRequestResult> = PendingImages.fetchRequest()
//            request.returnsObjectsAsFaults=false
//            request.predicate = NSPredicate(format:"image = %@", deletingImage)
//            do {
//                if let results = try imageManagedContext.fetch(request) as? [PendingImages] {
//                    if(results.count > 0) {
//                        imageManagedContext.delete(results[0])
//                        do {
//                            try imageManagedContext.save()
//                            return true
//                        } catch {
//                            let saveError = error as NSError
//                            print(saveError)
//                        }
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
        
        return false
    }
    
    func deleteAllImagesFromDatabase() {
       // let fetchRequest = NSFetchRequest(entityName: "PendingImages")
        let request: NSFetchRequest<PendingImages>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = PendingImages.fetchRequest() as! NSFetchRequest<PendingImages>
        } else {
            request = NSFetchRequest(entityName: "PendingImages")
        }
        
        
        if #available(iOS 9.0, *) {
            do {
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
                try persistentStoreCoordinator.execute(deleteRequest, with: self.managedObjectContext)
                do {
                    try self.managedObjectContext.save()
                } catch {
                    let saveError = error as NSError
                    print(saveError)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            // Fallback on earlier versions
            do {
                let results = try self.managedObjectContext.fetch(request)
                if results.count > 0 {
                    for taskData in results {
                        self.managedObjectContext.delete(taskData)
                        do {
                            try self.managedObjectContext.save()
                        } catch {
                            let saveError = error as NSError
                            print(saveError)
                        }
                    }
                } else{
                    print("0 results or potential error")
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        
//        if #available(iOS 10.0, *) {
//            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = PendingImages.fetchRequest()
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: self.managedObjectContext)
//                do {
//                    try self.managedObjectContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            // Fallback on earlier versions
//        }
//        if #available(iOS 9.0, *) {
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            do {
//                try persistentStoreCoordinator.execute(deleteRequest, with: self.managedObjectContext)
//                do {
//                    try self.managedObjectContext.save()
//                } catch {
//                    let saveError = error as NSError
//                    print(saveError)
//                }
//            } catch let error as NSError {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        } else {
//            do {
//                if let results = try self.managedObjectContext.fetch(fetchRequest) as? [Task] {
//                    if results.count > 0 {
//                        for taskData in results {
//                            self.managedObjectContext.delete(taskData)
//                            do {
//                                try self.managedObjectContext.save()
//                            } catch {
//                                let saveError = error as NSError
//                                print(saveError)
//                            }
//                        }
//                    } else {
//                        print("0 results or potential error")
//                    }
//                }
//            } catch {
//                let saveError = error as NSError
//                print(saveError)
//            }
//        }
    }
}
