//
//  HomeBaseController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 15/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Crashlytics
import MessageUI
import MapKit
import SDKDemo1
import AVFoundation

enum SwipeDirection {
    case topToBottom
    case bottomToTop
}

class HomeBaseController: UIViewController, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate {
    var filteredTasksListArray = [[TasksAssignedToDate]]()
    var searchedTasksListArray = [[TasksAssignedToDate]]()
    var runningJobsView:JobListTable!
    var panDirection:SwipeDirection!
    var navigation:navigationBar!
    var internetToast:NoInternetConnectionView!
    var mapIndex = 0
    var locationMarkerModel = [LocationMarkerModel]()
    var locationMarker: GMSMarker!
    var hasRouting:Int!
    var previousIndex = 0
    var directionButton:UIButton!
    var maxHideLimit:CGFloat = 40.0
    var menuView:MenuList!
    var myCurrentLocation:CLLocation!
    var taskDetailViewController:DetailController!
    let leftRightMargin:CGFloat = 5.0
    let runningJobOrigin:CGFloat = 75.0
    var menuViewOrigin:CGFloat = SCREEN_SIZE.height
    let maxTranslatingPointToMinimize:CGFloat = getAspectRatioValue(value: 150.0)//150
    var minimizeOrigin:CGFloat = getAspectRatioValue(value: SCREEN_SIZE.height - getAspectRatioValue(value: 188.0))
    let taskObjectContext = DatabaseManager.sharedInstance.managedObjectContext
    let jobModel = JobModel()
    let defaultHeightOfButtons:CGFloat = 60.0
    var isItFirstTimeForNewTaskButton = true
    var forceTouch = false
    var pathSummaryView:PathSummaryView?
    var scanView:ScanView!
    var warningView:TopWarningView!
    var navigationYPos:CGFloat = 0.0
    var navigationHeight = HEIGHT.navigationHeight
    
    
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = COLOR.themeForegroundColor
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        let attributedString = NSMutableAttributedString(string: TEXT.PULL_TO_REFRESH, attributes: [NSForegroundColorAttributeName:COLOR.themeForegroundColor, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.medium)!])//NSAttributedString(string: TEXT.PULL_TO_REFRESH)
        attributedString.addAttribute(NSKernAttributeName, value: 1.8, range: NSRange(location: 0, length: TEXT.PULL_TO_REFRESH.characters.count))
        refreshControl.attributedTitle = attributedString
        return refreshControl
    }()
    
    @IBOutlet var nextTaskButton: UIButton!
    @IBOutlet var googleMapView: GMSMapView!
    @IBOutlet var previousTaskButton: UIButton!
    @IBOutlet var newButton: UIButton!
    @IBOutlet var currentLocationButton: UIButton!
    @IBOutlet var currentLocationBottomConstraint: NSLayoutConstraint!
    @IBOutlet var bottomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if DEVICE_TYPE != "1" && DEVICE_TYPE != "3" {
            Singleton.sharedInstance.isNotificationEnabled(completion: { (isEnabled) in
                if isEnabled == false {
                    self.setNotificationWarningView()
                }
            })
        }
        
        guard UserDefaults.standard.value(forKey: USER_DEFAULT.appVersion) != nil else {
            Singleton.sharedInstance.showInvalidAccessTokenPopup(message: ERROR_MESSAGE.NEW_VERSION_UPDATE)
            return
        }
        
        LocationTracker.sharedInstance().checkToStartLocationTracking()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.pushNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCurrentDayTaskList), name: NSNotification.Name(rawValue: OBSERVER.pushNotification), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.viewTravelSummary), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showTravelSummary), name: NSNotification.Name(rawValue: OBSERVER.viewTravelSummary), object: nil)
        
        if Singleton.sharedInstance.getBottomInsetofSafeArea() != 0 {
            self.maxHideLimit = self.maxHideLimit + Singleton.sharedInstance.getBottomInsetofSafeArea() + 15
        }
        self.menuViewOrigin = SCREEN_SIZE.height - runningJobOrigin - maxHideLimit
        self.minimizeOrigin = SCREEN_SIZE.height - runningJobOrigin - self.defaultHeightOfButtons - getAspectRatioValue(value: 100)
        self.automaticallyAdjustsScrollViewInsets = false

        Singleton.sharedInstance.initilizeFugu()
        self.setNavigationBar()
        self.setTaskButton(index: 0)
        self.setRunningJobListView()
        self.setDirectionButton()
        self.setCurrentLocationButton()
        self.setNewTaskButton()
        if #available(iOS 9.0, *) {
            if traitCollection.forceTouchCapability == .available {
                registerForPreviewing(with: self, sourceView: runningJobsView.taskListTable)
            } else {
                print("3D Touch Not Available")
            }
        }
        
        if Singleton.sharedInstance.fromPush == true {
            if NetworkingHelper.sharedInstance.notificationArray.count > 0 {
                FuguConfig.shared.handleRemoteNotification(userInfo: NetworkingHelper.sharedInstance.notificationArray[0].jsonObject)
            } else if Singleton.sharedInstance.pushDataOfFugu != nil {
                FuguConfig.shared.handleRemoteNotification(userInfo: Singleton.sharedInstance.pushDataOfFugu!)
            }
            Singleton.sharedInstance.fromPush = false
        }
        self.view.bringSubview(toFront: self.bottomView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setGoogleMap()
        self.forceTouch = false
        UIApplication.shared.statusBarStyle = .lightContent
        self.getCurrentDayTasksList(Date().formattedWith(NetworkingHelper.sharedInstance.currentDate))
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCurrentDayTaskList), name: NSNotification.Name(rawValue: OBSERVER.changeJobStatusResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFilterData), name: NSNotification.Name(rawValue: OBSERVER.filterData), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFilterData), name: NSNotification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.putOffDuty), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateOnOffDuty), name: NSNotification.Name(rawValue: OBSERVER.putOffDuty), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setNotificationBadgeCount), name: NSNotification.Name(rawValue: OBSERVER.pushNotificationCount), object: nil)
        if Singleton.sharedInstance.forceTouch == true {
            self.gotoNotificationController()
            Singleton.sharedInstance.forceTouch = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.changeJobStatusResponse), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.filterData), object: nil)
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.pushNotification), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.putOffDuty), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.pushNotificationCount), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let isTrafficLayer = UserDefaults.standard.value(forKey: USER_DEFAULT.trafficLayer) as? Bool {
            if isTrafficLayer == true {
                self.googleMapView.isTrafficEnabled = true
            } else {
                self.googleMapView.isTrafficEnabled = false
            }
        } else {
            self.googleMapView.isTrafficEnabled = false
        }
        if self.runningJobsView.transform.ty == SCREEN_SIZE.height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.runningJobsView.alpha = 1
                self.directionButton.alpha = 0.0
                self.translateViewToPoint(point: 0, translatingView: self.runningJobsView)
            })
        }
    }
    
    func setNotificationWarningView() {
        if self.warningView == nil {
            if let view = UINib(nibName: NIB_NAME.topWarningView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as? TopWarningView {
                self.warningView = view
                self.warningView.delegate = self
                let yPos = 20.0 + Singleton.sharedInstance.getTopInsetofSafeArea()
                self.warningView.frame = CGRect(x: 0, y: yPos, width: SCREEN_SIZE.width, height: HEIGHT.warningViewHeight)
                self.navigationYPos = yPos + HEIGHT.warningViewHeight
                self.navigationHeight = self.navigationHeight - Singleton.sharedInstance.getTopInsetofSafeArea() - 20
                self.warningView.backgroundColor = COLOR.warningColor
                self.view.addSubview(self.warningView)
            }
        }
    }
    
    func setTutorials() {
        if SHOW_TUTORIAL == 1 {
            let tutorialResult = Singleton.sharedInstance.getRuntimeTutorialCase()
            if tutorialResult.0 == true {
                //if UserDefaults.standard.bool(forKey: "initialRun") == false && SHOW_TUTORIAL == 1 {
                // UserDefaults.standard.set(false, forKey: "firstRun")
                // UserDefaults.standard.set(true, forKey: "initialRun")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.tutorialController) as! TutorialController
                vc.navigationYPos = self.navigationYPos
                vc.navigationHeight = self.navigationHeight
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(vc, animated: false, completion: { finished in
                    vc.setRequiredTutorial(tutorialLocaion: tutorialResult.1!)
                })
            }
        }
    }
    
    
    
    // Set the status bar style to complement night-mode.
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.TASK_LIST_REFRESH, parameters: [:])
        getCurrentDayTasksList(NetworkingHelper.sharedInstance.currentDate)
    }
    
    func setCurrentLocationButton() {
        self.currentLocationButton.layer.cornerRadius = 17
        self.currentLocationBottomConstraint.constant = getAspectRatioValue(value: 188.0)
    }
    
    func setNotificationBadgeCount() {
        guard menuView != nil else {
            return
        }
        
        if let cell = menuView.menuTable.cellForRow(at: IndexPath(row: 1, section: 0)) as? MenuListCell {
            cell.countLabel.text = "\(NetworkingHelper.sharedInstance.notificationCount)"
        }
    }
    
    func updateCurrentDayTaskList() {
        self.getCurrentDayTasksList(NetworkingHelper.sharedInstance.currentDate)
    }
    
    func updateOnOffDuty() {
        if self.menuView != nil {
            self.menuView.setSwitchButton()
        }
    }
    
    /*================ SHOW/HIDE PATH SUMMARY =================*/
    func showTravelSummary() {
        self.runningJobsView.taskListTable.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.showPathSummaryView()
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.runningJobsView.transform = CGAffineTransform(translationX: 0, y: self.menuViewOrigin)
            self.runningJobsView.slideUpIcon.transform = CGAffineTransform.identity
            self.directionButton.alpha = 0
            self.nextTaskButton.alpha = 0
            self.previousTaskButton.alpha = 0
            self.newButton.transform = CGAffineTransform(translationX: 0, y: self.newButton.frame.height)
            self.googleMapView.clear()
            self.runningJobsView.slideUpIcon.isHidden = false
        }, completion: { finished in
            //self.setTaskButton(index: 0)
        })
    }
    
    func showPathSummaryView() {
        self.getSummaryPathFromServer()
        pathSummaryView = UINib(nibName: NIB_NAME.pathSummaryView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as? PathSummaryView
        pathSummaryView?.frame = CGRect(x: 5, y: self.view.frame.height, width: SCREEN_SIZE.width - 10, height: 102.0)
        pathSummaryView?.layer.cornerRadius = 8.0
        self.view.addSubview(pathSummaryView!)
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.pathSummaryView?.transform = CGAffineTransform(translationX: 0, y: -(self.pathSummaryView?.frame.height)! - 50)
        }, completion: { finished in
            //self.pathSummaryView?.setCount()
        })
    }

    
    func hideTravelSummary() {
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 2.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.googleMapView.clear()
            self.pathSummaryView?.transform = CGAffineTransform.identity
        }, completion: { finished in
            self.pathSummaryView?.removeFromSuperview()
            self.pathSummaryView = nil
            if NetworkingHelper.sharedInstance.polylines != nil {
                self.drawPath()
            }
            self.updateNewTaskButton()
            self.setDataForNoValueReceived()
            self.setMarkersOnMap()
            self.updateMarkerIcon(0, animation:true)
        })
    }
    
    func getSummaryPathFromServer() {
        let currentDate = NetworkingHelper.sharedInstance.currentDate
        let timeZone = -(NSTimeZone.system.secondsFromGMT() / 60)
        var params:[String : Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
        params["fleet_id"] = Singleton.sharedInstance.fleetDetails.fleetId!
        params["date"] = currentDate ?? ""
        params["timezone"] = timeZone
        print(params)
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getFleetHistory, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(message:(response["message"] as? String)!)
                        break
                    case STATUS_CODES.SHOW_DATA:
                        var completedTask = 0
                        var hours:Double = 0
                        var miles = 0
                        if let data = response["data"] as? [String:Any] {
                            if let completedTaskString = data["completed_tasks"] as? String {
                                completedTask = Int(completedTaskString)! //Int(String:completedTaskString)
                            } else if let completedTaskNumber = data["completed_tasks"] as? NSNumber {
                                completedTask = Int(completedTaskNumber)
                            }
                            
                            if let timeInSec = data["time_in_sec"] as? String {
                                hours = (Double(timeInSec)!) / 3600  //Int(String:completedTaskString)
                            } else if let timeInSec = data["time_in_sec"] as? NSNumber {
                                hours = Double(timeInSec) / 3600
                            }
                            
                            var divisor = 0
                            var unit = "Mile"
                            if let distanceType = data["distance_in"] as? String {
                                if distanceType == "KM" {
                                    divisor = 1000
                                    unit = distanceType
                                } else {
                                    divisor = 1609
                                    unit = distanceType
                                }
                            } else {
                                divisor = 1609
                            }
                            
                            
                            if let distanceInMeters = data["distance_in_metres"] as? String {
                                miles = (Int(distanceInMeters)!) / divisor  //Int(String:completedTaskString)
                            } else if let distanceInMeters = data["distance_in_metres"] as? NSNumber {
                                miles = Int(distanceInMeters) / divisor
                            }
                            
                            if let pathHistory = data["path_history"] as? [Any] {
                                self.summaryPath(pathHistory: pathHistory)
                            }
                            self.pathSummaryView?.setCountForMiles(limit: miles, unit: unit)
                            self.pathSummaryView?.setCountForHours(limit: hours)
                            self.pathSummaryView?.setCountForTask(limit: completedTask)
                        }
                        break
                    default:
                        self.pathSummaryView?.setCountForMiles(limit: 0, unit: "Mile")
                        break
                    }
                } else {
                    self.pathSummaryView?.setCountForMiles(limit: 0, unit: "Mile")
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func summaryPath(pathHistory:[Any]) {
        let path = GMSMutablePath()
        for i in (0..<pathHistory.count) {
            if let locationDictionary = pathHistory[i] as? [String:Any] {
                var latitude = 0.0
                var longitude = 0.0
                if let latitudeString = locationDictionary["lat"] as? String {
                    latitude = Double(latitudeString)!
                } else if let latitudeNumber = locationDictionary["lat"] as? NSNumber {
                    latitude = Double(latitudeNumber)
                }
                
                if let longitudeString = locationDictionary["lng"] as? String {
                    longitude = Double(longitudeString)!
                } else if let longitudeNumber = locationDictionary["lng"] as? NSNumber {
                    longitude = Double(longitudeNumber)
                }
                path.add(CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
            }
        }
        
        self.createRoutePathArray(path: path)
    }
    
    func createRoutePathArray(path:GMSMutablePath) {
        DispatchQueue.main.async {
            guard UIApplication.shared.applicationState == UIApplicationState.active else {
                return
            }
            self.googleMapView.clear()
            let polyline = GMSPolyline(path: path)
            polyline.strokeColor = COLOR.mapStrokeColor
            polyline.strokeWidth = 5.0
            polyline.geodesic = true;
            polyline.map = self.googleMapView
            
            CATransaction.begin()
            CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
            let rectangle = GMSPolyline(path: path)
            rectangle.map = self.googleMapView
            let bounds = GMSCoordinateBounds(path: path)
            self.googleMapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 15.0))
            CATransaction.commit()
        }
        
    }
    /*=========================================================*/
    
    
    
    /*================ GOOGLE MAP / MARKER =================*/
    func setGoogleMap() {
        /*============ Google Map ===============*/
        hasRouting = Singleton.sharedInstance.fleetDetails.hasRouting
        do {
            // Set the map style by passing the URL of the local file.
            if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
                if (mapStyle == MAP_STYLE.dark){
                    if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                        self.googleMapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                    } else {
                        NSLog("Unable to find style.json")
                    }
                    self.nextTaskButton.setImage(#imageLiteral(resourceName: "nextTask"), for: .normal)
                    self.previousTaskButton.setImage(#imageLiteral(resourceName: "previousTask"), for: .normal)
                    self.currentLocationButton.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
                } else{
                    self.googleMapView.mapStyle = nil
                    self.nextTaskButton.setImage(#imageLiteral(resourceName: "NextTask-1"), for: .normal)
                    self.previousTaskButton.setImage(#imageLiteral(resourceName: "PreviousTask-1"), for: .normal)
                    self.currentLocationButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
                }
                self.setAttributedNavigationTitle()
            }
        } catch {
            NSLog("The style definition could not be loaded: \(error)")
        }
        self.googleMapView.isMyLocationEnabled = true
        self.googleMapView.animate(toZoom: 16)
    }
    
    @IBAction func setToCurrentLocation(_ sender: Any) {
        DispatchQueue.main.async {
            guard UIApplication.shared.applicationState == UIApplicationState.active else {
                return
            }
            CATransaction.begin()
            CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                self.googleMapView.animate(toZoom: 16)
                self.googleMapView.animate(toLocation: location.coordinate)
            }
            self.googleMapView.animate(toViewingAngle: 45)
            CATransaction.commit()
        }
    }
    
    func setMarkersOnMap(){
        for j in (0..<filteredTasksListArray.count)  {
            let innerTaskArray = filteredTasksListArray[j]
            for i in (0..<innerTaskArray.count) {
                let markerModel = LocationMarkerModel()
                let coordinate = jobModel.getLatitudeLongitudeOf(job: innerTaskArray[i])
                markerModel.location = coordinate
                markerModel.row = i
                markerModel.section = j
                markerModel.locationMarker = GMSMarker(position: coordinate)
                markerModel.locationMarker.userData = index
                markerModel.jobStatus = innerTaskArray[i].jobStatus
                self.jobModel.setLocationMarkerStatus(markerModel.locationMarker, jobStatus: innerTaskArray[i].jobStatus)
                DispatchQueue.main.async {
                    guard UIApplication.shared.applicationState == UIApplicationState.active else {
                        return
                    }
                    markerModel.locationMarker.map = self.googleMapView
                }
                markerModel.locationMarker.title = jobModel.getAddressOf(job: innerTaskArray[i])
                self.locationMarkerModel.append(markerModel)
            }
        }
    }
    
    func updateMarkerIcon(_ newPosition: Int, animation:Bool){
        if filteredTasksListArray.count != 0 && self.locationMarkerModel.count > newPosition{
            let markerModel = self.locationMarkerModel[newPosition]
            markerModel.locationMarker.icon = self.jobModel.getSelectedMarker(jobStatus: markerModel.jobStatus)// UIImage(named: "marker_selected_assigned.png")
            markerModel.locationMarker.zIndex = 99
            if newPosition != previousIndex {
                if(self.locationMarkerModel.count > previousIndex){
                    let previousMarker = self.locationMarkerModel[previousIndex]
                    previousMarker.locationMarker.zIndex = 0
                    self.jobModel.setLocationMarkerStatus(self.locationMarkerModel[previousIndex].locationMarker, jobStatus: self.locationMarkerModel[previousIndex].jobStatus)
                }
                self.previousIndex = newPosition
            }
            
            DispatchQueue.main.async {
                guard UIApplication.shared.applicationState == UIApplicationState.active else {
                    return
                }
                if animation == true {
                    CATransaction.begin()
                    CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
                    if(self.locationMarkerModel.count > newPosition) {
                        self.googleMapView.animate(toLocation: self.locationMarkerModel[newPosition].location)
                    }
                    self.googleMapView.animate(toViewingAngle: 90)
                    CATransaction.commit()
                } else {
                    if(self.locationMarkerModel.count > newPosition) {
                        self.googleMapView.animate(toLocation: self.locationMarkerModel[newPosition].location)
                    }
                    self.googleMapView.animate(toViewingAngle: 90)
                }
            }
        }
    }
    
    func drawPath() {
        if(hasRouting == 1) {
            DispatchQueue.main.async {
                guard UIApplication.shared.applicationState == UIApplicationState.active else {
                    return
                }
                self.googleMapView.clear()
                for i in (0..<NetworkingHelper.sharedInstance.polylines.count) {
                    let taskDataModel = TaskDataModel()
                    let path = taskDataModel.decodePolyline(NetworkingHelper.sharedInstance.polylines[i] as! String)
                    let polyLine = GMSPolyline(path: path)
                    polyLine.strokeWidth = 5
                    polyLine.strokeColor = COLOR.mapStrokeColor
                    polyLine.map = self.googleMapView
                }
            }
        }
    }
    
    /*    func updateMarkerForTaskDetail(fromClosure:Bool) {
     if fromClosure == false {
     var count = 0
     outerLoop: for tasks in self.filteredTasksListArray {
     for task in tasks {
     count = count + 1
     if task.jobId == Singleton.sharedInstance.selectedTaskDetails.jobId {
     self.updateMarkerIcon(count - 1, animation:false)
     break outerLoop
     }
     }
     }
     } else {
     //           let animationTime = Int(floor(16.0 - self.googleMapView.camera.zoom) / 3)
     //            CATransaction.begin()
     //            CATransaction.setValue(NSNumber(value: animationTime), forKey: kCATransactionAnimationDuration)
     let destinationCoordinate = jobModel.getLatitudeLongitudeOf(job: Singleton.sharedInstance.selectedTaskDetails)
     self.googleMapView.animate(with: GMSCameraUpdate.setTarget(destinationCoordinate, zoom: 16.0))
     self.googleMapView.padding = UIEdgeInsetsMake(30, 0, SCREEN_SIZE.height/1.8, 0)
     //            CATransaction.commit()
     }
     
     //      self.googleMapView.animate(toZoom: 14.0)
     //        if fromClosure == true {
     //            let downwards = GMSCameraUpdate.scrollBy(x: 0, y: SCREEN_SIZE.height/2)
     //            self.googleMapView.animate(with: downwards)
     //        }
     
     //
     
     //
     //        if fromClosure == true {
     //            let downwards = GMSCameraUpdate.scrollBy(x: 0, y: SCREEN_SIZE.height/2)
     //            self.googleMapView.animate(with: downwards)
     //        }
     }*/
    
    func drawPathFromCurrentToDestination(controller:DetailController) {
        let originCoordinate = LocationTracker.sharedInstance().getLatestLocationForForegroundMode()
        let destinationCoordinate = jobModel.getLatitudeLongitudeOf(job: Singleton.sharedInstance.selectedTaskDetails)
        if myCurrentLocation == nil {
            self.myCurrentLocation = LocationTracker.sharedInstance().getLatestLocationForForegroundMode()
        } else {
            guard Double((originCoordinate?.distance(from: self.myCurrentLocation))!) > 200.0 else {
                //                googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
                //                CATransaction.begin()
                //                CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
                //                let bounds = GMSCoordinateBounds(coordinate: (originCoordinate?.coordinate)!, coordinate: destinationCoordinate)
                //                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(40, 20, controller.jobOrigin + 20, 20))
                //                googleMapView.moveCamera(update)
                //                //googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
                //                self.googleMapView.animate(toViewingAngle: 45)
                //                CATransaction.commit()
                return
            }
        }
        self.myCurrentLocation = originCoordinate
        NetworkingHelper.sharedInstance.fetchPathPoints((originCoordinate?.coordinate)!, to: destinationCoordinate) { (optionalRoute) in
            if let jsonRoutes = optionalRoute,
                let routes = jsonRoutes["routes"] as? NSArray{
                if routes.count > 0 {
                    if let shortestRoute = routes[0] as? [String: AnyObject],
                        let legs = shortestRoute["legs"] as? Array<[String: AnyObject]>,
                        let distanceDict = legs[0]["distance"] as? [String: AnyObject],
                        let distance = distanceDict["value"] as? NSNumber,
                        let polyline = shortestRoute["overview_polyline"] as? [String: String],
                        let points = polyline["points"]
                        , distance.doubleValue >= 0 {
                        self.drawPath(points, originCoordinate:(originCoordinate?.coordinate)!, destinationCoordinate:destinationCoordinate, minOrigin:controller.jobOrigin + 20)
                    }
                }else{
                    self.setMarker((originCoordinate?.coordinate)!, destinationCoordinate: destinationCoordinate, minOrigin:controller.jobOrigin + 20)
                }
            } else {
                DispatchQueue.main.async {
                    guard UIApplication.shared.applicationState == UIApplicationState.active else {
                        return
                    }
                    
                    self.googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
                    CATransaction.begin()
                    CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
                    let bounds = GMSCoordinateBounds(coordinate: (originCoordinate?.coordinate)!, coordinate: destinationCoordinate)
                    let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(40, 20, controller.jobOrigin + 20, 20))
                    self.googleMapView.moveCamera(update)
                    //googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
                    self.googleMapView.animate(toViewingAngle: 45)
                    CATransaction.commit()
                }
            }
        }
    }
    
    func drawPath(_ encodedPathString: String, originCoordinate:CLLocationCoordinate2D, destinationCoordinate:CLLocationCoordinate2D, minOrigin:CGFloat) -> Void {
        DispatchQueue.main.async {
            guard UIApplication.shared.applicationState == UIApplicationState.active else {
                return
            }
            self.googleMapView.clear()
            CATransaction.begin()
            CATransaction.setValue(NSNumber(value: 1), forKey: kCATransactionAnimationDuration)
            let path = GMSPath(fromEncodedPath: encodedPathString)
            let line = GMSPolyline(path: path)
            line.strokeWidth = 4.0
            line.strokeColor = COLOR.mapStrokeColor
            line.isTappable = true
            line.map = self.googleMapView
            self.setMarker(originCoordinate, destinationCoordinate: destinationCoordinate, minOrigin: minOrigin)
            // change the camera, set the zoom, whatever.  Just make sure to call the animate* method.
            self.googleMapView.animate(toViewingAngle: 90)
            CATransaction.commit()
        }
    }
    func setMarker(_ originCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D, minOrigin:CGFloat) {
        DispatchQueue.main.async {
            guard UIApplication.shared.applicationState == UIApplicationState.active else {
                return
            }
            self.googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            let destinationLocationMarker = GMSMarker(position: destinationCoordinate)
            destinationLocationMarker.icon = self.jobModel.getSelectedMarker(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus)
            destinationLocationMarker.map = self.googleMapView
            
            let northEastCoordinate = CLLocationCoordinate2D(latitude: max(originCoordinate.latitude, destinationCoordinate.latitude), longitude: max(originCoordinate.longitude, destinationCoordinate.longitude))
            let southWestCoordinate = CLLocationCoordinate2D(latitude: min(originCoordinate.latitude, destinationCoordinate.latitude), longitude: min(originCoordinate.longitude, destinationCoordinate.longitude))
            
            _ = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: (northEastCoordinate.latitude + southWestCoordinate.latitude)/2, longitude: (northEastCoordinate.longitude + southWestCoordinate.longitude)/2), zoom: 12, bearing: 0, viewingAngle: 0)
            
            //        googleMapView.animateToCameraPosition(cameraPosition)
            
            let bounds = GMSCoordinateBounds(coordinate: originCoordinate, coordinate: destinationCoordinate)
            let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(30, 20, minOrigin, 20))
            self.googleMapView.moveCamera(update)
        }
    }
    
    /*========================================================================*/
    /*======================== GOOGLE MAP NEXT / PREVIOUS BUTTONS ============================*/
    //MARK: GOOGLE MAP NEXT / PREVIOUS BUTTONS
    func setTaskButton(index:Int) {
        var nextButtonHideStatus:Bool!
        var previsusButtonHideStatus:Bool!
        if self.locationMarkerModel.count <= 1 {
            nextButtonHideStatus = true
            previsusButtonHideStatus = true
        } else {
            if index == 0 {
                previsusButtonHideStatus = true
                nextButtonHideStatus = false
            } else if index == self.locationMarkerModel.count - 1 {
                nextButtonHideStatus = true
                previsusButtonHideStatus = false
            } else {
                nextButtonHideStatus = false
                previsusButtonHideStatus = false
            }
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.nextTaskButton.alpha = nextButtonHideStatus == true ? 0 : 1
            self.previousTaskButton.alpha = previsusButtonHideStatus == true ? 0 : 1
        }, completion: { finished in
        })
        
        self.mapIndex = index
        self.updateMarkerIcon(index, animation: true)
        guard locationMarkerModel.count > 0 && locationMarkerModel.count > self.mapIndex else {
            return
        }
        
        if self.runningJobsView != nil && self.runningJobsView.transform.ty > 0 {
            let tableRect = self.runningJobsView.taskListTable.rectForRow(at: IndexPath(row: self.locationMarkerModel[self.mapIndex].row, section: self.locationMarkerModel[self.mapIndex].section))
            self.runningJobsView.taskListTable.setContentOffset(CGPoint(x: 0, y: tableRect.origin.y), animated: true)
        }
    }
    
    @IBAction func nextTaskAction(_ sender: Any) {
        self.setTaskButton(index: mapIndex + 1)
    }
    
    @IBAction func previousTaskAction(_ sender: Any) {
        guard mapIndex > 0 else {
            return
        }
        self.setTaskButton(index: mapIndex - 1)
    }
    /*========================================================================*/
    
    //MARK: SET DIRECTION BUTTON
    func setDirectionButton() {
        directionButton = UIButton()
        directionButton.alpha = 0
        directionButton.frame = CGRect(x: SCREEN_SIZE.width - 75, y: minimizeOrigin + getAspectRatioValue(value: 50.0) + Singleton.sharedInstance.getTopInsetofSafeArea(), width: 50, height: 50)
        directionButton.backgroundColor = COLOR.themeForegroundColor
        directionButton.setImage(#imageLiteral(resourceName: "getDirectionsLarge"), for: .normal)
        directionButton.layer.cornerRadius = 25
        directionButton.layer.masksToBounds = true
        directionButton.addTarget(self, action: #selector(self.directionAction), for: .touchUpInside)
        self.view.addSubview(directionButton)
    }
    
    func enableDirectionButton() {
        self.directionButton.isEnabled = true
    }
    
    func directionAction() {
        self.directionButton.isEnabled = false
        self.perform(#selector(self.enableDirectionButton), with: nil, afterDelay: 0.5)
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_START, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_START, customAttributes: [:])
        guard self.locationMarkerModel.count > 0 else {
            return
        }
        
        let destinationCoordinate = self.locationMarkerModel[self.mapIndex].location
        var originCoordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        if(LocationTracker.sharedInstance().myLocation != nil) {
            originCoordinate = LocationTracker.sharedInstance().myLocation.coordinate
        }
        
        if let navigationMap = UserDefaults.standard.value(forKey: USER_DEFAULT.navigationMap) as? Int {
            if navigationMap == NAVIGATION_MAP.appleMap {
                self.openAppleMap()
            } else {
                let appName = Singleton.sharedInstance.getNavigationAppName()
                let directionMode = Singleton.sharedInstance.getDirectionMode()
                let address = Singleton.sharedInstance.getNavigationAddress(destinationCoordinate: destinationCoordinate, directionMode: directionMode)
                if(UIApplication.shared.canOpenURL(URL(string: address)!)) {
                    UIApplication.shared.openURL(URL(string: address)!)
                } else{
                    self.navigationErrorMessage(appName: appName, directionMode:directionMode, origin:originCoordinate, destination:destinationCoordinate)
                }
            }
        }
    }
    
    func openAppleMap() {
        let destinationCoordinate = self.locationMarkerModel[self.mapIndex].location
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: destinationCoordinate, addressDictionary:nil))
        mapItem.name = "Destination Address"
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    
    func navigationErrorMessage(appName: String, directionMode:String, origin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D) {
        let alertController = UIAlertController(title: "", message: appName + ERROR_MESSAGE.NAVIGATION_ERROR_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
        let continueAction = UIAlertAction(title: TEXT.CONTINUE, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            let addr = "http://maps.google.com/maps?saddr=\(origin.latitude),\(origin.longitude)&daddr=\(destination.latitude),\(destination.longitude)&mode=\(directionMode)"
            UIApplication.shared.openURL(URL(string: addr)!)
        })
        
        let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(continueAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*===================== NEW TASK ORDER BUTTON =====================*/
    //MARK: SET NEW TASK BUTTON
    func setNewTaskButton() {
        self.newButton.transform = CGAffineTransform(translationX: 0, y: self.newButton.frame.height)
        self.newButton.backgroundColor = COLOR.themeForegroundColor
        self.newButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.newButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
    }
    
    func updateNewTaskButton() {
        self.view.bringSubview(toFront: self.newButton)
        let safeAreaForBottomConstraint = Singleton.sharedInstance.getBottomInsetofSafeArea() + 30
        if NetworkingHelper.sharedInstance.newTaskListArray.count == 0 {
            self.newButton.isHidden = true
            self.runningJobsView.bottomConstraint.constant = self.runningJobsView.defaultBottomConstraint - self.defaultHeightOfButtons + safeAreaForBottomConstraint
        } else {
            self.runningJobsView.bottomConstraint.constant = self.runningJobsView.defaultBottomConstraint + safeAreaForBottomConstraint
            self.newButton.isHidden = false
            var title = "\(NetworkingHelper.sharedInstance.newTaskListArray.count) \(TEXT.NEW)"// + TEXT.NEW
            if NetworkingHelper.sharedInstance.newTaskListArray.count == 1 {
                title = "\(title) \(TEXT.ORDER)"// + TEXT.ORDER
            } else {
                title = "\(title) \(TEXT.ORDER)"//title + " " + TEXT.ORDERS
            }
            self.newButton.setTitle(title, for: .normal)
            guard self.menuView == nil else {
                return
            }
            UIView.animate(withDuration: 0.2, animations: {
                self.newButton.transform = CGAffineTransform.identity
            })
            
            if isItFirstTimeForNewTaskButton == true  && Singleton.sharedInstance.isTaxiTask == false{
                self.gotoNewTaskContoller()
                self.isItFirstTimeForNewTaskButton = false
            }
        }
    }
    
    @IBAction func newTaskAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.newButton.transform = CGAffineTransform(translationX: 0, y: self.newButton.frame.height)
        }, completion: { finished in
            self.gotoNewTaskContoller()
        })
    }
    /*========================================================================*/
    //MARK: SET MENU LIST
    func setMenuView() {
        UIApplication.shared.statusBarStyle = .lightContent
        if self.menuView == nil {
            self.menuView = UINib(nibName: NIB_NAME.menuList, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! MenuList
            self.menuView.delegate = self
            self.menuView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height - maxHideLimit)
            self.menuView.alpha = 0.0
            self.view.addSubview(menuView)
        }
    }
    
    //MARK: SET RUNNING JOB LIST
    func setRunningJobListView() {
        if runningJobsView == nil {
            self.runningJobsView = UINib(nibName: NIB_NAME.jobListTable, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! JobListTable
            
            self.runningJobsView.delegate = self
            self.runningJobsView.frame = CGRect(x: leftRightMargin, y: self.navigationHeight + 10 + self.navigationYPos, width: SCREEN_SIZE.width - (leftRightMargin * 2), height: SCREEN_SIZE.height - self.navigationYPos)
            self.runningJobsView.layer.cornerRadius = 14.0
            self.runningJobsView.layer.masksToBounds = true
            self.runningJobsView.alpha = 0
            self.runningJobsView.transform = CGAffineTransform(translationX: 0, y: SCREEN_SIZE.height)
            self.runningJobsView.taskListTable.addSubview(self.refreshControl)
    
            self.view.addSubview(runningJobsView)
            
            self.runningJobsView.layer.masksToBounds = false
            self.runningJobsView.layer.shadowOffset = CGSize(width: 0, height: -5)
            self.runningJobsView.layer.shadowOpacity = 0.5
            self.runningJobsView.layer.shadowColor = COLOR.LIGHT_COLOR.cgColor
            self.runningJobsView.layer.shadowRadius = 3
        }
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.valueChanged(_:)))
        panGesture.delegate = self
        self.runningJobsView.addGestureRecognizer(panGesture)
    }
    
    //MARK: PanGesture Delegates
    func valueChanged(_ pan:UIPanGestureRecognizer) {
        guard self.menuView == nil else {
            return
        }
        
        guard self.pathSummaryView == nil else {
            return
        }
        
        guard self.filteredTasksListArray.count > 0 else {
            return
        }
        
        let translation  = pan.translation(in: pan.view?.superview)
        var theTransform = pan.view?.transform
        if forceTouch == false {
            if self.runningJobsView.taskListTable.contentOffset.y == 0 {
                if pan.state == UIGestureRecognizerState.began {
                    panDirection = self.getJobViewDirection(jobView: pan.view!)
                } else if(pan.state == UIGestureRecognizerState.changed) {
                    if panDirection == nil {
                        panDirection = self.getJobViewDirection(jobView: pan.view!)
                    }
                    switch panDirection! {
                    case .topToBottom:
                        if translation.y > 0 {
                            theTransform?.ty = translation.y
                            pan.view?.transform = theTransform!
                        }
                    case .bottomToTop:
                        if translation.y < 0 && Double((theTransform?.ty)!) >= 0.0 {
                            theTransform?.ty = minimizeOrigin + translation.y
                            pan.view?.transform = theTransform!
                            self.directionButton.alpha = 0
                        }
                    }
                } else if(pan.state == UIGestureRecognizerState.ended) {
                    if panDirection == nil {
                        panDirection = self.getJobViewDirection(jobView: pan.view!)
                    }
                    switch panDirection! {
                    case .topToBottom:
                        if translation.y > maxTranslatingPointToMinimize {
                            self.translateViewToPoint(point: minimizeOrigin, translatingView: pan.view!)
                        } else {
                            self.translateViewToPoint(point: 0, translatingView: pan.view!)
                        }
                    case .bottomToTop:
                        if translation.y > 0 && Double((theTransform?.ty)!) > 0.0 {
                            self.translateViewToPoint(point: minimizeOrigin, translatingView: pan.view!)
                        } else {
                            if translation.y < 0 {
                                self.translateViewToPoint(point: 0, translatingView: pan.view!)
                            }
                        }
                    }
                    
                }
            }
        }
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //MARK: Animate Card
    func translateViewToPoint(point:CGFloat, translatingView:UIView) {
        if self.menuView != nil {
            self.hideMenuView()
        }
        
        if self.pathSummaryView != nil {
            self.hideTravelSummary()
        }
        
        self.runningJobsView.taskListTable.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.curveLinear, animations: {
            if point == 0 {
                translatingView.transform = CGAffineTransform.identity
                self.runningJobsView.slideUpIcon.transform = CGAffineTransform(rotationAngle: .pi)
                self.previousTaskButton.alpha = 0
                self.nextTaskButton.alpha = 0
            } else {
                let safeArea = Singleton.sharedInstance.getBottomInsetofSafeArea() > 0 ? 0 : Singleton.sharedInstance.getBottomInsetofSafeArea()
                translatingView.transform = CGAffineTransform(translationX: 0, y: point + safeArea)
                self.runningJobsView.slideUpIcon.transform = CGAffineTransform.identity
                
            }
        }, completion: { finished in
            self.changeTableViewState(status: point == 0 ? true : false, jobView: translatingView)
            if point > 0 {
                self.setTaskButton(index: 0)
            }
            //self.setTaskButton(index: 0)
        })
    }
    
    //MARK: Get Pan Direction
    func getJobViewDirection(jobView:UIView) -> SwipeDirection {
        if jobView.transform.ty == 0 {
            return .topToBottom
        } else {
            return .bottomToTop
        }
    }
    
    func isNewTaskOnScreen() {
        let navigationController = (UIApplication.shared.delegate)?.window??.rootViewController as! UINavigationController
        let visibleController = navigationController.visibleViewController
        if(NSStringFromClass((visibleController?.classForCoder)!).components(separatedBy: ".").last! == STORYBOARD_ID.newTasksController) {
            let controller = visibleController as! NewTasksController
            if controller.newListTable != nil {
                ActivityIndicator.sharedInstance.showActivityIndicator()
                controller.newTaskListArray.removeAll()
                for task in NetworkingHelper.sharedInstance.newTaskListArray {
                    controller.newTaskListArray.append(task)
                }
                controller.newListTable.reloadData()
                ActivityIndicator.sharedInstance.hideActivityIndicator()
            }
        }
    }
    
    //MARK: Set Table Properties
    func changeTableViewState(status:Bool, jobView:UIView) {
        if jobView == self.runningJobsView {
            self.runningJobsView.taskListTable.isScrollEnabled = status
            self.mapIndex = 0
            if status == false {
                if directionButton != nil && self.filteredTasksListArray.count > 0 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.directionButton.alpha = 1.0
                    })
                }
            } else {
                if directionButton != nil {
                    self.directionButton.alpha = 0.0
                }
            }
        }
    }
    
    //MARK: SERVER HIT
    func getCurrentDayTasksList(_ selectedDate: String) {
        let offlineModel = OfflineDataModel()
        if IJReachability.isConnectedToNetwork() == true {
            if(self.filteredTasksListArray.count == 0 && self.refreshControl.isRefreshing == false) {
                self.runningJobsView.taskListTable.reloadData()
                self.runningJobsView.taskListTable.backgroundView = nil
                self.runningJobsView.backgroundView.isHidden = true
                let loaderView = UINib(nibName: NIB_NAME.loadingTaskView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! LoadingTaskView
                loaderView.frame = self.runningJobsView.taskListTable.frame
                self.runningJobsView.taskListTable.backgroundView = loaderView
            } else {
                self.runningJobsView.stopGradientAnimation = false
                self.runningJobsView.setGradientLayer()
            }
            
            if(offlineModel.uploadUnsyncedTaskToServer() == false) {
                if(offlineModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                    self.showInvalidAccessTokenPopup(message: offlineModel.hitFailMessage)
                } else {
                    self.alertForSyncingFailed(selectedDate)
                }
            } else {
                self.sendRequestForOnlineData(selectedDate)
            }
        } else {
            self.saveOfflineData(selectedDate)
        }
    }
    
    func alertForSyncingFailed(_ selectedDate: String) {
        let alert = UIAlertController(title: TEXT.ERROR, message: ERROR_MESSAGE.SYNC_FAILED_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
        let tryAgain = UIAlertAction(title: TEXT.TRY_AGAIN, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.getCurrentDayTasksList(selectedDate)
            })
        })
        alert.addAction(tryAgain)
        
        let offlineData = UIAlertAction(title: TEXT.CONTINUE_OFFLINE, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.saveOfflineData(selectedDate)
            })
        })
        alert.addAction(offlineData)
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendRequestForOnlineData(_ selectedDate:String) {
        var params:[String:Any] = ["access_token": Singleton.sharedInstance.getAccessToken()]
        params["date"] = selectedDate
        params["version"] = VERSION
        //        let params = [
        //            "access_token": Singleton.sharedInstance.getAccessToken(),
        //            "date":selectedDate,
        //            "version":VERSION]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.viewTaskForDate, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                print(response)
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.runningJobsView.stopGradientAnimation = true
                        self.setDataForNoValueReceived()
                        self.showInvalidAccessTokenPopup(message: response["message"] as! String)
                        break
                    default:
                        // DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        NetworkingHelper.sharedInstance.dayTasksListArray.removeAll(keepingCapacity: false)
                        NetworkingHelper.sharedInstance.newTaskListArray.removeAll(keepingCapacity: false)
                        
                        let currentDate = Date().formattedWith("yyyy-MM-dd")
                        if(currentDate == selectedDate) {
                            DatabaseManager.sharedInstance.deleteAssignedTaskFromDatabase(self.taskObjectContext)
                            DatabaseManager.sharedInstance.deletePolylineFromDatabase(self.taskObjectContext)
                            Auxillary.deleteAllFilesFromDocumentDirectory()
                        }
                        
                        if let data = response["data"] as? [String:Any] {
                            //                            if let count = data["total_new_task_count"] as? NSNumber {
                            //                                Singleton.sharedInstance.totalNewTaskCount = Int(count)
                            //                            } else if let count = data["total_new_task_count"] as? String {
                            //                                Singleton.sharedInstance.totalNewTaskCount = Int(count)!
                            //                            }
                            
                            if let items = data["tasks"] as? NSArray {
                                for item in items {
                                    var innerTaskListArray = [TasksAssignedToDate]()
                                    let taskList = item as! [[String:Any]]
                                    for task in taskList {
                                        let taskDetails = TasksAssignedToDate(json: task)
                                        innerTaskListArray.append(taskDetails)
                                        if(currentDate == selectedDate) {
                                            _ = DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(taskDetails, jobId: taskDetails.jobId, timeStamp: selectedDate, syncStatus: 1, taskManagedContext: self.taskObjectContext)
                                        }
                                    }
                                    NetworkingHelper.sharedInstance.dayTasksListArray.append(innerTaskListArray)
                                }
                            }
                            
                            if let items = data["routed_tasks"] as? NSArray {
                                for item in items {
                                    var innerTaskListArray = [TasksAssignedToDate]()
                                    let taskList = item as! [[String:Any]]
                                    for task in taskList {
                                        let taskDetails = TasksAssignedToDate(json: task)
                                        innerTaskListArray.append(taskDetails)
                                        if(currentDate == selectedDate) {
                                            _ = DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(taskDetails, jobId: taskDetails.jobId, timeStamp: selectedDate, syncStatus: 1, taskManagedContext: self.taskObjectContext)
                                        }
                                    }
                                    NetworkingHelper.sharedInstance.dayTasksListArray.insert(innerTaskListArray, at: 0)
                                }
                            }
                            
                            if let items = data["new_tasks"] as? NSArray {
                                for item in items {
                                    var innerTaskListArray = [TasksAssignedToDate]()
                                    let taskList = item as! [[String:Any]]
                                    for task in taskList {
                                        let taskDetails = TasksAssignedToDate(json: task)
                                        innerTaskListArray.append(taskDetails)
                                        if(currentDate == selectedDate) {
                                            _ = DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(taskDetails, jobId: taskDetails.jobId, timeStamp: selectedDate, syncStatus: 1, taskManagedContext: self.taskObjectContext)
                                        }
                                    }
                                    NetworkingHelper.sharedInstance.newTaskListArray.append(innerTaskListArray)
                                }
                                self.isNewTaskOnScreen()
                            }
                            
                            if let polylines = data["polylines"] as? NSArray {
                                NetworkingHelper.sharedInstance.polylines = polylines
                                if(currentDate == selectedDate) {
                                    DatabaseManager.sharedInstance.savePolylineToDatabase(NSMutableArray(array: polylines).jsonString, timeStamp: selectedDate, syncStatus: 1, userManagedContext: self.taskObjectContext)
                                }
                                
                            }
                        }
                        DispatchQueue.main.async {
                            self.setTutorials()
                            self.getFilterData()
                            self.runningJobsView.stopGradientAnimation = true
                        }
                        //}
                    }
                } else {
                    self.runningJobsView.stopGradientAnimation = true
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SLOW_INTERNET_CONNECTION:
                        self.saveOfflineData(selectedDate)
                        break
                    case STATUS_CODES.SHOW_MESSAGE:
                        self.getNoDataResponse()
                        self.setTutorials()
                        break
                    default:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        self.getNoDataResponse()
                        break
                    }
                }
            }
        }
    }
    
    func saveOfflineData(_ selectedDate:String) {
        self.showInternetToast()
        if(Auxillary.isAppSyncingEnable() == true) {
            DispatchQueue.main.async(execute: { () -> Void in
                let tasksFromDatabase = DatabaseManager.sharedInstance.fetchAssignedTaskFromDatabase(self.taskObjectContext, selectedDate: selectedDate)
                NetworkingHelper.sharedInstance.dayTasksListArray.removeAll(keepingCapacity: false)
                if(tasksFromDatabase != nil && (tasksFromDatabase?.count)! > 0) {
                    var relationshipIdArray = [String]()
                    for i in (0..<tasksFromDatabase!.count) {
                        var innerArray = [TasksAssignedToDate]()
                        let taskData = NSData(data: tasksFromDatabase?[i].value(forKey: "task_object") as! Data) as Data
                        let taskDetails = NSKeyedUnarchiver.unarchiveObject(with: taskData) as! TasksAssignedToDate
                        
                        if let index = relationshipIdArray.index(of: taskDetails.pickupDeliveryRelationship) {
                            innerArray = NetworkingHelper.sharedInstance.dayTasksListArray[index]
                            innerArray.append(taskDetails)
                            NetworkingHelper.sharedInstance.dayTasksListArray[index] = innerArray
                        } else {
                            relationshipIdArray.append(taskDetails.pickupDeliveryRelationship)
                            innerArray.append(taskDetails)
                            NetworkingHelper.sharedInstance.dayTasksListArray.append(innerArray)
                        }
                    }
                    
                    let polylines = DatabaseManager.sharedInstance.fetchPolylineFromDatabase(self.taskObjectContext, selectedDate: selectedDate)
                    if(polylines != nil) {
                        NetworkingHelper.sharedInstance.polylines = polylines?.jsonObject
                    }
                    self.getFilterData()
                } else {
                    // Auxillary.showAlert(STATUS_CODES.NO_INTERNET_CONNECTION)
                    self.getNoDataResponse()
                }
            })
        } else {
            //self.getNoDataResponse()
        }
    }
    
    func setDataForNoValueReceived() {
        self.refreshControl.endRefreshing()
        self.runningJobsView.taskListTable.backgroundView = nil
        if self.filteredTasksListArray.count == 0 {
            self.showHideNoTaskForTodayLabel(true)
            Analytics.logEvent(ANALYTICS_KEY.NO_TASK, parameters: [:])
        } else {
            self.runningJobsView.slideUpIcon.isHidden = false
        }
    }
    
    func showHideNoTaskForTodayLabel(_ show: Bool){
        self.runningJobsView.setBackgroundView()
        self.directionButton.alpha = 0.0
        self.translateViewToPoint(point: 0, translatingView: self.runningJobsView)
    }
    
    func getNoDataResponse() {
        self.locationMarkerModel.removeAll(keepingCapacity: false)
        self.googleMapView.clear()
        NetworkingHelper.sharedInstance.dayTasksListArray.removeAll(keepingCapacity: false)
        NetworkingHelper.sharedInstance.newTaskListArray.removeAll(keepingCapacity: false)
        self.getFilterData()
    }
    
    
    func showInvalidAccessTokenPopup(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                NotificationCenter.default.removeObserver(self)
                Auxillary.logoutFromDevice()
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getFilterData() {
        self.setAttributedNavigationTitle()
        let taskDataModel = TaskDataModel()
        filteredTasksListArray.removeAll(keepingCapacity: false)
        self.locationMarkerModel.removeAll(keepingCapacity: false)
        self.updateFilterButton()
        self.filteredTasksListArray = taskDataModel.getFilteredData(forNewTaskList: false)
        //NetworkingHelper.sharedInstance.newTaskListArray = taskDataModel.getFilteredData(forNewTaskList: true)
        self.runningJobsView.filteredTasksListArray = self.filteredTasksListArray
        self.runningJobsView.taskListTable.reloadData()
        
        if let controller = super.navigationController?.visibleViewController {
            if(NSStringFromClass(controller.classForCoder).components(separatedBy: ".").last! == STORYBOARD_ID.detailController) {
                DispatchQueue.main.async {
                    ActivityIndicator.sharedInstance.showActivityIndicator()
                    let detailController = controller as! DetailController
                    detailController.view.endEditing(true)
                    self.updateTaskDetailController()
                }
            } else  if(NSStringFromClass(controller.classForCoder).components(separatedBy: ".").last! == STORYBOARD_ID.homeBaseController) {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                self.updateSelectedTaskDetails()
            } else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
            }
        }
        
        guard self.navigation.alpha == 1 else {
            return
        }
        guard self.pathSummaryView == nil else {
            return
        }
        
        self.updateNewTaskButton()
        self.setDataForNoValueReceived()
        
        self.googleMapView.clear()
        if NetworkingHelper.sharedInstance.polylines != nil {
            self.drawPath()
        }
        self.setMarkersOnMap()
        self.updateMarkerIcon(0, animation:true)
        
        if Singleton.sharedInstance.isTaxiTask == true || (Singleton.sharedInstance.selectedTaskDetails != nil && Singleton.sharedInstance.selectedTaskDetails.vertical == VERTICAL.TAXI.rawValue) {
            self.updateSelectedTaskDetails()
            if let controller = super.navigationController?.visibleViewController {
                if(NSStringFromClass(controller.classForCoder).components(separatedBy: ".").last! == STORYBOARD_ID.taxiController) {
                    let taxiController = controller as! TaxiController
                    taxiController.updateTaskAfterNotifcation()
                } else {
                    guard Singleton.sharedInstance.selectedTaskDetails != nil else {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        return
                    }
                    guard Singleton.sharedInstance.selectedTaskDetails.vertical == VERTICAL.TAXI.rawValue else {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        return
                    }
                    guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray != nil && (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > 0 else {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        return
                    }
                    guard Singleton.sharedInstance.selectedTaskDetails.jobStatus != JOB_STATUS.assigned else {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        return
                    }
                    
                    switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
                    case JOB_STATUS.accepted, JOB_STATUS.arrived, JOB_STATUS.started:
                        self.gotoTaxiController(index: 0)
                        break
                    default:
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        break
                    }
                    
                }
            }
        }
        // self.setTaskButton(index: 0)
    }
    
    func updateSelectedTaskDetails() {
        if(Singleton.sharedInstance.selectedTaskDetails != nil) {
            var section = -1
            var row = -1
            OUTER: for i in (0..<filteredTasksListArray.count) {
                for j in 0..<self.filteredTasksListArray[i].count {
                    let task = self.filteredTasksListArray[i][j]
                    if task.jobId == Singleton.sharedInstance.selectedTaskDetails.jobId {
                        section = i
                        row = j
                        break OUTER
                    }
                }
            }
            
            guard section != -1 else {
                return
            }
            
            guard row != -1 else {
                return
            }
            
            guard section < filteredTasksListArray.count else {
                return
            }
            
            guard row < filteredTasksListArray[section].count else {
                return
            }
            
            Singleton.sharedInstance.selectedTaskDetails = filteredTasksListArray[section][row]
        }
    }
    
    func updateTaskDetailController() {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            return
        }
        
        if(NetworkingHelper.sharedInstance.notificationJobId == Singleton.sharedInstance.selectedTaskDetails.jobId) {
            guard Singleton.sharedInstance.lastNotificationFlagType != FLAG_TYPE.deleteTask else {
                if let controller = self.navigationController?.visibleViewController {
                    switch NSStringFromClass(controller.classForCoder).components(separatedBy: ".").last! {
                    case STORYBOARD_ID.detailController:
                        let detailController = controller as! DetailController
                        detailController.receivedDeleteTaskNotification()
                        Singleton.sharedInstance.lastNotificationFlagType = 0
                        break
                    default:
                        break
                    }
                }
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                return
            }
            var section = -1
            var row = -1
            OUTER: for i in (0..<filteredTasksListArray.count) {
                for j in 0..<self.filteredTasksListArray[i].count {
                    let task = self.filteredTasksListArray[i][j]
                    if task.jobId == Singleton.sharedInstance.selectedTaskDetails.jobId {
                        section = i
                        row = j
                        break OUTER
                    }
                }
            }
            
            guard section != -1 else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                return
            }
            
            guard row != -1 else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                return
            }
            
            guard section < filteredTasksListArray.count else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                return
            }
            
            guard row < filteredTasksListArray[section].count else {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                return
            }
            _ = self.gotoTaskDetailPage(index: row, startStatus: 0, section: section)
            ActivityIndicator.sharedInstance.hideActivityIndicator()
        } else {
            self.updateSelectedTaskDetails()
            if let controller = self.navigationController?.visibleViewController {
                switch NSStringFromClass(controller.classForCoder).components(separatedBy: ".").last! {
                case STORYBOARD_ID.detailController:
                    let detailController = controller as! DetailController
                    detailController.jobDetailsView.detailsTable.reloadData()
                    break
                default:
                    break
                }
            }
            ActivityIndicator.sharedInstance.hideActivityIndicator()
        }
    }
    
    func gotoNewTaskContoller() {
        NetworkingHelper.sharedInstance.newTaskListArray.removeAll(keepingCapacity: false)
        let controller = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.newTasksController) as! NewTasksController
        controller.callback = { message in
            if(NetworkingHelper.sharedInstance.notificationArray.count > 0) {
                Singleton.sharedInstance.removePushBanner()
            }
            UIApplication.shared.setStatusBarHidden(false, with: .fade)
            self.isItFirstTimeForNewTaskButton = false
            self.updateNewTaskButton()
            self.directionButton.alpha = 0.0
            self.translateViewToPoint(point: 0, translatingView: self.runningJobsView)
        }
        //controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: true, completion: nil)
    }
}

extension HomeBaseController: DetailControllerDelegate {
    func updateForceTouch() {
        self.forceTouch = false
    }
}


extension HomeBaseController:NoInternetConnectionDelegate {
    //MARK: NoInternetConnectionView
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: NIB_NAME.noInternetConnectionView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:20, width: self.view.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.view.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
}

extension HomeBaseController:NavigationDelegate {
    //MARK: Navigation Bar
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: self.navigationYPos, width: self.view.frame.width, height: self.navigationHeight)
        self.setAttributedNavigationTitle()
        navigation.titleLabel.isUserInteractionEnabled = true
        navigation.backButton.isHidden = true
        navigation.hamburgerButton.isHidden = false
        navigation.editButton.isHidden = false
        navigation.editButton.setTitle("", for: .normal)
        navigation.editButton.addTarget(self, action: #selector(self.filterAction), for: UIControlEvents.touchUpInside)
        navigation.editButtonTrailingConstraint.constant = 15.0
        self.view.addSubview(navigation)
        self.updateFilterButton()
        
        /*=============== Tap gesture ================*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.gotoCalendar))
        tap.numberOfTapsRequired = 1
        navigation.titleLabel.addGestureRecognizer(tap)
    }
    
    func setAttributedNavigationTitle() {
        var currentDate = NetworkingHelper.sharedInstance.currentDate.getSelectedLocaleDateFormat(input: "yyyy-MM-dd", output: "MMMM d")
        if(currentDate == "") {
            let formatter = DateFormatter()
            formatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!
            formatter.dateFormat = "MMMM d"
            formatter.timeZone = TimeZone.current
            currentDate = formatter.string(from: Date())
        }
        
        var themeColor:UIColor = UIColor.white
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                themeColor = UIColor.white
                self.navigation.blurEffectView.frame.size.height = self.navigationHeight
                self.navigation.addSubview(self.navigation.blurEffectView)
                self.navigation.sendSubview(toBack: self.navigation.blurEffectView)
                UIApplication.shared.statusBarStyle = .lightContent
                //navigation.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
            } else {
                themeColor = UIColor.black
                self.navigation.blurEffectView.removeFromSuperview()
                if menuView == nil {
                    UIApplication.shared.statusBarStyle = .default
                } else {
                    UIApplication.shared.statusBarStyle = .lightContent
                }
                self.navigation.backgroundColor = UIColor.clear
            }
            
            self.navigation.hamburgerButton.color = themeColor
            self.navigation.editButton.tintColor = themeColor
            self.navigation.titleLabel.textColor = themeColor
        }
        
        
        let attributedString = NSMutableAttributedString(string: "\(currentDate)  ", attributes: [NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!, NSForegroundColorAttributeName:themeColor])
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = #imageLiteral(resourceName: "arrowDate").withRenderingMode(.alwaysTemplate)
        image1Attachment.bounds = CGRect(x: 0, y: 0, width: 13, height: 8)
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        attributedString.append(image1String)
        navigation.titleLabel.attributedText = attributedString
    }
    
    func updateFilterButton() {
        if Singleton.sharedInstance.isFilterApplied() == true {
            navigation.editButton.setImage(#imageLiteral(resourceName: "filterdApplied"), for: .normal)
        } else {
            navigation.editButton.setImage(#imageLiteral(resourceName: "combinedShape").withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
    func backAction() {
        self.setMenuView()
//        if self.pathSummaryView != nil {
//            self.hideTravelSummary()
//        }
        UIView.animate(withDuration: 0.3, animations: {
            self.googleMapView.alpha = 0.0
            self.menuView.alpha = 1.0
            self.newButton.transform = CGAffineTransform(translationX: 0, y: self.newButton.frame.height * 2)
            self.runningJobsView.transform = CGAffineTransform(translationX: 0, y: self.menuViewOrigin)
            self.runningJobsView.slideUpIcon.transform = CGAffineTransform.identity
        }, completion: { finished in
            self.runningJobsView.slideUpIcon.isHidden = false
        })
    }
    
    func filterAction() {
        Analytics.logEvent(ANALYTICS_KEY.FILTER_ICON, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.FILTER_ICON, customAttributes: [:])
        let controller = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.filterViewController) as! FilterViewController
        self.present(controller, animated: false, completion: { finished in
            self.translateViewToPoint(point: 0, translatingView: self.runningJobsView)
        })
    }
    
    func hideMenuView() {
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                UIApplication.shared.statusBarStyle = .lightContent
            } else {
                UIApplication.shared.statusBarStyle = .default
            }
        }
        
//        guard self.pathSummaryView == nil else {
//            UIView.animate(withDuration: 0.3, animations: {
//                self.googleMapView.alpha = 1.0
//                if self.menuView != nil {
//                    self.menuView.alpha = 0.0
//                }
//            }, completion: { finished in
//                guard self.menuView != nil else {
//                    return
//                }
//                self.menuView.removeFromSuperview()
//                self.menuView = nil
//            })
//            return
//        }
        
        if filteredTasksListArray.count == 0 {
            self.runningJobsView.slideUpIcon.isHidden = true
        }
        
        if self.pathSummaryView != nil {
            self.hideTravelSummary()
        }
        
        self.changeTableViewState(status: true, jobView: self.runningJobsView)
        UIView.animate(withDuration: 0.3, animations: {
            self.googleMapView.alpha = 1.0
            if self.menuView != nil {
                self.menuView.alpha = 0.0
            }
            self.runningJobsView.transform = CGAffineTransform.identity
            self.newButton.transform = CGAffineTransform.identity
            self.runningJobsView.slideUpIcon.transform = CGAffineTransform(rotationAngle: .pi)
        }, completion: { finished in
            guard self.menuView != nil else {
                return
            }
            self.menuView.removeFromSuperview()
            self.menuView = nil
        })
    }
}

extension HomeBaseController:JobListDelegate {
    
    func setRelatedTasksForRoutedTask() {
        if Singleton.sharedInstance.selectedTaskRelatedArray != nil {
            Singleton.sharedInstance.selectedTaskRelatedArray.removeAll()
        } else {
            Singleton.sharedInstance.selectedTaskRelatedArray = [TasksAssignedToDate]()
        }
        for i in 0..<Singleton.sharedInstance.selectedTaskDetails.related_job_indexes.count {
            let routedTask = NetworkingHelper.sharedInstance.dayTasksListArray[0]
            let index = Singleton.sharedInstance.selectedTaskDetails.related_job_indexes[i] as! Int
            if index < routedTask.count {
                let relatedTask = routedTask[index]
                if relatedTask.pickupDeliveryRelationship == Singleton.sharedInstance.selectedTaskDetails.pickupDeliveryRelationship {
                    Singleton.sharedInstance.selectedTaskRelatedArray.append(relatedTask)
                }
            }
        }
        
    }
    
    func gotoTaskDetailPage(index:Int, startStatus:Int, section:Int) {
        Analytics.logEvent(ANALYTICS_KEY.TASK_LIST_CARD, parameters: [:])
        if(section < self.filteredTasksListArray.count && index < filteredTasksListArray[section].count) {
            var isTaskDetailControllerExist = false
            if(NSStringFromClass((self.navigationController?.visibleViewController!.classForCoder)!).components(separatedBy: ".").last! == STORYBOARD_ID.detailController) {
                isTaskDetailControllerExist = true
                taskDetailViewController = self.navigationController?.visibleViewController as! DetailController
            } else {
                taskDetailViewController = storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.detailController) as! DetailController
            }
            
            Singleton.sharedInstance.selectedTaskDetails = filteredTasksListArray[section][index]
            if(startStatus != 0) {
                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.started
            }
            
            if(isTaskDetailControllerExist == false) {
                self.myCurrentLocation = nil
                //taskSelected = TransitionFromVC.fromTaskVC
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                guard section < self.filteredTasksListArray.count else {
                    return
                }
                Singleton.sharedInstance.selectedTaskDetails = self.filteredTasksListArray[section][index]
                if Singleton.sharedInstance.selectedTaskDetails.isRouted == 0 {
                    Singleton.sharedInstance.selectedTaskRelatedArray = self.filteredTasksListArray[section]
                } else {
                    self.setRelatedTasksForRoutedTask()
                }
                
                UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.runningJobsView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
                    self.runningJobsView.slideUpIcon.transform = CGAffineTransform.identity
                    self.newButton.transform = CGAffineTransform(translationX: 0, y: self.newButton.frame.height)
                    self.navigation.changeHamburgerButton()
                    self.nextTaskButton.alpha = 0
                    self.directionButton.alpha = 0
                    self.previousTaskButton.alpha = 0
                    self.currentLocationButton.isHidden = true
                    
                    self.navigation.titleLabel.text = "\(Singleton.sharedInstance.selectedTaskDetails.jobId!)"
                    self.navigation.titleLabel.transform = CGAffineTransform(translationX: -self.navigation.titleLabel.frame.width/2 + getAspectRatioValue(value: 30.0), y: 0.0)
                    self.navigation.editButton.alpha = 0.0
                    
                }, completion: { finished in
                    self.taskDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.detailController) as! DetailController
                    
                    self.taskDetailViewController.callback = { message in
                        DispatchQueue.main.async {
                            guard UIApplication.shared.applicationState == UIApplicationState.active else {
                                return
                            }
                            self.setDataForNoValueReceived()
                            self.googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
                            self.googleMapView.animate(toZoom: 14.0)
                            self.navigation.alpha = 1
                            self.navigation.changeHamburgerButton()
                            self.updateNewTaskButton()
                            self.currentLocationButton.isHidden = false
                            //self.setTaskButton(index: 0)
                            self.translateViewToPoint(point: 0, translatingView: self.runningJobsView)
                            UIView.animate(withDuration: 0.4, animations: {
                                self.setAttributedNavigationTitle()
                                self.navigation.titleLabel.transform = CGAffineTransform.identity
                                self.navigation.editButton.alpha = 1.0
                            }, completion: { finished in
                                
                            })
                        }
                    }
                    
                    self.taskDetailViewController.drawPathClosuer = { message in
                        self.drawPathFromCurrentToDestination(controller: self.taskDetailViewController)
                    }
                    
                    self.taskDetailViewController.updateMarkerClosure = { message in
                        // self.updateMarkerForTaskDetail(fromClosure: true)
                    }
                    self.taskDetailViewController.delegate = self
                    self.present(self.taskDetailViewController, animated: false, completion: { finished in
                        if self.navigation != nil {
                            self.navigation.alpha = 0
                        }
                        if self.taskDetailViewController.navigation != nil {
                            self.taskDetailViewController.navigation.alpha = 1
                        }
                    })
                })
            } else {
                isTaskDetailControllerExist = false
                NetworkingHelper.sharedInstance.notificationJobId = -1
                Singleton.sharedInstance.lastNotificationFlagType = 0
                taskDetailViewController.updateView()
            }
            
        }
        
    }
    
    func gotoTaxiController(index:Int) {
        let taxiController = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.taxiController) as! TaxiController
        self.navigationController?.pushViewController(taxiController, animated: true)
        //self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 0.35)
    }
    
    func slideButtonAction() {
        guard menuView == nil else {
            self.hideMenuView()
            return
        }
        
        if self.runningJobsView.transform.ty > 0 {
            self.directionButton.alpha = 0.0
            self.translateViewToPoint(point: 0, translatingView: self.runningJobsView)
        } else {
            self.translateViewToPoint(point: self.minimizeOrigin, translatingView: self.runningJobsView)
        }
    }
    
}

extension HomeBaseController:MenuListDelegate {
    func dismissMenu() {
        self.hideMenuView()
    }
    
    func messageToShow(_ message: String) {
        self.showInvalidAccessTokenPopup(message: message)
    }
    
    func gotoNotificationController() {
        Analytics.logEvent(ANALYTICS_KEY.NOTIFICATION_ICON, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.NOTIFICATION_ICON, customAttributes: [:])
        let controller = storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.allNotificationController) as! AllNotificationsController
        self.present(controller, animated: true, completion: nil)
        self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 0.35)
    }
    
    
    func gotoProfile() {
        Analytics.logEvent(ANALYTICS_KEY.PROFILE_ACTION, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.PROFILE_ACTION, customAttributes: [:])
        if (Singleton.sharedInstance.fleetDetails.showTeams == 0) && (Singleton.sharedInstance.fleetDetails.isTeamEnabled == 0) {
            self.gotoProfileVC()
        } else {
            self.getTeamsAndTags()
        }
        
    }
    
    func gotoCalendar() {
        Analytics.logEvent(ANALYTICS_KEY.CALENDAR_ICON, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.CALENDAR_ICON, customAttributes: [:])
        let calendarController = storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.calendarViewController) as! CalendarViewController
        self.present(calendarController, animated: false, completion: { finished in
            self.translateViewToPoint(point: 0, translatingView: self.runningJobsView)
        })
    }
    
    func gotoSchedule() {
        let availabilityController = self.storyboard?.instantiateViewController(withIdentifier: "AvailabilityController") as! AvailabilityController
        self.navigationController?.pushViewController(availabilityController, animated: true)
        self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 0.35)
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.CALENDAR_AVAILABILITY_BUTTON, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.CALENDAR_AVAILABILITY_BUTTON, customAttributes: [:])
    }
    
    func gotoEarningController() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.earningController) as! EarningController
        self.navigationController?.pushViewController(controller, animated: true)
        self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 1)
    }
    
    func settingsAction() {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.SETTINGS, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.SETTINGS, customAttributes: [:])
        let vc = self.storyboard!.instantiateViewController(withIdentifier: STORYBOARD_ID.settingViewController) as! SettingViewController
        self.navigationController!.pushViewController(vc, animated: true)
        self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 1)
    }
    
    func tutorialAction() {
        self.hideMenuView()
        UIApplication.shared.statusBarStyle = .lightContent
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.TUTORIAL, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.TUTORIAL, customAttributes: [:])
        let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.tutorialController) as! TutorialController
        vc.navigationYPos = self.navigationYPos
        vc.navigationHeight = self.navigationHeight
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(vc, animated: false, completion: nil)
        vc.setRequiredTutorial(tutorialLocaion: TUTORIAL_START_POINT.FULL_TUTORIAL)
        //self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func supportAction() {
        Analytics.logEvent(ANALYTICS_KEY.SUPPORT_ACTION, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.SUPPORT_ACTION, customAttributes: [:])
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
            self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 0.35)
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NOT_SUPPORT_EMAIL, isError: .message)
        }
        
     }
    
    func gotoFuguChat() {
        guard Singleton.sharedInstance.fleetDetails.fugu_token != "" else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INVALID_FUGU_TOKEN, isError: .error)
            return
        }
        self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 0.35)
//        FuguConfig.shared = FuguConfig.init(fullName: Singleton.sharedInstance.fleetDetails.name!, email: Singleton.sharedInstance.fleetDetails.email, phoneNumber: Singleton.sharedInstance.fleetDetails.phone, userUniqueKey: "\(Singleton.sharedInstance.fleetDetails.fleetId!)", deviceToken: Singleton.sharedInstance.getDeviceToken())
//
        
        //FuguConfig.shared = FuguConfig.init(credentialType: .reseller)
       // FuguConfig.shared.setCredential(withToken: Singleton.sharedInstance.fleetDetails.fugu_token!, referenceId: Singleton.sharedInstance.fleetDetails.fleetId!)
        
//        FuguConfig.shared.setCredential(withToken: Singleton.sharedInstance.fleetDetails.fugu_token!, referenceId: Singleton.sharedInstance.fleetDetails.userId!, appType: "1")
//
//        let driverId = "driver + \(Singleton.sharedInstance.fleetDetails.fleetId!)"
//        let fuguUserDetail = FuguUserDetail(fullName: Singleton.sharedInstance.fleetDetails.name!, email: Singleton.sharedInstance.fleetDetails.email!, phoneNumber: Singleton.sharedInstance.fleetDetails.phone!, userUniqueKey: driverId)
//        FuguConfig.shared.updateUserDetail(userDetail: fuguUserDetail)
//
//
//        let fuguTheme = FuguTheme()
//        fuguTheme.headerTextFont = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
//        fuguTheme.leftBarButtonImage = #imageLiteral(resourceName: "back_btn")
//        fuguTheme.headerBackgroundColor = UIColor.black
//        fuguTheme.headerTextColor = COLOR.positiveButtonTitleColor
        //fuguTheme.backgroundColor = UIColor.black
        //Singleton.sharedInstance.initilizeFugu()
//        FuguConfig.shared.presentChatsViewController()
//        FuguConfig.shared.setCustomisedFuguTheme(theme: fuguTheme)
//        if APP_STORE.value == 0 {
//            FuguConfig.shared.switchEnvironment("TEST")
//        }
//        FuguConfig.shared.presentChatsViewController()
       // FuguConfig.shared.presentChatsViewController(self.navigationController)
        
        //FuguConfig.shared.allConversationBackButtonText = "Close"
        //FuguConfig.shared.chatScrenBackButtonText = "Back"
        //FuguConfig.shared.backButtonFont = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
        //FuguConfig.shared.backButtonTextColor = UIColor.blue
        //FuguConfig.shared.navigationTitleTextAlignMent = NSTextAlignment.center
        FuguConfig.shared.presentChatsViewController()
    }
    //MARK: MFMailComposeViewController
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let app_version = Singleton.sharedInstance.getVersion()//Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients([CONTACT_ID])
        mailComposerVC.setSubject("Support Request")
        mailComposerVC.setMessageBody("Agent ID: \(Singleton.sharedInstance.fleetDetails.fleetId!)\n iOS App Version: \(app_version)\n Current Location: \(LocationTracker.sharedLocationManager().location!.coordinate.latitude, LocationTracker.sharedLocationManager().location!.coordinate.longitude)\n\n", isHTML: false)
        return mailComposerVC
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    internal func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func logoutAction() {
        
    }
    
    func sendRequestForLogout() {
        ActivityIndicator.sharedInstance.showActivityIndicator((self.navigationController?.visibleViewController)!)
        if let accessToken = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as? String {
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.logout, params: [
                "access_token":accessToken as AnyObject], httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                    DispatchQueue.main.async {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        if isSucceeded == true {
                            switch(response["status"] as! Int) {
                            case STATUS_CODES.SHOW_DATA:
                                Auxillary.logoutFromDevice()
                                NotificationCenter.default.removeObserver(self)
                                break
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                self.showInvalidAccessTokenPopup(message: response["message"] as! String!)
                                break
                                
                            default:
                                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                                break
                            }
                        } else {
                            Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        }
                    }
            })
        } else {
            Auxillary.logoutFromDevice()
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    func syncAction() {
        let offlineDataModel = OfflineDataModel()
        if(offlineDataModel.uploadUnsyncedTaskToServer() == false) {
            if(offlineDataModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                self.showInvalidAccessTokenPopup(message: offlineDataModel.hitFailMessage)
            } else {
                let alert = UIAlertController(title: TEXT.ERROR, message: ERROR_MESSAGE.SYNC_FAILED_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
                let tryAgain = UIAlertAction(title: TEXT.TRY_AGAIN, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.syncAction()
                    })
                })
                alert.addAction(tryAgain)
                
                let offlineData = UIAlertAction(title: TEXT.LOGOUT_WITHOUT_SYNC, style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.sendRequestForLogout()
                    })
                })
                alert.addAction(offlineData)
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            self.sendRequestForLogout()
        }
    }
    
    func createTaskAction() {
        if IJReachability.isConnectedToNetwork() == false {
            self.showInternetToast()
        } else {
            Analytics.logEvent(ANALYTICS_KEY.CREATE_TASK, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.CREATE_TASK, customAttributes: [:])
            Singleton.sharedInstance.createTaskArray.removeAll()
            /*===============   Checking Layout Type   ========================*/
            //            switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
            //            case LAYOUT_TYPE.pickupAndDelivery:
            let order = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.createNewOrder) as! CreateNewOrder
            self.navigationController!.pushViewController(order, animated: false)
            // break
            //            case LAYOUT_TYPE.appointment, LAYOUT_TYPE.fos:
            //                let order = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.addDeliveryPoint) as! AddDeliveryPoint
            //                self.navigationController!.pushViewController(order, animated: false)
            //                break
            //            default:
            //                break
            //            }
            self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 0.35)
        }
    }
    
    func gotoWebViewController(url:String, name:String) {
        let invoiceController = InvoiceController()
        invoiceController.titleName = name
        invoiceController.webUrl = url
        self.present(invoiceController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.35) {
            self.hideMenuView()
        }
    }
    
    func scanToAssignAction(){
        self.scannerAction(index: 0)
    }
    
    func getTeamsAndTags() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        let params:[String:Any] = ["access_token": Singleton.sharedInstance.getAccessToken()]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.get_teams_tags_onsignup, params: params as [String:AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSuccessed, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            DispatchQueue.main.async {
                print(response)
                if isSuccessed == true{
                    switch (response["status"] as! Int){
                    case STATUS_CODES.SHOW_DATA:
                        if let data = response["data"] as? [String:Any] {
                            if let tags = data["tags"] as? [String] {
                                Singleton.sharedInstance.fleetDetails.tagsArray = tags
                            }
                            if let teams = data["teams"] as? [String] {
                                Singleton.sharedInstance.fleetDetails.teamsArray = teams
                            }
                        }
                        self.gotoProfileVC()
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(message: response["message"] as! String)
                        break
                    default:
                        self.gotoProfileVC()
                        break
                    }
                } else {
                    self.gotoProfileVC()
                }
            }
        }
    }
    
    func gotoProfileVC() {
        let profileController = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.profileViewController) as! ProfileViewController
        self.navigationController?.pushViewController(profileController, animated: true)
        self.perform(#selector(self.hideMenuView), with: nil, afterDelay: 0.35)
    }
}

//MARK: SCANNER
extension HomeBaseController:ScannerDelegate {
    func scannerAction(index: Int) {
        self.view.endEditing(true)
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
            self.startScanning()
        } else {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                DispatchQueue.main.async {
                    if granted == true {
                        self.startScanning()
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.REJECTED_CAMERA_SUPPORT, isError: .error)
                    }
                }
            })
        }
    }
    
    func startScanning() {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.SIGNUP_BARCODE_SCAN, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.SIGNUP_BARCODE_SCAN, customAttributes: [:])
        guard self.scanView == nil else {
            return
        }
        scanView = ScanView(frame: self.view.frame)
        scanView.delegate = self
        self.view.addSubview(scanView)
        self.view.endEditing(true)
        scanView.setScanView()
        scanView.setManualScanField()
    }
    
    //MARK: Scanner Delegate
    func decodedOutput(_ value: String) {
        self.serverHitForTask(scanedValue: value)
    }
    
    func dismissScannerView() {
        guard scanView != nil else {
            return
        }
        self.scanView.removeFromSuperview()
        self.scanView = nil
    }
    
    func serverHitForTask(scanedValue:String) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        var params:[String : Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
        params["barcode"] = scanedValue
        self.hideMenuView()
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.scan_tasks, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSuccessed, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                print(response)
                
                if isSuccessed == true {
                    switch (response["status"] as! Int){
                    case STATUS_CODES.SHOW_DATA:
                        self.showAlertForRescan(title: (response["message"] as? String)!, message: TEXT.SCAN_ANOTHER)
                        //self.sendRequestForOnlineData(Date().formattedWith(NetworkingHelper.sharedInstance.currentDate))
                        //Singleton.sharedInstance.showErrorMessage(error: TEXT.TASK_ASSIGNED_SUCCESSFULLY, isError: .success)
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(message: response["message"] as! String)
                        break
                    default:
                        break
                    }
                } else {
                    self.showAlertForRescan(title: "", message: "\((response["message"] as? String)!). \(TEXT.RE_SCAN)")
                    //Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func showAlertForRescan(title:String, message:String) {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: TEXT.CANCEL, style: .default, handler: nil)
        let scan = UIAlertAction(title: TEXT.SCAN, style: .default) { (alert) in
            self.scannerAction(index: 0)
        }
        alert.addAction(cancel)
        alert.addAction(scan)
        present(alert, animated: true, completion: nil)
    }
}

@available(iOS 9.0, *)
extension HomeBaseController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = runningJobsView.taskListTable.indexPathForRow(at: location),
            let cell = runningJobsView.taskListTable.cellForRow(at: indexPath) else {
                return nil
        }
        
        guard indexPath.section < self.filteredTasksListArray.count && indexPath.row < filteredTasksListArray[indexPath.section].count else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
            return nil
        }
        
        guard filteredTasksListArray[indexPath.section][indexPath.row].appOptionalFieldArray!.count > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
            return nil
        }
        
        Singleton.sharedInstance.selectedTaskDetails = filteredTasksListArray[indexPath.section][indexPath.row]
        
        if filteredTasksListArray[indexPath.section][indexPath.row].vertical == VERTICAL.TAXI.rawValue {
            
            guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray != nil && (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > 0 else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
                return nil
            }
            switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
            case JOB_STATUS.successful, JOB_STATUS.canceled, JOB_STATUS.failed, JOB_STATUS.declined:
                break
            default:
                self.gotoTaxiController(index: 0)
                break
            }
        }
        
        self.taskDetailViewController = storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.detailController) as! DetailController
        self.taskDetailViewController.delegate = self
        self.taskDetailViewController.preferredContentSize = CGSize(width: 0.0, height: 600)
        self.taskDetailViewController.fromForceTouch = true
        previewingContext.sourceRect = cell.frame
        if Singleton.sharedInstance.selectedTaskDetails.isRouted == 0 {
            Singleton.sharedInstance.selectedTaskRelatedArray = self.filteredTasksListArray[indexPath.section]
        } else {
            self.setRelatedTasksForRoutedTask()
        }
        return taskDetailViewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        self.forceTouch = true
        self.runningJobsView.slideUpIcon.transform = CGAffineTransform.identity
        
        self.navigation.titleLabel.text = "\(Singleton.sharedInstance.selectedTaskDetails.jobId!)"
        self.runningJobsView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
        self.setTaskDetailPageFromForceTouch()
        self.nextTaskButton.alpha = 0
        self.directionButton.alpha = 0
        self.previousTaskButton.alpha = 0
    }
    
    func setTaskDetailPageFromForceTouch() {
        Analytics.logEvent(ANALYTICS_KEY.TASK_LIST_CARD, parameters: [:])
        
        self.myCurrentLocation = nil
        ActivityIndicator.sharedInstance.hideActivityIndicator()
        
        self.runningJobsView.slideUpIcon.transform = CGAffineTransform.identity
        self.newButton.transform = CGAffineTransform(translationX: 0, y: self.newButton.frame.height)
        self.navigation.changeHamburgerButton()
        self.currentLocationButton.isHidden = true
        self.navigation.titleLabel.text = "\(Singleton.sharedInstance.selectedTaskDetails.jobId!)"
        self.navigation.titleLabel.transform = CGAffineTransform(translationX: -self.navigation.titleLabel.frame.width/2 + getAspectRatioValue(value: 30.0), y: 0.0)
        
        self.navigation.editButton.alpha = 0.0
        self.taskDetailViewController.callback = { message in
            self.googleMapView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            self.googleMapView.animate(toZoom: 14.0)
            self.navigation.alpha = 1
            self.navigation.changeHamburgerButton()
            
            self.updateNewTaskButton()
            self.currentLocationButton.isHidden = false
            self.translateViewToPoint(point: 0, translatingView: self.runningJobsView)
            
            UIView.animate(withDuration: 0.4, animations: {
                self.setAttributedNavigationTitle()
                self.navigation.titleLabel.transform = CGAffineTransform.identity
                
                self.navigation.editButton.alpha = 1.0
            }, completion: { finished in
                
            })
        }
        
        self.taskDetailViewController.drawPathClosuer = { message in
            self.drawPathFromCurrentToDestination(controller: self.taskDetailViewController)
        }
        
        self.present(self.taskDetailViewController, animated: false, completion: { finished in
            self.navigation.alpha = 0
            self.taskDetailViewController.navigation.alpha = 1
            
        })
        
    }
    
}

extension HomeBaseController: WarningViewDelegate {

    func removeWarningView() {
        self.navigationYPos = 0.0
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.navigationHeight = HEIGHT.navigationHeight
                self.navigation.frame.size.height = self.navigationHeight
                self.navigation.frame.origin.y = self.navigationYPos
                self.runningJobsView.frame.origin.y = HEIGHT.navigationHeight + 10
                self.warningView.frame.size.height = 0.0
                self.navigation.blurEffectView.frame.size.height = self.navigationHeight
            }) { (finished) in
                self.warningView.removeFromSuperview()
            }
        }
    }
}
