//
//  EarningCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 19/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class EarningCell: UITableViewCell {

    @IBOutlet var topLine: UIView!
    @IBOutlet var dot: UIView!
    @IBOutlet var bottomLine: UIView!
    @IBOutlet var firstLine: UILabel!
    @IBOutlet var secondLine: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var rightArrow: UIImageView!
    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.firstLine.textColor = COLOR.EXTRA_LIGHT_COLOR
        self.firstLine.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.extraSmall)
        
        self.amountLabel.textColor = COLOR.TEXT_COLOR
        self.amountLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.medium)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDotAndPatternLine(indexPath:IndexPath, sectionCount:Int) {
        self.dot.backgroundColor = COLOR.themeForegroundColor
        self.dot.layer.cornerRadius = 4.5
        self.dot.layer.masksToBounds = true
        
        self.topLine.backgroundColor = COLOR.EXTRA_LIGHT_COLOR
        self.bottomLine.backgroundColor = COLOR.EXTRA_LIGHT_COLOR
    
        if sectionCount > 1 {
            if indexPath.row == 0 {
                self.topLine.isHidden = true
                self.bottomLine.isHidden = false
                self.firstLine.isHidden = false
            } else if indexPath.row == sectionCount - 1 {
                self.topLine.isHidden = false
                self.bottomLine.isHidden = true
                self.firstLine.isHidden = true
            } else {
                self.topLine.isHidden = false
                self.bottomLine.isHidden = false
                self.firstLine.isHidden = true
            }
        } else {
            self.topLine.isHidden = true
            self.bottomLine.isHidden = true
            self.firstLine.isHidden = false
        }
    }
    
}
