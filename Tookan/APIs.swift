//
//  APIs.swift
//  Tookan
//
//  Created by cl-macmini-45 on 09/01/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class APIs: NSObject {

}

struct SERVER {
    static let dev = "https://nadeem-api.tookanapp.com/"
    static let test = "https://api2.tookanapp.com/"//
    static let live = "https://api.tookanapp.com/" //"http://52.23.253.217:8888/" //
    static let beta = "https://node1-api.tookanapp.com:444/"//"http://52.90.238.106:8888/"
    static var custom = "https://nadeem-api.tookanapp.com:444/"//"https://test.tookanapp.com:8004/"
}

struct HTTP_METHOD {
    static let POST = "POST"
    static let GET = "GET"
    static let PUT = "PUT"
}

struct STATUS_CODES {
    static let INVALID_ACCESS_TOKEN = 101
    static let BAD_REQUEST = 400
    static let UNAUTHORIZED_ACCESS = 401
    static let PICKUP_TASK = 410
    static let ERROR_IN_EXECUTION = 500
    static let SHOW_ERROR_MESSAGE = 304
    static let NOT_FOUND_MESSAGE = 404
    static let UNAUTHORIZED_FOR_AVAILABILITY = 210
    static let SHOW_MESSAGE = 201
    static let SHOW_DATA = 200
    static let SLOW_INTERNET_CONNECTION = 999
    static let DELETED_TASK = 501
}

struct API_NAME {
    static let fleet_login = "fleet_login"
    static let access_token_login = "fleet_access_token_login"
    static let update_fleet_location = "update_fleet_location"
    static let forgot_password = "fleet_forgot_password_from_email"
    static let forgot_password_admin = "users_forgot_password_from_email"
    static let view_task_month = "view_tasks"
    static let add_task_detail = "add_task_details"
    static let update_task_detail = "update_task_detail"
    static let delete_task_detail = "delete_task_detail"
    static let logout = "fleet_logout"
    static let updateCustomField = "update_custom_fields"
    static let updateTableCustomField = "update_tb_custom_field"
    static let changePassword = "fleet_change_password"
    static let changeFleetStatus = "change_fleet_status"
    static let masking = "masking"
    static let register = "register"
    static let adminLogin = "user_login"
    static let getTemplates = "get_templates"
    static let uploadReferenceImage = "upload_reference_images"
    static let createTask = "fleet_create_task"
    static let viewTaskForId = "view_task_via_id"
    static let viewAllNotification = "view_all_push_notifications"
    static let viewFilteredNotification = "view_filtered_push_notifications"
    static let clearNotification = "clear_notifications"
    static let uploadOfflineData = "update_offline_data"
    static let getFleetHistory = "get_fleet_history"
    static let viewTaskForDate = "view_tasks_for_date"
    static let changeJobStatus = "change_job_status"
    static let getExtra = "get_extras"
    static let editProfile = "edit_profile_information2"
    static let setNotificationTone = "set_notification_tone"
    static let getWeekAvailability = "get_week_availablilty"
    static let setDayAvailability = "set_day_availablilty"
    static let getRelatedTask = "get_related_tasks_fleet"
    static let get_new_tasks = "get_new_tasks"
    static let checkUniqueUserName = "check_unique_fleet_username"
    static let fleet_create_multiple_tasks = "fleet_create_multiple_tasks"
    static let fleet_signup = "fleet_signup"
    static let verify_otp = "verify_fleet_signup_otp"
    static let resend_signup_otp = "resend_signup_otp"
    static let submit_signup_template = "submit_signup_template"
    static let acknowledge_signup_verification = "acknowledge_signup_verification"
    static let fetch_fleet_weekly_job_report = "fetch_fleet_weekly_job_report"
    static let fetch_fleet_job_report = "fetch_fleet_job_report"
    static let add_ratings = "add_ratings"
    static let view_signup_fleet = "view_signup_fleet"
    static let scan_tasks = "scan_tasks"
    static let get_teams_tags_onsignup = "get_teams_tags_onsignup"
}
