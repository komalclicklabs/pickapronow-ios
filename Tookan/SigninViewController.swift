//
//  SigninViewController.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 3/31/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import CoreLocation
import Crashlytics
import FirebaseAnalytics


class SigninViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var signupButton: UIButton!
    @IBOutlet var signinButton: UIButton!
    @IBOutlet var signupBackgroundView: UIView!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgetButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var lineBelowUserName: UIView!
    @IBOutlet weak var lineBelowPassword: UIView!
    @IBOutlet weak var signInButtomBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var signInLabelTopConstraint: NSLayoutConstraint!
   
    var signInButtomBottomConstraintDefaultValue = 0.0
    let signInLabelTopConstraintDefaultValue:CGFloat = 90.0
    var getLocationTimer:Timer!
    var isUserLocationGet = false
    var locationAlert:UIAlertController!
    let userInfoObjectContext = DatabaseManager.sharedInstance.managedObjectContext

    fileprivate var accessToken: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*================Setting Sign In Label Design======================*/
        signInLabel.font = UIFont(name: UIFont().MontserratExtraLight, size: FONT_SIZE.extra_large)
        signInLabel.textColor =  COLOR.LIGHT_COLOR
        signInLabel.text = TEXT.SIGN_IN
        
        lineBelowUserName.backgroundColor = COLOR.LOGIN_LINE_COLOR
        lineBelowPassword.backgroundColor = COLOR.LOGIN_LINE_COLOR
        self.setButtons()
        self.setTextField()
        self.setSegmentForDevelopment()
        if SHOW_SIGNUP == 1 {
            self.setSignupViewButtons()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let userName = UserDefaults.standard.value(forKey: "useremail") as? String {
            if !userName.blank(userName){
                userNameTextField.text = userName
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func setTextField() {
        /*================Setting Text Fields Design======================*/
        let attributes:[String:Any]? = [
            NSForegroundColorAttributeName: COLOR.TEXT_COLOR,
            NSFontAttributeName :  UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular) ?? ""
        ]
        self.userNameTextField.attributedPlaceholder = NSAttributedString(string: "\(TEXT.USERNAME)/\(TEXT.MOBILE)", attributes: attributes )
        self.passwordTextField.attributedPlaceholder = NSAttributedString(string: TEXT.PASSWORD, attributes: attributes )
        self.userNameTextField.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.passwordTextField.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.userNameTextField.textColor = COLOR.TEXT_COLOR
        self.passwordTextField.textColor = COLOR.TEXT_COLOR
        self.userNameTextField.delegate = self
        self.passwordTextField.delegate = self
        self.userNameTextField.returnKeyType = .next
        self.userNameTextField.tintColor = COLOR.themeForegroundColor
        self.passwordTextField.returnKeyType = .done
        self.passwordTextField.tintColor = COLOR.themeForegroundColor
    }
    
    func setButtons() {
        /*================Setting Forget Design======================*/
        self.forgetButton?.setTitle(TEXT.FORGOT, for: .normal)
        self.forgetButton.setTitleColor(COLOR.themeForegroundColor, for: .normal)
        self.forgetButton?.titleLabel?.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
        let attributedTitle = NSAttributedString(string: TEXT.FORGOT, attributes: [NSKernAttributeName: 0.8])
        forgetButton.setAttributedTitle(attributedTitle, for: .normal)
        
        /*================Setting SignIn Button Design======================*/
        self.signInButton?.setTitle(TEXT.SIGN_IN, for: .normal)
        self.signInButton?.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.signInButton.backgroundColor = COLOR.themeForegroundColor
        self.signInButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
    }
    
    func setSignupViewButtons() {
        /*=============== Signup background =====================*/
        self.signupBackgroundView.isHidden = false
        self.signInButton.isHidden = true
        
        /*================Setting SignIn Button Design======================*/
        self.signinButton?.setTitle(TEXT.SIGN_IN, for: .normal)
        self.signinButton?.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.signinButton.backgroundColor = COLOR.themeForegroundColor
        self.signinButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.signinButton.layer.cornerRadius = 3.0
        
        /*================ Signup Button ======================*/
        let attributedTitle = NSMutableAttributedString(string: TEXT.NEW_USER, attributes: [NSForegroundColorAttributeName:COLOR.NEW_USER_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)!])
        attributedTitle.addAttribute(NSKernAttributeName, value: CGFloat(0.5), range: NSRange(location: 0, length: attributedTitle.length))
        
        let signupTitle = NSMutableAttributedString(string: TEXT.SIGNUP, attributes: [NSForegroundColorAttributeName:COLOR.themeForegroundColor, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)!])
        signupTitle.addAttribute(NSKernAttributeName, value: CGFloat(0.5), range: NSRange(location: 0, length: signupTitle.length))
        
        attributedTitle.append(signupTitle)
        self.signupButton.setAttributedTitle(attributedTitle, for: .normal)
    }
    
    
    func setSegmentForDevelopment() {
        /*----------------- It is only for Testing purpose -----------*/
        switch(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) {
        case SERVER.dev:
            segmentControl.selectedSegmentIndex = 0
            break
        case SERVER.test:
            segmentControl.selectedSegmentIndex = 1
            break
        case SERVER.live:
            segmentControl.selectedSegmentIndex = 2
            break
        case SERVER.beta:
            segmentControl.selectedSegmentIndex = 3
            break
        case SERVER.custom:
            segmentControl.selectedSegmentIndex = 4
        default:
            break
        }
        
        if(APP_STORE.value == 1) {
            segmentControl.isHidden = true
        } else {
            segmentControl.isHidden = false
        }
    }

    @IBAction func valueChanged(_ sender: Any) {
        switch(segmentControl.selectedSegmentIndex) {
        case 0:
            UserDefaults.standard.setValue(SERVER.dev, forKey: USER_DEFAULT.selectedServer)
            break
        case 1:
            UserDefaults.standard.setValue(SERVER.test, forKey: USER_DEFAULT.selectedServer)
            break
        case 2:
            UserDefaults.standard.setValue(SERVER.live, forKey: USER_DEFAULT.selectedServer)
            break
        case 3:
            UserDefaults.standard.setValue(SERVER.beta, forKey: USER_DEFAULT.selectedServer)
            break
        case 4:
            UserDefaults.standard.setValue(SERVER.custom, forKey: USER_DEFAULT.selectedServer)
            if let viewController = UIStoryboard(name: STORYBOARD_NAME.main, bundle: nil).instantiateViewController(withIdentifier: STORYBOARD_ID.customServerController) as? CustomServerController {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
            break
        default:
            break
        }
    }
    
    @IBAction func forgotAction(_ sender: Any) {
        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.forgotPasswordViewController) as! ForgotPasswordViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func signinActionFromSignup(_ sender: Any) {
        self.signInAction(sender)
    }
    
    @IBAction func singupAction(_ sender: Any) {
        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.signupViewController) as! SignupViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.SIGN_IN, parameters: [:])
        if userNameTextField.text!.trimText.isEmpty{
            Singleton.sharedInstance.showErrorMessage(error: "\(ERROR_MESSAGE.PLEASE_ENTER) \(TEXT.USERNAME)" , isError: .error)
            return
        } else if passwordTextField.text!.isEmpty {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.PLEASE_ENTER_PASSWORD, isError: .error)
            return
        } else {
            if LocationTracker.sharedInstance().sendFirstTimeLocation == true {
                LocationTracker.sharedInstance().startLocationTracking()
                getLocationTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.checkGetLocation), userInfo: nil, repeats: true)
            } else {
                if let userlocationDict = UserDefaults.standard.object(forKey: USER_DEFAULT.lastAccurateUserLocation) as? [String:Any] {
                    if IJReachability.isConnectedToNetwork() == true {
                        self.loginUser(userlocationDict)
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
                    }
                } else {
                    LocationTracker.sharedInstance().startLocationTracking()
                    getLocationTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.checkGetLocation), userInfo: nil, repeats: true)
                }
            }
        }

    }
    func loginUser(_ userlocationDictionary:[String:Any]) {
        UserDefaults.standard.setValue(userNameTextField.text!, forKey: USER_DEFAULT.useremail)
        self.activityIndicator.startAnimating()
        let deviceToken =  UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) != nil ? UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) as? String : "No deviceToken"
        let latitude = userlocationDictionary["latitude"] as! String
        let longitude = userlocationDictionary["longitude"] as! String
        var param:[String:Any] = ["device_type": DEVICE_TYPE]
        param["email"] = userNameTextField.text!
        param["password"] = passwordTextField.text!
        param["device_token"] = "\(deviceToken!)"
        param["latitude"] = latitude
        param["longitude"] = longitude
        param["store_version"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        param["device_desc"] = "\(DeviceInfo.oSVersion) \(DeviceInfo.deviceType) \(DeviceInfo.deviceName as String)"// + DeviceInfo.deviceType + " " + (DeviceInfo.deviceName as String)
        param["app_version"] = app_version
        param["bat_lvl"] = "\(UIDevice.current.batteryLevel * 100)"
//        let param = [
//            "email"         : userNameTextField.text!,
//            "password"      : passwordTextField.text!,
//            //"device_type"   : DEVICE_TYPE,
//            //"device_token"  : "\(deviceToken!)",
//            //"latitude"      : latitude,
//            //"longitude"     : longitude,
//            //"device_desc"       : "\(DeviceInfo.oSVersion) " + DeviceInfo.deviceType + " " + (DeviceInfo.deviceName as String),
//            //"store_version"     : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String,
//            "app_version"   : app_version,
//            "bat_lvl" : "\(UIDevice.current.batteryLevel * 100)"
//        ]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.fleet_login, params: param as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                if(isSucceeded == true) {
                    self.getJsonResponseDictionary(response)
                } else {
                    self.signInButton.isEnabled = true
                    self.signinButton.isEnabled = true
                    self.activityIndicator.stopAnimating()
                    Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                }
            }
        }
        signInButton.isEnabled = false
        signinButton.isEnabled = false
        userNameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    func checkGetLocation() {
        if(isUserLocationGet == false) {
            if CLLocationManager.locationServicesEnabled() == false {
                self.locationServiceDisabledAlert()
            }
            else {
                let authorizationStatus = CLLocationManager.authorizationStatus()
                if authorizationStatus == CLAuthorizationStatus.denied || authorizationStatus == CLAuthorizationStatus.restricted {
                    activityIndicator.stopAnimating()
                    self.locationServiceDisabledAlert()
                } else {
                    activityIndicator.startAnimating()
                    LocationTracker.sharedInstance().startLocationTracking()
                }
            }
        }
    }
    func updateForceballyWithPopUpDict(_ popUpDict : NSDictionary){
        var titleForPopUp = String()
        var updateTitleForPopUp = ""
        if let title = popUpDict["title"] as? String{
            titleForPopUp = title
        }
        if let updateTitle = popUpDict["text"] as? String{
            updateTitleForPopUp = updateTitle
        }
        let alert = UIAlertController(title: titleForPopUp, message: updateTitleForPopUp, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK".localized, style: UIAlertActionStyle.default){
            UIAlertAction in
            let openUrl = popUpDict["app_url"] as! String
            UIApplication.shared.openURL(URL(string: openUrl)!)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateWithPopUpDict(_ popUpDict: NSDictionary, loginResponse: FleetInfoDetails){
        var titleForPopUp = String()
        var updateTitleForPopUp = ""
        if let title = popUpDict["title"] as? String{
            titleForPopUp = title
        }
        if let updateTitle = popUpDict["text"] as? String{
            updateTitleForPopUp = updateTitle
        }
        let alert = UIAlertController(title: titleForPopUp, message: updateTitleForPopUp, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK".localized, style: UIAlertActionStyle.default){
            UIAlertAction in
            let openUrl = popUpDict["app_url"] as! String
            UIApplication.shared.openURL(URL(string: openUrl)!)
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.destructive){
            UIAlertAction in
            self.setValuesForNextController(loginResponse)
            DatabaseManager.sharedInstance.saveUserInfoInDatabase(loginResponse, syncStatus: 1, userManagedContext: self.userInfoObjectContext)
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }

    
    func getJsonResponseDictionary(_ notification: [String:Any]?){
        signInButton.isEnabled = true
        signinButton.isEnabled = true
        var loginResponse: FleetInfoDetails!
        activityIndicator.stopAnimating()
        if let statusCode = notification?["status"] as? Int {
            switch statusCode {
            case STATUS_CODES.SHOW_DATA:
                UserDefaults.standard.set(Singleton.sharedInstance.getVersion(), forKey: USER_DEFAULT.appVersion)
                UserDefaults.standard.set(true, forKey: "firstRun")
                if let userData = notification?["data"] as? [String:AnyObject] {
                    if let items = userData["fleet_info"] as? NSArray {
                        for item in items {
                            if let fleetInfo = item as? NSDictionary{
                                loginResponse = FleetInfoDetails(json: fleetInfo)
                            }
                        }
                    }
                    if let notificationCenter = userData["notification_center"] {
                        if let count = notificationCenter["count"] as? Int {
                            NetworkingHelper.sharedInstance.notificationCount = count
                        }
                        
                        if let serverTime = notificationCenter["server_time"] as? String {
                            NetworkingHelper.sharedInstance.serverTime = serverTime
                        }
                        
                        if let data = notificationCenter["data"] as? NSArray {
                            for item in data {
                                NetworkingHelper.sharedInstance.notificationArray.append(NotificationDataObject(json: item as! [String:Any]))
                            }
                        }
                        //NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotificationCount), object: self)
                    }
                    
                    if let popUp = userData["popup"] as? NSDictionary{
                        if popUp.count > 0{
                            if let is_force = popUp["is_force"] as? String{
                                if is_force == "1" {
                                    // Code here for popUp
                                    updateForceballyWithPopUpDict(popUp)
                                    return
                                }else{
                                    updateWithPopUpDict(popUp, loginResponse: loginResponse)
                                    return
                                }
                            }
                            if let is_force = popUp["is_force"] as? Int{
                                if is_force == 1 {
                                    updateForceballyWithPopUpDict(popUp)
                                    return
                                }else{
                                    updateWithPopUpDict(popUp, loginResponse: loginResponse)
                                    return
                                }
                            }
                        }
                    }
                }
                setValuesForNextController(loginResponse)
                DatabaseManager.sharedInstance.saveUserInfoInDatabase(loginResponse, syncStatus: 1, userManagedContext: self.userInfoObjectContext)
                
            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                let alert = UIAlertController(title: "", message: notification?["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                let actionPickup = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                    DispatchQueue.main.async(execute: { () -> Void in
                        Auxillary.logoutFromDevice()
                        NotificationCenter.default.removeObserver(self)
                       // _ = self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
                    })
                })
                alert.addAction(actionPickup)
                self.present(alert, animated: true, completion: nil)
                break
                
            default:
                Singleton.sharedInstance.showErrorMessage(error: (notification?["message"] as? String)!, isError: .error)
            }
        }
    }
    func setValuesForNextController(_ loginResponse: FleetInfoDetails) {
        Singleton.sharedInstance.fleetDetails = loginResponse
        accessToken = loginResponse.accessToken
        UserDefaults.standard.set(loginResponse.accessToken, forKey: "accessToken")
        UserDefaults.standard.set(loginResponse.batteryUsage, forKey: USER_DEFAULT.batteryUsage)
        //if(loginResponse.teams.count > 0) {
            let teams:AssignedTeamDetails = loginResponse.teams!
            UserDefaults.standard.set(teams.teamId, forKey: USER_DEFAULT.teamId)
//        } else {
//            UserDefaults.standard.set(0, forKey: USER_DEFAULT.teamId)
//        }
        
        
        NetworkingHelper.sharedInstance.currentDate = Date().formattedWith("yyyy-MM-dd")
        //Singleton.sharedInstance.gotoHomeStoryboard()
        self.gotoNextController()
    }

    func gotoNextController() {
        LocationTracker.sharedInstance().stopLocationTracking()
        let driverState = DRIVER_STATE(rawValue: Singleton.sharedInstance.fleetDetails.registrationStatus!)
        switch driverState! {
        case DRIVER_STATE.acknowledged:
            Singleton.sharedInstance.gotoHomeStoryboard()
        case DRIVER_STATE.otp_pending:
            self.gotoOTPController()
        default:
            self.gotoVerificationStateController()
            break
        }
        
    }
    
    func gotoOTPController() {
        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.otpController) as! OTPController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoVerificationStateController() {
        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.verificationStateController) as! VerificationStateController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

    
    func locationServiceDisabledAlert(){
        if(locationAlert == nil) {
            locationAlert = UIAlertController(
                title: ERROR_MESSAGE.BACKGROUND_LOCATION_DISABLED,
                message: ERROR_MESSAGE.LOCATION_ACCESS_PATH.localized,
                preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: .cancel, handler: { (action) in
                self.locationAlert = nil
            })
            locationAlert.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: TEXT.OPEN_SETTINGS, style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                    self.locationAlert = nil
                }
            }
            locationAlert.addAction(openAction)
            self.present(locationAlert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Handling Keyboard Toggle
    func keyboardWillShow(notification: NSNotification) {
//        self.view.setNeedsLayout()
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.signInButtomBottomConstraint.constant = keyboardSize.height
        }
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        if SCREEN_SIZE.height == 480 {
            signInLabelTopConstraint.constant = 0
        }
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
            self.view.layoutIfNeeded()
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: false)
    }
    
    func keyboardWillHide(notification: NSNotification) {
//        self.view.setNeedsLayout()
        self.signInButtomBottomConstraint.constant = 0.0
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        if SCREEN_SIZE.height == 480 {
            signInLabelTopConstraint.constant = signInLabelTopConstraintDefaultValue
        }
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
            self.view.layoutIfNeeded()
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == userNameTextField){
            passwordTextField.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            signInAction(self.signInButton)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.userNameTextField {
            if textField.text?.length == 0 {
                if string == " " {
                    return false
                }
            }
        }
        return true
    }

}
