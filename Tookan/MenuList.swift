//
//  MenuList.swift
//  Tookan
//
//  Created by cl-macmini-45 on 21/03/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Crashlytics
import SDKDemo1

protocol MenuListDelegate {
    func dismissMenu()
    func messageToShow(_ message:String)
    func gotoNotificationController()
    func gotoCalendar()
    func settingsAction()
    func tutorialAction()
    func supportAction()
    func createTaskAction()
    func gotoSchedule()
    func gotoProfile()
    func gotoEarningController()
    func gotoFuguChat()
    func gotoWebViewController(url:String, name:String)
    func scanToAssignAction()
}

class MenuList: UIView {

    @IBOutlet var navigationView: UIView!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var logo: UIImageView!
    @IBOutlet var profileButton: UIButton!
    @IBOutlet var menuTable: UITableView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var navigationTopConstraint: NSLayoutConstraint!
    
    var footerHeight:CGFloat = getAspectRatioValue(value: 130.0)
    var margin:CGFloat = getAspectRatioValue(value:26.0)
    var buttonHeight:CGFloat = getAspectRatioValue(value:62.0)
    var onDutyActivation:Int!
    var fleetStatus:Int!
    var countBeforeAppWebToken = 0
    var delegate:MenuListDelegate!
    
    var fuguTitle = Singleton.sharedInstance.fleetDetails.callFuguAs == "" ? TEXT.FUGU_CHAT : Singleton.sharedInstance.fleetDetails.callFuguAs
    var menuListArray = [String]()
    
    override func awakeFromNib() {
        self.backgroundColor = COLOR.MENU_BACKGROUND_COLOR
        self.menuListArray = [TEXT.ON_DUTY, TEXT.NOTIFICATIONS, TEXT.SCHEDULE, TEXT.EARNINGS, self.fuguTitle!, TEXT.SETTINGS, TEXT.TUTORIAL, TEXT.SUPPORT]
        if Singleton.sharedInstance.fleetDetails.app_web_view == 1 {
            self.countBeforeAppWebToken = self.menuListArray.count
            for appWebView in Singleton.sharedInstance.fleetDetails.app_web_view_token! {
                menuListArray.append(appWebView.name!)
            }
        }
        
        
        self.menuTable.delegate = self
        self.menuTable.dataSource = self
        self.menuTable.register(UINib(nibName: NIB_NAME.menuListCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.menuListCell)
        self.menuTable.rowHeight = UITableViewAutomaticDimension

        self.titleLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!
        self.titleLabel.textColor = UIColor.white
        self.titleLabel.text = APP_NAME
        if MENU_LOGO == 1 {
            self.logo.isHidden = false
            self.titleLabel.isHidden = true
        } else {
            self.titleLabel.isHidden = false
            self.logo.isHidden = true
        }
        switch ((Singleton.sharedInstance.fleetDetails.isCreateTaskAvailable == 1),(Singleton.sharedInstance.fleetDetails.isScanToAssignAvailable == 1)) {
        case (true,true):
            self.footerHeight = (footerHeight * 3) / 2
            self.setFooterView()
        case (true,false),(false,true):
            self.setFooterView()
        default:
            break
        }
        
//        if(UserDefaults.standard.integer(forKey: USER_DEFAULT.isCreateTaskAvailable) == 1) && (UserDefaults.standard.integer(forKey: USER_DEFAULT.isScanToAssignAvailable) == 1){
//            self.footerHeight = (footerHeight * 3) / 2
//            self.setFooterView()
//        }
        self.setSwitchButton()
        self.navigationTopConstraint.constant = (SCREEN_SIZE.height == 812.0) ? 20 : 0
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
       delegate.dismissMenu()
    }
    
    @IBAction func profileAction(_ sender: Any) {
        self.delegate.gotoProfile()
    }
    
    func createNewTaskAction() {
        self.delegate.createTaskAction()
    }
    
    //MARK: Switch Button
    func setSwitchButton() {
        var status = ""
        if(Singleton.sharedInstance.fleetDetails.isAvailable == DutyStatus.onDuty) {
            fleetStatus = DutyStatus.onDuty
            self.fleetStatus = DutyStatus.onDuty
            self.onDutyActivation = 0
            if LocationTracker.sharedInstance().sendFirstTimeLocation == true {
                LocationTracker.sharedInstance().restartLocationUpdates()
            }
            status = "ON"
        } else {
            fleetStatus = DutyStatus.offDuty
            self.fleetStatus = DutyStatus.offDuty
            self.onDutyActivation = 1
            LocationTracker.sharedInstance().stopLocationTracking()
            LocationTracker.sharedInstance().sendFirstTimeLocation = true
            status = "OFF"
        }
        Analytics.logEvent(ANALYTICS_KEY.DUTY_TOGGLE, parameters: ["Status":status as NSObject])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.DUTY_TOGGLE, customAttributes: ["Status":status])
        self.menuTable.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
    }
    
    func switchControllerAction(sender:UISwitch) {
        if IJReachability.isConnectedToNetwork() == false {
            self.setSwitchButton()
        } else {
            sender.isEnabled = false
            if UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil{
                var params:[String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
                params["is_available"] = "\(onDutyActivation!)"
//                 let params = [
//                    "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//                    "is_available":"\(onDutyActivation!)"
//                ]
                
                if self.fleetStatus == DutyStatus.offDuty {
                    self.fleetStatus = DutyStatus.onDuty
                } else {
                    self.fleetStatus = DutyStatus.offDuty
                }
                Singleton.sharedInstance.fleetDetails.isAvailable = self.fleetStatus
                UserDefaults.standard.set(self.fleetStatus, forKey: USER_DEFAULT.userAvailableStatus)
                self.setSwitchButton()
                
                NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.changeFleetStatus, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                    DispatchQueue.main.async(execute: { () -> Void in
                        if(isSucceeded == true) {
                            DispatchQueue.main.async(execute: { () -> Void in
                                sender.isEnabled = true
                                switch(response["status"] as! Int) {
                                case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                    if self.fleetStatus == DutyStatus.offDuty {
                                        self.fleetStatus = DutyStatus.onDuty
                                    } else {
                                        self.fleetStatus = DutyStatus.offDuty
                                    }
                                    Singleton.sharedInstance.fleetDetails.isAvailable = self.fleetStatus
                                    UserDefaults.standard.set(self.fleetStatus, forKey: USER_DEFAULT.userAvailableStatus)
                                    self.delegate.messageToShow(response["message"] as? String ?? ERROR_MESSAGE.INVALID_ACCESS_TOKEN)
                                    break
                                default:
                                    break
                                }
                            })
                        } else {
                            DispatchQueue.main.async(execute: { () -> Void in
                                if self.fleetStatus == DutyStatus.offDuty {
                                    self.fleetStatus = DutyStatus.onDuty
                                } else {
                                    self.fleetStatus = DutyStatus.offDuty
                                }
                                Singleton.sharedInstance.fleetDetails.isAvailable = self.fleetStatus
                                UserDefaults.standard.set(self.fleetStatus, forKey: USER_DEFAULT.userAvailableStatus)
                                self.setSwitchButton()
                                sender.isEnabled = true
                                if let message = response["message"] as? String {
                                    Singleton.sharedInstance.showErrorMessage(error: message, isError: .error)
                                } else {
                                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SERVER_NOT_RESPONDING, isError: .error)
                                }
                            })
                        }
                    })
                })
            }
        }
    }
}

extension MenuList:UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuListArray.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 2:
            if Singleton.sharedInstance.availabilityViewStatus == 0 {
                return 0
            }
            break
        case 3:
            if Singleton.sharedInstance.fleetDetails.isEarning == 0 {
                return 0
            }
        case 4:
            if Singleton.sharedInstance.fleetDetails.fugu_chat == 0 {
                return 0
            }
        case 6:
            if SHOW_TUTORIAL == 0 {
                return 0
            }
        
        default:
            break
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.menuListCell) as! MenuListCell
        cell.titleLabel.text = menuListArray[indexPath.row]
        cell.setGradientEffect()
        cell.countLabel.isHidden = true
        cell.switchButton.isHidden = true
        switch indexPath.row {
        case 0:
            cell.switchButton.isHidden = false
            cell.switchButton.addTarget(self, action: #selector(self.switchControllerAction(sender:)), for: .valueChanged)
            switch fleetStatus {
            case DutyStatus.onDuty:
                cell.titleLabel.text = TEXT.ON_DUTY
                cell.switchButton.setOn(true, animated: true)
                break
            case DutyStatus.offDuty:
                cell.titleLabel.text = TEXT.OFF_DUTY
                cell.switchButton.setOn(false, animated: true)
                break
            default:
                break
            }
        case 1:
            cell.countLabel.isHidden = false
            cell.countLabel.text = "\(NetworkingHelper.sharedInstance.notificationCount)"
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            self.delegate.gotoNotificationController()
            break
        case 2:
            self.delegate.gotoSchedule()
        case 3:
            self.delegate.gotoEarningController()
        case 4:
            self.delegate.gotoFuguChat()
        case 5:
            self.delegate.settingsAction()
        case 6:
            self.delegate.tutorialAction()
        case 7:
            self.delegate.supportAction()
        default:
            let appWebToken = Singleton.sharedInstance.fleetDetails.app_web_view_token?[indexPath.row - self.countBeforeAppWebToken]
            self.delegate.gotoWebViewController(url: (appWebToken?.url)!, name: (appWebToken?.name)!)
            break
        }
    }
    
    func setFooterView() {
        /*============== Footer View ====================*/
        let footerView = UIView(frame: CGRect(x: 0, y:0, width: SCREEN_SIZE.width, height: self.footerHeight))
        footerView.backgroundColor = UIColor.black
        var topSpace = getAspectRatioValue(value: 25)
        /*============== Create Task Button ====================*/
        if (Singleton.sharedInstance.fleetDetails.isCreateTaskAvailable == 1){
            let createTaskButton = UIButton(type: .custom)
            createTaskButton.frame = CGRect(x: self.margin, y: topSpace, width: footerView.frame.width - (margin * 2), height: buttonHeight)
            createTaskButton.backgroundColor = COLOR.themeForegroundColor
            createTaskButton.layer.cornerRadius = buttonHeight / 2
            createTaskButton.layer.masksToBounds = true
            createTaskButton.setTitleColor(UIColor.white, for: .normal)
            createTaskButton.setTitle(TEXT.CREATE_A_TASK, for: .normal)
            createTaskButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
            createTaskButton.addTarget(self, action: #selector(self.createNewTaskAction), for: .touchUpInside)
            footerView.addSubview(createTaskButton)
            topSpace = topSpace*2 + self.buttonHeight - 5.0
        }
        
        if (Singleton.sharedInstance.fleetDetails.isScanToAssignAvailable == 1) {
            let scanToAssign = UIButton(frame: CGRect(x: self.margin, y: topSpace, width: footerView.frame.width - (margin * 2), height: buttonHeight))
            scanToAssign.backgroundColor = COLOR.themeForegroundColor
            scanToAssign.layer.cornerRadius = buttonHeight / 2
            scanToAssign.layer.masksToBounds = true
            scanToAssign.setImage(#imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate), for: .normal)
            scanToAssign.tintColor = UIColor.white
            scanToAssign.imageEdgeInsets = UIEdgeInsets(top: getAspectRatioValue(value: 23), left: 0, bottom: getAspectRatioValue(value: 23), right: getAspectRatioValue(value: 10))
            scanToAssign.setTitleColor(UIColor.white, for: .normal)
            scanToAssign.setTitle(TEXT.SCAN_TO_ASSIGN, for: .normal)
            scanToAssign.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
            scanToAssign.addTarget(self, action: #selector(self.scanToAssignAction), for: .touchUpInside)
            footerView.addSubview(scanToAssign)
        }
        
        
        self.menuTable.tableFooterView = footerView
    }
    
    func scanToAssignAction()  {
        self.delegate.scanToAssignAction()
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            return
        }
        let cell  = tableView.cellForRow(at: indexPath) as! MenuListCell
        cell.contentView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.1)
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            return
        }
        let cell  = tableView.cellForRow(at: indexPath) as! MenuListCell
        cell.contentView.backgroundColor = UIColor.clear
    }
}
