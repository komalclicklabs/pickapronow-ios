//
//  InvoiceModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 26/09/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class InvoiceModel: NSObject {
    enum InvoiceResult {
        case success
        case failed
        case resend
        case error(String)
        case ignore
    }
    
    func getInvoiceHTML(completion:@escaping (InvoiceResult) -> ()) {
        let selectedJobId = Singleton.sharedInstance.selectedTaskDetails.jobId!
        var params:[String:Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
        params["job_id"] = "\(selectedJobId)"
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getExtra, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    if let dataDict = response["data"] as? [String:Any] {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            guard self.getPaymentCompleteStatus(dataDict: dataDict) == true else {
                                completion(.resend)
                                return
                            }
                            
                            if let stringHtml = dataDict["invoice_html"] as? String {
                                Singleton.sharedInstance.selectedTaskDetails.invoiceHtml = stringHtml
                                if(Singleton.sharedInstance.selectedTaskDetails.invoiceHtml.length > 0) {
                                    completion(.success)
                                } else {
                                    if(!(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)){
                                        completion(.failed)
                                    } else {
                                        completion(.ignore)
                                    }
                                }
                            } else {
                                if(!(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)){
                                    completion(.failed)
                                }else {
                                    completion(.ignore)
                                }
                            }
                            break
                        default:
                            if(!(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)){
                                completion(.failed)
                            } else {
                                completion(.ignore)
                            }
                            break
                        }
                    }
                } else {
                    completion(.error(response["message"] as! String))
                }
            }
        }
    }
    
    func getPaymentCompleteStatus(dataDict:[String:Any]) -> Bool {
        var paymentComplete = 1
        if let value = dataDict["paymentComplete"] as? String {
            if let intValue = Int(value)  {
                paymentComplete = intValue
            }
        } else if let value = dataDict["paymentComplete"] as? NSNumber {
            paymentComplete = Int(value)
        }
        guard paymentComplete == 1 else {
            return false
        }
        return true
    }
    
}

