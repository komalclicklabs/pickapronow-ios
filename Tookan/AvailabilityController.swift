//
//  AvailabilityController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 13/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class AvailabilityController: UIViewController, AvailabilityDelegate {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var availableColorView: UIView!
    @IBOutlet var unavailableColorView: UIView!
    @IBOutlet var busyColorView: UIView!
    @IBOutlet var availableLabel: UILabel!
    @IBOutlet var unavailableLabel: UILabel!
    @IBOutlet var busyLabel: UILabel!
    @IBOutlet var availableView: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var model = AvailabilityModel()
    var availabilityView:AvailabilityView!
    var editScheduleController:EditScheduleController!
    var availableColorViewHeight:CGFloat = 7
    var reloadView:ReloadView!
    var greenColor = UIColor(red: 99/255, green: 174/255, blue: 12/255, alpha: 1.0)
    var redColor = UIColor(red: 174/255, green: 12/255, blue: 12/255, alpha: 1.0)
    let addButtonHeight:CGFloat = 60.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.startAnimating()
        self.automaticallyAdjustsScrollViewInsets = false
       // self.setNavigationBar()
        self.setHeaderView()
         self.getAvailabilityDataFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.bringSubview(toFront: self.activityIndicator)
       
        self.perform(#selector(self.updateStatusBar), with: nil, afterDelay: 1.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.setAvailabilityView()
    }
    
    func updateStatusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                UIApplication.shared.statusBarStyle = .lightContent
            } else {
                UIApplication.shared.statusBarStyle = .default
            }
        }
    }

//    //MARK: Navigation Bar
//    func setNavigationBar() {
//        navigation = UINib(nibName: "navigationBar", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
//        navigation.backgroundColor = UIColor(red: 9/255, green: 41/255, blue: 58/255, alpha: 1.0)
//        navigation.delegate = self
//        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
//        navigation.backButton.tintColor = UIColor.white
//        navigation.titleLabel.text = TEXT.SCHEDULE
//        navigation.titleLabel.setLetterSpacing(value: 1.8)
//        navigation.titleLabel.textColor = UIColor.white//COLOR.TEXT_COLOR
//        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
//        navigation.bottomLine.isHidden = false
//        navigation.editButton.isHidden = false
//        navigation.editButton.setImage(#imageLiteral(resourceName: "reload").withRenderingMode(.alwaysTemplate), for: UIControlState())
//        navigation.editButton.tintColor = UIColor.white//COLOR.TEXT_COLOR
//        navigation.editButton.addTarget(self, action: #selector(self.getAvailabilityDataFromServer), for: UIControlEvents.touchUpInside)
//        navigation.editButton.setTitle("", for: UIControlState())
//        navigation.editButtonTrailingConstraint.constant = 15.0
//        self.view.addSubview(navigation)
//    }
//
//    func backAction() {
//        Singleton.sharedInstance.availabilityData = [AvailabilityDay]()
//        _ = self.navigationController?.popViewController(animated: true)
//    }
    
    //MARK: HEADER VIEW
    func setHeaderView() {
        /*------------- Color ------------*/
        headerView.backgroundColor = COLOR.availabilityHeaderColor
        availableColorView.backgroundColor = COLOR.availabilityFreeColor
        unavailableColorView.backgroundColor = COLOR.availabilityUnavailableColor
        busyColorView.backgroundColor = COLOR.availabilityBusyColor
        /*----------- Text -------------*/
        availableLabel.text = TEXT.AVAILABLE
        unavailableLabel.text = TEXT.UNAVAILABLE
        busyLabel.text = TEXT.BUSY
     //   headerTitle.text = model.getMonthTitle(maximumDays: Singleton.sharedInstance.fleetDetails.availability_days_limit!)
        /*----------- Corner radius ------------*/
        availableColorView.layer.cornerRadius = availableColorViewHeight / 2
        unavailableColorView.layer.cornerRadius = availableColorViewHeight / 2
        busyColorView.layer.cornerRadius = availableColorViewHeight / 2
    }
    
    //MARK: AVAILABILITY VIEW
    func setAvailabilityView(maximumDays:Int) {
        availabilityView = UINib(nibName: NIB_NAME.availabilityView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! AvailabilityView
        availabilityView.delegate = self
        availabilityView.maximumDays = maximumDays
        availabilityView.frame = CGRect(x: 0, y: 0, width: self.availableView.frame.width, height: self.availableView.frame.height)
        self.availableView.addSubview(self.availabilityView)
        availabilityView.setDayCollectionView()
        self.availabilityView.setTableView()
        if self.reloadView != nil {
            self.availabilityView.isHidden = true
        }
        self.view.sendSubview(toBack: availableView)
        
    }
    
    //MARK: AVAILABILITY DELEGATE METHODS:
    func backAction() {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    func selectedDay(selectedDate: String, availabilityStatus:Bool,weekday:Int) {
        guard editScheduleController == nil else {
            return
        }
        editScheduleController = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.editScheduleController) as! EditScheduleController
        editScheduleController.selectedDate = selectedDate
        editScheduleController.availabilityStatus = availabilityStatus
        editScheduleController.weekday = weekday
//        if self.topTransparentView.isHidden == false {
//            self.hideEditScheduleView()
//            self.perform(#selector(self.gotoEditScheduleController), with: nil, afterDelay: 0.5)
//        } else {
            self.gotoEditScheduleController()
//        }
    }
    
    func gotoEditScheduleController() {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.CALENDAR_EDIT_SCHEDULE, parameters: [:])
        editScheduleController.callback = { message in
            self.editScheduleController = nil
            self.getAvailabilityDataFromServer()
        }
        self.navigationController?.pushViewController(editScheduleController, animated: true)
    }
    
    //MARK: SHOW RELOAD VIEW
    func showReloadView() {
        if self.availabilityView != nil {
            self.availabilityView.isHidden = true
        }
        if reloadView == nil {
            reloadView = UINib(nibName: NIB_NAME.reloadView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ReloadView
        }
        reloadView.frame = self.availableView.frame
        self.view.addSubview(reloadView)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.getAvailabilityDataFromServer))
        tap.numberOfTapsRequired = 1
        self.reloadView.addGestureRecognizer(tap)
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Singleton.sharedInstance.availabilityData = [AvailabilityDay]()
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getAvailabilityDataFromServer() {
       // self.navigation.editButton.isHidden = true
      //  self.navigation.activityIndicator.startAnimating()
        if self.reloadView != nil {
            self.reloadView.removeFromSuperview()
            self.reloadView = nil
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.activityIndicator.startAnimating()
        let currentDate = Auxillary.getLocalDateString()
        var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["local_date_time"] = currentDate
        params["blank_array_check"] = 1
//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//            "local_date_time":currentDate,
//            "blank_array_check":1
//            ] as [String : Any]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getWeekAvailability, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
              //  self.navigation.editButton.isHidden = false
              //  self.navigation.activityIndicator.stopAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                print(response)
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    case STATUS_CODES.SHOW_DATA:
                        if let data = response["data"] as? [String:Any] {
                            Singleton.sharedInstance.availabilityData = [AvailabilityDay]()
                            if let days = data["days"] as? [Any] {
                                for day in days {
                                    if let slot = AvailabilityDay(json: day as! [String:Any]) as AvailabilityDay! {
                                        Singleton.sharedInstance.availabilityData.append(slot)
                                    }
                                }
                            }
                            if self.availabilityView != nil {
                                if let limit = data["availability_days_limit"] as? NSNumber {
                                    self.availabilityView.maximumDays = Int(limit)
                                } else if let limit = data["availability_days_limit"] as? String {
                                    self.availabilityView.maximumDays = Int(limit)!
                                }
                                Singleton.sharedInstance.fleetDetails.availability_days_limit = self.availabilityView.maximumDays
                                self.availabilityView.isHidden = false
                                self.availabilityView.setDayCollectionView()
                                //self.headerTitle.text = self.model.getMonthTitle(maximumDays: self.availabilityView.maximumDays)
                                self.availabilityView.availabilityCollectionView.reloadData()
                                self.availabilityView.dayCollectionView.reloadData()
                                self.availabilityView.timeCollectionView.reloadData()
                            } else {
                                if let limit = data["availability_days_limit"] as? NSNumber {
                                    self.setAvailabilityView(maximumDays: Int(limit))
                                } else if let limit = data["availability_days_limit"] as? String {
                                    self.setAvailabilityView(maximumDays: Int(limit)!)
                                }
                            }

                            //                            }
                            
                            if let status = data["fleet_calendar__edit_status"] as? Int {
                                Singleton.sharedInstance.availabilityEditStatus = status
                            }
                            //self.setAddButton()
                        }
                        break
                        
                    case STATUS_CODES.UNAUTHORIZED_FOR_AVAILABILITY:
                        if let data = response["data"] as? [String:Any] {
                            if let message = response["message"] as? String {
                                self.showPopupForInvalidAccessForAvailability(message: message, data: data)
                            }
                        }
                        break
                    default:
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    self.showReloadView()
                }
            }
        }
    }
    
//    func setAddButton() {
//        if Singleton.sharedInstance.availabilityEditStatus == 1 {
//            addButton.isHidden = false
//            addButton.backgroundColor = greenColor
//            addButton.layer.cornerRadius = addButtonHeight / 2
//            //self.selectDateTextLabel.text = "Select date to set your availability".localized
//        } else {
//            addButton.isHidden = true
//        }
//    }
    
//    @IBAction func addAction(_ sender: AnyObject) {
//        guard availabilityView != nil && availabilityView.isHidden == false  else {
//            return
//        }
//        addButton.isSelected = !addButton.isSelected
//        if addButton.isSelected == true {
//            FIRAnalytics.logEvent(withName: ANALYTICS_KEY.SCHEDULE_ADD_BUTTON, parameters: [:])
//            self.showEditScheduleView()
//        } else {
//             FIRAnalytics.logEvent(withName: ANALYTICS_KEY.SCHEDULE_DELETE_BUTTON, parameters: [:])
//            self.hideEditScheduleView()
//        }
//    }
    
//    func showEditScheduleView() {
//        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//            //self.addButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
//            }, completion: { finished in
//                self.startArrowAnimation()
//        })
//        
//        UIView.animate(withDuration: 0.3, animations: {
//           // self.topTransparentView.isHidden = false
//          //  self.bottomTransparentView.isHidden = false
//          //  self.topTransparentView.alpha = 1.0
//           // self.bottomTransparentView.alpha = 1.0
//           // self.addButton.backgroundColor = self.redColor
//            self.availabilityView.arrow.isHidden = false
//            self.availabilityView.arrow.alpha = 1.0
//            }, completion: { finished in
//        })
//    }
//    
//    func hideEditScheduleView() {
//        self.stopArrowAnimation()
//       // addButton.isSelected = false
//        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//            //self.addButton.transform = CGAffineTransform.identity
//            }, completion: { finished in
//        })
//        UIView.animate(withDuration: 0.3, animations: {
//         //   self.topTransparentView.alpha = 0
//         //   self.bottomTransparentView.alpha = 0
//            self.availabilityView.arrow.alpha = 0
//          //  self.addButton.backgroundColor = self.greenColor
//            }, completion: { finished in
//                self.availabilityView.arrow.isHidden = true
//             //   self.topTransparentView.isHidden = true
//             //   self.bottomTransparentView.isHidden = true
//        })
//    }
    
//    func startArrowAnimation() {
//        UIView.animate(withDuration: 0.8, delay: 0,
//                       options: [.repeat, .autoreverse, .curveEaseOut],
//                       animations: {
//                        self.availabilityView.arrow.transform = CGAffineTransform(translationX: 22, y: 0)
//            }, completion: nil)
//    }
//    
//    func stopArrowAnimation() {
//        self.availabilityView.arrow.transform = CGAffineTransform.identity
//        self.availabilityView.arrow.layer.removeAllAnimations()
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showPopupForInvalidAccessForAvailability(message:String, data:[String:Any]) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let status = data["fleet_calendar__view_status"] as? Int {
                    if status == 0 {
                        Singleton.sharedInstance.availabilityViewStatus = 0
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
}
