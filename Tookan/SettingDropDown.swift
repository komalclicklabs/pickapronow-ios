//
//  SettingDropDown.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 3/22/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol SettingDropDownDelegate
{
     func changeSettingMenuOption(selectedItem: String, selectedoption: SELECTED_MENU_FOR_DROP_DOWN)
}

class SettingDropDown: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var settingText: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lineBelowSearch: UIView!
    @IBOutlet weak var topRoundView: UIView!
    @IBOutlet var panExit: UIPanGestureRecognizer!
    @IBOutlet var closeButton: UIButton!
    
    var viewCornerRadius:CGFloat = 15.0
    var viewLeadingConstraintMaxValue = getAspectRatioValue(value: 475)
    var viewLeadingConstraintMinValue = getAspectRatioValue(value: 175)
    var viewLeadingconstraintMidValue = getAspectRatioValue(value: 300)
    let rowHeight:CGFloat = 50.0
    var isTrackingPanLocation = false
    var tableData : [String] = []
    var searchResult : [String] = []
    var defaultTableData : [String] = []
    var delegate : SettingDropDownDelegate!
    var setSelectedOption : SELECTED_MENU_FOR_DROP_DOWN?
    var isCellIconVisiable : Bool!
    var selectedItem = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lineBelowSearch.backgroundColor = COLOR.LINE_COLOR
        
        
        self.setTableView()
        self.setplaceHolder(placeHolder: "Title")
        
        /*============== Round Corners ===================*/
        let path = UIBezierPath(roundedRect:CGRect(x: self.topRoundView.bounds.origin.x, y: self.topRoundView.bounds.origin.y, width: SCREEN_SIZE.width - 10, height: self.topRoundView.frame.height), byRoundingCorners:[.topLeft, .topRight], cornerRadii: CGSize(width: viewCornerRadius, height: viewCornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds;
        maskLayer.path = path.cgPath
        self.topRoundView.layer.mask = maskLayer;
        
        self.panExit.delegate = self
        self.settingText.delegate = self
        self.settingText.clearButtonMode = UITextFieldViewMode.whileEditing;
        self.settingText?.isEnabled = false
       
        /*============= Close Button =================*/
        self.closeButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        self.closeButton.tintColor = COLOR.TEXT_COLOR
        
        /*============== Tap gesture ===================*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.updateConstraintBeforeDismiss))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        settingText.addTarget(self, action: #selector(SettingDropDown.textFieldTextChanged), for: UIControlEvents.editingChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        defaultTableData = tableData
        self.setConstraintForView()
    }
    
    override func loadView() {
        Bundle.main.loadNibNamed(NIB_NAME.settingDropDown, owner: self, options: nil)
        self.view.frame = UIScreen.main.bounds
    }
    
    func setTableView() {
        self.tableView.register(UINib(nibName: NIB_NAME.dropDownCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.dropDownCell)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        //self.tableView.estimatedRowHeight = 160
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsSelection = true
        
    }
    
    func setConstraintForView() {
        if(tableData.count>7){
            topConstraint.constant = CGFloat(viewLeadingConstraintMinValue)
        }else if(tableData.count<3){
            topConstraint.constant = CGFloat(viewLeadingConstraintMaxValue)
        }else{
            topConstraint.constant = SCREEN_SIZE.height - (CGFloat(tableData.count) * rowHeight) - getAspectRatioValue(value: rowHeight)
        }
        self.view.setNeedsUpdateConstraints()
        
        
        UIView.animate(withDuration: 1.0, delay:0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func updateConstraintBeforeDismiss() {
        topConstraint.constant = SCREEN_SIZE.height - 50
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }, completion: { finished in
            self.dismiss(animated: false, completion: nil)
        })
    }

    @IBAction func closeAction(_ sender: Any) {
        self.updateConstraintBeforeDismiss()
    }
    
    // MARK: PAN Action
    @IBAction func endViewOnPan(_ sender: Any) {
        let pan = sender as! UIPanGestureRecognizer
        let translation  = pan.translation(in: pan.view?.superview)
        var theTransform = pan.view?.transform
        if pan.state == UIGestureRecognizerState.changed{
            if translation.y > 0 {
                theTransform?.ty = translation.y
                topRoundView.transform = theTransform!
                tableView.transform = theTransform!
            }
        }else if pan.state == UIGestureRecognizerState.ended{
            if translation.y > 50 {
                updateConstraintBeforeDismiss()
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.topRoundView.transform = CGAffineTransform.identity
                    self.tableView.transform = CGAffineTransform.identity
                })
            }
        }
    }
    
//    @IBAction func searchAction(_ sender: UIButton) {
//        self.settingText.becomeFirstResponder()
//        self.settingText.placeholder = TEXT.TYPE_YOUR_SEARCH_HERE
//    }
    
    //MARK: TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        settingText.resignFirstResponder()
        return false
    }
    
    func textFieldTextChanged(sender : AnyObject) {
        updateSearchResult()
    }
    
    //MARK: To search from TableView
    func updateSearchResult() {
        let searchText = self.settingText?.text
        if searchText == "" {
            tableData = defaultTableData
            tableView.reloadData()
        } else {
            searchResult = defaultTableData.filter { item in
                return item.lowercased().contains((searchText?.lowercased())!)
            }
            tableData = searchResult
            tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Mark TableView Delegates
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.dropDownCell) as! DropDownCell
        cell.textLabel?.textColor = COLOR.TEXT_COLOR
        if(isCellIconVisiable == true){
            cell.imageView?.isHidden = false
            cell.imageView?.image = #imageLiteral(resourceName: "rightArrowOption").withRenderingMode(.alwaysTemplate)
            if(self.selectedItem == indexPath.row){
                cell.imageView?.tintColor = COLOR.themeForegroundColor
                cell.textLabel?.textColor = COLOR.themeForegroundColor
            }else {
                cell.imageView?.tintColor = COLOR.TEXT_COLOR
                cell.textLabel?.textColor = COLOR.TEXT_COLOR
            }
        }else{
            cell.imageView?.image = nil
        }
        cell.textLabel?.text = tableData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedText = tableData[indexPath.row]
        self.delegate?.changeSettingMenuOption(selectedItem: selectedText,selectedoption: setSelectedOption!)
        self.updateConstraintBeforeDismiss()
    }
    
    
    //MARK: Set values for DropDown from SettingViewController
    func updateDropDown(_ data : [String], option:SELECTED_MENU_FOR_DROP_DOWN, cellIconenabled : Bool, headingText : String,selectedItem:Int) {
        tableData = data
        self.setSelectedOption = option
        self.isCellIconVisiable = cellIconenabled
        self.setplaceHolder(placeHolder: headingText)
        self.selectedItem = selectedItem
        self.tableView.reloadData()
        self.setConstraintForView()
    }
    
    //MARK: Set Placeholder Text
    func setplaceHolder(placeHolder : String){
        let attributes = [
            NSForegroundColorAttributeName: COLOR.TEXT_COLOR,
            NSFontAttributeName :  UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
        ]
        self.settingText.attributedPlaceholder = NSAttributedString(string: placeHolder, attributes: attributes )
    }
    
}

//MARK: UIGestureRecognizerDelegate method
extension SettingDropDown:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view != nil && touch.view!.isDescendant(of: self.tableView) {
            return false
        }
        return true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer : UIGestureRecognizer)->Bool {
        return true
    }
}

