//
//  ImagePreviewCell.swift
//  
//
//  Created by cl-macmini-45 on 08/11/16.
//
//

import UIKit

class ImagePreviewCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var imageScrollView: UIScrollView!
    @IBOutlet var captionTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 4.0
        self.imageView.layer.cornerRadius = 4.0
        self.imageView.layer.borderWidth = 1.0
        self.imageView.layer.borderColor = UIColor().lineColor.cgColor
        self.captionTextView.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
    }
    
    

}
