//
//  KeyboardToolbar.swift
//  Butlers
//
//  Created by Rakesh Kumar on 10/6/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit


protocol KeyboardDelegate
{
    func doneFromKeyboard()
    func nextFromKeyboard(_ nextField: AnyObject)
    func prevFromKeyboard(_ prevField: AnyObject)
}

class KeyboardToolbar: UIToolbar {
    
    var keyboardDelegate:KeyboardDelegate!
    var previousTextField:AnyObject!
    var nextTextField:AnyObject!
    var currentTextField:AnyObject!
    
    func addButtons() {
        self.backgroundColor = UIColor.groupTableViewBackground
        self.sizeToFit()
        let previousButton = UIBarButtonItem(title: " < ", style: UIBarButtonItemStyle.plain, target: nil, action: #selector(KeyboardToolbar.prevAction))//UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Rewind, target: nil, action: nil)
        previousButton.setTitleTextAttributes([NSFontAttributeName: UIFont(name: UIFont().MontserratMedium, size: 20)!], for: UIControlState())
        let nextButton = UIBarButtonItem(title: " > ", style: UIBarButtonItemStyle.plain, target: nil, action: #selector(KeyboardToolbar.nextAction))//UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FastForward, target: nil, action: nil)
        nextButton.setTitleTextAttributes([NSFontAttributeName: UIFont(name: UIFont().MontserratMedium, size: 20)!], for: UIControlState())
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(KeyboardToolbar.doneAction))
        self.items = [previousButton, nextButton, flexBarButton, doneBarButton]
    }
    
    func doneAction() {
        keyboardDelegate.doneFromKeyboard()
    }
    
    func nextAction(){
        keyboardDelegate.nextFromKeyboard(currentTextField)
    }
    
    func prevAction(){
        keyboardDelegate.prevFromKeyboard(currentTextField)
    }

}
