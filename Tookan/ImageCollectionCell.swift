//
//  imageCollectionCell.swift
//  Tookan
//
//  Created by Rakesh Kumar on 1/4/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    @IBOutlet var transLayer: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var removeButton: UIButton!
//    @IBOutlet weak var progressViewForImage: UIProgressView!
    @IBOutlet weak var progressViewForImage: UIProgressView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var countLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 4.0
        self.imageView.layer.cornerRadius = 4.0
        self.imageView.layer.borderWidth = 1.0
        self.imageView.layer.borderColor = UIColor().lineColor.cgColor
        self.removeButton.layer.cornerRadius = 12.0
        self.removeButton.layer.masksToBounds = true
        
    }

}
