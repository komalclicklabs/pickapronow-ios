//
//  ForgotPasswordViewController.swift
//  Tookan
//
//  Created by Click Labs on 7/8/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit
import Crashlytics
import FirebaseAnalytics

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var forgotPasswordLabel: UILabel!
    @IBOutlet weak var emailID: UITextField!
    @IBOutlet weak var linkTextLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var backButton: UIButton!
    var userType = 0
    @IBOutlet var line: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.FORGOT_PASSWORD, parameters: [:])
        
        /*----------- Background Image/Color ----------*/
        self.view.backgroundColor = COLOR.themeBackgroundColor
        self.line.backgroundColor = COLOR.LINE_COLOR

        /*--------- Forgot Password ----------*/
        forgotPasswordLabel.text = TEXT.FORGOT_PASSWORD
        forgotPasswordLabel.textColor = COLOR.LIGHT_COLOR
        forgotPasswordLabel.font = UIFont(name: UIFont().MontserratExtraLight, size: FONT_SIZE.extra_large)
        
        /*================= Link Text Label ==============*/
        linkTextLabel.text = TEXT.FORGOT_PASSWORD_LINK
        linkTextLabel.textColor = COLOR.TEXT_COLOR
        linkTextLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)
        
        /*================= Back Button ===============*/
        self.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        self.backButton.tintColor = COLOR.TEXT_COLOR
        
        /*--------- Submit button -----------*/
        self.submitButton.setTitle(TEXT.SUBMIT, for: .normal)
        self.submitButton.backgroundColor = COLOR.themeForegroundColor
        self.submitButton.setTitleColor(COLOR.positiveButtonTitleColor, for: UIControlState())
        self.submitButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        
        /*------------- User email -------------*/
        let attributes = [
            NSForegroundColorAttributeName: COLOR.TEXT_COLOR,
            NSFontAttributeName :  UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        ]
        self.emailID.attributedPlaceholder = NSAttributedString(string: "\(TEXT.EMAIL)...", attributes: attributes )
        self.emailID.layer.cornerRadius = 4.0
        self.emailID.textColor = COLOR.TEXT_COLOR
        self.emailID.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.emailID.becomeFirstResponder()
        self.emailID.tintColor = COLOR.themeForegroundColor
        
        /*------------- Background Touch -----------*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(ForgotPasswordViewController.backgroundTouch))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
    }
    
    override var preferredStatusBarStyle:UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        UIApplication.shared.statusBarStyle = .lightContent
    }

    func backgroundTouch() {
        self.view.endEditing(true)
    }
    
    @IBAction func resetPassword(_ sender: UIButton) {
        guard (emailID.text?.length)! > 0 && Auxillary.validateEmail(emailID.text!) == true else {
            Singleton.sharedInstance.showErrorMessage(error: "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.EMAIL)", isError: .error)
            return
        }
        
        activityIndicator.startAnimating()
        
        var apiName:String!
        if(userType == USER_TYPE.agent) {
            apiName = API_NAME.forgot_password
            Answers.logCustomEvent(withName: "Forgot Password", customAttributes: ["Key":"Agent"])
        } else {
            Answers.logCustomEvent(withName: "Forgot Password", customAttributes: ["Key":"Admin"])
            apiName = API_NAME.forgot_password_admin
        }
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: apiName, params: ["email": (emailID.text?.trimText)! as AnyObject], httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
            DispatchQueue.main.async {
                if(isSucceeded == true) {
                    self.passwordResetStatus(response)
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    self.activityIndicator.stopAnimating()
                }
            }
        })
        
        emailID.resignFirstResponder()
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func passwordResetStatus(_ notification: [String:Any]?){
        activityIndicator.stopAnimating()
        if let status = notification?["status"] as? Int {
            switch status {
            case STATUS_CODES.SHOW_DATA:
                /*------------- Firebase ---------------*/
                Analytics.logEvent(ANALYTICS_KEY.FORGOT_PASSWORD_SUBMIT, parameters: [:])
                Singleton.sharedInstance.showErrorMessage(error: TEXT.RESET_LINK, isError: .success)
                break
            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                let alert = UIAlertController(title: "", message: notification?["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                let actionPickup = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                    DispatchQueue.main.async(execute: { () -> Void in
                        Auxillary.logoutFromDevice()
                        NotificationCenter.default.removeObserver(self)
                       // _ = self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
                    })
                })
                alert.addAction(actionPickup)
                self.present(alert, animated: true, completion: nil)
                break
            default:
                Singleton.sharedInstance.showErrorMessage(error: notification?["message"] as! String, isError: .error)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resetPassword(UIButton())
        return true
    }
    
    //MARK: Handling Keyboard Toggle
    func keyboardWillShow(notification: NSNotification) {
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.submitButton.transform = CGAffineTransform(translationX: 0, y: -keyboardSize.height)
            }

        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: false)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
           self.submitButton.transform = CGAffineTransform.identity
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }
}
