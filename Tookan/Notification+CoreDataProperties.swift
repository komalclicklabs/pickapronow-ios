//
//  Notification+CoreDataProperties.swift
//  
//
//  Created by Rakesh Kumar on 1/14/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Notification {

    @NSManaged var end_time: String?
    @NSManaged var start_time: String?
    @NSManaged var start_address: String?
    @NSManaged var job_id: NSNumber?
    @NSManaged var end_address: String?
    @NSManaged var d: String?
    @NSManaged var cust_name: String?
    @NSManaged var message: String?
    @NSManaged var accept_button: NSNumber?
    @NSManaged var flag: NSNumber?
    @NSManaged var job_type: NSNumber?
    @NSManaged var timer: String?

}
