//
//  TutorialController.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 4/7/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

enum TUTORIAL_START_POINT {
    case ALL_CLEAR
    case NEW_TASK
    case ONLY_ASSIGNED
    case ONLY_NEW_TASK
    case AFTER_ACCEPT
    case FULL_TUTORIAL
}

class TutorialController: UIViewController{

    @IBOutlet weak var newButton: UIButton!
    @IBOutlet weak var downArrow: UIImageView!
    @IBOutlet weak var leftArrow: UIImageView!
    @IBOutlet weak var centerArrow: UIImageView!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var labelLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageNumber: UIPageControl!
    @IBOutlet weak var tutorialText: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var centerArrowVerticalConstraint: NSLayoutConstraint!
    
    
    
    var navigation:navigationBar!
    var pos = 0
    var runningJobsView:JobListTable!
    let leftRightMargin:CGFloat = 5.0
    let runningJobOrigin:CGFloat = getAspectRatioValue(value: 78.0)//78.0
    var taskObject = TasksAssignedToDate(json:[:])
    var pageChangeLimit = 6
    var tutorialStartPoint:TUTORIAL_START_POINT!
    var navigationYPos:CGFloat = 0.0
    var navigationHeight = HEIGHT.navigationHeight
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.labelTopConstraint.constant = self.labelTopConstraint.constant + self.navigationYPos
        self.setNavigationBar()
        
        /*=============== Setting Tutorial Text=========================*/
        self.tutorialText.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.tutorial)
        self.tutorialText.textColor = COLOR.ACCEPT_TITLE_COLOR
        self.tutorialText.text = TEXT.TUTORIAL_1;
        self.tutorialText.alpha = 0.0
        self.view.bringSubview(toFront: self.tutorialText)
        
        /*=============== Setting Swipe Gesture=========================*/
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeleft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(gesture:)))
        swipeleft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeleft)
        
        self.rightArrow.alpha = 0.0
        self.centerArrow.alpha = 0.0
        self.leftArrow.alpha = 0.0
        self.downArrow.alpha = 0.0
        self.downArrow.image = #imageLiteral(resourceName: "downArrow").withRenderingMode(.alwaysTemplate)
        self.view.bringSubview(toFront: self.downArrow)
        
        if NetworkingHelper.sharedInstance.dayTasksListArray.count > 0 {
            self.taskObject = NetworkingHelper.sharedInstance.dayTasksListArray[0][0]
        } else {
            self.taskObject.jobPickupAddress = "3126, Mountains, US"
            self.taskObject.jobPickupDateTimeInTwelveHoursFormat = "12:00"
            self.taskObject.deliveryJobId = 123
            self.taskObject.jobStatus = 1
            self.taskObject.jobType = 0
            self.taskObject.jobDeliveryDatetime = "12:00"
            self.taskObject.customerUsername = Singleton.sharedInstance.fleetDetails.name!
            self.taskObject.jobPickupName = Singleton.sharedInstance.fleetDetails.name!
        }

        self.setRunningJobListView()
        self.setNewTaskButton()
        self.newButton.setTitle("1 \(TEXT.NEW) \(TEXT.ORDER)"/* + " " + TEXT.NEW + " " + TEXT.ORDER*/, for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        /*=============== Setting Skip Button=========================*/
        self.skipButton.backgroundColor = UIColor.clear
        self.skipButton.setTitle(TEXT.SKIP, for: .normal)
        self.skipButton.setTitleColor(COLOR.ACCEPT_TITLE_COLOR, for: .normal)
        self.view.bringSubview(toFront: self.skipButton)
        
        /*=============== Setting Next Button=========================*/
        self.nextButton.backgroundColor = UIColor.clear
        self.nextButton.setTitle(TEXT.NEXT, for: .normal)
        self.nextButton.setTitleColor(COLOR.ACCEPT_TITLE_COLOR, for: .normal)
        self.view.bringSubview(toFront: self.nextButton)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                UIApplication.shared.statusBarStyle = .lightContent
            } else {
                UIApplication.shared.statusBarStyle = .default
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func skipAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func pageChangeAction(_ sender: Any) {
//        let position = pageNumber.currentPage
//        let dir  : CGFloat = (position < pageNumber.currentPage) ? -1.0 : 1.0
//        self.setNextTutorial(nextPage: position + 1, animationDir: dir)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
    }
    
    @IBAction func nextPageAction(_ sender: Any) {
        let position = pageNumber.currentPage
        if(position < self.pageChangeLimit){
            self.setNextTutorial(nextPage: position + 1, animationDir: -1.0)
            self.pageNumber.currentPage = position + 1
        } else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        let position = pageNumber.currentPage
        if(position < self.pageChangeLimit){
            self.setNextTutorial(nextPage: position + 1, animationDir: -1.0)
            self.pageNumber.currentPage = position + 1
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    //MARK: Navigation Bar
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.frame = CGRect(x: 0, y: self.navigationYPos, width: self.view.frame.width, height: self.navigationHeight)
        self.setAttributedNavigationTitle()
        navigation.titleLabel.isUserInteractionEnabled = true
        navigation.backButton.isHidden = true
        navigation.hamburgerButton.isHidden = false
        navigation.editButton.isHidden = false
        navigation.editButton.setTitle("", for: .normal)
        navigation.editButtonTrailingConstraint.constant = 15.0
        navigation.hamburgerButton.isUserInteractionEnabled = false
        navigation.titleLabel.isUserInteractionEnabled = false
        navigation.editButton.isUserInteractionEnabled = false
        navigation.editButton.setImage(#imageLiteral(resourceName: "combinedShape"), for: .normal)
        self.view.addSubview(navigation)
        
        self.navigation.editButton.alpha = 0.0
        self.navigation.hamburgerButton.alpha = 0.0
        self.navigation.titleLabel.alpha = 0.0

    }
    
    func setAttributedNavigationTitle() {
        
        var currentDate = NetworkingHelper.sharedInstance.currentDate.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd", output: "MMMM d")
        if(currentDate == "") {
            currentDate = Date().formattedWith("MMMM d")
        }
        
        let attributedString = NSMutableAttributedString(string: "\(currentDate)  ", attributes: [NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!, NSForegroundColorAttributeName:UIColor.white])
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = #imageLiteral(resourceName: "arrowDate")
        image1Attachment.bounds = CGRect(x: 0, y: 0, width: 13, height: 8)
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        attributedString.append(image1String)
        navigation.titleLabel.attributedText = attributedString
        
    }
    
    func performBeforeTutorial(){
        self.rightArrow.alpha = 0.0
        self.centerArrow.alpha = 0.0
        self.leftArrow.alpha = 0.0
        self.downArrow.alpha = 0.0
        self.navigation.editButton.alpha = 0.0
        self.navigation.titleLabel.alpha = 0.0
        self.newButton.alpha = 0.0
        //self.newButton.transform = CGAffineTransform(translationX: 0, y: self.newButton.frame.height)
        self.navigation.hamburgerButton.alpha = 0.0
        self.runningJobsView.slideUpIcon.alpha = 0.0
        self.runningJobsView.taskListTable.alpha = 0.0
        self.runningJobsView.backgroundView.alpha = 0.0
        self.runningJobsView.alpha = 0.0
        self.tutorialText.textColor = COLOR.ACCEPT_TITLE_COLOR
        self.leftArrow.layer.removeAllAnimations()
        self.centerArrow.layer.removeAllAnimations()
        self.rightArrow.layer.removeAllAnimations()
        self.downArrow.layer.removeAllAnimations()
        self.leftArrow.layer.transform = CATransform3DIdentity
        self.leftArrow.layer.transform = CATransform3DIdentity
        self.centerArrow.layer.transform = CATransform3DIdentity
        self.rightArrow.layer.transform = CATransform3DIdentity
    }
    
    func menuTutorial(animationDir : CGFloat){
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
              self.performBeforeTutorial()
            
        }, completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                self.tutorialText.transform = CGAffineTransform(translationX: (animationDir * SCREEN_SIZE.width), y: 0)
            }, completion: { (finished:Bool) in
                self.tutorialText.alpha = 1.0
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                    self.tutorialText.transform = CGAffineTransform.identity
                    self.leftArrow.alpha = 1.0
                    self.labelTopConstraint.constant = 72.0 + self.navigationYPos
                    self.labelLeftConstraint.constant = 85.0
                    self.labelRightConstraint.constant = 85.0
                    self.tutorialText.text = TEXT.TUTORIAL_1
                    self.navigation.hamburgerButton.alpha = 1.0
                    self.startArrowAnimation(arrow: self.leftArrow)
                    self.view.isUserInteractionEnabled = true
                })})})
    }
    
    func startArrowAnimation(arrow:UIImageView) {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [.repeat, .autoreverse, .overrideInheritedDuration], animations: {
            arrow.transform = CGAffineTransform(translationX: 0, y: -10.0)
        }, completion: nil)
    }
    
    
    func filterTutorial(animationDir : CGFloat){
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
              self.performBeforeTutorial()
        }, completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                self.tutorialText.transform = CGAffineTransform(translationX: (animationDir * SCREEN_SIZE.width), y: 0)
            }, completion: { (finished:Bool) in
                self.tutorialText.alpha = 1.0
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                    self.tutorialText.transform = CGAffineTransform.identity
                    self.rightArrow.alpha = 1.0
                    self.labelTopConstraint.constant = 72.0 + self.navigationYPos
                    self.labelLeftConstraint.constant = 85.0
                    self.labelRightConstraint.constant = 85.0
                    self.tutorialText.text = TEXT.TUTORIAL_2
                    self.navigation.editButton.alpha = 1.0
                    self.startArrowAnimation(arrow: self.rightArrow)
                    self.view.isUserInteractionEnabled = true
                })})})
    }
    
    func calenderTutorial(animationDir : CGFloat){
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
            self.performBeforeTutorial()
            
        }, completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                self.tutorialText.transform = CGAffineTransform(translationX: (animationDir * SCREEN_SIZE.width), y: 0)
            }, completion: { (finished:Bool) in
                self.tutorialText.alpha = 1.0
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                    self.tutorialText.transform = CGAffineTransform.identity
                    self.centerArrow.alpha = 1.0
                    self.labelTopConstraint.constant = 122.0 + self.navigationYPos
                    self.labelLeftConstraint.constant = 69.0
                    self.labelRightConstraint.constant = 69.0
                    self.tutorialText.text = TEXT.TUTORIAL_3
                    self.navigation.editButton.alpha = 0.0
                    self.navigation.hamburgerButton.alpha = 0.0
                    self.navigation.titleLabel.alpha = 1.0
                    self.centerArrowVerticalConstraint.constant = 13.5
                    self.startArrowAnimation(arrow: self.centerArrow)
                    self.view.isUserInteractionEnabled = true
                })})})
    }
    
    func dropDownTutorial(animationDir : CGFloat){
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
            self.performBeforeTutorial()
            
        }, completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                self.tutorialText.transform = CGAffineTransform(translationX: (animationDir * SCREEN_SIZE.width), y: 0)
            }, completion: { (finished:Bool) in
                self.tutorialText.alpha = 1.0
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                    self.tutorialText.transform = CGAffineTransform.identity
                    self.runningJobsView.backgroundColor = UIColor.clear
                    self.runningJobsView.alpha = 1.0
                    self.runningJobsView.slideUpIcon.alpha = 1.0
                    self.runningJobsView.slideUpIcon.backgroundColor = UIColor.white
                    self.runningJobsView.taskListTable.isHidden = true
                    self.centerArrow.alpha = 1.0
                    self.labelTopConstraint.constant = 169 + self.navigationYPos
                    self.labelLeftConstraint.constant = 30.5
                    self.labelRightConstraint.constant = 30.5
                    self.tutorialText.text = TEXT.TUTORIAL_4
                    self.centerArrowVerticalConstraint.constant = 10.0
                    self.startArrowAnimation(arrow: self.centerArrow)
                    self.view.isUserInteractionEnabled = true
                })})})
    }
    
    func taskTutorial(animationDir : CGFloat){
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
            self.performBeforeTutorial()
            
        }, completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                self.tutorialText.transform = CGAffineTransform(translationX: (animationDir * SCREEN_SIZE.width), y: 0)
            }, completion: { (finished:Bool) in
                self.tutorialText.alpha = 1.0
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                    self.tutorialText.transform = CGAffineTransform.identity
                    self.runningJobsView.taskListTable.alpha = 1.0
                    self.runningJobsView.alpha = 1.0
                    self.runningJobsView.taskListTable.isHidden = false
                    self.runningJobsView.taskListTable.backgroundColor = UIColor.clear
                    self.runningJobsView.taskListTable.cellForRow(at: IndexPath(row: 0, section: 0))?.backgroundColor = UIColor.white
                    self.centerArrow.alpha = 1.0
                    self.labelTopConstraint.constant = 263 + self.navigationYPos
                    self.labelLeftConstraint.constant = 31
                    self.labelRightConstraint.constant = 31
                    self.tutorialText.text = TEXT.TUTORIAL_5
                    self.centerArrowVerticalConstraint.constant = 10.0
                    self.startArrowAnimation(arrow: self.centerArrow)
                    self.view.isUserInteractionEnabled = true
                })})})
    }
    
    func newButtonTutorial(animationDir : CGFloat){
        self.view.isUserInteractionEnabled = false
        self.downArrow.tintColor = COLOR.ACCEPT_TITLE_COLOR
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
            self.performBeforeTutorial()
        }, completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                self.tutorialText.transform = CGAffineTransform(translationX: (animationDir * SCREEN_SIZE.width), y: 0)
            }, completion: { (finished:Bool) in
                self.tutorialText.alpha = 1.0
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
                    self.tutorialText.transform = CGAffineTransform.identity
                    self.newButton.alpha = 1.0
                    //self.newButton.transform = CGAffineTransform.identity
                    self.downArrow.alpha = 1.0
                    self.labelTopConstraint.constant = 350.0 + self.navigationYPos
                    self.labelLeftConstraint.constant = 69.0
                    self.labelRightConstraint.constant = 69.0
                    self.tutorialText.text = TEXT.TUTORIAL_6
                    self.navigation.editButton.alpha = 0.0
                    self.navigation.hamburgerButton.alpha = 0.0
                    self.navigation.titleLabel.alpha = 0.0
                    self.centerArrowVerticalConstraint.constant = 13.5
                    self.startArrowAnimation(arrow: self.downArrow)
                    self.view.isUserInteractionEnabled = true
                })})})
    }
    
    func setRequiredTutorial(tutorialLocaion : TUTORIAL_START_POINT) {
        self.tutorialStartPoint = tutorialLocaion
        self.nextButton.setTitle(TEXT.NEXT, for: .normal)
        switch tutorialLocaion {
        case .ALL_CLEAR:
            self.pageChangeLimit = 1
            self.pageNumber.numberOfPages = 2
            self.menuTutorial(animationDir: -1.0)
        case .NEW_TASK:
            self.pageChangeLimit = 2
            self.pageNumber.numberOfPages = 3
            self.menuTutorial(animationDir: -1.0)
        case .ONLY_ASSIGNED:
            self.pageChangeLimit = 4
            self.pageNumber.numberOfPages = 5
            self.menuTutorial(animationDir: -1.0)
        case .FULL_TUTORIAL:
            self.pageChangeLimit = 5
            self.pageNumber.numberOfPages = 6
            self.menuTutorial(animationDir: -1.0)
        case .AFTER_ACCEPT:
            self.pageChangeLimit = 2
            self.pageNumber.numberOfPages = 3
            self.taskTutorial(animationDir: -1.0)
            break
        case .ONLY_NEW_TASK:
            self.skipButton.isHidden = true
            self.nextButton.isHidden = true
            self.pageNumber.isHidden = true
            self.nextButton.setTitle(TEXT.DONE, for: .normal)
            self.pageChangeLimit = 0
            self.pageNumber.numberOfPages = 1
            self.newButtonTutorial(animationDir: -1.0)
            break
        }
    }
    
    func setNextTutorial(nextPage:Int, animationDir: CGFloat){
        self.nextButton.setTitle(TEXT.NEXT, for: .normal)
        switch tutorialStartPoint! {
        case .ALL_CLEAR:
            switch nextPage {
            case 0:
                self.menuTutorial(animationDir: animationDir)
            case 1:
                self.nextButton.setTitle(TEXT.DONE, for: .normal)
                self.calenderTutorial(animationDir: animationDir)
            default:
                break
            }
            
        case .NEW_TASK:
            switch nextPage {
            case 0:
                self.menuTutorial(animationDir: animationDir)
            case 1:
                self.calenderTutorial(animationDir: animationDir)
                break
            case 2:
                self.nextButton.setTitle(TEXT.DONE, for: .normal)
                self.newButtonTutorial(animationDir: animationDir)
            default:
                break
            }
            break
        case .ONLY_ASSIGNED:
            switch nextPage {
            case 0:
                self.menuTutorial(animationDir: animationDir)
            case 1:
                self.calenderTutorial(animationDir: animationDir)
                break
            case 2:
                self.taskTutorial(animationDir: animationDir)
            case 3:
                self.dropDownTutorial(animationDir: animationDir)
            case 4:
                self.nextButton.setTitle(TEXT.DONE, for: .normal)
                self.filterTutorial(animationDir: animationDir)
            default:
                break
            }
        case .FULL_TUTORIAL:
            switch nextPage {
            case 0:
                self.menuTutorial(animationDir: animationDir)
            case 1:
                self.calenderTutorial(animationDir: animationDir)
            case 2:
                self.taskTutorial(animationDir: animationDir)
            case 3:
                self.dropDownTutorial(animationDir: animationDir)
            case 4:
                self.filterTutorial(animationDir: animationDir)
            case 5:
                self.nextButton.setTitle(TEXT.DONE, for: .normal)
                self.newButtonTutorial(animationDir: animationDir)
            default:
                break
            }
        case .AFTER_ACCEPT:
            switch nextPage {
            case 0:
                self.taskTutorial(animationDir: animationDir)
            case 1:
                self.dropDownTutorial(animationDir: animationDir)
            case 2:
                self.nextButton.setTitle(TEXT.DONE, for: .normal)
                self.filterTutorial(animationDir: animationDir)
            default:
                break
            }
            break
        case .ONLY_NEW_TASK:
            switch nextPage {
            case 0:
                self.nextButton.setTitle(TEXT.DONE, for: .normal)
                self.newButtonTutorial(animationDir: animationDir)
            default:
                break
            }
            break
        }
    }
    
    func setRunningJobListView() {
        if runningJobsView == nil {
            self.runningJobsView = UINib(nibName: NIB_NAME.jobListTable, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! JobListTable
            self.runningJobsView.delegate = self
            self.runningJobsView.frame = CGRect(x: leftRightMargin, y: self.navigationHeight + 10 + self.navigationYPos, width: SCREEN_SIZE.width - (leftRightMargin * 2), height: SCREEN_SIZE.height)
            self.runningJobsView.layer.cornerRadius = 14.0
            self.runningJobsView.layer.masksToBounds = true
            self.view.addSubview(runningJobsView)
            self.runningJobsView.alpha = 0.0
            self.runningJobsView.backgroundColor = UIColor.clear
            self.runningJobsView.taskListTable.backgroundColor = UIColor.clear
            self.runningJobsView.slideUpIcon.transform = CGAffineTransform(rotationAngle: .pi)
            self.runningJobsView.filteredTasksListArray = [[taskObject]]
            self.runningJobsView.slideUpIcon.isUserInteractionEnabled = false
            self.runningJobsView.taskListTable.isScrollEnabled = false
        }
    }
    
    //MARK: SET NEW TASK BUTTON
    func setNewTaskButton() {
        //self.newButton.transform = CGAffineTransform(translationX: 0, y: self.newButton.frame.height)
        self.newButton.alpha = 0.0
        self.newButton.backgroundColor = COLOR.themeForegroundColor
        self.newButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.newButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.newButton.isUserInteractionEnabled = false
    }
    
    //Mark: Swipe Gesture target
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        let currentPage = self.pageNumber.currentPage
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            if(swipeGesture.direction == .left){
                if(currentPage != self.pageChangeLimit){
                    self.setNextTutorial(nextPage: currentPage + 1, animationDir: -1.0)
                    self.pageNumber.currentPage = currentPage + 1
                }
            } else {
                if(currentPage != 0){
                    self.setNextTutorial(nextPage: currentPage - 1, animationDir: 1.0)
                    self.pageNumber.currentPage = currentPage - 1
                }
            }
        }
    }
}

extension TutorialController : JobListDelegate {
    func gotoTaskDetailPage(index:Int, startStatus:Int, section:Int) {
        return
    }

    func gotoTaxiController(index:Int){
        
    }
    
    func slideButtonAction(){
        
    }
//    func setForceTouch(cell: UITableViewCell) {
//        
//    }
}
