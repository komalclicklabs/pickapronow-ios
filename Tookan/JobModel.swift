//
//  JobModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 16/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class JobModel: NSObject {

    func setFirstLine(jobDetails:TasksAssignedToDate, forNewTask:Bool) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        switch jobDetails.jobType {
        case JOB_TYPE.pickup:
            var dateTime:String!
            var jobTypeString = ""
            if forNewTask == false {
                dateTime = jobDetails.jobPickupDateTimeInTwelveHoursFormat
            } else {
                dateTime = jobDetails.jobPickupDatetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "dd MMM, hh:mm a")
            }
            
            if jobDetails.vertical == VERTICAL.TAXI.rawValue {
                jobTypeString = TEXT.TAXI
            } else {
                jobTypeString = TEXT.PICKUP
            }
            attributedString = NSMutableAttributedString(string: "\(dateTime!) - \(jobTypeString)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        case JOB_TYPE.delivery:
            var dateTime:String!
            if forNewTask == false {
                dateTime = jobDetails.jobDeliveryDateTimeInTwelveFormat
            } else {
                dateTime = jobDetails.jobDeliveryDatetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "dd MMM, hh:mm a")
            }
            attributedString = NSMutableAttributedString(string: "\(dateTime!) - \(TEXT.DELIVERY)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        case JOB_TYPE.appointment, JOB_TYPE.fieldWorkforce:
            var startTime:String!
            var endTime:String!
            if forNewTask == false {
                startTime = jobDetails.jobPickupDateTimeInTwelveHoursFormat
                endTime = jobDetails.jobDeliveryDateTimeInTwelveFormat
            } else {
                startTime = jobDetails.jobPickupDatetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "dd MMM, hh:mm a")
                endTime = jobDetails.jobDeliveryDatetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "dd MMM, hh:mm a")
            }

            
            attributedString = NSMutableAttributedString(string: "\(startTime!) - \(endTime!)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        default:
            break
        }
        
        if forNewTask == false {
            let status = Singleton.sharedInstance.getAttributedStatusText(jobStatus:jobDetails.jobStatus, appOptionalField: jobDetails.acceptOptionalField.value!)
            attributedString.append(status)
        }
        return attributedString
    }
    
    func setSecondLine(jobDetails:TasksAssignedToDate) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        var addressString:NSAttributedString!
        switch jobDetails.jobType {
        case JOB_TYPE.pickup:
            attributedString = NSMutableAttributedString(string: jobDetails.jobPickupName, attributes: [NSForegroundColorAttributeName:COLOR.LITTLE_LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            addressString =  NSAttributedString(string: jobDetails.jobPickupAddress, attributes: [NSForegroundColorAttributeName:COLOR.LITTLE_LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            break
        default:
            attributedString = NSMutableAttributedString(string: jobDetails.customerUsername, attributes: [NSForegroundColorAttributeName:COLOR.LITTLE_LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            addressString =  NSAttributedString(string: jobDetails.jobAddress, attributes: [NSForegroundColorAttributeName:COLOR.LITTLE_LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            break
        }
        
        
        if attributedString.length > 0 {
            attributedString.append(NSMutableAttributedString(string:", ", attributes: [NSForegroundColorAttributeName:COLOR.LITTLE_LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!]))
        } else {
            attributedString.append(NSAttributedString(string: ""))
        }
        attributedString.append(addressString)
        return attributedString
    }
    
    //MARK: Custom Field to display
    func customFieldToDispaly(_ fieldArray:[String]) -> NSMutableAttributedString {
        let fieldAttributedString = NSMutableAttributedString()
        for i in (0..<fieldArray.count) {
            if(i % 2 == 0) {
                //Header
                if(fieldArray[i] == "t_d") {
                    let header = NSMutableAttributedString(string:"\n" + TEXT.DESCRIPTION, attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.regular)!])
                    fieldAttributedString.append(header)
                } else {
                    let header = NSMutableAttributedString(string: "\n" + fieldArray[i], attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.regular)!])
                    fieldAttributedString.append(header)
                }
            } else {
                let detail = NSMutableAttributedString(string: ": " + fieldArray[i], attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
                fieldAttributedString.append(detail)
            }
        }
        
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.lineSpacing = 10.0
//        fieldAttributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle , range: NSRange(location: 0, length: fieldAttributedString.length))
        return fieldAttributedString
    }
    
    func getLatitudeLongitudeOf(job:TasksAssignedToDate) -> CLLocationCoordinate2D {
        var coordinate: CLLocationCoordinate2D!
        let latitudeString = job.jobLatitude as NSString
        let longitudeString = job.jobLongitude as NSString
        switch job.jobType {
        case JOB_TYPE.pickup:
            let latitudeString = job.jobPickupLatitude as NSString
            let longitudeString = job.jobPickupLongitude as NSString
            coordinate = CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            return coordinate
        case JOB_TYPE.delivery, JOB_TYPE.fieldWorkforce, JOB_TYPE.appointment:
            coordinate = CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            return coordinate
        default:
            break
        }
        return coordinate
    }
    
    func getAddressOf(job:TasksAssignedToDate) -> String {
        switch job.jobType {
        case JOB_TYPE.pickup, JOB_TYPE.fieldWorkforce, JOB_TYPE.appointment:
            return job.jobPickupAddress
        case JOB_TYPE.delivery:
            return job.jobAddress
        default:
            return ""
        }
    }
    
    func setLocationMarkerStatus(_ marker: GMSMarker, jobStatus: Int){
        switch jobStatus{
        case JOB_STATUS.assigned:
            marker.icon = #imageLiteral(resourceName: "marker_assigned")
        case JOB_STATUS.started:
            marker.icon = #imageLiteral(resourceName: "marker_intransit")
        case JOB_STATUS.arrived:
            marker.icon = #imageLiteral(resourceName: "marker_arrived")
        case JOB_STATUS.successful:
            marker.icon = #imageLiteral(resourceName: "marker_completed")
        case JOB_STATUS.declined, JOB_STATUS.canceled, JOB_STATUS.failed:
            marker.icon = #imageLiteral(resourceName: "marker_failed")
        case JOB_STATUS.accepted:
            marker.icon = #imageLiteral(resourceName: "marker_accepted")
        default:
            break
        }
    }
    
    func getSelectedMarker(jobStatus:Int) -> UIImage {
        switch jobStatus{
        case JOB_STATUS.assigned:
            return #imageLiteral(resourceName: "marker_selected_assigned")
        case JOB_STATUS.started:
            return #imageLiteral(resourceName: "marker_selected_intransit")
        case JOB_STATUS.arrived:
            return #imageLiteral(resourceName: "marker_selected_arrived")
        case JOB_STATUS.successful:
            return #imageLiteral(resourceName: "marker_selected_completed")
        case JOB_STATUS.failed, JOB_STATUS.canceled, JOB_STATUS.declined:
            return #imageLiteral(resourceName: "marker_selected_failed")
        case JOB_STATUS.accepted:
            return #imageLiteral(resourceName: "marker_selected_accepted")
        default:
            return #imageLiteral(resourceName: "marker_selected_assigned")
        }
    }

}
