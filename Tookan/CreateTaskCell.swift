//
//  DeliveryPointCell.swift
//  Tookan
//
//  Created by cl-macmini-150 on 05/04/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
protocol CreateTaskCellDelegate {
    func customCellShouldBeginEditing(textView:UITextView, index: Int) -> Void
    func customCellShouldReturn(textView:UITextView, index: Int)
    func showTemplates(_ textView: UITextView)
    func gotoAddressController(_ textview: UITextView)
    func updateTableHeight(descriptionText: String)
    func showDatePicker(_ index : Int)
    func setCurrentIndex(_ index:Int)
}
class CreateTaskCell: UITableViewCell, UITextViewDelegate {

    var delegate:CreateTaskCellDelegate!
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var imageHolder: UIImageView!
    @IBOutlet weak var textViewCell: UITextView!
    @IBOutlet weak var leadingFromImageTOTextView: NSLayoutConstraint!
    @IBOutlet weak var downArrowImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textViewCell.delegate = self
        
        /*===============   Setting PlaceHolder   ========================*/
        placeHolderLabel.textColor = COLOR.LIGHT_COLOR
        placeHolderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
      //  self.placeHolderLabel.transform = CGAffineTransform(translationX: 0, y: self.textViewCell.frame.height - 7)
        
        /*===============   Setting font   ========================*/
        textViewCell.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        
        self.downArrowImage.image = #imageLiteral(resourceName: "dropdownDown").withRenderingMode(.alwaysTemplate)
        self.downArrowImage.tintColor = COLOR.themeForegroundColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Functions Of UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        if textViewCell.text.isEmpty == true {
            if textView.tag == CREATE_TASK_FIELDS.description.rawValue {
                self.placeHolderLabel.text = TEXT.ENTER_DESCRIPTION
            }
            self.tranlatePlaceholderLabel(status:false)
        } else {
            if textView.tag == CREATE_TASK_FIELDS.description.rawValue {
                self.placeHolderLabel.text = TEXT.DESCRIPTION
            }
            self.tranlatePlaceholderLabel(status:true)
        }
        delegate.updateTableHeight(descriptionText: textView.text)
        delegate.customCellShouldBeginEditing(textView: textView, index: textViewCell.tag)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {

      //  self.tranlatePlaceholderLabel(status:true)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        switch textView.tag {
        case CREATE_TASK_FIELDS.date.rawValue, CREATE_TASK_FIELDS.endDate.rawValue:
            delegate.showDatePicker(textView.tag)
            return false
        case CREATE_TASK_FIELDS.address.rawValue:
            delegate.gotoAddressController(textView)
            return false
        case CREATE_TASK_FIELDS.template.rawValue:
            delegate.showTemplates(textView)
            return false
        default:
            delegate.setCurrentIndex(textView.tag)
            return true
        }
  }
    
    func textViewDidEndEditing(_ textView: UITextView){
        if textViewCell.text.isEmpty == true {
            if textView.tag == CREATE_TASK_FIELDS.description.rawValue {
                self.placeHolderLabel.text = TEXT.ENTER_DESCRIPTION
            }
            self.tranlatePlaceholderLabel(status:false)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            delegate.customCellShouldReturn(textView :textView, index: (textViewCell.tag+1))
            return false
        }
        switch textView.tag {
        case CREATE_TASK_FIELDS.phoneNO.rawValue:
            let charsLimit = 16
            let startingLength = textView.text?.characters.count ?? 0
            let lengthToAdd = text.characters.count
            let lengthToReplace =  range.length
            let newLength = startingLength + lengthToAdd - lengthToReplace
            return newLength <= charsLimit
        default:
            return true
        }
    }
    
    //MARK: Moving PlaceHolder
    func tranlatePlaceholderLabel(status:Bool) {
        if status == true {
            if self.placeHolderLabel.transform.ty != 0 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.placeHolderLabel.transform = CGAffineTransform.identity
                    
                    self.placeHolderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
                })
            }
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                if self.placeHolderLabel.transform.ty == 0 {
                    self.placeHolderLabel.transform = CGAffineTransform(translationX: 0, y: self.textViewCell.center.y - 21)
                    self.placeHolderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
                }
            })
        }
    }
    
}
