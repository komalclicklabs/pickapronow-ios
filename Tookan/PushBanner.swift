//
//  PushBanner.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/03/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import SDKDemo1

protocol PushBannerDelegate {
    func removePushBanner()
}


class PushBanner: UIView {

    @IBOutlet var closeButton: UIButton!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    var delegate:PushBannerDelegate!
    
    override func awakeFromNib() {
        /*================= Blur effect ===================*/
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
        self.sendSubview(toBack: blurEffectView)
        /*===============================================*/
        
        /*============ Detail Label ==================*/
        detailLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.medium)
        detailLabel.textColor = COLOR.TEXT_COLOR
        
        /*============ message label ==================*/
        messageLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        messageLabel.textColor = COLOR.TEXT_COLOR
        
        /*============ Close Button ===================*/
        closeButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = COLOR.TEXT_COLOR
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.bannerTapAction))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    func setPushBanner(notificationData:NotificationDataObject) {
        var time = ""
        var preposition = ""
        var jobType = ""
        switch(notificationData.job_type!) {
        case JOB_TYPE.pickup:
            time = (notificationData.start_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!
            jobType = " \(TEXT.PICKUP)"// + TEXT.PICKUP
            preposition = " \(TEXT.FROM)"// + TEXT.FROM
            break
        case JOB_TYPE.delivery:
            time =  (notificationData.end_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!
            jobType = " \(TEXT.DELIVERY)"// + TEXT.DELIVERY
            preposition = " \(TEXT.TO)"// + TEXT.TO
            break
        case JOB_TYPE.appointment, JOB_TYPE.fieldWorkforce:
            time =  "\((notificationData.start_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!) - \((notificationData.end_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!)"
            jobType = ""
            preposition = " \(TEXT.WITH)"// + TEXT.WITH
            break
        default:
            break
        }
        
        var custName = ""
        if let customer = notificationData.cust_name {
            custName = customer
        }
        
        detailLabel.text = custName.length > 0 ? ("\(time)\(jobType)\(preposition) \(custName)") : (time + jobType)
        if detailLabel.text?.length == 0 {
            if let displayName = Bundle.main.displayName {
                self.detailLabel.text = displayName.capitalized
            }
        }
        messageLabel.text = notificationData.message
        
        let detailSize = detailLabel.text?.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 77, font: UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.medium)!)
        let messageSize = messageLabel.text?.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 77, font: UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!)
        
        let height = (detailSize?.height)! + (messageSize?.height)!
        if height > 32 {
            self.frame.size.height = (HEIGHT.pushBannerHeight - 26) + height
        }
        self.layoutIfNeeded()
        self.showPushBanner()
    }

    func setPushBannerForFugu(notificationData:NotificationDataObject) {
//        var time = ""
//        var preposition = ""
//        var jobType = ""
//        switch(notificationData.job_type!) {
//        case JOB_TYPE.pickup:
//            time = (notificationData.start_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!
//            jobType = " \(TEXT.PICKUP)"// + TEXT.PICKUP
//            preposition = " \(TEXT.FROM)"// + TEXT.FROM
//            break
//        case JOB_TYPE.delivery:
//            time =  (notificationData.end_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!
//            jobType = " \(TEXT.DELIVERY)"// + TEXT.DELIVERY
//            preposition = " \(TEXT.TO)"// + TEXT.TO
//            break
//        case JOB_TYPE.appointment, JOB_TYPE.fieldWorkforce:
//            time =  "\((notificationData.start_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!) - \((notificationData.end_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!)"
//            jobType = ""
//            preposition = " \(TEXT.WITH)"// + TEXT.WITH
//            break
//        default:
//            break
//        }
//
//        var custName = ""
//        if let customer = notificationData.cust_name {
//            custName = customer
//        }
        
        
        
        
        detailLabel.text = notificationData.fuguTitle //custName.length > 0 ? ("\(time)\(jobType)\(preposition) \(custName)") : (time + jobType)
        messageLabel.text = notificationData.fuguBody
        
        let detailSize = detailLabel.text?.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 77, font: UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.medium)!)
        let messageSize = messageLabel.text?.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 77, font: UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!)
        
        let height = (detailSize?.height)! + (messageSize?.height)!
        if height > 32 {
            self.frame.size.height = (HEIGHT.pushBannerHeight - 26) + height
        }
        self.layoutIfNeeded()
        self.showPushBanner()
    }
    
    func bannerTapAction() {
        self.hidePushBanner()
        //Singleton.sharedInstance.initilizeFugu()
        FuguConfig.shared.handleRemoteNotification(userInfo: NetworkingHelper.sharedInstance.notificationArray[0].jsonObject)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.hidePushBanner()
    }
    
    func showPushBanner() {
        UIApplication.shared.setStatusBarHidden(true, with: .fade)
        UIView.animate(withDuration: 0.3, animations: {
            self.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
        }, completion: { finished in
            self.perform(#selector(self.hidePushBanner), with: nil, afterDelay: 3.0)
        })
    }
    
    func hidePushBanner() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: { finished in
            UIApplication.shared.setStatusBarHidden(false, with: .fade)
            self.delegate.removePushBanner()
        })
    }
}
