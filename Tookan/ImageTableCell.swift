//
//  ImageTableCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 28/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol ImageTableDelegate {
    func imageTapped(imageType:ImageType, index:Int, collectionTag:Int)
    func deleteOptionalImage(index:Int)
    func deleteCustomFieldImage(index:Int, sectionIndex:Int)
}

class ImageTableCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var imageCollectionView: UICollectionView!
    var imageType:ImageType!
    var delegate:ImageTableDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setImageCollectionCell()
    }

    func setImageCollectionCell() {
        self.imageCollectionView.register(UINib(nibName: NIB_NAME.imageCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.imageCollectionCell)
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
    }
    
    //MARK: UICollection View Delegate...
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: 15, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return 0
        }
        switch self.imageType! {
        case ImageType.signatureImage:
            guard Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray != nil else {
                return 0
            }
            if (Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.count)! > 0 {
                return 1
            } else {
                return 0
            }
        case ImageType.image:
            guard Singleton.sharedInstance.selectedTaskDetails.addedImagesArray != nil else {
                return 0
            }
            return (Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.count)!
        case ImageType.customFieldImage:
            guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray != nil else {
                return 0
            }
            guard collectionView.tag < (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! else {
                return 0
            }
            return (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count)
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width:CGFloat = 82
        let height:CGFloat = 82
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.imageCollectionCell, for: indexPath) as! ImageCollectionCell
        
        switch imageType! {
        case .signatureImage:
            cell.transLayer.isHidden = true
            cell.removeButton.isHidden = true
            if Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.last!.taskDescription.range(of: "http", options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil) != nil {
                cell.imageView.setImageUrl(urlString: (Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.last!.taskDescription)!, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"), indexPath: indexPath, view:collectionView)
            } else {
                let imagePath = (Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.last!.taskDescription)!
                if(Singleton.sharedInstance.isFileExistAtPath(imagePath) == true) {
                    let imageFromDocumentDirectory = UIImage(contentsOfFile: Singleton.sharedInstance.createPath(imagePath))
                    NetworkingHelper.sharedInstance.allImagesCache.setObject(imageFromDocumentDirectory!, forKey: "\(imagePath)" as NSString)
                    cell.imageView.image = imageFromDocumentDirectory
                }
            }
            break
        
        case .image:
            if indexPath.item < Singleton.sharedInstance.selectedTaskDetails.addedImagesArray!.count {
                cell.removeButton.tag = indexPath.item
                cell.removeButton.removeTarget(self, action: #selector(self.deleteCustomFieldImage(sender:)), for: .touchUpInside)
                cell.removeButton.addTarget(self, action: #selector(self.deleteOptionalImage(sender:)), for: .touchUpInside)
                if(Singleton.sharedInstance.isFileExistAtPath(Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![indexPath.item].taskDescription as String) == true) {
                    cell.imageView.setImageFromDocumentDirectory(imagePath: Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![indexPath.item].taskDescription as String, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"))
                    
                    if IJReachability.isConnectedToNetwork() == true {
                        let offlineModel = OfflineDataModel()
                        if(offlineModel.isThereAnyUnsyncedTaskInDatabase() == 0) {
                            cell.removeButton.isHidden = true
                            cell.transLayer.isHidden = false
                            cell.activityIndicator.startAnimating()
                        }
                    } else {
                        cell.removeButton.isHidden = false
                        cell.transLayer.isHidden = true
                        cell.activityIndicator.stopAnimating()
                    }
                } else {
                    cell.removeButton.isHidden = false
                    cell.transLayer.isHidden = true
                    cell.activityIndicator.stopAnimating()
                    cell.imageView.setImageUrl(urlString: Singleton.sharedInstance.selectedTaskDetails.addedImagesArray![indexPath.item].taskDescription as String, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"), indexPath: indexPath, view: collectionView)
                }
            }
            
            if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
                cell.removeButton.isHidden = true
            }
            
            break
        case .customFieldImage:
            if indexPath.item < Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count {
                cell.removeButton.tag = indexPath.item
                cell.removeButton.removeTarget(self, action: #selector(self.deleteOptionalImage(sender:)), for: .touchUpInside)
                cell.removeButton.addTarget(self, action: #selector(self.deleteCustomFieldImage(sender:)), for: .touchUpInside)
                if((Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.object(at: indexPath.item) as AnyObject).isKind(of: UIImage.classForCoder()) == true) {
                    cell.transLayer.isHidden = true
                    cell.activityIndicator.stopAnimating()
                    cell.imageView.image = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.object(at: (indexPath as NSIndexPath).item) as? UIImage
                } else {
                    if(Auxillary.isFileExistAtPath(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.object(at:indexPath.item) as! String) == true) {
                        if IJReachability.isConnectedToNetwork() == true {
                            let offlineModel = OfflineDataModel()
                            if(offlineModel.isThereAnyUnsyncedTaskInDatabase() == 0) {
                                cell.removeButton.isHidden = true
                                cell.transLayer.isHidden = false
                                cell.activityIndicator.startAnimating()
                            }
                        } else {
                            cell.removeButton.isHidden = false
                            cell.transLayer.isHidden = true
                            cell.activityIndicator.stopAnimating()
                        }
                        cell.imageView.setImageFromDocumentDirectory(imagePath: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.object(at: indexPath.item) as! String, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"))
                    } else {
                        cell.removeButton.isHidden = false
                        cell.transLayer.isHidden = true
                        cell.activityIndicator.stopAnimating()
                        cell.imageView.setImageUrl(urlString: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.object(at: indexPath.item) as! String, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"), indexPath: indexPath, view: collectionView)
                    }
                }
            }
            if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
                cell.removeButton.isHidden = true
            } else if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == false {
                cell.removeButton.isHidden = true
            }
        default:
            break
        }
        
        cell.imageView.contentMode = UIViewContentMode.scaleAspectFill
        cell.imageView.layer.cornerRadius = 3
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.imageType! {
        case ImageType.signatureImage:
            delegate.imageTapped(imageType: .signatureImage, index: indexPath.item, collectionTag: 0)
            break
        case ImageType.image:
            delegate.imageTapped(imageType: .image, index: indexPath.item, collectionTag: collectionView.tag)
        case ImageType.customFieldImage:
            delegate.imageTapped(imageType: .customFieldImage, index: indexPath.item, collectionTag: collectionView.tag)
        default:
            break
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func deleteOptionalImage(sender:UIButton) {
        self.delegate.deleteOptionalImage(index: sender.tag)
    }
    
    func deleteCustomFieldImage(sender:UIButton) {
        self.delegate.deleteCustomFieldImage(index: sender.tag, sectionIndex: self.imageCollectionView.tag)
    }
}

//extension ImageTableCell:DetailControllerDelegate{
//    func updateProgressBar() {
//        if let cell = self.imageCollectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as? ImageCollectionCell{
//            //let imageData = UIImageJPEGRepresentation(cell.imageView.image!, 1)
//            
//            var counter = 0
//            let fractionalProgress = Float(counter) / 100.0
//            let animated = counter != 0
//            cell.progressViewForImage.setProgress(fractionalProgress, animated: animated)
//            
//            for _ in 0..<100 {
//                DispatchQueue.global(qos: .background).async{
//                    DispatchQueue.main.async(execute: { () -> Void in
//                        counter += 1
//                        return
//                    })
//                }
//            }
//        }
//    }
//}
