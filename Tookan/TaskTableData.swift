//
//  TaskTableData.swift
//  Tookan
//
//  Created by Rakesh Kumar on 4/5/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class TaskTableData: NSObject, NSCoding {
    var body:[Any] = [Any]()
    var head = NSMutableArray()
    var status = NSMutableArray()
    var typeArray = NSMutableArray()
    var headerArray = NSMutableArray()
    var detailArray = NSMutableArray()
    var syncStatusArray = NSMutableArray()
    var timeStampArray = NSMutableArray()
    var indexOfStatusType:Int? = -1
    var bodySyncStatus:Int? = 1
    var bodySyncTimeStamp = ""
    
    required init(coder aDecoder: NSCoder) {
        body = aDecoder.decodeObject(forKey: "body") as! [AnyObject]
        head = aDecoder.decodeObject(forKey: "head") as! NSMutableArray
        status = aDecoder.decodeObject(forKey: "status") as! NSMutableArray
        typeArray = aDecoder.decodeObject(forKey: "typeArray") as! NSMutableArray
        headerArray = aDecoder.decodeObject(forKey: "headerArray") as! NSMutableArray
        detailArray = aDecoder.decodeObject(forKey: "detailArray") as! NSMutableArray
        syncStatusArray = aDecoder.decodeObject(forKey: "syncStatusArray") as! NSMutableArray
        timeStampArray = aDecoder.decodeObject(forKey: "timeStampArray") as! NSMutableArray
        indexOfStatusType = aDecoder.decodeObject(forKey: "indexOfStatusType") as? Int
        bodySyncStatus = aDecoder.decodeObject(forKey: "bodySyncStatus") as? Int
        bodySyncTimeStamp = aDecoder.decodeObject(forKey: "bodySyncTimeStamp") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(body, forKey: "body")
        aCoder.encode(head, forKey: "head")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(typeArray, forKey: "typeArray")
        aCoder.encode(headerArray, forKey: "headerArray")
        aCoder.encode(detailArray, forKey: "detailArray")
        aCoder.encode(indexOfStatusType, forKey: "indexOfStatusType")
        aCoder.encode(syncStatusArray, forKey: "syncStatusArray")
        aCoder.encode(timeStampArray, forKey: "timeStampArray")
        aCoder.encode(bodySyncStatus, forKey: "bodySyncStatus")
        aCoder.encode(bodySyncTimeStamp, forKey: "bodySyncTimeStamp")
    }
    
    init(json: NSDictionary) {
        if let bodyData = json["body"] as? [Any] {
            
            body = bodyData //NSMutableArray(array:bodyData)
        }
        
        
        
        if let headData = json["head"] as? NSArray {
            head = NSMutableArray(array: headData)
            if let dataTypeArray = head.value(forKey: "type") as? NSArray {
                typeArray = NSMutableArray(array: dataTypeArray)
            }
            
            for j in (0..<head.count) {
                if let dataTypeObject = (head.value(forKey: "type") as AnyObject).object(at: j) as? String {
                    if(dataTypeObject != "status") {
                        if let head = (head.value(forKey: "display_name") as AnyObject).object(at: j) as? String {
                            headerArray.add(head)
                        } else if let head = (head.value(forKey: "label") as AnyObject).object(at: j) as? String {
                            headerArray.add(head)
                        } else {
                            headerArray.add("")
                        }
                    }
                }
            }
        }
        
        for i in (0..<body.count) {
            let tempHeadArray = NSMutableArray()
            for j in (0..<self.typeArray.count) {
                let valArray = body[i] as? NSArray
                if j < (valArray?.count)! {
                    let valDictionary = valArray?[j] as! [String:Any]
                    if(self.typeArray.object(at: j) as! String != "status") {
                        if let val = valDictionary["val"] {
                            tempHeadArray.add(val)
                        } else {
                            tempHeadArray.add("")
                        }
                    } else {
                        indexOfStatusType = j
                        syncStatusArray.add("1")
                        timeStampArray.add(Auxillary.getLocalDateString())
                        
                        if let val = valDictionary["val"] as? String {
                            if(val == "") {
                                status.add("0")
                            } else {
                                status.add(valDictionary["val"] as! String)
                            }
                        } else {
                            status.add("0")
                        }
                    }
                } else {
                    if(self.typeArray.object(at: j) as! String != "status") {
                            tempHeadArray.add("")
                    } else {
                        indexOfStatusType = j
                        syncStatusArray.add("1")
                        timeStampArray.add(Auxillary.getLocalDateString())
                        status.add("0")
                    }
                }
            }
            self.detailArray.add(tempHeadArray)
        }
        
        if(typeArray.contains("status") == true) {
            typeArray.remove("status")
        }
    }
}
