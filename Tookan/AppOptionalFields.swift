//
//  AppOptionalFields.swift
//  Tookan
//
//  Created by clicklabs106 on 9/15/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
class AppOptionalFields: NSObject {
    var label = ""
    var required:Bool? = false
    var value:Int? = 0
    
    required init(coder aDecoder: NSCoder) {
        label = aDecoder.decodeObject(forKey: "label") as! String
        required = aDecoder.decodeObject(forKey: "required") as? Bool
        value = aDecoder.decodeObject(forKey: "value") as? Int
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(label, forKey: "label")
        aCoder.encode(required, forKey: "required")
        aCoder.encode(value, forKey: "value")
    }
    
    init(json: NSDictionary) {
        self.required = false
        self.value = 0
        
        if let label = json["label"] as? String {
            self.label = label
        } else {
            self.label = ""
        }
        
        if let requiredValue = json["required"] as? String {
            if requiredValue == "false" {
                self.required = false
            }else if requiredValue == "0"{
                self.required = false
            }else {
                self.required = true
            }
        }else if let requiredValue = json["required"] as? Int{
            switch requiredValue{
            case 0:
                self.required = false
            default:
                self.required = true
            }
        } else {
            self.required = false
        }
        
        
        if let valueJson = json["value"] as? String {
            self.value = NSString(string: valueJson).integerValue
        }else if let value = json["value"] as? Int{
            self.value = value
        } else {
            self.value = 0
        }
        
    }
}
