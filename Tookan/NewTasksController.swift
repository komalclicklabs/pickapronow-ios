//
//  NewTasksController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 18/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class NewTasksController: UIViewController {
    
    @IBOutlet var newListTable: UITableView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var closeButton: UIButton!
    
    var callback : ((String) -> Void)?
    var sectionFooterHeight:CGFloat = 10
    var sectionHeaderHeight:CGFloat = 57
    var fromPush:Bool = false
    var newTaskListArray = [[TasksAssignedToDate]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        UIApplication.shared.setStatusBarHidden(true, with: .fade)
        /*================= Blur effect ===================*/
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubview(toBack: blurEffectView)
        /*===============================================*/
        self.setTableView()
        self.setNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if fromPush == false {
            self.getNewTasksList()
        } else {
            self.getFilterNewTaskArray()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.setStatusBarHidden(false, with: .fade)
    }
    
    func getFilterNewTaskArray() {
        var i = 0
        while i < self.newTaskListArray.count {
            let task = self.newTaskListArray[i][0]
            let timeInterval = Int(Date().timeIntervalSince(task.jobCreationTime))
            if task.time_left_in_seconds! > 0 {
                if timeInterval >= task.time_left_in_seconds! {
                    self.newTaskListArray.remove(at: i)
                } else {
                    i += 1
                }
            } else {
                i += 1
            }
        }
        guard self.newListTable != nil else {
            return
        }
        self.newListTable.reloadData()
        self.updateTitle(count: self.newTaskListArray.count)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func setTableView() {
        self.newListTable.delegate = self
        self.newListTable.dataSource = self
        self.newListTable.register(UINib(nibName: NIB_NAME.newTaskCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.newTaskCell)
        self.newListTable.estimatedRowHeight = UITableViewAutomaticDimension
        self.newListTable.rowHeight = UITableViewAutomaticDimension
    }
    
    func setNavigationBar() {
        self.titleLabel.font = UIFont(name: UIFont().MontserratExtraLight, size: FONT_SIZE.large)
        self.titleLabel.textColor = UIColor.white
        self.updateTitle(count:self.newTaskListArray.count)
    }
    
    func updateTitle(count:Int) {
        var title = "\(count) \(TEXT.NEW)"// + TEXT.NEW
        if self.newTaskListArray.count == 1 {
            title = "\(title) \(TEXT.ORDER)"// + TEXT.ORDER
        } else {
            title = "\(title) \(TEXT.ORDERS)"//title + " " + TEXT.ORDERS
        }
        self.titleLabel.text = title
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismissController()
    }
    
    func dismissController() {
        guard self.newListTable != nil else {
            return
        }
        self.closeButton.isEnabled = false
        self.newListTable.removeFromSuperview()
        self.newListTable = nil
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.changeJobStatusResponse), object: self, userInfo: nil)
        self.dismiss(animated: true, completion: { finished in
            self.callback?("Hi")
        })
    }
    
    func acceptAction(tap:UITapGestureRecognizer) {
        
        let section = (tap.view?.tag)!
        guard section < self.newTaskListArray.count else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
            self.dismissController()
            return
        }
        
        guard IJReachability.isConnectedToNetwork() == true else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            return
        }
        
        if let cell = self.newListTable.cellForRow(at: IndexPath(row: self.newTaskListArray[section].count - 1, section: section)) as? NewTaskCell {
            cell.removeTimerButton()
        }
        let task = self.newTaskListArray[section][0]
        var jobStatus = 0
        switch (task.accept_button!) {
        case ACCEPT_TYPE.startCancel:
            jobStatus = JOB_STATUS.okAcceptStatus
            break
        default:
            jobStatus = JOB_STATUS.accepted
            break
        }
        self.sendRequestToAcceptDeclineJobStatus(jobId: task.jobId, status: jobStatus, section: (tap.view?.tag)!, vertical: task.vertical!)
    }
    
    func declineAction(sender:UIButton) {
        guard sender.tag < self.newTaskListArray.count else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
            self.dismissController()
            return
        }
        
        guard IJReachability.isConnectedToNetwork() == true else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            return
        }
        
        let section = (sender.tag)
        if let cell = self.newListTable.cellForRow(at: IndexPath(row: self.newTaskListArray[section].count - 1, section: section)) as? NewTaskCell {
            cell.removeTimerButton()
        }
        let task = self.newTaskListArray[sender.tag][0]
        self.sendRequestToAcceptDeclineJobStatus(jobId: task.jobId, status: JOB_STATUS.declined, section: sender.tag, vertical: task.vertical!)
    }
    
    func sendRequestToAcceptDeclineJobStatus(jobId:Int, status:Int, section:Int, vertical:Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode()
            if location != nil {
                lat = "\(String(describing: location?.coordinate.latitude))"
                lng = "\(String(describing: location?.coordinate.longitude))"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var params:[String:Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
        params["job_id"] = jobId
        params["job_status"] = status
        params["reason"] = ""
        params["lat"] = lat
        params["lng"] = lng
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.changeJobStatus, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { [weak self](isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                guard self != nil else {
                    return
                }
                
                guard self?.newListTable != nil else {
                    return
                }
                
                if isSucceeded == true {
                    let statusCode = response["status"] as! Int
                    switch statusCode {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self?.showInvalidAccessTokenPopup(message: (response["message"] as? String)!)
                        break
                        
                    case STATUS_CODES.SHOW_DATA:
                        if status == JOB_STATUS.accepted {
                            guard vertical != VERTICAL.TAXI.rawValue else {
                                self?.getTaskDetailFromJobId(jobId: jobId)
                                return
                            }
                        }
                        
                        if (self?.newTaskListArray.count)! > section {
                            self?.newTaskListArray.remove(at: section)
                            self?.updateTitle(count: (self?.newTaskListArray.count)!)
                            self?.newListTable.reloadData()
                            if self?.newTaskListArray.count == 0 {
                                self?.dismissController()
                            }
                        } else if self?.newTaskListArray.count == 0 {
                            self?.dismissController()
                        }
                        
                        break
                    default:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        break
                    }
                } else {
                    let statusCode = response["status"] as! Int
                    switch statusCode {
                    case STATUS_CODES.DELETED_TASK:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        if (self?.newTaskListArray.count)! > section {
                            self?.newTaskListArray.remove(at: section)
                            self?.updateTitle(count: (self?.newTaskListArray.count)!)
                            self?.newListTable.reloadData()
                            if self?.newTaskListArray.count == 0 {
                                self?.dismissController()
                            }
                        } else if self?.newTaskListArray.count == 0 {
                            self?.dismissController()
                        }
                        break
                    default:
                        self?.newListTable.reloadData()
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        break
                    }
                }
            }
        }
    }
    
    func showInvalidAccessTokenPopup(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    

    //MARK: SERVER HIT
    func getNewTasksList() {
        if IJReachability.isConnectedToNetwork() == true {
            ActivityIndicator.sharedInstance.showActivityIndicator()
            self.sendRequestForOnlineData()
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        }
    }
    
    func sendRequestForOnlineData() {
        let params = ["access_token": Singleton.sharedInstance.getAccessToken()]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.get_new_tasks, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) {
            [weak self] (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                guard self != nil else {
                    return
                }
                
                guard self?.newListTable != nil else {
                    return
                }
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self?.showInvalidAccessTokenPopup(message: response["message"] as! String)
                        break
                    default:
                        self?.newTaskListArray.removeAll(keepingCapacity: false)
                        
                        if let data = response["data"] as? [String:Any] {
                            if let items = data["new_tasks"] as? [Any] {
                                for item in items {
                                    var innerTaskListArray = [TasksAssignedToDate]()
                                    let taskList = item as! [[String:Any]]
                                    for task in taskList {
                                        let taskDetails = TasksAssignedToDate(json: task)
                                        innerTaskListArray.append(taskDetails)
                                    }
                                    self?.newTaskListArray.append(innerTaskListArray)
                                }
                            }
                        }
                        self?.newListTable.reloadData()
                        self?.updateTitle(count: (self?.newTaskListArray.count)!)
                        guard (self?.newTaskListArray.count)! > 0 else {
                            self?.dismissController()
                            return
                        }
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    guard (self?.newTaskListArray.count)! > 0 else {
                        self?.dismissController()
                        return
                    }
                }
            }
        }
    }
    
    
    func getTaskDetailFromJobId(jobId:Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        var params:[String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["job_id"] = jobId
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.viewTaskForId, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, dataResponse) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if(isSucceeded == true) {
                    switch(dataResponse["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        if let items = dataResponse["data"] as? [[String:Any]] {
                            var taxiTask:TasksAssignedToDate!
                            for item in items {
                                taxiTask = TasksAssignedToDate(json: item)
                            }
                            Singleton.sharedInstance.selectedTaskDetails = taxiTask
                            self.gotoTaxiController()
                        }
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(message: (dataResponse["message"] as? String)!)
                        break
                    default:
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: dataResponse["message"] as! String, isError: .error)
                }
            }
        })
    }
    
    func gotoTaxiController() {
        if let presentingController: UINavigationController = self.presentingViewController as? UINavigationController {
            let taxiController = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.taxiController) as! TaxiController
            self.dismiss(animated: true, completion: { finished in
                presentingController.pushViewController(taxiController, animated: true)
            })
        } else {
            self.dismissController()
        }
    }
}

extension NewTasksController: UITableViewDelegate, UITableViewDataSource {
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.newTaskListArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newTaskListArray[section].count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: sectionFooterHeight))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return sectionFooterHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerTitle = ""
        guard self.newTaskListArray[section].count > 0 else {
            return nil
        }
        
        if section == 0 {
            if self.newTaskListArray[section][0].just_now == JUST_NOW.justNow.rawValue {
                headerTitle = TEXT.JUST_NOW
            } else {
                headerTitle = TEXT.EARLIER
            }
        } else {
            let lastSectionStatus = self.newTaskListArray[section - 1][0].just_now
            switch lastSectionStatus! {
            case JUST_NOW.justNow.rawValue:
                guard self.newTaskListArray[section][0].just_now == JUST_NOW.earlier.rawValue else {
                    return nil
                }
                headerTitle = TEXT.EARLIER
                break
            case JUST_NOW.earlier.rawValue:
                return nil
            default:
                break
            }
        }

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: sectionHeaderHeight))
        headerView.backgroundColor = UIColor.clear
        
        let label = UILabel(frame: CGRect(x: 20, y: 25, width: tableView.frame.width, height: sectionHeaderHeight - 25))
        label.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        label.textColor = UIColor.white
        label.textAlignment = .left
        //if section == 0 && newTaskListArray[section][0].just_now == 1 {
            label.text  = headerTitle
//        } else if section == 1 {
//            label.text  = TEXT.EARLIER
//        }
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard self.newTaskListArray[section].count > 0 else {
            return 0.001
        }
        if section == 0 {
            return sectionHeaderHeight
        } else {
            let lastSectionStatus = self.newTaskListArray[section - 1][0].just_now
            switch lastSectionStatus! {
            case JUST_NOW.justNow.rawValue:
                guard self.newTaskListArray[section][0].just_now == JUST_NOW.earlier.rawValue else {
                    return 0.001
                }
                return sectionHeaderHeight
            case JUST_NOW.earlier.rawValue:
                return 0.001
            default:
                break
            }
            return 0.001
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.newTaskCell) as! NewTaskCell
        cell.delegate = self
        let model = JobModel()
        cell.backgroundColor = UIColor.clear
        let jobDetails = self.newTaskListArray[indexPath.section][indexPath.row]
        cell.firstLine.attributedText = model.setFirstLine(jobDetails: jobDetails, forNewTask: true)
        let attributedString = model.setSecondLine(jobDetails: self.newTaskListArray[indexPath.section][indexPath.row])
        if((jobDetails.metaData?.length)! > 0) {
            let metaDataArray = jobDetails.metaData?.components(separatedBy: "|")
            attributedString.append(model.customFieldToDispaly(metaDataArray!))
        }
        cell.secondLine.attributedText = attributedString
        cell.setDotView(indexPath: indexPath, newTaskListArray: self.newTaskListArray)
        cell.setCornerRadius(indexPath: indexPath, newTaskListArray: self.newTaskListArray)
        cell.setAcceptDeclineButton(indexPath:indexPath, newTaskListArray: self.newTaskListArray)
        cell.declineButton.tag = indexPath.section
        cell.accept.tag = indexPath.section
        cell.declineButton.addTarget(self, action: #selector(self.declineAction(sender:)), for: .touchUpInside)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.acceptAction(tap:)))
        tap.numberOfTapsRequired = 1
        cell.accept.addGestureRecognizer(tap)
        
        let contentHeight = tableView.contentSize.height + sectionHeaderHeight
        if contentHeight < tableView.bounds.height {
            var contentInset:CGFloat!

            if self.newTaskListArray.count == 1 {
                if self.newTaskListArray[0].count == 1 {
                    contentInset = (tableView.bounds.height / 2 - contentHeight / 2) - 80
                } else {
                    contentInset = (tableView.bounds.height / 2 - contentHeight / 2)
                }
            } else {
                contentInset = (tableView.bounds.height / 2 - contentHeight / 2)
            }
            self.newListTable.contentInset = UIEdgeInsetsMake(contentInset - titleLabel.frame.height, 0, 0, 0)
        }

        cell.layoutIfNeeded()
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//    }
}

extension NewTasksController:NewTaskCellDelegate {
    func deleteTaskAfterTimerCompletion(jobId: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            guard self.newListTable != nil else {
                return
            }
            for i in 0..<self.newTaskListArray.count {
                let task = self.newTaskListArray[i][0]
                if "\(task.jobId!)" == jobId {
                    self.newTaskListArray.remove(at: i)
//                    let sections = self.newListTable.numberOfSections
//                    if sections > i {
//                        self.newListTable.deleteSections(IndexSet(integer: i), with: .automatic)
//                    }
                    self.updateTitle(count: self.newTaskListArray.count)
                    self.newListTable.reloadData()
                    break
                }
            }
            guard self.newTaskListArray.count > 0 else {
                self.dismissController()
                return
            }
        }
    }
}
