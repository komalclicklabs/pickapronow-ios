//
//  JobDetailCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 23/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class JobDetailCell: UITableViewCell {
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var jobStatus: UILabel!
    @IBOutlet var line: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        /*=================== detail Label ===================*/
        self.detailLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
        self.detailLabel.textColor = COLOR.TEXT_COLOR
        
        /*=================== Job status ==================*/
        self.jobStatus.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        
        /*=================== Line ==================*/
        self.line.backgroundColor = COLOR.LINE_COLOR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
