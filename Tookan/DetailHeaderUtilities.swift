//
//  DetailHeaderUtilities.swift
//  Tookan
//
//  Created by cl-macmini-45 on 10/10/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class DetailHeaderUtilities: NSObject {

    func getRequiredValueForNotes() -> Bool {
        guard Singleton.sharedInstance.selectedTaskDetails.addedNotesArray != nil else {
            return false
        }
        if (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! > 0 {
            return false
        } else {
            if let required = Singleton.sharedInstance.selectedTaskDetails.noteOptionalField.required {
                return required
            }
        }
        return false
    }
    
    func getRequiredValueForSignature() -> Bool {
        guard Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray != nil else {
            return false
        }
        if (Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.count)! > 0 {
            return false
        } else {
            if let required = Singleton.sharedInstance.selectedTaskDetails.signatureOptionalField.required {
                return required
            }
        }
        return false
    }
    
    func getRequiredValueForImage() -> Bool {
        guard Singleton.sharedInstance.selectedTaskDetails.addedImagesArray != nil else {
            return false
        }
        if (Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.count)! > 0 {
            return false
        } else {
            if let required = Singleton.sharedInstance.selectedTaskDetails.imageOptionalField.required {
                return required
            }
        }
        return false
    }
    
    func getRequiredValueForBarcode() -> Bool {
        guard Singleton.sharedInstance.selectedTaskDetails.addedScannerArray != nil else {
            return false
        }
        if (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! > 0 {
            return false
        } else {
            if let required = Singleton.sharedInstance.selectedTaskDetails.scannerOptionalField.required {
                return required
            }
        }
        return false
    }
    
    func getRequiredValueForCustomField(sectionType:DETAIL_SECTION, section:Int) -> Bool {
        guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray != nil else {
            return false
        }
        
        switch sectionType {
        case .checkbox:
            if (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[section].fleetData)! == "true" || (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[section].fleetData)! == "1" {
                return false
            }
        case .table:
            if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section].taskTableData.status.contains("0") == false{
                return false
            }
        case .checklist:
            let checkValue = self.getChecklistCheckValue(section: section)
            if checkValue == true {
                return false
            } 
        default:
            if Singleton.sharedInstance.getFleetData(customField: (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[section])!).length > 0 {
                return false
            }
        }
        if let required = Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[section].required {
            return required
        }
        return false
    }
    
    //MARK: Get checkList Check value
    func getChecklistCheckValue(section:Int) -> Bool {
        for checklistValues in Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section].checklistArray {
            if checklistValues.check == true {
                return true
            }
        }
        return false
    }
}
