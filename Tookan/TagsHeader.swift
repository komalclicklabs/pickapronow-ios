//
//  TagsHeader.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 9/13/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class TagsHeader: UIView {

    //@IBOutlet var listButton: UIButton!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var leftTagView: UIView!
    @IBOutlet var middleTagView: UIView!
    @IBOutlet var rightTagView: UIView!
    @IBOutlet var listImageView: UIImageView!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var listOuterView: UIView!
    @IBOutlet var middleTagLabel: UILabel!
    @IBOutlet var leftTagLabel: UILabel!
    @IBOutlet var rightTagLabel: UILabel!
    @IBOutlet var middleCloseButton: UIButton!
    @IBOutlet var leftCloseButton: UIButton!
    @IBOutlet var rightCloseButton: UIButton!
    //@IBOutlet var listButtonTrailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        self.searchTextField.placeholder = TEXT.TAGS
        self.searchTextField.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.searchTextField.textColor = COLOR.TEXT_COLOR
        self.searchTextField.attributedPlaceholder = NSAttributedString(string: TEXT.TAGS, attributes: [NSForegroundColorAttributeName : COLOR.TEXT_COLOR])
        
//        self.rightTagLabel.tag = 0
//        self.middleTagLabel.tag = 1
//        self.leftTagLabel.tag = 2
        
        self.leftTagLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.extraSmall)
        self.leftTagLabel.textColor = COLOR.TEXT_COLOR
        
        self.rightTagLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.extraSmall)
        self.rightTagLabel.textColor = COLOR.TEXT_COLOR
        
        self.middleTagLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.extraSmall)
        self.middleTagLabel.textColor = COLOR.TEXT_COLOR
        
        self.rightCloseButton.layer.cornerRadius = 4
        
        self.leftCloseButton.layer.cornerRadius = 4
        
        self.middleCloseButton.layer.cornerRadius = 4
        
        self.rightTagView.backgroundColor = COLOR.LINE_COLOR
        self.rightTagView.layer.cornerRadius = 5.0
        
        self.middleTagView.backgroundColor = COLOR.LINE_COLOR
        self.middleTagView.layer.cornerRadius = 5.0
        
        self.leftTagView.backgroundColor = COLOR.LINE_COLOR
        self.leftTagView.layer.cornerRadius = 5.0
        
//        self.listOuterView.layer.borderWidth = 1
//        self.listOuterView.layer.borderColor = COLOR.LINE_COLOR.cgColor
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
