//
//  TaskTableButton.swift
//  Tookan
//
//  Created by Rakesh Kumar on 4/4/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit


protocol TaskTableButtonDelegate {
   // func showConfirmationPopup(index:Int, response:(succeeded:Bool) -> ())
    func createRequestAndTableData(_ rowId:Int, statusValue:Int, responseOfRequest:@escaping (_ succeeded:Bool, _ status:Int) -> ())
}

class TaskTableButton: UIView {

    struct tagAction {
        static let complete = 1
        static let cancel = 2
        static let revert = 3
        static let none = 4
    }
    
    struct taskTableStatus {
        static let pending = 0
        static let completed = 1
        static let cancelled = 2
    }
    
    var delegate:TaskTableButtonDelegate!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var firstButtonWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        firstButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        firstButton.layer.shadowOpacity = 0.7
        firstButton.layer.shadowColor = UIColor.gray.cgColor
        firstButton.layer.shadowRadius = 2
        
        secondButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        secondButton.layer.shadowOpacity = 0.7
        secondButton.layer.shadowColor = UIColor.gray.cgColor
        secondButton.layer.shadowRadius = 2
    }

    func setButtonStatus(_ status:Int, index:Int, taskStatus:Bool) {
        
            switch(status) {
            case taskTableStatus.pending:
                /*--------- First Button -----------*/
                self.firstButtonWidth.constant = self.frame.width/2
                self.setNeedsUpdateConstraints()
                self.firstButton.tag = (index * 100) + tagAction.complete
                self.firstButton.setTitle("Complete".localized, for: UIControlState())
                UIView.animate(withDuration: 0.15, animations: { () -> Void in
                    self.layoutIfNeeded()
                    self.firstButton.backgroundColor = COLOR.themeForegroundColor
                })
                /*----------- Second Button ----------*/
                self.secondButton.setTitle("Cancel".localized, for: UIControlState())
                self.secondButton.tag = (index * 100) + tagAction.cancel
                self.secondButton.backgroundColor = COLOR.cancelButtonColor
                self.secondButton.setImage(UIImage(), for: UIControlState())
                if(taskStatus == true) {
                    self.firstButton.isEnabled = false
                    self.secondButton.isEnabled = false
                } else {
                    self.firstButton.isEnabled = true
                    self.secondButton.isEnabled = true
                }
                break
                
            case taskTableStatus.completed:
                
                if(taskStatus == false) {
                    /*--------- First Button -----------*/
                    self.firstButtonWidth.constant = self.frame.width/1.5
                     self.setNeedsUpdateConstraints()
                    self.firstButton.setTitle("Completed".localized, for: UIControlState())
                    self.firstButton.tag = (index * 100) + tagAction.none
                    UIView.animate(withDuration: 0.15, animations: { () -> Void in
                        self.layoutIfNeeded()
                        self.firstButton.backgroundColor = COLOR.alertPositiveColor
                    })

                
                    /*----------- Second Button ----------*/
                    self.secondButton.setTitle("", for: UIControlState())
                    self.secondButton.backgroundColor = COLOR.cancelButtonColor
                    self.secondButton.setImage(UIImage(named: "revert"), for: UIControlState())
                    self.secondButton.tag = (index * 100) + tagAction.revert
                } else {
                    /*--------- First Button -----------*/
                    self.firstButtonWidth.constant = self.frame.width
                    self.setNeedsUpdateConstraints()
                    self.firstButton.setTitle("Completed".localized, for: UIControlState())
                    UIView.animate(withDuration: 0.15, animations: { () -> Void in
                        self.layoutIfNeeded()
                        self.firstButton.backgroundColor = COLOR.alertPositiveColor
                    })

                    self.firstButton.tag = (index * 100) + tagAction.none
                    /*----------- Second Button ----------*/
                    self.secondButton.isHidden = true
                }
                break
                
            case taskTableStatus.cancelled:
                if(taskStatus == false) {
                    /*--------- First Button -----------*/
                    self.firstButtonWidth.constant = self.frame.width/1.5
                    self.setNeedsUpdateConstraints()
                    self.firstButton.setTitle("Cancelled".localized, for: UIControlState())
                    UIView.animate(withDuration: 0.15, animations: { () -> Void in
                        self.layoutIfNeeded()
                        self.firstButton.backgroundColor = COLOR.alertNegativeColor
                    })

                    self.firstButton.tag = (index * 100) + tagAction.none
                
                    /*----------- Second Button ----------*/
                    self.secondButton.setTitle("", for: UIControlState())
                    self.secondButton.backgroundColor = COLOR.cancelButtonColor
                    self.secondButton.setImage(UIImage(named: "revert"), for: UIControlState())
                    self.secondButton.tag = (index * 100) + tagAction.revert
                } else {
                    /*--------- First Button -----------*/
                    self.firstButtonWidth.constant = self.frame.width
                     self.setNeedsUpdateConstraints()
                    self.firstButton.setTitle("Cancelled".localized, for: UIControlState())
                    
                    UIView.animate(withDuration: 0.15, animations: { () -> Void in
                        self.layoutIfNeeded()
                        self.firstButton.backgroundColor = COLOR.alertNegativeColor
                    })

                    self.firstButton.tag = (index * 100) + tagAction.none
                    /*----------- Second Button ----------*/
                    self.secondButton.isHidden = true
                }
                
                break
                
            default:
                break
            }
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        switch(sender.tag % 100) {
        case tagAction.none:
            break
        default:
            self.setButtonStatusAfterAction(sender.tag)
        break
        }
    }
    
    func setButtonStatusAfterAction(_ index:Int) {
        var statusValue:Int!
        switch(index % 100) {
        case tagAction.complete:
            statusValue = 1
            break
        case tagAction.cancel:
            statusValue = 2
            break
        case tagAction.revert:
            statusValue = 0
            break
        case tagAction.none:
            break
        default:
            break
        }
        self.setButtonStatus(statusValue, index: index/100, taskStatus: false)
        delegate.createRequestAndTableData(index / 100, statusValue: statusValue) { (succeeded, status) -> () in
            if(succeeded == true) {
                
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.setButtonStatus(status, index: index/100, taskStatus: false)
                })
            }
        }
    }
    
}
