//
//  SignupFooterView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 30/05/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class SignupFooterView: UIView {

    @IBOutlet var termAndConditionButton: UIButton!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var signinButton: UIButton!
    
    override func awakeFromNib() {
       
        /*================ Signup Button ======================*/
        let message = NSMutableAttributedString(string: TEXT.TERM_CONDITIONS_MESSAGE, attributes: [NSForegroundColorAttributeName:COLOR.NEW_USER_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)!])
        
        let termAndConditions = NSMutableAttributedString(string: " \(TEXT.TERM_CONDITIONS)", attributes: [NSForegroundColorAttributeName:COLOR.themeForegroundColor, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)!])
        
        message.append(termAndConditions)
        self.termAndConditionButton.setAttributedTitle(message, for: .normal)
        self.termAndConditionButton.titleLabel?.numberOfLines = 0
        
        /*================Setting Signup Button Design======================*/
        self.signupButton?.setTitle(TEXT.SIGNUP, for: .normal)
        self.signupButton?.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.signupButton.backgroundColor = COLOR.themeForegroundColor
        self.signupButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.signupButton.layer.cornerRadius = 3.0
        
        /*================ Signup Button ======================*/
        let attributedTitle = NSMutableAttributedString(string: TEXT.HAVE_AN_ACCOUNT, attributes: [NSForegroundColorAttributeName:COLOR.NEW_USER_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)!])
        attributedTitle.addAttribute(NSKernAttributeName, value: CGFloat(0.5), range: NSRange(location: 0, length: attributedTitle.length))
        
        let signinTitle = NSMutableAttributedString(string: TEXT.SIGN_IN, attributes: [NSForegroundColorAttributeName:COLOR.themeForegroundColor, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)!])
        signinTitle.addAttribute(NSKernAttributeName, value: CGFloat(0.5), range: NSRange(location: 0, length: signinTitle.length))
        
        attributedTitle.append(signinTitle)
        self.signinButton.setAttributedTitle(attributedTitle, for: .normal)
    }
    
}
