//
//  GetImageFromPicker.swift
//  Tookan
//
//  Created by cl-macmini-45 on 25/09/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import FirebaseAnalytics
import Crashlytics

class GetImageFromPicker: NSObject {
    enum PickerType {
        case camera
        case gallery
        case both
    }

    enum PickerResult {
        case success(UIImage,Int)
        case error(String)
    }
    
    var imageCallBack: ((_ result: PickerResult) -> Void)?
    private var pickerType: PickerType? = .both
    private var imagePicker: UIImagePickerController?
    private var cameraButtonTitle = TEXT.CAMERA
    private var galleryButtonTitle = TEXT.GALLERY
    private var alertTitle = TEXT.UPLOAD_IMAGE_FROM
    var index = -1
    private var visibleController:UIViewController?
    
    override init() {}
    
    func getPickerTypeForCustomTemplate(tag:Int) -> PickerType {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return .both
        }
        
        guard tag != IMAGE_TAG else {
            return .both
        }
        
        switch ImageAttributeType(rawValue: (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[tag].attribute)!)! {
        case .camera:
            return .camera
        case .gallery:
            return .gallery
        default:
            return .both
        }
    }
    
    func getPickerTypeForSignupTemplate(tag:Int) -> PickerType {
        guard Singleton.sharedInstance.fleetDetails != nil else {
            return .both
        }
        
        guard tag < (Singleton.sharedInstance.fleetDetails.signup_template_data?.count)! else {
            return .both
        }
        
        switch ImageAttributeType(rawValue: (Singleton.sharedInstance.fleetDetails.signup_template_data?[tag].attribute)!)! {
        case .camera:
            return .camera
        case .gallery:
            return .gallery
        default:
            return .both
        }
    }
    
    public func setImagePicker(imagePickerType:PickerType = .both, tag:Int = -1) {
        self.pickerType = imagePickerType
        self.index = tag
        guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else {
            return
        }
        
        guard let visibleController = rootViewController.visibleViewController else {
            return
        }
        self.visibleController = visibleController
        self.showAlert()
    }
    
    private func showAlert() {
        switch self.pickerType! {
        case .camera:
            self.cameraAction()
        case .gallery:
            self.galleryAction()
        default:
            let alert = UIAlertController(title: self.alertTitle, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            self.addCameraAction(alert: alert)
            self.addGalleryAction(alert: alert)
            let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.cancel){
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            DispatchQueue.main.async {
                self.visibleController?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func addCameraAction(alert:UIAlertController) {
        let cameraAction = UIAlertAction(title: self.cameraButtonTitle, style: UIAlertActionStyle.default){
            UIAlertAction in
            self.cameraAction()
        }
        alert.addAction(cameraAction)
    }
    
    private func addGalleryAction(alert:UIAlertController) {
        let gallaryAction = UIAlertAction(title: self.galleryButtonTitle, style: UIAlertActionStyle.default){
            UIAlertAction in
            self.galleryAction()
        }
        alert.addAction(gallaryAction)
    }
    
    private func cameraAction() {
        self.checkCameraAutorizationStatus(completion: {(isAuthorized) in
            DispatchQueue.main.async {
                guard isAuthorized == true else {
                    self.imageCallBack!(.error(ERROR_MESSAGE.REJECTED_CAMERA_SUPPORT))
                    return
                }
                self.openCamera()
            }
        })
    }
    
    private func galleryAction() {
        self.checkGalleryAuthorizationStatus(completion: { (isAuthorized) in
            DispatchQueue.main.async {
                guard isAuthorized == true else {
                    self.imageCallBack!(.error(ERROR_MESSAGE.REJECTED_GALLERY_ACCESS))
                    return
                }
                self.openGallery()
            }
        })
    }
    
    private func checkCameraAutorizationStatus(completion: ((Bool) -> Void)?) {
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch authStatus {
        case .authorized:
        completion?(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                if granted {
                    completion?(true)
                } else {
                    completion?(false)
                }
            }
        default:
            return (completion?(false))!
        }
    }
    
    private func checkGalleryAuthorizationStatus(completion: ((Bool) -> Void)?) {
        let authStatus = PHPhotoLibrary.authorizationStatus() //PHPhotoLibrary.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch authStatus {
        case .authorized:
            completion?(true)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization{ grantedStatus in
                switch grantedStatus {
                case .authorized:
                    completion?(true)
                default:
                    (completion?(false))!
                }
            }
        default:
            return (completion?(false))!
        }
    }
    
    private func openCamera() {
        self.imagePicker = UIImagePickerController()
        self.imagePicker?.delegate = self
        Analytics.logEvent(ANALYTICS_KEY.TASK_DETAIL_IMAGE_CAMERA, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.TASK_DETAIL_IMAGE_CAMERA, customAttributes: [:])
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePicker?.allowsEditing = false
            self.imagePicker?.sourceType = UIImagePickerControllerSourceType.camera
            guard let picker = self.imagePicker else {
                    self.imageCallBack!(.error(ERROR_MESSAGE.SOMETHING_WRONG))
                    return
            }
            DispatchQueue.main.async {
                self.visibleController?.present(picker, animated: true, completion: nil)
            }
        } else {
            self.imageCallBack!(.error(ERROR_MESSAGE.NO_CAMERA_SUPPORT))
        }
    }
    
    private func openGallery() {
        self.imagePicker = UIImagePickerController()
        self.imagePicker?.delegate = self
        Analytics.logEvent(ANALYTICS_KEY.TASK_DETAIL_IMAGE_GALLERY, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.TASK_DETAIL_IMAGE_GALLERY, customAttributes: [:])
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            self.imagePicker?.sourceType = UIImagePickerControllerSourceType.photoLibrary
            guard let picker = self.imagePicker else {
                self.imageCallBack!(.error(ERROR_MESSAGE.SOMETHING_WRONG))
                return
            }
            DispatchQueue.main.async {
                self.visibleController?.present(picker, animated: true, completion: nil)
            }
        } else {
            self.imageCallBack!(.error(ERROR_MESSAGE.NO_GALLERY_SUPPORT))
        }
    }
}
//MARK: ImagePickerDelegate Methods
extension GetImageFromPicker:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        picker.dismiss(animated: true, completion: { finished in
            DispatchQueue.main.async {
                if self.imageCallBack != nil {
                    self.imageCallBack!(.success(image, self.index))
                }
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
