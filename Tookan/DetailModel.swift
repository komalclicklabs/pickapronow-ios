//
//  DetailModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 23/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Crashlytics
import MapKit

protocol NotesModelDelegate {
    func sendRequestToAddNewNotes(enteredNote:String, index:Int, timeStamp:String, type:ADDED_DETAILS_TYPE)
    func noInternetConnection()
    func deleteNoteAction(index:Int, type:ADDED_DETAILS_TYPE)
    func scannerAction(index:Int)
    func invalidAccessToken(message:String)
    func callFunction(url:URL)
    func openMessageController()
    func navigationErrorMessage(appName:String, directionMode:String, origin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D)
}

class DetailModel: NSObject {
    
    var sections = [DETAIL_SECTION]()
    var detailRows = [DETAIL_ROWS]()
    var detailsTable:UITableView!
    var notesDelegate:NotesModelDelegate!
    let unsyncedObjectContext = DatabaseManager.sharedInstance.managedObjectContext
    var currentIndex:Int!
    var currentSection:Int!
    var countBeforeCustomFields:Int!
    
    var showListCell = false

    func setSectionsAndRows() {
        sections = [.jobDetails]
        if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
            if Singleton.sharedInstance.selectedTaskDetails.addedNotesArray != nil {
                if (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! > 0 {
                    sections.append(.notes)
                }
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray != nil {
                if (Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.count)! > 0 {
                    sections.append(.signature)
                }
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.addedImagesArray != nil {
                if (Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.count)! > 0 {
                    sections.append(.image)
                }
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.addedScannerArray != nil {
                if (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! > 0 {
                    sections.append(.barcode)
                }
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.invoiceHtml.length > 0 {
                if Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.successful {
                    sections.append(.invoice)
                    
                }
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.successful {
                if (Singleton.sharedInstance.selectedTaskDetails.driverJobTotal?.length)! > 0 {
                    sections.append(.earnings)
                } else if (Singleton.sharedInstance.selectedTaskDetails.customerJobTotal?.length)! > 0 {
                    sections.append(.earnings)
                } else if Singleton.sharedInstance.selectedTaskDetails.isRatingEnabled == true {
                    sections.append(.earnings)
                }
            }
        } else if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
            if Singleton.sharedInstance.selectedTaskDetails.noteOptionalField.value == 1 {
                sections.append(.notes)
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.signatureOptionalField.value == 1 {
                sections.append(.signature)
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.imageOptionalField.value == 1 {
                sections.append(.image)
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.scannerOptionalField.value == 1 {
                sections.append(.barcode)
            }
        }
        
        
        self.countBeforeCustomFields = sections.count
        for customField in Singleton.sharedInstance.selectedTaskDetails.customFieldArray! {
            switch customField.dataType {
            case CUSTOM_FIELD_DATA_TYPE.barcode:
                sections.append(.customBarcode)
            case CUSTOM_FIELD_DATA_TYPE.checkbox:
                sections.append(.checkbox)
            case CUSTOM_FIELD_DATA_TYPE.checklist:
                sections.append(.checklist)
            case CUSTOM_FIELD_DATA_TYPE.date:
                sections.append(.date)
            case CUSTOM_FIELD_DATA_TYPE.dateFuture:
                sections.append(.dateFuture)
            case CUSTOM_FIELD_DATA_TYPE.datePast:
                sections.append(.datePast)
            case CUSTOM_FIELD_DATA_TYPE.dateTime:
                sections.append(.dateTime)
            case CUSTOM_FIELD_DATA_TYPE.dateTimeFuture:
                sections.append(.dateTimeFuture)
            case CUSTOM_FIELD_DATA_TYPE.dateTimePast:
                sections.append(.dateTimePast)
            case CUSTOM_FIELD_DATA_TYPE.dropDown:
                sections.append(.dropdown)
            case CUSTOM_FIELD_DATA_TYPE.email:
                sections.append(.email)
            case CUSTOM_FIELD_DATA_TYPE.number:
                sections.append(.number)
            case CUSTOM_FIELD_DATA_TYPE.image:
                sections.append(.customImage)
            case CUSTOM_FIELD_DATA_TYPE.table:
                sections.append(.table)
            case CUSTOM_FIELD_DATA_TYPE.telephone:
                sections.append(.telephone)
            case CUSTOM_FIELD_DATA_TYPE.text:
                sections.append(.text)
            case CUSTOM_FIELD_DATA_TYPE.url:
                sections.append(.url)
            default:
                break
            }
        }
        
        
        detailRows = [.job, .user, .location]
        if Singleton.sharedInstance.selectedTaskDetails.jobDescription.length > 0 || Singleton.sharedInstance.selectedTaskDetails.referenceImageArray.count > 0 {
            detailRows.append(.description)
        }
    }
    
    func getRowCounts(section:Int) -> Int {
        guard section < self.sections.count else {
            return 0
        }
        guard (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > (section - self.countBeforeCustomFields) else {
            return 0
        }
        switch sections[section] {
        case .jobDetails:
            return detailRows.count
        case .notes:
            return (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)!
        case .signature:
            if (Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.count)! > 0 {
                return 1
            } else {
                return 0
            }
        case .image:
            if (Singleton.sharedInstance.selectedTaskDetails.addedImagesArray?.count)! > 0 {
                return 1
            } else {
                return 0
            }
        case .barcode:
            return (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)!
        case .customImage:
            if (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[section - self.countBeforeCustomFields].imageArray.count)! > 0 {
                return 1
            } else {
                return 0
            }
        case .checkbox, .table, .invoice, .earnings:
            return 0
        case .checklist:
            if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.countBeforeCustomFields].showChecklistRows == false {
                return 0
            } else {
               return Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.countBeforeCustomFields].checklistArray.count
            }
         default:
            return 1
        }
    }
    
    func getDetailTableCell(indexPath:IndexPath, tableView:UITableView) -> UITableViewCell {
        guard indexPath.section < sections.count else {
            return UITableViewCell()
        }
        
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return UITableViewCell()
        }
        
        guard (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > (indexPath.section - self.countBeforeCustomFields) else {
            return UITableViewCell()
        }
        switch sections[indexPath.section] {
        case .jobDetails:
            switch detailRows[indexPath.row] {
            case .job:
                if showListCell == false {
                    let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobDetailCell) as! JobDetailCell
                    cell.detailLabel.text = self.getJobTime(jobDetails: Singleton.sharedInstance.selectedTaskDetails)
                    cell.jobStatus.attributedText = Singleton.sharedInstance.getAttributedStatusText(jobStatus:Singleton.sharedInstance.selectedTaskDetails.jobStatus, appOptionalField: Singleton.sharedInstance.selectedTaskDetails.acceptOptionalField.value!)
                    cell.icon.image = #imageLiteral(resourceName: "time_detail")
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobListCell) as! JobListCell
                    let jobModel = JobModel()
                    print(Singleton.sharedInstance.selectedTaskDetails)
                    cell.firstLine.attributedText = jobModel.setFirstLine(jobDetails: Singleton.sharedInstance.selectedTaskDetails, forNewTask:false)
                    cell.secondLine.attributedText = jobModel.setSecondLine(jobDetails: Singleton.sharedInstance.selectedTaskDetails)
                    cell.dot.backgroundColor = UIColor.black
                    cell.dot.layer.cornerRadius = 4.5
                    cell.patternLine.isHidden = true
                    cell.rightArrow.isHidden = true
                    //cell.setDotView(indexPath: indexPath, filteredTaskListArray: filteredTasksListArray, isRelatedTask: false)
                    return cell
                }
            case .user:
                let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.userDetailCell) as! UserDetailCell
                cell.icon.image = #imageLiteral(resourceName: "user")
                cell.detailLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
                cell.actionButton.isHidden = false
                cell.messageButton.isHidden = false
                let userName = self.getCustomerName(Singleton.sharedInstance.selectedTaskDetails)
                if userName.length > 0 {
                    cell.detailLabel.text = userName
                } else {
                    cell.detailLabel.text = " - "
                }
                
                if Singleton.sharedInstance.selectedTaskDetails.customerPhone.length > 0 {
                    if (Singleton.sharedInstance.selectedTaskDetails.maskingOptionalField.value == 1){
                        cell.messageButton.isHidden = true
                    } else {
                        cell.messageButton.isHidden = false
                    }
                    cell.actionButton.layer.cornerRadius = cell.actionButton.frame.height / 2
                    cell.actionButton.layer.masksToBounds = true
                    cell.actionButton.backgroundColor = COLOR.CALL_BUTTON_COLOR
                    cell.actionButton.setImage(#imageLiteral(resourceName: "call_detail"), for: .normal)
                    cell.actionButton.removeTarget(self, action: #selector(self.tappedOnMapNavigation), for: .touchUpInside)
                    cell.actionButton.addTarget(self, action: #selector(self.openPhone(_:)), for: .touchUpInside)
                    
                    cell.messageButton.layer.cornerRadius = cell.messageButton.frame.height / 2
                    cell.messageButton.layer.masksToBounds = true
                    cell.messageButton.backgroundColor = COLOR.CALL_BUTTON_COLOR
                    cell.messageButton.setImage(#imageLiteral(resourceName: "message").withRenderingMode(.alwaysTemplate), for: .normal)
                    cell.messageButton.tintColor = UIColor.white
                    cell.messageButton.addTarget(self, action: #selector(self.showMessageController), for: .touchUpInside)
                    
                    cell.trainlingConstraint.constant = cell.trailingConstraintWithBothButtons
                } else {
                    cell.actionButton.isHidden = true
                    cell.messageButton.isHidden = true
                }
                
                if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
                    cell.actionButton.isHidden = true
                    cell.messageButton.isHidden = true
                }
                
                return cell
            case .location:
                let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.userDetailCell) as! UserDetailCell
                cell.icon.image = #imageLiteral(resourceName: "blackPin")
                cell.actionButton.isHidden = false
                cell.messageButton.isHidden = true
                cell.trainlingConstraint.constant = cell.trailingConstraintWithSingleButton
                cell.detailLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
                let address = self.getAddress(Singleton.sharedInstance.selectedTaskDetails)
                if address.length > 0 {
                    cell.detailLabel.text = address
                } else {
                    cell.detailLabel.text = " - "
                }
                cell.actionButton.layer.cornerRadius = cell.actionButton.frame.height / 2
                cell.actionButton.layer.masksToBounds = true 
                cell.actionButton.backgroundColor = COLOR.themeForegroundColor
                cell.actionButton.setImage(#imageLiteral(resourceName: "getDirectionsSmall"), for: .normal)
                cell.actionButton.removeTarget(self, action: #selector(self.openPhone(_:)), for: .touchUpInside)
                cell.actionButton.addTarget(self, action: #selector(self.tappedOnMapNavigation), for: .touchUpInside)
                return cell
            case .description:
                let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobDescriptionCell) as! JobDescriptionCell
                cell.delegate = tableView.superview as! JobDescriptionDelegate!
                let jobDescription = self.getTaskDescription(Singleton.sharedInstance.selectedTaskDetails)
                if Singleton.sharedInstance.selectedTaskDetails.jobDescription.length > 0 {
                    cell.detailLabel.attributedText = jobDescription
                } else {
                    cell.detailLabel.text = " - "
                }
                
                if(Singleton.sharedInstance.selectedTaskDetails.referenceImageArray.count > 0) {
                    cell.bottomConstraint.constant = cell.bottomConstraintWithCollection
                    cell.imageCollectionView.isHidden = false
                } else {
                    cell.bottomConstraint.constant = cell.bottomConstraintWithoutCollection
                    cell.imageCollectionView.isHidden = true
                }
                return cell
            default:
                return UITableViewCell()
            }
        case .notes:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.notesCell) as! NotesCell
            cell.sectionTag = SECTION_TAG
            cell.headerLabel.text = TEXT.NOTE// + " " + "\(indexPath.row + 1)"
            cell.delegate = tableView.superview as! NotesDelegate!
            cell.placeholderLabel.text = TEXT.TYPE_NOTE
            var taskDescription = ""
            if indexPath.row < (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! {
                taskDescription = (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[indexPath.row].taskDescription)!
            }
            cell.descriptionView.text = taskDescription
            cell.actionButton.removeTarget(self, action: #selector(self.scanCustomBarcode(sender:)), for: .touchUpInside)
            cell.actionButton.removeTarget(self, action: #selector(self.addNewBarcode(sender:)), for: .touchUpInside)
            cell.actionButton.removeTarget(self, action: #selector(self.addNewNotes(sender:)), for: .touchUpInside)
            cell.actionButton.addTarget(self, action: #selector(self.addNewNotes(sender:)), for: .touchUpInside)
            cell.actionButton.tag = indexPath.row
            cell.actionButton.setImage(#imageLiteral(resourceName: "doneSmall").withRenderingMode(.alwaysTemplate), for: .normal)
            cell.actionButton.setImage(#imageLiteral(resourceName: "remove"), for: .selected)
            cell.descriptionView.tag = indexPath.section * SECTION_TAG + indexPath.row
            cell.actionButton.isHidden = false
            cell.activityIndicator.stopAnimating()
            if (taskDescription.length) > 0 {
                cell.placeholderLabel.isHidden = true
            } else {
                cell.placeholderLabel.isHidden = false
              //  cell.actionButton.isSelected = false
            }
            
            var notesId = 0
            if indexPath.row < (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! {
                notesId = (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[indexPath.row].IDs)!
            }
            if notesId == 0 {
                if (taskDescription.length) > 0 {
                    if IJReachability.isConnectedToNetwork() == true {
                        cell.descriptionView.isUserInteractionEnabled = false
                        cell.activityIndicator.startAnimating()
                        cell.actionButton.isHidden = true
                    } else {
                        cell.activityIndicator.stopAnimating()
                        cell.actionButton.isSelected = true
                        cell.descriptionView.isUserInteractionEnabled = true
                    }
                } else {
                    cell.activityIndicator.stopAnimating()
                    cell.actionButton.isSelected = true
                    cell.descriptionView.isUserInteractionEnabled = true
                }
            } else {
                if (taskDescription.length) > 0 {
                    cell.actionButton.isSelected = true
                } else {
                    cell.actionButton.isSelected = false
                }
            }
            
            if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
                cell.actionButton.isHidden = true
            }
            return cell
            
        case .signature:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.imageTableCell) as! ImageTableCell
            cell.imageType = ImageType.signatureImage
            cell.delegate = tableView.superview as! ImageTableDelegate!
            cell.imageCollectionView.reloadData()
            return cell
        case .checklist:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.checklistCell) as! ChecklistCell
            let checklistValues = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].checklistArray[indexPath.row] 
            cell.checklistTitle.text = checklistValues.value?.trimText
            
            if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
                cell.isUserInteractionEnabled = false
            } else if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) {
                if checklistValues.uploading == true {
                    if IJReachability.isConnectedToNetwork() == true {
                        cell.activityIndicator.startAnimating()
                        cell.isUserInteractionEnabled = false
                   } else {
                        cell.activityIndicator.stopAnimating()
                        cell.isUserInteractionEnabled = true

                    }
                } else {
                    cell.activityIndicator.stopAnimating()
                    cell.isUserInteractionEnabled = true
                }
            } else {
                cell.isUserInteractionEnabled = false
            }
            
            cell.checklistButton.isSelected = checklistValues.check!
            
            return cell
        case .image, .customImage:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.imageTableCell) as! ImageTableCell
            if sections[indexPath.section] == .image {
                cell.imageType = ImageType.image
                cell.imageCollectionView.tag = IMAGE_TAG
            } else {
                cell.imageType = ImageType.customFieldImage
                cell.imageCollectionView.tag = indexPath.section - self.countBeforeCustomFields
            }
            
            cell.delegate = tableView.superview as! ImageTableDelegate!
            cell.imageCollectionView.reloadData()
            return cell
        case .barcode, .customBarcode:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.notesCell) as! NotesCell
            cell.actionButton.isHidden = false
            if sections[indexPath.section] == .barcode {
                cell.sectionTag = BARCODE_TAG
                var taskDescription = ""
                if indexPath.row < (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! {
                    taskDescription = (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?[indexPath.row].taskDescription)!
                }
                cell.descriptionView.text = taskDescription
                cell.placeholderLabel.text = TEXT.ENTER_MANUALLY_HERE
                cell.actionButton.removeTarget(self, action: #selector(self.addNewNotes(sender:)), for: .touchUpInside)
                cell.actionButton.removeTarget(self, action: #selector(self.scanCustomBarcode(sender:)), for: .touchUpInside)
                cell.actionButton.removeTarget(self, action: #selector(self.addNewBarcode(sender:)), for: .touchUpInside)
                cell.actionButton.addTarget(self, action: #selector(self.addNewBarcode(sender:)), for: .touchUpInside)
                cell.actionButton.tag = indexPath.row
                cell.activityIndicator.stopAnimating()
                if (cell.descriptionView.text.length) > 0 {
                    cell.placeholderLabel.isHidden = true
                    cell.actionButton.isSelected = true
                    cell.actionButton.setImage(#imageLiteral(resourceName: "remove"), for: .selected)
                    var scanId = 0
                    if indexPath.row < (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! {
                        scanId = (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?[indexPath.row].IDs)!
                    }
                    if scanId == 0 {
                        if IJReachability.isConnectedToNetwork() == true {
                            cell.descriptionView.isUserInteractionEnabled = false
                            cell.activityIndicator.startAnimating()
                            cell.actionButton.isHidden = true
                            
                        } else {
                            cell.activityIndicator.stopAnimating()
                            cell.actionButton.isSelected = true
                            cell.descriptionView.isUserInteractionEnabled = true
                        }
                    } else {
                        cell.activityIndicator.stopAnimating()
                        cell.actionButton.isSelected = true
                        cell.descriptionView.isUserInteractionEnabled = true
                    }
                } else {
                    cell.placeholderLabel.isHidden = false
                    cell.actionButton.isSelected = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate), for: .normal)
                }
                
//                if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
//                    cell.actionButton.isHidden = true
//                }
                
            } else {
                cell.sectionTag = indexPath.section
                let fleetData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields])
                cell.descriptionView.text = fleetData
                cell.placeholderLabel.text = "\(TEXT.ADD) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].displayLabelName) \(TEXT.HERE)"// + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].displayLabelName + " " + TEXT.HERE
                cell.actionButton.tag = indexPath.section
                cell.actionButton.removeTarget(self, action: #selector(self.addNewNotes(sender:)), for: .touchUpInside)
                cell.actionButton.removeTarget(self, action: #selector(self.addNewBarcode(sender:)), for: .touchUpInside)
                cell.actionButton.removeTarget(self, action: #selector(self.scanCustomBarcode(sender:)), for: .touchUpInside)
                cell.actionButton.addTarget(self, action: #selector(self.scanCustomBarcode(sender:)), for: .touchUpInside)
                if (cell.descriptionView.text.length) > 0 {
                    cell.placeholderLabel.isHidden = true
                    cell.actionButton.isSelected = true
                    cell.actionButton.setImage(#imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate), for: .selected)
                } else {
                    cell.actionButton.isSelected = false
                    cell.placeholderLabel.isHidden = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate), for: .normal)
                }
            }
            cell.headerLabel.text = TEXT.BARCODE_SMALL// + " " + "\(indexPath.row + 1)"
            cell.delegate = tableView.superview as! NotesDelegate!
            cell.descriptionView.tag = indexPath.section * SECTION_TAG + indexPath.row  //indexPath.row + BARCODE_TAG
            if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == false {
                cell.actionButton.setImage(#imageLiteral(resourceName: "customLock"), for: .normal)
            }
            if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
                cell.actionButton.isHidden = true
                cell.placeholderLabel.isHidden = true
                if cell.descriptionView.text.trimText.length == 0 {
                    cell.descriptionView.text = "-"
                }
            }
            return cell
        case .date, .datePast, .dateFuture, .dateTime, .dateTimePast, .dateTimeFuture:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customFieldsCell) as! CustomFieldsCell
            cell.delegate = tableView.superview as! CustomFieldDelegate
            cell.icon.image = #imageLiteral(resourceName: "customCalendar").withRenderingMode(.alwaysTemplate)
            cell.placeholderLabel.text = "\(TEXT.SELECT) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].displayLabelName)"//TEXT.SELECT + " " + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].displayLabelName
            cell.actionButton.isEnabled = false
            var fleetData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields])
            if fleetData.range(of: "/") != nil {
                fleetData = fleetData.replacingOccurrences(of: "/", with: "-")
            }
            switch Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].dataType {
            case CUSTOM_FIELD_DATA_TYPE.date, CUSTOM_FIELD_DATA_TYPE.datePast, CUSTOM_FIELD_DATA_TYPE.dateFuture:
                cell.detailView.text! = fleetData.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd", output: "dd MMM yyyy")
                break
            default:
                cell.detailView.text! = fleetData.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd hh:mm a", output: "dd MMM yyyy hh:mm a")
                break
            }
            if cell.detailView.text.length == 0 {
                cell.detailView.text = fleetData
            }
            
            cell.detailView.tag = indexPath.section - self.countBeforeCustomFields
            
            if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
                cell.actionButton.isHidden = true
                cell.actionButton.isEnabled = false
                cell.placeholderLabel.isHidden = true
                cell.detailView.textColor = COLOR.TEXT_COLOR
                cell.icon.tintColor = COLOR.TEXT_COLOR
                if fleetData.length == 0 {
                    cell.detailView.text = "-"
                }
            } else if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
                if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].appSide == APP_SIDE.write) {
                    if fleetData.length == 0 {
                        cell.placeholderLabel.isHidden = false
                    } else {
                        cell.placeholderLabel.isHidden = true
                    }
                    cell.actionButton.isHidden = true
                    cell.detailView.textColor = COLOR.TEXT_COLOR
                    cell.icon.tintColor = COLOR.TEXT_COLOR
                } else {
                    cell.placeholderLabel.isHidden = true
                    cell.actionButton.isHidden = false
                    cell.actionButton.isSelected = false
                    cell.actionButton.isEnabled = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "customLock"), for: .normal)
                    cell.detailView.textColor = COLOR.LIGHT_COLOR
                    cell.icon.tintColor = COLOR.LIGHT_COLOR
                    cell.detailView.isEditable = false
                    cell.isUserInteractionEnabled = false
                    if fleetData.length == 0 {
                        cell.detailView.text = "-"
                    }
                }
            } else {
                cell.actionButton.isEnabled = false
                cell.actionButton.isHidden = false
                cell.actionButton.setImage(#imageLiteral(resourceName: "customLock"), for: .normal)
                cell.detailView.textColor = COLOR.LIGHT_COLOR
                cell.icon.tintColor = COLOR.LIGHT_COLOR
                if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].appSide == APP_SIDE.write) {
                    if fleetData.length == 0 {
                        cell.placeholderLabel.isHidden = false
                    } else {
                        cell.placeholderLabel.isHidden = true
                    }
                } else {
                    cell.placeholderLabel.isHidden = true
                    if fleetData.length == 0 {
                        cell.detailView.text = "-"
                    }
                }
            }
            return cell
        case .email, .number, .text, .telephone, .url, .dropdown:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customFieldsCell) as! CustomFieldsCell
            cell.delegate = tableView.superview as! CustomFieldDelegate
            cell.placeholderLabel.text = "\(TEXT.ADD) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].displayLabelName) \(TEXT.HERE)"//TEXT.ADD + " " + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].displayLabelName + " " + TEXT.HERE
            cell.actionButton.isHidden = true
            cell.actionButton.tag = indexPath.section - self.countBeforeCustomFields
            cell.actionButton.setImage(#imageLiteral(resourceName: "edit").withRenderingMode(.alwaysTemplate), for: .selected)
            cell.actionButton.setImage(#imageLiteral(resourceName: "doneSmall").withRenderingMode(.alwaysTemplate), for: .normal)
            cell.detailView.tag = indexPath.section - self.countBeforeCustomFields
            let fleetData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields])
            print(fleetData)
            cell.detailView.text = fleetData
            cell.actionButton.isEnabled = false
            
            if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
                cell.actionButton.isHidden = true
                cell.actionButton.isEnabled = false
                cell.detailView.isEditable = false
                cell.detailView.textColor = COLOR.TEXT_COLOR
                cell.icon.tintColor = COLOR.TEXT_COLOR
                cell.placeholderLabel.isHidden = true
                if fleetData.length == 0 {
                    cell.detailView.text = "-"
                }
            } else if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
                if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].appSide == APP_SIDE.write) {
                    cell.actionButton.isHidden = false
                    cell.actionButton.isEnabled = true
                    cell.actionButton.isSelected = true
                    cell.detailView.isEditable = true//false to disable
                    cell.detailView.textColor = COLOR.TEXT_COLOR
                    cell.icon.tintColor = COLOR.TEXT_COLOR
                    cell.isUserInteractionEnabled = true
                    if fleetData.length == 0 {
                        cell.placeholderLabel.isHidden = false
                    } else {
                        cell.placeholderLabel.isHidden = true
                    }
                    switch(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].dataType) {
                    case CUSTOM_FIELD_DATA_TYPE.dropDown:
                        cell.actionButton.isSelected = false
                        cell.actionButton.setImage(#imageLiteral(resourceName: "down_arrow").withRenderingMode(.alwaysTemplate), for: .normal)
                        if(fleetData.components(separatedBy: ",").count > 1) {
                            cell.detailView.text = ""
                            cell.placeholderLabel.text = TEXT.SELECT
                            cell.placeholderLabel.isHidden = false
                        }
                        break
                    default:
                        break
                    }
                    
                } else {
                    cell.actionButton.isEnabled = false
                    cell.placeholderLabel.isHidden = true
                    cell.actionButton.isHidden = false
                    cell.actionButton.isSelected = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "customLock"), for: .normal)
                    cell.detailView.textColor = COLOR.LIGHT_COLOR
                    cell.icon.tintColor = COLOR.LIGHT_COLOR
                    cell.detailView.isEditable = false
                    cell.isUserInteractionEnabled = false
                    if fleetData.length == 0 {
                        cell.detailView.text = "-"
                    }
                }
            } else {
                cell.actionButton.isEnabled = false
                cell.actionButton.isHidden = false
                cell.actionButton.setImage(#imageLiteral(resourceName: "customLock"), for: .normal)
                cell.detailView.textColor = COLOR.LIGHT_COLOR
                cell.icon.tintColor = COLOR.LIGHT_COLOR
                cell.isUserInteractionEnabled = false
                if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].appSide == APP_SIDE.write) {
                    if fleetData.length == 0 {
                        cell.placeholderLabel.isHidden = false
                    } else {
                        cell.placeholderLabel.isHidden = true
                    }
                } else {
                    cell.placeholderLabel.isHidden = true
                    if fleetData.length == 0 {
                        cell.detailView.text = "-"
                    }
                }
            }
            
            switch(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.countBeforeCustomFields].dataType) {
            case CUSTOM_FIELD_DATA_TYPE.text:
                cell.detailView.keyboardType = UIKeyboardType.alphabet
                cell.detailView.autocapitalizationType = UITextAutocapitalizationType.words
                cell.icon.image = #imageLiteral(resourceName: "input")//.withRenderingMode(.alwaysTemplate)
                return cell
            case CUSTOM_FIELD_DATA_TYPE.number:
                cell.detailView.keyboardType = UIKeyboardType.decimalPad
                cell.icon.image = #imageLiteral(resourceName: "numericInPut")//.withRenderingMode(.alwaysTemplate)
                return cell
            case CUSTOM_FIELD_DATA_TYPE.telephone:
                cell.isUserInteractionEnabled = true
                //cell.customSubTextField.isEnabled = true
                cell.detailView.keyboardType = UIKeyboardType.numberPad
                cell.icon.image = #imageLiteral(resourceName: "telephone")//.withRenderingMode(.alwaysTemplate)
                return cell
            case CUSTOM_FIELD_DATA_TYPE.email:
                cell.isUserInteractionEnabled = true
                //cell.customSubTextField.isEnabled = true
                cell.detailView.autocapitalizationType = UITextAutocapitalizationType.none
                cell.detailView.keyboardType = UIKeyboardType.emailAddress
                cell.icon.image = #imageLiteral(resourceName: "customEmail")//.withRenderingMode(.alwaysTemplate)
                return cell
            case CUSTOM_FIELD_DATA_TYPE.url:
                cell.isUserInteractionEnabled = true
                //cell.customSubTextField.isEnabled = true
                cell.detailView.autocapitalizationType = UITextAutocapitalizationType.none
                cell.detailView.keyboardType = UIKeyboardType.URL
                cell.icon.image = #imageLiteral(resourceName: "web")//.withRenderingMode(.alwaysTemplate)
                return cell
            case CUSTOM_FIELD_DATA_TYPE.dropDown:
                cell.icon.image = #imageLiteral(resourceName: "dropdownIcon")//.withRenderingMode(.alwaysTemplate)
                return cell
            default:
                return cell
            }
        default:
            return UITableViewCell()
        }
        //return UITableViewCell()
    }
    
    func getHeaderView(section:Int, headerView:DetailHeaderView) {
        guard section < sections.count else {
            return
        }
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return
        }
        let headerViewUtilities = DetailHeaderUtilities()
        var requiredValue:Bool!
        headerView.addButton.isHidden = false
        switch sections[section] {
        case .jobDetails:
            return
        case .notes:
            headerView.setForNotes()
            requiredValue = headerViewUtilities.getRequiredValueForNotes()
        case .signature:
            headerView.setForSignature()
            requiredValue = headerViewUtilities.getRequiredValueForSignature()
        case .image:
            headerView.setForImage()
            requiredValue = headerViewUtilities.getRequiredValueForImage()
        case .barcode:
            headerView.setForBarcode()
            requiredValue = headerViewUtilities.getRequiredValueForBarcode()
        case .customBarcode:
            headerView.setForCustomBarcode(index: section - self.countBeforeCustomFields)
            requiredValue = headerViewUtilities.getRequiredValueForCustomField(sectionType: .customBarcode, section: section - self.countBeforeCustomFields)
        case .customImage:
            headerView.setForCustomImage(index: section - self.countBeforeCustomFields)
            requiredValue = headerViewUtilities.getRequiredValueForCustomField(sectionType: .customImage, section: section - self.countBeforeCustomFields)
        case .checkbox:
            headerView.setForCheckbox(index: section - self.countBeforeCustomFields)
            requiredValue = headerViewUtilities.getRequiredValueForCustomField(sectionType: .checkbox, section: section - self.countBeforeCustomFields)
        case .table:
            headerView.setForTableChecklist(index: section - self.countBeforeCustomFields)
            requiredValue = headerViewUtilities.getRequiredValueForCustomField(sectionType: .table, section: section - self.countBeforeCustomFields)
        case .checklist:
            headerView.setForChecklist(index: section - self.countBeforeCustomFields)
            requiredValue = headerViewUtilities.getRequiredValueForCustomField(sectionType: .checklist, section: section - self.countBeforeCustomFields)
        case .invoice:
            headerView.setForInvoice(index:section - self.countBeforeCustomFields)
            requiredValue = false
        case .earnings:
            headerView.setForEarnings(index:section - self.countBeforeCustomFields)
            requiredValue = false
        case .dropdown:
            headerView.setForDefaultSection(index: section - self.countBeforeCustomFields)
            requiredValue = headerViewUtilities.getRequiredValueForCustomField(sectionType: .dropdown, section: section - self.countBeforeCustomFields)
        default:
            headerView.setForDefaultSection(index: section - self.countBeforeCustomFields)
            requiredValue = headerViewUtilities.getRequiredValueForCustomField(sectionType: .date, section: section - self.countBeforeCustomFields)
        }
        headerView.updateAddButton(sections: sections, section: section, countBeforeCustomFields: self.countBeforeCustomFields)
        headerView.requiredLabel.isHidden = !requiredValue
    }
    
    //MARK: Call Button Action
    func openPhone(_ sender: UIButton) {
        guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
            return
        }
        
        if(Singleton.sharedInstance.selectedTaskDetails.maskingOptionalField.value == 1) {
            sender.isEnabled = false
            Analytics.logEvent(ANALYTICS_KEY.TASK_DETAIL_MASKING_CALL, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.TASK_DETAIL_MASKING_CALL, customAttributes: [:])
            let selelctedJobId = Singleton.sharedInstance.selectedTaskDetails.jobId
            var params : [String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
            params["job_id"] = selelctedJobId ?? 0
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.masking, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    sender.isEnabled = true
                    if isSucceeded == true {
                        if ((response["data"] as? NSDictionary) != nil) {
                            switch(response["status"] as! Int) {
                            case STATUS_CODES.SHOW_DATA:
                                if let message = response["message"] as? String {
                                    Singleton.sharedInstance.showErrorMessage(error: message, isError: .success)
                                }
                                break
                                
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                self.notesDelegate.invalidAccessToken(message: response["message"] as! String)
                                break
                            default:
                                break
                            }
                        }
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                }
            })
        } else if let phoneNumber = Singleton.sharedInstance.selectedTaskDetails.customerPhone{
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.TASK_DETAIL_CALL, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.TASK_DETAIL_CALL, customAttributes: [:])
            let phone = "tel://\(phoneNumber.replacingOccurrences(of: " ", with: ""))"
            if let url:URL = URL(string:phone) {
                self.notesDelegate.callFunction(url: url)
            } else {
                Singleton.sharedInstance.showErrorMessage(error: TEXT.CONNECTION_ERROR, isError: .error)
            }
            
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.CALLING_NOT_AVAILABLE, isError: .message)
        }
    }
    
    func showMessageController() {
        self.notesDelegate.openMessageController()
    }
    
    
    func enableDirectionButton() {
        if let cell = self.detailsTable.cellForRow(at: IndexPath(row: detailRows.index(of: .location)!, section: self.sections.index(of: .jobDetails)!)) as? UserDetailCell {
            cell.actionButton.isEnabled = true
        }
    }
    
    func tappedOnMapNavigation() {
        if let cell = self.detailsTable.cellForRow(at: IndexPath(row: detailRows.index(of: .location)!, section: self.sections.index(of: .jobDetails)!)) as? UserDetailCell {
            cell.actionButton.isEnabled = false
            self.perform(#selector(self.enableDirectionButton), with: nil, afterDelay: 0.5)
        }
        
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_START, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_START, customAttributes: [:])
        
        let destinationCoordinate = self.getDestinationAddress()
        var originCoordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        if(LocationTracker.sharedInstance().myLocation != nil) {
            originCoordinate = LocationTracker.sharedInstance().myLocation.coordinate
        }
        
        if let navigationMap = UserDefaults.standard.value(forKey: USER_DEFAULT.navigationMap) as? Int {
            if navigationMap == NAVIGATION_MAP.appleMap {
                self.openAppleMap()
            } else {
                let appName = Singleton.sharedInstance.getNavigationAppName()
                let directionMode = Singleton.sharedInstance.getDirectionMode()
                let address = Singleton.sharedInstance.getNavigationAddress(destinationCoordinate: destinationCoordinate, directionMode: directionMode)
                
                if(UIApplication.shared.canOpenURL(URL(string: address)!)) {
                    UIApplication.shared.openURL(URL(string: address)!)
                } else{
                    self.notesDelegate.navigationErrorMessage(appName: appName, directionMode:directionMode, origin:originCoordinate, destination:destinationCoordinate)
                }

            }
        }
    }

    func openAppleMap() {
        let destinationCoordinate = self.getDestinationAddress()
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: destinationCoordinate, addressDictionary:nil))
        mapItem.name = "Destination Address"
        
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }


    func getJobTime(jobDetails:TasksAssignedToDate) -> String {
        switch jobDetails.jobType {
        case JOB_TYPE.pickup:
            var jobTypeString = ""
            if jobDetails.vertical == VERTICAL.TAXI.rawValue {
                jobTypeString = TEXT.TAXI
            } else {
                jobTypeString = TEXT.PICKUP
            }
            return "\(jobDetails.jobPickupDateTimeInTwelveHoursFormat!) - \(jobTypeString)" //+ jobTypeString
        case JOB_TYPE.delivery:
            return "\(jobDetails.jobDeliveryDateTimeInTwelveFormat!) - \(TEXT.DELIVERY)"//jobDetails.jobDeliveryDateTimeInTwelveFormat + " - " + TEXT.DELIVERY
        case JOB_TYPE.appointment, JOB_TYPE.fieldWorkforce:
            return "\(jobDetails.jobPickupDateTimeInTwelveHoursFormat!) - \(jobDetails.jobDeliveryDateTimeInTwelveFormat!)"//jobDetails.jobPickupDateTimeInTwelveHoursFormat + " - " + jobDetails.jobDeliveryDateTimeInTwelveFormat
        default:
            return "-"
        }
    }
    
    //MARK: Get Customer Name
    func getCustomerName(_ selectedTaskDetails:TasksAssignedToDate) -> String {
        switch(selectedTaskDetails.jobType) {
        case JOB_TYPE.pickup:
            return selectedTaskDetails.jobPickupName
        default:
            return selectedTaskDetails.customerUsername
        }
    }
    
    func getDestinationAddress() -> CLLocationCoordinate2D {
        if(Singleton.sharedInstance.selectedTaskDetails != nil) {
            let latitudeString = Singleton.sharedInstance.selectedTaskDetails.jobLatitude as NSString
            let longitudeString = Singleton.sharedInstance.selectedTaskDetails.jobLongitude as NSString
            switch Singleton.sharedInstance.selectedTaskDetails.jobType {
            case JOB_TYPE.pickup:
                let latitudeString = Singleton.sharedInstance.selectedTaskDetails.jobPickupLatitude as NSString
                let longitudeString = Singleton.sharedInstance.selectedTaskDetails.jobPickupLongitude  as NSString
                return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            case JOB_TYPE.delivery:
                return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            case JOB_TYPE.fieldWorkforce:
                if Singleton.sharedInstance.selectedTaskDetails.jobLatitude != nil{
                    return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
                }else if Singleton.sharedInstance.selectedTaskDetails.jobPickupLatitude != nil{
                    return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
                }
            case JOB_TYPE.appointment:
                return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            default:
                break
            }
        }
        return CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
    
    
    //MARK: Get Address
    func getAddress(_ selectedTaskDetails:TasksAssignedToDate) -> String {
        switch(selectedTaskDetails.jobType) {
        case JOB_TYPE.pickup:
            return selectedTaskDetails.jobPickupAddress
        default:
            return selectedTaskDetails.jobAddress
        }
    }
    
    //MARK: Task Description
    func getTaskDescription(_ selectedTaskDetails:TasksAssignedToDate) -> NSMutableAttributedString {
        let htmlText = selectedTaskDetails.jobDescription
        if htmlText?.range(of: "</") != nil {
            let theAttributedString = try! NSAttributedString(data: (htmlText?.data(using: String.Encoding.utf8, allowLossyConversion: false)!)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            let attributedText = NSMutableAttributedString(attributedString: theAttributedString)
            return attributedText
        } else {
            let jobDescription:String!
            if selectedTaskDetails.jobDescription.length > 0 {
                jobDescription = selectedTaskDetails.jobDescription
            } else {
                jobDescription = " - "
            }
            let attributedText = NSMutableAttributedString(string: jobDescription, attributes: [NSFontAttributeName:UIFont.init(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            return attributedText
        }
    }
    
    //MARK: Add New Notes
    func addNewNotes(sender:UIButton) {
        if sender.isSelected == true {
            self.deleteNoteAction(index: sender.tag)
        } else {
            self.sendNewNotes(index: sender.tag)
        }
    }
    
    func deleteNoteAction(index:Int) {
        guard index < Singleton.sharedInstance.selectedTaskDetails.addedNotesArray!.count else {
            return
        }
        let indexPath = self.getNoteSectionIndex(index: index)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            cell.descriptionView.isUserInteractionEnabled = false
            cell.actionButton.isHidden = true
            cell.activityIndicator.startAnimating()
            self.notesDelegate.deleteNoteAction(index: index, type:ADDED_DETAILS_TYPE.text_deleted)
        }
    }
    
    func sendNewNotes(index:Int) {
        guard index < Singleton.sharedInstance.selectedTaskDetails.addedNotesArray!.count else {
            return
        }
        
        if IJReachability.isConnectedToNetwork() == false && Singleton.sharedInstance.isAppSyncingEnable() == false {
            self.notesDelegate.noInternetConnection()
            return
        }
        
        var updatedText = ""
        let timeStamp = Singleton.sharedInstance.getUTCDateString()
        let detailType:ADDED_DETAILS_TYPE!

        let indexPath = IndexPath(row: index, section: self.sections.index(of: DETAIL_SECTION.notes)!)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            guard cell.descriptionView.text.trimText.length > 0 else {
                return
            }
            if Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[index].IDs! == 0 {
                detailType = ADDED_DETAILS_TYPE.text_added
            } else {
                guard Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[index].taskDescription != cell.descriptionView.text.trimText else {
                    cell.descriptionView.resignFirstResponder()
                    cell.actionButton.isSelected = true
                    return
                }
                detailType = ADDED_DETAILS_TYPE.text_updated
            }
            
            cell.descriptionView.resignFirstResponder()
            guard cell.activityIndicator.isAnimating == false else {
                return
            }
            cell.descriptionView.isUserInteractionEnabled = false
            cell.actionButton.isSelected = true
            cell.actionButton.isHidden = true
            cell.activityIndicator.startAnimating()
            
            updatedText = cell.descriptionView.text.trimText
        } else {
            guard (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[index].taskDescription.trimText.length)! > 0 else {
                return
            }
            
            if Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[index].IDs! == 0 {
                detailType = ADDED_DETAILS_TYPE.text_added
            } else {
                detailType = ADDED_DETAILS_TYPE.text_updated
            }
            
            updatedText = (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[index].taskDescription!.trimText)!
         }
        var dict : [String:Any] = ["description": updatedText]
        dict["creation_datetime"] = timeStamp
        dict["fleet_id"] = "\(Singleton.sharedInstance.fleetDetails.fleetId!)"
        dict["latitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)"
        dict["longitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.longitude)"
        dict["type"] = detailType.rawValue

        let taskHistory = TaskHistory(json: dict as NSDictionary)
        guard index < Singleton.sharedInstance.selectedTaskDetails.addedNotesArray!.count else {
            return
        }
        Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?[index] = taskHistory
        self.setAddButtonOfNotesHeaderView(status: false)
        self.notesDelegate.sendRequestToAddNewNotes(enteredNote: updatedText, index: index, timeStamp: timeStamp, type:detailType)
    }
    
    func editNotes(index:Int) {
        guard index < Singleton.sharedInstance.selectedTaskDetails.addedNotesArray!.count else {
            return
        }
        let indexPath = IndexPath(row: index, section: self.sections.index(of: DETAIL_SECTION.notes)!)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            cell.actionButton.isSelected = false
        }
    }
    
    func getNoteFromDescription(_ message:String, timeStamp:String) -> Int! {
        for i in (0..<Singleton.sharedInstance.selectedTaskDetails.addedNotesArray!.count) {
            if(Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![i].taskDescription == message && Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![i].creationDatetime == timeStamp) {
                return i
            }
        }
        return nil
    }
    
    func getNoteSectionIndex(index:Int) -> IndexPath {
        return IndexPath(row: index, section: self.sections.index(of: .notes)!)
    }
    
    func setAddButtonOfNotesHeaderView(status:Bool) {
        if let headerView = self.detailsTable.viewWithTag(self.sections.index(of: DETAIL_SECTION.notes)! + SECTION_TAG) as? DetailHeaderView {
            headerView.addButton.isHidden = status
            if status == false {
                headerView.buttonWidthConstraint.constant = headerView.defaultButtonWidth
            } else {
                headerView.buttonWidthConstraint.constant = 0.0
            }
            headerView.setNeedsUpdateConstraints()
            UIView.animate(withDuration: 0.3) {
                headerView.layoutIfNeeded()
            }
        }
    }
    
    func getNoteFromID(_ noteID:Int) -> Int! {
        for i in (0..<Singleton.sharedInstance.selectedTaskDetails.addedNotesArray!.count) {
            if(Singleton.sharedInstance.selectedTaskDetails.addedNotesArray![i].IDs == noteID) {
                return i
            }
        }
        return nil
    }

    func getImageIndexFromImageName(_ imageName:String, selectedTaskDetails:TasksAssignedToDate) -> Int! {
        for i in (0..<selectedTaskDetails.addedImagesArray!.count) {
            if(selectedTaskDetails.addedImagesArray![i].taskDescription == imageName) {
                return i
            }
        }
        return nil
    }
    
    /*----------------- Request to delete image --------------*/
    func requestToDeleteImage(_ jobId:Int, imageId:Int, imagePath:String, selectedTaskDetails:TasksAssignedToDate) {
        selectedTaskDetails.imageDeletingStatus = true
        
        NetworkingHelper.sharedInstance.deleteTaskDetails("image_deleted", jobId: jobId, id: imageId, imagePath: imagePath) { (isSucceeded, responseData) in
            DispatchQueue.main.async {
                if isSucceeded == true {
                    selectedTaskDetails.imageDeletingStatus = false
                    // print("success = \(response)")
                    let response = responseData["response"] as! [String:Any]
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        if let jobId = response["jobId"] as? Int {
                            guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                                return
                            }
                        }
                        let previousImagePath = responseData["imagePath"] as! String
                        Singleton.sharedInstance.deleteImageFromDocumentDirectory(previousImagePath)
                        if let indexItem = self.getImageIndexFromImageName(previousImagePath, selectedTaskDetails: selectedTaskDetails) {
                            if((selectedTaskDetails.addedImagesArray?.count)! > indexItem) {
                                selectedTaskDetails.addedImagesArray?.remove(at: indexItem)
                                selectedTaskDetails.addedCaptionArray?.remove(at: indexItem)
                                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.reloadTaskTableForImageSection), object: nil)
                            }
                        }
                        _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: selectedTaskDetails.jobId, taskDetails: selectedTaskDetails, syncStatus: 1)
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        break
                    default:
                        break
                    }
                } else {
                    selectedTaskDetails.imageDeletingStatus = false
                    Singleton.sharedInstance.showErrorMessage(error: responseData["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func setAddButtonOfBarcodeHeaderView(status:Bool) {
        if let headerView = self.detailsTable.viewWithTag(self.sections.index(of: DETAIL_SECTION.barcode)! + BARCODE_TAG) as? DetailHeaderView {
            headerView.addButton.isHidden = status
            if status == false {
                headerView.buttonWidthConstraint.constant = headerView.defaultButtonWidth
            } else {
                headerView.buttonWidthConstraint.constant = 0.0
            }
            headerView.setNeedsUpdateConstraints()
            UIView.animate(withDuration: 0.3) {
                headerView.layoutIfNeeded()
            }
        }
    }
    
    //MARK: Add New Notes
    func addNewBarcode(sender:UIButton) {
        if sender.isSelected == true {
            self.deleteBarcodeAction(index: sender.tag)
        } else {
            self.sendNewBarcode(index: sender.tag)
        }
    }
    
    func scanCustomBarcode(sender:UIButton) {
        guard Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true else {
            return 
        }
        self.currentIndex = 0
        self.currentSection = sender.tag
        let indexPath = IndexPath(row: 0, section: sender.tag)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            guard cell.descriptionView.text.trimText.length > 0 else {
                notesDelegate.scannerAction(index: 0)
                return
            }
            
            guard cell.actionButton.isSelected == false else {
                notesDelegate.scannerAction(index: 0)
                return
            }
            cell.actionButton.isSelected = true
            cell.descriptionView.resignFirstResponder()
        }
    }
    
    func deleteBarcodeAction(index:Int) {
        guard index < Singleton.sharedInstance.selectedTaskDetails.addedScannerArray!.count else {
            return
        }
        let indexPath = self.getBarcodeSectionIndex(index: index)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            cell.descriptionView.isUserInteractionEnabled = false
            cell.actionButton.isHidden = true
            cell.activityIndicator.startAnimating()
            self.notesDelegate.deleteNoteAction(index: index, type:.barcode_deleted)
        }
    }
    
    func sendNewBarcode(index:Int) {
        guard index < Singleton.sharedInstance.selectedTaskDetails.addedScannerArray!.count else {
            return
        }
        
        if IJReachability.isConnectedToNetwork() == false && Singleton.sharedInstance.isAppSyncingEnable() == false {
            self.notesDelegate.noInternetConnection()
            return
        }
        
        let indexPath = IndexPath(row: index, section: self.sections.index(of: DETAIL_SECTION.barcode)!)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            guard cell.descriptionView.text.trimText.length > 0 else {
                self.currentIndex = index
                self.currentSection = self.sections.index(of: DETAIL_SECTION.barcode)!
                notesDelegate.scannerAction(index: index)
                return
            }
            cell.descriptionView.resignFirstResponder()
            guard cell.activityIndicator.isAnimating == false else {
                return
            }
            cell.descriptionView.isUserInteractionEnabled = false
            cell.actionButton.isSelected = true
            cell.actionButton.isHidden = true
            cell.activityIndicator.startAnimating()
            self.sendRequestForBarcode(index: index, barcodeText: cell.descriptionView.text.trimText)
        } else {
            let updatedText = Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?[index].taskDescription!.trimText
            guard (updatedText?.length)! > 0 else {
                self.currentIndex = index
                self.currentSection = self.sections.index(of: DETAIL_SECTION.barcode)!
                notesDelegate.scannerAction(index: index)
                return
            }
            self.sendRequestForBarcode(index: index, barcodeText:updatedText!)
        }
    }
    
    func sendRequestForBarcode(index:Int, barcodeText:String) {
        guard index < (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! else {
            return
        }
        let timeStamp = Singleton.sharedInstance.getUTCDateString()
        let detailType:ADDED_DETAILS_TYPE!
        if Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?[index].IDs! == 0 {
            detailType = ADDED_DETAILS_TYPE.barcode_added
        } else {
            detailType = ADDED_DETAILS_TYPE.barcode_updated
        }
        var dict : [String:Any] = ["description": barcodeText]
        dict["creation_datetime"] = timeStamp
        dict["fleet_id"] = "\(Singleton.sharedInstance.fleetDetails.fleetId!)"
        dict["latitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)"
        dict["longitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.longitude)"
        dict["type"] = detailType.rawValue
        let taskHistory = TaskHistory(json: dict as NSDictionary)
        Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?[index] = taskHistory
        self.setAddButtonOfBarcodeHeaderView(status: false)
        self.notesDelegate.sendRequestToAddNewNotes(enteredNote: barcodeText, index: index, timeStamp: timeStamp, type:detailType)
    }
    
    func editBarcode(index:Int) {
        guard index < Singleton.sharedInstance.selectedTaskDetails.addedScannerArray!.count else {
            return
        }
        let indexPath = IndexPath(row: index, section: self.sections.index(of: DETAIL_SECTION.barcode)!)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            cell.actionButton.isSelected = false
            cell.actionButton.setImage(#imageLiteral(resourceName: "doneSmall").withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
    func getCodeFromID(_ codeID:Int) -> Int! {
        for i in (0..<Singleton.sharedInstance.selectedTaskDetails.addedScannerArray!.count) {
            if(Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![i].IDs == codeID) {
                return i
            }
        }
        return nil
    }
    
    func getCodeFromDescription(_ message:String, timeStamp:String) -> Int! {
        for i in (0..<Singleton.sharedInstance.selectedTaskDetails.addedScannerArray!.count) {
            if(Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![i].taskDescription == message && Singleton.sharedInstance.selectedTaskDetails.addedScannerArray![i].creationDatetime == timeStamp) {
                return i
            }
        }
        return nil
    }
    
    func getBarcodeSectionIndex(index:Int) -> IndexPath {
        return IndexPath(row: index, section: self.sections.index(of: .barcode)!)
    }

    func saveOfflineCustomFieldData(_ customLabel:String, jobId:Int, customData:String, section:Int, row:Int, customFieldType:String) {
        if(Singleton.sharedInstance.isAppSyncingEnable() == true) {
            guard section < (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .message)
                return
            }
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section].sync = 0
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section].timestamp = Date().getLocalDateString
            if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
            } else {
                if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .message)
                }
            }
        } else {
            notesDelegate.noInternetConnection()
        }
    }

    func sendOnlineRequestForCustomField(_ customLabel:String, jobId:Int, customData:String, section:Int, row:Int, customFieldType:String) {
        
        NetworkingHelper.sharedInstance.updateCustomFieldWithOutImage(customLabel, job_id: jobId, data: customData, status: "", imageName: "", collectionIndex: 0) { (succeeded, response) -> () in
            DispatchQueue.main.async {
                if(succeeded == true) {
                    if let jobId = response["jobId"] as? Int {
                        guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                            return
                        }
                    }
                    guard section < (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! else {
                        return
                    }
                    switch customFieldType {
                    case CUSTOM_FIELD_DATA_TYPE.checklist:
                        guard  Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.countBeforeCustomFields].checklistArray.count > row else {
                            return
                        }
                        let checklistValues = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.countBeforeCustomFields].checklistArray[row]
                        checklistValues.uploading = false
                        if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.countBeforeCustomFields].showChecklistRows == true {
                            self.reloadParticularSection(index: section)
                        }
                        
                        break
                    default:
                        self.reloadParticularSection(index: section)
                    }
                    //
                    _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                } else {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SLOW_INTERNET_CONNECTION:
                        if let jobId = response["jobId"] as? Int {
                            guard jobId == Singleton.sharedInstance.selectedTaskDetails.jobId else {
                                return
                            }
                        }
                        self.saveOfflineCustomFieldData(customLabel, jobId: jobId, customData: customData, section:section, row:row, customFieldType:customFieldType)
                        break
                    default:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        break
                    }
                }

            }
        }
    }
    
    func checkCustomFieldsValidation(index:Int, enteredText:String) -> String {
        var message = ""
        switch(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].dataType) {
        case CUSTOM_FIELD_DATA_TYPE.telephone:
            if self.mandatoryCheckForCustomField(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index], text: enteredText) == false {
                message = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName)"// + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName
            }
            break
        case CUSTOM_FIELD_DATA_TYPE.email:
            if self.mandatoryCheckForCustomField(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index], text: enteredText) == false {
                message = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName)"//ERROR_MESSAGE.PLEASE_ENTER_VALID + " " + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName
            }
        case CUSTOM_FIELD_DATA_TYPE.url:
            if self.mandatoryCheckForCustomField(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index], text: enteredText) == false {
                message = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName)"//ERROR_MESSAGE.PLEASE_ENTER_VALID + " " + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName
            }
            break
        case CUSTOM_FIELD_DATA_TYPE.text:
            if self.mandatoryCheckForCustomField(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index], text: enteredText) == false {
                message = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName)"//ERROR_MESSAGE.PLEASE_ENTER_VALID + " " + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName
            }
            break
        case CUSTOM_FIELD_DATA_TYPE.number:
            if self.mandatoryCheckForCustomField(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index], text: enteredText) == false {
                message = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName)"//ERROR_MESSAGE.PLEASE_ENTER_VALID + " " + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName
            }
            break
        case CUSTOM_FIELD_DATA_TYPE.barcode:
            if(enteredText.trimText.length == 0) {
                message = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName)"//ERROR_MESSAGE.PLEASE_ENTER_VALID + " " + Singleton.sharedInstance.selectedTaskDetails.customFieldArray![index].displayLabelName
            }
            break
            
        default:
            break
        }
        
        return message
    }

    func mandatoryCheckForCustomField(customField:CustomFields, text:String) -> Bool {
        switch(customField.dataType) {
        case CUSTOM_FIELD_DATA_TYPE.telephone:
            if customField.required == true && customField.appSide == "1" {
                if(text.trimText.length < 7) {
                    return false
                }
            } else {
                if text.length > 0 && text.trimText.length < 7 {
                    return false
                }
            }
            break
        case CUSTOM_FIELD_DATA_TYPE.email:
            if customField.required == true && customField.appSide == "1" {
                if(Auxillary.validateEmail(text.trimText) == false) {
                    return false
                }
            } else {
                if text.length > 0 && Auxillary.validateEmail(text.trimText) == false {
                    return false
                }
            }
            
            break
            
        case CUSTOM_FIELD_DATA_TYPE.url:
            if customField.required == true && customField.appSide == "1" {
                if(Auxillary.verifyUrl(text.trimText) == false) {
                    return false
                }
            } else {
                if text.length > 0 && Auxillary.verifyUrl(text.trimText) == false {
                    return false
                }
            }
            
            break
            
        default:
            if(text.trimText.length == 0 && customField.required == true && customField.appSide == "1") {
                return false
            }
            break
        }
        
        
        return true
    }
    
    func getCustomImageIndexFromImageName(_ imageName:String, imageArray:NSMutableArray) -> Int! {
        for i in (0..<imageArray.count) {
            if(imageArray.object(at: i) as! String == imageName) {
                return i
            }
        }
        return nil
    }
    
    func isHitRequiredForRelatedTask() -> Bool {
        if Singleton.sharedInstance.selectedTaskDetails.related_job_count == Singleton.sharedInstance.selectedTaskRelatedArray.count {
            return false
        } else {
            return true
        }
    }
    
    func reloadParticularSection(index:Int) {
        DispatchQueue.main.async {
            UIView.setAnimationsEnabled(false)
            if index < self.sections.count {
                guard self.detailsTable.isSectionExist(section: index) == true else {
                    return
                }
                self.detailsTable.reloadSections(IndexSet(integer: index), with: .none)
            }
            UIView.setAnimationsEnabled(true)
        }
    }
}
