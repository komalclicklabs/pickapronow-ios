//
//  TimeCollectionCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 13/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class TimeCollectionCell: UICollectionViewCell {

    @IBOutlet var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
         self.timeLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.extraSmall)
        self.timeLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        // Initialization code
    }

}
