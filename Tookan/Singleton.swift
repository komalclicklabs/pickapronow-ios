//
//  Singleton.swift
//  Tookan
//
//  Created by cl-macmini-45 on 19/08/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import AudioToolbox
import SDKDemo1
import UserNotifications

class FilterStatus : NSObject, NSCoding {
    
    var title : String = ""
    var count : Int? = 0
    var isSelected : Bool? = false
    var isLocallySelected : Bool? = false
    var layoutType : Int? = 0
    
    init (title: String, count: Int, isSelected: Bool, isLocallySelected: Bool, layoutType: Int) {
        self.title = title
        self.count = count
        self.isSelected = isSelected
        self.isLocallySelected = isLocallySelected
        self.layoutType = layoutType
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(count, forKey: "count")
        aCoder.encode(isSelected, forKey: "isSelected")
        aCoder.encode(isLocallySelected, forKey: "isLocallySelected")
        aCoder.encode(layoutType, forKey: "layoutType")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        let count = aDecoder.decodeObject(forKey: "count") as? Int ?? 0
        let isSelected = aDecoder.decodeObject(forKey: "isSelected") as? Bool ?? false
        let isLocallySelected = aDecoder.decodeObject(forKey: "isLocallySelected") as? Bool ?? false
        let layoutType = aDecoder.decodeObject(forKey: "layoutType") as? Int ?? 0
        self.init(title: title, count: count, isSelected: isSelected, isLocallySelected: isLocallySelected,layoutType: layoutType)
    }
}

class Singleton: NSObject, ErrorDelegate, PushBannerDelegate {
    
    static let sharedInstance = Singleton()
    var forceTouch = false
    var selectedTaskDetails: TasksAssignedToDate!
    var selectedDeliveryJobDetails: TasksAssignedToDate!
    var selectedPickupJobDetails:TasksAssignedToDate!
    var selectedTaskRelatedArray:[TasksAssignedToDate]!
    var availabilityViewStatus = 0
    var availabilityEditStatus = 0
    var isItFirstTimeLaunch = true
    var availabilityData = [AvailabilityDay]()
    var isTaxiTask = false
    var fleetDetails: FleetInfoDetails!
    var errorMessageView:ErrorView!
    var pushBanner:PushBanner!
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    let appWindow = (UIApplication.shared.delegate  as! AppDelegate).window
    //var totalNewTaskCount = 0
    var filterStatusArray = [FilterStatus]()
    var templateDictionary = [FilterStatus]()
    var sortingDictionary = [FilterStatus]()
    var searchedText = ""
    var lastNotificationFlagType = 0
    var createTaskArray = [CreateTaskModel]()
    var fromPush = false
    var pushDataOfFugu:[String:Any]? = [String:Any]()
    
    fileprivate override init() {
        
    }
    
    func getAccessToken() -> String {
        return UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil ? UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String : ""
    }
    
    func getDeviceToken() -> String {
        return UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) != nil ? UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) as! String : "NoDeviceToken"
    }
    
    func logoutFromDevice() {
        let homeStoryboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: Bundle.main)
        let navigationController = homeStoryboard.instantiateViewController(withIdentifier: STORYBOARD_ID.loginNavigation) as! UINavigationController
        appWindow?.rootViewController = navigationController
        appWindow?.makeKeyAndVisible()
    }
    
    func gotoHomeStoryboard(){
        NetworkingHelper.sharedInstance.currentDate = Date().formattedWith("yyyy-MM-dd")
        let homeStoryboard = UIStoryboard(name: STORYBOARD_NAME.home, bundle: Bundle.main)
        let navigationController = homeStoryboard.instantiateViewController(withIdentifier: STORYBOARD_ID.homeNavigation) as! UINavigationController
        appWindow?.rootViewController = navigationController
        appWindow?.makeKeyAndVisible()
    }
     func shouldPerformActionFor(){
        if self.forceTouch == true {
            if let rootViewController = self.appWindow?.rootViewController as? UINavigationController {
                if let viewController = rootViewController.viewControllers.first as? HomeBaseController {
                    viewController.gotoNotificationController()
                    self.forceTouch = false
                }
            }
        }
    }
    
    func removeExtraControllerFromStack() {
        let navigationController = self.appWindow?.rootViewController as! UINavigationController
        for controller in (navigationController.viewControllers) {
            switch NSStringFromClass(controller.classForCoder).components(separatedBy: ".").last! {
            case STORYBOARD_ID.splashController, STORYBOARD_ID.signinViewController, STORYBOARD_ID.homeBaseController, STORYBOARD_ID.taxiController:
                break
            default:
                navigationController.viewControllers.remove(at: (navigationController.viewControllers.index(of: controller)!))
                break
            }
        }
    }
    
    func showInvalidAccessTokenPopup(message:String) {
        let navigationController = (UIApplication.shared.delegate)?.window??.rootViewController as! UINavigationController
        let visibleController = navigationController.visibleViewController
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        visibleController?.present(alert, animated: true, completion: nil)
    }
    
    func setDefaultValuesOfApplication() {
        /*=========== For Location Tracking Log ===========*/
        UserDefaults.standard.setValue("DidFinishLaunch", forKey: USER_DEFAULT.applicationMode)
        /*=========== To maintain updateFleetLocation hit in sequence =========*/
        UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
        /*=========== Set Default Language Locale =============*/
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) == nil) {
            UserDefaults.standard.setValue("en", forKey: USER_DEFAULT.selectedLocale)
            UserDefaults.standard.set(["en"], forKey: USER_DEFAULT.AppleLanguages)
        } else {
            let locale = UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String
            UserDefaults.standard.set([locale!], forKey: USER_DEFAULT.AppleLanguages)
        }
        /*=========== Set Server for API Hit =============*/
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) == nil) {
            UserDefaults.standard.setValue(SERVER.live, forKey: USER_DEFAULT.selectedServer)
        } else {
            if(APP_STORE.value == 1) {
                UserDefaults.standard.setValue(SERVER.live, forKey: USER_DEFAULT.selectedServer)
            }
        }
        
        /*================= Battery Usage ==================*/
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.batteryUsage) == nil) {
            UserDefaults.standard.setValue(BATTERY_USAGE.low, forKey: USER_DEFAULT.batteryUsage)
        }
        
        /*================== Layout Type ==================*/
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.layoutType) == nil) {
            UserDefaults.standard.setValue(0, forKey: USER_DEFAULT.layoutType)
        }
        
        /*========== For prefilled email on Sigin ===============*/
        if (UserDefaults.standard.value(forKey: USER_DEFAULT.useremail) == nil){
            UserDefaults.standard.setValue("", forKey: USER_DEFAULT.useremail)
        }
        /*========== For Google Map Traffic Layer ===========*/
        if UserDefaults.standard.value(forKey: USER_DEFAULT.trafficLayer) == nil{
            UserDefaults.standard.setValue(true, forKey: USER_DEFAULT.trafficLayer)
        }
        /*========= For Push Vibration ===========*/
        if UserDefaults.standard.value(forKey: USER_DEFAULT.vibration) == nil{
            UserDefaults.standard.setValue(true, forKey: USER_DEFAULT.vibration)
        }
        /*========= For showing Accept/Decline button for first ten times ==========*/
        if UserDefaults.standard.value(forKey: USER_DEFAULT.buttonCountOnTaskDetailToShow) == nil {
            UserDefaults.standard.set(0, forKey: USER_DEFAULT.buttonCountOnTaskDetailToShow)
        }
        
        /*========== For Navigation ===========*/
        if UserDefaults.standard.value(forKey: USER_DEFAULT.navigationMap) == nil{
            UserDefaults.standard.set(NAVIGATION_MAP.googleMap, forKey: USER_DEFAULT.navigationMap)
        }
        /*========== For Map Style ===========*/
        if UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) == nil{
            UserDefaults.standard.set(MAP_STYLE.dark, forKey: USER_DEFAULT.mapStyle)
        }
        
        /*================== Layout Type ==================*/
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.userAvailableStatus) == nil) {
            UserDefaults.standard.setValue(DutyStatus.offDuty, forKey: USER_DEFAULT.userAvailableStatus)
        }
        
        /*========= For Showing Tutorials ===========*/
        if UserDefaults.standard.value(forKey: USER_DEFAULT.fullTutorial) == nil {
            UserDefaults.standard.set(false, forKey: USER_DEFAULT.fullTutorial)
        }
        if UserDefaults.standard.value(forKey: USER_DEFAULT.onlyAssignedTutorial) == nil {
            UserDefaults.standard.set(false, forKey: USER_DEFAULT.onlyAssignedTutorial)
        }
        if UserDefaults.standard.value(forKey: USER_DEFAULT.onlyNewTaskTutorial) == nil {
            UserDefaults.standard.set(false, forKey: USER_DEFAULT.onlyNewTaskTutorial)
        }
        if UserDefaults.standard.value(forKey: USER_DEFAULT.afterAcceptTutorial) == nil {
            UserDefaults.standard.set(false, forKey: USER_DEFAULT.afterAcceptTutorial)
        }
        if UserDefaults.standard.value(forKey: USER_DEFAULT.newTaskTutorial) == nil {
            UserDefaults.standard.set(false, forKey: USER_DEFAULT.newTaskTutorial)
        }
        if UserDefaults.standard.value(forKey: USER_DEFAULT.allClearTutorial) == nil {
            UserDefaults.standard.set(false, forKey: USER_DEFAULT.allClearTutorial)
        }
        
//        if UserDefaults.standard.value(forKey: USER_DEFAULT.appVersion) == nil {
//            UserDefaults.standard.set(self.getVersion(), forKey: USER_DEFAULT.appVersion)
//        }
        
        
        UserDefaults.standard.synchronize()
    }
    
    func hasTaskCompleted(jobStatus:Int) -> Bool {
        switch jobStatus {
        case JOB_STATUS.successful, JOB_STATUS.canceled, JOB_STATUS.failed, JOB_STATUS.declined:
            return true
        default:
            return false
        }
    }
    
    func hasTaskStarted(jobStatus:Int, arrivedValue:Int) -> Bool {
        switch jobStatus {
        case JOB_STATUS.started:
            if(Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value == 0) {
                return true
            }
            return false
        case JOB_STATUS.arrived:
            return true
        default:
            return false
        }
    }
    
    func getFleetData(customField:CustomFields) -> String {
        if(customField.fleetData != "") {
            return customField.fleetData
        } else if(customField.data != ""){
            return customField.data
        } else {
            return ""
        }
    }
    
    //MARK: Show New task Popup
    func showNewTaskScreen(fromPush:Bool) {
        DispatchQueue.main.async {
            let navigationController = (UIApplication.shared.delegate)?.window??.rootViewController as! UINavigationController
            let visibleController = navigationController.visibleViewController
            if(NSStringFromClass((visibleController?.classForCoder)!).components(separatedBy: ".").last! == STORYBOARD_ID.newTasksController) {
                let controller = visibleController as! NewTasksController
                ActivityIndicator.sharedInstance.showActivityIndicator()
                controller.newTaskListArray.removeAll()
                for task in NetworkingHelper.sharedInstance.newTaskListArray {
                    controller.newTaskListArray.append(task)
                }
                if fromPush == true {
                    controller.getFilterNewTaskArray()
                } else {
                    controller.getNewTasksList()
                }
                ActivityIndicator.sharedInstance.hideActivityIndicator()
            } else {
                let homeStoryboard = UIStoryboard(name: STORYBOARD_NAME.home, bundle: Bundle.main)
                let controller = homeStoryboard.instantiateViewController(withIdentifier: STORYBOARD_ID.newTasksController) as! NewTasksController
                controller.fromPush = fromPush
                for task in NetworkingHelper.sharedInstance.newTaskListArray {
                    controller.newTaskListArray.append(task)
                }
                visibleController?.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String,isError:ERROR_TYPE) {
        DispatchQueue.main.async {
            let window = UIApplication.shared.keyWindow
            let navigationController = (UIApplication.shared.delegate)?.window??.rootViewController as! UINavigationController
            let visibleController = navigationController.visibleViewController
            if self.errorMessageView == nil {
                self.errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
                self.errorMessageView.delegate = self
                self.errorMessageView.frame = CGRect(x: 0, y: (visibleController?.view.frame.height)! , width: SCREEN_SIZE.width, height: HEIGHT.errorMessageHeight)
                //visibleController?.view.addSubview(errorMessageView)
                window?.addSubview(self.errorMessageView)
            }
            self.errorMessageView.setErrorMessage(message: error,isError: isError)
        }
    }
    
    func translateErrorMessage(toBottom:Bool){
        if errorMessageView != nil {
            if toBottom == true {
                self.errorMessageView.translateToBottom()
            } else {
                self.errorMessageView.translateToTop()
            }
        }
    }
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        if errorMessageView != nil {
            self.errorMessageView.removeFromSuperview()
            self.errorMessageView = nil
        }
    }
    
    
    /*=================================== Handling Push Notifications ==============================*/
    //MARK: PUSH BANNER
    func showPushBanner(notification:NotificationDataObject) {
        DispatchQueue.main.async {
            let window = UIApplication.shared.keyWindow
            if self.pushBanner == nil {
                self.pushBanner = UINib(nibName: NIB_NAME.pushBanner, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! PushBanner
                self.pushBanner.delegate = self
                self.pushBanner.frame = CGRect(x: 0, y: -HEIGHT.pushBannerHeight, width: SCREEN_SIZE.width, height: HEIGHT.pushBannerHeight)
                window?.addSubview(self.pushBanner)
                self.pushBanner.setPushBanner(notificationData: notification)
            }
        }
    }
    
    func showPushBannerforFugu(notification:NotificationDataObject) {
        DispatchQueue.main.async {
            let window = UIApplication.shared.keyWindow
            if self.pushBanner == nil {
                self.pushBanner = UINib(nibName: NIB_NAME.pushBanner, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! PushBanner
                self.pushBanner.delegate = self
                self.pushBanner.frame = CGRect(x: 0, y: -HEIGHT.pushBannerHeight, width: SCREEN_SIZE.width, height: HEIGHT.pushBannerHeight)
                window?.addSubview(self.pushBanner)
                self.pushBanner.setPushBannerForFugu(notificationData: notification)
            }
        }
    }
    
    //MARK: Push Banner DELEGATE METHOD
    func removePushBanner() {
        if pushBanner != nil {
            self.pushBanner.removeFromSuperview()
            self.pushBanner = nil
        }
        if(NetworkingHelper.sharedInstance.notificationArray.count > 0) {
            NetworkingHelper.sharedInstance.notificationArray.remove(at: 0)
        }
        
        if(NetworkingHelper.sharedInstance.notificationArray.count > 0) {
            let notificationData =  NetworkingHelper.sharedInstance.notificationArray[0]
            var notificationType = 0
            if let type = notificationData.flag {
                notificationType = type
            }
            switch(notificationType) {
            case FLAG_TYPE.deleteTask,
                 FLAG_TYPE.reschedule,
                 FLAG_TYPE.taskUpdatedStatus,
                 FLAG_TYPE.upcoming:
                self.showPushBanner(notification: NetworkingHelper.sharedInstance.notificationArray[0])
                break
            case FLAG_TYPE.newTask:
                switch notificationData.accept_button! {
                case ACCEPT_TYPE.startCancel:
                    self.showPushBanner(notification: NetworkingHelper.sharedInstance.notificationArray[0])
                    break
                default:
                   NetworkingHelper.sharedInstance.notificationArray.remove(at: 0)
                }
            case FLAG_TYPE.putOffDuty:
                if notificationData.isAvailable == 1 {
                    Singleton.sharedInstance.fleetDetails.isAvailable = DutyStatus.onDuty
                    UserDefaults.standard.set(DutyStatus.onDuty, forKey: USER_DEFAULT.userAvailableStatus)
                } else {
                    Singleton.sharedInstance.fleetDetails.isAvailable = DutyStatus.offDuty
                    UserDefaults.standard.set(DutyStatus.offDuty, forKey: USER_DEFAULT.userAvailableStatus)
                }
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.putOffDuty), object: nil)
                self.showErrorMessage(error: notificationData.message!, isError: .message)
            default:
                break
            }
        }
    }
    
    func handlingPushAfterGettingAllNotification() {
        if pushBanner != nil {
            self.pushBanner.removeFromSuperview()
            self.pushBanner = nil
        }
        
        if(NetworkingHelper.sharedInstance.notificationArray.count > 0) {
            let notificationData =  NetworkingHelper.sharedInstance.notificationArray[0]
            var notificationType = 0
            if let type = notificationData.flag {
                notificationType = type
            }
            switch(notificationType) {
            case FLAG_TYPE.deleteTask,
                 FLAG_TYPE.reschedule,
                 FLAG_TYPE.taskUpdatedStatus,
                 FLAG_TYPE.upcoming:
                self.showPushBanner(notification: NetworkingHelper.sharedInstance.notificationArray[0])
                break
            case FLAG_TYPE.newTask:
                //self.handlingNewTaskPush(notificationData: notificationData)
                switch notificationData.accept_button! {
                case ACCEPT_TYPE.startCancel:
                    self.showPushBanner(notification: NetworkingHelper.sharedInstance.notificationArray[0])
                    break
                default:
                    self.handlingNewTaskPush(notificationData: NetworkingHelper.sharedInstance.notificationArray[0], fromAcceptTaskNotification: false)
                    NetworkingHelper.sharedInstance.notificationArray.remove(at: 0)
                }
            case FLAG_TYPE.putOffDuty:
                if notificationData.isAvailable == 1 {
                    Singleton.sharedInstance.fleetDetails.isAvailable = DutyStatus.onDuty
                    UserDefaults.standard.set(DutyStatus.onDuty, forKey: USER_DEFAULT.userAvailableStatus)
                } else {
                    Singleton.sharedInstance.fleetDetails.isAvailable = DutyStatus.offDuty
                    UserDefaults.standard.set(DutyStatus.offDuty, forKey: USER_DEFAULT.userAvailableStatus)
                }
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.putOffDuty), object: nil)
                self.showErrorMessage(error: notificationData.message!, isError: .message)
            default:
                break
            }
        }
    }
    
    func handlingNewTaskPush(notificationData:NotificationDataObject, fromAcceptTaskNotification:Bool) {
        NetworkingHelper.sharedInstance.serverTime = "0"
        let vibrationsEnabled = UserDefaults.standard.value(forKey: USER_DEFAULT.vibration) as! Bool
        if vibrationsEnabled{
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        }
        
        if notificationData.incomplete_data == 1 {
            Singleton.sharedInstance.showNewTaskScreen(fromPush: false)
        } else {
            guard (notificationData.jobs?.count)! > 0 else {
                if(NetworkingHelper.sharedInstance.syncView == nil) {
                    NetworkingHelper.sharedInstance.notificationCount += 1
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotificationCount), object: self)
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotification), object: self)
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.getAllNotification), object: self)
                }
                return
            }
            var taskArray = [TasksAssignedToDate]()
            for item in notificationData.jobs! {
                let task = TasksAssignedToDate(json: item)
                task.accept_button = notificationData.accept_button
                task.just_now = 1
                task.time_left_in_seconds = Int(notificationData.timer!)
                task.acceptOptionalField.value = notificationData.accept_button!
                task.jobStatus = JOB_STATUS.assigned
                task.jobId = notificationData.job_id
                task.vertical = notificationData.isTaxi
                task.metaData = notificationData.m
                if task.jobType == JOB_TYPE.appointment || task.jobType == JOB_TYPE.fieldWorkforce {
                    task.jobPickupDatetime = notificationData.start_time
                    task.jobDeliveryDatetime = notificationData.end_time
                    task.jobAddress = notificationData.end_address
                }
                taskArray.append(task)
            }
            NetworkingHelper.sharedInstance.newTaskListArray.insert(taskArray, at: 0)
            if fromAcceptTaskNotification == false {
                Singleton.sharedInstance.showNewTaskScreen(fromPush: true)
            }
        }
    }
    /*========================================================================================================*/
    
    func getAttributedStatusText(jobStatus :Int, appOptionalField : Int) -> NSMutableAttributedString {
        var attributedText:NSMutableAttributedString!
        switch jobStatus{
        case JOB_STATUS.unassigned:
            attributedText = NSMutableAttributedString(string: "  \(TEXT.UNASSIGNED)", attributes: [NSForegroundColorAttributeName:COLOR.UNASSIGNED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        case JOB_STATUS.assigned:
            attributedText = NSMutableAttributedString(string: "  \(TEXT.ASSIGNED)", attributes: [NSForegroundColorAttributeName:COLOR.ASSIGNED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        case JOB_STATUS.started:
            attributedText = NSMutableAttributedString(string: "  \(TEXT.STARTED)", attributes: [NSForegroundColorAttributeName:COLOR.STARTED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        case JOB_STATUS.arrived:
            attributedText = NSMutableAttributedString(string: "  \(TEXT.ARRIVED)", attributes: [NSForegroundColorAttributeName:COLOR.ARRIVED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        case JOB_STATUS.successful:
            attributedText = NSMutableAttributedString(string: "  \(TEXT.SUCCESSFUL)", attributes: [NSForegroundColorAttributeName:COLOR.SUCCESSFUL_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        case JOB_STATUS.failed:
            attributedText = NSMutableAttributedString(string: "  \(TEXT.FAILED)", attributes: [NSForegroundColorAttributeName:COLOR.FAILED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        case JOB_STATUS.canceled:
            attributedText = NSMutableAttributedString(string: "  \(TEXT.CANCELED)", attributes: [NSForegroundColorAttributeName:COLOR.CANCELED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        case JOB_STATUS.accepted:
            if appOptionalField == 1{
                attributedText = NSMutableAttributedString(string: "  \(TEXT.ACCEPTED)", attributes: [NSForegroundColorAttributeName:COLOR.ACCEPTED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
            }else{
                attributedText = NSMutableAttributedString(string: "  \(TEXT.ACKNOWLEDGED)", attributes: [NSForegroundColorAttributeName:COLOR.ACCEPTED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
            }
        case JOB_STATUS.declined:
            attributedText = NSMutableAttributedString(string: "  \(TEXT.DECLINED)", attributes: [NSForegroundColorAttributeName:COLOR.DECLINED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        default:
            attributedText = NSMutableAttributedString(string: "")
            break
        }
        return attributedText
    }
    
    func isAppSyncingEnable() -> Bool {
        switch UserDefaults.standard.integer(forKey: USER_DEFAULT.appCaching) {
        case APP_CACHING.ON:
            return true
        default:
            return false
        }
    }
    
    func getColor(jobStatus :Int) -> UIColor {
        switch jobStatus{
        case JOB_STATUS.unassigned:
            return COLOR.UNASSIGNED_COLOR
        case JOB_STATUS.assigned:
            return COLOR.ASSIGNED_COLOR
        case JOB_STATUS.started:
            return COLOR.STARTED_COLOR
        case JOB_STATUS.arrived:
            return COLOR.ARRIVED_COLOR
        case JOB_STATUS.successful:
            return COLOR.SUCCESSFUL_COLOR
        case JOB_STATUS.failed:
            return COLOR.FAILED_COLOR
        case JOB_STATUS.canceled:
            return COLOR.CANCELED_COLOR
        case JOB_STATUS.accepted:
            return COLOR.ACCEPTED_COLOR
        case JOB_STATUS.declined:
            return COLOR.DECLINED_COLOR
        default:
            return COLOR.ASSIGNED_COLOR
        }
    }
    
    
    
    func getUTCDateString() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation:  "UTC")
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    func convertDateToString() -> String {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.timeZone = TimeZone(abbreviation:  "UTC")
        styler.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        return styler.string(from: Date())
    }
    
    func isFileExistAtPath(_ path:String) -> Bool {
        //  print(path)
        let filePath = self.createPath(path)
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: filePath)
    }
    
    func createPath(_ imageName:String) -> String {
        let fileManager = FileManager.default
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        let imageDirUrl = url.appendingPathComponent("Image")
        var isDirectory:ObjCBool = false
        if fileManager.fileExists(atPath: imageDirUrl.path, isDirectory: &isDirectory) == false {
            do {
                try FileManager.default.createDirectory(atPath: imageDirUrl.path, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        let filePath = imageDirUrl.appendingPathComponent(imageName).path
        return filePath
    }
    
    func saveImageToDocumentDirectory(_ imageName:String, image:UIImage) -> String {
        let filePath = self.createPath(imageName)
        let pngImageData = UIImageJPEGRepresentation(image, 0.1)
        let fileManager = FileManager.default
        if(fileManager.fileExists(atPath: filePath) == true) {
            do {
                try fileManager.removeItem(atPath: filePath)
                NetworkingHelper.sharedInstance.allImagesCache.removeObject(forKey: "\(imageName)" as NSString)
            } catch let error as NSError {
                print("Error in removing Path = \(error.description)")
            }
            
        }
        try? pngImageData!.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
        return imageName
    }
    
    func deleteImageFromDocumentDirectory(_ imagePath:String) {
        // print(imagePath)
        let fileManager = FileManager.default
        let filePath = self.createPath(imagePath)
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print("Error in removing Path = \(error.description)")
        }
    }
    
    func getDirectionMode() -> String {
        var directionMode = "driving"
        switch (Singleton.sharedInstance.fleetDetails.transportType)! {
        case "1", "2", "4", "6":
            directionMode = "driving"
            break
        case "3":
            directionMode = "bicycling"
            break
        case  "5":
            directionMode = "walking"
            break
        default:
            directionMode = "driving"
            break
        }
        return directionMode
    }
    
    func getNavigationAppName() -> String {
        if let navigationMap = UserDefaults.standard.value(forKey: USER_DEFAULT.navigationMap) as? Int{
            if(navigationMap == NAVIGATION_MAP.googleMap) {
                return "Google Maps"
            } else if navigationMap == NAVIGATION_MAP.waze{
                return "Waze"
            } else {
                return "Apple Map"
            }
        } else {
            return "Google Maps"
        }
    }
    
    func getNavigationAddress(destinationCoordinate:CLLocationCoordinate2D, directionMode:String) -> String {
        if let navigationMap = UserDefaults.standard.value(forKey: USER_DEFAULT.navigationMap) as? Int{
            if(navigationMap == NAVIGATION_MAP.googleMap) {
                return "comgooglemaps://?daddr=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)&directionsmode=\(directionMode)"
            } else {
                return "waze://?ll=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)&navigate=yes"
            }
        } else {
            return "comgooglemaps://?daddr=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)&directionsmode=\(directionMode)"
        }

    }
    
    
    func isFilterApplied() -> Bool {
        
        if UserDefaults.standard.object(forKey: USER_DEFAULT.filterDictionary) != nil {
            if let decoded  = UserDefaults.standard.object(forKey: USER_DEFAULT.filterDictionary) as? Data {
                Singleton.sharedInstance.filterStatusArray = (NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [FilterStatus]) ?? [FilterStatus]()
            }
        }
        
        if UserDefaults.standard.object(forKey: USER_DEFAULT.sortingDictionary) != nil {
            if let decoded  = UserDefaults.standard.object(forKey: USER_DEFAULT.sortingDictionary) as? Data {
                Singleton.sharedInstance.sortingDictionary = (NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [FilterStatus]) ?? [FilterStatus]()
            }
        }
        
        if UserDefaults.standard.object(forKey: USER_DEFAULT.templateDictionary) != nil {
            if let decoded  = UserDefaults.standard.object(forKey: USER_DEFAULT.templateDictionary) as? Data {
                let templateFilterStatusArray = (NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [FilterStatus]) ?? [FilterStatus]()
                if templateFilterStatusArray.count > 0{
                    if templateFilterStatusArray[0].layoutType == UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType) {
                        Singleton.sharedInstance.templateDictionary = templateFilterStatusArray
                    }
                }
            }
        }
        
        if let searchedTextFromDefaults = UserDefaults.standard.string(forKey: USER_DEFAULT.searchTextForFilter)  {
//            if let decoded  = UserDefaults.standard.object(forKey: USER_DEFAULT.filterDictionary) as? Data {
                Singleton.sharedInstance.searchedText = searchedTextFromDefaults
//            }
        }
        
        for filter in Singleton.sharedInstance.filterStatusArray {
            if filter.isSelected == true {
                return true
            }
        }
        
        for template in Singleton.sharedInstance.templateDictionary {
            if template.isSelected == true {
                return true
            }
        }
        
        for sorting in Singleton.sharedInstance.sortingDictionary {
            if sorting.isSelected == true {
                return true
            }
        }
        
        guard self.searchedText.length == 0 else {
            return true
        }
        return false
    }
    
    func getRuntimeTutorialCase() ->(Bool,TUTORIAL_START_POINT?) {
        if NetworkingHelper.sharedInstance.dayTasksListArray.count == 0 {
            return self.getCaseForZeroTask()
        } else {
            return self.getCaseForHavingTask()
        }
    }
    
    func getCaseForZeroTask() -> (Bool,TUTORIAL_START_POINT?) {
        if NetworkingHelper.sharedInstance.newTaskListArray.count == 0 {
            return self.getCaseForZeroNewTaskZeroTask()
        } else {
            return self.getCaseForHavingNewTaskWithZeroTask()
        }
    }
    
    func getCaseForZeroNewTaskZeroTask() -> (Bool,TUTORIAL_START_POINT?) {
        if UserDefaults.standard.bool(forKey: USER_DEFAULT.allClearTutorial) == false {
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.allClearTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyAssignedTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.newTaskTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.fullTutorial)
            return (true,TUTORIAL_START_POINT.ALL_CLEAR)
        } else {
            return (false,nil)
        }
    }
    
    func getCaseForHavingNewTaskWithZeroTask() -> (Bool,TUTORIAL_START_POINT?) {
        if UserDefaults.standard.bool(forKey: USER_DEFAULT.newTaskTutorial) == true {
            if UserDefaults.standard.bool(forKey: USER_DEFAULT.onlyNewTaskTutorial) == true {
                return (false,nil)
            } else {
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyNewTaskTutorial)
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.newTaskTutorial)
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.fullTutorial)
                return (true, .ONLY_NEW_TASK)
            }
        } else {
            if UserDefaults.standard.bool(forKey: USER_DEFAULT.allClearTutorial) == true {
                if UserDefaults.standard.bool(forKey: USER_DEFAULT.onlyNewTaskTutorial) == true {
                    return (false,nil)
                } else {
                    UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyNewTaskTutorial)
                    UserDefaults.standard.set(true, forKey: USER_DEFAULT.newTaskTutorial)
                    UserDefaults.standard.set(true, forKey: USER_DEFAULT.fullTutorial)
                    return (true, .ONLY_NEW_TASK)
                }
            } else {
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyNewTaskTutorial)
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.newTaskTutorial)
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.allClearTutorial)
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyAssignedTutorial)
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.fullTutorial)
                return (true, .NEW_TASK)
            }
        }
    }
    
    func getCaseForHavingTask() -> (Bool,TUTORIAL_START_POINT?) {
        if NetworkingHelper.sharedInstance.newTaskListArray.count == 0 {
            return self.getCaseForZeroNewTaskWithTask()
        } else {
            return self.getCaseForHavingBothTasks()
        }
    }
    
    
    func getCaseForZeroNewTaskWithTask() -> (Bool,TUTORIAL_START_POINT?) {
        if UserDefaults.standard.bool(forKey: USER_DEFAULT.onlyAssignedTutorial) == true {
            if UserDefaults.standard.bool(forKey: USER_DEFAULT.afterAcceptTutorial) == true {
                return (false,nil)
            } else {
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.afterAcceptTutorial)
                return (true, .AFTER_ACCEPT)
            }
        } else {
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.newTaskTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.allClearTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyAssignedTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.fullTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.afterAcceptTutorial)
            return (true, .ONLY_ASSIGNED)
        }
    }
    
    func getCaseForHavingBothTasks() -> (Bool,TUTORIAL_START_POINT?) {
        if UserDefaults.standard.bool(forKey: USER_DEFAULT.fullTutorial) == true {
            if UserDefaults.standard.bool(forKey: USER_DEFAULT.onlyAssignedTutorial) == true {
                if UserDefaults.standard.bool(forKey: USER_DEFAULT.newTaskTutorial) == true {
                    if UserDefaults.standard.bool(forKey: USER_DEFAULT.onlyNewTaskTutorial) == true {
                        if UserDefaults.standard.bool(forKey: USER_DEFAULT.afterAcceptTutorial) == true {
                            return (false,nil)
                        } else {
                            UserDefaults.standard.set(true, forKey: USER_DEFAULT.afterAcceptTutorial)
                            return (true, .AFTER_ACCEPT)
                        }
                    } else {
                        UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyNewTaskTutorial)
                        return (true, .ONLY_NEW_TASK)
                    }
                }
                return (false,nil)
            }
        } else {
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.newTaskTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.allClearTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyAssignedTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.fullTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.afterAcceptTutorial)
            UserDefaults.standard.set(true, forKey: USER_DEFAULT.onlyNewTaskTutorial)
            return (true, .FULL_TUTORIAL)
        }
        return (false,nil)
    }
    
    func getVersion() -> String {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return ""
    }
    
    func removeAcceptedTaskFromNewTask() {
        guard NetworkingHelper.sharedInstance.newTaskListArray.count > 0 else {
            return
        }
        
        for i in 0..<NetworkingHelper.sharedInstance.newTaskListArray.count {
            var newTaskArray = NetworkingHelper.sharedInstance.newTaskListArray[i]
            for j in 0..<newTaskArray.count {
                let task = newTaskArray[j]
                if task.jobId == Singleton.sharedInstance.selectedTaskDetails.jobId {
                    NetworkingHelper.sharedInstance.newTaskListArray.remove(at: i)
                    return
                }
            }
        }
    }
    
    func changeStatusForAllRelatedTask(jobStatus:Int) {
        if Singleton.sharedInstance.selectedTaskDetails.isRouted == 0 {
            if Singleton.sharedInstance.selectedTaskRelatedArray.count > 0 {
                for i in 0..<Singleton.sharedInstance.selectedTaskRelatedArray.count {
                    if Singleton.sharedInstance.selectedTaskRelatedArray[i].jobId != Singleton.sharedInstance.selectedTaskDetails.jobId {
                        Singleton.sharedInstance.selectedTaskRelatedArray[i].jobStatus = jobStatus
                    }
                }
            }
        }
    }
    
    func cancelAllURLRequest() {
        if #available(iOS 9.0, *) {
            URLSession.shared.getAllTasks { (requestedTaskList: [URLSessionTask]) in
                for task in requestedTaskList {
                    if task.originalRequest?.url?.lastPathComponent != API_NAME.update_fleet_location {
                        task.cancel()
                    }
                }
            }
        }
    }
    
    func getCountOfInProgressHit(receivedCount:@escaping (_ count:Int) -> ()) {
        if #available(iOS 9.0, *) {
            URLSession.shared.getAllTasks { (requestedTaskList: [URLSessionTask]) in
                let count = requestedTaskList.count
                receivedCount(count)
            }
        }
    }
    
    func checkForTeamEnabled() -> Bool {
        guard self.fleetDetails.isTeamEnabled == 1 else {
            return false
        }
        guard (self.fleetDetails.teamsArray?.count)! > 0 else {
            return false
        }
        return true
    }
    
    func showTeamsEnabled() -> Bool {
        guard self.fleetDetails.showTeams == 1 else {
            return false
        }
        
        guard (self.fleetDetails.teamsArray?.count)! > 0 else {
            return false
        }
        
        return true
    }
    
    func checkForTagsEnabled() -> Bool {
        guard self.fleetDetails.isTagsEnabled == 1 else {
            return false
        }
        guard (self.fleetDetails.tagsArray?.count)! > 0 else {
            return false
        }
        return true
    }
    
    func initilizeFugu() {
       // Auxillary.showAlert("\(self.fleetDetails.fugu_token)")
//        guard self.fleetDetails != nil else {
//            
//            return
//        }
        FuguConfig.shared.setCredential(withToken: self.fleetDetails.fugu_token!, referenceId: self.fleetDetails.userId!, appType: "1")
        
        let driverId = "driver + \(self.fleetDetails.fleetId!)"
        let fuguUserDetail = FuguUserDetail(fullName: self.fleetDetails.name!, email: self.fleetDetails.email!, phoneNumber: self.fleetDetails.phone!, userUniqueKey: driverId)
        FuguConfig.shared.updateUserDetail(userDetail: fuguUserDetail)
        
    }
    
    func setFuguTheme() {
        let fuguTheme = FuguTheme.defaultTheme()
        fuguTheme.headerTextFont = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
        fuguTheme.leftBarButtonImage = #imageLiteral(resourceName: "back_btn")
        fuguTheme.headerBackgroundColor = UIColor.black
        fuguTheme.headerTextColor = COLOR.positiveButtonTitleColor
        FuguConfig.shared.setCustomisedFuguTheme(theme: fuguTheme)
        if APP_STORE.value == 0 {
            FuguConfig.shared.switchEnvironment(.dev)
        }
    }
    
    func getBottomInsetofSafeArea() -> CGFloat {
        return UIApplication.shared.keyWindow!.safeAreaInsetsForAllOS.bottom
    }
    
    func getTopInsetofSafeArea() -> CGFloat {
        let topInset = UIApplication.shared.keyWindow!.safeAreaInsetsForAllOS.top
        if topInset > 0 {
            return topInset - 20
        }
        return topInset
    }
    
    func isNotificationEnabled(completion:@escaping (_ enabled:Bool)->()){
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings: UNNotificationSettings) in
                let status =  (settings.authorizationStatus == .authorized)
                completion(status)
            })
        } else {
            if let status = UIApplication.shared.currentUserNotificationSettings?.types{
                let status = status.rawValue != UIUserNotificationType(rawValue: 0).rawValue
                completion(status)
            }else{
                completion(false)
            }
        }
    }
    
    func isWhiteLabelUserWithDeviceTokenCheck() -> Bool {
        let deviceToken =  UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) != nil ? UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) as? String : "No deviceToken"
        if(APP_STORE.value == 1) {
            if DEVICE_TYPE == "1" || DEVICE_TYPE == "3" {
                if(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String != SERVER.dev) {
                    if deviceToken?.characters.count != 64 || deviceToken == "No deviceToken" {
                        return true
                    }
                }
            }
        }
        return false
    }
    
}


