//
//  JobsMonthlyData.swift
//  Tookan
//
//  Created by Click Labs on 7/28/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation

class TasksAssignedToDate: NSObject, NSCoding {
    
    var customerEmail : String!
    var customerPhone : String!
    var customerUsername :  String!
    var hasFieldInput : Int!
    var hasPickup : Int!
    var isRouted:Int!
    var jobAddress: String!
    var jobDeliveryDatetime : String!
    var jobDeliveryDateTimeInTwelveFormat:String!
    var jobDescription : String!
    var jobId :Int!
    var userId:Int!
    var jobLongitude : String!
    var jobLatitude : String!
    var jobPickupAddress : String!
    var jobPickupDatetime: String!
    var jobPickupDateTimeInTwelveHoursFormat:String!
    var jobPickupEmail : String!
    var jobPickupLatitude : String!
    var jobPickupLongitude : String!
    //var jobPickupPhone : String!
    var jobPickupName : String!
    var jobStatus : Int!
    var jobTime : String!
    var jobTimeInTwelveHoursFormat:String!
    var jobType : Int!
    var deliveryJobId:Int!
    var pickupJobId:Int!
    var pickupDeliveryRelationship : String!
    //    var sessionId: Int!
    var taskHistoryArray: [TaskHistory]?
    var appOptionalFieldArray: Array<AppOptionalFields>?
   
    var acceptOptionalField = AppOptionalFields(json:[:])
    var noteOptionalField = AppOptionalFields(json:[:])
    var imageOptionalField = AppOptionalFields(json:[:])
    var signatureOptionalField = AppOptionalFields(json:[:])
    var arrivedOptionalField = AppOptionalFields(json:[:])
    var sliderOptionalField = AppOptionalFields(json:[:])
    var confirmOptionalField = AppOptionalFields(json:[:])
    var fleetNotificationField = AppOptionalFields(json:[:])
    var maskingOptionalField = AppOptionalFields(json:[:])
    var scannerOptionalField = AppOptionalFields(json: [:])
    var hideCancelOptionalField = AppOptionalFields(json: [:])
    var hideFailedOptionalField = AppOptionalFields(json: [:])
    var captionOptionalField = AppOptionalFields(json: [:])
    
    var addedNotesArray: [TaskHistory]? = [TaskHistory]()
    var deletedNotesArray = [TaskHistory]()
    var addedImagesArray: [TaskHistory]?
    var addedCaptionArray:[Caption]? = [Caption]()
    var addedSignatureArray: [TaskHistory]?
    var addedScannerArray:([TaskHistory])?
    var deletedScannerArray = [TaskHistory]()
    var customFieldArray: Array<CustomFields>?
    var referenceImageArray = NSArray()
    var referenceImagesCaptionArray:[Caption]? = [Caption]()
    //var jobCompleteStatus:Int? = 1
    var invoiceHtml = ""
    var imageUploadingStatus:Bool? = false
    var imageDeletingStatus:Bool? = false
    var travelledDistance:Double?
    var travelledTime:Double? = 0.0
    var vertical:Int? = 0
    var related_job_count:Int?
    var related_job_type:Int?
    var related_job_indexes = [Any]()
    var just_now:Int?
    var time_left_in_seconds:Int?
    var jobCreationTime = Date()
    var accept_button:Int?
    var metaData:String? = ""
    var driverJobTotal:String? = ""
    var customerJobTotal:String? = ""
    var customerRating:Int? = 0
    var customerComment:String? = ""
    var isRatingEnabled:Bool? = false
    
    required init?(coder aDecoder: NSCoder) {
        customerEmail = aDecoder.decodeObject(forKey: "customerEmail") as! String
        customerPhone = aDecoder.decodeObject(forKey: "customerPhone") as! String
        customerUsername = aDecoder.decodeObject(forKey: "customerUsername") as! String
        hasFieldInput = aDecoder.decodeObject(forKey: "hasFieldInput") as! Int
        hasPickup = aDecoder.decodeObject(forKey: "hasPickup") as! Int
        isRouted = aDecoder.decodeObject(forKey: "isRouted") as! Int
        jobAddress = aDecoder.decodeObject(forKey: "jobAddress") as! String
        jobDeliveryDatetime = aDecoder.decodeObject(forKey: "jobDeliveryDatetime") as! String
        jobDeliveryDateTimeInTwelveFormat = aDecoder.decodeObject(forKey: "jobDeliveryDateTimeInTwelveFormat") as! String
        jobDescription = aDecoder.decodeObject(forKey: "jobDescription") as! String
        jobId = aDecoder.decodeObject(forKey: "jobId") as! Int
        userId = aDecoder.decodeObject(forKey: "userId") as! Int
        jobLongitude = aDecoder.decodeObject(forKey: "jobLongitude") as! String
        jobLatitude = aDecoder.decodeObject(forKey: "jobLatitude") as! String
        jobPickupAddress = aDecoder.decodeObject(forKey: "jobPickupAddress") as! String
        jobPickupDatetime = aDecoder.decodeObject(forKey: "jobPickupDatetime") as! String
        jobPickupDateTimeInTwelveHoursFormat = aDecoder.decodeObject(forKey: "jobPickupDateTimeInTwelveHoursFormat") as! String
        jobPickupEmail = aDecoder.decodeObject(forKey: "jobPickupEmail") as! String
        jobPickupLatitude = aDecoder.decodeObject(forKey: "jobPickupLatitude") as! String
        jobPickupLongitude = aDecoder.decodeObject(forKey: "jobPickupLongitude") as! String
        //jobPickupPhone = aDecoder.decodeObjectForKey("jobPickupPhone") as! String
        jobPickupName = aDecoder.decodeObject(forKey: "jobPickupName") as! String
        jobStatus = aDecoder.decodeObject(forKey: "jobStatus") as! Int
        jobTime = aDecoder.decodeObject(forKey: "jobTime") as! String
        jobTimeInTwelveHoursFormat = aDecoder.decodeObject(forKey: "jobTimeInTwelveHoursFormat") as! String
        jobType = aDecoder.decodeObject(forKey: "jobType") as! Int
        deliveryJobId = aDecoder.decodeObject(forKey: "deliveryJobId") as! Int
        pickupDeliveryRelationship = aDecoder.decodeObject(forKey: "pickupDeliveryRelationship") as! String
        taskHistoryArray = aDecoder.decodeObject(forKey: "taskHistoryArray") as? [TaskHistory]
        acceptOptionalField = (aDecoder.decodeObject(forKey: "acceptOptionalField") as? AppOptionalFields)!
        noteOptionalField = (aDecoder.decodeObject(forKey: "noteOptionalField") as? AppOptionalFields)!
        imageOptionalField = (aDecoder.decodeObject(forKey: "imageOptionalField") as? AppOptionalFields)!
        signatureOptionalField = (aDecoder.decodeObject(forKey: "signatureOptionalField") as? AppOptionalFields)!
        sliderOptionalField = (aDecoder.decodeObject(forKey: "sliderOptionalField") as? AppOptionalFields)!
        confirmOptionalField = (aDecoder.decodeObject(forKey: "confirmOptionalField") as? AppOptionalFields)!
        fleetNotificationField = (aDecoder.decodeObject(forKey: "fleetNotificationField") as? AppOptionalFields)!
        maskingOptionalField = (aDecoder.decodeObject(forKey: "maskingOptionalField") as? AppOptionalFields)!
        arrivedOptionalField = (aDecoder.decodeObject(forKey: "arrivedOptionalField") as? AppOptionalFields)!
        appOptionalFieldArray = aDecoder.decodeObject(forKey: "appOptionalFieldArray") as? Array<AppOptionalFields>
        addedNotesArray = aDecoder.decodeObject(forKey: "addedNotesArray") as? [TaskHistory]
        deletedNotesArray = (aDecoder.decodeObject(forKey: "deletedNotesArray") as? [TaskHistory])!
        addedImagesArray = aDecoder.decodeObject(forKey: "addedImagesArray") as? [TaskHistory]
        addedSignatureArray = aDecoder.decodeObject(forKey: "addedSignatureArray") as? [TaskHistory]
        customFieldArray = aDecoder.decodeObject(forKey: "customFieldArray") as? Array<CustomFields>
        referenceImageArray = aDecoder.decodeObject(forKey: "referenceImageArray") as! NSArray
//        jobCompleteStatus = aDecoder.decodeObject(forKey: "jobCompleteStatus") as? Int
        invoiceHtml = aDecoder.decodeObject(forKey: "invoiceHtml") as! String
        imageUploadingStatus = aDecoder.decodeObject(forKey: "imageUploadingStatus") as? Bool
        imageDeletingStatus = aDecoder.decodeObject(forKey: "imageDeletingStatus") as? Bool
        pickupJobId = aDecoder.decodeObject(forKey: "pickupJobId") as! Int
        scannerOptionalField = (aDecoder.decodeObject(forKey: "scannerOptionalField") as? AppOptionalFields)!
        addedScannerArray = aDecoder.decodeObject(forKey: "addedScannerArray") as? [TaskHistory]
        deletedScannerArray = (aDecoder.decodeObject(forKey: "deletedScannerArray") as? [TaskHistory])!
        hideCancelOptionalField = (aDecoder.decodeObject(forKey: "hideCancelOptionalField") as? AppOptionalFields)!
        hideFailedOptionalField = (aDecoder.decodeObject(forKey: "hideFailedOptionalField") as? AppOptionalFields)!
        travelledDistance = aDecoder.decodeObject(forKey: "travelledDistance") as? Double
        travelledTime = aDecoder.decodeObject(forKey: "travelledTime") as? Double
        vertical = aDecoder.decodeObject(forKey: "vertical") as? Int
        related_job_count = aDecoder.decodeObject(forKey: "related_job_count") as? Int
        related_job_type = aDecoder.decodeObject(forKey: "related_job_type") as? Int
        just_now = aDecoder.decodeObject(forKey: "just_now") as? Int
        time_left_in_seconds = aDecoder.decodeObject(forKey: "time_left_in_seconds") as? Int
        accept_button = aDecoder.decodeObject(forKey: "accept_button") as? Int
        related_job_indexes = (aDecoder.decodeObject(forKey: "related_job_indexes") as? [Any])!
        metaData = aDecoder.decodeObject(forKey: "metaData") as? String
        driverJobTotal = aDecoder.decodeObject(forKey: "driverJobTotal") as? String
        customerJobTotal = aDecoder.decodeObject(forKey: "customerJobTotal") as? String
        customerRating = aDecoder.decodeObject(forKey: "customerRating") as? Int
        customerComment = aDecoder.decodeObject(forKey: "customerComment") as? String
        isRatingEnabled = aDecoder.decodeObject(forKey: "isRatingEnabled") as? Bool
        captionOptionalField = (aDecoder.decodeObject(forKey: "captionOptionalField") as? AppOptionalFields)!
        addedCaptionArray = aDecoder.decodeObject(forKey: "addedCaptionArray") as? [Caption]
        referenceImagesCaptionArray = aDecoder.decodeObject(forKey: "referenceImagesCaptionArray") as? [Caption]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(customerEmail, forKey: "customerEmail")
        aCoder.encode(customerPhone, forKey: "customerPhone")
        aCoder.encode(customerUsername, forKey: "customerUsername")
        aCoder.encode(hasFieldInput, forKey: "hasFieldInput")
        aCoder.encode(hasPickup, forKey: "hasPickup")
        aCoder.encode(isRouted, forKey: "isRouted")
        aCoder.encode(jobAddress, forKey: "jobAddress")
        aCoder.encode(jobDeliveryDatetime, forKey: "jobDeliveryDatetime")
        aCoder.encode(jobDeliveryDateTimeInTwelveFormat, forKey: "jobDeliveryDateTimeInTwelveFormat")
        aCoder.encode(jobDescription, forKey: "jobDescription")
        aCoder.encode(jobId, forKey: "jobId")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(jobLongitude, forKey: "jobLongitude")
        aCoder.encode(jobLatitude, forKey: "jobLatitude")
        aCoder.encode(jobPickupAddress, forKey: "jobPickupAddress")
        aCoder.encode(jobPickupDatetime, forKey: "jobPickupDatetime")
        aCoder.encode(jobPickupDateTimeInTwelveHoursFormat, forKey: "jobPickupDateTimeInTwelveHoursFormat")
        aCoder.encode(jobPickupEmail, forKey: "jobPickupEmail")
        aCoder.encode(jobPickupLatitude, forKey: "jobPickupLatitude")
        aCoder.encode(jobPickupLongitude, forKey: "jobPickupLongitude")
       // aCoder.encodeObject(jobPickupPhone, forKey: "jobPickupPhone")
        aCoder.encode(jobPickupName, forKey: "jobPickupName")
        aCoder.encode(jobStatus, forKey: "jobStatus")
        aCoder.encode(jobTime, forKey: "jobTime")
        aCoder.encode(jobTimeInTwelveHoursFormat, forKey: "jobTimeInTwelveHoursFormat")
        aCoder.encode(jobType, forKey: "jobType")
        aCoder.encode(deliveryJobId, forKey: "deliveryJobId")
        aCoder.encode(pickupDeliveryRelationship, forKey: "pickupDeliveryRelationship")
        aCoder.encode(taskHistoryArray, forKey: "taskHistoryArray")
      
        aCoder.encode(acceptOptionalField, forKey: "acceptOptionalField")
        aCoder.encode(sliderOptionalField, forKey: "sliderOptionalField")
        aCoder.encode(confirmOptionalField, forKey: "confirmOptionalField")
        aCoder.encode(arrivedOptionalField, forKey: "arrivedOptionalField")
        aCoder.encode(noteOptionalField, forKey: "noteOptionalField")
        aCoder.encode(imageOptionalField, forKey: "imageOptionalField")
        aCoder.encode(signatureOptionalField, forKey: "signatureOptionalField")
        aCoder.encode(fleetNotificationField, forKey: "fleetNotificationField")
        aCoder.encode(maskingOptionalField, forKey: "maskingOptionalField")
        aCoder.encode(appOptionalFieldArray, forKey: "appOptionalFieldArray")
        
        aCoder.encode(addedNotesArray, forKey: "addedNotesArray")
        aCoder.encode(deletedNotesArray, forKey: "deletedNotesArray")
        aCoder.encode(addedImagesArray, forKey: "addedImagesArray")
        aCoder.encode(addedSignatureArray, forKey: "addedSignatureArray")
        aCoder.encode(customFieldArray, forKey: "customFieldArray")
        aCoder.encode(referenceImageArray, forKey: "referenceImageArray")
       // aCoder.encode(jobCompleteStatus, forKey: "jobCompleteStatus")
        aCoder.encode(invoiceHtml, forKey: "invoiceHtml")
        aCoder.encode(imageUploadingStatus, forKey: "imageUploadingStatus")
        aCoder.encode(imageDeletingStatus, forKey: "imageDeletingStatus")
        aCoder.encode(pickupJobId, forKey: "pickupJobId")
        aCoder.encode(scannerOptionalField, forKey: "scannerOptionalField")
        aCoder.encode(addedScannerArray, forKey: "addedScannerArray")
        aCoder.encode(deletedScannerArray, forKey: "deletedScannerArray")
        aCoder.encode(hideCancelOptionalField, forKey: "hideCancelOptionalField")
        aCoder.encode(hideFailedOptionalField, forKey: "hideFailedOptionalField")
        aCoder.encode(travelledDistance, forKey: "travelledDistance")
        aCoder.encode(travelledTime, forKey: "travelledTime")
        aCoder.encode(vertical, forKey: "vertical")
        aCoder.encode(related_job_count, forKey: "related_job_count")
        aCoder.encode(related_job_type, forKey: "related_job_type")
        aCoder.encode(just_now, forKey: "just_now")
        aCoder.encode(time_left_in_seconds, forKey: "time_left_in_seconds")
        aCoder.encode(accept_button, forKey: "accept_button")
        aCoder.encode(related_job_indexes, forKey: "related_job_indexes")
        aCoder.encode(metaData, forKey: "metaData")
        aCoder.encode(driverJobTotal, forKey: "driverJobTotal")
        aCoder.encode(customerJobTotal, forKey: "customerJobTotal")
        aCoder.encode(customerRating, forKey: "customerRating")
        aCoder.encode(customerComment, forKey: "customerComment")
        aCoder.encode(isRatingEnabled, forKey: "isRatingEnabled")
        aCoder.encode(captionOptionalField, forKey: "captionOptionalField")
        aCoder.encode(addedCaptionArray, forKey: "addedCaptionArray")
        aCoder.encode(referenceImagesCaptionArray, forKey: "referenceImagesCaptionArray")
    }
    
    init(json:[String:Any]) {
        
        if let customerEmail = json["customer_email"] as? String{
            self.customerEmail = customerEmail
        } else {
            customerEmail = ""
        }
        
        if let customerPhone = json["customer_phone"] as? String{
            let trimmedString: String = customerPhone.replacingOccurrences( of: " ", with: "" ) as String
            if(trimmedString.length > 1) {
                self.customerPhone = trimmedString
            } else {
                if let jobPickupPhone = json["job_pickup_phone"] as? String{
                    let trimmedString: String = jobPickupPhone.replacingOccurrences( of: " ", with: "" ) as String
                    if(trimmedString.length > 1) {
                        self.customerPhone = trimmedString
                    } else {
                        self.customerPhone = ""
                    }
                } else {
                    self.customerPhone = ""
                }
            }
        } else {
            customerPhone = ""
        }
        
        if let customerUsername = json["customer_username"] as? String{
            self.customerUsername = customerUsername
        } else {
            customerUsername = ""
        }
        
        if let hasFieldInput = json["has_field_input"] as? Int{
            self.hasFieldInput = hasFieldInput
        } else {
            self.hasFieldInput = 0
        }
        
        if let hasPickup = json["has_pickup"] as? Int{
            self.hasPickup = hasPickup
        } else {
            hasPickup = 0
        }
        
        if let isRouted = json["is_routed"] as? Int{
            self.isRouted = isRouted
        } else {
            self.isRouted = 0
        }
        
        if let jobId = json["job_id"] as? Int{
            self.jobId = jobId
        } else {
            self.jobId = 0
        }
        
        if let userId = json["user_id"] as? Int{
            self.userId = userId
        } else {
            self.userId = 0
        }
        
        if let jobStatus = json["job_status"] as? Int{
            self.jobStatus = jobStatus
        }
        
        if let jobTime = json["job_time"] as? String{
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.jobTime = jobTime
            let time = dateFormatter.date(from: self.jobTime)
            dateFormatter.dateFormat = "hh:mm a"
            self.jobTimeInTwelveHoursFormat = time != nil ? dateFormatter.string(from: time!) : "Unsupported Timezone".localized
        } else {
            self.jobTime = ""
        }
        
        if let jobType = json["job_type"] as? Int{
            self.jobType = jobType
            if let deliveryJob = json["delivery_job_id"] as? Int {
                self.deliveryJobId = deliveryJob
            } else {
                self.deliveryJobId = 0
            }
            
            if let pickupJob = json["pickup_job_id"] as? Int {
                self.pickupJobId = pickupJob
            } else {
                self.pickupJobId = 0
            }
        }
        
        if let jobPickupAddress = json["job_pickup_address"] as? String{
            self.jobPickupAddress = jobPickupAddress
        }else{
            self.jobPickupAddress = ""
        }
        
        if let jobPickupDatetime = json["job_pickup_datetime"] as? String{
            self.jobPickupDatetime = jobPickupDatetime
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let time = dateFormatter.date(from: self.jobPickupDatetime)
            dateFormatter.dateFormat = "hh:mm a"
            self.jobPickupDateTimeInTwelveHoursFormat = time != nil ? dateFormatter.string(from: time!) : "Unsupported Timezone".localized
        } else {
            self.jobPickupDatetime = ""
        }
        
        if let jobPickupEmail = json["job_pickup_email"] as? String{
            self.jobPickupEmail = jobPickupEmail
        } else {
            self.jobPickupEmail = ""
        }
        
        if let jobPickupLatitude = json["job_pickup_latitude"] as? String{
            self.jobPickupLatitude = jobPickupLatitude
        } else {
            self.jobPickupLatitude = ""
        }
        
        if let jobPickupLongitude = json["job_pickup_longitude"] as? String{
            self.jobPickupLongitude = jobPickupLongitude
            if jobPickupLongitude.blank(jobPickupLongitude){
                self.jobPickupLongitude = ""
            }
        } else {
            self.jobPickupLongitude = ""
        }
        
//        if let jobPickupPhone = json["job_pickup_phone"] as? String{
//            self.jobPickupPhone = jobPickupPhone
//            if(self.jobPickupPhone.length > 1) {
//                self.customerPhone = self.jobPickupPhone
//            }
//        } else {
//            self.jobPickupPhone = ""
//        }
        
        if let jobPickupName = json["job_pickup_name"] as? String{
            self.jobPickupName = jobPickupName
            if jobPickupName.blank(jobPickupName){
                self.jobPickupName = ""
            }
        } else {
            self.jobPickupName = ""
        }
        
        if let jobAddress = json["job_address"] as? String{
            self.jobAddress = jobAddress
            
            if jobAddress.blank(jobAddress){
                self.jobAddress = ""
            }
        }else{
            self.jobAddress = ""
        }
        
        if let jobDeliveryDatetime = json["job_delivery_datetime"] as? String{
            self.jobDeliveryDatetime = jobDeliveryDatetime
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let time = dateFormatter.date(from: self.jobDeliveryDatetime)
            dateFormatter.dateFormat = "hh:mm a"
            self.jobDeliveryDateTimeInTwelveFormat = time != nil ? dateFormatter.string(from: time!) : "Unsupported Timezone".localized
        } else {
            self.jobDeliveryDatetime = ""
        }
        
        if let jobDescription = json["job_description"] as? String{
            self.jobDescription = jobDescription
            if jobDescription.isEmpty || jobDescription.blank(jobDescription){
                self.jobDescription = ""
            }
        } else {
            self.jobDescription = ""
        }
        
        if let jobLongitude = json["job_longitude"] as? String{
            self.jobLongitude = jobLongitude
            
            if jobLongitude.blank(jobLongitude){
                self.jobLongitude = ""
            }
        } else {
            self.jobLongitude = ""
        }
        
        if let jobLatitude = json["job_latitude"] as? String{
            self.jobLatitude = jobLatitude
            if jobLatitude.blank(jobLatitude){
                self.jobLatitude = ""
            }
        } else {
            self.jobLatitude = ""
        }
        
        if let pickupDeliveryRelationship = json["pickup_delivery_relationship"] as? String{
            if pickupDeliveryRelationship.isEmpty{
                self.pickupDeliveryRelationship = ""
            }
            self.pickupDeliveryRelationship = pickupDeliveryRelationship
        } else {
            self.pickupDeliveryRelationship = ""
        }
        
        if let items = json["task_history"] as? NSArray{
            taskHistoryArray = [TaskHistory]()
            addedImagesArray = [TaskHistory]()
            addedNotesArray = [TaskHistory]()
            addedSignatureArray = [TaskHistory]()
            addedScannerArray = [TaskHistory]()
            
            for itemDictionary in items{
                let item = itemDictionary as! NSDictionary
                if let type = item["type"] as? String{
                    switch type {
                    case "image_added":
                        if let taskHistory = TaskHistory(json: item ) as TaskHistory! {
                            addedImagesArray?.append(taskHistory)
                            let caption = Caption(json: [:])
                            caption.caption_data = taskHistory.extra_fields!
                            caption.image_url = taskHistory.taskDescription!
                            addedCaptionArray?.append(caption)
                        }
                    case "text_added","text_updated":
                        if let taskHistory = TaskHistory(json: item ) as TaskHistory! {
                            addedNotesArray?.append(taskHistory)
                        }
                    case "signature_image_added":
                        if let taskHistory = TaskHistory(json: item ) as TaskHistory! {
                            addedSignatureArray?.append(taskHistory)
                        }
                    case "state_changed":
                        if let taskHistory = TaskHistory(json: item ) as TaskHistory! {
                            taskHistoryArray?.append(taskHistory)
                        }
                    case "barcode_added", "barcode_updated":
                        if let taskHistory = TaskHistory(json: item ) as TaskHistory! {
                            addedScannerArray?.append(taskHistory)
                        }
                    default:
                        break
                    }
                }
            }
        } else {
            taskHistoryArray = [TaskHistory]()
            addedImagesArray = [TaskHistory]()
            addedNotesArray = [TaskHistory]()
            addedSignatureArray = [TaskHistory]()
            addedScannerArray = [TaskHistory]()
        }
        
        if let fields = json["fields"] as? NSDictionary {
            if let appOptionalFields = fields["app_optional_fields"] as? NSArray {
                
                if appOptionalFields.count == 0{
                    appOptionalFieldArray = [AppOptionalFields]()
                } else {
                    appOptionalFieldArray = [AppOptionalFields]()
                }
                
                for item in appOptionalFields {
                    if let optionalField = AppOptionalFields(json: item as! NSDictionary) as AppOptionalFields!{
                        appOptionalFieldArray?.append(optionalField)
                        switch optionalField.label {
                        case APP_OPTIONAL_FIELD.accept:
                            acceptOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.notes:
                            noteOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.images:
                            imageOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.signature:
                            signatureOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.slider:
                            sliderOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.confirm:
                            confirmOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.arrived:
                            arrivedOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.masking:
                            maskingOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.fleetNotification:
                            fleetNotificationField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.scanner:
                            scannerOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.hideCancelButton:
                            hideCancelOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.hideFailedButton:
                            hideFailedOptionalField = optionalField
                            break
                        case APP_OPTIONAL_FIELD.caption:
                            captionOptionalField = optionalField
                        default:
                            break
                        }
                    }
                }
            } else {
                appOptionalFieldArray = [AppOptionalFields]()
            }
        } else {
            appOptionalFieldArray = [AppOptionalFields]()
        }
        
        
        
        if let fields = json["fields"] as? NSDictionary {
            if let customFields = fields["custom_field"] as? NSArray {
                if customFields.count == 0{
                    customFieldArray = [CustomFields]()
                } else {
                    customFieldArray = [CustomFields]()
                }
                for item in customFields {
                    if let customField = CustomFields(json: item as! [String:Any]) as CustomFields!{
                        switch customField.dataType {
                        case CUSTOM_FIELD_DATA_TYPE.barcode,
                             CUSTOM_FIELD_DATA_TYPE.dropDown,
                             CUSTOM_FIELD_DATA_TYPE.text,
                             CUSTOM_FIELD_DATA_TYPE.number,
                             CUSTOM_FIELD_DATA_TYPE.image,
                             CUSTOM_FIELD_DATA_TYPE.date,
                             CUSTOM_FIELD_DATA_TYPE.checkbox,
                             CUSTOM_FIELD_DATA_TYPE.telephone,
                             CUSTOM_FIELD_DATA_TYPE.email,
                             CUSTOM_FIELD_DATA_TYPE.url,
                             CUSTOM_FIELD_DATA_TYPE.checklist,
                             CUSTOM_FIELD_DATA_TYPE.dateFuture,
                             CUSTOM_FIELD_DATA_TYPE.datePast,
                             CUSTOM_FIELD_DATA_TYPE.dateTime,
                             CUSTOM_FIELD_DATA_TYPE.dateTimeFuture,
                             CUSTOM_FIELD_DATA_TYPE.dateTimePast,
                             CUSTOM_FIELD_DATA_TYPE.table:
                            customFieldArray?.append(customField)
                        default:
                            break
                        }
                    }
                }
            } else {
                customFieldArray = [CustomFields]()
            }
        } else {
           customFieldArray = [CustomFields]() 
        }
        
        if let fields = json["fields"] as? NSDictionary {
            if let refImages = fields["ref_images"] as? NSArray {
                referenceImageArray = refImages
                for _ in 0..<referenceImageArray.count {
                    let caption = Caption(json: [:])
                    referenceImagesCaptionArray?.append(caption)
                }
            }
        }
        
        if let fields = json["fields"] as? NSDictionary {
            if let extra = fields["extras"] as? NSDictionary {
                if let htmlString = extra["invoice_html"] as? String {
                    self.invoiceHtml = htmlString
                } else {
                    self.invoiceHtml = ""
                }
            } else {
                self.invoiceHtml = ""
            }
        }
        
        if let value = json["total_distance_travelled"] as? String{
            self.travelledDistance = Double(value)!
        } else if let value = json["total_distance_travelled"] as? NSNumber{
            self.travelledDistance = Double(value)
        } else {
            self.travelledDistance = 0.0
        }
        
        if let value = json["total_time"] as? String{
            self.travelledTime = Double(value)
        } else if let value = json["total_time"] as? NSNumber{
            self.travelledTime = Double(value)
        } else {
            self.travelledTime = 0.0
        }

        if let value = json["vertical"] as? String{
            self.vertical = Int(value)!
        } else if let value = json["vertical"] as? NSNumber{
            self.vertical = Int(value)
        } else {
            self.vertical = 0
        }
        
        if let value = json["related_job_count"] as? String{
            self.related_job_count = Int(value)!
        } else if let value = json["related_job_count"] as? NSNumber{
            self.related_job_count = Int(value)
        } else {
            self.related_job_count = 0
        }
       
        
        if let value = json["related_job_type"] as? String{
            self.related_job_type = Int(value)!
        } else if let value = json["related_job_type"] as? NSNumber{
            self.related_job_type = Int(value)
        } else {
            self.related_job_type = 0
        }
        
        if let value = json["just_now"] as? String{
            self.just_now = Int(value)!
        } else if let value = json["just_now"] as? NSNumber{
            self.just_now = Int(value)
        } else {
            self.just_now = 0
        }
        
        if let value = json["time_left_in_seconds"] as? String{
            self.time_left_in_seconds = Int(value)!
        } else if let value = json["time_left_in_seconds"] as? NSNumber{
            self.time_left_in_seconds = Int(value)
        } else {
            self.time_left_in_seconds = 0
        }
        
        if let value = json["accept_button"] as? String{
            self.accept_button = Int(value)!
        } else if let value = json["accept_button"] as? NSNumber{
            self.accept_button = Int(value)
        } else {
            self.accept_button = 0
        }
        
        if let value = json["related_job_indexes"] as? [Any] {
            self.related_job_indexes = value
        }
        
        if let value = json["m"] as? String{
            self.metaData = value
        } else {
            self.metaData = ""
        }
        
        if let value = json["driver_job_total"] as? String{
            self.driverJobTotal = value
        } else if let value = json["driver_job_total"] as? NSNumber{
            self.driverJobTotal = "\(value)"
        } else {
            self.driverJobTotal = ""
        }
        
        if let value = json["customer_job_total"] as? String{
            self.customerJobTotal = value
        } else if let value = json["customer_job_total"] as? NSNumber{
            self.customerJobTotal = "\(value)"
        } else {
            self.customerJobTotal = ""
        }
        
        if Singleton.sharedInstance.fleetDetails != nil {
            if Singleton.sharedInstance.fleetDetails.rating_comments == 1 {
                if let fields = json["fields"] as? NSDictionary {
                    if let value = fields["is_rating_comment_enabled"] as? Bool {
                        self.isRatingEnabled = value
                    } else if let value = fields["is_rating_comment_enabled"] as? NSNumber {
                        self.isRatingEnabled = value.boolValue
                    } else if let value = fields["is_rating_comment_enabled"] as? String {
                        self.isRatingEnabled = value == "1" ? true : false
                    }
                }
                
                if let value = json["customer_rating"] as? String{
                    if value.length > 0 {
                        self.customerRating = Int(value)!
                    }
                } else if let value = json["customer_rating"] as? NSNumber{
                    self.customerRating = Int(value)
                } else {
                    self.customerRating = 0
                }
                
                if let value = json["driver_comment"] as? String{
                    self.customerComment = value
                } else if let value = json["driver_comment"] as? NSNumber{
                    self.customerComment = "\(value)"
                } else {
                    self.customerComment = ""
                }
            }
        }
    }
}
