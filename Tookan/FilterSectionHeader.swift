//
//  FilterSectionHeader.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 4/5/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class FilterSectionHeader: UIView {

    
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var filterHeaderLabel: UILabel!
    @IBOutlet weak var filterHeaderIcon: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        filterHeaderLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        filterHeaderLabel.textColor = COLOR.TEXT_COLOR
        
        clearButton.isHidden = true
        clearButton.layer.cornerRadius = getAspectRatioValue(value: clearButton.frame.height/2)
        clearButton.backgroundColor = COLOR.TABLE_BACKGROUND_COLOR
        clearButton.setTitle(TEXT.CLEAR, for: .normal)
        clearButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        clearButton.setTitleColor(COLOR.FILTER_CLEAR_COLOR, for: .normal)
        self.seperatorView.backgroundColor = COLOR.LINE_COLOR
    }
    

}
