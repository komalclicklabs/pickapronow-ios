//
//  CopyRepeatCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class CopyRepeatCell: UICollectionViewCell {

    let repeatCellHeight:CGFloat = 55
    let normalColor = UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 1.0)
    let selectedColor = COLOR.themeForegroundColor
    @IBOutlet var checkbox: UIButton!
    @IBOutlet var dateLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        checkbox.setTitleColor(normalColor, for: UIControlState.normal)
        checkbox.setTitleColor(COLOR.themeForegroundColor, for: UIControlState.selected)
        checkbox.layer.borderWidth = 2.0
        checkbox.layer.borderColor = normalColor.cgColor
        checkbox.layer.cornerRadius = repeatCellHeight / 2
        dateLabel.textColor = normalColor
    }

    @IBAction func checkboxAction(_ sender: UIButton) {
        //sender.isSelected = !sender.isSelected
    }
    
}
