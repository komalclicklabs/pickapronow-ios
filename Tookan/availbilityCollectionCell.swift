//
//  availbilityCollectionCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 14/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class availbilityCollectionCell: UICollectionViewCell {

    @IBOutlet var centerImageView: UIImageView!
    @IBOutlet var lock: UIImageView!
    @IBOutlet var bottomMarginView: UIView!
    @IBOutlet var topMarginView: UIView!
    @IBOutlet var statusView: UIView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
       // statusView.layer.cornerRadius = 2.0
        topMarginView.isHidden = true
        bottomMarginView.isHidden = true
        lock.isHidden = true
        // Initialization code
    }

}
