//
//  ProfileViewController.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 3/28/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Crashlytics
import AVFoundation

enum PROFILE_FIELDS:Int {
    case name = 0
    case phone
    case email
    case teams
    case oldPassword
    case newPassword
    case retypePassword
}


class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate,UINavigationControllerDelegate, NoInternetConnectionDelegate, UITextViewDelegate, NavigationDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var doneActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var doneButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet var bgView: UIView!
    @IBOutlet var topConstraintOfMainView: NSLayoutConstraint!
    
    var internetToast:NoInternetConnectionView!
    var imagePicker: UIImagePickerController!
    var isEditEnabled = false
    var defaultTableBottomConstraint:CGFloat = 30.0
    var cell_Y : CGFloat = 0.0
    var currentRect:CGRect! = CGRect.zero
    var isTextFieldValidate = false
    var isPasswordReTypedCellCreated = false
    var updatedUserDetails = FleetInfoDetails(json:[:])
    var profileModel = ProfileModel()
    var isusernameValidate = true
    let headerHeight:CGFloat =  getAspectRatioValue(value:280.0)
    var footerHeight:CGFloat = 30.0
    var headerView:ProfileHeaderView!
    var drowDownWithSearch:TemplateController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.setUpdatedUserDetails()
        /*===============Setting Done Button Design=================*/
        self.doneButton.backgroundColor = COLOR.themeForegroundColor
        self.doneButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        
        self.setTable()
        self.doneActivityIndicator.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
        
        self.topConstraintOfMainView.constant = HEIGHT.navigationHeight + 10.0
        self.bgView.layer.cornerRadius = 15.0
        guard SHOW_SIGNUP == 1 && Singleton.sharedInstance.fleetDetails.fleet_signup! == 1 else {
            self.footerHeight = 0.0
            return
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.perform(#selector(self.updateStatusBar), with: nil, afterDelay: 1.0)
        self.doneButton.transform = CGAffineTransform(translationX: 0, y: self.doneButton.frame.height + Singleton.sharedInstance.getBottomInsetofSafeArea())
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func updateStatusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func backgroundTouch() {
        self.view.endEditing(true)
    }
    
    func setTable(){
        self.tableView.register(UINib(nibName: NIB_NAME.profileViewCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.profileViewCell)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        //self.tableView.estimatedRowHeight = 160
        self.tableView.allowsSelection = false
        //self.setTableHeaderView()
    }
    
    func editButtonAction() {
        Analytics.logEvent(ANALYTICS_KEY.PROFILE_EDIT, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.PROFILE_EDIT, customAttributes: [:])
        self.view.endEditing(true)
        isusernameValidate  = true
        isEditEnabled = !isEditEnabled
        self.updateEditButton(isEditEnabled: isEditEnabled)
    }
    
    func setUpdatedUserDetails() {
        guard Singleton.sharedInstance.fleetDetails != nil else {
            return
        }
        self.updatedUserDetails.name = Singleton.sharedInstance.fleetDetails.name!
        self.updatedUserDetails.phone = Singleton.sharedInstance.fleetDetails.phone!
//        if Singleton.sharedInstance.fleetDetails.teams.count == 0 {
//            let team = AssignedTeamDetails(json: [:])
//            Singleton.sharedInstance.fleetDetails.teams.append(team)
//        }
//        if self.updatedUserDetails.teams.count == 0 {
//            let team = AssignedTeamDetails(json: [:])
//            self.updatedUserDetails.teams.append(team)
//        }
        
        self.updatedUserDetails.teams?.teamName = Singleton.sharedInstance.fleetDetails.teams?.teamName
        self.updatedUserDetails.oldPassword = ""
        self.updatedUserDetails.newPassword = ""
        self.updatedUserDetails.retypePassword = ""
    }
    
    
    //MARK: Navigation Bar
    func setNavigationBar() {
        let navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = TEXT.MY_PROFILE
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        //        navigation.bottomLine.isHidden = false
        
        navigation.editButton.isHidden = false
        navigation.editButton.setTitle(TEXT.LOGOUT, for: .normal)
        navigation.editButton.setTitleColor(UIColor.white, for: .normal)
        navigation.editButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        navigation.editButton.addTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
        navigation.editButtonTrailingConstraint.constant = 15.0
        self.view.addSubview(navigation)
    }
    
    //MARK: NavigationDelegate Methods
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func logoutAction() {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.LOGOUT, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.LOGOUT, customAttributes: [:])
        //  dismissAction()
        let offlineModel = OfflineDataModel()
        let isThereAnyUnsyncedTask = offlineModel.isThereAnyUnsyncedTaskInDatabase()
        var message = ""
        if(isThereAnyUnsyncedTask > 0) {
            message = ERROR_MESSAGE.LOGOUT_DATA_LOST
        }
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        if(isThereAnyUnsyncedTask > 0) {
            alertController.message = message
        }
        let confirmAction = UIAlertAction(title: TEXT.LOGOUT, style: UIAlertActionStyle.destructive) { (confirmed) -> Void in
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.LOGOUT_CONFIRM, parameters: [:])
            Answers.logCustomEvent(withName:  ANALYTICS_KEY.LOGOUT_CONFIRM, customAttributes: [:])
            self.sendRequestForLogout()
        }
        
        let syncAction = UIAlertAction(title: TEXT.SYNC, style: UIAlertActionStyle.default) { (UIAlertAction) in
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.LOGOUT_SYNC, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.LOGOUT_SYNC, customAttributes: [:])
            self.syncAction()
        }
        
        let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.cancel, handler: {(UIAlertAction) in
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.LOGOUT_CANCEL, parameters: [:])
        })
        if(isThereAnyUnsyncedTask > 0) {
            alertController.addAction(syncAction)
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sendRequestForLogout() {
        ActivityIndicator.sharedInstance.showActivityIndicator((self.navigationController?.visibleViewController)!)
        if let accessToken = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as? String {
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.logout, params: [
                "access_token":accessToken as AnyObject], httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                    DispatchQueue.main.async {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        if isSucceeded == true {
                            switch(response["status"] as! Int) {
                            case STATUS_CODES.SHOW_DATA:
                                Auxillary.logoutFromDevice()
                                NotificationCenter.default.removeObserver(self)
                                break
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                self.showInvalidAccessTokenPopup(message: response["message"] as! String!)
                                break
                                
                            default:
                                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                                break
                            }
                        } else {
                            Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        }
                    }
            })
        } else {
            Auxillary.logoutFromDevice()
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    func syncAction() {
        let offlineDataModel = OfflineDataModel()
        if(offlineDataModel.uploadUnsyncedTaskToServer() == false) {
            if(offlineDataModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                self.showInvalidAccessTokenPopup(message: offlineDataModel.hitFailMessage)
            } else {
                let alert = UIAlertController(title: TEXT.ERROR, message: ERROR_MESSAGE.SYNC_FAILED_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
                let tryAgain = UIAlertAction(title: TEXT.TRY_AGAIN, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.syncAction()
                    })
                })
                alert.addAction(tryAgain)
                
                let offlineData = UIAlertAction(title: TEXT.LOGOUT_WITHOUT_SYNC, style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.sendRequestForLogout()
                    })
                })
                alert.addAction(offlineData)
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            self.sendRequestForLogout()
        }
    }
    
    func showInvalidAccessTokenPopup(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                NotificationCenter.default.removeObserver(self)
                Auxillary.logoutFromDevice()
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Edit Button Action
    //    @IBAction func isEditing(_ sender: UIButton) {
    //        self.view.endEditing(true)
    //        isusernameValidate  = true
    //        isEditEnabled = !isEditEnabled
    //        self.updateEditButton(isEditEnabled: isEditEnabled)
    //    }
    
    func updateEditButton(isEditEnabled:Bool) {
        self.headerView.editButton.isSelected = isEditEnabled
        if(isEditEnabled == true) {
            self.headerView.headerLabel.text = TEXT.EDIT_ACCOUNT_DETAILS
            self.doneButton.setTitle(TEXT.DONE_EDITING, for: .normal)
            self.tableBottomConstraint.constant = self.doneButton.frame.height + footerHeight
            UIView.animate(withDuration: 0.35, animations: {
                self.doneButton.transform = CGAffineTransform.identity
            }
                , completion: { finished in
                    // self.setTableHeaderView()
            })
        }else {
            self.view.endEditing(true)
            self.setUpdatedUserDetails()
            self.headerView.headerLabel.text = TEXT.ACCOUNT_DETAILS
            self.tableBottomConstraint.constant = CGFloat(self.defaultTableBottomConstraint)
            UIView.animate(withDuration: 0.3, animations: { finished in
                self.doneButton.transform = CGAffineTransform(translationX: 0, y: self.doneButton.frame.height + Singleton.sharedInstance.getBottomInsetofSafeArea())
            })
        }
        self.tableView.reloadData()
    }
    
    
    @IBAction func doneEditing(_ sender: Any) {
        self.view.endEditing(true)
        
        guard self.profileModel.hasAnychangesMade(updatedUserDetails: self.updatedUserDetails) == true else {
            isEditEnabled = !isEditEnabled
            self.updateEditButton(isEditEnabled: isEditEnabled)
            return
        }
        
        guard self.checkForTextFields(index: 0, text: self.updatedUserDetails.name!) == true else {
            return
        }
        
        guard self.checkForTextFields(index: 1, text: self.updatedUserDetails.phone!) == true else {
            return
        }
        
        Analytics.logEvent(ANALYTICS_KEY.DONE_EDITING, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.DONE_EDITING, customAttributes: [:])
        if self.profileModel.isPasswordValidationRequired(updatedUserDetails: self.updatedUserDetails) == true {
            let data = self.profileModel.hasPasswordConditionsValidated(updatedUserDetails: self.updatedUserDetails)
            guard data.0 == true else {
                tableView.scrollToRow(at: IndexPath(row: data.2, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: data.2, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.isHidden = false
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.text = data.1
                    isTextFieldValidate = false
                    
                    // cell.textView.becomeFirstResponder()
                    if cell.bottomConstraint.constant == 0 {
                        cell.bottomConstraint.constant = cell.defaultBottomConstraintValue
                        self.updateCellHeight()
                    }
                    //tableView.scrollToRow(at: IndexPath(row: data.2, section: 0), at: .bottom, animated: true)
                }
                return
            }
            
            tableView.scrollToRow(at: IndexPath(row: data.2, section: 0), at: .bottom, animated: true)
            if let cell = self.tableView.cellForRow(at: IndexPath(row: data.2, section: 0)) as? ProfileViewCell{
                cell.verticalLine.backgroundColor = COLOR.LINE_COLOR
                cell.labelBelowTextField.isHidden = true
                isTextFieldValidate = true
                if cell.bottomConstraint.constant != 0 {
                    cell.bottomConstraint.constant = 0
                    self.updateCellHeight()
                }
            }
        }
        
        if isTextFieldValidate == true {
            self.doneButton.setTitle("\(TEXT.SAVING)...", for: .normal)
            self.doneActivityIndicator.isHidden = false
            self.doneActivityIndicator.startAnimating()
            guard IJReachability.isConnectedToNetwork() == true else {
                return self.showInternetToast()
            }
            
            let arrayName = self.updatedUserDetails.name?.trimText.components(separatedBy: " ")
            let firstName = arrayName?[0]
            var lastName = ""
            if (arrayName?.count)! >= 2 {
                for i in (1..<(arrayName?.count)!) {
                    lastName += "\(lastName) \(arrayName?[i] ?? "" )"// + arrayName[i]
                }
            }
            var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
            params["username"] = self.updatedUserDetails.name
            params["first_name"] = firstName
            params["last_name"] = lastName
            params["phone"] = self.updatedUserDetails.phone
            params["old_password"] = self.updatedUserDetails.oldPassword
            params["new_password"] = self.updatedUserDetails.newPassword
            params["timezone"] = -(NSTimeZone.system.secondsFromGMT() / 60)
            params["device_token"] = UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) as! String
            params["device_type"] = DEVICE_TYPE
//            if self.updatedUserDetails.teams.count == 0 {
//                let team = AssignedTeamDetails(json: [:])
//                self.updatedUserDetails.teams.append(team)
//            }
            params["team"] = self.updatedUserDetails.teams?.teamName
            //            let params = [
            //                "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
            //                "username":self.updatedUserDetails.name,
            //                "first_name":firstName,
            //                "last_name":lastName,
            //                "phone":self.updatedUserDetails.phone,
            //                "old_password":self.updatedUserDetails.oldPassword,
            //                "new_password":self.updatedUserDetails.newPassword,
            //                "timezone":-(NSTimeZone.system.secondsFromGMT() / 60),
            //                "device_token":UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) as! String,
            //                "device_type":DEVICE_TYPE
            //                ] as [String : Any]
            
            
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.editProfile, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
                DispatchQueue.main.async {
                    self.doneActivityIndicator.stopAnimating()
                    if(isSucceeded == true) {
                        Singleton.sharedInstance.fleetDetails.name = self.updatedUserDetails.name?.trimText
                        Singleton.sharedInstance.fleetDetails.phone = self.updatedUserDetails.phone?.trimText
//                        if Singleton.sharedInstance.fleetDetails.teams.count == 0 {
//                            let team = AssignedTeamDetails(json: [:])
//                            Singleton.sharedInstance.fleetDetails.teams.append(team)
//                        }
//                        if self.updatedUserDetails.teams.count == 0 {
//                            let team = AssignedTeamDetails(json: [:])
//                            self.updatedUserDetails.teams.append(team)
//                        }
                        Singleton.sharedInstance.fleetDetails.teams?.teamName = self.updatedUserDetails.teams?.teamName
                        self.successfullResponse(response)
                        self.isEditEnabled = false
                        self.doneButton.setTitle(TEXT.DONE_EDITING, for: .normal)
                        self.doneActivityIndicator.isHidden = true
                        self.doneActivityIndicator.stopAnimating()
                        UIView.animate(withDuration: 1.0, animations: {
                            self.doneButton.transform = CGAffineTransform(translationX: 0, y: self.doneButton.frame.height + Singleton.sharedInstance.getBottomInsetofSafeArea())
                        }
                            , completion: nil)
                        self.tableBottomConstraint.constant = CGFloat(self.defaultTableBottomConstraint)
                        self.tableView.reloadData()
                        self.headerView.editButton.isSelected = false
                        self.setUpdatedUserDetails()
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        self.doneButton.setTitle(TEXT.DONE_EDITING, for: .normal)
                        self.doneActivityIndicator.isHidden = true
                        self.doneActivityIndicator.stopAnimating()
                    }
                }}
        }
        
    }
    //MARK: For changing Profile Pic
    func getImageFromPhotoLibrary(){
        self.imagePicker =  UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    func getImageFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePicker =  UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: {
                self.checkAuthorization()
            })
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_CAMERA_SUPPORT, isError: .error)
        }
    }
    
    func captureImageFromCamera(){
        let alert = UIAlertController(title: TEXT.UPLOAD_IMAGE_FROM, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: TEXT.CAMERA, style: UIAlertActionStyle.default){
            UIAlertAction in
            let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
            switch authStatus {
            case .authorized, .notDetermined :
                self.getImageFromCamera()
                break
            default:
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.REJECTED_CAMERA_SUPPORT, isError: .error)
                break
            }
        }
        let gallaryAction = UIAlertAction(title: TEXT.GALLERY, style: UIAlertActionStyle.default){
            UIAlertAction in
            self.getImageFromPhotoLibrary()
        }
        let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.cancel){
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkAuthorization() {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == AVAuthorizationStatus.notDetermined {
            
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (videoGranted: Bool) -> Void in
                
                // User clicked ok
                if (videoGranted) {
                    // User clicked don't allow
                } else {
                    self.imagePicker.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    
    //MARK: imagePickerController Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        picker.dismiss(animated: true, completion: nil)
        if let newPic = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if IJReachability.isConnectedToNetwork() == false {
                self.showInternetToast()
            } else {
                self.headerView.profilePic.image = newPic
                self.headerView.transView.isHidden = false
                self.headerView.transView.isOpaque = true
                self.headerView.profilePicActivityIndicator.isHidden = false
                self.headerView.profilePicActivityIndicator.startAnimating()
                self.profileModel.uploadImageToServer(newPic) { (succeeded, response) in
                    DispatchQueue.main.async(execute: {
                        self.headerView.transView.isHidden = true
                        self.headerView.profilePicActivityIndicator.isHidden = true
                        self.headerView.profilePicActivityIndicator.stopAnimating()
                        if(succeeded == true) {
                            self.successfullResponse(response)
                        } else {
                            Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                            self.headerView.profilePic.image = nil
                            if Singleton.sharedInstance.fleetDetails.fleetImage != "" {
                                self.headerView.profilePic.image = downloadImageAsynchronously(Singleton.sharedInstance.fleetDetails.fleetImage!, imageView: self.headerView.profilePic, placeHolderImage: UIImage(named: "img_placeholder")!, contentMode: UIViewContentMode.scaleAspectFill)
                            } else {
                                self.headerView.profilePic.image = UIImage(named: "img_placeholder")!
                            }
                        }
                    })
                }
            }
        }
        
    }
    
    //MARK: TableView Delegates and DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isEditEnabled == false){
//            if Singleton.sharedInstance.fleetDetails.teamsArray.count > 0 {
//                return 5
//            }
            return 5//self.isEditingUserDetails.count
        }else{
//            if Singleton.sharedInstance.fleetDetails.teamsArray.count > 0 {
//                return 7
//            }
            return 7//self.userDetails.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.profileViewCell) as! ProfileViewCell
        cell.delegate = self
        cell.textView.tag = indexPath.row
        cell.isUserInteractionEnabled = true
        let fieldType:PROFILE_FIELDS = PROFILE_FIELDS(rawValue: indexPath.row)!
        switch fieldType {
        case .name:
            let usernameValidationState = (cell.textView.text.trimText != "" && cell.textView.text.trimText != Singleton.sharedInstance.fleetDetails.name && isusernameValidate == false) ? false : true
            cell.setUsernameField(isEditable: self.isEditEnabled, name: self.updatedUserDetails.name!, validationStatus : usernameValidationState)
            break
        case .email:
            cell.setEmailField()
            break
        case .phone:
            cell.setPhoneNumberField(isEditable: self.isEditEnabled, phone: self.updatedUserDetails.phone!)
            break
        case .oldPassword:
            cell.setOldPassword(isEditable: self.isEditEnabled, oldPassword:self.updatedUserDetails.oldPassword)
            break
        case .newPassword:
            cell.setNewPassword(isEditable: self.isEditEnabled, newPassword:self.updatedUserDetails.newPassword, oldPassword:self.updatedUserDetails.oldPassword)
            break
        case .retypePassword:
            cell.setRetypePassword(isEditable: self.isEditEnabled, retypePassword:self.updatedUserDetails.retypePassword,newPassword:self.updatedUserDetails.newPassword)
            break
        case .teams:
            guard Singleton.sharedInstance.showTeamsEnabled() == true else {
                return UITableViewCell()
            }
            cell.setTeamsCell(isEditable:self.isEditEnabled, selectedValue: (self.updatedUserDetails.teams?.teamName)!)
//            if (self.updatedUserDetails.teams?.teamName.length)! > 0 {
//                
//            }
//            else {
//                cell.setTeamsCell(isEditable:self.isEditEnabled, selectedValue:Singleton.sharedInstance.fleetDetails.teamsArray.joined(separator: ", "))
//            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let fieldType:PROFILE_FIELDS = PROFILE_FIELDS(rawValue: indexPath.row)!
        switch fieldType {
        case .teams:
            guard Singleton.sharedInstance.showTeamsEnabled() == true else {
                return 0.01
            }
        default:
            return UITableViewAutomaticDimension
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard self.headerView == nil else {
            return headerView
        }
        headerView = UINib(nibName: NIB_NAME.profileHeaderView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ProfileHeaderView
        headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: headerHeight)
        //self.tableView.tableHeaderView = headerView
        headerView.setProfilePic()
        headerView.editButton.addTarget(self, action: #selector(self.editButtonAction), for: .touchUpInside)
        
        let singleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.captureImageFromCamera))
        singleTap.numberOfTapsRequired = 1;
        self.headerView.cameraImage.addGestureRecognizer(singleTap)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        guard self.footerHeight == 0.0 else {
            footerView.frame = CGRect(x: 0.0, y: 0.0, width: SCREEN_SIZE.width, height: footerHeight)
            footerView.backgroundColor = COLOR.LINE_COLOR
            footerView.layer.borderColor = COLOR.EXTRA_LIGHT_COLOR.cgColor
            footerView.layer.borderWidth = 0.5
            
            let signUpLabel = UILabel()
            signUpLabel.frame = CGRect(x: 20.0, y: 0.0, width: 200, height: footerHeight)
            signUpLabel.text = TEXT.ADDITIONAL_INFORMATION
            signUpLabel.textColor = COLOR.LIGHT_COLOR
            signUpLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
            signUpLabel.setLetterSpacing(value: 1.8)
            
            let imageView = UIImageView()
            imageView.frame = CGRect(x: SCREEN_SIZE.width - #imageLiteral(resourceName: "right_arrow").size.width - 20.0, y: (footerView.frame.height/2) -  (#imageLiteral(resourceName: "right_arrow").size.height/2), width: #imageLiteral(resourceName: "right_arrow").size.width, height: #imageLiteral(resourceName: "right_arrow").size.height)
            imageView.image = #imageLiteral(resourceName: "right_arrow").withRenderingMode(.alwaysTemplate)
            imageView.tintColor = COLOR.themeForegroundColor
            footerView.addSubview(imageView)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.sendServerRequestToGetUserTemplateData))
            tap.numberOfTapsRequired = 1
            footerView.addGestureRecognizer(tap)
            footerView.addSubview(signUpLabel)
            return footerView
        }
        return footerView
    }
    
    func openSignUpController() {
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.fleet_signup, params: [:], httpMethod: HTTP_METHOD.POST) { (isSucceded, response) in
            print(response)
        }
        
        let homeStoryboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: Bundle.main)
        let controller  = homeStoryboard.instantiateViewController(withIdentifier:STORYBOARD_ID.signupTemplateController) as! SignupTemplateController
        controller.fromProfileVC = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return footerHeight
    }
    
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: NIB_NAME.noInternetConnectionView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:HEIGHT.navigationHeight, width: self.view.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.view.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
    
    func successfullResponse(_ response:[String:Any]) {
        switch(response["status"] as! Int) {
        case STATUS_CODES.SHOW_DATA:
            if let data = response["data"] as? NSDictionary {
                if let fleetImage = data["fleetImage"] as? String {
                    Singleton.sharedInstance.fleetDetails.fleetImage = fleetImage
                }
                
                if let accessToken  = data["access_token"] as? String {
                    print(accessToken)
                    guard accessToken.length > 0 else {
                        return
                    }
                    UserDefaults.standard.setValue(accessToken, forKey: USER_DEFAULT.accessToken)
                }
            }
            break
        case STATUS_CODES.INVALID_ACCESS_TOKEN:
            self.showInvalidAccessTokenPopup((response["message"] as? String)!)
            break
        default:
            break
        }
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
                //_ = self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendServerRequestToGetUserTemplateData() {
        var params:[String : Any] = ["fleet_access_token":Singleton.sharedInstance.getAccessToken()]
        params["fleet_id"] = Singleton.sharedInstance.fleetDetails.fleetId!
        params["mobile_device"] = 1
        ActivityIndicator.sharedInstance.showActivityIndicator()
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.view_signup_fleet, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceded, response) in
            DispatchQueue.main.async {
                print(response)
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        if let data = response["data"] as? [String:Any] {
                            if let signupData = data["signup_template_items"] as? [[String:Any]] {
                                print(signupData)
                                Singleton.sharedInstance.fleetDetails.signup_template_data?.removeAll()
                                for item in signupData {
                                    if let customField = CustomFields(json: item) as CustomFields!{
                                        Singleton.sharedInstance.fleetDetails.signup_template_data?.append(customField)
                                    }
                                }
//                                if Singleton.sharedInstance.fleetDetails.showTeams == 1 {
//                                    Singleton.sharedInstance.fleetDetails.isTeamEnabled = 1
//                                }
                                self.gotoSignUpController()
                            }
                        }
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    default:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func gotoSignUpController() {
        guard ((Singleton.sharedInstance.fleetDetails.signup_template_data?.count)! == 0)  else {
            self.gotoSignUpTemplateController()
            return
        }
//        guard (Singleton.sharedInstance.fleetDetails.isTeamEnabled == 0) else {
//            self.gotoSignUpTemplateController()
//            return
//        }
        Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
    }
    
    func gotoSignUpTemplateController() {
        let homeStoryboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: Bundle.main)
        let controller  = homeStoryboard.instantiateViewController(withIdentifier:STORYBOARD_ID.signupTemplateController) as! SignupTemplateController
        controller.fromProfileVC = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: Get data from Table
    func getTableData()->[String]
    {
        var userData : [String] = []
        var index = IndexPath(row: 0, section: 0)
        for _ in 0...1
        {
            userData.append(((self.tableView.cellForRow(at: index) as! ProfileViewCell).textView?.text)! )
            index.row += 1
        }
        print(userData)
        return userData
    }
    
    //MARK: Handling Keyboard Toggle
    func keyboardWillShow(notification: NSNotification) {
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.tableBottomConstraint.constant = keyboardSize.height + self.defaultTableBottomConstraint
            self.tableView.scrollRectToVisible(self.currentRect, animated: false)
        }
        
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
        }, completion: { finished in
            self.currentRect.size = CGSize.zero
        })
        Singleton.sharedInstance.translateErrorMessage(toBottom: false)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        //self.imageTopSpacing.constant = CGFloat(defaultImageTopSpacing)
        if(isEditEnabled == true){
            self.tableBottomConstraint.constant = self.doneButton.frame.height + footerHeight
        } else{
            self.tableBottomConstraint.constant = CGFloat(defaultTableBottomConstraint)
        }
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
            //self.view.layoutIfNeeded()
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }
    
    //MARK: UITEXTVIEW DELEGATE
    func checkForTextFields(index:Int, text:String) -> Bool {
        let fieldType:PROFILE_FIELDS = PROFILE_FIELDS(rawValue: index)!
        switch fieldType {
        case .name:
            Analytics.logEvent(ANALYTICS_KEY.PROFILE_EDIT_FIELDS, parameters: [ANALYTICS_KEY.PROFILE_EDIT_FIELDS : "name" as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.PROFILE_EDIT_FIELDS, customAttributes: [ANALYTICS_KEY.PROFILE_EDIT_FIELDS : "name"])
            if (text.trimText == ""){
                self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.isHidden = false
                    cell.labelBelowTextField.text = ERROR_MESSAGE.PLEASE_ENTER_NAME //TEXT.ENTER + " " + TEXT.NAME
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    isTextFieldValidate = false
                    if cell.bottomConstraint.constant == 0 {
                        cell.bottomConstraint.constant = cell.defaultBottomConstraintValue
                    }
                    cell.textView.becomeFirstResponder()
                }
                return false
            } else if(isusernameValidate == false) {
                // self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.isHidden = false
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.text = TEXT.USERNAME_TAKEN
                    isTextFieldValidate = false
                    if cell.bottomConstraint.constant == 0 {
                        cell.bottomConstraint.constant = cell.defaultBottomConstraintValue
                        self.updateCellHeight()
                    }
                    //cell.textView.becomeFirstResponder()
                    return false
                }
            } else {
                //self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.LINE_COLOR
                    cell.labelBelowTextField.isHidden = true
                    isTextFieldValidate = true
                    if cell.bottomConstraint.constant != 0 {
                        cell.bottomConstraint.constant = 0
                        self.updateCellHeight()
                    }
                }
                return true
            }
        case .phone:
            Analytics.logEvent(ANALYTICS_KEY.PROFILE_EDIT_FIELDS, parameters: [ANALYTICS_KEY.PROFILE_EDIT_FIELDS : "phone" as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.PROFILE_EDIT_FIELDS, customAttributes: [ANALYTICS_KEY.PROFILE_EDIT_FIELDS : "phone"])
            if (text.trimText.characters.count < 8){
                // self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.isHidden = false
                    cell.labelBelowTextField.text = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.PHONE_NUMBER)"// + TEXT.PHONE_NUMBER
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    isTextFieldValidate = false
                    if cell.bottomConstraint.constant == 0 {
                        cell.bottomConstraint.constant = cell.defaultBottomConstraintValue
                        self.updateCellHeight()
                    }
                    // cell.textView.becomeFirstResponder()
                }
                return false
                
            } else {
                // self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.LINE_COLOR
                    cell.labelBelowTextField.isHidden = true
                    isTextFieldValidate = true
                    if cell.bottomConstraint.constant != 0 {
                        cell.bottomConstraint.constant = 0
                        self.updateCellHeight()
                    }
                    return true
                }
            }
            
        case .oldPassword:
            if (text.trimText.length > 0 && text.trimText.characters.count < 6){
                //  self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.isHidden = false
                    cell.labelBelowTextField.text = ERROR_MESSAGE.SIX_CHAR_PASSWORD
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    isTextFieldValidate = false
                    if cell.bottomConstraint.constant == 0 {
                        cell.bottomConstraint.constant = cell.defaultBottomConstraintValue
                        self.updateCellHeight()
                    }
                    // cell.textView.becomeFirstResponder()
                }
                return false
            } else {
                //  self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.LINE_COLOR
                    cell.labelBelowTextField.isHidden = true
                    isTextFieldValidate = true
                    if cell.bottomConstraint.constant != 0 {
                        cell.bottomConstraint.constant = 0
                        self.updateCellHeight()
                    }
                }
                return true
            }
            
        case .newPassword:
            if (text.trimText.length > 0 && text.trimText.characters.count < 6) {
                //  self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.isHidden = false
                    cell.labelBelowTextField.text = ERROR_MESSAGE.SIX_CHAR_PASSWORD
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    isTextFieldValidate = false
                    if cell.bottomConstraint.constant == 0 {
                        cell.bottomConstraint.constant = cell.defaultBottomConstraintValue
                        self.updateCellHeight()
                    }
                    // cell.textView.becomeFirstResponder()
                }
                return false
            } else if(text.trimText.length > 0 && self.updatedUserDetails.oldPassword.length > 0 && self.updatedUserDetails.newPassword == self.updatedUserDetails.oldPassword) {
                //  self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.isHidden = false
                    cell.labelBelowTextField.text = ERROR_MESSAGE.SAME_PASSWORD
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    isTextFieldValidate = false
                    if cell.bottomConstraint.constant == 0 {
                        cell.bottomConstraint.constant = cell.defaultBottomConstraintValue
                        self.updateCellHeight()
                    }
                    // cell.textView.becomeFirstResponder()
                }
                return false
            } else {
                // self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.LINE_COLOR
                    cell.labelBelowTextField.isHidden = true
                    isTextFieldValidate = true
                    if cell.bottomConstraint.constant != 0 {
                        cell.bottomConstraint.constant = 0
                        self.updateCellHeight()
                    }
                }
                return true
            }
        case .retypePassword:
            Analytics.logEvent(ANALYTICS_KEY.PROFILE_EDIT_FIELDS, parameters: [ANALYTICS_KEY.PROFILE_EDIT_FIELDS : "password" as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.PROFILE_EDIT_FIELDS, customAttributes: [ANALYTICS_KEY.PROFILE_EDIT_FIELDS : "password"])
            if (self.updatedUserDetails.newPassword != self.updatedUserDetails.retypePassword){
                // self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.isHidden = false
                    cell.labelBelowTextField.text = ERROR_MESSAGE.DIFF_PASSWORD
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    isTextFieldValidate = false
                    if cell.bottomConstraint.constant == 0 {
                        cell.bottomConstraint.constant = cell.defaultBottomConstraintValue
                        self.updateCellHeight()
                    }
                    // cell.textView.becomeFirstResponder()
                }
                return false
            }else{
                //  self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: false)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProfileViewCell{
                    cell.verticalLine.backgroundColor = COLOR.LINE_COLOR
                    cell.labelBelowTextField.isHidden = true
                    isTextFieldValidate = true
                    if cell.bottomConstraint.constant != 0 {
                        cell.bottomConstraint.constant = 0
                        self.updateCellHeight()
                    }
                }
                return true
            }
        default:
            return true
        }
        return true
        
    }
    
    func updateCellHeight() {
        UIView.setAnimationsEnabled(false) // Disable animations
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
}
extension ProfileViewController : ProfileViewCellDelegate{
    func changeCellHeight(textView:UITextView) {
        UIView.setAnimationsEnabled(false)
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        self.setTextView(textView: textView)
    }
    
    func setTextView(textView: UITextView) {
        let fieldType:PROFILE_FIELDS = PROFILE_FIELDS(rawValue: textView.tag)!
        switch fieldType {
        case .name:
            self.updatedUserDetails.name = textView.text.trimText
            if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfileViewCell {
                if(textView.text.trimText != "") {
                    let activityIndicator = cell.textViewActivityIndicator
                    cell.labelBelowTextField.isHidden = true
                    self.checkForValidUserName(enteredText: textView.text.trimText,activityIndicator:activityIndicator! )
                } else {
                    cell.labelBelowTextField.isHidden = true
                }
            }
        case .phone:
            self.updatedUserDetails.phone = textView.text.trimText
        case .oldPassword:
            if textView.text.length < self.updatedUserDetails.oldPassword.length {
                self.updatedUserDetails.oldPassword = self.updatedUserDetails.oldPassword.substring(to: self.updatedUserDetails.oldPassword.index(before: self.updatedUserDetails.oldPassword.endIndex))
            }
            let passwordString = textView.text.replacingOccurrences(of: "*", with: "")
            self.updatedUserDetails.oldPassword += passwordString
            var passwordWord = ""
            for _ in 0..<self.updatedUserDetails.oldPassword.characters.count {
                passwordWord += "*"
            }
            textView.text = passwordWord
        case .newPassword:
            if textView.text.length < self.updatedUserDetails.newPassword.length {
                self.updatedUserDetails.newPassword = self.updatedUserDetails.newPassword.substring(to: self.updatedUserDetails.newPassword.index(before: self.updatedUserDetails.newPassword.endIndex))
            }
            let passwordString = textView.text.replacingOccurrences(of: "*", with: "")
            self.updatedUserDetails.newPassword += passwordString
            var passwordWord = ""
            for _ in 0..<self.updatedUserDetails.newPassword.characters.count {
                passwordWord += "*"
            }
            textView.text = passwordWord
        case .retypePassword:
            if textView.text.length < self.updatedUserDetails.retypePassword.length {
                self.updatedUserDetails.retypePassword = self.updatedUserDetails.retypePassword.substring(to: self.updatedUserDetails.retypePassword.index(before: self.updatedUserDetails.retypePassword.endIndex))
            }
            let passwordString = textView.text.replacingOccurrences(of: "*", with: "")
            self.updatedUserDetails.retypePassword += passwordString
            var passwordWord = ""
            for _ in 0..<self.updatedUserDetails.retypePassword.characters.count {
                passwordWord += "*"
            }
            textView.text = passwordWord
        default:
            break
        }
    }
    
    func returnButtonPressed(textView : UITextView){
        let fieldType:PROFILE_FIELDS = PROFILE_FIELDS(rawValue: textView.tag)!
        switch fieldType {
        case .name:
            self.tableView.scrollToRow(at: IndexPath(row: PROFILE_FIELDS.phone.rawValue, section: 0), at: .bottom, animated: false)
            if let cell = self.tableView.cellForRow(at: IndexPath(row: PROFILE_FIELDS.phone.rawValue, section: 0)) as? ProfileViewCell {
                cell.textView.becomeFirstResponder()
                return
            }
        case .phone:
            self.tableView.scrollToRow(at: IndexPath(row: PROFILE_FIELDS.oldPassword.rawValue, section: 0), at: .bottom, animated: false)
            if let cell = self.tableView.cellForRow(at: IndexPath(row: PROFILE_FIELDS.oldPassword.rawValue, section: 0)) as? ProfileViewCell {
                cell.textView.becomeFirstResponder()
                return
            }
        case .oldPassword:
            self.tableView.scrollToRow(at: IndexPath(row: PROFILE_FIELDS.newPassword.rawValue, section: 0), at: .bottom, animated: false)
            if let cell = self.tableView.cellForRow(at: IndexPath(row: PROFILE_FIELDS.newPassword.rawValue, section: 0)) as? ProfileViewCell {
                cell.textView.becomeFirstResponder()
                return
            }
        case .newPassword:
            self.tableView.scrollToRow(at: IndexPath(row: PROFILE_FIELDS.retypePassword.rawValue, section: 0), at: .bottom, animated: false)
            if let cell = self.tableView.cellForRow(at: IndexPath(row: PROFILE_FIELDS.retypePassword.rawValue, section: 0)) as? ProfileViewCell {
                cell.textView.becomeFirstResponder()
                return
            }
        case .retypePassword:
            self.view.endEditing(true)
            return
        default:
            break
        }
        self.view.endEditing(true)
    }
    
    func getCellPosition(textView: UITextView)  -> Bool {
        currentRect = self.tableView.rectForRow(at: IndexPath(row: textView.tag, section: 0))
        let fieldType:PROFILE_FIELDS = PROFILE_FIELDS(rawValue: textView.tag)!
        if fieldType == .teams {
            self.showTeams(tag: textView.tag)
            return false
        }
        return true
    }
    
    func showTeams(tag: Int) {
        self.view.endEditing(true)
        guard (Singleton.sharedInstance.fleetDetails.teamsArray?.count)! > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
            return
        }
        drowDownWithSearch = TemplateController()
        drowDownWithSearch.itemArray = Singleton.sharedInstance.fleetDetails.teamsArray!
        drowDownWithSearch.placeholderValue = TEXT.TEAMS
        drowDownWithSearch.delegate = self
        drowDownWithSearch.view.tag = tag
        drowDownWithSearch.modalPresentationStyle = .overCurrentContext
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.present(drowDownWithSearch, animated: false, completion: nil)
    }
    
    
    func checkForTextFieldValidation(textView : UITextView){
        _ = self.checkForTextFields(index: textView.tag, text: textView.text)
    }
}
extension ProfileViewController{
    func checkForValidUserName(enteredText:String,activityIndicator:UIActivityIndicatorView){
        activityIndicator.startAnimating()
        var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["username"] = enteredText
        //        let params = [
        //            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
        //            "username":enteredText
        //            ] as [String : Any]
        
        if #available(iOS 9.0, *) {
            URLSession.shared.getAllTasks { (requestedTaskList: [URLSessionTask]) in
                for task in requestedTaskList {
                    if task.originalRequest?.url?.lastPathComponent == API_NAME.checkUniqueUserName {
                        task.cancel()
                    }
                }
            }
        } else {
            // Fallback on earlier versions
        }
        if IJReachability.isConnectedToNetwork() == true {
            
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.checkUniqueUserName, params:params as [String : AnyObject]? , httpMethod: HTTP_METHOD.POST) { [weak self](isSucceeded, response) in
                DispatchQueue.main.async {
                    guard self != nil else {
                        return
                    }
                    
                    if(isSucceeded == true) {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            self?.updateForValidUserName(validUserName: true)
                            activityIndicator.stopAnimating()
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self?.showInvalidAccessTokenPopup(response["message"] as! String)
                            activityIndicator.stopAnimating()
                        default:
                            activityIndicator.stopAnimating()
                        }
                        
                    } else {
                        if(response["status"] as! Int == STATUS_CODES.SHOW_MESSAGE){
                            self?.updateForValidUserName(validUserName: false)
                            activityIndicator.stopAnimating()
                        }
                    }
                }
            }}else{
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            activityIndicator.stopAnimating()
        }
        
    }
    
    func updateForValidUserName(validUserName:Bool){
        if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfileViewCell {
            self.isusernameValidate = validUserName
            if(cell.textView.text.trimText == ""){
                cell.labelBelowTextField.isHidden = true
            }else{
                if(validUserName == true){
                    cell.labelBelowTextField.text = "\(TEXT.VALID) \(TEXT.NAME)"// + TEXT.NAME
                    cell.labelBelowTextField.isHidden = false
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_COLOR
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_COLOR
                }else{
                    cell.labelBelowTextField.isHidden = false
                    cell.verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
                    cell.labelBelowTextField.text = TEXT.USERNAME_TAKEN
                }
            }
        }
    }
}
//MARK: TemplateControllerDelegate Methods
extension ProfileViewController:TemplateControllerDelegate {
    func selectedValue(value: String, tag:Int, isDirectDismiss: Bool) {
        if value.isEmpty == false {
            self.updateValue(value, tagValue: self.drowDownWithSearch.view.tag)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.drowDownWithSearch = nil
        
        
    }
    
    func updateValue(_ updatedValue:String, tagValue:Int) {
        if IJReachability.isConnectedToNetwork() == false && Auxillary.isAppSyncingEnable() == false {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        } else {
//            if self.updatedUserDetails.teams.count == 0 {
//                let team = AssignedTeamDetails(json: [:])
//                Singleton.sharedInstance.fleetDetails.teams.append(team)
//            }
            self.updatedUserDetails.teams?.teamName = updatedValue
            //self.selectedTag = updatedValue
            let index = IndexPath(row: tagValue, section: 0)
            UIView.setAnimationsEnabled(false)
            self.tableView.reloadRows(at: [index], with: .none)
            UIView.setAnimationsEnabled(true)
        }
    }
}

