//
//  UserStateView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 31/05/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

class UserStateView: UIView {

    @IBOutlet var firstButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var contactLabel: UILabel!
    @IBOutlet var firstButton: UIButton!
    @IBOutlet var secondButton: UIButton!
    @IBOutlet var activityBackgroundView: UIView!
    @IBOutlet var refreshButton: UIButton!
    
    let defaultFirstButtonTopConstraintValue:CGFloat = 28.0
    let defaultTopConstraintValue:CGFloat = 36.0
    var activityIndicator:DGActivityIndicatorView!

    override func awakeFromNib() {
        self.contactLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)
        self.contactLabel.textColor = COLOR.LITTLE_LIGHT_COLOR
        
        self.firstButton.titleLabel?.font = UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.timer)
        self.firstButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.firstButton.layer.cornerRadius = 2.0
        self.firstButton.setShadow()
        
        self.secondButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.secondButton.setTitleColor(COLOR.LITTLE_LIGHT_COLOR, for: .normal)
        self.secondButton.backgroundColor = UIColor.clear
        
        self.refreshButton.titleLabel?.font = UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.timer)
        self.refreshButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.refreshButton.backgroundColor = COLOR.themeForegroundColor
        self.refreshButton.layer.cornerRadius = 2.0
        self.refreshButton.setShadow()
        
        self.activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballClipRotate, tintColor: COLOR.themeForegroundColor , size: 53.0)
        self.activityIndicator.frame = CGRect(x: (SCREEN_SIZE.width - 10) / 2 - 50, y: self.activityBackgroundView.frame.height / 2 - 50, width: 100, height: 100)
        self.activityIndicator.clipsToBounds = true
        self.activityBackgroundView.addSubview(self.activityIndicator)
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.callAction))
//        tap.numberOfTapsRequired = 1
//        self.contactLabel.addGestureRecognizer(tap)
    }
    
    func setStartVerificationProcess() {
        self.contactLabel.text = ""
        self.topConstraint.constant = self.defaultTopConstraintValue
        self.firstButtonTopConstraint.constant = self.defaultFirstButtonTopConstraintValue
        self.activityIndicator.stopAnimating()
        self.secondButton.isHidden = false

        self.firstButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.firstButton.backgroundColor = COLOR.themeForegroundColor
        self.firstButton.setTitle(TEXT.CONTINUE, for: .normal)
        self.firstButton.setImage(nil, for: .normal)
        self.firstButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.secondButton.setTitle(TEXT.LOGOUT, for: .normal)
    }
    
    func setReVerificationProcess() {
        self.contactLabel.text = ""
        self.topConstraint.constant = self.defaultTopConstraintValue
        self.firstButtonTopConstraint.constant = self.defaultFirstButtonTopConstraintValue
        self.activityIndicator.stopAnimating()
        self.secondButton.isHidden = false

        self.firstButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.firstButton.backgroundColor = COLOR.themeForegroundColor
        self.firstButton.setTitle(TEXT.START_OVER, for: .normal)
        self.firstButton.setImage(#imageLiteral(resourceName: "restartIcon"), for: .normal)
        self.firstButton.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0)
        self.secondButton.setTitle(TEXT.LOGOUT, for: .normal)
    }
    
    func setVerifiedProcess() {
        self.contactLabel.text = ""
        self.topConstraint.constant = self.defaultTopConstraintValue
        self.firstButtonTopConstraint.constant = self.defaultFirstButtonTopConstraintValue
        self.activityIndicator.stopAnimating()
        self.secondButton.isHidden = false
        self.firstButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.firstButton.backgroundColor = COLOR.SUCCESSFUL_VERIFICATION_COLOR
        self.firstButton.setTitle(TEXT.CONTINUE, for: .normal)
        self.firstButton.setImage(nil, for: .normal)
        self.firstButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.secondButton.setTitle(TEXT.LOGOUT, for: .normal)
    }
    
    func setFailedProcess() {
        self.contactLabel.text = ""
        self.topConstraint.constant = self.defaultTopConstraintValue
        self.firstButtonTopConstraint.constant = self.defaultFirstButtonTopConstraintValue
        self.activityIndicator.stopAnimating()
        self.secondButton.isHidden = false

       // self.setContactLabelText()
        self.firstButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.firstButton.backgroundColor = COLOR.FAILED_VERIFICATION_COLOR
        self.firstButton.setTitle(TEXT.LOGOUT, for: .normal)
        self.firstButton.setImage(nil, for: .normal)
        self.firstButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.secondButton.isHidden = true
    }
    
    func setBeingProcessed() {
        self.setContactLabelText()
        self.topConstraint.constant = 130
        self.firstButtonTopConstraint.constant = 0.0
        self.activityIndicator.startAnimating()
        
        self.firstButton.backgroundColor = UIColor.clear
        self.firstButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.firstButton.setTitleColor(COLOR.LITTLE_LIGHT_COLOR, for: .normal)
        self.firstButton.setTitle(TEXT.LOGOUT, for: .normal)
        self.firstButton.setImage(nil, for: .normal)
        self.firstButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.secondButton.isHidden = true
    }
    
    
    func setContactLabelText() {
        self.contactLabel.isHidden = false
        let attributedText = NSMutableAttributedString(string: Singleton.sharedInstance.fleetDetails.fleet_signup_info!, attributes: [NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)!, NSForegroundColorAttributeName:COLOR.LIGHT_COLOR])
//        let contactNumber = NSMutableAttributedString(string: CONTACT_NUMBER, attributes: [NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)!, NSForegroundColorAttributeName:COLOR.themeForegroundColor])
  //      attributedText.append(contactNumber)
        self.contactLabel.attributedText = attributedText
    }
    
    func callAction() {
        let phone = "tel://\(CONTACT_NUMBER.replacingOccurrences(of: " ", with: ""))"
        if let url:URL = URL(string:phone) {
            UIApplication.shared.openURL(url)
        } else {
            Singleton.sharedInstance.showErrorMessage(error: TEXT.CONNECTION_ERROR, isError: .error)
        }
    }
    
}
