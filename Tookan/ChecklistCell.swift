//
//  ChecklistCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 03/05/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class ChecklistCell: UITableViewCell {

    @IBOutlet var checklistTitle: UILabel!
    @IBOutlet var checklistButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*============ checklist Button =============*/
        
        self.checklistTitle.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.checklistTitle.textColor = COLOR.TEXT_COLOR
        //self.checklistButton.tintColor = COLOR.themeForegroundColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
