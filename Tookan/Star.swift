//
//  Star.swift
//  RatingsScreen
//
//  Created by CL-Macmini-110 on 6/29/17.
//  Copyright © 2017 CL-Macmini-110. All rights reserved.
//

import UIKit

class Star: UIView {

    //@IBOutlet weak var starImageOutlet: UIImageView!
    var lineWidth: CGFloat = 1.0
    var fillColor = UIColor.yellow.cgColor
    var strokeColor = UIColor.yellow.cgColor
    var path: UIBezierPath!
    var sizeOfStar:CGFloat!
    var shapeLayer = CAShapeLayer()
    
    override func awakeFromNib() {
        
    }
    
    override func draw(_ rect: CGRect) {
        //self.drawStar()
        
    }
    
    init(xPos: CGFloat,yPos: CGFloat,size: CGFloat,color:UIColor) {
        super.init(frame: CGRect(x: xPos, y: yPos, width: size, height: size))
        self.sizeOfStar = size
        self.fillColor = color.cgColor
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setPath() {
        self.path = starPathInRect(self.bounds.insetBy(dx: lineWidth,dy: lineWidth))
        shapeLayer = CAShapeLayer()
        shapeLayer.path = self.path.cgPath
        shapeLayer.fillColor = self.fillColor
        shapeLayer.strokeColor = self.strokeColor
        shapeLayer.lineWidth = self.lineWidth
        shapeLayer.miterLimit = 5.0
        self.layer.addSublayer(shapeLayer)
    }
    
    func starPathInRect(_ rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        let starExtrusion:CGFloat = sizeOfStar/5 //15.0
        let center = CGPoint(x: rect.width / 2.0, y: rect.height / 2.0)
        let pointsOnStar = 5 //+ arc4random() % 10
        var angle:CGFloat = -CGFloat(M_PI / 2.0)
        let angleIncrement = CGFloat(M_PI * 2.0 / Double(pointsOnStar))
        let radius = rect.width / 2.0
        let point = pointFrom(angle, radius: radius, offset: center)

        path.move(to: point)
        for _ in 1...pointsOnStar {
            let nextPoint = pointFrom(angle + angleIncrement, radius: radius, offset: center)
            let midPoint = pointFrom(angle + angleIncrement / 2.0, radius: starExtrusion, offset: center)
            path.addLine(to: midPoint)
            path.addLine(to: nextPoint)
            angle += angleIncrement
        }
        path.close()
        return path
    }

    
    func pointFrom(_ angle: CGFloat, radius: CGFloat, offset: CGPoint) -> CGPoint {
        return CGPoint(x: radius * cos(angle) + offset.x, y: radius * sin(angle) + offset.y)
    }
    
    
    
   
}
