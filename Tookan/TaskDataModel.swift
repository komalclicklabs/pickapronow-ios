//
//  TaskDataModel.swift
//  Tookan
//
//  Created by clicklabs106 on 9/17/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import CoreData

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class TaskDataModel: NSObject {
    
    /*---------------- Getting filtered Data ------------------*/
    func getFilteredData(forNewTaskList: Bool) -> [[TasksAssignedToDate]] {
        var filteredData = [[TasksAssignedToDate]]()
        filteredData = self.filterTaskOrderByStatus(NetworkingHelper.sharedInstance.dayTasksListArray)
        filteredData = self.filterTaskOrderByTemplates(filteredData)
        filteredData = self.getSearchedBasedTask(searchedText: Singleton.sharedInstance.searchedText, filteredList: filteredData)
        filteredData = self.setSortingOnJob(dayTasksListArray: filteredData)
        return filteredData
    }
    
    func getSearchedBasedTask(searchedText:String, filteredList:[[TasksAssignedToDate]]) -> [[TasksAssignedToDate]] {
        guard searchedText.length > 0 else {
            return filteredList
        }

        guard filteredList.count > 0 else {
            return filteredList
        }
        
        var statusInnerArray = [[TasksAssignedToDate]]()
        for i in (0..<filteredList.count){
            let innerArray = filteredList[i]
            let searchedTasksListArray = innerArray.filter { innerArray in
                
                if(innerArray.jobPickupAddress.lowercased().contains(searchedText.lowercased()) == true ||
                    innerArray.jobPickupName.lowercased().contains(searchedText.lowercased()) == true ||
                    innerArray.customerUsername.lowercased().contains(searchedText.lowercased()) == true ||
                    innerArray.jobAddress.lowercased().contains(searchedText.lowercased()) == true ||
                    "\(innerArray.jobId)".lowercased().contains(searchedText.lowercased())) {
                    return true
                } else if innerArray.customerEmail.lowercased().contains(searchedText.lowercased()) == true ||
                    innerArray.jobPickupEmail.lowercased().contains(searchedText.lowercased()) == true ||
                    innerArray.customerPhone.lowercased().contains(searchedText.lowercased()) ==  true ||
                    // filteredTasksListArray.jobPickupPhone.lowercaseString.containsString(searchText.lowercaseString) == true ||
                    innerArray.jobDeliveryDateTimeInTwelveFormat.lowercased().contains(searchedText.lowercased()) == true ||
                    innerArray.jobPickupDateTimeInTwelveHoursFormat.lowercased().contains(searchedText.lowercased()) == true ||
                    innerArray.jobDescription.lowercased().contains(searchedText.lowercased()) == true ||
                    innerArray.jobTimeInTwelveHoursFormat.lowercased().contains(searchedText.lowercased()) == true {
                    return true
                } else {
                    for i in (0..<innerArray.customFieldArray!.count){
                        if(innerArray.customFieldArray![i].displayLabelName.lowercased().contains(searchedText.lowercased()) == true) {
                            return true
                        }
                    }
                    return false
                }
            }
            if searchedTasksListArray.count > 0 {
                statusInnerArray.append(searchedTasksListArray)
            }
        }
        return statusInnerArray
    }
    
    
    /*-------------- Get List of Sorted Job list -----------------*/
//    func sortingTasksBasedJobOrder(_ dayTasksListArray: [TasksAssignedToDate], jobOrder:NSMutableArray) -> [TasksAssignedToDate] {
//        var sortedList = [TasksAssignedToDate]()
//        for i in (0..<jobOrder.count) {
//            for index in (0..<dayTasksListArray.count) {
//                if(dayTasksListArray[index].jobId == jobOrder[i] as! Int){
//                    sortedList.append(dayTasksListArray[index])
//                    break
//                }
//            }
//        }
//        if(sortedList.count > 0) {
//            return sortedList
//        } else {
//            return dayTasksListArray
//        }
//    }
    
    /*-------------- Get filter list of Status -----------------*/
    func getFilteredItemCount() -> [Int] {

        var filteredArray = [Int]()
        for i in (0..<Singleton.sharedInstance.filterStatusArray.count) {
            switch(Singleton.sharedInstance.filterStatusArray[i].title) {
            case TEXT.ASSIGNED:
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                        filteredArray.append(JOB_STATUS.assigned)
                    }
                break
            case TEXT.STARTED:
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                        filteredArray.append(JOB_STATUS.started)
                    }
                break
            case TEXT.SUCCESSFUL:
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                        filteredArray.append(JOB_STATUS.successful)
                    }
                break
            case TEXT.FAILED:
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                        filteredArray.append(JOB_STATUS.failed)
                    }
                break
            case TEXT.ARRIVED:
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                            filteredArray.append(JOB_STATUS.arrived)
                    }
                break
            case "Partial".localized:
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                        filteredArray.append(JOB_STATUS.partial)
                    }
                break
            case "\(TEXT.ACCEPTED) / \(TEXT.ACKNOWLEDGED)":
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                            filteredArray.append(JOB_STATUS.accepted)
                    }
                break
            case TEXT.DECLINE:
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                            filteredArray.append(JOB_STATUS.declined)
                    }
                break
            case TEXT.CANCELED:
                    if Singleton.sharedInstance.filterStatusArray[i].isSelected == true {
                        filteredArray.append(JOB_STATUS.canceled)
                    }
                break
            default:
                break
            }
        }
        return filteredArray
    }
    
    /*------------- Get Filter list Of Template -----------------*/
    func getTemplateItemCount()-> [String] {
        var selectedTemplateArray = [String]()
        for i in (0..<Singleton.sharedInstance.templateDictionary.count) {
            if(Singleton.sharedInstance.templateDictionary[i].isSelected == true) {
                selectedTemplateArray.append(Singleton.sharedInstance.templateDictionary[i].title)
            }
        }
        return selectedTemplateArray
    }
    
    /*--------------------- Filtering Task List by Status ---------------------*/
    func filterTaskOrderByStatus(_ dayTasksListArray: [[TasksAssignedToDate]]) -> [[TasksAssignedToDate]] {
        var sortedList = [[TasksAssignedToDate]]()
        let filteredArray = self.getFilteredItemCount()
        if(filteredArray.count > 0) {
            for i in (0..<dayTasksListArray.count){
                let innerArray = dayTasksListArray[i]
                var sortedInnerArray = [TasksAssignedToDate]()
                for task in innerArray {
                    if(filteredArray.contains(task.jobStatus)) {
                        sortedInnerArray.append(task)
                    }
                }
                if sortedInnerArray.count > 0 {
                    sortedList.append(sortedInnerArray)
                }
            }
            return sortedList
        } else {
            return dayTasksListArray
        }
    }
    
    /*-------------- Filtering Task List by Templates -----------*/
    func filterTaskOrderByTemplates(_ dayTasksListArray: [[TasksAssignedToDate]]) -> [[TasksAssignedToDate]] {
        var sortedList = [[TasksAssignedToDate]]()
        let templateArray = self.getTemplateItemCount()
        if(templateArray.count > 0) {
            for i in (0..<dayTasksListArray.count){
                let innerArray = dayTasksListArray[i]
                var statusInnerArray = [TasksAssignedToDate]()
                for task in innerArray {
                    if(task.customFieldArray?.count > 0) {
                        if(templateArray.contains(task.customFieldArray![0].templateName)) {
                            statusInnerArray.append(task)
                        }
                    }
                }
                if statusInnerArray.count > 0 {
                    sortedList.append(statusInnerArray)
                }
            }
            return sortedList
        } else {
            return dayTasksListArray
        }
    }
    /*===================== Sorting =================*/
    func setSortingOnJob(dayTasksListArray: [[TasksAssignedToDate]]) -> [[TasksAssignedToDate]] {
        guard Singleton.sharedInstance.sortingDictionary.count > 0 else {
            return dayTasksListArray
        }
        if Singleton.sharedInstance.sortingDictionary[0].isSelected == true {
            
            return self.updateTasksListByJobStatus(self.sortingTasksBasedOnTime(dayTasksListArray))
        } else if Singleton.sharedInstance.sortingDictionary[1].isSelected == true {
            return self.updateTasksListByJobStatus(self.sortingTasksBasedOnDistance(dayTasksListArray))
        }
        return dayTasksListArray
    }
    
    
    /*--------------- Sorting Task List by Distance -----------------*/
    func sortingTasksBasedOnDistance(_ dayTasksListArray: [[TasksAssignedToDate]]) -> [[TasksAssignedToDate]]{
        var distanceArray = [[Double]]()
        var tasksObjectArray = [[TasksAssignedToDate]]()
        var currentLocation : CLLocation!
        var fromLocation : CLLocation!
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        currentLocation = LocationTracker.sharedInstance().getLatestLocationForForegroundMode()
        for tasks in dayTasksListArray {
            for task in tasks {
                var latitude = CLLocationDegrees()
                var longitude = CLLocationDegrees()
                if task.jobType == 0 {
                    if let value = numberFormatter.number(from: task.jobPickupLatitude)?.doubleValue {
                        latitude = value
                    }
                    
                    if let value = numberFormatter.number(from: task.jobPickupLongitude)?.doubleValue {
                        longitude = value
                    }
                } else {
                    if let value = numberFormatter.number(from: task.jobLatitude)?.doubleValue {
                        latitude = value
                    }
                    
                    if let value = numberFormatter.number(from: task.jobLongitude)?.doubleValue {
                        longitude = value
                    }
                }
                fromLocation = CLLocation(latitude: latitude, longitude: longitude)
                let newDistanceArray = [currentLocation.distance(from: fromLocation)]
                distanceArray.append(newDistanceArray)
                tasksObjectArray.append([task])
            }
        }
        
        let nElements = distanceArray.count
        
        for fixedIndex in 0..<nElements {
            for i in fixedIndex+1..<nElements {
                if distanceArray[fixedIndex][0] > distanceArray[i][0] {
                    let tmp = distanceArray[fixedIndex]
                    let tmpObj = tasksObjectArray[fixedIndex]
                    distanceArray[fixedIndex] = distanceArray[i]
                    tasksObjectArray[fixedIndex] = tasksObjectArray[i]
                    distanceArray[i] = tmp
                    tasksObjectArray[i] = tmpObj
                }
            }
        }
        return tasksObjectArray
    }
    
    /*----------------- Sorting Task List by Time --------------*/
    func sortingTasksBasedOnTime(_ dayTasksListArray: [[TasksAssignedToDate]]) -> [[TasksAssignedToDate]] {
        var timeArray = [[Int]]()
        var newTime = Int()
        var tasksObjectArray = [[TasksAssignedToDate]]()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        for tasks in dayTasksListArray {
            for task in tasks {
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                if task.jobType == JOB_TYPE.pickup {
                    let time = dateFormatter.date(from: task.jobPickupDatetime)
                    dateFormatter.dateFormat = "HHmm"
                    newTime = numberFormatter.number(from: dateFormatter.string(from: time!))!.intValue
                } else if task.jobType == JOB_TYPE.delivery {
                    let time = dateFormatter.date(from: task.jobDeliveryDatetime)
                    dateFormatter.dateFormat = "HHmm"
                    newTime = numberFormatter.number(from: dateFormatter.string(from: time!))!.intValue
                } else if task.jobType == JOB_TYPE.appointment {
                    let time = dateFormatter.date(from: task.jobPickupDatetime)
                    dateFormatter.dateFormat = "HHmm"
                    newTime = numberFormatter.number(from: dateFormatter.string(from: time!))!.intValue
                } else if task.jobType == JOB_TYPE.fieldWorkforce {
                    let time = dateFormatter.date(from: task.jobPickupDatetime)
                    dateFormatter.dateFormat = "HHmm"
                    newTime = numberFormatter.number(from: dateFormatter.string(from: time!))!.intValue
                }
                let newTimeArray = [newTime]
                timeArray.append(newTimeArray)
                tasksObjectArray.append([task])
            }
        }
        
        
        let nElements = timeArray.count
        for fixedIndex in 0..<nElements {
            for i in fixedIndex+1..<nElements {
                if timeArray[fixedIndex][0] > timeArray[i][0] {
                    let tmp = timeArray[fixedIndex]
                    let tmpObj = tasksObjectArray[fixedIndex]
                    timeArray[fixedIndex] = timeArray[i]
                    tasksObjectArray[fixedIndex] = tasksObjectArray[i]
                    timeArray[i] = tmp
                    tasksObjectArray[i] = tmpObj
                }
            }
        }
        return tasksObjectArray
    }
    
    /*------------- Sort Task List By their job status -----------------*/
    func updateTasksListByJobStatus(_ dayTasksListArray: [[TasksAssignedToDate]]) -> [[TasksAssignedToDate]] {
        var completedTasks = [[TasksAssignedToDate]]()
        var incompletedTasks = [[TasksAssignedToDate]]()
        var newTaskList = [[TasksAssignedToDate]]()
        for tasks in dayTasksListArray {
            for task in tasks {
                switch task.jobStatus {
                case JOB_STATUS.assigned:
                    newTaskList.append([task])
                    break
                case JOB_STATUS.successful,JOB_STATUS.failed,JOB_STATUS.canceled,JOB_STATUS.declined:
                    completedTasks.append([task])
                case JOB_STATUS.started,JOB_STATUS.arrived,JOB_STATUS.accepted:
                    incompletedTasks.append([task])
                default:
                    break
                }
            }
        }
        let updatedList = newTaskList + incompletedTasks + completedTasks
        return updatedList
    }

    
    
    func decodePolyline(_ encodedPolyline: String, precision: Double = 1e6) -> GMSMutablePath! {
        let data = encodedPolyline.data(using: String.Encoding.utf8)!
        let byteArray = (data as NSData).bytes.assumingMemoryBound(to: Int8.self)
        let length = Int(data.count)
        var position = Int(0)
        
        let decodedCoordinates = GMSMutablePath()
        
        var lat = 0.0
        var lon = 0.0
        
        while position < length {
            
            do {
                let resultingLat = try decodeSingleCoordinate(byteArray: byteArray, length: length, position: &position, precision: precision)
                lat += resultingLat
                
                let resultingLon = try decodeSingleCoordinate(byteArray: byteArray, length: length, position: &position, precision: precision)
                lon += resultingLon
            } catch {
                return nil
            }
            
            decodedCoordinates.addLatitude(lat, longitude: lon)
        }
        
        return decodedCoordinates
    }
    
    func decodePolylineForCoordinates(_ encodedPolyline: String, precision: Double = 1e5) -> [CLLocation]! {
        let data = encodedPolyline.data(using: String.Encoding.utf8)!
        let byteArray = (data as NSData).bytes.assumingMemoryBound(to: Int8.self)
        let length = Int(data.count)
        var position = Int(0)
        
        var decodedCoordinates = [CLLocation]()
        
        var lat = 0.0
        var lon = 0.0
        
        while position < length {
            
            do {
                let resultingLat = try decodeSingleCoordinate(byteArray: byteArray, length: length, position: &position, precision: precision)
                lat += resultingLat
                
                let resultingLon = try decodeSingleCoordinate(byteArray: byteArray, length: length, position: &position, precision: precision)
                lon += resultingLon
            } catch {
                return nil
            }
            let location = CLLocation(latitude: lat, longitude: lon)
            decodedCoordinates.append(location)
        }
        
        return decodedCoordinates
    }

    
    fileprivate func decodeSingleCoordinate(byteArray: UnsafePointer<Int8>, length: Int, position: inout Int, precision: Double = 1e6) throws -> Double {
        
        guard position < length else { throw PolylineError.singleCoordinateDecodingError }
        
        let bitMask = Int8(0x1F)
        
        var coordinate: Int32 = 0
        
        var currentChar: Int8
        var componentCounter: Int32 = 0
        var component: Int32 = 0
        
        repeat {
            currentChar = byteArray[position] - 63
            component = Int32(currentChar & bitMask)
            coordinate |= (component << (5*componentCounter))
            position += 1
            componentCounter += 1
        } while ((currentChar & 0x20) == 0x20) && (position < length) && (componentCounter < 6)
        
        if (componentCounter == 6) && ((currentChar & 0x20) == 0x20) {
            throw PolylineError.singleCoordinateDecodingError
        }
        
        if (coordinate & 0x01) == 0x01 {
            coordinate = ~(coordinate >> 1)
        } else {
            coordinate = coordinate >> 1
        }
        
        return Double(coordinate) / precision
    }
    
    enum PolylineError:Error {
        case singleCoordinateDecodingError
        case chunkExtractingError
    }
    
}
