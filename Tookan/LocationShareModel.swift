//
//  LocationShareModel.swift
//  Tookan
//
//  Created by Click Labs on 8/14/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation

class LocationShareModel: NSObject {
    
    var timer: Timer?
    
    var delay10Seconds: Timer?
    
    var bgTask: BackgroundTaskManager?
    
    fileprivate static let shareMyModel = LocationShareModel()
    
    class func sharedModel() -> LocationShareModel {
        return shareMyModel
    }
}
