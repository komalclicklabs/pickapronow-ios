//
//  InvoiceController.swift
//  Tookan
//
//  Created by Rakesh Kumar on 2/29/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class InvoiceController: UIViewController, NavigationDelegate {

    @IBOutlet var background: UIView!
    @IBOutlet weak var invoiceWebView: UIWebView!
    @IBOutlet var topconstraintOfWebView: NSLayoutConstraint!
    var jobId:Int!
    var htmlString:String!
    var webUrl:String!
    var titleName:String!
    
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.background.layer.cornerRadius = 15.0
        setNavigationBar()
        if self.webUrl == nil {
            invoiceWebView.loadHTMLString(htmlString, baseURL: nil)
        } else {
            self.setRefreshController()
            self.loadWebView()
        }
        
        self.topconstraintOfWebView.constant = self.topconstraintOfWebView.constant + Singleton.sharedInstance.getTopInsetofSafeArea()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.perform(#selector(self.updateStatusBar), with: nil, afterDelay: 1.0)
    }
    
    func updateStatusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                UIApplication.shared.statusBarStyle = .lightContent
            } else {
                UIApplication.shared.statusBarStyle = .default
            }
        }
    }
    
    override func loadView() {
        Bundle.main.loadNibNamed(STORYBOARD_ID.invoiceController, owner: self, options: nil)
        self.view.frame = UIScreen.main.bounds
    }

    func setRefreshController(){
        refreshControl.tintColor = COLOR.themeForegroundColor
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        let attributedString = NSMutableAttributedString(string: TEXT.PULL_TO_REFRESH, attributes: [NSForegroundColorAttributeName:COLOR.themeForegroundColor, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.medium)!])//NSAttributedString(string: TEXT.PULL_TO_REFRESH)
        attributedString.addAttribute(NSKernAttributeName, value: 1.8, range: NSRange(location: 0, length: TEXT.PULL_TO_REFRESH.characters.count))
        refreshControl.attributedTitle = attributedString
        self.invoiceWebView.scrollView.addSubview(refreshControl)
    }
    
    func loadWebView() {
        guard IJReachability.isConnectedToNetwork() == true else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            return
        }
        
        let urlString = webUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = URL(string: urlString!) {
            invoiceWebView.loadRequest(URLRequest(url: url))
            invoiceWebView.delegate = self
            ActivityIndicator.sharedInstance.showActivityIndicator()
        }
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        //self.invoiceWebView.reload()
        if let urlString = self.invoiceWebView.request?.url?.absoluteString {
            if urlString.length > 0 {
                webUrl = urlString
            }
        }
        self.loadWebView()
        self.refreshControl.endRefreshing()
    }
    
    //MARK: Navigation Bar
    func setNavigationBar() {
        let navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        if Singleton.sharedInstance.fleetDetails.app_web_view == 1 {
            navigation.titleLabel.text = self.titleName
            self.invoiceWebView.isUserInteractionEnabled = true
        } else if Singleton.sharedInstance.selectedTaskDetails.vertical == VERTICAL.TAXI.rawValue {
            navigation.titleLabel.text = TEXT.RIDE_COMPLETED
            self.invoiceWebView.isUserInteractionEnabled = true
        } else {
            navigation.titleLabel.text = TEXT.INVOICE
            self.invoiceWebView.isUserInteractionEnabled = false
        }
        
        
        self.view.addSubview(navigation)
    }
    
    //MARK: NavigationDelegate Methods
    func backAction() {
        if Singleton.sharedInstance.fleetDetails.app_web_view == 1 {
            self.dismiss(animated: true, completion: nil)
        } else if Singleton.sharedInstance.selectedTaskDetails.vertical == VERTICAL.TOOKAN.rawValue {
            self.dismiss(animated: true, completion: nil)
           // _ = self.navigationController?.popViewController(animated: true)
        } else {
            Singleton.sharedInstance.isTaxiTask = false
            if self.navigationController != nil {
                for controller in (self.navigationController?.viewControllers)! {
                    switch NSStringFromClass(controller.classForCoder).components(separatedBy: ".").last! {
                    case STORYBOARD_ID.homeBaseController:
                        _ = self.navigationController?.popToViewController(controller, animated: true)
                        self.perform(#selector(self.postNotification), with: self, afterDelay: 0.4)
                        break
                    default:
                        break
                    }
                }
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func postNotification() {
        Singleton.sharedInstance.isTaxiTask = false
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
    }
}

extension InvoiceController:UIWebViewDelegate {
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        ActivityIndicator.sharedInstance.hideActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ActivityIndicator.sharedInstance.hideActivityIndicator()
    }
}
