//
//  RelatedTaskController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 16/03/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class RelatedTaskController: UIViewController {

    @IBOutlet var relatedTaskTable: UITableView!
    var navigation:navigationBar!
  
    override func viewDidLoad() {
        super.viewDidLoad()

       // UIApplication.shared.setStatusBarHidden(true, with: .fade)
        /*================= Blur effect ===================*/
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubview(toBack: blurEffectView)
        /*===============================================*/
        self.setTableView()
        self.setNavigationBar()
        
//        if #available(iOS 9.0, *) {
//            if traitCollection.forceTouchCapability == .available {
//                registerForPreviewing(with: self, sourceView: self.relatedTaskTable)
//                //registerForPreviewing(with: cell, sourceView: cell.contentView)
//            } else {
//                print("3D Touch Not Available")
//            }
//        } else {
//            // Fallback on earlier versions
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("received memory warning in RelatedTask")
        // Dispose of any resources that can be recreated.
    }
 
    func setTableView() {
        self.relatedTaskTable.delegate = self
        self.relatedTaskTable.dataSource = self
        self.relatedTaskTable.register(UINib(nibName: NIB_NAME.jobListCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobListCell)
      //  self.relatedTaskTable.estimatedRowHeight = 60
        self.relatedTaskTable.rowHeight = UITableViewAutomaticDimension
        self.relatedTaskTable.layer.cornerRadius = 15.0
        self.relatedTaskTable.layer.masksToBounds = true
    }
}

extension RelatedTaskController:UITableViewDataSource, UITableViewDelegate {
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Singleton.sharedInstance.selectedTaskRelatedArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
        headerView.backgroundColor = UIColor.white
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if #available(iOS 9.0, *) {
            if traitCollection.forceTouchCapability == .available {
                registerForPreviewing(with: self, sourceView: self.relatedTaskTable)
                //registerForPreviewing(with: cell, sourceView: cell.contentView)
            } else {
                print("3D Touch Not Available")
            }
        } else {
            // Fallback on earlier versions
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobListCell) as! JobListCell
        let model = JobModel()
        cell.rightArrow.isHidden = true
        cell.firstLine.attributedText = model.setFirstLine(jobDetails: Singleton.sharedInstance.selectedTaskRelatedArray[indexPath.row], forNewTask: true)
        cell.secondLine.attributedText = model.setSecondLine(jobDetails: Singleton.sharedInstance.selectedTaskRelatedArray[indexPath.row])
        cell.setDotView(indexPath: indexPath, filteredTaskListArray: [Singleton.sharedInstance.selectedTaskRelatedArray], isRelatedTask: true)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Singleton.sharedInstance.selectedTaskDetails = Singleton.sharedInstance.selectedTaskRelatedArray[indexPath.row]
        self.backAction()
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        self.view.window!.layer.add(transition, forKey: kCATransition)
//        self.dismiss(animated: false, completion: { finished in
//            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.reloadDetailTable), object: nil)
//        })
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath) as! JobListCell
        cell.contentView.backgroundColor = UIColor(red: 70/255, green: 149/255, blue: 246/255, alpha: 0.1)
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath) as! JobListCell
        cell.contentView.backgroundColor = UIColor.clear
    }
}

//MARK: Navigation Bar
extension RelatedTaskController:NavigationDelegate {
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = "\(Singleton.sharedInstance.selectedTaskDetails.jobId!)"
        navigation.titleLabel.textAlignment = NSTextAlignment.left
        navigation.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        self.view.addSubview(navigation)
    }
    
    func backAction() {
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        self.view.window!.layer.add(transition, forKey: kCATransition)
       // NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
        self.dismiss(animated: true, completion: { finished in
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.reloadDetailTable), object: nil)
        })
    }
}

@available(iOS 9.0, *)
extension RelatedTaskController: UIViewControllerPreviewingDelegate {

    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = relatedTaskTable.indexPathForRow(at: location),
            let cell = relatedTaskTable.cellForRow(at: indexPath) else {
                return nil
        }
        let taskDetailViewController = storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.detailController) as! DetailController
        taskDetailViewController.preferredContentSize = CGSize(width: 0.0, height: 600)
        taskDetailViewController.fromForceTouch = true
        previewingContext.sourceRect = cell.frame
        Singleton.sharedInstance.selectedTaskDetails = Singleton.sharedInstance.selectedTaskRelatedArray[indexPath.row]
        return taskDetailViewController
    }

    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
//        self.present(self.taskDetailViewController, animated: false, completion: { finished in
//            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.reloadDetailTable), object: nil)
////            self.navigation.alpha = 0
////            self.taskDetailViewController.navigation.alpha = 1
//            self.navigation.titleLabel.text = "\(Singleton.sharedInstance.selectedTaskDetails.jobId!)"
//        })
        self.backAction()
    }
}
