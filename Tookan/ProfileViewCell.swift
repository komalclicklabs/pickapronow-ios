//
//  ProfileViewCell.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 3/28/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol ProfileViewCellDelegate {
    func changeCellHeight(textView:UITextView)
   // func setTextView(textView : UITextView)
    func returnButtonPressed(textView : UITextView)
    func getCellPosition(textView : UITextView) -> Bool
    func checkForTextFieldValidation(textView : UITextView)
}

class ProfileViewCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var textViewActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var rigthImage: UIImageView!
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var verticalLine: UIView!
    @IBOutlet weak var labelBelowTextField: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    let defaultBottomConstraintValue:CGFloat = 20.0
    var delegate : ProfileViewCellDelegate!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        /*=========== vertical line ===============*/
        verticalLine.backgroundColor = COLOR.LINE_COLOR
        
        /*=========== Top Label ================*/
        self.cellLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
        self.cellLabel.textColor = COLOR.EXTRA_LIGHT_COLOR

        /*=============== UITextView ===============*/
        self.textView.textColor = COLOR.LIGHT_COLOR
        self.textView.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)
        self.textView.isEditable = false
        self.textView.delegate = self
        self.textView.isScrollEnabled = false

        /*================ Placeholder Label ==============*/
        self.placeHolderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.medium)
        self.placeHolderLabel.textColor = COLOR.LIGHT_COLOR
        self.placeHolderLabel.isHidden = true

        /*============== Label below Text view ==========*/
        self.labelBelowTextField.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
        self.labelBelowTextField.textColor = COLOR.PROFILE_LINE_ERROR_COLOR
        self.labelBelowTextField.isHidden = true
        
        /*=============== Right Lock Image ==============*/
        self.rigthImage.image = #imageLiteral(resourceName: "iconPasswordActive").withRenderingMode(.alwaysTemplate)
        self.rigthImage.tintColor = COLOR.LIGHT_COLOR
        
        self.bottomConstraint.constant = 0.0
    }

    func setUsernameField(isEditable:Bool, name:String,validationStatus:Bool) {
        self.textView.keyboardType = UIKeyboardType.default
        self.leftImage.image = #imageLiteral(resourceName: "user").withRenderingMode(.alwaysTemplate)
        self.cellLabel.text = TEXT.NAME
        self.textView.text = name
        self.textView.isEditable = isEditable
        self.placeHolderLabel.text = "\(TEXT.ENTER) \(TEXT.NAME)"// + TEXT.NAME
        self.placeHolderLabel.textColor = COLOR.LIGHT_COLOR
        self.placeHolderLabel.isHidden = true
        self.labelBelowTextField.isHidden = true
        self.verticalLine.backgroundColor = COLOR.LINE_COLOR
        self.bottomConstraint.constant = 0.0
        self.textView.returnKeyType = UIReturnKeyType.next
        if isEditable == true {
            self.textView.textColor = COLOR.TEXT_COLOR
            self.leftImage.tintColor = COLOR.TEXT_COLOR
            self.rigthImage.isHidden = true
            if name.isEmpty == true {
                self.placeHolderLabel.isHidden = false
                self.labelBelowTextField.text = ERROR_MESSAGE.PLEASE_ENTER_NAME
                self.verticalLine.backgroundColor = COLOR.ERROR_COLOR
                self.labelBelowTextField.isHidden = false
                self.bottomConstraint.constant = self.defaultBottomConstraintValue

            }
            if validationStatus == false{
                verticalLine.backgroundColor = COLOR.PROFILE_LINE_ERROR_COLOR
                labelBelowTextField.isHidden = false
                labelBelowTextField.text = TEXT.USERNAME_TAKEN
                if bottomConstraint.constant == 0 {
                    bottomConstraint.constant = defaultBottomConstraintValue
                }
            }
        } else {
            self.textView.textColor = COLOR.LIGHT_COLOR
            self.leftImage.tintColor = COLOR.LIGHT_COLOR
            self.rigthImage.isHidden = false
            if name.isEmpty == true {
                self.textView.text = "-"
            }
        }
    }
    
    func setPhoneNumberField(isEditable:Bool, phone:String) {
        self.textView.keyboardType = UIKeyboardType.numberPad
        self.leftImage.image = #imageLiteral(resourceName: "iconContactActive").withRenderingMode(.alwaysTemplate)
        self.cellLabel.text = TEXT.PHONE_NUMBER
        self.textView.text = phone
        self.textView.isEditable = isEditable
        self.placeHolderLabel.text = TEXT.ENTER_PHONE
        self.placeHolderLabel.textColor = COLOR.LIGHT_COLOR
        self.placeHolderLabel.isHidden = true
        self.labelBelowTextField.isHidden = true
        self.verticalLine.backgroundColor = COLOR.LINE_COLOR
        self.bottomConstraint.constant = 0.0
        self.textView.returnKeyType = UIReturnKeyType.next
        if isEditable == true {
            self.textView.textColor = COLOR.TEXT_COLOR
            self.leftImage.tintColor = COLOR.TEXT_COLOR
            self.rigthImage.isHidden = true
            if phone.isEmpty == true {
                self.placeHolderLabel.isHidden = false
            } else if phone.trimText.length < 8 {
                self.labelBelowTextField.text = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.PHONE_NUMBER)"// + TEXT.PHONE_NUMBER
                self.verticalLine.backgroundColor = COLOR.ERROR_COLOR
                self.labelBelowTextField.isHidden = false
                self.bottomConstraint.constant = self.defaultBottomConstraintValue
            }
        } else {
            self.textView.textColor = COLOR.LIGHT_COLOR
            self.leftImage.tintColor = COLOR.LIGHT_COLOR
            self.rigthImage.isHidden = false
            if phone.isEmpty == true {
                self.textView.text = "-"
            }
        }
    }
    
    func setEmailField() {
        self.textView.keyboardType = UIKeyboardType.emailAddress
        self.textView.isEditable = false
        self.leftImage.image = #imageLiteral(resourceName: "iconMailInactive").withRenderingMode(.alwaysTemplate)
        self.cellLabel.text = TEXT.EMAIL
        self.textView.text = Singleton.sharedInstance.fleetDetails.email!
        self.textView.textColor = COLOR.LIGHT_COLOR
        self.leftImage.tintColor = COLOR.LIGHT_COLOR
        self.rigthImage.isHidden = false
        self.placeHolderLabel.textColor = COLOR.LIGHT_COLOR
        self.placeHolderLabel.text = ""
        self.placeHolderLabel.isHidden = true
        self.labelBelowTextField.isHidden = true
        self.verticalLine.backgroundColor = COLOR.LINE_COLOR
        self.bottomConstraint.constant = 0.0
    }
    
    func setOldPassword(isEditable:Bool, oldPassword:String) {
        self.textView.keyboardType = UIKeyboardType.default
        self.leftImage.image = #imageLiteral(resourceName: "iconPasswordActive").withRenderingMode(.alwaysTemplate)
        self.textView.isEditable = isEditable
        self.placeHolderLabel.text = TEXT.OLD_PASSWORD
        self.placeHolderLabel.textColor = COLOR.TEXT_COLOR
        self.placeHolderLabel.isHidden = true
        self.labelBelowTextField.isHidden = true
        self.verticalLine.backgroundColor = COLOR.LINE_COLOR
        self.bottomConstraint.constant = 0.0
        self.textView.returnKeyType = UIReturnKeyType.next
        if isEditable == true {
            var passwordWord = ""
            for _ in 0..<oldPassword.characters.count {
                passwordWord += "*"
            }
            self.textView.text = passwordWord
            self.cellLabel.text = TEXT.OLD_PASSWORD
            self.textView.textColor = COLOR.TEXT_COLOR
            self.leftImage.tintColor = COLOR.TEXT_COLOR
            self.rigthImage.isHidden = true
            if oldPassword.isEmpty == true {
                self.placeHolderLabel.isHidden = false
            } else if oldPassword.length < 6 {
                self.labelBelowTextField.text = ERROR_MESSAGE.SIX_CHAR_PASSWORD
                self.verticalLine.backgroundColor = COLOR.ERROR_COLOR
                self.labelBelowTextField.isHidden = false
                self.bottomConstraint.constant = self.defaultBottomConstraintValue
            }
        } else {
            self.cellLabel.text = TEXT.PASSWORD
            self.textView.text = "******"
            self.textView.textColor = COLOR.LIGHT_COLOR
            self.leftImage.tintColor = COLOR.LIGHT_COLOR
            self.rigthImage.isHidden = false
        }
    }
    
    func setNewPassword(isEditable:Bool, newPassword:String, oldPassword:String) {
        self.textView.keyboardType = UIKeyboardType.default
        self.leftImage.image = #imageLiteral(resourceName: "iconPasswordActive").withRenderingMode(.alwaysTemplate)
        self.cellLabel.text = TEXT.NEW_PASSWORD
        self.textView.isEditable = isEditable
        var passwordWord = ""
        for _ in 0..<newPassword.characters.count {
            passwordWord += "*"
        }
        self.textView.text = passwordWord
        self.placeHolderLabel.text = TEXT.NEW_PASSWORD
        self.placeHolderLabel.textColor = COLOR.TEXT_COLOR
        self.placeHolderLabel.isHidden = true
        self.labelBelowTextField.isHidden = true
        self.verticalLine.backgroundColor = COLOR.LINE_COLOR
        self.bottomConstraint.constant = 0.0
        self.textView.returnKeyType = UIReturnKeyType.next
        if isEditable == true {
            self.textView.textColor = COLOR.TEXT_COLOR
            self.leftImage.tintColor = COLOR.TEXT_COLOR
            self.rigthImage.isHidden = true
            if newPassword.isEmpty == true {
                self.placeHolderLabel.isHidden = false
            }  else if newPassword.length < 6 {
                self.labelBelowTextField.text = ERROR_MESSAGE.SIX_CHAR_PASSWORD
                self.verticalLine.backgroundColor = COLOR.ERROR_COLOR
                self.labelBelowTextField.isHidden = false
                self.bottomConstraint.constant = self.defaultBottomConstraintValue
            } else if oldPassword == newPassword {
                self.labelBelowTextField.text = ERROR_MESSAGE.SAME_PASSWORD
                self.verticalLine.backgroundColor = COLOR.ERROR_COLOR
                self.labelBelowTextField.isHidden = false
                self.bottomConstraint.constant = self.defaultBottomConstraintValue
            } else {
                self.bottomConstraint.constant = 0.0
            }
        } else {
            self.textView.textColor = COLOR.LIGHT_COLOR
            self.leftImage.tintColor = COLOR.LIGHT_COLOR
            self.rigthImage.isHidden = false
        }
    }
    
    func setRetypePassword(isEditable:Bool, retypePassword:String, newPassword:String) {
        self.textView.keyboardType = UIKeyboardType.default
        self.leftImage.image = #imageLiteral(resourceName: "iconPasswordActive").withRenderingMode(.alwaysTemplate)
        self.cellLabel.text = TEXT.RETYPE_PASSWORD
        self.textView.isEditable = isEditable
        var passwordWord = ""
        for _ in 0..<retypePassword.characters.count {
            passwordWord += "*"
        }
        self.textView.text = passwordWord
        self.placeHolderLabel.text = TEXT.RETYPE_PASSWORD
        self.placeHolderLabel.textColor = COLOR.TEXT_COLOR
        self.placeHolderLabel.isHidden = true
        self.labelBelowTextField.isHidden = true
        self.verticalLine.backgroundColor = COLOR.LINE_COLOR
        self.bottomConstraint.constant = 0.0
        self.textView.returnKeyType = UIReturnKeyType.done
        if isEditable == true {
            self.textView.textColor = COLOR.TEXT_COLOR
            self.leftImage.tintColor = COLOR.TEXT_COLOR
            self.rigthImage.isHidden = true
             if newPassword != retypePassword {
                self.labelBelowTextField.text = ERROR_MESSAGE.DIFF_PASSWORD
                self.verticalLine.backgroundColor = COLOR.ERROR_COLOR
                self.labelBelowTextField.isHidden = false
                self.bottomConstraint.constant = self.defaultBottomConstraintValue
             } else if retypePassword.length == 0 {
                self.placeHolderLabel.isHidden = false
            }
        } else {
            self.textView.textColor = COLOR.LIGHT_COLOR
            self.leftImage.tintColor = COLOR.LIGHT_COLOR
            self.rigthImage.isHidden = false
        }
    }
    
    func setTeamsCell(isEditable:Bool, selectedValue:String) {
        self.leftImage.image = #imageLiteral(resourceName: "teamIcon").withRenderingMode(.alwaysTemplate)
        self.textView.text = selectedValue
        self.textView.isEditable = isEditable
        self.cellLabel.text = TEXT.TEAMS
        self.placeHolderLabel.text = TEXT.TEAMS
        self.placeHolderLabel.textColor = COLOR.TEXT_COLOR
        self.placeHolderLabel.isHidden = true
        self.labelBelowTextField.isHidden = true
        self.verticalLine.backgroundColor = COLOR.LINE_COLOR
        self.bottomConstraint.constant = 0.0
        if isEditable == true {
            self.textView.textColor = COLOR.TEXT_COLOR
            self.leftImage.tintColor = COLOR.TEXT_COLOR
            self.rigthImage.isHidden = true
        } else {
            self.textView.textColor = COLOR.LIGHT_COLOR
            self.leftImage.tintColor = COLOR.LIGHT_COLOR
            self.rigthImage.isHidden = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        let returnValue = delegate?.getCellPosition(textView: textView)
        let fieldType:PROFILE_FIELDS = PROFILE_FIELDS(rawValue: textView.tag)!
        switch fieldType {
        case .teams:
            self.verticalLine.backgroundColor = COLOR.LINE_COLOR
        default:
            self.verticalLine.backgroundColor = COLOR.PROFILE_LINE_COLOR
        }
        
        return returnValue!
    }

    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            delegate?.returnButtonPressed(textView : textView)
            return false
        } else {
            let fieldType:PROFILE_FIELDS = PROFILE_FIELDS(rawValue: textView.tag)!
            switch fieldType {
            case .phone:
                if (textView.text + text).length > 16 {
                    return false
                }
                return true
            case .name:
                if textView.text.length == 0 {
                    if text == " " {
                        return false
                    }
                }
                return true
            case .oldPassword:
                return true
            case .newPassword:
                return true
            case .retypePassword:
                return true
            default:
                return true
            }
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty == true {
            placeHolderLabel.isHidden = false
        } else {
            placeHolderLabel.isHidden = true
        }
        
        delegate?.changeCellHeight(textView : textView)
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        self.delegate?.checkForTextFieldValidation(textView: textView)
        return true
    }

 
}
