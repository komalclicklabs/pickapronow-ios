//
//  CancelSlider.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 4/21/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

protocol CustomSliderDelegate {
    func sliderPositiveAction()
    func sliderNegativeAction()
}

import UIKit

enum PAN_GESTURE_DIRECTION {
    case right
    case left
}

class CustomSlider: UIView ,UIGestureRecognizerDelegate{

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var slidingImage: UIImageView!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var cancelLabel: UILabel!
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var directionArrow: UIImageView!
    @IBOutlet var shimmerView: FBShimmeringView!
    @IBOutlet var thumbImageCenterConstraint: NSLayoutConstraint!
    
    var positiveRed:CGFloat = 0.0
    var positiveGreen:CGFloat = 0.0
    var positiveBlue:CGFloat = 0.0
    var negativeRed:CGFloat = 0.0
    var negativeGreen:CGFloat = 0.0
    var negativeBlue:CGFloat = 0.0
    var loadingText:String!
    var statusText:String!
    var direction:PAN_GESTURE_DIRECTION!
    var delegate:CustomSliderDelegate?
    var hideLeftButton = false
    var maxSlidingLimit:CGFloat = 0.0
    var alphaFactorValue:CGFloat = 1.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(valueChanged(pan:)))
        panGesture.delegate = self
        slidingImage.addGestureRecognizer(panGesture)
        
        let positiveTap = UITapGestureRecognizer(target: self, action: #selector(positiveTapAction(tap:)))
        positiveTap.numberOfTapsRequired = 1
        startLabel.addGestureRecognizer(positiveTap)
        
        let negativeTap = UITapGestureRecognizer(target: self, action: #selector(negativeTapAction(tap:)))
        negativeTap.numberOfTapsRequired = 1
        cancelLabel.addGestureRecognizer(negativeTap)

        //self.centerLabel.isHidden = true
        self.centerLabel.alpha = 0.0
        self.centerLabel.textColor = UIColor.white
        self.centerLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        
        self.cancelLabel.textColor = COLOR.LIGHT_COLOR
        self.cancelLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        
        self.startLabel.textColor = COLOR.TEXT_COLOR
        self.startLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        
        self.backgroundColor = COLOR.ACCEPT_TITLE_COLOR
    }
    
    func setThumbSliderAndMaxLimit() {
        if self.hideLeftButton == true {
            self.slidingImage.image = #imageLiteral(resourceName: "startRideGreen")
            self.thumbImageCenterConstraint.constant = -((SCREEN_SIZE.width / 2) - self.slidingImage.frame.width + 10)
            self.maxSlidingLimit = SCREEN_SIZE.width - self.slidingImage.frame.width - 20
            self.alphaFactorValue = 1.0
        } else {
            self.slidingImage.image = #imageLiteral(resourceName: "startRide")
            self.thumbImageCenterConstraint.constant = 0
            self.maxSlidingLimit = (self.frame.width - self.slidingImage.frame.width)/2
            self.alphaFactorValue = 2.0
        }
        
        self.slidingImage.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
    }
    
    
    func setShimmerView() {
        /*============== Shimmer View =============*/
        self.shimmerView.contentView = self.directionArrow
        self.shimmerView.isShimmering = true
        self.shimmerView.shimmeringPauseDuration = 0.3
        self.shimmerView.shimmeringAnimationOpacity = 1.0
        self.shimmerView.shimmeringOpacity = 0.3
        self.shimmerView.shimmeringSpeed = 50
        self.shimmerView.shimmeringHighlightLength = 0.5
        self.shimmerView.shimmeringDirection = FBShimmerDirection.right
        self.shimmerView.shimmeringBeginFadeDuration = 0.1
        self.shimmerView.shimmeringEndFadeDuration = 0.3
    }
    
    
    func setLabeltext(left:String,right:String,center:String, loadingText:String){
        self.cancelLabel.text = left
        self.startLabel.text = right
        self.centerLabel.text = ""
        self.loadingText = loadingText
        self.statusText = center
    }
    
    func valueChanged(pan:UIPanGestureRecognizer){
        let translation  = pan.translation(in: pan.view)
        var transform = pan.view?.transform

        if(translation.x > 0) {//Right Side Slide
            self.directionArrow.alpha = 0.0
            self.shimmerView.alpha = 0.0

            let alpha = self.alphaFactorValue * (translation.x / (self.frame.width - self.slidingImage.frame.width ))
            self.direction = PAN_GESTURE_DIRECTION.right
            self.backgroundView.backgroundColor = UIColor(red: self.positiveRed, green: self.positiveGreen, blue: self.positiveBlue, alpha: alpha)
            self.startLabel.alpha = 1 - alpha
            self.centerLabel.alpha = alpha
            self.centerLabel.text = self.startLabel.text
            if self.hideLeftButton == false {
                self.cancelLabel.alpha = 1 - alpha
            }
            if(pan.state == .changed){
                if(translation.x > 0 && translation.x < self.maxSlidingLimit){
                    transform?.tx = translation.x
                    self.slidingImage.transform = transform!
                }
            }
            if(pan.state == .ended){
                if(slidingImage.frame.maxX <= self.startLabel.frame.minX  - 30){
                    self.setAlphaForLabels()
                }else{
                    self.backgroundView.backgroundColor = UIColor(red: self.positiveRed, green: self.positiveGreen, blue: self.positiveBlue, alpha: 1.0)
                    self.slideAction()
                    self.delegate?.sliderPositiveAction()
                }
            }
        } else { //Left Side Slide
            guard hideLeftButton == false else {
                self.setAlphaForLabels()
                return
            }
            
            self.directionArrow.alpha = 0.0
            self.shimmerView.alpha = 0.0

            self.direction = PAN_GESTURE_DIRECTION.left
            let alpha = self.alphaFactorValue * abs(translation.x / (self.frame.width - self.slidingImage.frame.width ))
            self.backgroundView.backgroundColor = UIColor(red:self.negativeRed , green:self.negativeGreen , blue:self.negativeBlue , alpha: alpha)
            self.startLabel.alpha = 1 - alpha
            self.centerLabel.alpha = alpha
            self.centerLabel.text = self.cancelLabel.text
            if self.hideLeftButton == false {
                self.cancelLabel.alpha = 1 - alpha
            }
            if(pan.state == .changed){
                if(translation.x < 0 && translation.x > -self.maxSlidingLimit){
                    transform?.tx = translation.x
                    self.slidingImage.transform = transform!
                }
            }

            if(pan.state == .ended){
                if(slidingImage.frame.minX > self.cancelLabel.frame.maxX + 30){
                    self.setAlphaForLabels()
                } else {
                    self.backgroundView.backgroundColor = UIColor(red: self.negativeRed, green: self.negativeGreen, blue: self.negativeBlue, alpha: 1.0)
                    self.slideAction()
                    self.delegate?.sliderNegativeAction()
                }
            }
        }
    }
    
    func setAlphaForLabels() {
        self.backgroundView.backgroundColor = COLOR.ACCEPT_TITLE_COLOR
        self.slidingImage.transform = CGAffineTransform.identity
        self.startLabel.alpha = 1.0
        self.slidingImage.alpha = 1.0
        self.directionArrow.alpha = 1.0
        self.shimmerView.alpha = 1.0
        self.centerLabel.alpha = 0.0
        if hideLeftButton == true {
            self.cancelLabel.alpha = 0.0
        } else {
            self.cancelLabel.alpha = 1.0
        }
    }
    
    
    func slideAction() {
        self.directionArrow.alpha = 0.0
        self.cancelLabel.alpha = 0.0
        self.startLabel.alpha = 0.0
        self.centerLabel.alpha = 1.0
        self.slidingImage.alpha = 0.0
        self.shimmerView.alpha = 0.0
        switch(direction!) {
            case PAN_GESTURE_DIRECTION.left:
                self.backgroundView.backgroundColor = UIColor(red: self.negativeRed, green: self.negativeGreen, blue: self.negativeBlue, alpha: 1.0)
                break
            case PAN_GESTURE_DIRECTION.right:
                self.backgroundView.backgroundColor = UIColor(red: self.positiveRed, green: self.positiveGreen, blue: self.positiveBlue, alpha: 1.0)
                break
        }
    }
        
    func resetView() {
        self.slidingImage.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.35, animations: {
        //UIView.animate(withDuration: 0.5, delay:0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.slidingImage.transform = CGAffineTransform.identity
            self.backgroundView.alpha = 0.0
            self.directionArrow.alpha = 1.0
            self.startLabel.alpha = 1.0
            self.centerLabel.alpha = 0.0
            self.slidingImage.alpha = 1.0
            self.shimmerView.alpha = 1.0
            if self.hideLeftButton == true {
                self.cancelLabel.alpha = 0.0
            } else {
                self.cancelLabel.alpha = 1.0
            }
        }, completion: { finished in
            self.backgroundView.backgroundColor = COLOR.ACCEPT_TITLE_COLOR
            self.backgroundView.alpha = 1.0
        })
    }
    
    func setPositiveSlidingColor(r:CGFloat,g:CGFloat,b:CGFloat){
        self.positiveRed = r
        self.positiveGreen = g
        self.positiveBlue = b
    }
    
    func setNegativeSlidingColor(r:CGFloat,g:CGFloat,b:CGFloat) {
        self.negativeRed = r
        self.negativeGreen = g
        self.negativeBlue = b
    }
    
    func positiveTapAction(tap:UITapGestureRecognizer) {
//        self.direction = PAN_GESTURE_DIRECTION.right
//        UIView.animate(withDuration: 0.2, animations: {
//            self.slidingImage.transform = CGAffineTransform(translationX: (self.frame.width - self.slidingImage.frame.width)/2, y: 0)
//        }, completion: { finished in
//            self.slideAction()
//            self.delegate?.sliderPositiveAction()
//        })
    }
    
    func negativeTapAction(tap:UITapGestureRecognizer){
        self.direction = PAN_GESTURE_DIRECTION.left
        UIView.animate(withDuration: 0.2, animations: {
            self.slidingImage.transform = CGAffineTransform(translationX: -(self.frame.width - self.slidingImage.frame.width)/2, y: 0)
        }, completion: { finished in
            self.slideAction()
            self.delegate?.sliderNegativeAction()
        })
    }


}
