//
//  SettingViewCell.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 3/21/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class SettingViewCell: UITableViewCell {

    
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var settingSwitch: UISwitch!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var labelLeadingValue = 20.0

    @IBOutlet var cellLabelLeading: NSLayoutConstraint!
    
    @IBOutlet var iconLabelLeading: NSLayoutConstraint!
    @IBOutlet weak var labelLeadingConstrint: NSLayoutConstraint!
    var defaultLabelLeadingValue = 47.0
    override func awakeFromNib() {
       
        super.awakeFromNib()
        /*=============== Hidding Controls ===================*/
        self.icon.isHidden = false
        self.settingSwitch.isHidden = true
        self.activityIndicator.isHidden = true

        
        /*=============== Setting Label ===================*/
        self.settingLabel.font =  UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.settingLabel.textColor = COLOR.TEXT_COLOR
        
        /*============= Setting Button ==================*/
        self.settingButton.setTitleColor(COLOR.themeForegroundColor, for: .normal)
        self.settingButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.settingButton.isHidden = false
        
        self.iconLabelLeading.isActive = false
        self.cellLabelLeading.isActive = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
