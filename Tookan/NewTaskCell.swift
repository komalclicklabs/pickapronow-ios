//
//  NewTaskCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 20/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol NewTaskCellDelegate {
    func deleteTaskAfterTimerCompletion(jobId:String)
}


class NewTaskCell: UITableViewCell {

    @IBOutlet var dot: UIView!
    @IBOutlet var firstLine: UILabel!
    @IBOutlet var secondLine: UILabel!
    @IBOutlet var patternLine: UIView!
    @IBOutlet var declineButton: UIButton!
    @IBOutlet var accept: UIView!
    @IBOutlet var cardView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    var bottomConstant:CGFloat = 64
    var delegate:NewTaskCellDelegate!
    var timerButton:TimerButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*================= DECLINE BUTTON =====================*/
        self.declineButton.setTitle(TEXT.DECLINE, for: .normal)
        self.declineButton.setTitleColor(COLOR.DECLINE_TITLE_COLOR, for: .normal)
        self.declineButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        
        /*================= ACCEPT BUTTON ======================*/
        self.accept.backgroundColor = COLOR.ACCEPT_BACKGROUND_COLOR
        self.accept.layer.cornerRadius = 7.0
        self.accept.layer.masksToBounds = true
        
        patternLine.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "iconPattern"))
    }
    


    func setAcceptButton(title:String, declineTitle:String, seconds:Int, jobId:String) {
        self.removeTimerButton()
        timerButton = UINib(nibName: NIB_NAME.timerButton, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TimerButton
        timerButton.frame = CGRect(x: 0, y: 0, width: accept.frame.width, height: accept.frame.height)
        timerButton.jobId = jobId
        timerButton.delegate = self
        self.accept.addSubview(timerButton)
        self.timerButton.setTitle(title: title)
        self.timerButton.setTimer(seconds: seconds)
        self.declineButton.setTitle(declineTitle, for: .normal)
        if declineTitle.length == 0 {
            self.declineButton.isHidden = true
            let centerX = self.center.x - self.accept.center.x
            self.accept.transform = CGAffineTransform(translationX: centerX, y: 0)
        } else {
            self.accept.transform = CGAffineTransform.identity
        }
    }
    
    func setDotView(indexPath:IndexPath, newTaskListArray:[[TasksAssignedToDate]]) {
        var dotColor:UIColor!
        var borderColor:UIColor!
        var isPatterHidden:Bool!
        
        if newTaskListArray[indexPath.section].count > 1 {
            if indexPath.row == 0 {
                dotColor = UIColor.black
                borderColor = UIColor.white
                isPatterHidden = false
            } else {
                dotColor = UIColor.white
                borderColor = UIColor.black
                if indexPath.row == newTaskListArray[indexPath.section].count - 1 {
                    isPatterHidden = true
                } else {
                    isPatterHidden = false
                }
            }
        } else {
            dotColor = UIColor.black
            borderColor = UIColor.white
            isPatterHidden = true
        }
        
        self.dot.backgroundColor = dotColor
        self.dot.layer.cornerRadius = 4.5
        self.dot.layer.borderWidth = 0.5
        self.dot.layer.borderColor = borderColor.cgColor
        self.dot.layer.masksToBounds = true
        self.patternLine.isHidden = isPatterHidden
    }

    func setCornerRadius(indexPath:IndexPath, newTaskListArray:[[TasksAssignedToDate]]) {
        if newTaskListArray[indexPath.section].count > 1 {
            if indexPath.row == 0 {
                self.topView.roundCorners(corners: [.topLeft, .topRight], radius: 15.0, width: SCREEN_SIZE.width - 26, height: 20)
                self.bottomView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 0.0, width: SCREEN_SIZE.width - 26, height: 20)
            } else if indexPath.row == newTaskListArray[indexPath.section].count - 1 {
                self.topView.roundCorners(corners: [.topLeft, .topRight], radius: 0.0, width: SCREEN_SIZE.width - 26, height: 20)
                self.bottomView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 15.0, width: SCREEN_SIZE.width - 26, height: 20)
            } else {
                self.topView.roundCorners(corners: [.topLeft, .topRight], radius: 0.0, width: SCREEN_SIZE.width - 26, height: 20)
                self.bottomView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 0.0, width: SCREEN_SIZE.width - 26, height: 20)
            }
        } else {
            self.topView.roundCorners(corners: [.topLeft, .topRight], radius: 15.0, width: SCREEN_SIZE.width - 26, height: 20)
            self.bottomView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 15.0, width: SCREEN_SIZE.width - 26, height: 20)
        }
    }
    
    func setAcceptDeclineButton(indexPath:IndexPath, newTaskListArray:[[TasksAssignedToDate]]) {
        var acceptTitle = ""
        var declineTitle = ""
        let task = newTaskListArray[indexPath.section][indexPath.row]
        switch (task.accept_button!) {
        case ACCEPT_TYPE.acknowledge:
            acceptTitle = BUTTON_TITLE.acknowledge
            break
        case ACCEPT_TYPE.acceptDecline:
            acceptTitle = BUTTON_TITLE.accept
            declineTitle = BUTTON_TITLE.decline
            break
        case ACCEPT_TYPE.startCancel:
            acceptTitle = TEXT.OK
            break
        default:
            break
        }
        
        let timeInterval = Int(Date().timeIntervalSince(task.jobCreationTime))
        var timeInSeconds = 0
        if task.time_left_in_seconds! > 0 {
            if timeInterval >= task.time_left_in_seconds! {
                timeInSeconds = 1
            } else {
                timeInSeconds = task.time_left_in_seconds! - timeInterval
            }
        }
        
        
        if newTaskListArray[indexPath.section].count > 1 {
            if indexPath.row == 0 {
                self.setHideStatusOfButton(status: true)
                self.removeTimerButton()
            } else if indexPath.row == newTaskListArray[indexPath.section].count - 1 {
                self.setHideStatusOfButton(status: false)
                self.setAcceptButton(title: acceptTitle, declineTitle: declineTitle, seconds: timeInSeconds, jobId: "\(newTaskListArray[indexPath.section][0].jobId!)")
            } else {
                self.setHideStatusOfButton(status: true)
                self.removeTimerButton()
            }
        } else {
            self.setHideStatusOfButton(status: false)
            self.setAcceptButton(title: acceptTitle, declineTitle: declineTitle, seconds: timeInSeconds, jobId: "\(newTaskListArray[indexPath.section][0].jobId!)")
        }
    }
    
    func setHideStatusOfButton(status:Bool) {
        self.accept.isHidden = status
        self.declineButton.isHidden = status
        if status == false {
            self.bottomConstraint.constant = bottomConstant
        } else {
            self.bottomConstraint.constant = 0
        }
        self.updateConstraints()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func removeTimerButton() {
        if timerButton != nil {
            timerButton.timerLabel.cancel()
            timerButton.timerLabel.cancel({
                self.timerButton.timerLabel.removeFromSuperview()
                self.timerButton.timerLabel = nil
                self.timerButton.delegate = nil
                self.timerButton.removeFromSuperview()
                self.timerButton = nil
            })
        }
    }
    
}

extension NewTaskCell:TimerDelegate {
    func timerFinished(jobId: String) {
        self.delegate.deleteTaskAfterTimerCompletion(jobId: jobId)
    }
}
