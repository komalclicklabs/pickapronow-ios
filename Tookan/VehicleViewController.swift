//
//  VehicleViewController.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 3/31/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol VehicleViewControllerDelegate{
    func vehicleChanged()
}

class VehicleViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {

    @IBOutlet var labelTop: NSLayoutConstraint!
    @IBOutlet var mainViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var firstLine: UIView!
    @IBOutlet weak var secondLine: UIView!
    @IBOutlet weak var thirdLine: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var vehicleTitle: UILabel!
    @IBOutlet weak var doneSelection: UIButton!
    @IBOutlet var spacingBetweenLabelAnddone: NSLayoutConstraint!
    @IBOutlet var doneButtonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var doneButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    
    var vehicleListArray = [TEXT.WALKING,TEXT.CYCLE,TEXT.SCOOTER,TEXT.BIKE,TEXT.CAR,TEXT.TRUCK]
    var vehicleIconArray = [#imageLiteral(resourceName: "walkingMan"),#imageLiteral(resourceName: "bike"),#imageLiteral(resourceName: "scooter"),#imageLiteral(resourceName: "motorbike"),#imageLiteral(resourceName: "car"),#imageLiteral(resourceName: "truck")]
    var viewCornerRadius:CGFloat = 15.0
    var viewTopConstraintValue = getAspectRatioValue(value: 179.0)
    var viewTopConstraintBaseValue = getAspectRatioValue(value: 550.0)
    var cellSpacingFromCollectionView = getAspectRatioValue(value: 5)
    var keyBoardOn  = false
    var selectedIndex = 4
    var internetToast:NoInternetConnectionView!
    var model : String = ""
    var color : String = ""
    var lisencePlate : String = ""
    var delegate : VehicleViewControllerDelegate?
    
    enum VehicleType:Int {
        case walking
        case cycle
        case scooter
        case bike
        case car
        case truck
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*============== Tap gesture ===================*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        tap.delegate = self
        tap.numberOfTapsRequired = 1
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        /*============== Label title ===================*/
        self.vehicleTitle.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
        self.vehicleTitle.text = TEXT.CHOOSE_VEHICLE_TYPE
        
        self.setCollectionView()
        self.setTextFields()
        
        
        self.mainView.transform = CGAffineTransform(translationX: 0, y: SCREEN_SIZE.height)
        
        /*============== Done Button  ===================*/
        self.doneSelection.setTitle(TEXT.DONE_EDITING, for: .normal)
        self.doneSelection.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.doneSelection.backgroundColor = COLOR.themeForegroundColor
        self.spacingBetweenLabelAnddone.isActive = false
        self.doneSelection.isHidden = true
        
        
        self.model = Singleton.sharedInstance.fleetDetails.transportDesc!
        self.lisencePlate = Singleton.sharedInstance.fleetDetails.license!
        self.color = Singleton.sharedInstance.fleetDetails.color!
    }
    
    override func loadView() {
        Bundle.main.loadNibNamed(NIB_NAME.VehicleViewController, owner: self, options: nil)
        self.mainView.frame = UIScreen.main.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let path = UIBezierPath(roundedRect:CGRect(x: self.mainView.bounds.origin.x, y: self.mainView.bounds.origin.y, width: SCREEN_SIZE.width - 10, height: self.mainView.frame.height), byRoundingCorners:[.topLeft, .topRight], cornerRadii: CGSize(width: viewCornerRadius, height: viewCornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds;
        maskLayer.path = path.cgPath
        self.mainView.layer.mask = maskLayer;
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.showView()
        collectionView.delegate?.collectionView!(self.collectionView, didDeselectItemAt: IndexPath(row: 0, section: 0))
        collectionView.delegate?.collectionView!(self.collectionView, didSelectItemAt: IndexPath(row: selectedIndex, section: 0))
        self.collectionView.selectItem(at: IndexPath(row: selectedIndex, section: 0), animated: false, scrollPosition: .left)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //Mark: Setting Text Fields
    func setTextFields(){
        self.firstTextField.delegate = self
        self.secondTextField.delegate = self
        self.thirdTextField.delegate = self
        self.firstTextField.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.secondTextField.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.thirdTextField.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.firstLine.backgroundColor = COLOR.LINE_COLOR
        self.secondLine.backgroundColor = COLOR.LINE_COLOR
        self.thirdLine.backgroundColor = COLOR.LINE_COLOR
    }
    
    //Mark Setting CollectionView
    func setCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: NIB_NAME.VehicleCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.VehicleCollectionViewCell)
        selectedIndex = self.setVehicleType(Singleton.sharedInstance.fleetDetails.transportType!)
    }
    
    func showView(){
        self.mainView.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .beginFromCurrentState, animations: {
            self.mainView.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    func dismissView(){
        if  self.keyBoardOn == false{
            UIView.animate(withDuration: 0.3, animations: {
                self.mainView.transform = CGAffineTransform(translationX: 0, y: self.mainView.frame.height)
            }, completion: { finished in
                self.dismiss(animated: false, completion: nil)
            })} else{
            self.view.endEditing(true)
        }
    }
    
    
    
    //MARK: UICollectionView Delegate methods
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: getAspectRatioValue(value: 15), bottom: 0, right: cellSpacingFromCollectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.vehicleListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.VehicleCollectionViewCell, for: indexPath) as! VehicleCollectionViewCell
        cell.cellButton.setImage(self.vehicleIconArray[indexPath.row].withRenderingMode(.alwaysTemplate), for: .normal)
        cell.cellButton.tintColor = COLOR.TEXT_COLOR
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            walkingCellSelected(indexPath: indexPath)
        case 1:
            cycleCellSelected(indexPath: indexPath)
        default:
            cellSelected(indexPath: indexPath)
        }
        
        selectedIndex = indexPath.row

        if(self.setVehicleType(Singleton.sharedInstance.fleetDetails.transportType!) != selectedIndex){
            self.doneSelection.isHidden = false
            self.firstTextField.text = ""
            self.secondTextField.text = ""
            self.thirdTextField.text = ""
        }else{
            self.doneSelection.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width:CGFloat = getAspectRatioValue(value: 47)
        let height:CGFloat = getAspectRatioValue(value: 47)
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! VehicleCollectionViewCell
        cell.cellButton.backgroundColor = COLOR.VEHICLE_BUTTON_COLOR
        cell.cellButton.tintColor = COLOR.TEXT_COLOR
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func walkingCellSelected(indexPath : IndexPath){
        let cell = collectionView.cellForItem(at: indexPath) as! VehicleCollectionViewCell
        cell.cellButton.backgroundColor = COLOR.VEHICLE_CELL_SELECTION_COLOR
        cell.cellButton.tintColor = COLOR.ACCEPT_TITLE_COLOR
        self.firstTextField.isHidden = true
        self.firstLine.isHidden = true
        self.secondLine.isHidden = true
        self.thirdLine.isHidden = true
        self.secondTextField.isHidden = true
        self.thirdTextField.isHidden = true
        
    }
    
    func cycleCellSelected(indexPath:IndexPath){
        let cell = collectionView.cellForItem(at: indexPath) as! VehicleCollectionViewCell
        cell.cellButton.backgroundColor = COLOR.VEHICLE_CELL_SELECTION_COLOR
        cell.cellButton.tintColor = COLOR.ACCEPT_TITLE_COLOR
        self.firstTextField.isHidden = false
        self.firstTextField.returnKeyType = UIReturnKeyType.done
        self.firstLine.isHidden = false
        self.secondLine.isHidden = true
        self.thirdLine.isHidden = true
        self.secondTextField.isHidden = true
        self.thirdTextField.isHidden = true
        let attributes = [
            NSForegroundColorAttributeName: COLOR.TEXT_COLOR,
            NSFontAttributeName :  UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        ]
        self.firstTextField.attributedPlaceholder = NSAttributedString(string: TEXT.COLOR,attributes: attributes)
        if  Singleton.sharedInstance.fleetDetails.color != ""{
            self.firstTextField.text = Singleton.sharedInstance.fleetDetails.color
        }
    }
    
    func cellSelected(indexPath:IndexPath){
        let cell = collectionView.cellForItem(at: indexPath) as! VehicleCollectionViewCell
        cell.cellButton.backgroundColor = COLOR.VEHICLE_CELL_SELECTION_COLOR
        cell.cellButton.tintColor = COLOR.ACCEPT_TITLE_COLOR
        self.firstTextField.isHidden = false
        self.firstLine.isHidden = false
        self.secondLine.isHidden = false
        self.thirdLine.isHidden = false
        self.secondTextField.isHidden = false
        self.thirdTextField.isHidden = false
        self.firstTextField.returnKeyType = UIReturnKeyType.next
        self.secondTextField.returnKeyType = UIReturnKeyType.next
        self.thirdTextField.returnKeyType = UIReturnKeyType.done
        let attributes = [
            NSForegroundColorAttributeName: COLOR.TEXT_COLOR,
            NSFontAttributeName :  UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        ]
        self.firstTextField.attributedPlaceholder = NSAttributedString(string: TEXT.MODEL,attributes: attributes)
        self.secondTextField.attributedPlaceholder = NSAttributedString(string: TEXT.LICENCE_PLATE,attributes: attributes)
        self.thirdTextField.attributedPlaceholder = NSAttributedString(string: TEXT.COLOR,attributes: attributes)
        
        if Singleton.sharedInstance.fleetDetails.color != ""{
            self.thirdTextField.text = Singleton.sharedInstance.fleetDetails.color
        }
        if Singleton.sharedInstance.fleetDetails.license != ""{
            self.secondTextField.text = self.lisencePlate
        }
        if Singleton.sharedInstance.fleetDetails.transportDesc != ""{
            self.firstTextField.text = self.model
        }
    }
    
    //MARK: Dismiss keyboard
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(touches.first?.view == self.mainView){
            self.mainView.endEditing(true)
        }
        
    }
    
    @IBAction func doneSelection(_ sender: Any) {
        self.view.endEditing(true)
        
        if IJReachability.isConnectedToNetwork() == false {
            self.showInternetToast()
        } else {
            self.doneSelection.isEnabled = false
            let type = self.getVehicleType(selectedIndex)
            switch type {
            case 0:
                self.model = ""
                self.color = ""
                self.lisencePlate = ""
            case 1:
                self.model = ""
                self.lisencePlate = ""
                self.color = self.firstTextField.text!
            default:
                self.model = self.firstTextField.text!
                self.lisencePlate = self.secondTextField.text!
                self.color = self.thirdTextField.text!
            }
            var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
            params["transport_type"] = "\(type)"
            params["transport_desc"] = self.model
            params["license"] = self.lisencePlate
            params["color"] = self.color
            params["timezone"] = -(NSTimeZone.system.secondsFromGMT() / 60)
            params["device_token"] = UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) as! String
            params["device_type"] = DEVICE_TYPE
//            let params = [
//                "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//                "transport_type":"\(type)",
//                "transport_desc":self.model,
//                "license":self.lisencePlate,
//                "color":self.color,
//                "timezone":-(NSTimeZone.system.secondsFromGMT() / 60),
//                "device_token":UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) as! String,
//                "device_type":DEVICE_TYPE
//                ] as [String : Any]
            self.doneButtonActivityIndicator.startAnimating()
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.editProfile, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async(execute: {
                    self.doneSelection.isEnabled = true
                    self.doneButtonActivityIndicator.stopAnimating()
                    if(isSucceeded == true) {
                        Singleton.sharedInstance.fleetDetails.transportType = "\(type)"
                        Singleton.sharedInstance.fleetDetails.transportDesc = self.model
                        Singleton.sharedInstance.fleetDetails.license = self.lisencePlate
                        Singleton.sharedInstance.fleetDetails.color = self.color
                        self.delegate?.vehicleChanged()
                        self.dismissView()
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                })
            })
        }
    }
    @IBAction func panExit(_ sender: Any) {
        let pan = sender as! UIPanGestureRecognizer
        let translation  = pan.translation(in: pan.view?.superview)
        var theTransform = pan.view?.transform
        if pan.state == UIGestureRecognizerState.changed{
            if translation.y > 0 {
                theTransform?.ty = translation.y
                mainView.transform = theTransform!
            }
        }else if pan.state == UIGestureRecognizerState.ended{
            if translation.y > 50 {
                self.dismissView()
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.mainView.transform = CGAffineTransform.identity
                })
            }
        }
        
    }
    
    //MARK: Handling Keyboard Toggle
    func keyboardWillShow(notification: NSNotification) {
        self.keyBoardOn = true
        labelTop.constant = -105
        self.view.setNeedsLayout()
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
            self.mainView.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: false)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        keyBoardOn = false
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        
        labelTop.constant = 13
        self.view.setNeedsLayout()
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { _ in
            self.view.layoutIfNeeded()
        }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }
    
    func setVehicleType(_ code: String) -> Int {
        switch code {
        case "1":
            return VehicleType.car.rawValue
        case "2":
            return VehicleType.bike.rawValue
        case "3":
            return VehicleType.cycle.rawValue
        case "4":
            return VehicleType.scooter.rawValue
        case "5":
            return VehicleType.walking.rawValue
        case "6":
            return VehicleType.truck.rawValue
        default:
            return 0
        }
    }
    func getVehicleType(_ selectedIndex:Int) -> Int {
        switch selectedIndex {
        case VehicleType.walking.rawValue:
            return 5
        case VehicleType.cycle.rawValue:
            return 3
        case VehicleType.scooter.rawValue:
            return 4
        case VehicleType.bike.rawValue:
            return 2
        case VehicleType.car.rawValue:
            return 1
        case VehicleType.truck.rawValue:
            return 6
        default:
            return 0
        }
    }
    
    //MARK: UITextField Delegate Methods
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(self.selectedIndex == 1){
            if(firstTextField.text != Singleton.sharedInstance.fleetDetails.color){
                self.doneSelection.isHidden = false
            }else{
                self.doneSelection.isHidden = true
            }
        }else{
            guard self.selectedIndex == self.setVehicleType(Singleton.sharedInstance.fleetDetails.transportType!) else {
                self.doneSelection.isHidden = false
                return
            }
            if(self.firstTextField.text != Singleton.sharedInstance.fleetDetails.transportDesc){
                self.doneSelection.isHidden = false
            } else if(self.secondTextField.text != Singleton.sharedInstance.fleetDetails.license){
                self.doneSelection.isHidden = false
            } else if(thirdTextField.text != Singleton.sharedInstance.fleetDetails.color){
                self.doneSelection.isHidden = false
            } else{
                self.doneSelection.isHidden = true
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstTextField:
            if self.selectedIndex == 1 {
                textField.resignFirstResponder()
            } else {
                self.secondTextField.becomeFirstResponder()
            }
        case secondTextField:
            if self.selectedIndex == 1 {
                textField.resignFirstResponder()
            } else {
                self.thirdTextField.becomeFirstResponder()
            }
        case thirdTextField:
            textField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
            break
        }
        return false
    }
    

    
}

//MARK: UIGestureRecognizerDelegate method
extension VehicleViewController:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view != nil && (touch.view!.isDescendant(of: self.collectionView) || touch.view!.isDescendant(of: self.mainView)) {
            return false
        }
        return true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,otherGestureRecognizer : UIGestureRecognizer)->Bool {
        return true
    }
}

extension VehicleViewController:NoInternetConnectionDelegate {
    //MARK: NoInternetConnectionView
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: NIB_NAME.noInternetConnectionView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:20, width: self.view.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.view.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
}
