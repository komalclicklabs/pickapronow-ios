//
//  SignupCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 30/05/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class SignupCell: UITableViewCell {

    @IBOutlet var countryField: UITextField!
    @IBOutlet var signupField: UITextField!
    @IBOutlet var leadingConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.signupField.textColor = COLOR.TEXT_COLOR
        self.signupField.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        
        self.countryField.textColor = COLOR.TEXT_COLOR
        self.countryField.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setNameField() {
        self.signupField.placeholder = "\(TEXT.YOUR) \(TEXT.NAME)"
        self.signupField.keyboardType = UIKeyboardType.default
        self.signupField.autocapitalizationType = .sentences
        self.signupField.isSecureTextEntry = false
        self.leadingConstraint.constant = 0.0
        self.countryField.isHidden = true
    }
    
    func setEmailField() {
        self.signupField.placeholder = "\(TEXT.YOUR) \(TEXT.EMAIL)"
        self.signupField.keyboardType = UIKeyboardType.emailAddress
        self.signupField.autocapitalizationType = .none
        self.signupField.isSecureTextEntry = false
        self.leadingConstraint.constant = 0.0
        self.countryField.isHidden = true
    }
    
    func setPhoneField() {
        self.signupField.placeholder = "\(TEXT.YOUR) \(TEXT.PHONE_NUMBER)"
        self.signupField.keyboardType = UIKeyboardType.phonePad
        self.signupField.autocapitalizationType = .none
        self.signupField.isSecureTextEntry = false
        self.leadingConstraint.constant = 72.0
        self.countryField.isHidden = false
        self.countryField.placeholder = "+1"
        
        self.countryField.rightViewMode = UITextFieldViewMode.always
        let imageView = UIImageView(image: #imageLiteral(resourceName: "smallDownArrow"))
        self.countryField.rightView = imageView
    }
    
    func setPasswordField() {
        self.signupField.placeholder = "\(TEXT.SET) \(TEXT.PASSWORD)"
        self.signupField.keyboardType = UIKeyboardType.default
        self.signupField.autocapitalizationType = .none
        self.signupField.isSecureTextEntry = true
        self.leadingConstraint.constant = 0.0
        self.countryField.isHidden = true
    }
    
    func setConfirmPasswordField() {
        self.signupField.placeholder = "\(TEXT.CONFIRM) \(TEXT.PASSWORD)"
        self.signupField.keyboardType = UIKeyboardType.default
        self.signupField.autocapitalizationType = .none
        self.signupField.isSecureTextEntry = true
        self.leadingConstraint.constant = 0.0
        self.countryField.isHidden = true
    }
}
