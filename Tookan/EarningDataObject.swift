//
//  EarningDataObject.swift
//  Tookan
//
//  Created by cl-macmini-45 on 19/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit



class EarningDataObject: NSObject {
    
}

struct WEEK_REPORT {
    var date = ""
    var day = ""
    var day_id = 0
    var total = ""
    
    init(json:[String:Any]) {
        if let value = json["date"] as? NSNumber {
            self.date = "\(value)"
        } else if let value = json["date"] as? String {
            self.date = value
        }
        
        if let value = json["day"] as? String {
            self.day = value
        }
        
        if let value = json["day_id"] as? NSNumber {
            self.day_id = Int(value)
        } else if let value = json["day_id"] as? String {
            self.day_id = Int(value)!
        }
        
        if let value = json["total"] as? NSNumber {
            self.total = "\(value)"
        } else if let value = json["total"] as? String {
            self.total = value
        }
    }
}


struct WEEK_JOBS {
    var job_id:Int64 = 0
    var job_type = ""
    var job_status = 2
    var time = ""
    var date = ""
    var total = ""
    var duration = ""
    var distance = ""
    
    init(json:[String:Any]) {
        if let value = json["job_id"] as? NSNumber {
            self.job_id = Int64(value)
        } else if let value = json["job_id"] as? String {
            self.job_id = Int64(value)!
        }
        
        if let value = json["job_type"] as? String {
            self.job_type = value
        } else {
            self.job_type = ""
        }
        
        if let value = json["job_status"] as? NSNumber {
            self.job_status = Int(value)
        } else if let value = json["job_status"] as? String {
            self.job_status = Int(value)!
        }
        
        if let value = json["time"] as? String {
            self.time = value
        } else {
            self.time = ""
        }
        
        if let value = json["date"] as? String {
            self.date = value
        } else {
            self.date = ""
        }
        
        if let value = json["total"] as? NSNumber {
            self.total = "\(value)"
        } else if let value = json["total"] as? String {
            self.total = value
        }
        
        if let value = json["duration"] as? String {
            self.duration = value
        } else {
            self.duration = ""
        }
        
        if let value = json["distance"] as? String {
            self.distance = value
        } else {
            self.distance = ""
        }
    }
}

struct FORMULA_FIELDS {
    var display_name = ""
    var key = ""
    var type = 0
    var expression = ""
    var replaced_exp = ""
    var sum = ""
    
    init(json:[String:Any]) {
        if let value = json["display_name"] as? String {
            self.display_name = value
        }
        
        if let value = json["key"] as? String {
            self.key = value
        }
        
        if let value = json["type"] as? NSNumber {
            self.type = Int(value)
        } else if let value = json["type"] as? String {
            self.type = Int(value)!
        }
        
        if let value = json["expression"] as? String {
            self.expression = value
        }
        
        if let value = json["replaced_exp"] as? String {
            self.replaced_exp = value
        }
        
        if let value = json["sum"] as? NSNumber {
            self.sum = "\(value)"
        } else if let value = json["sum"] as? String {
            self.sum = value
        }
    }
}


