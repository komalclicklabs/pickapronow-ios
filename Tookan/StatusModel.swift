//
//  StatusModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 04/03/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Crashlytics
protocol StatusModelDelegate {
    func animationForBottomView()
    func setBottomButtonView()
    func dismissPartial()
    func dismissComplete()
    func dismissConstroller()
    func updateJobIdTitle()
    func showInvalidAccessTokenPopup(_ message:String)
    func alertForSyncingFailed(_ jobStatus:Int, reason:String)
    func showInternetToast()
    func showGotoPickupTask(_ message:String, jobId:Int,pickupTaskDetails:TasksAssignedToDate)
    func gotoInvoiceController()
    func discardAlertPopup(_ oldJobStatus : Int, reason : String)
    func gotoEarningController(earningFormulaFields:[FORMULA_FIELDS],pricingFormulaFields:[FORMULA_FIELDS], totalDistane:String, totalDuration:String, hasTaskCompleted:Bool, isRatingAvailable:Int)
}

class StatusModel: NSObject {

    var viewShowStatus:Int!
    var acceptTitle:String!
    var rejectTitle:String!
    var stateTitle:String!
    var loadingTitle:String!
    var positiveStatusColor:UIColor!
    var dictionary = [String: String]()
    var taskUpdatingStatus = ""
    var hitProgessCount = 0
    var locationUpdateCount = 0
    var syncStatus = 1
    var hideLeftButton = false
    var delegate:StatusModelDelegate!
    let offlineModel = OfflineDataModel()
    let unsyncedObjectContext = DatabaseManager.sharedInstance.managedObjectContext

    //MARK: Setting Job Status and title
    func setJobStatusAndButtonTitle() {
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
        case JOB_STATUS.assigned:
            switch (Singleton.sharedInstance.selectedTaskDetails.acceptOptionalField.value)! {
            case ACCEPT_TYPE.acknowledge:
                viewShowStatus = SHOW_HIDE.showPartial
                acceptTitle = BUTTON_TITLE.acknowledge
                rejectTitle = ""
                self.stateTitle = TEXT.ACKNOWLEDGED
                self.loadingTitle = TEXT.ACKNOWLEDGING
                self.positiveStatusColor = COLOR.ACCEPTED_COLOR
                self.hideLeftButton = false
                break
                
            case ACCEPT_TYPE.acceptDecline:
                viewShowStatus = SHOW_HIDE.showComplete
                acceptTitle = BUTTON_TITLE.accept
                rejectTitle = BUTTON_TITLE.decline
                self.stateTitle = TEXT.ACCEPTED
                self.loadingTitle = TEXT.ACCEPTING
                self.positiveStatusColor = COLOR.ACCEPTED_COLOR
                self.hideLeftButton = false
                break
                
            case ACCEPT_TYPE.startCancel:
                if(Singleton.sharedInstance.selectedTaskDetails.hideCancelOptionalField.value == 1) {
                    viewShowStatus = SHOW_HIDE.showPartial
                    self.hideLeftButton = true
                } else {
                    viewShowStatus = SHOW_HIDE.showComplete
                    self.hideLeftButton = false
                }
                acceptTitle = BUTTON_TITLE.start
                rejectTitle = BUTTON_TITLE.cancel
                self.stateTitle = TEXT.STARTED
                self.loadingTitle = TEXT.STARTING
                self.positiveStatusColor = COLOR.STARTED_COLOR
                break
                
            default:
                break
            }
            break
            
        case JOB_STATUS.accepted:
            if(Singleton.sharedInstance.selectedTaskDetails.hideCancelOptionalField.value == 1) {
                viewShowStatus = SHOW_HIDE.showPartial
                self.hideLeftButton = true
            } else {
                viewShowStatus = SHOW_HIDE.showComplete
                self.hideLeftButton = false
            }
            acceptTitle = BUTTON_TITLE.start
            rejectTitle = BUTTON_TITLE.cancel
            self.stateTitle = TEXT.STARTED
            self.loadingTitle = TEXT.STARTING
            self.positiveStatusColor = COLOR.STARTED_COLOR
            break
            
        case JOB_STATUS.started:
            if(Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value == 1) {
                if(Singleton.sharedInstance.selectedTaskDetails.hideCancelOptionalField.value == 1) {
                    viewShowStatus = SHOW_HIDE.showPartial
                    self.hideLeftButton = true
                } else {
                    viewShowStatus = SHOW_HIDE.showComplete
                    self.hideLeftButton = false
                }
                acceptTitle = BUTTON_TITLE.arrived
                rejectTitle = BUTTON_TITLE.cancel
                self.stateTitle = TEXT.ARRIVED
                self.loadingTitle = TEXT.ARRIVING
                 self.positiveStatusColor = COLOR.ARRIVED_COLOR
            } else {
                acceptTitle = BUTTON_TITLE.successfull
                rejectTitle = BUTTON_TITLE.failed
                self.stateTitle = TEXT.SUCCESSFUL
                self.loadingTitle = TEXT.COMPLETING
                 self.positiveStatusColor = COLOR.SUCCESSFUL_COLOR
                if(Singleton.sharedInstance.selectedTaskDetails.hideFailedOptionalField.value == 1) {
                    viewShowStatus = SHOW_HIDE.showPartial
                    self.hideLeftButton = true
                } else {
                    viewShowStatus = SHOW_HIDE.showComplete
                    self.hideLeftButton = false
                }
            }
            break
            
        case JOB_STATUS.successful:
            if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)
            {
                viewShowStatus = SHOW_HIDE.hidePartial
                acceptTitle = BUTTON_TITLE.gotoDelivery
                rejectTitle = ""
            } else {
                viewShowStatus = SHOW_HIDE.hideComplete
                acceptTitle = ""
                rejectTitle = ""
            }
            //Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            break
            
        case JOB_STATUS.failed:
            viewShowStatus = SHOW_HIDE.hideComplete
            acceptTitle = ""
            rejectTitle = ""
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.failed
            //Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            break
            
        case JOB_STATUS.arrived:
            if(Singleton.sharedInstance.selectedTaskDetails.hideFailedOptionalField.value == 1) {
                viewShowStatus = SHOW_HIDE.showPartial
                self.hideLeftButton = true
            } else {
                viewShowStatus = SHOW_HIDE.showComplete
                self.hideLeftButton = false
            }
            acceptTitle = BUTTON_TITLE.successfull
            rejectTitle = BUTTON_TITLE.failed
            self.stateTitle = TEXT.SUCCESSFUL
            self.loadingTitle = TEXT.COMPLETING
             self.positiveStatusColor = COLOR.SUCCESSFUL_COLOR
            break
            
        case JOB_STATUS.declined:
            viewShowStatus = SHOW_HIDE.hideComplete
            acceptTitle = ""
            rejectTitle = ""
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.declined
            break
            
        case JOB_STATUS.canceled:
            viewShowStatus = SHOW_HIDE.hideComplete
            acceptTitle = ""
            rejectTitle = ""
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.canceled
            break
            
        default:
            break
        }
        
        delegate.setBottomButtonView()
    }
    
    
    func getMandatoryMessageForNotes(selectedTaskDetails:TasksAssignedToDate) -> String {
        if selectedTaskDetails.noteOptionalField.value == 1 {
            if selectedTaskDetails.noteOptionalField.required == true {
                if selectedTaskDetails.addedNotesArray!.count == 0 {
                    return String(format: "* %@ \n", TEXT.ADD_NOTES)
                }
            }
        }
        return ""
    }
    
    func getMandatoryMessageForImages(selectedTaskDetails:TasksAssignedToDate) -> String {
        if selectedTaskDetails.imageOptionalField.value == 1 {
            if selectedTaskDetails.imageOptionalField.required == true {
                if selectedTaskDetails.addedImagesArray!.count == 0 {
                    return String(format: "* %@ \n", TEXT.ADD_IMAGE)//"* Add Images\n"
                }
            }
        }
        return ""
    }
    
    func getMandatoryMessageForSignature(selectedTaskDetails:TasksAssignedToDate) -> String {
        if selectedTaskDetails.signatureOptionalField.value == 1 {
            if selectedTaskDetails.signatureOptionalField.required == true {
                if selectedTaskDetails.addedSignatureArray!.count == 0 {
                    return String(format: "* %@ \n", TEXT.ADD_SIGNATURE)//"* Add Signature\n"
                }
            }
        }
        return ""
    }
    
    func getMandatoryMessageForScanner(selectedTaskDetails:TasksAssignedToDate) -> String {
        if selectedTaskDetails.scannerOptionalField.value == 1 {
            if selectedTaskDetails.scannerOptionalField.required == true {
                if selectedTaskDetails.addedScannerArray!.count == 0 {
                    return String(format: "* %@ \n", TEXT.ADD_BARCODE)//"* Add Signature\n"
                }
            }
        }
        return ""
    }
    
    func getCustomFieldMandatoryMessage(selectedTaskDetails:TasksAssignedToDate) -> String {
        var alertFieldString = ""
        var count = 0
        for i in (0..<selectedTaskDetails.customFieldArray!.count) {
            switch(selectedTaskDetails.customFieldArray![i].dataType) {
            case CUSTOM_FIELD_DATA_TYPE.dropDown:
                if selectedTaskDetails.customFieldArray![i].fleetData == "" {
                    if selectedTaskDetails.customFieldArray![i].required == true {
                        if selectedTaskDetails.customFieldArray![i].appSide == "1" {
                            if(count == 0) {
                                alertFieldString = "\(alertFieldString)\n\(TEXT.CUSTOM_FIELDS)\n"//  + String(format: "\n* %@\n", TEXT.CUSTOM_FIELDS)//"\n* Custom Fields\n"
                            }
                            count += 1
                            alertFieldString = "\(alertFieldString)- \(selectedTaskDetails.customFieldArray![i].displayLabelName)\n"
                        }
                    }
                }
                break
                
            case CUSTOM_FIELD_DATA_TYPE.image:
                if selectedTaskDetails.customFieldArray![i].data == "" {
                    if selectedTaskDetails.customFieldArray![i].fleetData == "" {
                        if selectedTaskDetails.customFieldArray![i].required == true {
                            if selectedTaskDetails.customFieldArray![i].appSide == "1" {
                                if(count == 0) {
                                    alertFieldString = "\(alertFieldString)\n\(TEXT.CUSTOM_FIELDS)\n"
                                }
                                count += 1
                                alertFieldString = "\(alertFieldString)- \(selectedTaskDetails.customFieldArray![i].displayLabelName)\n"
                            }
                        }
                    }
                } else if(selectedTaskDetails.customFieldArray![i].fleetData == "Uploading"){
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_UPLOADING, isError: .error)
                    return "Uploading"
                }
                break
                
            case CUSTOM_FIELD_DATA_TYPE.checkbox:
                if(selectedTaskDetails.customFieldArray![i].data == "false" || selectedTaskDetails.customFieldArray![i].data == "0"){
                    if (selectedTaskDetails.customFieldArray![i].fleetData == "false" || selectedTaskDetails.customFieldArray![i].fleetData == "") {
                        if selectedTaskDetails.customFieldArray![i].required == true {
                            if selectedTaskDetails.customFieldArray![i].appSide == "1" {
                                if(count == 0) {
                                    alertFieldString = "\(alertFieldString)\n\(TEXT.CUSTOM_FIELDS)\n"
                                }
                                count += 1
                                alertFieldString = "\(alertFieldString)- \(selectedTaskDetails.customFieldArray![i].displayLabelName)\n"
                            }
                        }
                    }
                }
                break
                
            case CUSTOM_FIELD_DATA_TYPE.checklist:
                let checkValue = self.getChecklistCheckValue(section: i)
                if selectedTaskDetails.customFieldArray![i].required == true {
                    if selectedTaskDetails.customFieldArray![i].appSide == "1" {
                        if(selectedTaskDetails.customFieldArray![i].fleetData == "" || checkValue == false) {
                            if(count == 0) {
                                alertFieldString = "\(alertFieldString)\n\(TEXT.CUSTOM_FIELDS)\n"
                            }
                            count += 1
                            alertFieldString = "\(alertFieldString)- \(selectedTaskDetails.customFieldArray![i].displayLabelName)\n"
                        }
                    }
                }
                break
                
            case CUSTOM_FIELD_DATA_TYPE.table:
                if selectedTaskDetails.customFieldArray![i].required == true {
                    if (selectedTaskDetails.customFieldArray![i].appSide == APP_SIDE.write || selectedTaskDetails.customFieldArray![i].appSide == APP_SIDE.tableEdit) {
                        if(selectedTaskDetails.customFieldArray![i].taskTableData.status.contains("0") == true) {
                            if(count == 0) {
                                alertFieldString = "\(alertFieldString)\n\(TEXT.CUSTOM_FIELDS)\n"
                            }
                            count += 1
                            alertFieldString = "\(alertFieldString)- \(selectedTaskDetails.customFieldArray![i].displayLabelName)\n"
                        }
                    }
                }
                break
                
            case CUSTOM_FIELD_DATA_TYPE.date,
                 CUSTOM_FIELD_DATA_TYPE.dateFuture,
                 CUSTOM_FIELD_DATA_TYPE.datePast,
                 CUSTOM_FIELD_DATA_TYPE.dateTime,
                 CUSTOM_FIELD_DATA_TYPE.dateTimeFuture,
                 CUSTOM_FIELD_DATA_TYPE.dateTimePast,
                 CUSTOM_FIELD_DATA_TYPE.email,
                 CUSTOM_FIELD_DATA_TYPE.number,
                 CUSTOM_FIELD_DATA_TYPE.telephone,
                 CUSTOM_FIELD_DATA_TYPE.text,
                 CUSTOM_FIELD_DATA_TYPE.url,
                 CUSTOM_FIELD_DATA_TYPE.barcode:
                
                if selectedTaskDetails.customFieldArray![i].data == "" {
                    if selectedTaskDetails.customFieldArray![i].fleetData == "" {
                        if selectedTaskDetails.customFieldArray![i].required == true {
                            if selectedTaskDetails.customFieldArray![i].appSide == "1" {
                                if(count == 0) {
                                    alertFieldString = "\(alertFieldString)\n\(TEXT.CUSTOM_FIELDS)\n"
                                }
                                count += 1
                                alertFieldString = "\(alertFieldString)- \(selectedTaskDetails.customFieldArray![i].displayLabelName)\n"
                            }
                        }
                    }
                }
                break
                
            default:
                break
            }
        }
        return alertFieldString
    }
    
    /*------- Mandatory field check ---------*/
    func showMandatoryFieldsAlert(_ selectedTaskDetails:TasksAssignedToDate) -> Bool{
        self.updateAddedBarcodeAndNotesArrayAfterSuccessful()
        if(selectedTaskDetails.imageUploadingStatus == true) {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_UPLOADING, isError: .error)
            return false
        }
        
        var alertFieldString:String = self.getMandatoryMessageForNotes(selectedTaskDetails: selectedTaskDetails)
        alertFieldString =  "\(alertFieldString)\(self.getMandatoryMessageForImages(selectedTaskDetails: selectedTaskDetails))"
        alertFieldString =  "\(alertFieldString)\(self.getMandatoryMessageForSignature(selectedTaskDetails: selectedTaskDetails))"
        alertFieldString =  "\(alertFieldString)\(self.getMandatoryMessageForScanner(selectedTaskDetails: selectedTaskDetails))"
        
        let customAlertString = self.getCustomFieldMandatoryMessage(selectedTaskDetails: selectedTaskDetails)
        if customAlertString == "Uploading" {
            return false
        } else {
            alertFieldString = "\(alertFieldString)\(customAlertString)"
        }
        
        if alertFieldString.isEmpty {
            return true
        }else {
            UIAlertView(title: TEXT.MANDATORY_FIELD, message: alertFieldString, delegate: self, cancelButtonTitle: TEXT.OK).show()
            return false
        }
    }

    
    //MARK: Get checkList Check value
    func getChecklistCheckValue(section:Int) -> Bool {
        for checklistValues in Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section].checklistArray {
            if checklistValues.check == true {
                return true
            }
        }
        return false
    }
    //MARK: POSITIVE STATUS ACTION
    func positiveStatusAction(){
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
        case JOB_STATUS.accepted:
            self.changeJobStatus(jobStatus: JOB_STATUS.started, reason: "")
            self.dictionary["description"] = "\(TEXT.STARTED) \(TEXT.AT)"// + TEXT.AT
            self.taskUpdatingStatus = "Started"
            
        case JOB_STATUS.started:
            if(Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value == 1) {
                self.changeJobStatus(jobStatus: JOB_STATUS.arrived, reason: "")
                self.dictionary["description"] = "\(TEXT.ARRIVED) \(TEXT.AT)"//TEXT.ARRIVED + " " + TEXT.AT
                self.taskUpdatingStatus = "Arrived"
            } else {
                if self.showMandatoryFieldsAlert(Singleton.sharedInstance.selectedTaskDetails) == true {
                    // LocationTracker.sharedInstance().enterInForegroundFromBackground()
                    self.changeJobStatus(jobStatus: JOB_STATUS.successful, reason: "")
                    guard IJReachability.isConnectedToNetwork() == true else {
                        self.dictionary["creation_datetime"] = Date().formattedWith("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                        return
                    }
                    self.dictionary["description"] = "\(TEXT.SUCCESSFUL) \(TEXT.AT)"//TEXT.SUCCESSFUL + " " + TEXT.AT
                    self.taskUpdatingStatus = "Successful"
                }else{
                    viewShowStatus = SHOW_HIDE.showComplete
                    delegate.animationForBottomView()
                }
            }
            
        case JOB_STATUS.arrived:
            self.changeJobStatus(jobStatus: JOB_STATUS.successful, reason: "")
            guard IJReachability.isConnectedToNetwork() == true else {
                self.dictionary["creation_datetime"] = Date().formattedWith("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                return
            }
            
            self.dictionary["description"] = "\(TEXT.SUCCESSFUL) \(TEXT.AT)"//TEXT.SUCCESSFUL + " " + TEXT.AT
            self.taskUpdatingStatus = "Successful"
            
        case JOB_STATUS.successful:
            if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
                if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)
                {
                    self.viewShowStatus = SHOW_HIDE.hidePartial
                    self.acceptTitle = BUTTON_TITLE.gotoDelivery
                    self.rejectTitle = ""
                } else {
                    self.viewShowStatus = SHOW_HIDE.hideComplete
                    delegate.dismissConstroller()
                    return
                }
                
            }
            
        case JOB_STATUS.assigned:
            switch (Singleton.sharedInstance.selectedTaskDetails.acceptOptionalField.value)! {
            case ACCEPT_TYPE.acknowledge:
                self.changeJobStatus(jobStatus: JOB_STATUS.accepted, reason: "")
                self.dictionary["description"] = "\(TEXT.ACKNOWLEDGED) \(TEXT.AT)"//TEXT.ACKNOWLEDGED + " " + TEXT.AT
                self.taskUpdatingStatus = "Acknowledged"
                break
                
            case ACCEPT_TYPE.acceptDecline:
                self.changeJobStatus(jobStatus: JOB_STATUS.accepted, reason: "")
                self.dictionary["description"] = "\(TEXT.ACCEPTED) \(TEXT.AT)"//TEXT.ACCEPTED + " " + TEXT.AT
                self.taskUpdatingStatus = "Accepted"
                break
                
            case ACCEPT_TYPE.startCancel:
                self.changeJobStatus(jobStatus: JOB_STATUS.started, reason: "")
                self.dictionary["description"] = "\(TEXT.STARTED) \(TEXT.AT)"//TEXT.STARTED + " " + TEXT.AT
                self.taskUpdatingStatus = "Started"
                break
                
            default:
                break
            }
            break
        default:
            break
        }
        
        if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
            if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)
            {
                self.gotoDeliveryTaskAction()
            }
        }
        
        self.dictionary["creation_datetime"] = Date().formattedWith("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    }
    
    
    //MARK: CHANGE JOB STATUS
    func changeJobStatus(jobStatus:Int, reason:String) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        Analytics.logEvent(ANALYTICS_KEY.CHANGE_JOB_STATUS, parameters: ["Status":jobStatus as NSObject])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.CHANGE_JOB_STATUS, customAttributes: ["Status":jobStatus])
        self.hitProgessCount = 0
        self.locationUpdateCount = 0
        self.checkLocationHitInProgress(jobStatus, reason: reason)
    }
    
    func checkLocationHitInProgress(_ jobStatus:Int, reason:String) {
        if IJReachability.isConnectedToNetwork() == true {
            self.hitProgessCount = self.hitProgessCount + 1
            if(UserDefaults.standard.bool(forKey: USER_DEFAULT.isHitInProgress) == false) {
                self.locationUpdateCount = self.locationUpdateCount + 1
                var locationArray = [Any]()
                if let array = UserDefaults.standard.object(forKey: USER_DEFAULT.locationArray) as? [Any]{
                    locationArray = array
                }
                
                if(self.locationUpdateCount > 5) {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SERVER_NOT_RESPONDING, isError: .error)
                    self.setJobStatusAndButtonTitle()
                } else {
                    if(locationArray.count > 0) {
                        UserDefaults.standard.set(true, forKey: USER_DEFAULT.isHitInProgress)
                        UIApplication.shared.isNetworkActivityIndicatorVisible = true
                        NetworkingHelper.sharedInstance.updatingFleetLocation(locationArray.jsonString, status: "ChangeJobStatus", locationUpdateMode: UserDefaults.standard.value(forKey: USER_DEFAULT.applicationMode) as! String, getLocationLatitude: 0.0, getLocationLongitude: 0.0, getLocationAccuracy: 0.0, receivedResponse: { (succeeded, response) -> () in
                            if(succeeded == true) {
                                DispatchQueue.main.async(execute: { () -> Void in
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                    if let _ = response["data"] as? NSDictionary {
                                        switch(response["status"] as! Int) {
                                        case STATUS_CODES.SHOW_DATA:
                                            var sentLocationArray = [Any]()
                                            if let sentLocationString = response["location"] as? String {
                                                sentLocationArray = sentLocationString.jsonObjectArray
                                            }
                                            for i in (0..<sentLocationArray.count) {
                                                let sendDictionaryObject = sentLocationArray[i] as! [String:Any]
                                                if let sendTimeStamp = sendDictionaryObject["tm_stmp"] as? String {
                                                    for j in (0..<locationArray.count) {
                                                        let locationDictionaryObject = locationArray[j] as! [String:Any]
                                                        if let locationTimeStamp = locationDictionaryObject["tm_stmp"] as? String {
                                                            if(sendTimeStamp == locationTimeStamp) {
                                                                locationArray.remove(at: j)
                                                                break
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                            UserDefaults.standard.set(locationArray, forKey: USER_DEFAULT.locationArray)
                                            self.checkUnsyncedTask(jobStatus, reason: reason)
                                            break
                                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                            ActivityIndicator.sharedInstance.hideActivityIndicator()
                                            UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                            if let message = response["message"] as? String {
                                                self.delegate.showInvalidAccessTokenPopup(message)
                                            } else {
                                                self.delegate.showInvalidAccessTokenPopup(ERROR_MESSAGE.INVALID_ACCESS_TOKEN)
                                            }
                                            break
                                        default:
                                            ActivityIndicator.sharedInstance.hideActivityIndicator()
                                            self.setJobStatusAndButtonTitle()
                                            break
                                        }
                                    }
                                })
                            } else {
                                sleep(1)
                                self.checkLocationHitInProgress(jobStatus, reason: reason)
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                            }
                        })
                    } else {
                        self.checkUnsyncedTask(jobStatus, reason: reason)
                    }
                }
            } else {
                if(self.hitProgessCount > 15) {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SERVER_NOT_RESPONDING, isError: .error)
                    self.setJobStatusAndButtonTitle()
                } else {
                    sleep(1)
                    self.checkLocationHitInProgress(jobStatus, reason: reason)
                }
            }
        } else {
            UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            self.checkUnsyncedTask(jobStatus, reason: reason)
        }
    }
    
    func checkUnsyncedTask(_ jobStatus:Int, reason:String) {
        ActivityIndicator.sharedInstance.hideActivityIndicator()
        if IJReachability.isConnectedToNetwork() == true {
            if(offlineModel.uploadUnsyncedTaskToServer() == false){
                if(offlineModel.hitFailStatus == STATUS_CODES.INVALID_ACCESS_TOKEN) {
                    self.delegate.showInvalidAccessTokenPopup(offlineModel.hitFailMessage)
                } else {
                    self.delegate.alertForSyncingFailed(jobStatus, reason: reason)
                }
            } else {
                self.changeJobStatusServerHit(jobStatus, reason: reason)
            }
        } else {
            self.saveOfflineData(jobStatus, reason: reason)
        }
    }
    
    func saveOfflineData(_ jobStatus:Int, reason:String) {
        self.delegate.showInternetToast()
        if(Auxillary.isAppSyncingEnable() == true) {
            syncStatus = 0
            if(jobStatus == JOB_STATUS.started && Singleton.sharedInstance.selectedTaskDetails.jobType == JOB_TYPE.delivery && Singleton.sharedInstance.selectedTaskDetails.pickupJobId != 0) {
                if let tasksFromDatabase = DatabaseManager.sharedInstance.fetchAssignedTaskFromDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.pickupJobId) {
                    let taskData = NSData(data: tasksFromDatabase.value(forKey: "task_object") as! Data) as Data
                    let taskDetails = NSKeyedUnarchiver.unarchiveObject(with: taskData) as! TasksAssignedToDate
                    if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: taskDetails.jobStatus) == false ) {
                        self.delegate.showGotoPickupTask(ERROR_MESSAGE.GOTO_PICKUP, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId,pickupTaskDetails: taskDetails)
                        self.setJobStatusAndButtonTitle()
                        return
                    }
                }
            }
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = jobStatus
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
            if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                if(DatabaseManager.sharedInstance.saveStatusForUnsyncedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus:jobStatus, latitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", longitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.longitude)", reason: reason, userManagedContext: self.unsyncedObjectContext,status:taskUpdatingStatus) == true) {
                    self.setUpdatedJobStatus(jobStatus, isEarningControllerVisible: false)
                    self.updateOfflinePickupAndDeliveryTaskWhenAccepted(jobStatus: jobStatus, reason: reason)
                }
            } else {
                if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                    if(DatabaseManager.sharedInstance.saveStatusForUnsyncedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus:jobStatus, latitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", longitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", reason: reason, userManagedContext: self.unsyncedObjectContext,status:taskUpdatingStatus) == true) {
                        self.setUpdatedJobStatus(jobStatus, isEarningControllerVisible: false)
                        self.updateOfflinePickupAndDeliveryTaskWhenAccepted(jobStatus: jobStatus, reason: reason)
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                }
            }
        }
        self.setJobStatusAndButtonTitle()
    }

    func updateOfflinePickupAndDeliveryTaskWhenAccepted(jobStatus:Int, reason:String) {
        if(jobStatus == JOB_STATUS.accepted && Singleton.sharedInstance.selectedTaskDetails.jobType == JOB_TYPE.delivery && Singleton.sharedInstance.selectedTaskDetails.pickupJobId > 0 && Singleton.sharedInstance.selectedPickupJobDetails != nil) {
            if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedPickupJobDetails.jobId, taskDetails: Singleton.sharedInstance.selectedPickupJobDetails, syncStatus: 0) == true) {
                if(DatabaseManager.sharedInstance.saveStatusForUnsyncedTaskToDatabase(Singleton.sharedInstance.selectedPickupJobDetails.jobId, jobStatus:jobStatus, latitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", longitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.longitude)", reason: reason, userManagedContext: self.unsyncedObjectContext,status:taskUpdatingStatus) == true) {
                }
            } else {
                if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedPickupJobDetails, jobId: Singleton.sharedInstance.selectedPickupJobDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                    if (DatabaseManager.sharedInstance.saveStatusForUnsyncedTaskToDatabase(Singleton.sharedInstance.selectedPickupJobDetails.jobId, jobStatus:jobStatus, latitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", longitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", reason: reason, userManagedContext: self.unsyncedObjectContext,status:taskUpdatingStatus) == true) {
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                }
            }
        } else if(jobStatus == JOB_STATUS.accepted && Singleton.sharedInstance.selectedTaskDetails.jobType == JOB_TYPE.pickup && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0 && Singleton.sharedInstance.selectedDeliveryJobDetails != nil) {
            if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedDeliveryJobDetails.jobId, taskDetails: Singleton.sharedInstance.selectedDeliveryJobDetails, syncStatus: 0) == true) {
                if(DatabaseManager.sharedInstance.saveStatusForUnsyncedTaskToDatabase(Singleton.sharedInstance.selectedDeliveryJobDetails.jobId, jobStatus:jobStatus, latitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", longitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.longitude)", reason: reason, userManagedContext: self.unsyncedObjectContext,status:taskUpdatingStatus) == true) {
                }
            } else {
                if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedDeliveryJobDetails, jobId: Singleton.sharedInstance.selectedDeliveryJobDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                    if (DatabaseManager.sharedInstance.saveStatusForUnsyncedTaskToDatabase(Singleton.sharedInstance.selectedDeliveryJobDetails.jobId, jobStatus:jobStatus, latitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", longitude: "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)", reason: reason, userManagedContext: self.unsyncedObjectContext,status:taskUpdatingStatus) == true) {
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                }
            }
        }
    }
    
    func updateAddedBarcodeAndNotesArrayAfterSuccessful() {
        if Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.last?.IDs == 0 && Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.last?.taskDescription.length == 0  {
            Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.removeLast()
        }
        
        if Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.last?.IDs == 0 && Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.last?.taskDescription.length == 0 {
            Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.removeLast()
        }
    }

    func setUpdatedJobStatus(_ jobStatus:Int, isEarningControllerVisible:Bool) {
        Singleton.sharedInstance.selectedTaskDetails.taskHistoryArray?.append(TaskHistory(json: self.dictionary as NSDictionary))
        switch jobStatus {
        case JOB_STATUS.assigned:
            if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0 && Singleton.sharedInstance.selectedDeliveryJobDetails != nil) {
                Singleton.sharedInstance.selectedDeliveryJobDetails.jobStatus = JOB_STATUS.assigned
            }
            
            if(Singleton.sharedInstance.selectedTaskDetails.jobType == JOB_TYPE.delivery && Singleton.sharedInstance.selectedTaskDetails.pickupJobId > 0 && Singleton.sharedInstance.selectedPickupJobDetails != nil) {
                Singleton.sharedInstance.selectedPickupJobDetails.jobStatus = JOB_STATUS.assigned
            }
            
            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
            break
            
        case JOB_STATUS.accepted:
            switch (Singleton.sharedInstance.selectedTaskDetails.acceptOptionalField.value)! {
            case ACCEPT_TYPE.acknowledge, ACCEPT_TYPE.acceptDecline:
                Singleton.sharedInstance.removeAcceptedTaskFromNewTask()
                Singleton.sharedInstance.changeStatusForAllRelatedTask(jobStatus: JOB_STATUS.accepted)
                if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0 && Singleton.sharedInstance.selectedDeliveryJobDetails != nil) {
                    Singleton.sharedInstance.selectedDeliveryJobDetails.jobStatus = JOB_STATUS.accepted
                }
                
                if(Singleton.sharedInstance.selectedTaskDetails.jobType == JOB_TYPE.delivery && Singleton.sharedInstance.selectedTaskDetails.pickupJobId > 0 && Singleton.sharedInstance.selectedPickupJobDetails != nil) {
                    Singleton.sharedInstance.selectedPickupJobDetails.jobStatus = JOB_STATUS.accepted
                }
                
                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.accepted
                _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
                break
                
            case ACCEPT_TYPE.startCancel:
                if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0 && Singleton.sharedInstance.selectedDeliveryJobDetails != nil) {
                    Singleton.sharedInstance.selectedDeliveryJobDetails.jobStatus = JOB_STATUS.started
                }
                
                if(Singleton.sharedInstance.selectedTaskDetails.jobType == JOB_TYPE.delivery && Singleton.sharedInstance.selectedTaskDetails.pickupJobId > 0 && Singleton.sharedInstance.selectedPickupJobDetails != nil) {
                    Singleton.sharedInstance.selectedPickupJobDetails.jobStatus = JOB_STATUS.started
                }
                
                UserDefaults.standard.setValue([Any](), forKey: "SavedLocations")
                UserDefaults.standard.setValue([Any](), forKey: USER_DEFAULT.updatingLocationPathArray)
                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.started
                _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
                break
                
            default:
                break
            }
            break
            
        case JOB_STATUS.started:
            UserDefaults.standard.setValue([Any](), forKey: "SavedLocations")
            UserDefaults.standard.setValue([Any](), forKey: USER_DEFAULT.updatingLocationPathArray)
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.started
            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
            break
            
        case JOB_STATUS.successful:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.successful
            //Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
            break
            
        case JOB_STATUS.failed:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.failed
            // Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
            delegate.dismissConstroller()
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
            return
            
        case JOB_STATUS.arrived:
            if(Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value == 1) {
                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.arrived
            } else {
                Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.successful
                // Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            }
            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
            break
            
        case JOB_STATUS.declined:
            Singleton.sharedInstance.removeAcceptedTaskFromNewTask()
            Singleton.sharedInstance.changeStatusForAllRelatedTask(jobStatus: JOB_STATUS.declined)
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.declined
            //Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
            delegate.dismissConstroller()
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
            return
            
        case JOB_STATUS.canceled:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.canceled
            // Singleton.sharedInstance.selectedTaskDetails.jobCompleteStatus = JOB_COMPLETE_STATUS.complete
            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: syncStatus)
            delegate.dismissConstroller()
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
            return
            
        default:
            break
        }
        
        if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
            if(Singleton.sharedInstance.selectedTaskDetails.jobType == 0 && Singleton.sharedInstance.selectedTaskDetails.deliveryJobId > 0)
            {
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
                self.viewShowStatus = SHOW_HIDE.hidePartial
                self.acceptTitle = BUTTON_TITLE.gotoDelivery
                self.rejectTitle = ""
                if Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.successful {
                    if Singleton.sharedInstance.fleetDetails.has_invoicing_module == 1 {
                        if Singleton.sharedInstance.selectedTaskDetails.invoiceHtml.length > 0 {
                            if isEarningControllerVisible == false {
                                self.getInvoiceHTML()
                            }
                        }
                    }
                }
            } else {
                self.viewShowStatus = SHOW_HIDE.hideComplete
                guard isEarningControllerVisible == false else {
                    return
                }
                
                guard Singleton.sharedInstance.selectedTaskDetails.jobStatus == JOB_STATUS.successful else {
                    delegate.dismissConstroller()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
                    return
                }
                
                guard Singleton.sharedInstance.fleetDetails.has_invoicing_module == 1 else {
                    delegate.dismissConstroller()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
                    return
                }

                guard Singleton.sharedInstance.selectedTaskDetails.invoiceHtml.length > 0 else {
                    delegate.dismissConstroller()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
                    return
                }
                self.getInvoiceHTML()
                
            }
        }
    }

    func changeJobStatusServerHit(_ jobStatus:Int, reason:String) {
        syncStatus = 1
        self.requestToChangeJobStatus(Singleton.sharedInstance.getAccessToken(), jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, jobStatus: jobStatus, reason: reason) { (succeeded, response) -> () in
            if(succeeded == true) {
                DispatchQueue.main.async(execute: { () -> Void in
                    if let responseData = response["data"] as? NSDictionary {
                        print(response)
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            print(response)
                            var earningFormulaFields = [FORMULA_FIELDS]()
                            var pricingFormulaFields = [FORMULA_FIELDS]()
                            var totalDistance = ""
                            var totalDuration = ""
                            var isEarningVisible = false
                            var ratingValue = 0
                            if jobStatus == JOB_STATUS.successful {
                                if let responseData = response["data"] as? [String:Any] {
                                    if let fields = responseData["formula_fields"] as? [[String:Any]] {
                                        for field in fields {
                                            let formulaField = FORMULA_FIELDS(json: field)
                                            earningFormulaFields.append(formulaField)
                                        }
                                    }
                                    
                                    if let fields = responseData["customer_formula_fields"] as? [[String:Any]] {
                                        for field in fields {
                                            let formulaField = FORMULA_FIELDS(json: field)
                                            pricingFormulaFields.append(formulaField)
                                        }
                                    }
                                    
                                    if let value = responseData["total_distance"] as? NSNumber {
                                        totalDistance = "\(value)"
                                    } else if let value = responseData["total_distance"] as? String {
                                        totalDistance = value
                                    }
                                    
                                    if let value = responseData["total_duration"] as? NSNumber {
                                        totalDuration = "\(value)"
                                    } else if let value = responseData["total_duration"] as? String {
                                        totalDuration = value
                                    }
                                    
                                    if let value = responseData["total_fields"] as? NSNumber {
                                        if Int(value) != 0 {
                                            Singleton.sharedInstance.selectedTaskDetails.driverJobTotal = "\(value)"
                                        }
                                    } else if let value = responseData["total_fields"] as? String {
                                        if value != "0" {
                                            Singleton.sharedInstance.selectedTaskDetails.driverJobTotal = value
                                        }
                                    }
                                    
                                    if let value = responseData["customer_total_fields"] as? NSNumber {
                                        if Int(value) != 0 {
                                            Singleton.sharedInstance.selectedTaskDetails.customerJobTotal = "\(value)"
                                        }
                                    } else if let value = responseData["customer_total_fields"] as? String {
                                        if value != "0" {
                                            Singleton.sharedInstance.selectedTaskDetails.customerJobTotal = value
                                        }
                                    }
                                    
                                    if Singleton.sharedInstance.fleetDetails.rating_comments == 1 {
                                        if let value = responseData["is_rating_comment_enabled"] as? NSNumber {
                                            ratingValue = Int(value)
                                        } else if let value = responseData["is_rating_comment_enabled"] as? String {
                                            if value.length > 0 {
                                                ratingValue = Int(value)!
                                            }
                                        }
                                    }
                                }
                                
                                if earningFormulaFields.count > 0 || pricingFormulaFields.count > 0 || ratingValue == 1{
                                    isEarningVisible = true
                                    self.delegate.gotoEarningController(earningFormulaFields: earningFormulaFields, pricingFormulaFields: pricingFormulaFields, totalDistane: totalDistance, totalDuration: totalDuration, hasTaskCompleted: false, isRatingAvailable: ratingValue)
                                }
                            }
                            
                            self.setUpdatedJobStatus(jobStatus, isEarningControllerVisible: isEarningVisible)
                            break
                            
                        case STATUS_CODES.PICKUP_TASK:
                            DispatchQueue.main.async(execute: { () -> Void in
                                var jobId = 0
                                if let id = responseData["job_id"] {
                                    jobId = id as! Int
                                }
                                var pickupTask:TasksAssignedToDate!
                                for i in (0..<NetworkingHelper.sharedInstance.dayTasksListArray.count) {
                                    let innerTaskArray = NetworkingHelper.sharedInstance.dayTasksListArray[i]
                                    for task in innerTaskArray {
                                        if(task.jobId == jobId) {
                                            Singleton.sharedInstance.selectedDeliveryJobDetails = Singleton.sharedInstance.selectedTaskDetails
                                            pickupTask = task
                                            break
                                        }
                                    }
                                }
                                if(pickupTask != nil) {
                                    self.delegate.showGotoPickupTask(response["message"] as! String,jobId: jobId,pickupTaskDetails: pickupTask)
                                } else {
                                    var params:[String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
                                    params["job_id"] = jobId
//                                    let params = [
//                                        "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//                                        "job_id":jobId
//                                        ] as [String : Any]
                                    NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.viewTaskForId, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, dataResponse) in
                                        DispatchQueue.main.async {
                                            if(isSucceeded == true) {
                                                switch(dataResponse["status"] as! Int) {
                                                case STATUS_CODES.SHOW_DATA:
                                                    if let items = dataResponse["data"] as? NSArray {
                                                        var pickupTask:TasksAssignedToDate!
                                                        for item in items {
                                                            pickupTask = TasksAssignedToDate(json: item as! [String : AnyObject])
                                                        }
                                                        if(pickupTask != nil) {
                                                            Singleton.sharedInstance.selectedDeliveryJobDetails = Singleton.sharedInstance.selectedTaskDetails
                                                            self.delegate.showGotoPickupTask(response["message"] as! String,jobId: jobId,pickupTaskDetails: pickupTask)
                                                        }
                                                    }
                                                    break
                                                case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                                    self.delegate.showInvalidAccessTokenPopup((dataResponse["message"] as? String)!)
                                                    break
                                                default:
                                                    break
                                                }
                                            } else {
                                                Singleton.sharedInstance.showErrorMessage(error: dataResponse["message"] as! String, isError: .error)
                                            }
                                        }
                                    })
                                }
                            })
                            break
                            
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.delegate.showInvalidAccessTokenPopup((response["message"] as? String)!)
                            break
                        default:
                            break
                        }
                    }
                    self.setJobStatusAndButtonTitle()
                })
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SLOW_INTERNET_CONNECTION:
                        self.saveOfflineData(jobStatus, reason: reason)
                        break
                    default:
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        self.setJobStatusAndButtonTitle()
                        break
                    }
                })
            }
        }
    }
    
    func requestToChangeJobStatus(_ accessToken:String, jobId:Int, jobStatus:Int, reason:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var params:[String:Any] = ["access_token": accessToken]
        params["job_id"] = "\(jobId)"
        params["job_status"] = "\(jobStatus)"
        params["reason"] = "\(reason)"
        params["lat"] = lat
        params["lng"] = lng
//        let params:[String:Any] = [
//            "access_token":accessToken,
//            "job_id":"\(jobId)",
//            "job_status":"\(jobStatus)",
//            "reason":"\(reason)",
//            "lat":lat,
//            "lng":lng
//        ]
        //    print(params)
        //Initial reachability check
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(API_NAME.changeJobStatus, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async(execute: { () -> Void in
                    print(response)
                    if(succeeded){
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA, STATUS_CODES.PICKUP_TASK, STATUS_CODES.INVALID_ACCESS_TOKEN:
                            receivedResponse(true, response)
                            break
                        case STATUS_CODES.SHOW_MESSAGE, STATUS_CODES.SHOW_ERROR_MESSAGE:
                            if let message = response["message"] as? String {
                                receivedResponse(false, ["status":STATUS_CODES.SHOW_MESSAGE, "message":message])
                            } else {
                                receivedResponse(false, ["status":STATUS_CODES.SHOW_MESSAGE, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                            }
                            break
                        default:
                            if let message = response["message"] as? String {
                                receivedResponse(false, ["status":response["status"] as! Int, "message":message])
                            } else {
                                receivedResponse(false, ["status":response["status"] as! Int, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                            }
                            break
                        }
                    } else {
                        receivedResponse(false, ["status":STATUS_CODES.SLOW_INTERNET_CONNECTION, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                })
            }
        } else {
            receivedResponse(false, ["status":STATUS_CODES.SLOW_INTERNET_CONNECTION, "message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    func gotoDeliveryTaskAction() {
        Analytics.logEvent(ANALYTICS_KEY.GOTO_DELIVERY, parameters: [:])
        delegate.dismissComplete()
        if Singleton.sharedInstance.selectedTaskDetails.related_job_count! > 2 {
            for i in 0..<Singleton.sharedInstance.selectedTaskRelatedArray.count {
                if Singleton.sharedInstance.selectedTaskRelatedArray[i].jobId == Singleton.sharedInstance.selectedTaskDetails.deliveryJobId {
                    Singleton.sharedInstance.selectedTaskDetails = Singleton.sharedInstance.selectedTaskRelatedArray[i]
                    self.delegate.updateJobIdTitle()
                    self.setJobStatusAndButtonTitle()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
                    break
                }
            }
        } else if Singleton.sharedInstance.selectedTaskDetails.related_job_count! > 1{
            for i in 0..<Singleton.sharedInstance.selectedTaskRelatedArray.count {
                if Singleton.sharedInstance.selectedTaskRelatedArray[i].jobId != Singleton.sharedInstance.selectedTaskDetails.jobId {
                    Singleton.sharedInstance.selectedTaskDetails = Singleton.sharedInstance.selectedTaskRelatedArray[i]
                    self.delegate.updateJobIdTitle()
                    self.setJobStatusAndButtonTitle()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
                    break
                }
            }
        } else {
            let deliveryJobId = Singleton.sharedInstance.selectedTaskDetails.deliveryJobId
            var params:[String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
            params["job_id"] = deliveryJobId ?? 0
//            let params = [
//                "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//                "job_id":deliveryJobId ?? 0
//                ] as [String : Any]
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.viewTaskForId, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    if(isSucceeded == true) {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            if let items = response["data"] as? NSArray {
                                var deliveryTask:TasksAssignedToDate!
                                for item in items {
                                    deliveryTask = TasksAssignedToDate(json: (item as! [String : AnyObject]))
                                }
                                if(deliveryTask != nil) {
                                    Singleton.sharedInstance.selectedTaskDetails = deliveryTask
                                    self.delegate.updateJobIdTitle()
                                    self.setJobStatusAndButtonTitle()
                                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
                                }
                            }
                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.delegate.showInvalidAccessTokenPopup((response["message"] as? String)!)
                            break
                        default:
                            break
                        }
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                }
            })
        }
    }
    
    func gotoPickupTaskAction(pickupJobId:Int) {
        if Singleton.sharedInstance.selectedTaskDetails.related_job_count! > 2 {
            for i in 0..<Singleton.sharedInstance.selectedTaskRelatedArray.count {
                if Singleton.sharedInstance.selectedTaskRelatedArray[i].jobId == pickupJobId {
                    Singleton.sharedInstance.selectedTaskDetails = Singleton.sharedInstance.selectedTaskRelatedArray[i]
                    self.delegate.updateJobIdTitle()
                    self.setJobStatusAndButtonTitle()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
                    break
                }
            }
        } else if Singleton.sharedInstance.selectedTaskDetails.related_job_count! > 1{
            for i in 0..<Singleton.sharedInstance.selectedTaskRelatedArray.count {
                if Singleton.sharedInstance.selectedTaskRelatedArray[i].jobId != Singleton.sharedInstance.selectedTaskDetails.jobId {
                    Singleton.sharedInstance.selectedTaskDetails = Singleton.sharedInstance.selectedTaskRelatedArray[i]
                    self.delegate.updateJobIdTitle()
                    self.setJobStatusAndButtonTitle()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
                    break
                }
            }
        } else {
            let pickupJodId = pickupJobId
            var params:[String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
            params["job_id"] = pickupJodId
//            let params = [
//                "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//                "job_id":pickupJodId ?? 0
//                ] as [String : Any]
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.viewTaskForId, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    if(isSucceeded == true) {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            if let items = response["data"] as? NSArray {
                                var pickupTask:TasksAssignedToDate!
                                for item in items {
                                    pickupTask = TasksAssignedToDate(json: (item as! [String : AnyObject]))
                                }
                                if(pickupTask != nil) {
                                    Singleton.sharedInstance.selectedTaskDetails = pickupTask// pickupTaskDetails
                                    self.delegate.updateJobIdTitle()
                                    self.setJobStatusAndButtonTitle()
                                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.updatePickupDeliveryTask), object: nil)
                                }
                            }
                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.delegate.showInvalidAccessTokenPopup((response["message"] as? String)!)
                            break
                        default:
                            break
                        }
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                }
            })
        }
    }
    
    func getInvoiceHTML() {
        let invoiceModel = InvoiceModel()
        _ = invoiceModel.getInvoiceHTML(completion: {(result) in
            switch result {
            case .success:
                self.delegate.gotoInvoiceController()
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
                break
            case .failed:
                self.delegate.dismissConstroller()
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
                break
            case .resend:
                ActivityIndicator.sharedInstance.showActivityIndicator()
                self.perform(#selector(self.getInvoiceHTML), with: nil, afterDelay: 1.0)
                break
            case .error(let message):
                Singleton.sharedInstance.showErrorMessage(error: message, isError: .error)
            case .ignore:
                break
            }
        })
    }

    //MARK:NegativeStatusAction
    func negativeStatusAction(_ title:String) -> Bool {
        if(Singleton.sharedInstance.selectedTaskDetails.imageUploadingStatus == true) {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_UPLOADING, isError: .error)
            return false
        }
        let oldJobStatus = Singleton.sharedInstance.selectedTaskDetails.jobStatus
        switch title {
        case BUTTON_TITLE.decline:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.declined
            break
        case BUTTON_TITLE.cancel:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.canceled
            break
        case BUTTON_TITLE.failed:
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = JOB_STATUS.failed
            break
        default:
            break
        }
        
        rejectTask(oldJobStatus!, reason :TEXT.ADD_REASON)
        return true
    }

    func rejectTask(_ oldJobStatus : Int, reason : String ){
        self.delegate.dismissPartial()
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus{
        case JOB_STATUS.declined:
            self.dictionary["description"] = "\(TEXT.DECLINED) \(TEXT.AT)"// + TEXT.AT //"Declined at".localized
            self.taskUpdatingStatus = "Declined"
            Singleton.sharedInstance.selectedTaskDetails.jobStatus = oldJobStatus
            self.changeJobStatus(jobStatus: JOB_STATUS.declined, reason: "")
            
        case JOB_STATUS.failed, JOB_STATUS.canceled:
            self.delegate.discardAlertPopup(oldJobStatus, reason: reason)
            break
            
        default:
            break
        }
        self.dictionary["creation_datetime"] = Date().formattedWith("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    }

}
