//
//  WeekEarningDetailCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 22/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class WeekEarningDetailCell: UITableViewCell {

    @IBOutlet var totalPayoutView: UIView!
    @IBOutlet var totalPayoutLabel: UILabel!
    
    @IBOutlet var totalTaskView: UIView!
    @IBOutlet var totalTaskLabel: UILabel!
    
    @IBOutlet var totalDistanceVie: UIView!
    @IBOutlet var totalDistanceLabel: UILabel!
    
    @IBOutlet var totalTimeView: UIView!
    @IBOutlet var totalTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.totalPayoutView.layer.cornerRadius = 2.0
        self.totalPayoutView.layer.borderWidth = 1.0
        self.totalPayoutView.layer.borderColor = COLOR.LINE_COLOR.cgColor
        
        self.totalTaskView.layer.cornerRadius = 2.0
        self.totalTaskView.layer.borderWidth = 1.0
        self.totalTaskView.layer.borderColor = COLOR.LINE_COLOR.cgColor

        self.totalDistanceVie.layer.cornerRadius = 2.0
        self.totalDistanceVie.layer.borderWidth = 1.0
        self.totalDistanceVie.layer.borderColor = COLOR.LINE_COLOR.cgColor

        self.totalTimeView.layer.cornerRadius = 2.0
        self.totalTimeView.layer.borderWidth = 1.0
        self.totalTimeView.layer.borderColor = COLOR.LINE_COLOR.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
