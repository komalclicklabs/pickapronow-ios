//
//  TaskHistory.swift
//  Tookan
//
//  Created by Karan Kumar on 12/09/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import UIKit

class TaskHistory: NSObject, NSCoding {
    var creationDatetime: String!
    var taskDescription: String!
    var fleetId: Int!
    var IDs : Int!
    var jobId: Int!
    var latitude: Double!
    var longitude: Double!
    var type: String!
    var notes: String!
    var extra_fields:String? = ""
    
    required init(coder aDecoder: NSCoder) {
        creationDatetime = aDecoder.decodeObject(forKey: "creationDatetime") as! String
        taskDescription = aDecoder.decodeObject(forKey: "taskDescription") as! String
        fleetId = aDecoder.decodeObject(forKey: "fleetId") as! Int
        IDs = aDecoder.decodeObject(forKey: "IDs") as! Int
        jobId = aDecoder.decodeObject(forKey: "jobId") as! Int
        latitude = aDecoder.decodeObject(forKey: "latitude") as! Double
        longitude = aDecoder.decodeObject(forKey: "longitude") as! Double
        type = aDecoder.decodeObject(forKey: "type") as! String
        notes = aDecoder.decodeObject(forKey: "notes") as! String
        extra_fields = aDecoder.decodeObject(forKey: "extra_fields") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(creationDatetime, forKey: "creationDatetime")
        aCoder.encode(taskDescription, forKey: "taskDescription")
        aCoder.encode(fleetId, forKey: "fleetId")
        aCoder.encode(IDs, forKey: "IDs")
        aCoder.encode(jobId, forKey: "jobId")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(notes, forKey: "notes")
        aCoder.encode(extra_fields, forKey: "extra_fields")
    }
    
    init(json: NSDictionary) {
        
        if let creationDatetime = json["creation_datetime"] as? String{
            self.creationDatetime = creationDatetime
        } else {
            self.creationDatetime = ""
        }
        
        if let taskDescription = json["description"] as? String{
            self.taskDescription = taskDescription
        }else{
            self.taskDescription = ""
        }
        
        if let fleetId = json["fleet_id"] as? Int {
            self.fleetId = fleetId
        } else {
            self.fleetId = 0
        }
        
        if let ID = json["id"] as? Int {
            self.IDs = ID
        }else if let ID = json["id"] as? String {
            self.IDs = Int(ID)!
        }else{
            self.IDs = 0
        }
        
        if let jobId = json["job_id"] as? Int {
            self.jobId = jobId
        } else {
            self.jobId = 0
        }
        
        if let latitude = json["latitude"] as? Double {
            self.latitude = latitude
        } else {
            self.latitude = 0.0
        }
        
        if let longitude = json["longitude"] as? Double {
            self.longitude = longitude
        } else {
            self.longitude = 0.0
        }
        
        if let type = json["type"] as? String {
            self.type = type
        } else {
            self.type = ""
        }
        
        if let notes = json["notes"] as? String {
            self.notes = notes
        } else {
            self.notes = ""
        }
        
        if let value = json["extra_fields"] as? String {
            self.extra_fields = value
        }
    }
}
