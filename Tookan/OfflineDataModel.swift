//
//  OfflineDataModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 27/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class OfflineDataModel: NSObject {
    var hitFailStatus = 0
    let unsyncedObjectContext = DatabaseManager.sharedInstance.managedObjectContext
    var hitFailMessage:String!

    
    func createOfflineJsonData(_ pendingTask:[UnsyncedTask]!, unsyncTask:Task) -> Bool {
        var statusDictionaryArray = [Any]()
        if(pendingTask != nil) {
            statusDictionaryArray = self.createStatusJson(pendingTask)
        }
        //  print(statusDictionaryArray)
        
        let taskData = NSData(data: unsyncTask.value(forKey: "task_object") as! Data) as Data
        let taskDetails = NSKeyedUnarchiver.unarchiveObject(with: taskData) as! TasksAssignedToDate
        let notesDictionaryArray = self.createNotesJson(taskDetails.addedNotesArray!,deletesNotes: taskDetails.deletedNotesArray)
        let barcodeDictionaryArray = self.createBarcodeJson(taskDetails.addedScannerArray!, deletedBarcodes: taskDetails.deletedScannerArray)
        // print(notesDictionaryArray)
        
        var customFieldsDictionaryArray = [Any]()
        var customTableDataDictionaryArray = [Any]()
        for i in (0..<taskDetails.customFieldArray!.count) {
            let customFieldData = taskDetails.customFieldArray![i]
            if(customFieldData.dataType != CUSTOM_FIELD_DATA_TYPE.image && customFieldData.dataType != CUSTOM_FIELD_DATA_TYPE.table && customFieldData.sync == 0) {
                var dictionary:[String : Any] = ["custom_field_label":customFieldData.label]
                dictionary["data"] = customFieldData.data
                dictionary["timestamp"] = customFieldData.timestamp
                customFieldsDictionaryArray.append(dictionary)
            } else {
                if(customFieldData.dataType == CUSTOM_FIELD_DATA_TYPE.table) {
                    if(customFieldData.taskTableData.bodySyncStatus == 0) {
                        var dictionary:[String : Any] = ["custom_field_label":customFieldData.label]
                        dictionary["data"] = customFieldData.taskTableData.body.jsonString
                        dictionary["timestamp"] = customFieldData.taskTableData.bodySyncTimeStamp
                        dictionary["flag"] = 1
                        customTableDataDictionaryArray.append(dictionary)
                    }
                    
                    if(customFieldData.taskTableData.status.count > 0) {
                        for j in (0..<customFieldData.taskTableData.status.count) {
                            if(customFieldData.taskTableData.syncStatusArray.object(at: j) as! String == "0") {
                                var dictionary:[String : Any] = ["custom_field_label":customFieldData.label]
                                dictionary["data"] = customFieldData.taskTableData.status.object(at: j)
                                dictionary["timestamp"] = customFieldData.taskTableData.timeStampArray.object(at: j) as! String
                                dictionary["row"] = j
                                dictionary["col"] = customFieldData.taskTableData.indexOfStatusType!
                                customTableDataDictionaryArray.append(dictionary)
                            }
                        }
                    }
                }
            }
        }
        
        var dataJson:[String : Any] = ["statuses":statusDictionaryArray]
        dataJson["custom_field"] = customFieldsDictionaryArray
        dataJson["notes"] = notesDictionaryArray
        dataJson["tb_custom_field"] = customTableDataDictionaryArray
        dataJson["barcode"] = barcodeDictionaryArray
        if let response = NetworkingHelper.sharedInstance.sendRequestToUploadOfflineData(unsyncTask.job_id!, dataJson: dataJson) {
            switch(response["status"] as! Int) {
            case STATUS_CODES.SHOW_DATA:
                if let data  = response["data"] as? NSDictionary {
                    if let updatedJobId = data["job_id"] {
                        DatabaseManager.sharedInstance.deleteUnsyncedTaskFromDatabase(updatedJobId as! Int, userManagedContext: self.unsyncedObjectContext)
                    }
                }
                return true
            case  STATUS_CODES.DELETED_TASK:
                if let data  = response["data"] as? NSDictionary {
                    if let updatedJobId = data["job_id"] {
                        DatabaseManager.sharedInstance.deleteUnsyncedTaskFromDatabase(updatedJobId as! Int, userManagedContext: self.unsyncedObjectContext)
                        for section in 0..<NetworkingHelper.sharedInstance.dayTasksListArray.count {
                            var taskArray = NetworkingHelper.sharedInstance.dayTasksListArray[section] 
                            for row in 0..<taskArray.count {
                                let task  = taskArray[row] 
                                if task.jobId == updatedJobId as! Int {
                                    taskArray.remove(at: row)
                                    break
                                }
                            }
                            if taskArray.count == 0 {
                                NetworkingHelper.sharedInstance.dayTasksListArray.remove(at: section)
                                break
                            }
                        }
                        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.filterData), object: nil)
                    }
                }
                break
            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                self.hitFailStatus = STATUS_CODES.INVALID_ACCESS_TOKEN
                self.hitFailMessage = response["message"] as! String
                return false
            default:
                self.hitFailStatus = 0
                return false
            }
        }
        self.hitFailStatus = 0
        return false
    }
    
    func createStatusJson(_ pendingTask:[UnsyncedTask]) -> [Any] {
        var statusesArray = [Any]()
        for i in (0..<pendingTask.count) {
            let task = pendingTask[i]
            var dictionary:[String : Any] = ["job_status":task.job_status!]
            dictionary["reason"] = task.reason!
            dictionary["timestamp"] = task.timestamp!
            dictionary["latitude"] = task.latitude!
            dictionary["longitude"] = task.longitude!
            statusesArray.append(dictionary)
        }
        return statusesArray
    }
    
    func createNotesJson(_ notesData:[TaskHistory], deletesNotes:[TaskHistory]) -> [Any] {
        var notesArray = [Any]()
        for i in (0..<notesData.count) {
            let note = notesData[i]
            if(note.IDs! == 0) {
                var dictionary:[String : Any] = ["data":note.taskDescription]
                dictionary["type"] = note.type
                dictionary["timestamp"] = Auxillary.convertUTCStringToLocalString(note.creationDatetime)
                dictionary["latitude"] = note.latitude
                dictionary["longitude"] = note.longitude
                notesArray.append(dictionary)
            }
        }
        
        for j in (0..<deletesNotes.count) {
            let note = deletesNotes[j]
            var dictionary:[String : Any] = ["data":note.taskDescription]
            dictionary["type"] = note.type
            dictionary["id"] = note.IDs!
            dictionary["timestamp"] = Auxillary.convertUTCStringToLocalString(note.creationDatetime)
            dictionary["latitude"] = note.latitude
            dictionary["longitude"] = note.longitude
            notesArray.append(dictionary)
        }
        return notesArray
    }
    
    func createBarcodeJson(_ barcodeData:[TaskHistory], deletedBarcodes:[TaskHistory]) -> [Any] {
        var barcodeArray = [Any]()
        for i in (0..<barcodeData.count) {
            let barcode = barcodeData[i]
            if(barcode.IDs! == 0) {
                //  print(Auxillary.convertUTCStringToLocalString(note.creationDatetime))
                var dictionary:[String : Any] = ["data":barcode.taskDescription]
                dictionary["type"] = barcode.type
                dictionary["timestamp"] = Auxillary.convertUTCStringToLocalString(barcode.creationDatetime)
                dictionary["latitude"] = barcode.latitude
                dictionary["longitude"] = barcode.longitude
                barcodeArray.append(dictionary)
            }
        }
        
        for j in (0..<deletedBarcodes.count) {
            let barcode = deletedBarcodes[j]
            var dictionary:[String : Any] = ["data":barcode.taskDescription]
            dictionary["type"] = barcode.type
            dictionary["id"] = barcode.IDs!
            dictionary["timestamp"] = Auxillary.convertUTCStringToLocalString(barcode.creationDatetime)
            dictionary["latitude"] = barcode.latitude
            dictionary["longitude"] = barcode.longitude
            barcodeArray.append(dictionary)
        }
        return barcodeArray
    }
    
    
    func isThereAnyUnsyncedTaskInDatabase() -> Int {
        var count = 0
        let unsyncedTask = DatabaseManager.sharedInstance.fetchAllUnsyncTaskFromDatabase(self.unsyncedObjectContext)
        if(unsyncedTask != nil && (unsyncedTask?.count)! > 0) {
            count = count + (unsyncedTask?.count)!
        }
        let pendingImages = DatabaseManager.sharedInstance.fetchImagesFromDatabase(self.unsyncedObjectContext)
        if(pendingImages != nil && (pendingImages?.count)! > 0) {
            count = count + (pendingImages?.count)!
        }
        
        return count
    }

    func uploadUnsyncedTaskToServer() -> Bool {
        let totalTaskToUpload = Float(isThereAnyUnsyncedTaskInDatabase())
        var completedTask:Float = 0.0
        if(totalTaskToUpload > 0) {
            NetworkingHelper.sharedInstance.showSyncView()
        }
        let unsyncedTask = DatabaseManager.sharedInstance.fetchAllUnsyncTaskFromDatabase(self.unsyncedObjectContext)
        if(unsyncedTask != nil && (unsyncedTask?.count)! > 0) {
            for i in (0..<unsyncedTask!.count) {
                let pendingTask = DatabaseManager.sharedInstance.fetchUnsyncedTaskFromDatabase(self.unsyncedObjectContext, jobID: unsyncedTask?[i].job_id! as! Int)
                NetworkingHelper.sharedInstance.updateProgressValue(Float(completedTask / totalTaskToUpload))
                if self.createOfflineJsonData(pendingTask, unsyncTask: (unsyncedTask?[i])!) == false {
                    NetworkingHelper.sharedInstance.hideSyncView()
                    return false
                } else {
                    completedTask = completedTask + 1
                    let taskData = NSData(data: unsyncedTask?[i].value(forKey: "task_object") as! Data) as Data
                    let taskDetails = NSKeyedUnarchiver.unarchiveObject(with: taskData) as! TasksAssignedToDate
                    _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: taskDetails.jobId, taskDetails: taskDetails, syncStatus: 1)
                    NetworkingHelper.sharedInstance.updateProgressValue(Float(completedTask / totalTaskToUpload))
                }
            }
        }
        
        if(uploadOfflineImageData(totalTaskToUpload, completedTask: completedTask) == false) {
            NetworkingHelper.sharedInstance.hideSyncView()
            return false
        }
        NetworkingHelper.sharedInstance.hideSyncView()
        return true
    }
    
    func uploadOfflineImageData(_ totalTask:Float, completedTask:Float) -> Bool {
        var completedTaskForUpload = completedTask
        let pendingImages = DatabaseManager.sharedInstance.fetchImagesFromDatabase(self.unsyncedObjectContext)
        var response:[String:Any]!
        guard pendingImages != nil else {
            return true
        }
        if((pendingImages?.count)! > 0) {
            for imageData in pendingImages! {
                
                var job_id:Int? = 0
                var custom_label:String = ""
                var image:String? = ""
                var type:Int? = 0
                var caption:String? = ""
                
                if let value = imageData.type {
                    type = value.intValue
                }
                
                if let value = imageData.custom_label {
                    custom_label = value
                }
                
                if let value = imageData.image {
                    image = value
                }
                
                if let value = imageData.job_id {
                    job_id = value.intValue
                }
                
                if let value = imageData.caption {
                    caption = value
                }
                
                // print(Float(completedTaskForUpload / totalTask))
                NetworkingHelper.sharedInstance.updateProgressValue(Float(completedTaskForUpload / totalTask))
                switch type! {
                case DatabaseImageType.customField:
                    response = self.sendSynchrounousRequestToUploadCustomFieldImage(custom_label, jobId: job_id!, data: "", image: image!, caption: caption!)
                    break
                case DatabaseImageType.image:
                    response = self.sendSynchrounousRequestToUploadSignatureImage(job_id!, image: image!, type: "image_added", caption: caption!)
                    break
                case DatabaseImageType.signature:
                    response = self.sendSynchrounousRequestToUploadSignatureImage(job_id!, image: image!, type: "signature_image_added", caption: caption!)
                    break
                default:
                    break
                }
                
                if response != nil {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA, STATUS_CODES.DELETED_TASK:
                        completedTaskForUpload = completedTaskForUpload + 1
                        NetworkingHelper.sharedInstance.updateProgressValue(Float(completedTaskForUpload / totalTask))
                        if DatabaseManager.sharedInstance.deleteImageFromDatabase(image!, imageManagedContext: self.unsyncedObjectContext) == true {
                            Auxillary.deleteImageFromDocumentDirectory(image!)
                        }
                        
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.hitFailStatus = STATUS_CODES.INVALID_ACCESS_TOKEN
                        return false
                    default:
                        self.hitFailStatus = 0
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        return false
                    }
                } else {
                    self.hitFailStatus = 0
                    return false
                }
            }
        }
        return true
    }
    
    /*-------------- Upload Offline Image for custom Field -----------*/
    func sendSynchrounousRequestToUploadCustomFieldImage(_ customFieldLabel:String,jobId:Int,data:String,image:String, caption:String) -> [String:Any]! {
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var params:[String:Any] = ["access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["custom_field_label"] = customFieldLabel
        params["job_id"] = jobId
        params["data"] = data
        params["lat"] = lat
        params["lng"] = lng
        params["caption"] = caption
        let imageData = try? Data(contentsOf: URL(fileURLWithPath: Auxillary.createPath(image)))
        if(imageData != nil) {
            if IJReachability.isConnectedToNetwork() == true {
                //CREATE AND SEND REQUEST ----------
                if let response = sendSynchronousRequestToUploadImage(API_NAME.updateCustomField, params: params as [String:AnyObject], imageData: imageData, imageKey: "custom_image") {
                    return response
                } else {
                    return nil
                }
            } else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
                return nil
            }
        } else {
            if DatabaseManager.sharedInstance.deleteImageFromDatabase(image, imageManagedContext: self.unsyncedObjectContext) == true {
                Auxillary.deleteImageFromDocumentDirectory(image)
            }
            return nil
        }
    }
    
    /*------------- Upload offline image for Signature --------------*/
    func sendSynchrounousRequestToUploadSignatureImage(_ jobId:Int, image:String, type:String, caption:String) -> [String:Any]!{
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var params:[String:Any] = ["access_token": Singleton.sharedInstance.getAccessToken()]
        params["text"] = ""
        params["job_id"] = "\(jobId)"
        params["type"] = type
        params["lat"] = lat
        params["lng"] = lng
        params["caption"] = caption
        let imageData = try? Data(contentsOf: URL(fileURLWithPath: Auxillary.createPath(image)))
        if imageData != nil {
            if IJReachability.isConnectedToNetwork() == true {
                //CREATE AND SEND REQUEST ----------
                if let response = sendSynchronousRequestToUploadImage(API_NAME.add_task_detail, params: params as [String:AnyObject], imageData: imageData, imageKey: "image") {
                    return response
                } else {
                    return nil
                }
            } else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
                return nil
            }
        } else {
            if DatabaseManager.sharedInstance.deleteImageFromDatabase(image, imageManagedContext: self.unsyncedObjectContext) == true {
                Auxillary.deleteImageFromDocumentDirectory(image)
            }
            return nil
        }
    }

}
