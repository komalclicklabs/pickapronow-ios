//
//  CalendarCollectionViewCell.swift
//  Tookan
//
//  Created by Click Labs on 7/14/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class CalendarCollectionViewCell: UICollectionViewCell {
    
//     var date: UILabel! = UILabel()
//     var completedTaskStatus: UILabel!  = UILabel()
//     var pendingTaskStatus: UILabel! = UILabel()
    
    @IBOutlet var background: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pendingTaskView: UIView!
    @IBOutlet weak var bottomLine: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.white
        /*=========== DateLabel ===========*/
        self.dateLabel.textColor = COLOR.TEXT_COLOR
        self.dateLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.dateLabel.backgroundColor = UIColor.clear

        /*============== Backgound View ============*/
        self.background.backgroundColor = UIColor.white
        self.background.layer.cornerRadius = 25 / 2
        
        self.pendingTaskView.layer.cornerRadius = 5 / 2
        self.pendingTaskView.backgroundColor = COLOR.themeForegroundColor
        self.pendingTaskView.isHidden = true
        
        self.bottomLine.backgroundColor = COLOR.LINE_COLOR
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
 }
