//
//  PathSummaryView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 10/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class PathSummaryView: UIView {

    @IBOutlet var milesLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var taskLabel: UILabel!
    @IBOutlet var milesCoveredLabel: UILabel!
    @IBOutlet var hoursLabel: UILabel!
    @IBOutlet var taskCompletedLabel: UILabel!
    var milesTimer:Timer!
    var hoursTimer:Timer!
    var taskTimer:Timer!
    var mileCounter = 0
    var hoursCounter:Double = 0
    var taskCounter = 0
    var mileLimit = 0
    var hoursLimit:Double = 0
    var taskLimit = 0

    override func awakeFromNib() {
        milesCoveredLabel.text = ""
        hoursLabel.text = ""
        taskCompletedLabel.text = ""
    }
    
    
    func setCountForMiles(limit:Int, unit:String) {
        
        if unit == "KM" {
            milesCoveredLabel.text = "Km(s) Covered".localized
        } else {
            milesCoveredLabel.text = "Mile(s) Covered".localized
        }
        
        hoursLabel.text = "Hour(s)".localized
        taskCompletedLabel.text = "Task(s) Completed".localized
        
        guard limit > 0 else {
            return
        }
        
        var dividend:Double = 0.0
        if(limit < 20) {
            dividend = Double(1 / Float(limit))
        } else if limit <= 1000 {
            dividend = Double(2 / Float(limit))
        } else {
            dividend = 0.001
        }
        milesTimer = Timer.scheduledTimer(timeInterval: dividend, target: self, selector: #selector(self.setMiles), userInfo: nil, repeats: true)
        mileCounter = 0
        mileLimit = limit
    }
    
    func setMiles() {
        
        var incrementValue = 1
        if(mileLimit > 1000) {
            incrementValue = mileLimit / 1000
        }
        if mileCounter < mileLimit {
            mileCounter += incrementValue
            self.milesLabel.text = "\(mileCounter)"
        } else {
            self.milesLabel.text = "\(mileLimit)"
            milesTimer.invalidate()
            milesTimer = nil
            mileCounter = 0
        }
    }
    
    func setCountForHours(limit:Double) {
        guard limit > 0 else {
            return
        }
        var dividend:Double = 0.0
        if(limit < 2.0) {
            dividend = Double(1 / (limit*10))
        } else if limit <= 10.0 {
            dividend = Double(2 / (limit*10))
        } else {
            dividend = 0.001
        }
        hoursTimer = Timer.scheduledTimer(timeInterval: dividend, target: self, selector: #selector(self.setHours), userInfo: nil, repeats: true)
        hoursCounter = 0
        hoursLimit = limit
    }
    
    func setHours() {
        var incrementValue = 0.1
        if(hoursLimit > 10.0) {
            incrementValue = hoursLimit / 100
        }
        if hoursCounter < hoursLimit {
            hoursCounter += incrementValue
            self.timeLabel.text = String(format: "%.1f", hoursCounter) //"\(hoursCounter)"
        } else {
            guard hoursTimer != nil else {
                hoursCounter = 0
                return
            }
            hoursTimer.invalidate()
            hoursTimer = nil
            hoursCounter = 0
        }
    }
    
    func setCountForTask(limit:Int) {
        guard limit > 0 else {
            return
        }
        var dividend:Double = 0.0
        if(limit < 20) {
            dividend = Double(1 / Float(limit))
        } else if limit <= 1000 {
            dividend = Double(2 / Float(limit))
        } else {
            dividend = 0.001
        }
        
        taskTimer = Timer.scheduledTimer(timeInterval: dividend, target: self, selector: #selector(self.setTasks), userInfo: nil, repeats: true)
        taskCounter = 0
        taskLimit = limit
    }
    
    func setTasks() {
        var incrementValue = 1
        if(taskLimit > 1000) {
            incrementValue = taskLimit / 1000
        }
        if taskCounter < taskLimit {
            taskCounter += incrementValue
            self.taskLabel.text = "\(taskCounter)"
        } else {
            taskTimer.invalidate()
            taskTimer = nil
            taskCounter = 0
        }
    }
}
