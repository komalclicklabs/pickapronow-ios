//
//  AppUpdateDetails.swift
//  Tookan
//
//  Created by Click Labs on 7/8/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation

class AppUpdateDetails: NSObject {
    
    var title: String!
    var text:  String!
    var cur_version: Int!
    var app_url: String!
    var is_force: Int!
    
    init?(json: NSDictionary) {
        
        super.init()
        
        if let title = json["title"] as? String {
           if title.isEmpty {
                return nil
            }
            self.title = title
        }
        if let text = json["text"] as? String {
            if text.isEmpty{
                return nil
            }
            self.text = text
        }
        if let cur_version = json["cur_version"] as? Int {
            if "\(cur_version)".isEmpty{
                return nil
            }
            self.cur_version = cur_version
        }
        if let app_url = json["app_url"] as? String {
            if app_url.isEmpty{
                return nil
            }
            self.app_url = app_url
        }
        if let is_force = json["is_force"] as? Int {
            if "\(is_force)".isEmpty{
                return nil
            }
            self.is_force = is_force
            
        }
    }

}