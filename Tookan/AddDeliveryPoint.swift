//
//  AddDeliveryPoint.swift
//  Tookan
//
//  Created by cl-macmini-150 on 05/04/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import Crashlytics
import FirebaseAnalytics

enum DELIVERY_OR_PICKUP:Int{
    case pickup
    case delivery
}

enum CREATE_TASK_FIELDS:Int {
    case description = 0
    case phoneNO
    case email
    case name
    case date
    case endDate
    case address
    case template
}


class AddDeliveryPoint: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let keyboardMargin:CGFloat = 30.0
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    var currentRect:CGRect = CGRect.zero
    var createTaskFieldArray = [CREATE_TASK_FIELDS]()
    var customPicker:CustomDatePicker!
    var dateOfDatePicker : String = ""
    var endDateOfDatePicker : String = ""
    var dateInDateFormat = Date()
    var endDateInDateFormat = Date()
    var indicationOfType: Int! = 0
    var selectedTaskIndex = -1
    var currentTextField = 0
    var bottomConstraint: CGFloat = -60.0
    var pickupLocation:CLLocation!
    var selectedField:UITextView!
    var templateArray = [String]()
    var arrayForLocalData: CreateTaskModel!
    var navigation:navigationBar!
    var drowDownWithSearch:TemplateController!
    let headerHeight:CGFloat = 15.0
    let footerHeight:CGFloat = 125.0
    
    @IBOutlet weak var tableViewToCreateNewOrder: UITableView!
    @IBOutlet weak var saveDataOutlet: UIButton!
    @IBOutlet var bottomConstraintOfTable: NSLayoutConstraint!
    
    var descriptionOfTask = ""
    var phone = ""
    var email = ""
    var name = ""
    var date = ""
    var endDate = ""
    var address = ""
    var template = ""
    var latitude = ""
    var longitude = ""
    //var callback : ((String) -> Void)?
    var callBack:((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        
        /*===============   Setting Save Button   ========================*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
        case LAYOUT_TYPE.pickupAndDelivery:
            createTaskFieldArray = [.description, .phoneNO, .email, .name, .date, .address, .template]
            if self.indicationOfType == 1 {
                self.saveDataOutlet.setTitle("\(TEXT.SAVE) \(TEXT.DELIVERY) \(TEXT.POINT)", for: .normal)
            }
            else {
                self.saveDataOutlet.setTitle("\(TEXT.SAVE) \(TEXT.PICKUP) \(TEXT.POINT)", for: .normal)
            }
            break
        case LAYOUT_TYPE.appointment:
            createTaskFieldArray = [.description, .phoneNO, .email, .name, .date, .endDate, .address, .template]
            self.saveDataOutlet.setTitle("\(TEXT.CREATE) \(TEXT.APPOINTMENT)", for: .normal)
        default:
            createTaskFieldArray = [.description, .phoneNO, .email, .name, .date, .endDate, .address, .template]
            self.saveDataOutlet.setTitle("\(TEXT.CREATE) \(TEXT.FIELD_WORKFORCE)", for: .normal)
            break
        }
        
        /*===============   Save Button   ========================*/
        self.saveDataOutlet.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        self.saveDataOutlet.backgroundColor = COLOR.themeForegroundColor
        self.saveDataOutlet.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.saveDataOutlet.transform = CGAffineTransform(translationX: 0, y: HEIGHT.navigationHeight)
        self.setTableView()
        
        /*===============   Setting Date   ====================*/
        let schedulePicker: Date = Date()
        self.dateInDateFormat = schedulePicker.addingTimeInterval(15.0 * 60.0)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        dateOfDatePicker = formatter.string(from: dateInDateFormat)
        date = dateOfDatePicker
        
        self.endDateInDateFormat = schedulePicker.addingTimeInterval(45.0 * 60.0)
        endDateOfDatePicker = formatter.string(from: endDateInDateFormat)
        endDate = endDateOfDatePicker
        
        self.editingData()
    }
    
    func setTableView() {
        /*===============   Setting Table View   ========================*/
        tableViewToCreateNewOrder.layer.cornerRadius = getAspectRatioValue(value: 15)
        self.tableViewToCreateNewOrder.register(UINib(nibName: NIB_NAME.createTaskCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.createTaskCell)
        self.tableViewToCreateNewOrder.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height - HEIGHT.navigationHeight)
        self.tableViewToCreateNewOrder.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    func editingData(){
        /*===============   Setting On Edit View   ====================*/
        if selectedTaskIndex != -1 {
            navigation.editButton.isHidden = false
            let currentTask = Singleton.sharedInstance.createTaskArray[selectedTaskIndex]
            descriptionOfTask = currentTask.taskDescription
            phone = currentTask.phone
            email = currentTask.email
            name = currentTask.name
            address = currentTask.address
            template = currentTask.template
            date = currentTask.startTime

            switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
            case LAYOUT_TYPE.pickupAndDelivery:
                if isPickup() == true {
                    self.navigation.titleLabel.text = "\(TEXT.EDIT) \(TEXT.PICKUP) \(TEXT.POINT)"// + TEXT.PICKUP + " " + TEXT.POINT
                    saveDataOutlet.setTitle("\(TEXT.SAVE) \(TEXT.PICKUP) \(TEXT.POINT)", for: .normal)
                } else{
                    self.navigation.titleLabel.text = "\(TEXT.EDIT) \(TEXT.DELIVERY) \(TEXT.POINT)"// + TEXT.DELIVERY + " " + TEXT.POINT
                    saveDataOutlet.setTitle("\(TEXT.SAVE) \(TEXT.DELIVERY) \(TEXT.POINT)", for: .normal)
                }
                break
            case LAYOUT_TYPE.appointment:
                self.navigation.titleLabel.text = "\(TEXT.EDIT) \(TEXT.APPOINTMENT) \(TEXT.POINT)"// + TEXT.PICKUP + " " + TEXT.POINT
                saveDataOutlet.setTitle("\(TEXT.SAVE) \(TEXT.APPOINTMENT) \(TEXT.POINT)", for: .normal)
                break
            case LAYOUT_TYPE.fos:
                self.navigation.titleLabel.text = "\(TEXT.EDIT) \(TEXT.FIELD_WORKFORCE) \(TEXT.POINT)"// + TEXT.PICKUP + " " + TEXT.POINT
                saveDataOutlet.setTitle("\(TEXT.SAVE) \(TEXT.FIELD_WORKFORCE) \(TEXT.POINT)", for: .normal)
                break
            default:
                break
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        /*===============   Removing Observer for Keyborad   ====================*/
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        /*===============   Setting Current Address   ====================*/
        if(LocationTracker.sharedInstance().myLocation != nil) {
            CLGeocoder().reverseGeocodeLocation(LocationTracker.sharedInstance().myLocation, completionHandler: { (placemarks, error) -> Void in
                if error != nil {
                    return
                } else {
                    let pm = placemarks![0]
                    let address = (pm.addressDictionary!["FormattedAddressLines"] as! NSArray).componentsJoined(by: ", ")
                    DispatchQueue.main.async {
                        self.setCurrentAddress(address: address, currentLocation: LocationTracker.sharedInstance().myLocation)
                    }
                }
            })
        }
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.tableViewToCreateNewOrder.transform = CGAffineTransform.identity
            self.navigation.titleLabel.transform = CGAffineTransform.identity
        }, completion: nil)
        
        UIView.animate(withDuration: 0.3) {
            self.saveDataOutlet.transform = CGAffineTransform.identity
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getTemplate()
        /*===============   Setting Observer for Keyborad   ====================*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func getTemplate(){
        /*===============   Setting the List of Templates   ====================*/
        var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["fleet_id"] = Singleton.sharedInstance.fleetDetails.fleetId!
        params["layout_type"] = UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)
//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//            "fleet_id":Singleton.sharedInstance.fleetDetails.fleetId!,
//            "layout_type":UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)
//            ] as [String : Any]
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getTemplates, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        if let array = response["data"] as? [String] {
                            self.templateArray = array//NSMutableArray(array: array)
                        }
//                        } else {
//                            self.templateArray = array//NSMutableArray()
//                        }
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        let alert = UIAlertController(title: "", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.dismiss(animated: true, completion: {
                                    Auxillary.logoutFromDevice()
                                    NotificationCenter.default.removeObserver(self)
                                    
                                })
                            })
                        })
                        alert.addAction(actionPickup)
                        self.present(alert, animated: true, completion: nil)
                        break
                    default:
                        break
                    }
                }
            }
        }
    }

    func enableSaveButton() {
        self.saveDataOutlet.isEnabled = true
    }
    
    @IBAction func saveDeliveryPoint(_ sender: Any) {
        self.saveDataOutlet.isEnabled = false
        self.perform(#selector(self.enableSaveButton), with: nil, afterDelay: 0.5)
        if selectedTaskIndex != -1 {
            let currentTask = Singleton.sharedInstance.createTaskArray[selectedTaskIndex]
            if self.checkValidation() == false{
                currentTask.taskDescription = descriptionOfTask
                currentTask.phone = phone
                currentTask.email = email
                currentTask.name = name
                currentTask.address = address
                currentTask.template = template
                currentTask.latitude = latitude
                currentTask.longitude = longitude
                currentTask.startTime = date

                switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
                case LAYOUT_TYPE.pickupAndDelivery:
                    if isPickup() == true {
                        currentTask.has_pickup = 1
                        currentTask.jobType = 0
                    } else{
                        currentTask.has_delivery = 1
                        currentTask.jobType = 1
                    }
                    break
                default:
                    currentTask.has_pickup = 0
                    currentTask.has_delivery = 0
                    break
                }
                
                UIView.animate(withDuration: 0.35, animations: {
                    self.tableViewToCreateNewOrder.transform = CGAffineTransform(translationX: 0, y: self.tableViewToCreateNewOrder.frame.height)
                    self.saveDataOutlet.transform = CGAffineTransform(translationX: 0, y: self.saveDataOutlet.frame.height)
                    self.navigation.titleLabel.transform = CGAffineTransform(translationX: SCREEN_SIZE.width, y: 0)
                }, completion: { finished in
                    _ = self.navigationController?.popViewController(animated: false)
                })
            }
        } else {
            if checkValidation() == false{
                let createTaskDetail = CreateTaskModel()
                createTaskDetail.taskDescription = descriptionOfTask
                createTaskDetail.phone = phone
                createTaskDetail.email = email
                createTaskDetail.name = name
                createTaskDetail.address = address
                createTaskDetail.template = template
                createTaskDetail.latitude = latitude
                createTaskDetail.longitude = longitude
                createTaskDetail.startTime = date
                switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
                case LAYOUT_TYPE.pickupAndDelivery:
                    switch indicationOfType {
                    case DELIVERY_OR_PICKUP.pickup.rawValue:
                        createTaskDetail.has_pickup = 1
                        createTaskDetail.jobType = 0
                    default:
                        createTaskDetail.has_delivery = 1
                        createTaskDetail.jobType = 1
                    }
                    break
                    
                case LAYOUT_TYPE.appointment:
                    createTaskDetail.startTime = date
                    createTaskDetail.endTime = endDate
                    createTaskDetail.jobType = 2
                default:
                    createTaskDetail.startTime = date
                    createTaskDetail.endTime = endDate
                    createTaskDetail.jobType = 3
                    break
                }
                Singleton.sharedInstance.createTaskArray.append(createTaskDetail)
                
               
                switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
                case LAYOUT_TYPE.pickupAndDelivery:
                    self.dismissController()
                    break
                default:
                    self.dismissController()
                    //self.createTaskAction()
                    break
                }
            }
        }
    }
    
    func createTaskAction() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        let model = CreateTaskModel()
        let params = model.setJsonForCreateTaskRequest()
        print(params)
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.fleet_create_multiple_tasks, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                print(response)
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        print(response)
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .success)
                        self.dismissController()
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    default:
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: Validations
    func checkValidation() -> Bool {
        var errorMessage = ""
        if (((phone.length < 8) || (phone.length > 16)) && (phone.isEmpty == false)){
            errorMessage = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.PHONE_NUMBER)"  //"Please enter valid phone number"
            Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
            return true
        }
        
        if((email.isEmpty == false) && (Auxillary.validateEmail(email.trimText) == false)) {
            errorMessage = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.EMAIL)" //"Please enter valid email"
            Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
            return true
        }
        
        if self.address.isEmpty == true {
            errorMessage = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.ADDRESS)"//"Please enter address"
            Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
            return true
        }
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"//"MM/dd/yyyy HH:mm"
        let startDate = dateFormatter.date(from: date)
        guard startDate != nil else {
            errorMessage = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.TIME)"
            Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
            return true
        }
        
        
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
        case LAYOUT_TYPE.pickupAndDelivery:
            if self.date.isEmpty == true {
                errorMessage = "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) \(TEXT.TIME)" //"Please Enter Time"
                Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
                return true
            }
            if indicationOfType == 0{
                if(startDate!.timeIntervalSince(Date()) < 0) {
                    errorMessage = ERROR_MESSAGE.PICKUP_TIME_CURRENT_TIME//"Pickup time must be greater than current time"
                    Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
                    return true
                }
            }
            else{
                if(startDate!.timeIntervalSince(Date()) < 0) {
                    errorMessage =  ERROR_MESSAGE.DELIVERY_TIME_CURRENT_TIME //"Delivery time must be greater than current time"
                    Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
                    return true
                }
            }
            
            break
        default:
            if self.date.isEmpty == true {
                errorMessage = "\(ERROR_MESSAGE.PLEASE_ENTER) \(TEXT.START_TIME)" //"Please Enter Start Time"
                Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
                return true
            }
            if self.endDate.isEmpty == true{
                errorMessage = "\(ERROR_MESSAGE.PLEASE_ENTER) \(TEXT.END_TIME)"//"Please Enter End Time"
                Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
                return true
            }
            else{
                let endDate = dateFormatter.date(from: self.endDate)
                if let timeInterval = endDate?.timeIntervalSince(startDate!){
                    if(Double(timeInterval) < 60.0) {
                        errorMessage = ERROR_MESSAGE.END_TIME_START_TIME//"End time must be greater than start time"
                        Singleton.sharedInstance.showErrorMessage(error: errorMessage, isError: .error)
                        return true
                    }
                }
                
            }
            break
        }
        return false
    }
    
    //MARK: Calling Custom Date picker
    func showCustomDatePicker(index: Int) {
        self.view.endEditing(true)
        customPicker = UINib(nibName: NIB_NAME.customDatePicker, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomDatePicker
        customPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        customPicker.delegate = self
        self.view.addSubview(customPicker)
        customPicker.tag = index
        customPicker.setDatePicker()
        customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        customPicker.datePicker.minimumDate = dateInDateFormat
    }
    
    //MARK: UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.createTaskFieldArray.count
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.createTaskCell, for: indexPath) as! CreateTaskCell
        cell.delegate = self
        cell.textViewCell.tag = createTaskFieldArray[indexPath.row].rawValue
        switch createTaskFieldArray[indexPath.row] {
        case CREATE_TASK_FIELDS.description:
            cell.imageHolder.image = #imageLiteral(resourceName: "details")
            cell.placeHolderLabel.text = TEXT.ENTER_DESCRIPTION
            cell.downArrowImage.isHidden = true
            cell.imageHolder.isHidden = false
            cell.leadingFromImageTOTextView.constant = 14
            cell.textViewCell.text = descriptionOfTask
            cell.textViewCell.keyboardType = UIKeyboardType.default
            if cell.textViewCell.text != ""{
                cell.tranlatePlaceholderLabel(status: true)
            }else{
                cell.tranlatePlaceholderLabel(status: false)
            }
            
            
        case CREATE_TASK_FIELDS.phoneNO:
            cell.imageHolder.image = #imageLiteral(resourceName: "telephone")
            cell.placeHolderLabel.text = TEXT.PHONE_NUMBER
            cell.downArrowImage.isHidden = true
            cell.imageHolder.isHidden = false
            cell.leadingFromImageTOTextView.constant = 14
            cell.textViewCell.text = phone
            cell.textViewCell.keyboardType = UIKeyboardType.numberPad
            if cell.textViewCell.text != ""{
                cell.tranlatePlaceholderLabel(status: true)
            }else{
                cell.tranlatePlaceholderLabel(status: false)
            }
            
        case CREATE_TASK_FIELDS.email:
            cell.imageHolder.image = #imageLiteral(resourceName: "customEmail")
            cell.placeHolderLabel.text = TEXT.EMAIL
            cell.downArrowImage.isHidden = true
            cell.imageHolder.isHidden = false
            cell.leadingFromImageTOTextView.constant = 14
            cell.textViewCell.text = email
            cell.textViewCell.keyboardType = UIKeyboardType.emailAddress
            if cell.textViewCell.text != ""{
                cell.tranlatePlaceholderLabel(status: true)
            }else{
                cell.tranlatePlaceholderLabel(status: false)
            }
            
        case CREATE_TASK_FIELDS.name:
            cell.imageHolder.image = #imageLiteral(resourceName: "user")
            cell.placeHolderLabel.text = TEXT.NAME
            cell.downArrowImage.isHidden = true
            cell.imageHolder.isHidden = false
            cell.leadingFromImageTOTextView.constant = 14
            cell.textViewCell.text = name
            cell.textViewCell.keyboardType = UIKeyboardType.default
            if cell.textViewCell.text != ""{
                cell.tranlatePlaceholderLabel(status: true)
            }else{
                cell.tranlatePlaceholderLabel(status: false)
            }
            
        case CREATE_TASK_FIELDS.date:
            cell.imageHolder.image = #imageLiteral(resourceName: "customCalendar")
            cell.placeHolderLabel.text = TEXT.DATE
            cell.textViewCell.text = date
            cell.downArrowImage.isHidden = true
            cell.imageHolder.isHidden = false
            cell.leadingFromImageTOTextView.constant = 14
            if cell.textViewCell.text != ""{
                cell.tranlatePlaceholderLabel(status: true)
            }else{
                cell.tranlatePlaceholderLabel(status: false)
            }
            
        case CREATE_TASK_FIELDS.endDate:
            cell.imageHolder.image = #imageLiteral(resourceName: "customCalendar")
            cell.placeHolderLabel.text = TEXT.END_DATE
            cell.textViewCell.text = endDateOfDatePicker
            cell.downArrowImage.isHidden = true
            cell.imageHolder.isHidden = false
            cell.leadingFromImageTOTextView.constant = 14
            if cell.textViewCell.text != ""{
                cell.tranlatePlaceholderLabel(status: true)
            }else{
                cell.tranlatePlaceholderLabel(status: false)
            }
            
        case CREATE_TASK_FIELDS.address:
            cell.imageHolder.image = #imageLiteral(resourceName: "blackPin")
            cell.placeHolderLabel.text = TEXT.ADDRESS
            cell.downArrowImage.isHidden = true
            cell.imageHolder.isHidden = false
            cell.leadingFromImageTOTextView.constant = 14
            cell.textViewCell.text = address
            if cell.textViewCell.text != ""{
                cell.tranlatePlaceholderLabel(status: true)
            }else{
                cell.tranlatePlaceholderLabel(status: false)
            }
            
        case CREATE_TASK_FIELDS.template:
            cell.placeHolderLabel.text = TEXT.SELECT_TEMPLATE
            cell.imageHolder.isHidden = true
            cell.leadingFromImageTOTextView.constant = -12
            cell.downArrowImage.isHidden = false
            cell.textViewCell.text = template
            if cell.textViewCell.text != ""{
                cell.tranlatePlaceholderLabel(status: true)
            }
            else{
                cell.tranlatePlaceholderLabel(status: false)
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    } 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: headerHeight))
        returnedView.backgroundColor = .white
        return returnedView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return footerHeight
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: footerHeight))
        returnedView.backgroundColor  = .clear
        return returnedView
    }
    
    //MARK: Keyboard Observer
    
    func keyboardWillShow(_ notification : Foundation.Notification){
        //print(keyboardSize.height)
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        Singleton.sharedInstance.keyboardSize = value.cgRectValue.size
        keyboardSize = value.cgRectValue.size
        self.bottomConstraintOfTable.constant = keyboardSize.height - 15
        let indexPath = NSIndexPath.init(row: self.currentTextField, section: 0)
        //self.tableViewToCreateNewOrder.setNeedsLayout()
//        UIView.animate(withDuration: 0.1) {
//            self.tableViewToCreateNewOrder.layoutIfNeeded()
            self.tableViewToCreateNewOrder.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
//        }
        Singleton.sharedInstance.translateErrorMessage(toBottom: false)
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        bottomConstraintOfTable.constant = bottomConstraint
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }
    
    //MARK: Checking Type
    func isPickup() -> Bool {
        if Singleton.sharedInstance.createTaskArray[selectedTaskIndex].has_pickup == 1{
            return true
        }
        return false
    }
}


//MARK: CreateTaskCellDelegate Delegate Methods
extension AddDeliveryPoint:CreateTaskCellDelegate {
    func updateTableHeight(descriptionText: String) {
        UIView.setAnimationsEnabled(false) // Disable animations
        self.tableViewToCreateNewOrder.beginUpdates()
        self.tableViewToCreateNewOrder.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func customCellShouldReturn(textView: UITextView, index: Int)  {
        if let cell = self.tableViewToCreateNewOrder.cellForRow(at: IndexPath(row: index, section: 0)) as? CreateTaskCell{
            let cell1 = self.tableViewToCreateNewOrder.cellForRow(at: IndexPath(row: index-1, section: 0)) as? CreateTaskCell
            if index >= CREATE_TASK_FIELDS.date.rawValue {
                cell1?.textViewCell.resignFirstResponder()
                cell.textViewCell.becomeFirstResponder()
            }
            else{
                cell.textViewCell.becomeFirstResponder()
            }
        }
    }
    
    func showDatePicker(_ index: Int) {
        self.showCustomDatePicker(index: index)
    }
    
    func customCellShouldBeginEditing(textView: UITextView, index: Int) -> Void {
        switch createTaskFieldArray[index] {
        case CREATE_TASK_FIELDS.description:
            self.descriptionOfTask = textView.text
        case CREATE_TASK_FIELDS.phoneNO:
            self.phone = textView.text
        case CREATE_TASK_FIELDS.email:
            self.email = textView.text
        case CREATE_TASK_FIELDS.name:
            self.name = textView.text
        case CREATE_TASK_FIELDS.date:
            self.date = textView.text
        case CREATE_TASK_FIELDS.endDate:
            self.date = textView.text
        case CREATE_TASK_FIELDS.address:
            self.address = textView.text
            print(address)
        default:
            break
        }
    }
    
    func setCurrentAddress(address:String, currentLocation:CLLocation) {
        if let cell = self.tableViewToCreateNewOrder.cellForRow(at: IndexPath(row: createTaskFieldArray.index(of: .address)!, section: 0)) as? CreateTaskCell {
            guard cell.textViewCell.text.length == 0 else {
                return
            }
            cell.textViewCell.text = address
            self.address = address
            print(address)
            self.pickupLocation = currentLocation
            self.latitude = "\(currentLocation.coordinate.latitude)"
            self.longitude = "\(currentLocation.coordinate.longitude)"
            cell.tranlatePlaceholderLabel(status: true)
        }
    }
    
    func gotoAddressController(_ textview: UITextView) {
        selectedField = textview
        let addressController = AddressController()
        addressController.view.frame = self.view.frame
        addressController.delegate = self
        addressController.createTaskAddressView = textview
        self.present(addressController, animated: true, completion: nil)
    }
    
    func showTemplates(_ textView: UITextView) {
        self.view.endEditing(true)
        selectedField = textView
        guard self.templateArray.count > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
            return
        }
        drowDownWithSearch = TemplateController()
        drowDownWithSearch.itemArray = self.templateArray
        drowDownWithSearch.placeholderValue = TEXT.TEMPLATE
        drowDownWithSearch.delegate = self
        drowDownWithSearch.modalPresentationStyle = .overCurrentContext
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.present(drowDownWithSearch, animated: false, completion: nil)
        
    }
    
    func setCurrentIndex(_ index:Int) {
        self.currentTextField = index
    }
}

//MARK: CustomDatePicker Delegate Methods
extension AddDeliveryPoint: CustomPickerDelegate {
    func dismissDatePicker() {
        self.updateValueForDatePicker("",customPickerTag: customPicker.tag)
    }
    
    func selectedDateFromPicker(_ selectedDate: String) {
        self.view.endEditing(true)
        Analytics.logEvent(ANALYTICS_KEY.CUSTOM_FIELD_DATE, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.CUSTOM_FIELD_DATE, customAttributes: [:])
        self.view.endEditing(true)
        self.updateValueForDatePicker(selectedDate, customPickerTag: customPicker.tag)
    }
    
    func updateValueForDatePicker(_ updatedDate:String, customPickerTag:Int) {
        if let cell = self.tableViewToCreateNewOrder.cellForRow(at: IndexPath(row: customPickerTag, section: 0)) as? CreateTaskCell {
            if updatedDate.length > 0 {
                cell.tranlatePlaceholderLabel(status: true)
            } else {
                cell.tranlatePlaceholderLabel(status: false)
            }
            cell.textViewCell.text! = updatedDate
            if customPickerTag == createTaskFieldArray.index(of: .date)!{
                self.date = updatedDate
            }
            else{
                self.endDate = updatedDate
            }
        }
        customPicker = nil
    }
}

//MARK: AddressDelegate Methods
extension AddDeliveryPoint: AddressDelegate {
    func setLocation(_ location: CLLocation, address: String) {
        if let cell = self.tableViewToCreateNewOrder.cellForRow(at: IndexPath(row: createTaskFieldArray.index(of: .address)!, section: 0)) as? CreateTaskCell {
            cell.tranlatePlaceholderLabel(status: true)
            cell.textViewCell.text = address
            self.pickupLocation = location
            self.address = address
            print(address)
            latitude = "\(location.coordinate.latitude)"
            longitude = "\(location.coordinate.longitude)"
        }
    }
}

//MARK: TemplateControllerDelegate Methods
extension AddDeliveryPoint:TemplateControllerDelegate {
    func selectedValue(value: String, tag:Int, isDirectDismiss: Bool) {
        if isDirectDismiss == false {
            self.template = value
            if let cell = self.tableViewToCreateNewOrder.cellForRow(at: IndexPath(row: createTaskFieldArray.index(of: .template)!, section: 0)) as? CreateTaskCell {
                cell.textViewCell.text = value
                cell.tranlatePlaceholderLabel(status: true)
                self.tableViewToCreateNewOrder.reloadRows(at: [IndexPath(row: createTaskFieldArray.index(of: .template)!, section: 0)], with: UITableViewRowAnimation.automatic)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.drowDownWithSearch = nil

    }
    
//    func closeButtonTapped(){
//        self.template = ""
//        if let cell = self.tableViewToCreateNewOrder.cellForRow(at: IndexPath(row: createTaskFieldArray.index(of: .template)!, section: 0)) as? CreateTaskCell {
//            cell.textViewCell.text = ""
//            cell.tranlatePlaceholderLabel(status: false)
//        }
//    }
//    
//    func dismissTemplateController(){
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//        self.drowDownWithSearch = nil
//    }
}


//MARK: Navigation Bar
extension AddDeliveryPoint:NavigationDelegate {
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = "\(TEXT.NEW) \(TEXT.ORDER)"
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "close_profile").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        navigation.titleLabel.transform = CGAffineTransform(translationX: SCREEN_SIZE.width, y: 0)
        navigation.editButton.isHidden = true
        navigation.editButtonTrailingConstraint.constant = 15.0
        navigation.editButton.setTitle("", for: UIControlState.normal)
        navigation.editButton.setTitleColor(UIColor.white, for: .normal)
        navigation.editButton.addTarget(self, action: #selector(self.editAction), for: UIControlEvents.touchUpInside)
        navigation.editButton.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
        self.view.addSubview(navigation)
        
        /*===============   Setting header   ========================*/
        
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
        case LAYOUT_TYPE.pickupAndDelivery:
            if indicationOfType == 1 {
                self.navigation.titleLabel.text = TEXT.ADD_A_DELIVERY_POINT
            }
            else{
                self.navigation.titleLabel.text = TEXT.ADD_A_PICKUP_POINT
            }
            break
        case LAYOUT_TYPE.appointment:
            self.navigation.titleLabel.text = TEXT.APPOINTMENT
        default:
            self.navigation.titleLabel.text = TEXT.FIELD_WORKFORCE
            break
        }
        
    }
    
    func backAction() {
        let alert = UIAlertController(title: "", message: ERROR_MESSAGE.ARE_YOU_SURE, preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: TEXT.YES, style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            DispatchQueue.main.async {
                self.dismissController()
            }
        }
        
        let noAction = UIAlertAction(title: TEXT.NO, style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func dismissController() {
        UIView.animate(withDuration: 0.35, animations: {
            self.tableViewToCreateNewOrder.transform = CGAffineTransform(translationX: 0, y: self.tableViewToCreateNewOrder.frame.height)
            self.saveDataOutlet.transform = CGAffineTransform(translationX: 0, y: self.saveDataOutlet.frame.height)
            self.navigation.titleLabel.transform = CGAffineTransform(translationX: SCREEN_SIZE.width, y: 0)
            
        }, completion: { finished in
            _ = self.navigationController?.popViewController(animated: false)
            self.callBack!("Dismiss")
        })
    }
    
    func editAction() {
        if selectedTaskIndex != -1 {
            let alertController = UIAlertController(title: title, message: ERROR_MESSAGE.DELETE_CURRENT_TASK, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: TEXT.OK, style: .default, handler: { (action) in
                DispatchQueue.main.async {
                    Singleton.sharedInstance.createTaskArray.remove(at: self.selectedTaskIndex)
                    UIView.animate(withDuration: 0.35, animations: {
                        self.tableViewToCreateNewOrder.transform = CGAffineTransform(translationX: 0, y: self.tableViewToCreateNewOrder.frame.height)
                        self.saveDataOutlet.transform = CGAffineTransform(translationX: 0, y: self.saveDataOutlet.frame.height)
                        self.navigation.titleLabel.transform = CGAffineTransform(translationX: SCREEN_SIZE.width, y: 0)
                    }, completion: { finished in
                        _ = self.navigationController?.popViewController(animated: false)
                        self.callBack!("Dismiss")
                    })
                }
            })
            let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: .cancel, handler: { (action) in
                alertController.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
        }
    }
}
