//
//  TaskTableController.swift
//  Tookan
//
//  Created by Rakesh Kumar on 4/4/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import Crashlytics
import FirebaseAnalytics

class TaskTableController: UIViewController, NavigationDelegate, UITableViewDelegate, UITableViewDataSource, TaskTableButtonDelegate, UICollectionViewDataSource, UICollectionViewDelegate,NoInternetConnectionDelegate, UITextFieldDelegate {
    var appOptionalField:Int!
    var taskStatus:Bool!
    var jobStatus:Int!
   // var selectedTaskDetails:TasksAssignedToDate!
   // var selectedJobDetails:CustomFields!
    var selectedIndex:Int!
    var jobId:Int!
    var firstTimeRun = false
    let rowHeight:CGFloat = 60
    var rowHeightForFooter:CGFloat = 48
    var rowHeightWithImage:CGFloat = 90
    var internetToast:NoInternetConnectionView!
    let unsyncedObjectContext = DatabaseManager.sharedInstance.managedObjectContext
    var navigation:navigationBar!
    var madeChanges = false
    @IBOutlet weak var taskTable: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    struct hiddenStatus {
        static let hidden = 1
        static let show = 0
    }
    
    struct dataType {
        static let text = "text"
        static let location = "location"
        static let number = "number"
        static let phone = "phone"
        static let status = "status"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        self.setNavigationBar()
        self.edgesForExtendedLayout = UIRectEdge()
        self.extendedLayoutIncludesOpaqueBars = false
        self.automaticallyAdjustsScrollViewInsets = false
        /*------------- Tap Gesture -----------*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        if(firstTimeRun == false) {
            firstTimeRun = true
            taskTable.delegate = self
            taskTable.dataSource = self
            
            self.taskTable.tableFooterView = UIView(frame: CGRect.zero)
            self.taskTable.register(UINib(nibName: "TaskTableCell", bundle: nil), forCellReuseIdentifier: "TaskTableCell")
            activityIndicator.stopAnimating()
            self.taskTable.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            self.taskTable.contentInset = UIEdgeInsets.init(top: -32, left: 0, bottom: 0, right: 0)
            if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body.count == 0) {
                self.tableBackgroundView()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: UIStatusBarStyle
    override var preferredStatusBarStyle:UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    //MARK: Navigation Bar
    func setNavigationBar() {
        navigation = UINib(nibName: "navigationBar", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.backgroundColor = UIColor(red: 9/255, green: 41/255, blue: 58/255, alpha: 1.0)
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = "Task".localized + ": \(jobId!)"
        navigation.backButton.tintColor = UIColor.white
        if(self.needToDisplayEditButton() == true) {
            navigation.editButton.isHidden = false
            navigation.editButton.setImage(#imageLiteral(resourceName: "edit"), for: UIControlState())
            navigation.editButton.setImage(#imageLiteral(resourceName: "doneSmall"), for: UIControlState.selected)
            navigation.editButton.setTitle("", for: UIControlState())
            navigation.editButton.addTarget(self, action: #selector(TaskTableController.editAction(_:)), for: UIControlEvents.touchUpInside)
            navigation.editButton.contentHorizontalAlignment = .center
            navigation.backButton.setImage(UIImage(named: "close"), for: UIControlState.selected)
        }
        self.view.addSubview(navigation)
    }
    
    //MARK: NavigationDelegate Methods
    func backAction() {
        if(navigation.backButton.isSelected == false) {
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.TABLE_BACK, parameters: [:])
            self.dismiss(animated: true, completion: nil)
            //_ = self.navigationController?.popViewController(animated: true)
        } else {
            self.view.endEditing(true)
            navigation.backButton.isSelected = false
            navigation.editButton.isSelected = false
            self.resetValue()
            taskTable.reloadData()
        }
    }
    
    func editAction(_ sender:UIButton) {
        if(sender.isSelected == false) {
            sender.isSelected = !sender.isSelected
            navigation.backButton.isSelected = true
             self.taskTable.reloadData()
        } else {
            self.view.endEditing(true)
            if(madeChanges == true) {
                if(checkForMandatoryField() == true) {
                    sender.isSelected = !sender.isSelected
                    madeChanges = false
                    navigation.backButton.isSelected = false
                    self.setBodyValue()
                    self.taskTable.reloadData()
                    self.sendUpdatedBodyToServer({ (succeeded) in
                    })
                }
            } else {
                sender.isSelected = !sender.isSelected
                navigation.backButton.isSelected = false
                self.taskTable.reloadData()
            }
        }
    }
    
    func resetValue() {
        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.removeAllObjects()
        
        for i in (0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body.count) {
            let tempHeadArray = NSMutableArray()
            for j in (0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.typeArray.count) {
                if(j < Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.indexOfStatusType! || Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.indexOfStatusType == -1) {
                    let valArray = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body[i] as! NSArray
                    let valDictionary = valArray[j] as! [String:Any]
                    tempHeadArray.add(valDictionary["val"]!)
                } else if(j > Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.indexOfStatusType!) {
                        let valArray = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body[i] as! NSArray
                        let valDictionary = valArray[j - 1] as! [String:Any]
                        tempHeadArray.add(valDictionary["val"]!)
                }
            }
            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.add(tempHeadArray)
        }
    }
    
    func checkForMandatoryField() -> Bool {
        for section in (0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.count) {
            let details = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: section) as! NSMutableArray
            for row in (0..<details.count) {
                let dictionary = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.head.object(at: row) as! NSDictionary
                let inputText:String!
                if let inputValue = details.object(at: row) as? String{
                    inputText = "\(inputValue)"
                } else if let inputValue = details.object(at: row) as? NSNumber{
                    inputText = "\(inputValue)"
                } else {
                    inputText = ""
                }
                if let getType = dictionary.value(forKey: "type") as? String {
                    switch(getType) {
                    case dataType.text, dataType.number:
                        var required:String!
                        if let requiredValue = dictionary.value(forKey: "required") {
                           required = "\(requiredValue)"
                        } else {
                            required = "0"
                        }
                        
                        if(required == "1" && inputText.length == 0) {
                            if let cell = taskTable.cellForRow(at: IndexPath(row: row, section: section)) as? TaskTableCell {
                                cell.underline.backgroundColor = UIColor.red
                                cell.subTitleField.becomeFirstResponder()
                            }
                            Singleton.sharedInstance.showErrorMessage(error: "Please enter".localized + " \(dictionary.value(forKey: "label") as! String)", isError: .error)
                            return false
                        }
                        break
                        
                    case dataType.phone:
                        var required:String!
                        if let requiredValue = dictionary.value(forKey: "required") {
                            required = "\(requiredValue)"
                        } else {
                            required = "0"
                        }
                        
                        if(required == "1" && inputText.length < 6) {
                            if let cell = taskTable.cellForRow(at: IndexPath(row: row, section: section)) as? TaskTableCell {
                                cell.underline.backgroundColor = UIColor.red
                                cell.subTitleField.becomeFirstResponder()
                            }
                            Singleton.sharedInstance.showErrorMessage(error: "Please enter".localized + " \(dictionary.value(forKey: "label") as! String)", isError: .error)
                            return false
                        }
                        break
                        
                    case dataType.status, dataType.location:
                        break
                        
                    default:
                        break
                    }
                }
            }
        }
        return true
    }
    
    func setBodyValue() {
        for i in (0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body.count) {
            for j in (0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.typeArray.count) {
                if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.typeArray.object(at: j) as! String != "status") {
                    if(j < Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.indexOfStatusType! || Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.indexOfStatusType == -1) {
                        let updatedValue = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: i) as AnyObject).object(at: j)
                        var body:[Any] = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body
                        var valArray:[[String:Any]] = body[i] as! [[String:Any]]
                        var valDictionary = valArray[j]
                        valDictionary["val"] = updatedValue
                        valArray[j] = valDictionary
                        body[i] = valArray
                        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body = body
                    } else if(j > Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.indexOfStatusType!){
                        let updatedValue = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: i) as AnyObject).object(at: j - 1)
                        var body:[Any] = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body
                        var valArray:[[String:Any]] = body[i] as! [[String:Any]]
                        var valDictionary = valArray[j]
                        valDictionary["val"] = updatedValue
                        valArray[j] = valDictionary
                        body[i] = valArray
                        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body = body
                    }
                }
            }
        }
    }
    
    
    //MARK: UITableViewDelegae And UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.headerArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body.count > 0) {
            taskTable.backgroundView = nil
        }
        return Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if let getType = self.selectedJobDetails.taskTableData.typeArray.objectAtIndex(indexPath.row) as? String {
//            switch(getType) {
//            case dataType.image:
//                return rowHeightWithImage
//            default:
//                return rowHeight
//            }
//        }
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskTableCell") as! TaskTableCell
       
        cell.subTitleField.text = ""
        cell.actionButton.isHidden = true
        cell.tableCollectionView.isHidden = true
        cell.subTitleField.delegate = self
         cell.subTitleField.keyboardType = UIKeyboardType.default
        cell.subTitleField.tag = ((indexPath as NSIndexPath).section * 1000) + (indexPath as NSIndexPath).row
        var appSide:String!
        if let value = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.head.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "app_side") {
            appSide = "\(value)"
        } else {
            appSide = "0"
        }
        if(navigation.editButton.isSelected == true) {
            if(appSide == "1") {
                cell.underline.isHidden = false
            } else {
                cell.underline.isHidden = true
            }
        } else {
            cell.underline.isHidden = true
        }
        
        var required:String!
        if let value = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.head.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "required") {
            required = "\(value)"
        } else {
            required = "0"
        }
        
        cell.titleLabel.attributedText = self.addMandatoryFieldStar((Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.headerArray.object(at: (indexPath as NSIndexPath).row) as? String)!, required: required)
        if let getType = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.typeArray.object(at: (indexPath as NSIndexPath).row) as? String {
            switch(getType) {
            case dataType.text:
                if let detail = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String {
                    cell.subTitleField.text = detail
                } else {
                    cell.subTitleField.text = ""
                }
                break
           
            case dataType.number:
                cell.subTitleField.keyboardType = UIKeyboardType.decimalPad
                if let detail = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(at: (indexPath as NSIndexPath).row) as? NSNumber {
                    cell.subTitleField.text = "\(detail)"
                } else if let detail = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String {
                    cell.subTitleField.text = "\(detail)"
                }
                else {
                    cell.subTitleField.text = ""
                }
                break
           
            case dataType.phone:
                cell.subTitleField.keyboardType = UIKeyboardType.numberPad
                if let detail = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String {
                    cell.subTitleField.text = "\(detail)"
                } else {
                    cell.subTitleField.text = ""
                }
                
                cell.actionButton.tag = (1000 * (indexPath as NSIndexPath).section) + (indexPath as NSIndexPath).row
                cell.actionButton.removeTarget(self, action: #selector(TaskTableController.navigationButtonAction(_:)), for: UIControlEvents.touchUpInside)
                cell.actionButton.addTarget(self, action: #selector(TaskTableController.openPhone(_:)), for: UIControlEvents.touchUpInside)
                cell.actionButton.setImage(UIImage(named: "call"), for: UIControlState())
                cell.actionButton.isHidden = false
                break
            
            case dataType.status:
                break
           
            case dataType.location:
                if let detail =  (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(at: (indexPath as NSIndexPath).row) as? [String:Any] {
                    if let detailData = detail["add"] as? String{
                        cell.subTitleField.text = "\(detailData)"
                    } else {
                        cell.subTitleField.text = ""
                    }
                } else {
                    cell.subTitleField.text = ""
                }
                
                cell.actionButton.tag = (1000 * (indexPath as NSIndexPath).section) + (indexPath as NSIndexPath).row
                cell.actionButton.removeTarget(self, action: #selector(TaskTableController.openPhone(_:)), for: UIControlEvents.touchUpInside)
                cell.actionButton.addTarget(self, action: #selector(TaskTableController.navigationButtonAction(_:)), for: UIControlEvents.touchUpInside)
                cell.actionButton.setImage(UIImage(named: "directions"), for: UIControlState())
                cell.actionButton.isHidden = false
                break
            
//            case dataType.image:
//                cell.tableCollectionView.registerNib(UINib(nibName: "ImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionCell")
//                cell.tableCollectionView.delegate = self
//                cell.tableCollectionView.dataSource = self
//                cell.tableCollectionView.hidden = false
//                
//                break
          
            default:
                cell.subTitleField.text = "Unsupported".localized
                break
            }
        }
        
        if(cell.underline.isHidden == false) {
            if(cell.subTitleField.text!.length > 0) {
                cell.underline.backgroundColor = UIColor().blueLineColor
            } else {
                cell.underline.backgroundColor = UIColor.red
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(self.needToDisplayButtonForFooterView(section) == true) {
            rowHeightForFooter = 48
        } else {
            rowHeightForFooter = 5
        }
        return rowHeightForFooter
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UINib(nibName: "TaskTableButton", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TaskTableButton
        footerView.delegate = self
        footerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: rowHeightForFooter)
        
        if(self.needToDisplayButtonForFooterView(section) == true) {
            if let status = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.status.object(at: section) as? NSString {
                if(status.length == 0) {
                    footerView.setButtonStatus(0, index: section, taskStatus: taskStatus)
                } else {
                    footerView.setButtonStatus(status.integerValue, index: section, taskStatus: taskStatus)
                }
                footerView.firstButton.isHidden = false
                footerView.secondButton.isHidden = false
            }
        } else {
            footerView.firstButton.isHidden = true
            footerView.secondButton.isHidden = true
        }
        return footerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func needToDisplayButtonForFooterView(_ index:Int) -> Bool {
        if(navigation.editButton.isSelected == true) {
            return false
        }
        
        if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.status.count > 0 && self.taskStatus == false) {
            if(self.jobStatus == JOB_STATUS.started && appOptionalField == 0 && (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].appSide == APP_SIDE.write || Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].appSide == APP_SIDE.tableEdit)) {
                return true
            } else if(self.jobStatus == JOB_STATUS.arrived && (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].appSide == APP_SIDE.write || Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].appSide == APP_SIDE.tableEdit)) {
                return true
            } else {
                return false
            }
        } else if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.status.count > 0 && self.taskStatus == true){
            if let status = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.status.object(at: index) as? NSString {
                if(status.length == 0 || status.integerValue == 0) {
                    return false
                } else {
                    return true
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func needToDisplayEditButton() -> Bool {
        if(self.taskStatus == false) {
            if(self.jobStatus == JOB_STATUS.started && Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].appSide == APP_SIDE.tableEdit) { //2
                return true
            } else if(self.jobStatus == JOB_STATUS.arrived && Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].appSide == APP_SIDE.tableEdit) {//2
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    
    
    func tableBackgroundView() {
        taskTable.backgroundView = nil
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: taskTable.frame.width, height: taskTable.frame.height)
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0, width: taskTable.frame.width, height: taskTable.frame.height)
        imageView.image = UIImage(named: "no_data")
        imageView.contentMode = UIViewContentMode.center
        imageView.backgroundColor = UIColor.white
        view.addSubview(imageView)
        taskTable.backgroundView = view
    }
    
    //MARK: UICollection View Delegate...
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 0
//        if(self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count < 4) {
//            //if(selectedTaskDetails.jobStatus != JOB_STATUS.complete) {
//            if(self.selectedTaskDetails.jobCompleteStatus != JOB_COMPLETE_STATUS.complete) {
//                return self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count + 1
//            } else {
//                return self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count
//            }
//        } else {
//            return 4
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width:CGFloat = collectionView.frame.width/5
        let height = collectionView.frame.width/5
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
//        var limit = 0
//        if(self.selectedTaskDetails.jobCompleteStatus != JOB_COMPLETE_STATUS.complete) {
//            limit = 3
//        } else {
//            limit = 4
//        }
//        if(indexPath.item < self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count && indexPath.item < limit) {
//            print(self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.objectAtIndex(indexPath.item))
//            if(self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.objectAtIndex(indexPath.item).isKindOfClass(UIImage.classForCoder()) == true) {
//                cell.imageView.image = self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.objectAtIndex(indexPath.item) as? UIImage
//            } else {
//                cell.imageView.image = downloadImageAsynchronously(self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.objectAtIndex(indexPath.item) as! NSString, imageView: cell.imageView, placeHolderImage: UIImage(named: "imagePlaceholder")!, contentMode: UIViewContentMode.ScaleAspectFill)
//            }
//            
//            cell.imageView.contentMode = UIViewContentMode.ScaleAspectFill
//            if(indexPath.item == limit - 1 && self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count > limit) {
//                cell.countLabel.hidden = false
//                cell.countLabel.text = "+ \(self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count - limit)"
//            } else {
//                cell.countLabel.hidden = true
//            }
//        } else {
//            cell.imageView.image = UIImage(named: "custom_image_placeholder")
//            cell.imageView.contentMode = UIViewContentMode.Center
//            cell.imageView.tag = collectionView.tag
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //  if(selectedTaskDetails.jobStatus != JOB_STATUS.complete) {
//        if(self.selectedTaskDetails.jobCompleteStatus != JOB_COMPLETE_STATUS.complete) {
//            if(indexPath.item == self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count || indexPath.item == 3) {
//                self.addImageActionForCustomField(collectionView.tag)
//            } else {
//                self.showImagesPreview(indexPath.item, collectionTag: collectionView.tag)
//            }
//        } else {
//            if(self.selectedTaskDetails.customFieldArray![collectionView.tag].imageArray.count > 0) {
//                self.showImagesPreview(indexPath.item, collectionTag: collectionView.tag)
//            }
//        }
    }


    //MARK: Call Button Action
    func openPhone(_ sender: UIButton) {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.TABLE_CALL, parameters: [:])
        Answers.logCustomEvent(withName: "Task Table Open Phone", customAttributes: [:])
        if let phoneNumber = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: sender.tag / 1000) as AnyObject).object(at: sender.tag % 1000) as? String{
            let phone = "tel://\(phoneNumber.replacingOccurrences(of: " ", with: ""))"
            let url:URL = URL(string:phone)!
            UIApplication.shared.openURL(url)
        }else{
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.CALLING_NOT_AVAILABLE, isError: .error)
        }
    }
    
    //MARK: Go to Navigation Screen
    func navigationButtonAction(_ sender:UIButton) {
        /*------------- Firebase ---------------*/
//        FIRAnalytics.logEventWithName(ANALYTICS_KEY.TABLE_NAVIGATION, parameters: [:])
//        Answers.logCustomEventWithName("Task Table Navigation", customAttributes: [:])
        if let locationValue = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: sender.tag / 1000) as AnyObject).object(at: sender.tag % 1000) as? [String:Any] {
          //  let mapNavigationVC = storyboard?.instantiateViewControllerWithIdentifier("mapNavigationVC") as? MapNavigationViewController
            var latitudeValue:Double = 0.00
            var longitudeValue:Double = 0.00
            if let lat = locationValue["lat"] as? NSNumber {
                latitudeValue = Double(lat)
            }
            
            if let lng = locationValue["lng"] as? NSNumber {
                longitudeValue = Double(lng)
            }
            
//            if let detailData = locationValue.valueForKey("add") as? String{
//                mapNavigationVC!.address = "\(detailData)"
//            } else {
//                mapNavigationVC!.address = ""
//            }
            
            var directionMode = "driving"
            switch (Singleton.sharedInstance.fleetDetails.transportType)! {
            case "1", "2", "4", "6":
                directionMode = "driving"
                break
            case "3":
                directionMode = "bicycling"
                break
            case  "5":
                directionMode = "walking"
                break
            default:
                directionMode = "driving"
                break
            }

            
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_START, parameters: [:])
            Answers.logCustomEvent(withName: "Map Start Navigation", customAttributes: [:])
            let address:String!
            var appName = ""
            let destinationCoordinate = CLLocationCoordinate2D(latitude: latitudeValue, longitude: longitudeValue)
            var originCoordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
            if(LocationTracker.sharedInstance().myLocation != nil) {
                originCoordinate = LocationTracker.sharedInstance().myLocation.coordinate
            }
            if let navigationMap = UserDefaults.standard.value(forKey: USER_DEFAULT.navigationMap) as? Int{
                if(navigationMap == NAVIGATION_MAP.googleMap) {
                    address = "comgooglemaps://?daddr=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)&directionsmode=\(directionMode)"
                    appName = "Google Maps"
                } else {
                    address = "waze://?ll=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)&navigate=yes"
                    appName = "Waze"
                }
            } else {
                address = "comgooglemaps://?daddr=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)&directionsmode=\(directionMode)"
                appName = "Google Maps"
            }
            
            if(UIApplication.shared.canOpenURL(URL(string: address)!)) {
                UIApplication.shared.openURL(URL(string: address)!)
            }else{
                let alertController = UIAlertController(title: "", message: appName + " app is not installed on your Phone. Continue navigation with default Navigation App?".localized, preferredStyle: UIAlertControllerStyle.alert)
                let continueAction = UIAlertAction(title: "Continue".localized, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                    let addr = "http://maps.google.com/maps?saddr=\(originCoordinate.latitude),\(originCoordinate.longitude)&daddr=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)&mode=\(directionMode)"
                    UIApplication.shared.openURL(URL(string: addr)!)
                })
                
                let cancelAction = UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.default, handler: nil)
                alertController.addAction(continueAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }

            
            
//            mapNavigationVC?.destinationCoordinate = CLLocationCoordinate2D(latitude: latitudeValue, longitude: longitudeValue)
//            mapNavigationVC?.selectedJobId = self.jobId
//            navigationController?.pushViewController(mapNavigationVC!, animated: true)
        }
    }
    
    //MARK: TaskTableButtonDelegate Methods
    internal func createRequestAndTableData(_ rowId: Int, statusValue: Int, responseOfRequest: @escaping (Bool, Int) -> ()) {
        if(checkForMandatoryField() == true) {
            /*------------- Firebase ---------------*/
            Analytics.logEvent(ANALYTICS_KEY.TABLE_STATUS, parameters: ["Key":"\(statusValue)" as NSObject])
            Answers.logCustomEvent(withName: "Task Table Status Action", customAttributes: ["Key":"\(statusValue)"])
            if IJReachability.isConnectedToNetwork() == true {
                var lat:String = ""
                var lng:String = ""
                /*======= SET MODE ==========*/
                switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
                case BATTERY_USAGE.foreground:
                    if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                        lat = "\(location.coordinate.latitude)"
                        lng = "\(location.coordinate.longitude)"
                    }
                    break
                default:
                    break
                }
                /*==========================================*/
                let label = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].label
                let col = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.indexOfStatusType!
                let param = [
                    "custom_field_label" : label,
                    "job_id"            : self.jobId,
                    "data"              : "\(statusValue)",
                    "access_token"      : UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
                    "row":rowId,
                    "col":col,
                    "lat":lat,
                    "lng":lng
                    ] as [String : Any]
                print(param)
                NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.updateTableCustomField, params: param as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                    DispatchQueue.main.async {
                        if isSucceeded == true {
                            Singleton.sharedInstance.selectedTaskDetails.customFieldArray![self.selectedIndex].taskTableData.status.replaceObject(at: rowId, with: "\(statusValue)")
                            _ = DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: self.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 1)
                            responseOfRequest(true, statusValue)
                        } else {
                            Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                            responseOfRequest(false, (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![self.selectedIndex].taskTableData.status.object(at: rowId) as! NSString).integerValue)
                        }
                    }
                })
                
            } else {
                if(Auxillary.isAppSyncingEnable() == true) {
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.status.replaceObject(at: rowId, with: "\(statusValue)")
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.timeStampArray.replaceObject(at: rowId, with: Auxillary.getLocalDateString())
                    
                    Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[selectedIndex].taskTableData.syncStatusArray[rowId] = "0"
                    
                   // Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.syncStatusArray.replaceObject(at: rowId, with: "0")
                    if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: self.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                        responseOfRequest(true, statusValue)
                    } else {
                        if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                            responseOfRequest(true, statusValue)
                        } else {
                            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .message)
                            responseOfRequest(false, (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.status.object(at: rowId) as! NSString).integerValue)
                        }
                    }
                } else {
                    self.showInternetToast()
                    responseOfRequest(false, (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.status.object(at: rowId) as! NSString).integerValue)
                }
            }
        } else {
            responseOfRequest(false, (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.status.object(at: rowId) as! NSString).integerValue)
        }

    }
    
//    
//    internal func createRequestAndTableData(_ rowId: Int, statusValue: Int, responseOfRequest: @escaping (_ succeeded: Bool, _ status:Int) -> ()) {
//            }
    
    //MARK: NoInternetConnectionView
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: "NoInternetConnectionView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:HEIGHT.navigationHeight, width: self.view.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.view.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(navigation.editButton.isSelected == true) {
            var appSide:String!
            if let value = (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.head.object(at: textField.tag % 1000) as AnyObject).value(forKey: "app_side") {
                appSide = "\(value)"
            } else {
                appSide = "0"
            }
            if(appSide == "1") {
                return true
            }
        }
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.text?.trimText.length == 0) {
            if let cell = taskTable.cellForRow(at: IndexPath(row: textField.tag % 1000, section: textField.tag / 1000)) as? TaskTableCell {
                cell.underline.backgroundColor = UIColor.red
            }
        }
        (Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.detailArray.object(at: textField.tag / 1000) as AnyObject).replaceObject(at: textField.tag % 1000, with: textField.text!.trimText)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        madeChanges = true
        if let cell = taskTable.cellForRow(at: IndexPath(row: textField.tag % 1000, section: textField.tag / 1000)) as? TaskTableCell {
            let updatedText = textField.text!
            var updatedString:NSString = "\(updatedText)" as NSString
            updatedString = updatedString.replacingCharacters(in: range, with: string) as NSString
            if(updatedString.length > 0) {
                cell.underline.backgroundColor = UIColor().blueLineColor
            } else {
                cell.underline.backgroundColor = UIColor.red
            }
        }
        return true
    }
    
    
    //MARK: Keyboard Functions
    func keyboardWillShow(_ notification : Foundation.Notification){
//        let info = notification.userInfo
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue //info.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let duration = (notification as NSNotification).userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = (notification as NSNotification).userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        Singleton.sharedInstance.keyboardSize = value.cgRectValue.size
        let keyboardSize: CGSize = value.cgRectValue.size
        bottomConstraint.constant = keyboardSize.height + 20
        self.view.setNeedsUpdateConstraints()
        
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { () -> Void in
            self.view.layoutIfNeeded()
            }, completion: { void in
        })
        Singleton.sharedInstance.translateErrorMessage(toBottom: false)
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        let duration = (notification as NSNotification).userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = (notification as NSNotification).userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        Singleton.sharedInstance.keyboardSize = CGSize(width: 0.0, height: 0.0)
        bottomConstraint.constant = 0
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: { () -> Void in
            self.view.layoutIfNeeded()
            }, completion: nil)
        Singleton.sharedInstance.translateErrorMessage(toBottom: true)
    }

    //MARK: Dismiss Keypad
    func backgroundTouch() {
        self.view.endEditing(true)
    }
    
    func sendUpdatedBodyToServer(_ responseOfRequest: (_ succeeded: Bool) -> ()) {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.TABLE_EDIT, parameters: [:])
        Answers.logCustomEvent(withName: "Task Table Edit Action", customAttributes: [:])
        if IJReachability.isConnectedToNetwork() == true {
            var lat:String = ""
            var lng:String = ""
            /*======= SET MODE ==========*/
            switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
            case BATTERY_USAGE.foreground:
                if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                    lat = "\(location.coordinate.latitude)"
                    lng = "\(location.coordinate.longitude)"
                }
                break
            default:
                break
            }
            /*==========================================*/
            let label = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].label
            let data = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.body.jsonString
            let param = [
                "custom_field_label" : label,
                "job_id"            : self.jobId,
                "data"              : data,
                "access_token"      : UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
                "flag":1,
                "lat":lat,
                "lng":lng
                ] as [String : Any]
            print(param)
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.updateTableCustomField, params: param as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    if isSucceeded == false {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                }
            })
        } else {
            if(Auxillary.isAppSyncingEnable() == true) {
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.bodySyncTimeStamp = Auxillary.getLocalDateString()
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![selectedIndex].taskTableData.bodySyncStatus = 0
                if(DatabaseManager.sharedInstance.updateAssignedTaskOnDatabase(self.unsyncedObjectContext, jobId: self.jobId, taskDetails: Singleton.sharedInstance.selectedTaskDetails, syncStatus: 0) == true) {
                    responseOfRequest(true)
                } else {
                    if(DatabaseManager.sharedInstance.saveAssignedTaskToDatabase(Singleton.sharedInstance.selectedTaskDetails, jobId: Singleton.sharedInstance.selectedTaskDetails.jobId, timeStamp: NetworkingHelper.sharedInstance.currentDate, syncStatus: 0, taskManagedContext: self.unsyncedObjectContext) == true) {
                        responseOfRequest(true)
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                        responseOfRequest(false)
                    }
                }
            } else {
                self.showInternetToast()
                responseOfRequest(false)
            }
        }
    }
    
    func addMandatoryFieldStar(_ title:String, required:String) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: title, attributes: [NSFontAttributeName:UIFont.init(name: UIFont().MontserratMedium, size: 12)!])
        let star = NSMutableAttributedString(string: " *", attributes: [NSForegroundColorAttributeName:UIColor.red])
        if(required == "1") {
            attributedString.append(star)
        }
        return attributedString
    }
}
