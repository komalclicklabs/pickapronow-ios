//
//  SettingViewController.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 3/21/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import Crashlytics
import FirebaseAnalytics

enum SELECTED_MENU_FOR_DROP_DOWN {
    case Ringtone
    case Navigation
    case Language
    case MapStyle
}

class SettingViewController:UIViewController, NavigationDelegate {

    @IBOutlet weak var tabelView: UITableView!
    @IBOutlet var bgView: UIView!

    var sectionArray = [TEXT.VEHICLE,TEXT.NOTIFICATION,TEXT.ADVANCED]
    var vehicleRowArray = [TEXT.WALKING]
    var notificationRowArray = [TEXT.RINGTONE,TEXT.VIBRATION]
    var advancedRowArray = [TEXT.LANGUAGE,TEXT.NAVIGATION,TEXT.SHOWTRAFFIC,TEXT.POWERSAVINGMODE,TEXT.MAP_STYLE]
    var sectionHeaderHeight:CGFloat = getAspectRatioValue(value: 35)
    var selectedIndexPath : IndexPath?
    var heightOfFooter:CGFloat = 50
    var labelVersion:UILabel!
    var isCellUpdating = false
    let accessToken = Singleton.sharedInstance.getAccessToken()
    var vehicleList = [TEXT.WALKING,TEXT.CYCLE,TEXT.SCOOTER,TEXT.BIKE,TEXT.CAR,TEXT.TRUCK]
    var vehicleIconArray = [#imageLiteral(resourceName: "walkingMan"),#imageLiteral(resourceName: "bike"),#imageLiteral(resourceName: "scooter"),#imageLiteral(resourceName: "motorbike"),#imageLiteral(resourceName: "car"),#imageLiteral(resourceName: "truck")]
    var selectedRingtone : Int = 0
    var selectedNavigation : Int = 0
    var selectedMapStyle : Int = 0
    enum SETTING_SECTION:Int {
        case vehicle
        case notification
        case advanced
    }
    
    var sectionType:SETTING_SECTION!
    var selectedMenu:SELECTED_MENU_FOR_DROP_DOWN!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        
        /*============== Set Table View ===================*/
        self.tabelView.delegate = self
        self.tabelView.dataSource = self
        self.tabelView.register(UINib(nibName: NIB_NAME.settingViewCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.settingViewCell)
        self.tabelView.rowHeight = UITableViewAutomaticDimension
        self.setFooterView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.perform(#selector(self.updateStatusBar), with: nil, afterDelay: 1.0)
        self.bgView.layer.cornerRadius = 15.0
    }
    
    func updateStatusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Navigation Bar
    func setNavigationBar() {
        let navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = TEXT.SETTINGS_UPPER
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        //navigation.bottomLine.isHidden = false
        self.view.addSubview(navigation)
    }
    
    
    //MARK: NavigationDelegate Methods
    func backAction() {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.SETTING_BACK, parameters: [:])
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func setFooterView() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width - 10, height: heightOfFooter))
        let labelVersion = UILabel(frame: CGRect(x: footerView.frame.width - 100 - 18, y: 0, width: 100, height:heightOfFooter))
        labelVersion.textColor = COLOR.LIGHT_COLOR
        labelVersion.textAlignment = .right
        labelVersion.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            labelVersion.text = "\(TEXT.VERSION) \(version)"
            footerView.addSubview(labelVersion)
            self.tabelView.tableFooterView = footerView
        } else {
            self.tabelView.tableFooterView = UIView(frame: CGRect.zero)
        }
    }
    
    //MARK: Call for DropDownOption
    func callForDropDownOptions(dropDownData:[String],isCellIconVisible : Bool,selectedOption : SELECTED_MENU_FOR_DROP_DOWN,heading : String,selection :Int ){
        let settingDropDown = SettingDropDown()
        settingDropDown.delegate = self
        settingDropDown.modalPresentationStyle = .overCurrentContext
        settingDropDown.modalTransitionStyle = .coverVertical
        self.present(settingDropDown, animated: false, completion: { finished in
            settingDropDown.updateDropDown(dropDownData, option: selectedOption, cellIconenabled: isCellIconVisible,headingText:heading,selectedItem: selection)
        })
    }
    
    func languageRowSelected(){
        var tableData:[String] = [String](language.keys) //language.allKeys as! [String]
        let tableValues:[String] = [String](language.values)
        let selectedItem = (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String)! //language[(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String)!] //(language.allKeys(for: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String)!)[0] as? String)!
        print(tableData)
        let index = tableValues.index(of: selectedItem)
        let selectedValue = tableData[index!]
        tableData.remove(at: index!)
        tableData.insert(selectedValue, at: 0)
        callForDropDownOptions(dropDownData: tableData ,isCellIconVisible: true, selectedOption: .Language,heading: TEXT.LANGUAGE,selection: 0)
    }
    func navigationRowSelected(){
        let tabledata = ["Google","Waze", "Apple Map"]
        callForDropDownOptions(dropDownData: tabledata,isCellIconVisible: true,selectedOption: .Navigation,heading: TEXT.NAVIGATION,selection: self.selectedNavigation)
    }
    func mapStyleSelected(){
        let tabledata = [TEXT.DARK_MAP,TEXT.LIGHT_MAP]
        callForDropDownOptions(dropDownData: tabledata,isCellIconVisible: true,selectedOption: .MapStyle,heading: TEXT.MAP_STYLE,selection: self.selectedMapStyle)
    }
    func ringtoneRowSelected(){
        let tabledata = [TEXT.SHORT,TEXT.LONG]
        callForDropDownOptions(dropDownData: tabledata,isCellIconVisible: true,selectedOption: .Ringtone,heading: TEXT.RINGTONE,selection: self.selectedRingtone)
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
                _ = self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: VIBRATION
    func setVibration(_ sender:UISwitch) {
        if sender.isOn == true {
            UserDefaults.standard.setValue(true, forKey: USER_DEFAULT.vibration)
            Answers.logCustomEvent(withName: ANALYTICS_KEY.VIBRATION, customAttributes: ["Status":"Enable"])
            Analytics.logEvent(ANALYTICS_KEY.VIBRATION, parameters: ["Status":"Enable" as NSObject])
        } else {
            UserDefaults.standard.setValue(false, forKey: USER_DEFAULT.vibration)
            Answers.logCustomEvent(withName: ANALYTICS_KEY.VIBRATION, customAttributes: ["Status":"Disable"])
            Analytics.logEvent(ANALYTICS_KEY.VIBRATION, parameters: ["Status":"Disable" as NSObject])
        }
    }
    
    //MARK: TRAFFIC
    func selectTrafficAction(_ sender:UISwitch) {
        if sender.isOn == true {
            Analytics.logEvent(ANALYTICS_KEY.SHOW_TRAFFIC, parameters: ["Status":"ON" as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.SHOW_TRAFFIC, customAttributes: ["Status":"check"])
            UserDefaults.standard.setValue(true, forKey: USER_DEFAULT.trafficLayer)
        } else {
            UserDefaults.standard.setValue(false, forKey: USER_DEFAULT.trafficLayer)
            Answers.logCustomEvent(withName:ANALYTICS_KEY.SHOW_TRAFFIC, customAttributes: ["Status":"Uncheck"])
            Analytics.logEvent(ANALYTICS_KEY.SHOW_TRAFFIC, parameters: ["Status":"OFF" as NSObject])
        }
    }
    
    //MARK: POWER SAVING MODE
    func selectPowerSavingMode(_ sender:UISwitch){
        if sender.isOn == true {
            UserDefaults.standard.setValue(true, forKey: USER_DEFAULT.batterySaver)
            Answers.logCustomEvent(withName: ANALYTICS_KEY.POWER_SAVING_MODE, customAttributes: ["Status":"ON"])
            Analytics.logEvent(ANALYTICS_KEY.POWER_SAVING_MODE, parameters: ["Status":"ON" as NSObject])
        } else {
            UserDefaults.standard.setValue(false, forKey: USER_DEFAULT.batterySaver)
            Answers.logCustomEvent(withName: ANALYTICS_KEY.POWER_SAVING_MODE, customAttributes: ["Status":"Off"])
            Analytics.logEvent(ANALYTICS_KEY.POWER_SAVING_MODE, parameters: ["Status":"Off" as NSObject])
        }
    }
    
}

//MARK: UITableView Delegate And DataSource methods
extension SettingViewController:UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sectionType = SettingViewController.SETTING_SECTION(rawValue: section)
        switch sectionType! {
        case SETTING_SECTION.advanced:
            return self.advancedRowArray.count
        case SETTING_SECTION.notification:
            return self.notificationRowArray.count
        case SETTING_SECTION.vehicle:
            return self.vehicleRowArray.count
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 0.01
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: sectionHeaderHeight))
        headerView.backgroundColor = COLOR.TABLE_HEADER_COLOR
        
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.frame.width, height: sectionHeaderHeight))
        label.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        label.textColor = COLOR.LIGHT_COLOR
        label.textAlignment = .left
        label.text  = self.sectionArray[section]
        label.setLetterSpacing(value: 1.8)
        headerView.addSubview(label)
        
        let topLine = UIView(frame: CGRect(x: 0, y: 0, width: headerView.frame.width, height: 1))
        topLine.backgroundColor = COLOR.LINE_COLOR
        headerView.addSubview(topLine)
        
        let bottomLine = UIView(frame: CGRect(x: 0, y: sectionHeaderHeight - 1, width: headerView.frame.width, height: 1))
        bottomLine.backgroundColor = COLOR.LINE_COLOR
        headerView.addSubview(bottomLine)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.settingViewCell) as! SettingViewCell
        sectionType = SettingViewController.SETTING_SECTION(rawValue: indexPath.section)
        cell.activityIndicator.isHidden = true
        cell.activityIndicator.stopAnimating()
        cell.settingButton.setImage(nil, for: .normal)
        
        switch(sectionType!){
        case SETTING_SECTION.vehicle:
            cell.settingLabel?.text = vehicleList[VehicleViewController().setVehicleType(Singleton.sharedInstance.fleetDetails.transportType!)]
            cell.icon.image = self.vehicleIconArray[VehicleViewController().setVehicleType(Singleton.sharedInstance.fleetDetails.transportType!)]
            cell.icon.isHidden = false
            cell.labelLeadingConstrint.constant = CGFloat(cell.defaultLabelLeadingValue)
            cell.settingButton.isHidden = false
            cell.settingButton.setTitle("", for: .normal)
            cell.settingButton.setImage(#imageLiteral(resourceName: "down_arrow"), for: .normal)
            cell.settingSwitch.isHidden = true
            cell.settingButton.contentMode  = .right
            cell.settingButton.imageView?.contentMode = .scaleAspectFit
            cell.iconLabelLeading.isActive = true
            cell.iconLabelLeading.constant = 15.0
            cell.cellLabelLeading.isActive = false
        case SETTING_SECTION.notification:
            cell.cellLabelLeading.isActive = true
            cell.iconLabelLeading.isActive = false
            cell.settingLabel?.text = self.notificationRowArray[indexPath.row]
            cell.labelLeadingConstrint.constant = CGFloat(cell.labelLeadingValue)
            cell.icon.isHidden = true
            /*==================== Vibration Setting========================*/
            if(indexPath.row == 1){
                cell.settingButton.isHidden = true
                cell.settingSwitch.isHidden = false
                cell.settingSwitch.addTarget(self, action: #selector(self.setVibration(_:)), for: UIControlEvents.valueChanged)
                if let vibrationsEnabled = UserDefaults.standard.value(forKey: USER_DEFAULT.vibration) as? Bool{
                    if vibrationsEnabled == true {
                        cell.settingSwitch.setOn(true, animated: true)
                    } else {
                        cell.settingSwitch.setOn(false, animated: true)
                    }
                }else{
                    cell.settingSwitch.setOn(false, animated: true)
                    UserDefaults.standard.setValue(false, forKey: USER_DEFAULT.vibration)
                }
            }/*==================== Ringtone Setting========================*/
            else{
                cell.settingButton.isHidden = false
                cell.settingSwitch.isHidden = true
                if(Singleton.sharedInstance.fleetDetails.notiTone == NOTIFICATION_TONE.short) {
                    cell.settingButton.setTitle(TEXT.SHORT, for: UIControlState())
                    self.selectedRingtone = 0
                } else {
                    cell.settingButton.setTitle(TEXT.LONG, for: UIControlState())
                    self.selectedRingtone = 1
                }
                if isCellUpdating == true {
                    cell.activityIndicator.isHidden = false
                    cell.activityIndicator.startAnimating()
                    cell.settingButton.isHidden = true
                } else {
                    cell.activityIndicator.stopAnimating()
                    cell.activityIndicator.isHidden = true
                    cell.settingButton.isHidden = false
                }
                
            }
        case SETTING_SECTION.advanced:
            cell.cellLabelLeading.isActive = true
            cell.iconLabelLeading.isActive = false
            cell.settingLabel?.text = self.advancedRowArray[indexPath.row]
            cell.labelLeadingConstrint.constant = CGFloat(cell.labelLeadingValue)
            cell.icon.isHidden = true
            if(indexPath.row>1) {
                cell.settingButton.isHidden = true
                cell.settingSwitch.isHidden = false
            } else {
                cell.settingButton.isHidden = false
                cell.settingSwitch.isHidden = true
            }
            switch indexPath.row {
            case 0:
                var tableData:[String] = [String](language.keys) //language.allKeys as! [String]
                let tableValues:[String] = [String](language.values)
                let selectedItem = (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String)! //language[(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String)!] //(language.allKeys(for: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String)!)[0] as? String)!
                print(tableData)
                let index = tableValues.index(of: selectedItem)
                let selectedValue = tableData[index!]
                
                cell.settingButton.setTitle(selectedValue, for: UIControlState());
            case 1:
                if let navigationMap = UserDefaults.standard.value(forKey: USER_DEFAULT.navigationMap) as? Int{
                    if(navigationMap == NAVIGATION_MAP.googleMap) {
                        Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_SELECTED, parameters:[ANALYTICS_KEY.NAVIGATION_SELECTED : "Google" as NSObject])
                        Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_SELECTED, customAttributes: [ANALYTICS_KEY.NAVIGATION_SELECTED : "Google"])
                        cell.settingButton.setTitle("Google", for: UIControlState())
                        self.selectedNavigation = 0
                    } else if(navigationMap == NAVIGATION_MAP.waze){
                        Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_SELECTED, parameters:[ANALYTICS_KEY.NAVIGATION_SELECTED : "Waze" as NSObject])
                        Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_SELECTED, customAttributes: [ANALYTICS_KEY.NAVIGATION_SELECTED : "Waze"])
                        cell.settingButton.setTitle("Waze", for: UIControlState())
                        self.selectedNavigation = 1
                    } else {
                        Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_SELECTED, parameters:[ANALYTICS_KEY.NAVIGATION_SELECTED : "Apple Map" as NSObject])
                        Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_SELECTED, customAttributes: [ANALYTICS_KEY.NAVIGATION_SELECTED : "Apple Map"])
                        cell.settingButton.setTitle("Apple Map", for: UIControlState())
                        self.selectedNavigation = 2
                    }
                } else {
                    Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_SELECTED, parameters:[ANALYTICS_KEY.NAVIGATION_SELECTED : "Google" as NSObject])
                    Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_SELECTED, customAttributes: [ANALYTICS_KEY.NAVIGATION_SELECTED : "Google"])
                    UserDefaults.standard.setValue(NAVIGATION_MAP.googleMap, forKey: USER_DEFAULT.navigationMap)
                    cell.settingButton.setTitle("Google", for: UIControlState())
                }
            case 2:
                cell.settingSwitch.addTarget(self, action: #selector(self.selectTrafficAction(_:)), for: UIControlEvents.valueChanged)
                if let trafficlayer = UserDefaults.standard.value(forKey: USER_DEFAULT.trafficLayer) as? Bool{
                    if trafficlayer == true {
                        cell.settingSwitch.setOn(true, animated: true)
                    } else {
                        cell.settingSwitch.setOn(false, animated: true)
                    }
                }else{
                    cell.settingSwitch.setOn(false, animated: true)
                    UserDefaults.standard.setValue(false, forKey: USER_DEFAULT.trafficLayer)
                }
            case 3:
                cell.settingSwitch.addTarget(self, action: #selector(self.selectPowerSavingMode(_:)), for: UIControlEvents.valueChanged)
                if let batterySaver = UserDefaults.standard.value(forKey: USER_DEFAULT.batterySaver) as? Bool{
                    if batterySaver == true {
                        cell.settingSwitch.setOn(true, animated: true)
                    } else {
                        cell.settingSwitch.setOn(false, animated: true)
                    }
                }else{
                    cell.settingSwitch.setOn(false, animated: true)
                    UserDefaults.standard.setValue(false, forKey: USER_DEFAULT.batterySaver)
                }
            case 4:
                if let mapStyle = UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int{
                    if(mapStyle == MAP_STYLE.dark) {
                        Analytics.logEvent(ANALYTICS_KEY.MAP_STYLE, parameters:[ANALYTICS_KEY.MAP_STYLE : TEXT.DARK_MAP as NSObject])
                        Answers.logCustomEvent(withName: ANALYTICS_KEY.MAP_STYLE, customAttributes: [ANALYTICS_KEY.MAP_STYLE : TEXT.DARK_MAP])
                        cell.settingButton.setTitle(TEXT.DARK_MAP, for: UIControlState())
                        self.selectedMapStyle = 0
                    } else {
                        Analytics.logEvent(ANALYTICS_KEY.MAP_STYLE, parameters:[ANALYTICS_KEY.MAP_STYLE : TEXT.LIGHT_MAP as NSObject])
                        Answers.logCustomEvent(withName: ANALYTICS_KEY.MAP_STYLE, customAttributes: [ANALYTICS_KEY.MAP_STYLE : TEXT.LIGHT_MAP])
                        cell.settingButton.setTitle(TEXT.LIGHT_MAP, for: UIControlState())
                        self.selectedMapStyle = 1
                    }
                } else {
                    Analytics.logEvent(ANALYTICS_KEY.MAP_STYLE, parameters:[ANALYTICS_KEY.MAP_STYLE : TEXT.DARK_MAP as NSObject])
                    Answers.logCustomEvent(withName: ANALYTICS_KEY.MAP_STYLE, customAttributes: [ANALYTICS_KEY.MAP_STYLE : TEXT.DARK_MAP])
                    UserDefaults.standard.setValue(MAP_STYLE.dark, forKey: USER_DEFAULT.mapStyle)
                    cell.settingButton.setTitle(TEXT.DARK_MAP, for: UIControlState())
                }
                cell.settingButton.isHidden = false
//                cell.settingButton.setTitle(selectedValue, for: UIControlState());
                cell.settingSwitch.isEnabled = false
                cell.settingSwitch.isHidden = true
            default:
                break
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        sectionType = SettingViewController.SETTING_SECTION(rawValue: indexPath.section)
        switch sectionType! {
        case SETTING_SECTION.vehicle:
            let vehicleViewController = VehicleViewController();
            vehicleViewController.modalPresentationStyle = .overCurrentContext
            vehicleViewController.modalTransitionStyle = .coverVertical
            vehicleViewController.delegate = self
            self.present(vehicleViewController, animated: false, completion: nil)
        case SETTING_SECTION.notification:
            switch indexPath.row {
            case 0:
                ringtoneRowSelected()
            default:
                break
            }
        case SETTING_SECTION.advanced:
            switch indexPath.row {
            case 0:
                languageRowSelected()
            case 1:
                Answers.logCustomEvent(withName: ANALYTICS_KEY.VIBRATION, customAttributes: ["Status":"Enable"])
                Analytics.logEvent(ANALYTICS_KEY.VIBRATION, parameters: ["Status":"Enable" as NSObject])
                navigationRowSelected()
            case 4:
                Answers.logCustomEvent(withName: ANALYTICS_KEY.VIBRATION, customAttributes: ["Status":"Enable"])
                Analytics.logEvent(ANALYTICS_KEY.VIBRATION, parameters: ["Status":"Enable" as NSObject])
                mapStyleSelected()
            default:
                break
            }
        }
    }

}

//MARK: SettingDropDown Delegate
extension SettingViewController:SettingDropDownDelegate {
    func changeSettingMenuOption(selectedItem: String, selectedoption: SELECTED_MENU_FOR_DROP_DOWN) {
        switch selectedoption {
        case SELECTED_MENU_FOR_DROP_DOWN.Ringtone:
            let cell = tabelView.cellForRow(at: IndexPath(row: 0, section: 1)) as? SettingViewCell
            cell?.activityIndicator.isHidden = false
            cell?.activityIndicator.startAnimating()
            cell?.settingButton.isHidden = true
            cell?.settingSwitch.isHidden = true
            self.isCellUpdating = true
            var notificationTone:String
            if selectedItem == TEXT.SHORT {
                Answers.logCustomEvent(withName: ANALYTICS_KEY.NOTIFICATION_TONE, customAttributes: ["Status":"Short"])
                Analytics.logEvent(ANALYTICS_KEY.NOTIFICATION_TONE, parameters: ["Status":"Short" as NSObject])
                notificationTone = NOTIFICATION_TONE.short
            } else {
                Answers.logCustomEvent(withName: ANALYTICS_KEY.NOTIFICATION_TONE, customAttributes: ["Status":"Long"])
                Analytics.logEvent(ANALYTICS_KEY.NOTIFICATION_TONE, parameters: ["Status":"Long" as NSObject])
                notificationTone = NOTIFICATION_TONE.long
            }
            
            let fleetId = Singleton.sharedInstance.fleetDetails.fleetId!
            var params:[String : Any] = ["access_token":self.accessToken]
            params["tone"] = notificationTone
            params["fleet_id"] = fleetId
//            let params = ["access_token":self.accessToken,
//                          "tone":notificationTone,
//                          "fleet_id":fleetId] as [String : Any]
            
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.setNotificationTone, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async(execute: { () -> Void in
                    if(isSucceeded == true) {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.showInvalidAccessTokenPopup((response["message"] as? String)!)
                            break
                        default:
                            Singleton.sharedInstance.fleetDetails.notiTone = notificationTone
                        }
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                    self.isCellUpdating = false
                    self.tabelView.reloadRows(at: [self.selectedIndexPath!], with: .automatic)
                })
            })
            
            
        case SELECTED_MENU_FOR_DROP_DOWN.Navigation:
            if selectedItem == "Google" {
                UserDefaults.standard.setValue(NAVIGATION_MAP.googleMap, forKey: USER_DEFAULT.navigationMap)
                Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_SELECTED, customAttributes: ["Map":"Google"])
                Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_SELECTED, parameters: ["Map":"Google" as NSObject])
            } else if selectedItem == "Waze" {
                UserDefaults.standard.setValue(NAVIGATION_MAP.waze, forKey: USER_DEFAULT.navigationMap)
                Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_SELECTED, customAttributes: ["Map":"Waze"])
                Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_SELECTED, parameters: ["Map":"Waze" as NSObject])
            } else {
                UserDefaults.standard.setValue(NAVIGATION_MAP.appleMap, forKey: USER_DEFAULT.navigationMap)
                Answers.logCustomEvent(withName: ANALYTICS_KEY.NAVIGATION_SELECTED, customAttributes: ["Map":"Apple"])
                Analytics.logEvent(ANALYTICS_KEY.NAVIGATION_SELECTED, parameters: ["Map":"Apple" as NSObject])
            }
            
        case SELECTED_MENU_FOR_DROP_DOWN.Language:
            var locale:String!
            locale = language[selectedItem]!
            UserDefaults.standard.setValue(language[selectedItem]!, forKey: USER_DEFAULT.selectedLocale)
            UserDefaults.standard.set([locale!], forKey: USER_DEFAULT.AppleLanguages)
            Answers.logCustomEvent(withName: ANALYTICS_KEY.LANGUAGE_SELECTED, customAttributes: ["Locale":locale])
            Analytics.logEvent(ANALYTICS_KEY.LANGUAGE_SELECTED, parameters: ["LANGUAGE":locale as NSObject])
            Auxillary.resetFilterValue()
            self.perform(#selector(self.updateLanguage), with: nil, afterDelay: 1.0)
            
        case SELECTED_MENU_FOR_DROP_DOWN.MapStyle:
            if selectedItem == TEXT.DARK_MAP {
                UserDefaults.standard.setValue(MAP_STYLE.dark, forKey: USER_DEFAULT.mapStyle)
            } else {
                UserDefaults.standard.setValue(MAP_STYLE.light, forKey: USER_DEFAULT.mapStyle)
            }
        }
        self.tabelView.reloadRows(at: [self.selectedIndexPath!], with: .automatic)
    }
    
    func showErrorMessage() {
        
    }
    
    func updateLanguage() {
        Singleton.sharedInstance.showErrorMessage(error: TEXT.RESTART_APPLICATION, isError: .message)
    }
}
extension SettingViewController : VehicleViewControllerDelegate{
    func vehicleChanged() {
        self.tabelView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
    }
}

