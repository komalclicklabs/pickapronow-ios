//
//  CustomBarCodeCell.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 8/21/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class CustomBarCodeCell: UITableViewCell {

    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var descriptionView: UITextView!
    @IBOutlet var placeholderLabel: UILabel!
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var line: UIView!
    @IBOutlet var actionButtonTrailing: NSLayoutConstraint!
    //var sectionTag: Int!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        /*--------- Header Label -------------*/
        self.headerLabel.textColor = COLOR.LIGHT_COLOR
        self.headerLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        
        /*------------- Placeholder label -----------*/
        self.placeholderLabel.textColor = COLOR.EXTRA_LIGHT_COLOR
        self.placeholderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        
        /*---------- Text View -------------*/
        self.descriptionView.textColor = COLOR.TEXT_COLOR
        self.descriptionView.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        //self.descriptionView.delegate = self
        self.descriptionView.tintColor = COLOR.themeForegroundColor
        
        /*--------- UnderLine ------------*/
        self.line.backgroundColor = COLOR.LIGHT_COLOR
        
        self.actionButton.setImage(#imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate), for: .selected)
        // Configure the view for the selected state
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
}
