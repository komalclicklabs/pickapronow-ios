//
//  AllNotificationsController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/03/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class AllNotificationsController: UIViewController {

    @IBOutlet var notificationTable: UITableView!
    var refreshControl: UIRefreshControl?
    var navigation:navigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        /*================= Blur effect ===================*/
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubview(toBack: blurEffectView)
        /*===============================================*/
        //adding pull to refresh control
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.HandleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.notificationTable.addSubview(refreshControl!)
        
        self.setTableView()
        self.setNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.getAllNotification), name: NSNotification.Name(rawValue: OBSERVER.getAllNotification), object: nil)
        if(NetworkingHelper.sharedInstance.allNotificationDataArray.count == 0 && self.refreshControl!.isRefreshing == false) {
            let loaderView = UINib(nibName: NIB_NAME.loaderView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! LoaderView
            loaderView.frame = self.notificationTable.frame
            loaderView.setLoader()
            self.notificationTable.backgroundView = loaderView
        }
        self.getAllNotification()
        self.perform(#selector(self.updateStatusBar), with: nil, afterDelay: 1.0)
    }
    
    func updateStatusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTableView() {
        self.notificationTable.delegate = self
        self.notificationTable.dataSource = self
        self.notificationTable.register(UINib(nibName: NIB_NAME.jobListCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobListCell)
        //self.notificationTable.estimatedRowHeight = 60
        self.notificationTable.rowHeight = UITableViewAutomaticDimension
        self.notificationTable.layer.cornerRadius = 15.0
        self.notificationTable.layer.masksToBounds = true
    }
    
    func getAllNotification() {
        if IJReachability.isConnectedToNetwork() == false {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            self.dataSetWhenNoDataFound()
        }
        else {
            var params:[String : Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
            params["fleet_id"] = Singleton.sharedInstance.fleetDetails.fleetId!
            params["flag"] = "all"
//            let params = [
//                "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//                "fleet_id":Singleton.sharedInstance.fleetDetails.fleetId!,
//                "flag":"all"
//                ] as [String : Any]
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.viewAllNotification, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    if isSucceeded == true {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            print(response)
                            if let responseData = response["data"] as? NSDictionary {
                                if let serverTime = responseData["server_time"] as? String {
                                    NetworkingHelper.sharedInstance.serverTime = serverTime
                                }
                                if let count = responseData["count"] as? Int {
                                    NetworkingHelper.sharedInstance.notificationCount = count
                                }
                                if let notificationData = responseData["data"] as? NSArray {
                                    NetworkingHelper.sharedInstance.allNotificationDataArray.removeAll(keepingCapacity: false)
                                    for item in notificationData {
                                        NetworkingHelper.sharedInstance.allNotificationDataArray.append(NotificationDataObject(json: item as! [String : Any]))
                                    }
                                    
                                    if(NetworkingHelper.sharedInstance.allNotificationDataArray.count == 0) {
                                        self.dataSetWhenNoDataFound()
                                        self.navigation.editButton.isHidden = true
                                    } else {
                                        self.navigation.editButton.isHidden = false
                                    }
                                    DispatchQueue.main.async(execute: {
                                        self.notificationTable.reloadData()
                                        self.dataSetWhenNoDataFound()
                                    })
                                }
                            }
                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.showInvalidAccessTokenPopup(response["message"] as! String)
                            self.dataSetWhenNoDataFound()
                            break
                        default:
                            self.dataSetWhenNoDataFound()
                            break
                        }
                    } else {
                        self.dataSetWhenNoDataFound()
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                }
            })
        }
    }
    
    func clearNotification(_ jobId:Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        self.notificationTable.isUserInteractionEnabled = false
        var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil ? UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String : ""]
        params["fleet_id"] = Singleton.sharedInstance.fleetDetails.fleetId!
        params["job_id"] = jobId == 0 ? "" : "\(jobId)"
//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil ? UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String : "",
//            "fleet_id":Singleton.sharedInstance.fleetDetails.fleetId!,
//            "job_id":jobId == 0 ? "" : "\(jobId)"
//            ] as [String : Any]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.clearNotification, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                print(response)
                if isSucceeded == true {
                    self.notificationTable.isUserInteractionEnabled = true
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        DispatchQueue.main.async(execute: {
                            if(jobId == 0) {
                                NetworkingHelper.sharedInstance.allNotificationDataArray.removeAll(keepingCapacity: false)
                                self.notificationTable.reloadData()
                                self.dataSetWhenNoDataFound()
                                NetworkingHelper.sharedInstance.notificationCount = 0
                                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotificationCount), object: self)
                            }
                            if(NetworkingHelper.sharedInstance.allNotificationDataArray.count == 0) {
                                self.dataSetWhenNoDataFound()
                                // self.backButton.isHidden = true
                                // self.editButton.setTitle("Edit", for: UIControlState())
                                self.navigation.editButton.isHidden = true
                            }
                        })
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    default:
                        break
                    }
                } else {
                    self.notificationTable.isUserInteractionEnabled = true
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func HandleRefresh(_ sender: AnyObject?) {
        self.getAllNotification()
    }
    
    func dataSetWhenNoDataFound(){
        self.refreshControl?.endRefreshing()
        if(NetworkingHelper.sharedInstance.allNotificationDataArray.count == 0) {
            self.navigation.editButton.isHidden = true
        } else {
            self.navigation.editButton.isHidden = false
        }
        
        //  if(self.notificationTable.backgroundView == nil) {
        if(NetworkingHelper.sharedInstance.allNotificationDataArray.count == 0) {
            self.notificationTable.backgroundView = self.tableBackgroundView(self.notificationTable.frame)
        } else {
            self.notificationTable.backgroundView = nil
        }
        //  }
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }

    func tableBackgroundView(_ frame:CGRect) -> UIView {
        let backgroundView = UIView()
        backgroundView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        let label = UILabel(frame: CGRect(x: 0,y: frame.height/2 - HEIGHT.navigationHeight, width: frame.width, height: 60))
        label.text = ERROR_MESSAGE.NO_NOTIFICATIONS_FOR_NOW
        label.font = UIFont(name: UIFont().MontserratRegular, size: 17)
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        backgroundView.addSubview(label)
        return backgroundView
    }

    func setFirstLine(notificationData:NotificationDataObject) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        
        //(notificationData.d?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))
        guard notificationData.flag != FLAG_TYPE.putOffDuty else {
            attributedString = NSMutableAttributedString(string: "\(Auxillary.convertUTCStringToLocalString(notificationData.d!).datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            return attributedString
        }
        
        switch notificationData.job_type! {
        case JOB_TYPE.pickup:
            attributedString = NSMutableAttributedString(string: "\((notificationData.start_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!) - \(TEXT.PICKUP)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        case JOB_TYPE.delivery:
            attributedString = NSMutableAttributedString(string: "\((notificationData.end_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!) - \(TEXT.DELIVERY)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        case JOB_TYPE.appointment, JOB_TYPE.fieldWorkforce:
            attributedString = NSMutableAttributedString(string:  "\((notificationData.start_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!) - \((notificationData.end_time?.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd HH:mm:ss", output: "HH:mm, dd MMM"))!)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        default:
            attributedString = NSMutableAttributedString()
            break
        }
        
        let status = NSMutableAttributedString(string: "  \(self.getNotificationType(flag: notificationData.flag!))", attributes: [NSForegroundColorAttributeName:COLOR.ASSIGNED_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        attributedString.append(status)
        return attributedString
    }
    
    func getNotificationType(flag:Int) -> String {
        switch(flag) {
        case FLAG_TYPE.deleteTask:
            return TEXT.DELETED
        case FLAG_TYPE.reschedule,FLAG_TYPE.taskUpdatedStatus:
            return TEXT.UPDATED
        case FLAG_TYPE.upcoming:
            return TEXT.UPCOMING
        case FLAG_TYPE.newTask:
            return TEXT.CREATED
        default:
            return ""
        }
    }
    
    
    func setSecondLine(notificationData:NotificationDataObject) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        var addressString:NSAttributedString!
        guard notificationData.flag != FLAG_TYPE.putOffDuty else {
            attributedString = NSMutableAttributedString(string: notificationData.message!, attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            return attributedString
        }
        
        
        switch notificationData.job_type! {
        case JOB_TYPE.pickup:
            attributedString = NSMutableAttributedString(string: notificationData.pickupName!, attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            addressString =  NSAttributedString(string: notificationData.start_address!, attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            break
        default:
            attributedString = NSMutableAttributedString(string: notificationData.cust_name!, attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            addressString =  NSAttributedString(string: notificationData.end_address!, attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
            break
        }
        
        
        if attributedString.length > 0 {
            attributedString.append(NSMutableAttributedString(string:", ", attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!]))
        } else {
            attributedString.append(NSAttributedString(string: ""))
        }
        attributedString.append(addressString)
        return attributedString
    }

}

extension AllNotificationsController:UITableViewDataSource, UITableViewDelegate {
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NetworkingHelper.sharedInstance.allNotificationDataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
        headerView.backgroundColor = UIColor.white
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobListCell) as! JobListCell
        cell.rightArrow.isHidden = true
        cell.dot.isHidden = true
        cell.patternLine.isHidden = true
        
        cell.firstLine.attributedText = self.setFirstLine(notificationData: NetworkingHelper.sharedInstance.allNotificationDataArray[indexPath.row])
        cell.secondLine.attributedText = self.setSecondLine(notificationData: NetworkingHelper.sharedInstance.allNotificationDataArray[indexPath.row])
//        cell.setDotView(indexPath: indexPath, filteredTaskListArray: [Singleton.sharedInstance.selectedTaskRelatedArray], isRelatedTask: true)
        return cell
    }
    
}

//MARK: Navigation Bar
extension AllNotificationsController:NavigationDelegate {
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = TEXT.NOTIFICATION_UPPER
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        if NetworkingHelper.sharedInstance.allNotificationDataArray.count > 0 {
            navigation.editButton.isHidden = false
        }
        navigation.editButton.setTitle(TEXT.CLEAR, for: UIControlState.normal)
        navigation.editButton.setTitleColor(UIColor.white, for: .normal)
        navigation.editButton.addTarget(self, action: #selector(self.editAction), for: UIControlEvents.touchUpInside)
        navigation.editButtonTrailingConstraint.constant = 15.0
        self.view.addSubview(navigation)
    }
    
    func backAction() {
        self.dismiss(animated: true, completion: { finished in
            if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
                if (mapStyle == MAP_STYLE.dark){
                    UIApplication.shared.statusBarStyle = .lightContent
                } else {
                    UIApplication.shared.statusBarStyle = .default
                }
            }
            NetworkingHelper.sharedInstance.allNotificationDataArray.removeAll(keepingCapacity: false)
        })
       // _ = self.navigationController?.popViewController(animated: true)
    }
    
    func editAction() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let clearAll = UIAlertAction(title: TEXT.CLEAR_ALL_NOTIFICATION, style: UIAlertActionStyle.destructive){
            UIAlertAction in
            self.clearNotification(0)
        }
        let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(clearAll)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}
