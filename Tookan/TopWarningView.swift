//
//  TopWarningView.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 11/9/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol WarningViewDelegate {
    func removeWarningView()
}

class TopWarningView: UIView {

    @IBOutlet var leftWarningButton: UIButton!
    @IBOutlet var centreLabelView: UILabel!
    @IBOutlet var closeButton: UIButton!
    var delegate:WarningViewDelegate!
    
    override func awakeFromNib() {
//        self.backgroundColor = COLOR.warningColor
        self.setleftWarningButton()
        self.setCloseButton()
        self.setCenterLabel()
        self.addTapGestureOnBackground()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.delegate.removeWarningView()
    }
    
    func setleftWarningButton() {
        self.leftWarningButton.setImage(#imageLiteral(resourceName: "warningImage").withRenderingMode(.alwaysTemplate), for: .normal)
        self.leftWarningButton.tintColor = UIColor.white
        self.leftWarningButton.backgroundColor = UIColor.clear
        self.leftWarningButton.isUserInteractionEnabled = false
    }
    
    func setCloseButton() {
        self.closeButton.setImage(#imageLiteral(resourceName: "close.png").withRenderingMode(.alwaysTemplate), for: .normal)
        self.closeButton.tintColor = UIColor.white
        self.closeButton.backgroundColor = UIColor.clear
    }
    
    func setCenterLabel() {
        self.centreLabelView.text = TEXT.NOTIFICATIONS_DISABLE
        self.centreLabelView.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
        self.centreLabelView.textColor = UIColor.white
        self.centreLabelView.backgroundColor = UIColor.clear
    }
    
    func addTapGestureOnBackground() {
        /*------------- Tap Gesture ----------------*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.moveToSettings))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    func moveToSettings() {
        Singleton.sharedInstance.isNotificationEnabled(completion: { (isEnabled) in
            if isEnabled{
                self.delegate.removeWarningView()
            }else{
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    DispatchQueue.main.async {
                        UIApplication.shared.openURL(url)
                    }
                    self.delegate.removeWarningView()
                }
            }
        })
    }
    
    
}
