//
//  ProfileModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 18/04/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class ProfileModel: NSObject {
    
    func hasAnychangesMade(updatedUserDetails:FleetInfoDetails) -> Bool {
        guard updatedUserDetails.name == Singleton.sharedInstance.fleetDetails.name else {
            return true
        }
        
        guard updatedUserDetails.phone == Singleton.sharedInstance.fleetDetails.phone else {
            return true
        }
        
//        if Singleton.sharedInstance.fleetDetails.teams.count == 0 {
//            let team = AssignedTeamDetails(json: [:])
//            Singleton.sharedInstance.fleetDetails.teams.append(team)
//        }
//        
//        if updatedUserDetails.teams.count == 0 {
//            let team = AssignedTeamDetails(json: [:])
//            updatedUserDetails.teams.append(team)
//        }
        
        guard updatedUserDetails.teams?.teamName == Singleton.sharedInstance.fleetDetails.teams?.teamName else {
            return true
        }
        
        return isPasswordValidationRequired(updatedUserDetails: updatedUserDetails)
    }
    
    func isPasswordValidationRequired(updatedUserDetails:FleetInfoDetails) -> Bool {
        guard updatedUserDetails.oldPassword.length == 0 else {
            return true
        }
        
        guard updatedUserDetails.newPassword.length == 0 else {
            return true
        }
        
        guard updatedUserDetails.retypePassword.length == 0 else {
            return true
        }
        return false
    }
    
    func hasPasswordConditionsValidated(updatedUserDetails:FleetInfoDetails) -> (Bool, String, Int) {
        guard updatedUserDetails.oldPassword.length >= 6 else {
            return (false,ERROR_MESSAGE.SIX_CHAR_PASSWORD, PROFILE_FIELDS.oldPassword.rawValue)
        }
        
        guard updatedUserDetails.newPassword.length >= 6 else {
            return (false,ERROR_MESSAGE.SIX_CHAR_PASSWORD, PROFILE_FIELDS.newPassword.rawValue)
        }
        
        guard updatedUserDetails.oldPassword.length <= 20 else {
            return (false,ERROR_MESSAGE.MAX_20_CHAR_PASSWORD, PROFILE_FIELDS.oldPassword.rawValue)
        }
        
        guard updatedUserDetails.newPassword.length <= 20 else {
            return (false,ERROR_MESSAGE.MAX_20_CHAR_PASSWORD, PROFILE_FIELDS.newPassword.rawValue)
        }
        
        
        guard updatedUserDetails.newPassword != updatedUserDetails.oldPassword else {
            return (false,ERROR_MESSAGE.SAME_PASSWORD, PROFILE_FIELDS.newPassword.rawValue)
        }
        
        guard updatedUserDetails.retypePassword == updatedUserDetails.newPassword else {
            return (false,ERROR_MESSAGE.DIFF_PASSWORD, PROFILE_FIELDS.retypePassword.rawValue)
        }
        return (true,"",0)
    }
    /*---------- Upload User Image ------------*/
    func uploadImageToServer(_ refImage:UIImage, receivedResponse:@escaping (_ succeeded:Bool,_ response:[String:Any])-> ()) {
        if IJReachability.isConnectedToNetwork() == true {
            var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
            params["timezone"] = -(NSTimeZone.system.secondsFromGMT() / 60)
            params["device_token"] = UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) as! String
            params["device_type"] = DEVICE_TYPE
//            let params = [
//                "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//                "timezone":-(NSTimeZone.system.secondsFromGMT() / 60),
//                "device_token":UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) as! String,
//                "device_type":DEVICE_TYPE
//                ] as [String : Any]
            var imageData = UIImageJPEGRepresentation(refImage, 0.1)
            var isImage = false
            if imageData == nil {
                imageData = Data()
                isImage = false
            } else {
                isImage = true
            }
            
            uploadingMultipleTask(API_NAME.editProfile, params: params as [String : AnyObject], isImage: isImage, imageData: [refImage], imageKey: "fleet_image", receivedResponse: { (succeeded, response) -> () in
                if(succeeded == true) {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        DispatchQueue.main.async(execute: { () -> Void in
                            receivedResponse(true, response)
                        })
                        break
                        
                    default:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["message":message])
                        } else {
                            receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        }
                    }
                } else {
                    receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                }
            })
            
        } else {
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }

}
