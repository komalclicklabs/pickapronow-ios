//
//  MapTrackingController.swift
//  Tookan
//
//  Created by Rakesh Kumar on 4/18/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import GoogleMaps

class MapTrackingController: UIViewController, NavigationDelegate,GMSMapViewDelegate {

    @IBOutlet weak var googleMapView: GMSMapView!
    var originCoordinate: CLLocationCoordinate2D!
    var path = GMSMutablePath()
    override func viewDidLoad() {
        super.viewDidLoad()
        googleMapView.isTrafficEnabled = true
        googleMapView.isMyLocationEnabled = true
        self.setNavigationBar()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        NotificationCenter.default.addObserver(self, selector: #selector(MapTrackingController.updatePath), name: NSNotification.Name(rawValue: OBSERVER.updatePath), object: nil)

        if let userlocationDict = UserDefaults.standard.object(forKey: USER_DEFAULT.lastAccurateUserLocation) as? [String:Any] {
            let latitudeString = userlocationDict["latitude"] as! NSString
            let longitudeString = userlocationDict["longitude"] as! NSString
            let coordinate = CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            originCoordinate = coordinate
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 16)
            googleMapView.camera = camera
        }
        
        self.updatePath()
    }
    
    func updatePath() {
        path = GMSMutablePath()
        if let locationDictionaryArray = UserDefaults.standard.value(forKey: USER_DEFAULT.updatingLocationPathArray) as? [Any] {
            for i in (0..<locationDictionaryArray.count) {
                if let locationDictionary = locationDictionaryArray[i] as? [String:Any] {
                    path.add(CLLocationCoordinate2D(latitude: locationDictionary["Latitude"] as! Double, longitude: locationDictionary["Longitude"] as! Double))
                }
            }
        }
        
        self.createRoutePathArray()
    }
    
    func createRoutePathArray() {
        googleMapView.clear()
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.blue
        polyline.strokeWidth = 8.0
        polyline.geodesic = true;
        polyline.map = googleMapView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  //MARK: Navigation Bar
    func setNavigationBar() {
        let navigation = UINib(nibName: "navigationBar", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = "Live Tracking"
        self.view.addSubview(navigation)
    }
    
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }

}
