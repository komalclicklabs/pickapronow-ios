//
//  StatusKey.swift
//  Tookan
//
//  Created by cl-macmini-45 on 15/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class StatusKey: NSObject {

}

/*============ Push Notification ===============*/
struct NOTIFICATION {
    struct action {
        static let accept = "Accept"
        static let decline = "Decline"
        static let open = "Open"
        static let ok = "OK"
        static let acknowledge = "Acknowledge"
    }
    
    struct category {
        static let newTaskAcceptDecline = "NT_A"
        static let newTaskAcknowledge = "NT_ACK"
        static let newTaskNone = "NT_N"
        static let newTaskOpen = "NT_O"
    }
}


/*---------------- Optional Field Struct ------------*/
struct APP_OPTIONAL_FIELD {
    static let accept = "accept"
    static let notes = "notes"
    static let images = "images"
    static let signature = "signature"
    static let arrived = "arrived"
    static let slider = "slider"
    static let confirm = "confirm"
    static let fleetNotification = "fleet_notification"
    static let masking = "masking"
    static let scanner = "barcode"
    static let hideCancelButton = "cancel_btn"
    static let hideFailedButton = "failed_btn"
    static let caption = "images_caption"
}

struct CUSTOM_FIELD_DATA_TYPE {
    static let dropDown = "Dropdown"
    static let text = "Text"
    static let number = "Number"
    static let image = "Image"
    static let date = "Date"
    static let checkbox = "Checkbox"
    static let telephone = "Telephone"
    static let email = "Email"
    static let url = "URL"
    static let checklist = "Checklist"
    static let dateFuture = "Date-Future"
    static let datePast = "Date-Past"
    static let dateTime = "Date-Time"
    static let dateTimeFuture = "Datetime-Future"
    static let dateTimePast = "Datetime-Past"
    static let table = "Table"
    static let barcode = "Barcode"
}

struct JOB_STATUS {
    static let assigned = 0;
    static let started = 1;
    static let successful = 2;
    static let failed = 3;
    static let arrived = 4;
    static let partial = 5;
    static let unassigned = 6;
    static let accepted = 7; // Acknowledged
    static let declined = 8;
    static let canceled = 9;
    static let ignored = 11;
    static let okAcceptStatus = 12;
    //static let complete = 99
}

struct  AVAILABILITY {
    static let available = 0
    static let unavailable = 1
    static let busy = 2
    static let highlightedAvailable = 3
    static let highlightedUnavailable = 4
}

struct BATTERY_USAGE {
    static let low = 0
    static let medium = 1
    static let high = 2
    static let foreground = 3
}

struct LAYOUT_TYPE {
    static let pickupAndDelivery = 0
    static let appointment = 1
    static let fos = 2
}

struct JOB_TYPE {
    static let pickup = 0
    static let delivery = 1
    static let fieldWorkforce = 2
    static let appointment = 3
    static let both = 10
}

struct RELATED_JOB_TYPE {
    static let SINGLE = 0
    static let GOTO_PICKUP = 1
    static let GOTO_DELIVERY = 2
    static let GOTO_MULTIPLE = 3
}

struct FLAG_TYPE {
    static let newTask = 1
    static let reschedule = 2
    static let upcoming = 3
    static let deleteTask = 4
    static let taskUpdatedStatus = 6
    static let putOffDuty = 7
    static let none = 8
    static let silent = 9
    static let updateLocation = 10
    static let unableToUploadLocation = 13
    static let batteryUsage = 14
    static let signupVerificationCode = 15
    static let alertMessage = 16
}

struct ACCEPT_TYPE {
    static let acknowledge = 0
    static let acceptDecline = 1
    static let startCancel = 2
    static let edit = 3
}

struct APP_SIDE {
    static let read = "0"
    static let write = "1"
    static let hidden = "2"
    static let tableEdit = "3"
}

struct NOTIFICATION_TONE {
    static let short = "ping.caf"
    static let long = "ring.aiff"
}

struct NAVIGATION_MAP {
    static let googleMap = 0
    static let waze = 1
    static let appleMap = 2
}

struct MAP_STYLE {
    static let dark = 0
    static let light = 1
}

enum VERTICAL:Int {
    case TOOKAN = 0
    case TAXI = 1
}

struct DutyStatus {
    static let onDuty = 1
    static let offDuty = 0
}

struct APP_CACHING {
    static let ON = 1
    static let OFF = 0
}

struct SHOW_HIDE {
    static let showPartial = 1
    static let showComplete = 2
    static let hidePartial = 3
    static let hideComplete = 4
}

enum JUST_NOW:Int {
    case earlier = 0
    case justNow = 1
}

enum DRIVER_STATE:Int {
    case otp_pending = 2
    case otp_verified = 3
    case verification_pending = 4
    case resubmit_verificaton = 6
    case verified = 7
    case rejected = 5
    case acknowledged = 1
}

/*
1: Initial sign up - { filled first signup screen }  => show OTP screen
2: OTP verified - { Verified OTP sent via SMS/call} => show template fields if available
3: Verification pending / Submitted template - { Submitted template; Now action pending from dashboard } => show pending screen
4: Resubmit - { admin wants resubmission of some keys } => show signup template screen *
5: Verified - { admin verified agent } => show success verification screen
6: Rejected - { admin rejected } => access_token becomes unusable
7: Acknowledged - { Verified acknowledged by driver } => normal driver app flow*/



