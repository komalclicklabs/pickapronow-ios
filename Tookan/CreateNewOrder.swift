//
//  CreateNewOrder.swift
//  Tookan
//
//  Created by cl-macmini-150 on 05/04/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class CreateNewOrder: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var newTaskView: UIView!
    @IBOutlet weak var pickupBtn: UIButton!
    @IBOutlet weak var deliveryBtn: UIButton!
    @IBOutlet weak var taskListTableView: UITableView!
    @IBOutlet weak var createOrderButtonOutlet: UIButton!
    var numberOfCells = 1
    var navigation:navigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.setTable()
        self.settingLayout()
        /*===============   Setting View And Gesture   ========================*/
        self.newTaskView.layer.cornerRadius = getAspectRatioValue(value: 15)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
        tap.delegate = self
        self.newTaskView.isHidden = true
        
        /*===============   Create Button   ========================*/
        createOrderButtonOutlet.isHidden = true
        createOrderButtonOutlet.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        createOrderButtonOutlet.backgroundColor = COLOR.themeForegroundColor
        createOrderButtonOutlet.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        createOrderButtonOutlet.setTitle("\(TEXT.CREATE) \(TEXT.ORDER)", for: .normal)
        
        /*===============   Setting View   ============================*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
        case LAYOUT_TYPE.appointment, LAYOUT_TYPE.fos:
            self.movingToNextVC(type: 0, index: -1)
            break
        default:
            break
        }
    }
    
    func settingLayout(){
        if Singleton.sharedInstance.createTaskArray.count == 0 {
            newTaskView.isHidden = false
        } else{
            taskListTableView.isHidden = false
            createOrderButtonOutlet.isHidden = false
        }
    }
    
    func setTable() {
        /*===============   Setting Table View   ========================*/
        self.taskListTableView.isHidden = true
        self.taskListTableView.layer.cornerRadius = getAspectRatioValue(value: 15)
        //self.taskListTableView.estimatedRowHeight = 117
        self.taskListTableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.taskListTableView.rowHeight = UITableViewAutomaticDimension
        /*===============   Registration of Cell   ========================*/
        self.taskListTableView.register(UINib(nibName: NIB_NAME.jobListCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobListCell)
        self.taskListTableView.register(UINib(nibName: NIB_NAME.addAnotherPickupAndDelivery, bundle: nil), forCellReuseIdentifier: NIB_NAME.addAnotherPickupAndDelivery)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        /*===============   Setting Animation   ============================*/
        self.newTaskView.transform = CGAffineTransform(translationX: 0, y: SCREEN_SIZE.height - HEIGHT.navigationHeight)
        self.taskListTableView.transform = CGAffineTransform(translationX: 0, y: SCREEN_SIZE.height - HEIGHT.navigationHeight)
        self.createOrderButtonOutlet.transform = CGAffineTransform(translationX: 0, y: HEIGHT.navigationHeight)
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.newTaskView.transform = CGAffineTransform.identity
            self.taskListTableView.transform = CGAffineTransform.identity
            self.navigation.titleLabel.transform = CGAffineTransform.identity
        }, completion: nil)
        
        UIView.animate(withDuration: 0.35) {
            self.createOrderButtonOutlet.transform = CGAffineTransform.identity
        }
        
        /*===============   Setting Button   ============================*/
        pickupBtn.layer.cornerRadius = getAspectRatioValue(value: pickupBtn.frame.height/2)
        pickupBtn.layer.borderWidth = 1.0
        pickupBtn.layer.borderColor = COLOR.CREATE_TASK_BORDER_COLOR.cgColor
        deliveryBtn.layer.cornerRadius = getAspectRatioValue(value: deliveryBtn.frame.height/2)
        deliveryBtn.layer.borderWidth = 1.0
        deliveryBtn.layer.borderColor = COLOR.CREATE_TASK_BORDER_COLOR.cgColor
        
        /*===============   Setting View   ============================*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
        case LAYOUT_TYPE.pickupAndDelivery:
            
            if Singleton.sharedInstance.createTaskArray.count == 0 {
                newTaskView.isHidden = false
                taskListTableView.isHidden = true
                createOrderButtonOutlet.isHidden = true
            }else{
                newTaskView.isHidden = true
                taskListTableView.isHidden = false
                createOrderButtonOutlet.isHidden = false
            }
            break
        case LAYOUT_TYPE.appointment, LAYOUT_TYPE.fos:
                if Singleton.sharedInstance.createTaskArray.count > 0 {
                    self.newTaskView.isHidden = true
                    self.taskListTableView.isHidden = false
                    self.createOrderButtonOutlet.isHidden = false
                }
            break
        default:
            break
        }
        self.taskListTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.perform(#selector(self.updateStatusBar), with: nil, afterDelay: 1.0)
    }
    
    func updateStatusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    //MARK: Buttons Action
    @IBAction func pickupAction(_ sender: Any) {
        self.movingToNextVC(type: 0, index: -1)
    }
    
    @IBAction func deliveryAction(_ sender: Any) {
        self.movingToNextVC(type: 1, index: -1)
    }
    
    //MARK: UITapGestureRecognizer
    func tapped() {
        Singleton.sharedInstance.createTaskArray.removeAll()
        UIView.animate(withDuration: 0.35, animations: {
            self.newTaskView.transform = CGAffineTransform(translationX: 0, y: self.newTaskView.frame.height - 60)
            self.taskListTableView.transform = CGAffineTransform(translationX: 0, y: self.taskListTableView.frame.height + 60)
            self.createOrderButtonOutlet.transform = CGAffineTransform(translationX: 0, y: self.createOrderButtonOutlet.frame.height)
        }, completion: { finished in
            _ = self.navigationController?.popViewController(animated: true)
        })
    }
    
    func checkForMultiplePickupMultipleDeliveries() -> Bool {
        var pickupCount = 0
        for task in Singleton.sharedInstance.createTaskArray {
            if task.jobType == JOB_TYPE.pickup {
                pickupCount = pickupCount + 1
                if pickupCount >= 2 {
                    return true
                }
            }
        }
        
        return false
    }
    
    //MARK: TableViewDelegate
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 20))
        returnedView.backgroundColor = .white
        return returnedView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 119
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 119))
        returnedView.backgroundColor = .clear
        return returnedView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if Singleton.sharedInstance.createTaskArray.count > 0{
            if Singleton.sharedInstance.createTaskArray.last?.has_delivery == 1 {
                if self.checkForMultiplePickupMultipleDeliveries() == true {
                    numberOfCells = 0
                    return Singleton.sharedInstance.createTaskArray.count
                } else {
                    numberOfCells = 1
                    return Singleton.sharedInstance.createTaskArray.count + 1
                }
            } else if Singleton.sharedInstance.createTaskArray.last?.has_pickup == 1 {
                numberOfCells = 2
                return Singleton.sharedInstance.createTaskArray.count + 2
            } else {
                numberOfCells = 1
                return Singleton.sharedInstance.createTaskArray.count + 1
            }
        }
        return Singleton.sharedInstance.createTaskArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < Singleton.sharedInstance.createTaskArray.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobListCell, for: indexPath) as! JobListCell
            cell.firstLine.attributedText = self.setFirstLine(jobDetails: Singleton.sharedInstance.createTaskArray[indexPath.row])
            cell.secondLine.attributedText = self.setSecondLine(jobDetails: Singleton.sharedInstance.createTaskArray[indexPath.row])
            cell.dot.layer.cornerRadius = 4.5
            cell.dot.layer.borderWidth = 0.5
            cell.dot.layer.borderColor = UIColor.black.cgColor
            cell.topPatternLine.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "iconPattern"))
            cell.dot.layer.masksToBounds = true
            if indexPath.row == 0{
                cell.topPatternLine.isHidden = true
            } else {
                cell.topPatternLine.isHidden = false
            }
            
            if Singleton.sharedInstance.createTaskArray[indexPath.row].has_pickup == 1{
                cell.dot.backgroundColor = UIColor.black
            } else {
                cell.dot.backgroundColor = UIColor.white
              
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.addAnotherPickupAndDelivery, for: indexPath) as! AddAnotherPickupAndDelivery
            
            if numberOfCells == 2 {
                cell.addAnotherTaskLabel.text = TEXT.ADD_ANOTHER_PICKUP_POINT
                numberOfCells = 1
            } else {
                switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
                case LAYOUT_TYPE.appointment, LAYOUT_TYPE.fos:
                    cell.addAnotherTaskLabel.text = TEXT.ADD_ANOTHER_POINT
                    cell.bottomView.isHidden = true
                    break
                default:
                    cell.addAnotherTaskLabel.text = TEXT.ADD_A_DELIVERY_POINT
                    cell.bottomView.isHidden = true
                    break
                }
                
            }
            
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var jobType = -1
        var index = -1
        if indexPath.row < Singleton.sharedInstance.createTaskArray.count{
            index = indexPath.row
            if Singleton.sharedInstance.createTaskArray[indexPath.row].has_pickup == 1 {
                jobType = 1
            } else{
                jobType = 0
            }
        } else {
            index = -1
            if addPoint(index : indexPath.row) == true {
                jobType = 1
            } else {
                jobType = 0
            }
        }
        self.movingToNextVC(type: jobType, index: index)
    }
    func movingToNextVC(type: Int, index:Int){
        if let order = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.addDeliveryPoint) as? AddDeliveryPoint {
            order.indicationOfType = type
            order.selectedTaskIndex = index
            order.modalPresentationStyle = .overCurrentContext
            order.modalTransitionStyle = .coverVertical
            order.callBack = { finished in
                self.navigation.isHidden = false
                if Singleton.sharedInstance.createTaskArray.count == 0 {
                    _ = self.navigationController?.popViewController(animated: false)
                } else {
                    self.newTaskView.isHidden = true
                    self.taskListTableView.isHidden = false
                    self.createOrderButtonOutlet.isHidden = false
                }
            }
            
            UIView.animate(withDuration: 0.35, animations: {
                self.newTaskView.transform = CGAffineTransform(translationX: 0, y: self.newTaskView.frame.height - 60)
                self.taskListTableView.transform = CGAffineTransform(translationX: 0, y: self.taskListTableView.frame.height + 60)
                self.createOrderButtonOutlet.transform = CGAffineTransform(translationX: 0, y: self.createOrderButtonOutlet.frame.height)
                self.navigation.titleLabel.transform = CGAffineTransform(translationX: SCREEN_SIZE.width, y: 0)
            }, completion: { finished in
                if self.navigationController != nil {
                    self.navigationController?.pushViewController(order, animated: false)
                }
            })
        }
    }
    
    //MARK: Functions Used in didSelectRowAt
    func setFirstLine(jobDetails:CreateTaskModel) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        switch jobDetails.jobType {
        case JOB_TYPE.pickup:
            attributedString = NSMutableAttributedString(string: "\(jobDetails.startTime) - \(TEXT.PICKUP)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        case JOB_TYPE.delivery:
            attributedString = NSMutableAttributedString(string: "\(jobDetails.startTime) - \(TEXT.DELIVERY)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        case JOB_TYPE.appointment, JOB_TYPE.fieldWorkforce:
            attributedString = NSMutableAttributedString(string: "\(jobDetails.startTime) - \(jobDetails.endTime)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
            break
        default:
            break
        }
        
        return attributedString
    }
    
    func setSecondLine(jobDetails:CreateTaskModel) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        var addressString:NSAttributedString!
        attributedString = NSMutableAttributedString(string: jobDetails.name, attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
        addressString =  NSAttributedString(string: jobDetails.address, attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
        if attributedString.length > 0 {
            attributedString.append(NSMutableAttributedString(string:", ", attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!]))
        } else {
            attributedString.append(NSAttributedString(string: ""))
        }
        attributedString.append(addressString)
        return attributedString
    }
    
    //MARK: Checking the type of task
    func addPoint(index : Int) -> Bool {
        if Singleton.sharedInstance.createTaskArray.last?.has_delivery == 1 {
            return true
        } else {
            if index == Singleton.sharedInstance.createTaskArray.count{
                return false
            } else {
                return true
            }
        }
    }
    
    @IBAction func createTaskAction(_ sender: Any) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        let model = CreateTaskModel()
        let params = model.setJsonForCreateTaskRequest()
        print(params)
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.fleet_create_multiple_tasks, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                print(response)
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        print(response)
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .success)
                        self.tapped()
                        break
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    default:
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK: Functions of UIGestureRecognizerDelegate
extension CreateNewOrder: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view != nil && touch.view!.isDescendant(of: self.taskListTableView)){
            return false
        }
        return true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,shouldRecognizeSimultaneouslyWith otherGestureRecognizer : UIGestureRecognizer)->Bool{
        return true
    }
}

//MARK: Navigation Bar
extension CreateNewOrder:NavigationDelegate {
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = "\(TEXT.NEW) \(TEXT.ORDER)"// + TEXT.ORDER
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "close_profile").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        navigation.titleLabel.transform = CGAffineTransform(translationX: SCREEN_SIZE.width, y: 0)
        self.view.addSubview(navigation)
        
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)) {
        case LAYOUT_TYPE.appointment, LAYOUT_TYPE.fos:
            navigation.isHidden = true
            break
        default:
            break
        }

    }
    
    func backAction() {
        let alert = UIAlertController(title: "", message: ERROR_MESSAGE.ARE_YOU_SURE, preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: TEXT.YES, style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            DispatchQueue.main.async {
                self.tapped()
            }
        }
        
        let noAction = UIAlertAction(title: TEXT.NO, style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func editAction() {
        
    }
}
