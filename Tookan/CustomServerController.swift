//
//  CustomServer.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 6/13/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class CustomServerController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var protocolTextField: UITextField!
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var portTextField: UITextField!
    @IBOutlet weak var deviceTypeTextField: UITextField!
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBOutlet weak var submitOutlet: UIButton!
    @IBOutlet weak var protocolLabel: UILabel!
    @IBOutlet weak var portLabel: UILabel!
    @IBOutlet weak var deviceTypeLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var completeUrl: UILabel!
    var keyboardToolbar:KeyboardToolbar!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backButtonOutlet.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        backButtonOutlet.tintColor = COLOR.TEXT_COLOR
        
        self.submitOutlet?.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.submitOutlet.backgroundColor = COLOR.themeForegroundColor
        self.submitOutlet.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        
        self.protocolLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.portLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.deviceTypeLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.urlLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.completeUrl.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        
        self.protocolTextField.delegate = self
        self.urlTextField.delegate = self
        self.portTextField.delegate = self
        self.deviceTypeTextField.delegate = self
        let url = URL(string: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String))
        self.deviceTypeTextField.text = DEVICE_TYPE
        self.protocolTextField.text = url?.scheme!
        self.urlTextField.text = url?.host!
        self.portTextField.text = "\((url?.port)!)"
        self.completeUrl.text = "\(protocolTextField.text!)://\(urlTextField.text!):\(portTextField.text!)/"
        
        
        /*--------------- UITapGesture -------------*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
        
        self.keyboardToolbar = KeyboardToolbar()
        self.keyboardToolbar.keyboardDelegate = self
        self.keyboardToolbar.addButtons()
    }

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func backgroundTouch() {
        self.view.endEditing(true)
    }

    
    @IBAction func submitAction(_ sender: Any) {
        if checkValidation() == true {
            SERVER.custom = "\(protocolTextField.text!)://\(urlTextField.text!):\(portTextField.text!)/"
            
            if (deviceTypeTextField.text?.length)! > 0 {
                DEVICE_TYPE = "\(deviceTypeTextField.text!)"
            }
            
            UserDefaults.standard.setValue(SERVER.custom, forKey: USER_DEFAULT.selectedServer)
            print(SERVER.custom)
            print(DEVICE_TYPE)
            
            _ = self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func backAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func checkValidation() -> Bool{
        guard (protocolTextField.text?.length)! > 0 else {
            Singleton.sharedInstance.showErrorMessage(error:"\(ERROR_MESSAGE.PLEASE_ENTER_VALID) Scheme" , isError: .error)
            return false
        }
        
        guard (urlTextField.text?.length)! > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) Host", isError: .error)
            return false
        }
        
        guard Auxillary.verifyUrl(urlTextField.text) == true else {
            Singleton.sharedInstance.showErrorMessage(error: "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) Host", isError: .error)
            return false
        }
        
        guard (portTextField.text?.length)! > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: "\(ERROR_MESSAGE.PLEASE_ENTER_VALID) Port", isError: .error)
            return false
        }
        
        if (Int(deviceTypeTextField.text!)! % 2 == 0){
            Singleton.sharedInstance.showErrorMessage(error: "Device type must be Odd", isError: .error)
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedText = textField.text!
        var updatedString:NSString = "\(updatedText)" as NSString
        updatedString = updatedString.replacingCharacters(in: range, with: string) as NSString
        
        switch(textField) {
        case protocolTextField:
            self.completeUrl.text = "\(updatedString)://\(urlTextField.text!):\(portTextField.text!)/"
        case urlTextField:
            self.completeUrl.text = "\(protocolTextField.text!)://\(updatedString):\(portTextField.text!)/"
        case portTextField:
            self.completeUrl.text = "\(protocolTextField.text!)://\(urlTextField.text!):\(updatedString)/"
        default:
            break
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        switch(textField) {
        case protocolTextField:
            self.completeUrl.text = "://\(urlTextField.text!):\(portTextField.text!)/"
        case urlTextField:
            self.completeUrl.text = "\(protocolTextField.text!)://:\(portTextField.text!)/"
        case portTextField:
            self.completeUrl.text = "\(protocolTextField.text!)://\(urlTextField.text!):/"
        default:
            break
        }

        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.completeUrl.text = "\(protocolTextField.text!)://\(urlTextField.text!):\(portTextField.text!)/"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.inputAccessoryView = keyboardToolbar
        keyboardToolbar.currentTextField = textField
    }
}

extension CustomServerController : KeyboardDelegate {
    func doneFromKeyboard() {
        self.view.endEditing(true)
    }
    func nextFromKeyboard(_ nextField: AnyObject) {
        let textField = nextField as! UITextField
        switch(textField) {
        case protocolTextField:
            urlTextField.becomeFirstResponder()
        case urlTextField:
            portTextField.becomeFirstResponder()
        case portTextField:
            deviceTypeTextField.becomeFirstResponder()
        default:
            self.view.endEditing(true)
        }
        
    }
    
    func prevFromKeyboard(_ prevField: AnyObject) {
        let textField = prevField as! UITextField
        switch(textField) {
        case deviceTypeTextField:
            portTextField.becomeFirstResponder()
        case urlTextField:
            protocolTextField.becomeFirstResponder()
        case portTextField:
            urlTextField.becomeFirstResponder()
        default:
            self.view.endEditing(true)
        }
    }
}

