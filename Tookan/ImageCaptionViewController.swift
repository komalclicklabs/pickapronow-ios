//
//  ImageCaptionViewController.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 8/11/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
protocol ImageCaptionViewControllerDelegate {
    func setImage(caption: String,image: UIImage, pickerTag: Int)
}

class ImageCaptionViewController: UIViewController {
    
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var sendButtonOutlet: UIButton!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var captionTextView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    
    var delegate:ImageCaptionViewControllerDelegate!
    var navigation:navigationBar!
    var pickerTag: Int!
    let defaultConstraint = Singleton.sharedInstance.getBottomInsetofSafeArea()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.setTextView()
        self.tapGesture()
        self.setButton()
        self.setPlaceHolderLabel()
        self.view.backgroundColor = UIColor.black
        self.imagePreview.contentMode = UIViewContentMode.scaleAspectFit
        self.textViewBottomConstraint.constant = self.defaultConstraint
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func loadView() {
        Bundle.main.loadNibNamed(NIB_NAME.imageCaptionViewController, owner: self, options: nil)
        self.view.frame = UIScreen.main.bounds
    }
    
    func setPlaceHolderLabel() {
        self.placeHolderLabel.text = TEXT.ADD_CAPTION
        self.placeHolderLabel.textColor = UIColor.lightGray
        self.placeHolderLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
    }
    
    func setTextView()  {
        self.captionTextView.backgroundColor = COLOR.imagePreviewBackgroundColor
        self.captionTextView.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.captionTextView.textColor = UIColor.white
        self.captionTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 51)
        self.captionTextView.delegate = self
    }
    
    func setButton() {
        self.sendButtonOutlet.backgroundColor = COLOR.themeForegroundColor
        self.sendButtonOutlet.layer.cornerRadius = 18.0
        self.sendButtonOutlet.setImage(#imageLiteral(resourceName: "right_arrow").withRenderingMode(.alwaysTemplate), for: .normal)
        self.sendButtonOutlet.tintColor = UIColor.white
    }
    
    //MARK: Keyboard Observer
    func keyboardWillShow(_ notification: Foundation.Notification) {
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = value.cgRectValue.size
        let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: [UIViewAnimationOptions(rawValue: UInt(curve))], animations: {
            self.textViewBottomConstraint.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: [UIViewAnimationOptions(rawValue: UInt(curve))], animations: {
            self.textViewBottomConstraint.constant = self.defaultConstraint
        }, completion: nil)
    }
    
    //MARK: Button Action
    @IBAction func doneButtonAction(_ sender: Any) {
        if Singleton.sharedInstance.selectedTaskDetails.captionOptionalField.required == true {
            guard self.captionTextView.text.trimText.length > 0 else {
                Singleton.sharedInstance.showErrorMessage(error: "\(ERROR_MESSAGE.PLEASE_ENTER) \(TEXT.CAPTION_S)", isError: .error)
                return
            }
        }
        self.sendButtonOutlet.isEnabled = false
        ActivityIndicator.sharedInstance.showActivityIndicator()
        self.view.endEditing(true)
        self.delegate.setImage(caption: self.captionTextView.text.trimText, image: self.imagePreview.image!, pickerTag: self.pickerTag)
        self.dismiss(animated: true) {
            ActivityIndicator.sharedInstance.hideActivityIndicator()
        }
    }
    
    //MARK: Tap Gesture
    func tapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
    }
    
    func backgroundTouch()  {
        self.view.endEditing(true)
    }
    
}

//MARK: NavigationDelegate
extension ImageCaptionViewController: NavigationDelegate{
    func backAction() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = TEXT.CAPTION
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "close_profile").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        navigation.backgroundColor = COLOR.imagePreviewBackgroundColor
        self.view.addSubview(navigation)
    }
}

extension ImageCaptionViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view != nil && touch.view!.isDescendant(of: self.imagePreview) {
            return false
        }
        return true
    }
}

extension ImageCaptionViewController:UITextViewDelegate {
    //MARK: TextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        
        if self.captionTextView.contentSize.height < self.textViewHeightConstraint.constant {
            self.captionTextView.isScrollEnabled = false
        } else {
            self.captionTextView.isScrollEnabled = true
        }
        
        if self.captionTextView.text == "" {
            self.placeHolderLabel.isHidden = false
        } else {
            self.placeHolderLabel.isHidden = true
        }
    }
}
