//
//  MenuListCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 21/03/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class MenuListCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bottomLine: UIView!
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var switchButton: UISwitch!
    var gradientLayer: CAGradientLayer!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.titleLabel.textColor = UIColor.white
        self.countLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.countLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        self.countLabel.text = "0"
        self.bottomLine.alpha = 0.5
        self.switchButton.transform = CGAffineTransform(scaleX: 0.80, y: 0.80)
    }

    func setGradientEffect() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 1)
        gradientLayer.colors = [UIColor.black.cgColor, UIColor.white.cgColor, UIColor.black.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5) //CGPointMake(0.0, 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5) //CGPointMake(1.0, 0.5)
        self.bottomLine.layer.addSublayer(gradientLayer)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
