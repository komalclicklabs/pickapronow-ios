//
//  AvailabilityModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 17/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class AvailabilityModel: NSObject {

    /*------------ Get availability Data ---------------*/
//    func getAvailabilitySummary(_ selectedDate:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//            "local_date_time":selectedDate,
//            "blank_array_check":1
//            ] as [String : Any]
//        if IJReachability.isConnectedToNetwork() == true {
//            sendRequestToServer(API_NAME.getWeekAvailability, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
//                if(succeeded){
//                   // print(response)
//                    switch(response["status"] as! Int) {
//                    case STATUS_CODES.SHOW_DATA, STATUS_CODES.INVALID_ACCESS_TOKEN, STATUS_CODES.UNAUTHORIZED_FOR_AVAILABILITY:
//                        DispatchQueue.main.async(execute: { () -> Void in
//                            receivedResponse(true, response)
//                        })
//                        break
//                        
//                    case STATUS_CODES.SHOW_MESSAGE:
//                        Auxillary.showAlert(response["message"] as! String!)
//                        receivedResponse(false, [:])
//                        break
//                        
//                    default:
//                        Auxillary.showAlert(response["message"] as! String!)
//                        receivedResponse(false, [:])
//                    }
//                } else {
//                    Auxillary.showAlert(ERROR_MESSAGE.SERVER_NOT_RESPONDING)
//                    receivedResponse(false, [:])
//                }
//            }
//        } else {
//            Auxillary.showAlert(ERROR_MESSAGE.NO_INTERNET_CONNECTION)
//            receivedResponse(false, [:])
//        }
//    }
    
    /*------------ Set availability Data ---------------*/
//    func setAvailabilitySummary(_ day:Int, calendarUpdatedArray:[Int], isDayBlocked:Int, copyRepeatArray:[Int], everyWeekStatus:Int, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
//     //   let dateJson = self.getDateJson(day: day)
//     //   print(day)
//        let calendarJson = self.getTimeSlotJson(indexArray: calendarUpdatedArray, day: day)
//        let copyRepeatJson = self.getCopyRepeatJson(indexArray: copyRepeatArray)
//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//            "local_date_time":Singleton.sharedInstance.availabilityData[day].dateTime,
//            "calendar_information_json":calendarJson.jsonString,
//            "is_day_blocked":isDayBlocked,
//            "copy_to_date_json":copyRepeatJson.jsonString,
//            "every_week_status":everyWeekStatus
//            ] as [String : Any]
//     //   print(params)
//        if IJReachability.isConnectedToNetwork() == true {
//            sendRequestToServer(API_NAME.setDayAvailability, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
//                if(succeeded){
//                  //  print(response)
//                    switch(response["status"] as! Int) {
//                    case STATUS_CODES.SHOW_DATA, STATUS_CODES.INVALID_ACCESS_TOKEN, STATUS_CODES.UNAUTHORIZED_FOR_AVAILABILITY:
//                        DispatchQueue.main.async(execute: { () -> Void in
//                            receivedResponse(true, response)
//                        })
//                        break
//                        
//                    case STATUS_CODES.SHOW_MESSAGE:
//                        Auxillary.showAlert(response["message"] as! String!)
//                        receivedResponse(false, [:])
//                        break
//                        
//                    default:
//                        Auxillary.showAlert(response["message"] as! String!)
//                        receivedResponse(false, [:])
//                    }
//                } else {
//                    Auxillary.showAlert(ERROR_MESSAGE.SERVER_NOT_RESPONDING)
//                    receivedResponse(false, [:])
//                }
//            }
//        } else {
//            Auxillary.showAlert(ERROR_MESSAGE.NO_INTERNET_CONNECTION)
//            receivedResponse(false, [:])
//        }
//    }

    func getDateJson(day:Int) -> [Any] {
        let json = ["local_date_time":Singleton.sharedInstance.availabilityData[day].dateTime]
        let jsonArray = [json]
        return jsonArray
    }
    
    func getTimeSlotJson(indexArray:[Int], day:Int) -> [Any] {
        var jsonArray = [Any]()
        let slots = Singleton.sharedInstance.availabilityData[day].slots
        for i in (0..<indexArray.count) {
            
            var availabilityStatus:Int!
            switch slots[indexArray[i]].availabilityStatus! {
            case AVAILABILITY.available, AVAILABILITY.highlightedAvailable:
                availabilityStatus = AVAILABILITY.available
                break
            case AVAILABILITY.unavailable, AVAILABILITY.highlightedUnavailable:
                availabilityStatus = AVAILABILITY.unavailable
                break
            default:
                availabilityStatus = AVAILABILITY.busy
                break
            }
            
            var json:[String:Any] = ["slot_timming":slots[indexArray[i]].slotTiming]
            json["available_status"] = availabilityStatus!
            
            jsonArray.append(json)
        }
        return jsonArray
    }
    
    func getCopyRepeatJson(indexArray:[Int]) -> [Any] {
        var jsonArray = [Any]()
        for i in (0..<indexArray.count) {
            let date = self.getDateSlotForRepeatPopup(indexPath: indexArray[i], format: "yyyy-MM-dd")
            let json = ["local_date_time":date] as [String : Any]
            jsonArray.append(json)
        }
        return jsonArray
    }
    
    /*------------------ Set Time Slots ---------------*/
    func getTimeSlot(indexPath:Int, totalTimeSlots:Int) -> String {
        let slotMinutes = 60 * 24 / totalTimeSlots
        let slotMinutesAtIndex = slotMinutes * indexPath
        let slotTimeMinutesPart = slotMinutesAtIndex % 60
        let slotTimeHoursPart = slotMinutesAtIndex / 60
        let slotTime = "\(slotTimeHoursPart):\(slotTimeMinutesPart)"
        return Auxillary.convert24HoursTimeInto12HoursFormat(timeFormat24: slotTime)
    }
    
    /*---------------- Set Date Slots ---------------*/
    func getDateSlot(indexPath:Int) -> String {
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
        let currentDate = Auxillary.currentDate()
        let newDate:Date = (calendar as NSCalendar).date(byAdding: .day, value: indexPath, to: currentDate, options: [])!
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!//NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation:"UTC")
        dateFormatter.dateFormat = "dd\nEEE"
        return dateFormatter.string(from: newDate)
    }
    
    func getDateSlotForEditSchedule(indexPath:Int) -> String {
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
        let currentDate = Auxillary.currentDate()
        let newDate:Date = (calendar as NSCalendar).date(byAdding: .day, value: indexPath, to: currentDate, options: [])!
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!//NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation:"UTC")
        dateFormatter.dateFormat = "MMM dd, yyyy"
        return dateFormatter.string(from: newDate)
    }
    
    func getDateSlotForRepeatPopup(indexPath:Int, format:String) -> String {
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
        let currentDate = Auxillary.currentDate()
        let newDate:Date = (calendar as NSCalendar).date(byAdding: .day, value: indexPath, to: currentDate, options: [])!
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!//NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation:"UTC")
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: newDate)
    }
    
    /*------------------ Set Month Title ------------------*/
    func getMonthTitle(maximumDays:Int) -> String {
        let currentDate = Auxillary.currentDate()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!
        dateFormatter.dateFormat = "MMMM"
        let currentMonth = dateFormatter.string(from: currentDate)
        if (Auxillary.getNumberOfDaysInCurrentMonth(currentDate) - Auxillary.getCurrentDay(currentDate)) > maximumDays {
            let currentYear = Auxillary.getCurrentYear(currentDate, format: "yyyy")
            return "\(currentMonth) \(currentYear)"
        } else {
            let currentYear = Auxillary.getCurrentYear(currentDate, format: "yy")
            let nextMonthYear = Auxillary.getNextMonthYear(currentDate: currentDate)
            dateFormatter.dateFormat = "MMM"
            let currentMonth = dateFormatter.string(from: currentDate)
            let nextMonth = Auxillary.getNextMonth(currentDate: currentDate, format: "MMM")
            return "\(currentMonth)'\(currentYear) - \(nextMonth)'\(nextMonthYear)"
        }
    }
    
    /*------------ Get availability Status -----------------*/
    func getAvailabilityStatus(indexPath:Int, maximumDays:Int, numberOfDots:Int, row:Int) -> Int {
        let day = indexPath % maximumDays
        let timeSlot = (indexPath / maximumDays) * numberOfDots + row
        if(Singleton.sharedInstance.availabilityData.count > 0 && day < Singleton.sharedInstance.availabilityData.count) {
            if Singleton.sharedInstance.availabilityData[day].slots.count > timeSlot {
                let availability = Singleton.sharedInstance.availabilityData[day].slots[timeSlot]
                return availability.availabilityStatus!
            }
        }
        return AVAILABILITY.available
    }
    
    func isGivenDayContainsAnyBusyStatus(day:Int) -> Bool {
        guard day < Singleton.sharedInstance.availabilityData.count else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
            return false
        }
        
        if(Singleton.sharedInstance.availabilityData.count > 0) {
            let slots = Singleton.sharedInstance.availabilityData[day].slots
            for slot in slots {
                if slot.availabilityStatus == AVAILABILITY.busy {
                    return true
                }
            }
        }
        return false
    }
    
    /*---------------- Get day blocked status ---------------*/
    func isGivenDayBlocked(day:Int) -> Bool {
        
        if(Singleton.sharedInstance.availabilityData.count > 0 && day < Singleton.sharedInstance.availabilityData.count) {
            if Singleton.sharedInstance.availabilityData[day].isDayBlocked == 1 {
                return true
            }
        }
        return false
    }
    
    /*------------ Get availability Status -----------------*/
    func getAvailabilityStatusForParticularDay(indexPath:Int, day:Int) -> Int {
        if(Singleton.sharedInstance.availabilityData.count > 0 && day < Singleton.sharedInstance.availabilityData.count) {
            if Singleton.sharedInstance.availabilityData[day].slots.count > indexPath {
                let availability = Singleton.sharedInstance.availabilityData[day].slots[indexPath]
                return availability.availabilityStatus!
            }
        }
        return AVAILABILITY.available
    }

    /*----------- Set Availability Status -------------*/
    func setAvailabilityStatusForParticularDay(indexPath:Int, day:Int, updatedStatus:Int) {
        if(Singleton.sharedInstance.availabilityData.count > 0 && day < Singleton.sharedInstance.availabilityData.count) {
            if Singleton.sharedInstance.availabilityData[day].slots.count > indexPath {
                Singleton.sharedInstance.availabilityData[day].slots[indexPath].availabilityStatus = updatedStatus
            }
        }
    }
    
    /*-------------- Reset availability Status -----------------*/
    func updateLastAvailabilityStatus(updatedStatusArray:[Int], day:Int) {
        let slots = Singleton.sharedInstance.availabilityData[day].slots
        for i in (0..<updatedStatusArray.count) {
            if slots[updatedStatusArray[i]].availabilityStatus == AVAILABILITY.highlightedAvailable {
                slots[updatedStatusArray[i]].availabilityStatus = AVAILABILITY.available
            } else if slots[updatedStatusArray[i]].availabilityStatus == AVAILABILITY.highlightedUnavailable {
                slots[updatedStatusArray[i]].availabilityStatus = AVAILABILITY.unavailable
            }
            slots[updatedStatusArray[i]].lastAvailabilityStatus = slots[updatedStatusArray[i]].availabilityStatus
        }
    }
     
    /*-------------- Reset availability Status -----------------*/
    func resetAvailabilityStatus(updatedStatusArray:[Int], day:Int) {
        let slots = Singleton.sharedInstance.availabilityData[day].slots
        for i in (0..<updatedStatusArray.count) {
            slots[updatedStatusArray[i]].availabilityStatus = slots[updatedStatusArray[i]].lastAvailabilityStatus
        }
    }
    
    /*-------------- Reset availability Status to unavailable -----------------*/
    func resetAvailabilityStatusToUnavailable(day:Int) {
        let slots = Singleton.sharedInstance.availabilityData[day].slots
        for i in (0..<slots.count) {
            if slots[i].availabilityStatus != AVAILABILITY.busy {
                slots[i].availabilityStatus = AVAILABILITY.unavailable
                slots[i].lastAvailabilityStatus = AVAILABILITY.unavailable
            }
        }
    }
    
    /*-------------- Reset availability Status to available -----------------*/
    func resetAvailabilityStatusToAvailable(day:Int) {
        let slots = Singleton.sharedInstance.availabilityData[day].slots
        for i in (0..<slots.count) {
            slots[i].availabilityStatus = AVAILABILITY.available
            slots[i].lastAvailabilityStatus = AVAILABILITY.available
        }
    }

    
    /*---------------- Check for combine view ----------------*/
    func isTopMarginViewEligibleForCombineView(indexPath:Int, maximumDays:Int, currentNodeStatus:Int) -> Bool {
        if indexPath >= maximumDays {
            let previousNodeStatus = self.getAvailabilityStatus(indexPath: indexPath - maximumDays, maximumDays: maximumDays, numberOfDots:4, row:0)
            if currentNodeStatus == previousNodeStatus {
                return true
            }
        }
        return false
    }
    
    func isBottomMarginViewEligibleForCombineView(indexPath:Int, maximumDays:Int, currentNodeStatus:Int, maximumSlots:Int) -> Bool {
        if indexPath < (maximumSlots * maximumDays) - maximumDays {
            let nextNodeStatus = self.getAvailabilityStatus(indexPath: indexPath + maximumDays, maximumDays: maximumDays, numberOfDots:4, row:0)
            if currentNodeStatus == nextNodeStatus {
                return true
            }
        }
        return false
    }
    
    func currentStatusColor(currentNodeStatus:Int) -> UIColor {
        let color:UIColor!
        switch currentNodeStatus {
        case AVAILABILITY.unavailable:
            color = COLOR.availabilityUnavailableColor
            break
        case AVAILABILITY.busy:
            color = COLOR.availabilityBusyColor
            break
        case AVAILABILITY.highlightedAvailable:
            color = COLOR.availabilityHighlightedFreeColor
            break
        case AVAILABILITY.highlightedUnavailable:
            color = COLOR.availabilityHighlightedUnavailableColor
            break
        default:
            color = COLOR.availabilityFreeColor
            break
        }
        return color
    }
    
    /*-------------------- Set Copy/Repeat Description label ---------------*/
    func setDescriptionLabel(selectedDays:[Int],forever:Bool, weekday:Int) -> String {
        var text = "\(TEXT.REPEAT_THIS_DAY_ON) "
        if selectedDays.count > 0 {
            for day in selectedDays {
                if selectedDays.count > 1 {
                    if day != selectedDays.last {
                        if day == selectedDays.first {
                            text = text + self.getDateSlotForRepeatPopup(indexPath: day, format: "EEE")
                        } else {
                            text = "\(text), \(self.getDateSlotForRepeatPopup(indexPath: day, format: "EEE"))"
                        }
                    } else {
                        text = "\(text) & \(self.getDateSlotForRepeatPopup(indexPath: day, format: "EEE"))"
                    }
                } else {
                    text = text + self.getDateSlotForRepeatPopup(indexPath: day, format: "EEE")
                }
            }
            
            if forever == false {
                text = "\(text) \(TEXT.TILL) \(self.getDateSlotForRepeatPopup(indexPath: 6 + weekday, format: "MMM dd"))"
            } else {
                text = "\(text) \(TEXT.EVERYDAY)"
            }
            return text
            
        } else {
            return ""
        }
    }
}
