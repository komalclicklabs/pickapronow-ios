//
//  KeyWordSectionHeader.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 4/6/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol KeyWordSectionHeaderDelegate{
    func setApplyButton()
    func hideApplyButton(textField:UITextField)
}

class KeyWordSectionHeader: UIView, UITextFieldDelegate {

    @IBOutlet weak var seperationView: UIView!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var keywordTextField: UITextField!
    
    var delegate : KeyWordSectionHeaderDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.searchIcon.image = #imageLiteral(resourceName: "searchIcon").withRenderingMode(.alwaysTemplate)
        self.searchIcon.tintColor = COLOR.TEXT_COLOR
        let attributes = [
            NSForegroundColorAttributeName: COLOR.TEXT_COLOR,
            NSFontAttributeName :  UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        ]
        self.keywordTextField.attributedPlaceholder = NSAttributedString(string: TEXT.KEYWORD, attributes: attributes )
        self.seperationView.backgroundColor = COLOR.LINE_COLOR
        self.keywordTextField.textColor = COLOR.TEXT_COLOR
        self.keywordTextField.delegate = self
        self.keywordTextField.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.timer)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.delegate.setApplyButton()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.delegate.hideApplyButton(textField: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var updatedString:NSString = "\(textField.text!)" as NSString
        updatedString = updatedString.replacingCharacters(in: range, with: string) as NSString
        Singleton.sharedInstance.searchedText = updatedString as String
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        Singleton.sharedInstance.searchedText = ""
        return true
    }

}
