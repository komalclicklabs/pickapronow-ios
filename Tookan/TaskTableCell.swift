//
//  TaskTableCell.swift
//  Tookan
//
//  Created by Rakesh Kumar on 4/4/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class TaskTableCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleField: UITextField!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var underline: UIView!
    @IBOutlet weak var tableCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
