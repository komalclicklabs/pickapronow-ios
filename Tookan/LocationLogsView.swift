//
//  LocationLogsView.swift
//  Tookan
//
//  Created by Rakesh Kumar on 4/18/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class LocationLogsView: UIViewController, NavigationDelegate {

    @IBOutlet weak var logsTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        if let array = UserDefaults.standard.value(forKey: "SavedLocations") as? [Any] {
           self.logsTextView.text = "\(array)"
        } else {
            self.logsTextView.text = "Nil"
        }
        
        let bottom = self.logsTextView.contentSize.height
        self.logsTextView.setContentOffset(CGPoint(x: 0, y: bottom), animated: true) // Scrolls to end
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Navigation Bar
    func setNavigationBar() {
        let navigation = UINib(nibName: "navigationBar", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigation.titleLabel.text = "Location Logs"
        self.view.addSubview(navigation)

        navigation.editButton.isHidden = false
        navigation.editButton.setTitle("Map", for: UIControlState())
        navigation.editButton.addTarget(self, action: #selector(LocationLogsView.mapAction), for: UIControlEvents.touchUpInside)
    }
    
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func mapAction() {
        let logController = storyboard?.instantiateViewController(withIdentifier: "MapTrackingController") as! MapTrackingController
        self.navigationController?.pushViewController(logController, animated: true)
    }

}
