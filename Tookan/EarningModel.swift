//
//  EarningModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 19/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class EarningModel: NSObject {

    func getAttributedText(weekTask:WEEK_JOBS) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        var dateTime:String!
        dateTime = weekTask.time
        attributedString = NSMutableAttributedString(string: "\(dateTime!) - \(weekTask.job_type)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])

        let status = Singleton.sharedInstance.getAttributedStatusText(jobStatus:weekTask.job_status, appOptionalField: 0)
        attributedString.append(status)
        
        let jobId = NSMutableAttributedString(string: "\nID:\(weekTask.job_id)", attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)!])
        attributedString.append(jobId)
        
        let duration = NSMutableAttributedString(string: "\n\(weekTask.distance) - \(weekTask.duration)", attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.medium)!])
        attributedString.append(duration)
        return attributedString
    }
    
    func getAttributedPayout(totalPayout:String) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        attributedString = NSMutableAttributedString(string: "\(TEXT.TOTAL_PAYOUT)  ", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!])
        
        let payout = NSMutableAttributedString(string: "\(totalPayout)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.twenty)!])
        attributedString.append(payout)
        return attributedString
    }
    
    func getAttributedDetailText(field:FORMULA_FIELDS) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        attributedString = NSMutableAttributedString(string: "\(field.display_name)", attributes: [NSForegroundColorAttributeName:COLOR.LIGHT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)!])

        var sumText = "-"
        var color = COLOR.TEXT_COLOR
        if field.sum.length > 0 {
            sumText = field.sum
        }
        
        if field.type == 2 {
            color = COLOR.themeForegroundColor
        }
        
        let sum = NSMutableAttributedString(string: "\n\(sumText)", attributes: [NSForegroundColorAttributeName:color, NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)!])
        attributedString.append(sum)
        
        return attributedString
    }
    
    func getAttributedTotalText(fieldLabel:String,fieldValue:String) -> NSMutableAttributedString {
        var attributedString:NSMutableAttributedString!
        attributedString = NSMutableAttributedString(string: "\(TEXT.TOTAL) \(fieldLabel)", attributes: [NSForegroundColorAttributeName:COLOR.TEXT_COLOR, NSFontAttributeName:UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)!])
        
        let sum = NSMutableAttributedString(string: "\n\(fieldValue)", attributes: [NSForegroundColorAttributeName:COLOR.themeForegroundColor, NSFontAttributeName:UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.regular)!])
        attributedString.append(sum)
        
        return attributedString
    }
    
    
}



