//
//  ErrorView.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 18/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

protocol ErrorDelegate {
    func removeErrorView()
}
class ErrorView: UIView {

    @IBOutlet var statusIcon: UIImageView!
    @IBOutlet var errorMessage: UILabel!
    var delegate:ErrorDelegate!
    
    override func awakeFromNib() {
        errorMessage.textColor = UIColor.white
        errorMessage.font = UIFont(name: UIFont().MontserratLight, size: 14)
        
        self.statusIcon.image = #imageLiteral(resourceName: "cancel_notes").withRenderingMode(.alwaysTemplate)
        self.statusIcon.tintColor = UIColor.white
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideErrorMessage))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    func setErrorMessage(message:String, isError:ERROR_TYPE) {
        switch isError {
        case .error:
            self.backgroundColor = COLOR.ERROR_COLOR
        case .success:
            self.backgroundColor = COLOR.SUCCESS_COLOR
        case .message:
            self.backgroundColor = COLOR.MESSAGE_COLOR
        }
        
        self.errorMessage.text = message
        let size = message.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 57, font: UIFont(name: UIFont().MontserratLight, size: 14)!)
        print(size)
        if size.height > 14 {
            self.frame.size.height = (HEIGHT.errorMessageHeight - 13) + size.height
        }
    
        self.showErrorMessage()
    }
    
    func showErrorMessage() {
        UIView.animate(withDuration: 0.3, animations: {
            self.transform = CGAffineTransform(translationX: 0, y: -(self.frame.height + Singleton.sharedInstance.keyboardSize.height))
            }, completion: { finished in
                self.perform(#selector(self.hideErrorMessage), with: nil, afterDelay: 3.0)
        })
    }
    
    func hideErrorMessage() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
            }, completion: { finished in
            self.delegate.removeErrorView()
        })
    }
    
    func translateToBottom() {
        UIView.animate(withDuration: 0.5, delay: 0.1, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.transform = CGAffineTransform(translationX: 0, y: -(self.frame.height))
        }, completion: { finished in
           // self.delegate.removeErrorView()
        })
    }
    
    func translateToTop() {
        UIView.animate(withDuration: 0.3, animations: {
            self.transform = CGAffineTransform(translationX: 0, y: -(self.frame.height + Singleton.sharedInstance.keyboardSize.height))
        }, completion: { finished in
            //self.perform(#selector(self.hideErrorMessage), with: nil, afterDelay: 3.0)
        })
    }
}
