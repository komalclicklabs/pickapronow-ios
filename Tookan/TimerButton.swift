//
//  TimerButton.swift
//  Tookan
//
//  Created by cl-macmini-45 on 20/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
protocol TimerDelegate {
    func timerFinished(jobId: String)
}


class TimerButton: UIView {

    @IBOutlet var timerLabel: CountdownLabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var trailingConstraint: NSLayoutConstraint!

    var jobId:String!
    
    var delegate:TimerDelegate!
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        /*============= Title ==============*/
        self.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.titleLabel.textColor = UIColor.white
        
        /*============= Timer ================*/
        self.timerLabel.font = UIFont(name: UIFont().BebasNeueRegular, size: FONT_SIZE.timer)
        self.timerLabel.textColor = UIColor.white
        self.timerLabel.countdownDelegate = self
        self.timerLabel.animationType = .evaporate
        self.timerLabel.timeFormat = "mm:ss"
    }
    
    func setTimer(seconds:Int) {
        if seconds > 0 {
            self.timerLabel.addTime(TimeInterval(seconds))
            self.timerLabel.start()
        } else {
            self.timerLabel.isHidden = true
            self.timerLabel.text = ""
            self.trailingConstraint.constant = 0.0
        }
    }
    
    func setTitle(title:String) {
        self.titleLabel.text  = title
    }
}

extension TimerButton:CountdownLabelDelegate {
    func countdownFinished() {
        print("finished")
        print(self.timerLabel.tag)
        delegate.timerFinished(jobId: self.jobId)
    }
    
    func countdownCancelled() {
        print("canceled")
    }
}
