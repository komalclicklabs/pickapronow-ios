//
//  EarningDetailController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 21/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class EarningDetailController: UIViewController {

    @IBOutlet var earningDetailTable: UITableView!
    
    var taskJob:WEEK_JOBS!
    var formulaFields = [FORMULA_FIELDS]()
    let model = EarningModel()
    let sectionHeaderHeight:CGFloat = 36.0
    var forCompletedTask = false
    var forEarningDetails:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableView()
        self.setNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if forCompletedTask == false {
            self.getEarningsDetails()
        }
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
            if (mapStyle == MAP_STYLE.dark){
                UIApplication.shared.statusBarStyle = .lightContent
            } else {
                UIApplication.shared.statusBarStyle = .default
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTableView() {
        self.earningDetailTable.delegate = self
        self.earningDetailTable.dataSource = self
        self.earningDetailTable.register(UINib(nibName: NIB_NAME.earningCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.earningCell)
        self.earningDetailTable.register(UINib(nibName: NIB_NAME.earningDetailCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.earningDetailCell)
        self.earningDetailTable.register(UINib(nibName: NIB_NAME.weekEarningDetailCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.weekEarningDetailCell)
        self.earningDetailTable.rowHeight = UITableViewAutomaticDimension
        self.earningDetailTable.layer.cornerRadius = 15.0
        self.earningDetailTable.layer.masksToBounds = true
        self.earningDetailTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func tableBackgroundView(_ frame:CGRect) -> UIView {
        let backgroundView = UIView()
        backgroundView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        let label = UILabel(frame: CGRect(x: 0,y: frame.height/2 - HEIGHT.navigationHeight, width: frame.width, height: 60))
        label.text = ERROR_MESSAGE.NO_DATA_FOUND
        label.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        backgroundView.addSubview(label)
        return backgroundView
    }
    
    
    func getEarningsDetails() {
        if IJReachability.isConnectedToNetwork() == false {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        } else {
            let tableBackgroundView = UIView(frame: self.earningDetailTable.frame)
            tableBackgroundView.backgroundColor = UIColor.white
            let rect = self.earningDetailTable.rectForRow(at: IndexPath(row: 0, section: 0))
            let loaderView = UINib(nibName: NIB_NAME.loadingTaskView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! LoadingTaskView
            loaderView.frame = CGRect(x: 0, y: rect.height + 70, width: rect.width, height: self.earningDetailTable.frame.height)
            tableBackgroundView.addSubview(loaderView)
            self.earningDetailTable.backgroundView = tableBackgroundView
            var params:[String : Any] = ["access_token":Singleton.sharedInstance.getAccessToken()]
            params["job_id"] = self.taskJob.job_id
            
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.fetch_fleet_job_report, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    print(response)
                    if isSucceeded == true {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            if let responseData = response["data"] as? [String:Any] {
                                if let fields = responseData["formula_fields"] as? [[String:Any]] {
                                    for field in fields {
                                        let formulaField = FORMULA_FIELDS(json: field)
                                        self.formulaFields.append(formulaField)
                                    }
                                }
                                self.earningDetailTable.reloadData()
                            }
                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.showInvalidAccessTokenPopup(response["message"] as! String)
                            break
                        default:
                            break
                        }
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                    self.dataSetWhenNoDataFound()
                }
            })
        }
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                Auxillary.logoutFromDevice()
                NotificationCenter.default.removeObserver(self)
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    func dataSetWhenNoDataFound(){
        if(self.formulaFields.count == 0) {
            self.earningDetailTable.backgroundView = self.tableBackgroundView(self.earningDetailTable.frame)
        } else {
            self.earningDetailTable.backgroundView = nil
        }
    }
}

extension EarningDetailController:UITableViewDelegate, UITableViewDataSource {
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if forCompletedTask == false {
                return 1
            } else {
                return 0
            }
        } else {
            return self.formulaFields.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if forCompletedTask == false {
                return UITableViewAutomaticDimension
            } else {
                if indexPath.row == 0 {
                    return 0.01
                }
                return UITableViewAutomaticDimension
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width - 10, height: self.sectionHeaderHeight))
            headerView.backgroundColor = COLOR.TABLE_HEADER_COLOR
        
            let headerLabel = UILabel(frame: CGRect(x: 15, y: 0, width: SCREEN_SIZE.width - 25, height: self.sectionHeaderHeight))
            headerLabel.textColor = COLOR.LIGHT_COLOR
            headerLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
            if self.forEarningDetails == true {
                headerLabel.text = "\(TEXT.EARNINGS_CAPS) \(TEXT.DETAILS)"
            } else {
                headerLabel.text = "\(TEXT.PRICING_CAP) \(TEXT.DETAILS)"
            }
            headerLabel.setLetterSpacing(value: 1.8)
            headerView.addSubview(headerLabel)
        
            let topLine = Auxillary.drawSingleLine(0, yOrigin: 0, lineWidth: headerView.frame.width, lineHeight: 0.5)
            headerView.addSubview(topLine)
            
            let line = Auxillary.drawSingleLine(0, yOrigin: headerView.frame.height - 1, lineWidth: headerView.frame.width, lineHeight: 0.5)
            headerView.addSubview(line)
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width - 10, height: self.sectionHeaderHeight))
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return sectionHeaderHeight
        } else {
            return 0.01
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
           /* let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.weekEarningDetailCell) as! WeekEarningDetailCell
            cell.totalPayoutLabel.attributedText = self.model.getAttributedPayout(totalPayout: "5000")
            cell.totalTaskLabel.attributedText = self.model.getAttributedTotalText(fieldLabel: TEXT.ORDERS, fieldValue: "10")
            cell.totalDistanceLabel.attributedText = self.model.getAttributedTotalText(fieldLabel: TEXT.DISTANCE_SMALL, fieldValue: "66 KM")
            cell.totalTimeLabel.attributedText = self.model.getAttributedTotalText(fieldLabel: TEXT.TIME, fieldValue: "12h 50m")
            return cell*/
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.earningCell) as! EarningCell
            cell.dot.isHidden = true
            cell.topLine.isHidden = true
            cell.bottomLine.isHidden = true
            cell.rightArrow.isHidden = true
            cell.firstLine.text = taskJob.date
            cell.amountLabel.text = "\(taskJob.total)"
            cell.secondLine.attributedText = self.model.getAttributedText(weekTask: taskJob)
            cell.leadingConstraint.constant = 20.0
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.earningDetailCell) as! EarningDetailCell
            cell.detailLabel?.attributedText = self.model.getAttributedDetailText(field: self.formulaFields[indexPath.row])
            cell.detailLabel?.numberOfLines = 0
            return cell
        }
    }
}

//MARK: Navigation Bar
extension EarningDetailController:NavigationDelegate {
    func setNavigationBar() {
        let navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        if forCompletedTask == false {
            navigation.titleLabel.text = "\(self.taskJob.job_id)"
        } else {
            navigation.titleLabel.text = "\(TEXT.ORDER) \(TEXT.COMPLETED)"
        }
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.view.addSubview(navigation)
    }
    
    func backAction() {
        if forCompletedTask == false {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func editAction() {
        
    }
}
