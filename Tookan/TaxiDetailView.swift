//
//  TaxiDetailView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 15/05/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol TaxiViewDelegate {
    func slideButtonAction()
    func navigationAction()
    func callAction()
    func openMessageController()
    func delegateForAddressController()
}

class TaxiDetailView: UIView {

    @IBOutlet var detailsTable: UITableView!
    @IBOutlet var slideButton: UIButton!
    
    var model:TaxiModel?
    var delegate:TaxiViewDelegate?
    var showListCell = false
    
    let headerHeight:CGFloat = 28.0
    let footerHeight:CGFloat = 15.0
    
    override func awakeFromNib() {
        
    }
    
    func setDetailTable() {
        self.model?.setSectionsAndRows()
        self.detailsTable.delegate = self
        self.detailsTable.dataSource = self

        self.detailsTable.register(UINib(nibName: NIB_NAME.jobDetailCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobDetailCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.userDetailCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.userDetailCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.jobListCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobListCell)
        
        self.detailsTable.estimatedRowHeight = UITableViewAutomaticDimension
        self.detailsTable.rowHeight = UITableViewAutomaticDimension
        self.detailsTable.bounces = false
    }
    
    
    @IBAction func slideAction(_ sender: Any) {
        self.delegate?.slideButtonAction()
    }
    
    func tappedOnMapNavigation() {
        delegate?.navigationAction()
    }
    
    func callAction() {
        delegate?.callAction()
    }
    
    func showMessageController() {
        self.delegate?.openMessageController()
    }
}

extension TaxiDetailView:UITableViewDelegate, UITableViewDataSource {
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.model!.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model!.detailRows.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch (self.model?.sections[section])! {
        case .jobDetails:
            return 0.01
        default:
            return headerHeight
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch (self.model?.sections[section])! {
        case .jobDetails:
            return footerHeight
        default:
            return footerHeight
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.section < (self.model?.sections.count)! else {
            return UITableViewCell()
        }
        
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return UITableViewCell()
        }
        
        switch (self.model?.sections[indexPath.section])! {
        case .jobDetails:
            switch (self.model?.detailRows[indexPath.row])! {
            case .job:
                if showListCell == false {
                    let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobDetailCell) as! JobDetailCell
                    cell.detailLabel.text = self.model?.getJobTime(jobDetails: Singleton.sharedInstance.selectedTaskDetails)
                    cell.jobStatus.attributedText = Singleton.sharedInstance.getAttributedStatusText(jobStatus:Singleton.sharedInstance.selectedTaskDetails.jobStatus, appOptionalField: Singleton.sharedInstance.selectedTaskDetails.acceptOptionalField.value!)
                    cell.icon.image = #imageLiteral(resourceName: "time_detail")
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobListCell) as! JobListCell
                    let jobModel = JobModel()
                    print(Singleton.sharedInstance.selectedTaskDetails)
                    cell.firstLine.attributedText = jobModel.setFirstLine(jobDetails: Singleton.sharedInstance.selectedTaskDetails, forNewTask:false)
                    cell.secondLine.attributedText = jobModel.setSecondLine(jobDetails: Singleton.sharedInstance.selectedTaskDetails)
                    cell.dot.backgroundColor = UIColor.black
                    cell.dot.layer.cornerRadius = 4.5
                    cell.patternLine.isHidden = true
                    //cell.setDotView(indexPath: indexPath, filteredTaskListArray: filteredTasksListArray, isRelatedTask: false)
                    return cell
                }
            case .user:
                let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.userDetailCell) as! UserDetailCell
                cell.dot.isHidden = true
                cell.topPattern.isHidden = true
                cell.bottomPattern.isHidden = true
                cell.icon.isHidden = false
                cell.icon.image = #imageLiteral(resourceName: "user")
                cell.detailLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
                cell.line.backgroundColor = COLOR.LINE_COLOR
                cell.icon.tintColor = UIColor.black
                cell.detailLabel.textColor = UIColor.black
                let userName = self.model?.getCustomerName(Singleton.sharedInstance.selectedTaskDetails)
                if (userName?.length)! > 0 {
                    cell.detailLabel.text = userName
                } else {
                    cell.detailLabel.text = " - "
                }
                
                if Singleton.sharedInstance.selectedTaskDetails.customerPhone.length > 0 {
                    cell.actionButton.isHidden = false
                    cell.actionButton.layer.cornerRadius = cell.actionButton.frame.height / 2
                    cell.actionButton.layer.masksToBounds = true
                    cell.actionButton.backgroundColor = COLOR.CALL_BUTTON_COLOR
                    cell.actionButton.setImage(#imageLiteral(resourceName: "call_detail"), for: .normal)
                    cell.actionButton.removeTarget(self, action: #selector(self.tappedOnMapNavigation), for: .touchUpInside)
                    cell.actionButton.addTarget(self, action: #selector(self.callAction), for: .touchUpInside)
                    
                    cell.messageButton.layer.cornerRadius = cell.messageButton.frame.height / 2
                    cell.messageButton.layer.masksToBounds = true
                    cell.messageButton.backgroundColor = COLOR.CALL_BUTTON_COLOR
                    cell.messageButton.setImage(#imageLiteral(resourceName: "message").withRenderingMode(.alwaysTemplate), for: .normal)
                    cell.messageButton.tintColor = UIColor.white
                    cell.messageButton.addTarget(self, action: #selector(self.showMessageController), for: .touchUpInside)
                    
                    cell.trainlingConstraint.constant = cell.trailingConstraintWithBothButtons
                } else {
                    cell.actionButton.isHidden = true
                    cell.messageButton.isHidden = true
                }
                return cell
            case .location:
                let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.userDetailCell) as! UserDetailCell
                cell.line.backgroundColor = COLOR.LINE_COLOR
                cell.icon.image = #imageLiteral(resourceName: "blackPin")
                cell.detailLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
                cell.icon.tintColor = UIColor.black
                cell.detailLabel.textColor = UIColor.black
                cell.messageButton.isHidden = true
                cell.trainlingConstraint.constant = cell.trailingConstraintWithSingleButton

                let address = self.model?.getAddress(Singleton.sharedInstance.selectedTaskDetails)
                if (address?.length)! > 0 {
                    cell.detailLabel.text = address
                } else {
                    cell.detailLabel.text = " - "
                }
                
                //if self.model?.detailRows.count == 3 {
                    cell.actionButton.isHidden = false
                    cell.actionButton.layer.cornerRadius = cell.actionButton.frame.height / 2
                    cell.actionButton.layer.masksToBounds = true
                    cell.actionButton.backgroundColor = COLOR.themeForegroundColor
                    cell.actionButton.setImage(#imageLiteral(resourceName: "getDirectionsSmall"), for: .normal)
                    cell.actionButton.removeTarget(self, action: #selector(self.callAction), for: .touchUpInside)
                    cell.actionButton.addTarget(self, action: #selector(self.tappedOnMapNavigation), for: .touchUpInside)
                    
                    cell.dot.isHidden = true
                    cell.topPattern.isHidden = true
                    cell.bottomPattern.isHidden = true
                    cell.icon.isHidden = false
//                } else {
//                    cell.actionButton.isHidden = true
//                    cell.dot.isHidden = false
//                    cell.topPattern.isHidden = true
//                    cell.bottomPattern.isHidden = false
//                    cell.icon.isHidden = true
//                }
                return cell
                
            case .destinationLocation:
                let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.userDetailCell) as! UserDetailCell
                cell.actionButton.isHidden = true
              //  cell.actionButton.layer.cornerRadius = cell.actionButton.frame.height / 2
               // cell.actionButton.layer.masksToBounds = true
               // cell.actionButton.backgroundColor = COLOR.themeForegroundColor
               // cell.actionButton.setImage(#imageLiteral(resourceName: "getDirectionsSmall"), for: .normal)
               // cell.actionButton.removeTarget(self, action: #selector(self.callAction), for: .touchUpInside)
               // cell.actionButton.addTarget(self, action: #selector(self.tappedOnMapNavigation), for: .touchUpInside)
                cell.messageButton.isHidden = true
                cell.trainlingConstraint.constant = cell.trailingConstraintWithSingleButton

                cell.icon.image = #imageLiteral(resourceName: "blackPin").withRenderingMode(.alwaysTemplate)
                cell.detailLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
                let address = self.model?.destinationAddress
                if (address?.length)! > 0 {
                    cell.detailLabel.text = address
                   // cell.actionButton.isHidden = false
                    cell.icon.tintColor = UIColor.black
                    cell.detailLabel.textColor = UIColor.black
                    cell.line.backgroundColor = COLOR.LINE_COLOR
                } else {
                    cell.detailLabel.text = TEXT.ENTER_DESTINATION
                   // cell.actionButton.isHidden = true
                    cell.icon.tintColor = UIColor.red
                    cell.detailLabel.textColor = UIColor.red
                    cell.line.backgroundColor = UIColor.red
                }
                
                cell.dot.isHidden = true
                cell.topPattern.isHidden = false
                cell.bottomPattern.isHidden = true
                cell.icon.isHidden = false
                return cell
            default:
                return UITableViewCell()
            }
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UINib(nibName: NIB_NAME.detailHeaderView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! DetailHeaderView
        headerView.backgroundColor = COLOR.TABLE_HEADER_COLOR
        headerView.addButton.transform = CGAffineTransform.identity
        headerView.addButton.isUserInteractionEnabled = true
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: footerHeight))
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (self.model?.sections[indexPath.section])! {
        case .jobDetails:
            switch (self.model?.detailRows[indexPath.row])! {
            case .destinationLocation:
                self.delegate?.delegateForAddressController()
                break
            default:
                break
            }
        default:
            break
        }
    }

}
