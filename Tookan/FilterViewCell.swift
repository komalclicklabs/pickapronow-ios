//
//  FilterViewCell.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 4/5/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class FilterViewCell: UITableViewCell {


    @IBOutlet weak var filterNumber: UILabel!
    @IBOutlet weak var filterName: UILabel!
    @IBOutlet weak var filterCellIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.filterCellIcon.isHidden = true
        self.filterCellIcon.image = #imageLiteral(resourceName: "smallCheckmark")
        
        self.filterName.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.filterName.textColor = COLOR.TEXT_COLOR
        self.filterNumber.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.filterNumber.textColor = COLOR.TEXT_COLOR
        self.filterNumber.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
