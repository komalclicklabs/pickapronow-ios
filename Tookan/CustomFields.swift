

//
//  CustomFields.swift
//  Tookan
//
//  Created by LOGIO on 10/5/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation

enum ImageAttributeType:Int {
    case both = 0
    case camera
    case gallery
}

class Caption: NSObject,NSCoding {
    var image_url:String? = ""
    var caption_data:String? = ""
    required init(coder aDecoder:NSCoder){
        image_url = aDecoder.decodeObject(forKey: "image_url") as? String
        caption_data = aDecoder.decodeObject(forKey: "caption_data") as? String
    }
    
    func encode(with aCoder: NSCoder){
        aCoder.encode(image_url, forKey: "image_url")
        aCoder.encode(caption_data, forKey: "caption_data")
    }
    
    init(json: [String:Any]) {
        if let imageURL = json["image_url"] as? String{
            self.image_url = imageURL
        }
        
        if let captionText = json["caption_data"] as? String{
            self.caption_data = captionText
        }
    }
}

class CustomFields: NSObject, NSCoding {
    var appSide = String()
    var data = String()
    var dataType = String()
    var label = String()
    var displayLabelName = String()
    var required:Bool!
    var value:Bool!
    var dropdown = [String]()//NSMutableArray()
    var fleetData = String()
    var input = String()
    var imageArray = NSMutableArray()
    var checklistDictionary = NSMutableArray()
    var checklistArray = [ChecklistData]()
    var taskTableData = TaskTableData(json: [:])
    var templateName = String()
    var sync:Int? = 1
    var timestamp:String? = ""
    var showChecklistRows = false
    var captionArray:[Caption]? = [Caption]()
    var attribute:Int? = 0
    
    required init(coder aDecoder: NSCoder) {
        appSide = aDecoder.decodeObject(forKey: "appSide") as! String
        data = aDecoder.decodeObject(forKey: "data") as! String
        dataType = aDecoder.decodeObject(forKey: "dataType") as! String
        label = aDecoder.decodeObject(forKey: "label") as! String
        required = aDecoder.decodeObject(forKey: "required") as! Bool
        value = aDecoder.decodeObject(forKey: "value") as! Bool
        dropdown = aDecoder.decodeObject(forKey: "dropdown") as! [String]
        fleetData = aDecoder.decodeObject(forKey: "fleetData") as! String
        input = aDecoder.decodeObject(forKey: "input") as! String
        imageArray = aDecoder.decodeObject(forKey: "imageArray") as! NSMutableArray
        checklistDictionary = aDecoder.decodeObject(forKey: "checklistDictionary") as! NSMutableArray
        checklistArray = aDecoder.decodeObject(forKey: "checklistArray") as! [ChecklistData]
        taskTableData = aDecoder.decodeObject(forKey: "taskTableData") as! TaskTableData
        templateName = aDecoder.decodeObject(forKey: "templateName") as! String
        sync = aDecoder.decodeObject(forKey: "sync") as? Int
        timestamp = aDecoder.decodeObject(forKey: "timestamp") as? String
        displayLabelName = aDecoder.decodeObject(forKey: "displayLabelName") as! String
        captionArray = aDecoder.decodeObject(forKey: "captionArray") as? [Caption]
        attribute = aDecoder.decodeObject(forKey: "attribute") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(appSide, forKey: "appSide")
        aCoder.encode(data, forKey: "data")
        aCoder.encode(dataType, forKey: "dataType")
        aCoder.encode(label, forKey: "label")
        aCoder.encode(required, forKey: "required")
        aCoder.encode(value, forKey: "value")
        aCoder.encode(dropdown, forKey: "dropdown")
        aCoder.encode(fleetData, forKey: "fleetData")
        aCoder.encode(input, forKey: "input")
        aCoder.encode(imageArray, forKey: "imageArray")
        aCoder.encode(checklistDictionary, forKey: "checklistDictionary")
        aCoder.encode(checklistArray, forKey: "checklistArray")
        aCoder.encode(taskTableData, forKey: "taskTableData")
        aCoder.encode(templateName, forKey: "templateName")
        aCoder.encode(sync, forKey: "sync")
        aCoder.encode(timestamp, forKey: "timestamp")
        aCoder.encode(displayLabelName, forKey: "displayLabelName")
        aCoder.encode(captionArray, forKey: "captionArray")
        aCoder.encode(attribute, forKey: "attribute")
    }
    
    init(json: [String:Any]) {
        
        if let appSide = json["app_side"] as? String{
            self.appSide = appSide
        } else if let appSideInt = json["app_side"] as? Int{
            self.appSide = "\(appSideInt)"
        }else {
            self.appSide = ""
        }
        //print(json["data"])
        
        if let data = json["data"] as? String {
            self.data = data
            self.fleetData = data
        } else if let dataFloat = json["data"] as? NSNumber{
            self.data = "\(dataFloat)"
            self.fleetData = "\(dataFloat)"
        } else if let dataInt = json["data"] as? Double {
            self.data = "\(dataInt)"
            self.fleetData = "\(dataInt)"
        } else if let value = json["data"] as? Bool{
            self.data = "\(value)"
            self.fleetData = "\(value)"
        } else {
            self.data = ""
        }
        
        if let fleet_data = json["fleet_data"] as? String{
            if !fleet_data.blank(fleet_data){
                self.data = fleet_data
                self.fleetData = fleet_data
            } else {
                self.fleetData = ""
            }
        } else if let fleet_data_float = json["fleet_data"] as? NSNumber{
            self.data = "\(fleet_data_float)"
            self.fleetData = "\(fleet_data_float)"
        } else if let fleet_data_float = json["fleet_data"] as? Double {
            self.data = "\(fleet_data_float)"
            self.fleetData = "\(fleet_data_float)"
        } else if let fleet_data_int = json["fleet_data"] as? Int{
            self.data = "\(fleet_data_int)"
            self.fleetData = "\(fleet_data_int)"
        } else if let value = json["fleet_data"] as? Bool{
            self.data = "\(value)"
            self.fleetData = "\(value)"
        }else {
            self.fleetData = ""
        }
        
        if let inputData = json["input"] as? String{
            self.input = inputData
        } else {
            self.input = ""
        }
        
        if let templateId = json["template_id"] as? String {
            self.templateName = templateId
        } else {
            self.templateName = "Template Name".localized
        }
        
        
        if let dataType = json["data_type"] as? String{
            self.dataType = dataType
            if(self.dataType == CUSTOM_FIELD_DATA_TYPE.image) {
                if(self.data != "") {
                    self.imageArray = self.data.jsonObject
                }
                // if let fleet_data = json["fleet_data"] as? [String] {
                    if(self.fleetData != "") {
                        self.imageArray = self.fleetData.jsonObject
                    }
//                    for data in fleet_data{
//                        if(data != "") {
//                            self.imageArray.add(data.jsonObject)
//                        }
//                    }
                    
                //}
            } else if(self.dataType == CUSTOM_FIELD_DATA_TYPE.checklist) {
                if let fleet_data = json["fleet_data"] as? String {
                    if(fleet_data != "") {
                        self.checklistDictionary = fleet_data.jsonObject
                        let checklistData = fleet_data.jsonObjectArray as! [[String:Any]]
                        for data in checklistData {
                            let checklistValues = ChecklistData(json: data)
                            self.checklistArray.append(checklistValues)
                        }
                    } else {
                        if let dataChecklist = json["data"] as? String {
                            if(dataChecklist != "") {
                                self.checklistDictionary = dataChecklist.jsonObject
                                let checklistData = dataChecklist.jsonObjectArray as! [[String:Any]]
                                for data in checklistData {
                                    let checklistValues = ChecklistData(json: data)
                                    self.checklistArray.append(checklistValues)
                                }
                            }
                        }
                    }
                } else if let dataChecklist = json["data"] as? String {
                    if(dataChecklist != "") {
                        self.checklistDictionary = dataChecklist.jsonObject
                        let checklistData = dataChecklist.jsonObjectArray as! [[String:Any]]
                        for data in checklistData {
                            let checklistValues = ChecklistData(json: data)
                            self.checklistArray.append(checklistValues)
                        }
                    }
                }
            } else if(self.dataType == CUSTOM_FIELD_DATA_TYPE.table) {
                if let fleet_data = json["fleet_data"] as? NSDictionary{
                    self.taskTableData = TaskTableData(json: fleet_data)
                } else {
                    if let data = json["data"] as? NSDictionary {
                        self.taskTableData = TaskTableData(json: data)
                    } else {
                        self.taskTableData = TaskTableData(json: [:])
                    }
                }
            }
        } else {
            self.dataType = ""
        }
        
        if let label = json["label"] as? String {
            self.label = label
        }else {
            self.label = ""
        }
        
        if let value = json["display_name"] as? String {
            self.displayLabelName = value
        }else {
            self.displayLabelName = self.label
        }
        
        
        if let required = json["required"] as? String{
            if required == "false" {
                self.required = false
            } else if required == "0"{
                self.required = false
            }else {
                self.required = true
            }
        } else if let required = json["required"] as? Int{
                switch required{
                case 0:
                    self.required = false
                default:
                    self.required = true
                }
        } else {
            self.required = false
        }
    
        if let value = json["value"] as? String{
            if value == "false" {
                self.value = false
            }else if value == "0"{
                self.value = false
            }else {
                self.value = true
            }
        } else if let value = json["value"] as? Int{
                switch value{
                case 0:
                    self.value = false
                default:
                    self.value = true
                }
        } else {
            self.value = false
        }
    
        if let dropdown = json["dropdown"] as? [[String:Any]]{
            //self.dropdown = NSMutableArray(array: dropdown as NSArray)
            
            //self.dropdown = ["1"]
            for array in dropdown{
                let value = (array["value"] as! String).trimText
                if value != ""{
                    self.dropdown.append(value)  // = [!]
                }
                
            }
        } else {
            self.dropdown = [String]()
        }
        
        if let value = json["attribute"] as? String {
            if let intValue = Int(value) {
                self.attribute = intValue
            }
        } else if let value = json["attribute"] as? NSNumber {
            self.attribute = Int(value)
        }
        
        if let captionDict = json["caption"] as? [[String:Any]]{
            self.captionArray?.removeAll()
            var tempCaptionArray = [Caption]()
            for item in captionDict {
                let caption = Caption(json: item)
                tempCaptionArray.append(caption)
            }
            for i in 0..<self.imageArray.count {
                let imageUrl = self.imageArray.object(at: i) as! String
                var isUrlExist = false
                var captionExist:Caption!
                
                for caption in tempCaptionArray {
                    if imageUrl == caption.image_url {
                        isUrlExist = true
                        captionExist = caption
                        break
                    }
                }
                if isUrlExist == true {
                    self.captionArray?.append(captionExist)
                } else {
                    let caption = Caption(json: [:])
                    caption.image_url = imageUrl
                    caption.caption_data = ""
                    self.captionArray?.append(caption)
                }
            }
        } else {
            for i in 0..<self.imageArray.count {
                let imageUrl = self.imageArray.object(at: i) as! String
                var isUrlExist = false
                var captionExist:Caption!
                
                for caption in self.captionArray! {
                    if imageUrl == caption.image_url {
                        isUrlExist = true
                        captionExist = caption
                        break
                    }
                }
                if isUrlExist == true {
                    self.captionArray?.append(captionExist)
                } else {
                    let caption = Caption(json: [:])
                    caption.image_url = imageUrl
                    caption.caption_data = ""
                    self.captionArray?.append(caption)
                }
            }
        }
    }
}
