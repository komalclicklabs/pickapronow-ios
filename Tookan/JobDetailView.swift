//
//  JobDetailView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 23/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
protocol JobDetailsDelegate {
    func addSignatureAction()
    func addImageAction(imageType:ImageType, tag:Int)
    func deleteOptionalImage(index:Int)
    func deleteImageFromCustomField(_ indexItem:Int, collectionViewIndex:Int)
    func showImagesPreview(_ indexItem:Int, collectionTag:Int, imageType:ImageType)
    func noInternetConnectionAvailable()
    func showCustomDatePicker(customField:CustomFields, tag:Int)
    func showDropdown(customField:CustomFields, tag:Int)
    func showEmailController(urlText:String)
    func showCalling(phoneNumber:String)
    func checkCustomFieldValidation(index: Int, enteredText: String, row:Int, customFieldType:String)
    func checkCustomFieldValidationForBarcode(index: Int, enteredText: String)
    func checkboxAction(tag:Int, isSelected:Bool)
    func tableAction(index:Int)
    func slideButtonAction()
    func viewInvoiceController()
    func updateChecklistCustomField(index: Int, row:Int)
    func showEarningController()
}

let SECTION_TAG = 1000
let IMAGE_TAG = 2000
let BARCODE_TAG = 3000

class JobDetailView: UIView, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var detailsTable: UITableView!
    @IBOutlet var slideButton: UIButton!
    @IBOutlet var bottomConstraintOfDetailTable: NSLayoutConstraint!

    var delegate:JobDetailsDelegate!
    var model:DetailModel!
    var currentIndexPath = IndexPath()
    var keyboardToolbar:KeyboardToolbar!
    let headerHeight:CGFloat = 28.0
    let footerHeight:CGFloat = 15.0
    let offlineModel = OfflineDataModel()
    
    override func awakeFromNib() {
        /*--------- Keyboard Toolbar -------------*/
        self.keyboardToolbar = KeyboardToolbar()
        self.keyboardToolbar.keyboardDelegate = self
        self.keyboardToolbar.addButtons()
    }
    
    func setDetailTable() {
        self.model.setSectionsAndRows()
        self.detailsTable.delegate = self
        self.detailsTable.dataSource = self
        self.model.detailsTable = self.detailsTable
        self.detailsTable.register(UINib(nibName: NIB_NAME.jobDetailCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobDetailCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.userDetailCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.userDetailCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.jobDescriptionCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobDescriptionCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.notesCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.notesCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.imageTableCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.imageTableCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.customFieldsCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.customFieldsCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.checklistCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.checklistCell)
        self.detailsTable.register(UINib(nibName: NIB_NAME.jobListCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobListCell)
        //self.detailsTable.estimatedRowHeight = 100
        self.detailsTable.rowHeight = UITableViewAutomaticDimension
        self.detailsTable.bounces = false
    }
    
    @IBAction func slideAction(_ sender: Any) {
        delegate.slideButtonAction()
    }
    
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.model.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.getRowCounts(section: section)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard self.checkValidationForTable(section: indexPath.section) == true else {
            return 0
        }
        switch self.model.sections[indexPath.section] {
        case .jobDetails,.notes, .signature, .barcode, .image, .invoice, .earnings:
            return UITableViewAutomaticDimension
        default:
          guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[indexPath.section - self.model.countBeforeCustomFields].appSide != APP_SIDE.hidden else {
                return 0
            }
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch self.model.sections[section] {
        case .jobDetails:
            return 0.01
        case .notes:
            guard Singleton.sharedInstance.selectedTaskDetails.noteOptionalField.value == 1 else {
                return 0.01
            }
        case .signature:
            guard Singleton.sharedInstance.selectedTaskDetails.signatureOptionalField.value == 1 else {
                return 0.01
            }
        case .barcode:
            guard Singleton.sharedInstance.selectedTaskDetails.scannerOptionalField.value == 1 else {
                return 0.01
            }
        case .image:
            guard Singleton.sharedInstance.selectedTaskDetails.imageOptionalField.value == 1 else {
                return 0.01
            }
        case .invoice, .earnings:
            return headerHeight
        default:
            guard (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > (section - self.model.countBeforeCustomFields) else {
                return 0.01
            }
            
            guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[section - self.model.countBeforeCustomFields].appSide != APP_SIDE.hidden else {
                return 0.01
            }
            return headerHeight
        }
        return headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch self.model.sections[section] {
        case .jobDetails:
            return footerHeight
        case .notes:
            guard Singleton.sharedInstance.selectedTaskDetails.noteOptionalField.value == 1 else {
                return 0.01
            }
        case .signature:
            guard Singleton.sharedInstance.selectedTaskDetails.signatureOptionalField.value == 1 else {
                return 0.01
            }
        case .barcode:
            guard Singleton.sharedInstance.selectedTaskDetails.scannerOptionalField.value == 1 else {
                return 0.01
            }
        case .image:
            guard Singleton.sharedInstance.selectedTaskDetails.imageOptionalField.value == 1 else {
                return 0.01
            }
        case .invoice, .earnings:
            return footerHeight
        default:
            guard (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > (section - self.model.countBeforeCustomFields) else {
                return 0.01
            }
            guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray?[section - self.model.countBeforeCustomFields].appSide != APP_SIDE.hidden else {
                return 0.01
            }
            return footerHeight
        }
        return footerHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.model.getDetailTableCell(indexPath: indexPath, tableView: tableView)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard self.checkValidationForTable(section: section) == true else {
            return nil
        }
        let headerView = DetailHeaderView.getHeaderView()
        self.model.getHeaderView(section: section, headerView: headerView)
        switch self.model.sections[section] {
        case .notes:
            headerView.tag = section + SECTION_TAG
            headerView.addButton.addTarget(self, action: #selector(self.notesHeaderAction(sender:)), for: .touchUpInside)
        case .signature:
            headerView.addButton.addTarget(self, action: #selector(self.signatureHeaderAction(sender:)), for: .touchUpInside)
        case .image, .customImage:
            headerView.addButton.addTarget(self, action: #selector(self.imageHeaderAction(sender:)), for: .touchUpInside)
        case .barcode:
            headerView.tag = section + BARCODE_TAG
            headerView.addButton.addTarget(self, action: #selector(self.barcodeHeaderAction(sender:)), for: .touchUpInside)
        case .checkbox:
            headerView.addButton.addTarget(self, action: #selector(self.checkboxHeaderAction(sender:)), for: .touchUpInside)
            headerView.addButton.tag = section - self.model.countBeforeCustomFields
        case .checklist:
            headerView.tag = section - self.model.countBeforeCustomFields
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.checklistHeaderTapAction(gesture:)))
            tap.numberOfTapsRequired = 1
            headerView.addGestureRecognizer(tap)
            headerView.addButton.isUserInteractionEnabled = false
            if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.model.countBeforeCustomFields].showChecklistRows == true {
                headerView.addButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
            } else {
                headerView.addButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) * 180.0)
            }
        case .table:
            headerView.addButton.addTarget(self, action: #selector(self.tableHeaderAction(sender:)), for: .touchUpInside)
            headerView.tag = section - self.model.countBeforeCustomFields
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tableHeaderTapAction(gesture:)))
            tap.numberOfTapsRequired = 1
            headerView.addGestureRecognizer(tap)
        case .invoice:
            headerView.addButton.addTarget(self, action: #selector(self.viewInvoiceAction), for: .touchUpInside)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.viewInvoiceAction))
            tap.numberOfTapsRequired = 1
            headerView.addGestureRecognizer(tap)
        case .earnings:
            headerView.addButton.addTarget(self, action: #selector(self.earningAction), for: .touchUpInside)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.earningAction))
            tap.numberOfTapsRequired = 1
            headerView.addGestureRecognizer(tap)
        default:
            break
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: footerHeight))
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.checkValidationForTable(section: indexPath.section) == true else {
            return
        }
        switch self.model.sections[indexPath.section] {
        case .checklist:
            if IJReachability.isConnectedToNetwork() == false && Auxillary.isAppSyncingEnable() == false {
                self.delegate.noInternetConnectionAvailable()
            } else {
                let checklistValues = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.model.countBeforeCustomFields].checklistArray[indexPath.row]
                checklistValues.uploading = true
                if(checklistValues.check == true) {
                    checklistValues.check = false
                } else {
                    checklistValues.check = true
                }
                self.setChecklistDictionary(section: indexPath.section - self.model.countBeforeCustomFields, row:indexPath.row)
                if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![indexPath.section - self.model.countBeforeCustomFields].showChecklistRows == true {
                    self.detailsTable.reloadRows(at: [IndexPath(row: indexPath.row, section: indexPath.section)], with: .none)
                }
            }
        default:
            break
        }
    }
    
    func checkValidationForTable(section:Int) -> Bool {
        guard section < self.model.sections.count else {
            return false
        }
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return false
        }
        guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray != nil else {
            return false
        }
        guard (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > (section - self.model.countBeforeCustomFields) else {
            return false
        }
        return true
    }

    //MARK: SetChecklistDictionary
    func setChecklistDictionary(section:Int, row:Int) {
        var checklistValues = [[String:Any]]()
        for checklist in Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section].checklistArray {
            let checklistDictionary = ["check":checklist.check ?? false,"id":checklist.check_id ?? 0, "value":checklist.value ?? 0] as [String : Any]
            checklistValues.append(checklistDictionary)
        }
        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section].fleetData = checklistValues.jsonString
        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section].data = checklistValues.jsonString
        delegate.updateChecklistCustomField(index: section, row:row)
    }
    
    //MARK: NOTE HEADER ACTION
    func notesHeaderAction(sender:UIButton) {
        self.model.setAddButtonOfNotesHeaderView(status: true)
        var dict:[String:Any] = ["description": ""]
        dict["creation_datetime"] = ""
        dict["fleet_id"] = "\(Singleton.sharedInstance.fleetDetails.fleetId!)"
        dict["latitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)"
        dict["longitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.longitude)"
        dict["type"] = "text_added"
        let taskHistory = TaskHistory(json: dict as NSDictionary)
        Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.append(taskHistory)
        let indexPath = IndexPath(row: (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! - 1, section: self.model.sections.index(of: DETAIL_SECTION.notes)!)
        self.detailsTable.insertRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
        self.detailsTable.scrollToRow(at: indexPath, at: .middle, animated: false)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            self.currentIndexPath = indexPath
            cell.descriptionView.becomeFirstResponder()
        }
    }
    
    //MARK: Signature Header Action
    func signatureHeaderAction(sender:UIButton) {
        self.delegate.addSignatureAction()
    }
    
    //MARK: Image Header Action
    func imageHeaderAction(sender:UIButton) {
        self.delegate.addImageAction(imageType: .image, tag:sender.tag)
    }
    
    //MARK: Checkbox header action
    func checkboxHeaderAction(sender:UIButton) {
        if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false) {
            if IJReachability.isConnectedToNetwork() == false && Auxillary.isAppSyncingEnable() == false {
                delegate.noInternetConnectionAvailable()
            } else {
                sender.isSelected = !sender.isSelected
                delegate.checkboxAction(tag: sender.tag, isSelected: sender.isSelected)
            }
        }
    }
    
    //MARK: Checklist header action
    func checklistHeaderTapAction(gesture:UITapGestureRecognizer) {
        Singleton.sharedInstance.selectedTaskDetails.customFieldArray![(gesture.view?.tag)!].showChecklistRows = !Singleton.sharedInstance.selectedTaskDetails.customFieldArray![(gesture.view?.tag)!].showChecklistRows
        if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![(gesture.view?.tag)!].showChecklistRows == true {
            var rows = [IndexPath]()
            for i in 0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray![(gesture.view?.tag)!].checklistDictionary.count {
                rows.append(IndexPath(row: i, section: self.model.countBeforeCustomFields + (gesture.view?.tag)!))
            }
            self.detailsTable.insertRows(at: rows, with: .bottom)
        } else {
            var rows = [IndexPath]()
            for i in 0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray![(gesture.view?.tag)!].checklistDictionary.count {
                rows.append(IndexPath(row: i, section: self.model.countBeforeCustomFields + (gesture.view?.tag)!))
            }
            self.detailsTable.deleteRows(at: rows, with: .top)
        }
        if let headerView = self.detailsTable.viewWithTag( (gesture.view?.tag)!) as? DetailHeaderView {
            UIView.animate(withDuration: 0.3) {
                if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![(gesture.view?.tag)!].showChecklistRows == true {
                    headerView.addButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
                } else {
                    headerView.addButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) * 180.0)
                }
            }
        }
    }
    
    //MARK: Table header action
    func tableHeaderAction(sender:UIButton) {
        delegate.tableAction(index: sender.tag)
    }
    
    func tableHeaderTapAction(gesture:UITapGestureRecognizer) {
        delegate.tableAction(index: (gesture.view?.tag)!)
    }
    
    //MARK: View Invoice
    func viewInvoiceAction() {
        delegate.viewInvoiceController()
    }
    
    func earningAction() {
        delegate.showEarningController()
    }
    
    //MARK: Barcode Header Action
    func barcodeHeaderAction(sender:UIButton) {
        self.model.setAddButtonOfBarcodeHeaderView(status: true)
        var dict:[String:Any] = ["description": ""]
        dict["creation_datetime"] = ""
        dict["fleet_id"] = "\(Singleton.sharedInstance.fleetDetails.fleetId!)"
        dict["latitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)"
        dict["longitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.longitude)"
        dict["type"] = "barcode_added"
        let taskHistory = TaskHistory(json: dict as NSDictionary)
        Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.append(taskHistory)
        let indexPath = IndexPath(row: (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! - 1, section: self.model.sections.index(of: DETAIL_SECTION.barcode)!)
        self.detailsTable.insertRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            self.currentIndexPath = indexPath
            cell.descriptionView.becomeFirstResponder()
        }
    }
    
    //MARK: Custom Barcode Header Action
    func customBarcodeHeaderAction(sender:UIButton) {
        self.model.setAddButtonOfBarcodeHeaderView(status: true)
        var dict:[String:Any] = ["description": ""]
        dict["creation_datetime"] = ""
        dict["fleet_id"] = "\(Singleton.sharedInstance.fleetDetails.fleetId!)"
        dict["latitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.latitude)"
        dict["longitude"] = "\(LocationTracker.sharedInstance().myLocation.coordinate.longitude)"
        dict["type"] = "barcode_added"
        
        let taskHistory = TaskHistory(json: dict as NSDictionary)
        Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.append(taskHistory)
        let indexPath = IndexPath(row: (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! - 1, section: self.model.sections.index(of: DETAIL_SECTION.barcode)!)
        self.detailsTable.insertRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
        if let cell = self.detailsTable.cellForRow(at: indexPath) as? NotesCell {
            self.currentIndexPath = indexPath
            cell.descriptionView.becomeFirstResponder()
        }
    }
}

extension JobDetailView : KeyboardDelegate {
    func doneFromKeyboard() {
        self.endEditing(true)
    }
    
    func nextFromKeyboard(_ nextField: AnyObject) {
        let textField = nextField as! UITextView
        var currentFieldTag = textField.tag + self.model.countBeforeCustomFields
        if textField.tag >= SECTION_TAG{//this for only optional fields ie. Notes, barcode
            currentFieldTag = textField.tag / SECTION_TAG
        }
        guard currentFieldTag + 1 < self.model.sections.count else {
            self.endEditing(true)
            return
        }
        let rowTag = textField.tag % SECTION_TAG
        switch(self.model.sections[currentFieldTag]) {
        case .text, .number, .email, .telephone, .url, .customBarcode:
            if self.scrollToNextSection(currentFieldTag + 1, self.model.sections.count, increment : 1) == 1{
                self.toResignFirstResponderForCustomFieldCell(row: 0, section: currentFieldTag)
            }
        case .notes:
            if  rowTag < (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! - 1  {
                self.toMakeNoteCellFirstResponder(row: rowTag + 1, section: currentFieldTag)
            } else { //Jumps between Section conditions
                if self.scrollToNextSection(currentFieldTag + 1, self.model.sections.count, increment : 1) == 1{
                    self.toResignFirstResponderForNoteCell(row: rowTag, section: currentFieldTag)
                }
            }
        case .barcode:
            if rowTag < (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! - 1  {
                self.toMakeNoteCellFirstResponder(row: rowTag + 1, section: currentFieldTag)
            } else {
                if self.scrollToNextSection(currentFieldTag + 1, self.model.sections.count, increment : 1) == 1{
                    self.toResignFirstResponderForNoteCell(row: rowTag, section: currentFieldTag)
                }
            }
        default:
            break
        }
    }
    
    func prevFromKeyboard(_ prevField: AnyObject) {
        let textField = prevField as! UITextView
        var currentFieldTag = textField.tag + self.model.countBeforeCustomFields
        if textField.tag >= SECTION_TAG {
            currentFieldTag = textField.tag / SECTION_TAG
        }
        guard currentFieldTag - 1 < self.model.sections.count else {
            return
        }
        
        let rowTag = textField.tag % SECTION_TAG
        switch(self.model.sections[currentFieldTag]) {
        case .text, .number, .email, .telephone, .url, .customBarcode:
            if self.scrollToNextSection(currentFieldTag - 1, 0, increment : -1) == 1 {
                self.toResignFirstResponderForCustomFieldCell(row: 0, section: currentFieldTag)
            }
        case .notes, .barcode:
            if  rowTag == 0  {
                if self.scrollToNextSection(currentFieldTag - 1, 0, increment : -1) == 1 {
                    self.toResignFirstResponderForNoteCell(row: rowTag, section: currentFieldTag)
                }
            } else { //Jumps between rows conditions
                self.toMakeNoteCellFirstResponder(row: rowTag - 1, section: currentFieldTag)
            }
        default:
            break
        }
    }
    
    func scrollToNextSection(_ currentFieldTag: Int, _ findUpToTag: Int, increment : Int) -> Int {
        for section in stride(from: currentFieldTag , to: findUpToTag, by: increment){
            switch self.model.sections[section] {
            case .barcode:
                if (Singleton.sharedInstance.selectedTaskDetails.addedScannerArray?.count)! > 0 {
                    self.toMakeNoteCellFirstResponder(row: 0, section: section)
                }
            case .notes:
                if (Singleton.sharedInstance.selectedTaskDetails.addedNotesArray?.count)! > 0 {
                    self.toMakeNoteCellFirstResponder(row: 0, section: section)
                }
            case .email, .number,.telephone,.text, .url:
                if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.model.countBeforeCustomFields].appSide == APP_SIDE.write) {
                    let fleetData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.model.countBeforeCustomFields])
                    switch self.model.sections[section] {
                    case .email, .url, .telephone:
                        if fleetData.length == 0{
                            self.toMakeCustomFieldCellFirstResponder(row: 0, section: section)
                        }
                    default:
                        self.toMakeCustomFieldCellFirstResponder(row: 0, section: section)
                    }
                }
            case .customBarcode:
                if(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![section - self.model.countBeforeCustomFields].appSide == APP_SIDE.write) {
                    self.toMakeNoteCellFirstResponder(row: 0, section: section)
                }
            default:
                break
            }
        }
        return 0
    }
    
    func toMakeNoteCellFirstResponder(row:Int, section:Int) {
        self.detailsTable.scrollToRow(at: IndexPath(row: row, section: section), at: .middle, animated: false)
        if let cell = self.detailsTable.cellForRow(at: IndexPath(row: row, section: section)) as? NotesCell {
            cell.descriptionView.becomeFirstResponder()
        }
    }
    
    func toMakeCustomFieldCellFirstResponder(row:Int, section:Int) {
        self.detailsTable.scrollToRow(at: IndexPath(row: 0, section: section), at: .middle, animated: false)
        if let cell = self.detailsTable.cellForRow(at: IndexPath(row: 0, section: section)) as? CustomFieldsCell {
            cell.detailView.becomeFirstResponder()
        }
    }
    
    func toResignFirstResponderForNoteCell(row:Int, section:Int) {
        if let cell = self.detailsTable.cellForRow(at: IndexPath(row: row, section: section)) as? NotesCell {
            cell.descriptionView.resignFirstResponder()
        }
    }
    
    func toResignFirstResponderForCustomFieldCell(row:Int, section:Int) {
        if let cell = self.detailsTable.cellForRow(at: IndexPath(row: row, section: section)) as? CustomFieldsCell {
            cell.detailView.resignFirstResponder()
        }
    }
}

extension JobDetailView:NotesDelegate {
    func updateDescriptionCellHeight(descriptionText:String, section:Int, index:Int) {
        if let section = self.model.sections.index(of: DETAIL_SECTION.notes) {
            if self.detailsTable.isSectionExist(section: section){
                let numberOfRows = self.detailsTable.numberOfRows(inSection: self.model.sections.index(of: DETAIL_SECTION.notes)!)
                if numberOfRows > 0 {
                    UIView.setAnimationsEnabled(false) // Disable animations
                    self.detailsTable.beginUpdates()
                    self.detailsTable.endUpdates()
                    UIView.setAnimationsEnabled(true)
                }
            }
        }
    }
    
    func notesViewShouldReturn(index:Int, section:Int) {
        let currentIndex = index % SECTION_TAG
        switch section {
        case SECTION_TAG:
            self.model.sendNewNotes(index: currentIndex)
            break
        case BARCODE_TAG:
            self.model.sendNewBarcode(index: currentIndex)
            break
        case Int(self.model.sections.index(of: .customBarcode)!):
            if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false) {
                if let cell = detailsTable.cellForRow(at: IndexPath(row: 0, section:section)) as? NotesCell {
                    let enteredText = cell.descriptionView.text.trimText
                    delegate.checkCustomFieldValidationForBarcode(index: section - self.model.countBeforeCustomFields, enteredText: enteredText)
                }
            }
            break
        default:
            break
        }
    }
    func notesShouldBeginEditing(textView: UITextView, section: Int) {
        textView.inputAccessoryView = keyboardToolbar
        keyboardToolbar.currentTextField = textView
        let index = textView.tag % SECTION_TAG
        switch section {
        case SECTION_TAG:
            let indexPath = IndexPath(row: index, section: self.model.sections.index(of: DETAIL_SECTION.notes)!)
            self.currentIndexPath = indexPath
            self.model.editNotes(index: index)
            break
        case BARCODE_TAG:
            let indexPath = IndexPath(row: index, section: self.model.sections.index(of: DETAIL_SECTION.barcode)!)
            self.currentIndexPath = indexPath
            self.model.editBarcode(index: index)
            break
        default:
            let indexPath = IndexPath(row: index, section: self.model.sections.index(of: DETAIL_SECTION.customBarcode)!)
            self.currentIndexPath = indexPath
            break
        }
    }
    
    func notesShouldStartEditing(textView: UITextView, section: Int) {
        textView.inputAccessoryView = keyboardToolbar
        keyboardToolbar.currentTextField = textView
        let index = textView.tag % SECTION_TAG
        switch section {
        case SECTION_TAG:
            let indexPath = IndexPath(row: index, section: self.model.sections.index(of: DETAIL_SECTION.notes)!)
            self.currentIndexPath = indexPath
            break
        case BARCODE_TAG:
            let indexPath = IndexPath(row: index, section: self.model.sections.index(of: DETAIL_SECTION.barcode)!)
            self.currentIndexPath = indexPath
            break
        default:
            let indexPath = IndexPath(row: index, section: self.model.sections.index(of: DETAIL_SECTION.customBarcode)!)
            self.currentIndexPath = indexPath
            break
        }
    }
}

extension JobDetailView:JobDescriptionDelegate {
    func imageTappedForTaskDescription(imageType: ImageType, index: Int) {
        delegate.showImagesPreview(index, collectionTag: 0, imageType: imageType)
    }
}

extension JobDetailView:ImageTableDelegate {
    func imageTapped(imageType: ImageType, index: Int, collectionTag: Int) {
        switch imageType {
        case ImageType.signatureImage:
            delegate.addSignatureAction()
            break
        case ImageType.image, .customFieldImage:
            delegate.showImagesPreview(index, collectionTag: collectionTag, imageType: imageType)
            break
        default:
            break
        }
    }
    
    func deleteOptionalImage(index:Int) {
        delegate.deleteOptionalImage(index: index)
    }
    
    func deleteCustomFieldImage(index: Int, sectionIndex:Int) {
        delegate.deleteImageFromCustomField(index, collectionViewIndex: sectionIndex)
    }
}

extension JobDetailView:CustomFieldDelegate {
    func uploadUnsyncTask() {
        _ = offlineModel.uploadUnsyncedTaskToServer()
    }
    
    func isOfflineSyncingRequired() -> Bool {
        if IJReachability.isConnectedToNetwork() == true {
            if(offlineModel.isThereAnyUnsyncedTaskInDatabase() > 0) {
                self.perform(#selector(self.uploadUnsyncTask), with: nil, afterDelay: 1.0)
                self.endEditing(true)
                return true
            }
        }
        return false
    }
    
    func customFieldShouldBeginEditing(textView: UITextView) -> Bool {
        guard self.isOfflineSyncingRequired() == false else {
            return false
        }
        if IJReachability.isConnectedToNetwork() == false && Auxillary.isAppSyncingEnable() == false {
            delegate.noInternetConnectionAvailable()
            return false
        } else {
            guard Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true else {
                return false
            }
            textView.inputAccessoryView = keyboardToolbar
            keyboardToolbar.currentTextField = textView
            switch(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![textView.tag].dataType) {
            case CUSTOM_FIELD_DATA_TYPE.date, CUSTOM_FIELD_DATA_TYPE.dateTime, CUSTOM_FIELD_DATA_TYPE.dateFuture,
                 CUSTOM_FIELD_DATA_TYPE.datePast,CUSTOM_FIELD_DATA_TYPE.dateTimeFuture,CUSTOM_FIELD_DATA_TYPE.dateTimePast:
                if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false) {
                   self.endEditing(true)
                    delegate.showCustomDatePicker(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![textView.tag], tag: textView.tag)
                }
            case CUSTOM_FIELD_DATA_TYPE.dropDown:
                self.endEditing(true)
                if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false) {
                    textView.isUserInteractionEnabled = false
                    delegate.showDropdown(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![textView.tag], tag: textView.tag)
                }
            case CUSTOM_FIELD_DATA_TYPE.url:
                if let cell = detailsTable.cellForRow(at: IndexPath(row: 0, section: textView.tag + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
                    return self.handlingUrlCase(tag: textView.tag, cell: cell)
                }
            case CUSTOM_FIELD_DATA_TYPE.telephone:
                if let cell = detailsTable.cellForRow(at: IndexPath(row: 0, section: textView.tag + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
                    return self.handlingPhoneCase(tag: textView.tag, cell: cell)
                }
            case CUSTOM_FIELD_DATA_TYPE.email:
                if let cell = detailsTable.cellForRow(at: IndexPath(row: 0, section: textView.tag + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
                    return self.handlingEmailCase(tag: textView.tag, cell: cell)
                }
            case CUSTOM_FIELD_DATA_TYPE.number, CUSTOM_FIELD_DATA_TYPE.text:
                if let cell = detailsTable.cellForRow(at: IndexPath(row: 0, section: textView.tag + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
                    cell.actionButton.isSelected = false
                    let indexPath = IndexPath(row: 0, section: textView.tag + self.model.countBeforeCustomFields)
                    self.currentIndexPath = indexPath
                    return true
                }
            default:
                return true
            }
            return false
        }
    }
    
    func customFieldShouldReturn(textView: UITextView) -> Bool {
        if IJReachability.isConnectedToNetwork() == true {
            if(offlineModel.isThereAnyUnsyncedTaskInDatabase() > 0) {
                return true
            }
        }
        delegate.checkCustomFieldValidation(index: textView.tag, enteredText: textView.text!, row:0, customFieldType: CUSTOM_FIELD_DATA_TYPE.text)
        return true
    }
    //MARK: EMAIL CASE
    func handlingEmailCase(tag:Int, cell:CustomFieldsCell) -> Bool {
        guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
            return false
        }
        let urlText = cell.detailView.text!.trimText
        if(urlText.length > 0 && cell.actionButton.isSelected == true) {
            delegate.showEmailController(urlText: urlText)
        } else {
            if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
                cell.actionButton.isSelected = false
                let indexPath = IndexPath(row: 0, section: tag + self.model.countBeforeCustomFields)
                self.currentIndexPath = indexPath
                return true
            }
        }
        return false
    }
    //MARK: PHONE CASE
    func handlingPhoneCase(tag:Int, cell:CustomFieldsCell) -> Bool {
        guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
            return false
        }
        let phoneNumber = cell.detailView.text!.trimText
        if(phoneNumber.length > 8 && phoneNumber.length <= 12 && cell.actionButton.isSelected == true) {
            delegate.showCalling(phoneNumber: phoneNumber)
        } else {
            if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
                cell.actionButton.isSelected = false
                let indexPath = IndexPath(row: 0, section: tag + self.model.countBeforeCustomFields)
                self.currentIndexPath = indexPath
                return true
            }
        }
        return false
    }
    //MARK: URL CASE
    func handlingUrlCase(tag:Int, cell:CustomFieldsCell) -> Bool {
        guard Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false else {
            return false
        }
        let urlText = cell.detailView.text!.trimText
        if(urlText.length > 0 && cell.actionButton.isSelected == true) {
            var url:URL!
            if(urlText.range(of: "http://") != nil || urlText.range(of: "https://") != nil) {
                url = URL(string: urlText)!
            } else {
                url = URL(string: "http://\(urlText)")
            }
            UIApplication.shared.openURL(url)
        } else {
            if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true {
                cell.actionButton.isSelected = false
                let indexPath = IndexPath(row: 0, section: tag + self.model.countBeforeCustomFields)
                self.currentIndexPath = indexPath
                return true
            }
        }
        return false
    }
    
    
    //MARK: EDIT ACTION
    func editAction(sender:UIButton) {
        if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false) {
            if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![sender.tag].dataType == CUSTOM_FIELD_DATA_TYPE.dropDown {
                self.endEditing(true)
                if(Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == false) {
                    delegate.showDropdown(customField: Singleton.sharedInstance.selectedTaskDetails.customFieldArray![sender.tag], tag: sender.tag)
                }
                return
            }
            if let cell = detailsTable.cellForRow(at: IndexPath(row: 0, section:sender.tag + self.model.countBeforeCustomFields)) as? CustomFieldsCell {
                sender.isSelected = !sender.isSelected
                if(sender.isSelected == false) {
                    cell.detailView.isEditable = true
                    cell.detailView.becomeFirstResponder()
                } else {
                    var enteredText:String!
                    switch(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![sender.tag].dataType) {
                    case CUSTOM_FIELD_DATA_TYPE.text:
                        enteredText = cell.detailView.text.trimText
                        break
                    default:
                        enteredText = cell.detailView.text!
                        break
                    }
                    delegate.checkCustomFieldValidation(index: sender.tag, enteredText: enteredText, row:0, customFieldType:CUSTOM_FIELD_DATA_TYPE.text)
                }
            }
        }
    }
    
    func updateTableHeight(descriptionText: String) {
        UIView.setAnimationsEnabled(false) // Disable animations
        self.detailsTable.beginUpdates()
        self.detailsTable.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
}
