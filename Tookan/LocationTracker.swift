

//
//  LocationHandler.swift
//  Tookan
//
//  Created by Click Labs on 8/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import SystemConfiguration
import GoogleMaps
import Darwin.C

class LocationTracker: LocationShareModel,CLLocationManagerDelegate {
    
    var myLastLocation: CLLocation!
    var shareModel: LocationShareModel?
    var myLocation: CLLocation!
    var myLocationAccuracy: CLLocationAccuracy!
    var sendFirstTimeLocation = true
    var locationUpdateTimer: Timer!
    var locationManager:CLLocationManager!
    var speed:Float = 0
    fileprivate static let locationManagerObj = CLLocationManager()
    fileprivate static let locationTracker = LocationTracker()
    
    override init() {
        super.init()
        self.shareModel = LocationShareModel.sharedModel()
//        NotificationCenter.default.addObserver(self, selector: #selector(LocationTracker.applicationEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    class func sharedInstance() -> LocationTracker {
        return locationTracker
    }
    
    class func sharedLocationManager() -> CLLocationManager {
        return locationManagerObj
    }
    
    func setLocationUpdate() {
        if(self.locationManager != nil) {
            self.locationManager.stopMonitoringSignificantLocationChanges()
            self.locationManager.stopUpdatingLocation()
        }
        locationManager = LocationTracker.sharedLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation //kCLLocationAccuracyThreeKilometers//
        locationManager.activityType = CLActivityType.automotiveNavigation
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.distanceFilter = 20 //kCLDistanceFilterNone//
        if #available(iOS 9.0, *) {
            locationManager.allowsBackgroundLocationUpdates = true
        }
       
        /*====== RESET TIMER ==========*/
        if self.locationUpdateTimer != nil {
            self.locationUpdateTimer.invalidate()
            self.locationUpdateTimer = nil
        }

        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.high:
            self.locationUpdateTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.updateLocationToServer), userInfo: nil, repeats: true)
            self.updateLocationToServer()
            break
        case BATTERY_USAGE.low, BATTERY_USAGE.medium:
            self.locationUpdateTimer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.updateLocationToServer), userInfo: nil, repeats: true)
            self.updateLocationToServer()
            break
        case BATTERY_USAGE.foreground:
            if #available(iOS 9.0, *) {
                locationManager.allowsBackgroundLocationUpdates = false
            }
//            guard UIApplication.shared.applicationState == UIApplicationState.active || UIApplication.shared.applicationState == UIApplicationState.inactive else {
//                return
//            }
            self.locationUpdateTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.updateLocationToServer), userInfo: nil, repeats: true)
            self.updateLocationToServer()
            break
        default:
            break
        }
        /*==========================================*/

        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func restartLocationUpdates() {
        if self.checkLocationAuthorization() == true {
            if (self.shareModel!.timer != nil) {
                self.shareModel!.timer!.invalidate()
                self.shareModel!.timer = nil
            }
            if(self.locationManager != nil) {
                self.locationManager.stopMonitoringSignificantLocationChanges()
            }
            self.checkToStartLocationTracking()
           // setLocationUpdate()
        }
    }
    
    func startLocationTracking() {
        if self.checkLocationAuthorization() == true {
            setLocationUpdate()
        }
    }
    
    func stopLocationTracking() {
        if(self.locationUpdateTimer != nil) {
            self.locationUpdateTimer.invalidate()
            self.locationUpdateTimer = nil
        }
        
        if (self.shareModel!.timer != nil) {
            self.shareModel!.timer!.invalidate()
            self.shareModel!.timer = nil
        }
        let locationManager: CLLocationManager = LocationTracker.sharedLocationManager()
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (self.shareModel!.timer != nil) {
            return
        }
        self.shareModel!.bgTask = BackgroundTaskManager.sharedBackgroundTaskManager()
        _ = self.shareModel!.bgTask!.beginNewBackgroundTask()
        if locations.last != nil {
            self.myLocation = locations.last! as CLLocation
            self.sendLocationThroughUDP(socketLocation: self.myLocation)
            self.myLocationAccuracy = self.myLocation.horizontalAccuracy
            if(self.sendFirstTimeLocation == true) {
                var locationDict:[String:Any] = ["latitude" : "\(self.myLocation.coordinate.latitude)"]
                locationDict["longitude"] = "\(self.myLocation.coordinate.longitude)"
                print(locationDict)
                //let locationDict = ["latitude" : "\(self.myLocation.coordinate.latitude)", "longitude" : "\(self.myLocation.coordinate.longitude)"]
                UserDefaults.standard.set(locationDict, forKey: USER_DEFAULT.lastAccurateUserLocation)
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NotifRequestingResponse.currentLocationOfUser.rawValue), object: self, userInfo: ["userLocationCoordinate" : self.myLocation])
                self.sendFirstTimeLocation = false
            }
            
            /*======= SET MODE ==========*/
            switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
            case BATTERY_USAGE.high, BATTERY_USAGE.low, BATTERY_USAGE.medium:
                self.applyFilterOnGetLocation()
                break
            case BATTERY_USAGE.foreground:
                //self.locationManager.stopUpdatingLocation()
                DispatchQueue.main.async {
                    guard UIApplication.shared.applicationState == UIApplicationState.active  else {
                        return
                    }
                }
                
                self.applyFilterOnGetLocation()
                break
            default:
                break
            }
            /*==========================================*/
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func sendLocationThroughUDP(socketLocation:CLLocation){
        guard Singleton.sharedInstance.fleetDetails != nil else {
            return
        }
        
        guard Singleton.sharedInstance.fleetDetails.isUDPEnabled == 1 else {
            return
        }
        
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil) {
            let timestamp = socketLocation.timestamp.timeIntervalSince1970 * 1000
            var myLocationToSend:[String:Any] = ["lat" : socketLocation.coordinate.latitude as Double]
            myLocationToSend["lng"] = socketLocation.coordinate.longitude as Double
            myLocationToSend["tm_stmp"] = timestamp
            myLocationToSend["bat_lvl"] = UIDevice.current.batteryLevel * 100
            myLocationToSend["acc"] = socketLocation.horizontalAccuracy
            //myLocationToSend = ["lat" : socketLocation.coordinate.latitude as Double,"lng" :socketLocation.coordinate.longitude as Double, "tm_stmp" : timestamp, "bat_lvl" : UIDevice.current.batteryLevel * 100,"acc":socketLocation.horizontalAccuracy]\
            var udpData:[String:Any] = ["flag" : 0]
            udpData["data"] = myLocationToSend
            udpData["fleet_id"] = Singleton.sharedInstance.fleetDetails.fleetId!
            udpData["device_type"] = DEVICE_TYPE
            //let udpData = ["flag":0, "data":myLocationToSend, "fleet_id":Singleton.sharedInstance.fleetDetails.fleetId!, "device_type":DEVICE_TYPE] as [String : Any]
            let client:UDPClient = UDPClient(addr: IP_ADDRESS, port: PORT)
            _ = client.send(str: udpData.jsonString)
            _ = client.close()
        }
    }
    
    func stopLocationDelayBy10Seconds() {
        let locationManager: CLLocationManager = LocationTracker.sharedLocationManager()
        locationManager.stopUpdatingLocation()
    }
    
    func applyFilterOnGetLocation() {
        if self.myLocation != nil  {
            var locationArray = [Any]()
            if let array = UserDefaults.standard.object(forKey: USER_DEFAULT.locationArray) as? [Any] {
                locationArray = array
            }
            var locationDict:[String:Any] = ["latitude" : "\(self.myLocation.coordinate.latitude)"]
            locationDict["longitude"] = "\(self.myLocation.coordinate.longitude)"
            //let locationDict = ["latitude" : "\(self.myLocation.coordinate.latitude)", "longitude" : "\(self.myLocation.coordinate.longitude)"]
            if let _ = UserDefaults.standard.object(forKey: "accessToken") as? String {
                if(self.myLocationAccuracy < 20){
                    if(self.myLastLocation == nil) {
                        UserDefaults.standard.set(locationDict, forKey: USER_DEFAULT.lastAccurateUserLocation)
                        //var myLocationToSend = [String:Any]()
                        let date = myLocation.timestamp.timeIntervalSince1970 * 1000
                        //let timestamp = Auxillary.getUTCDateString()
                        var myLocationToSend:[String:Any] = ["lat" : myLocation!.coordinate.latitude as Double]
                        myLocationToSend["lng"] = myLocation!.coordinate.longitude as Double
                        myLocationToSend["tm_stmp"] = "\(date)"
                        myLocationToSend["bat_lvl"] = UIDevice.current.batteryLevel * 100
                        myLocationToSend["acc"] = (self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300)
                        myLocationToSend["d_acc"] = "1"
                        myLocationToSend["gps"] = 1
                        //myLocationToSend = ["lat" : myLocation!.coordinate.latitude as Double,"lng" :myLocation!.coordinate.longitude as Double, "tm_stmp" : "\(date)", "bat_lvl" : UIDevice.current.batteryLevel * 100, "gps":1,"acc":(self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300), "d_acc":"1"]
                        self.setLogsForDevelopmentMode(locationData: "First Location", status: "ApplyFilter", hitResponse: "true", latitude: nil, longitude: nil, locationAccuracy: nil)
                        self.addFilteredLocationToLocationArray(myLocationToSend)
                        self.myLastLocation = self.myLocation
                    } else {
                        let speedValue = self.getSpeed()
                        if( speedValue < 35.0) {
                            UserDefaults.standard.set(locationDict, forKey: USER_DEFAULT.lastAccurateUserLocation)
                            //var myLocationToSend = [String:Any]()
                            let date = myLocation.timestamp.timeIntervalSince1970 * 1000
                           // let timestamp = Auxillary.getUTCDateString()
                            var myLocationToSend:[String:Any] = ["lat" : myLocation!.coordinate.latitude as Double]
                            myLocationToSend["lng"] = myLocation!.coordinate.longitude as Double
                            myLocationToSend["tm_stmp"] = "\(date)"
                            myLocationToSend["bat_lvl"] = UIDevice.current.batteryLevel * 100
                            myLocationToSend["acc"] = (self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300)
                            myLocationToSend["d_acc"] = "1"
                            myLocationToSend["gps"] = 1
                            
                            //myLocationToSend = ["lat" : myLocation!.coordinate.latitude as Double,"lng" :myLocation!.coordinate.longitude as Double, "tm_stmp" : "\(date)", "bat_lvl" : UIDevice.current.batteryLevel * 100, "gps":1,"acc":(self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300), "d_acc":"1"]
                            self.setLogsForDevelopmentMode(locationData: "SpeedCheckPassed", status: "speed = \(speedValue)ms", hitResponse: "readyToSend", latitude: nil, longitude: nil, locationAccuracy: nil)
                            self.addFilteredLocationToLocationArray(myLocationToSend)
                            self.myLastLocation = self.myLocation
                        } else {
                            self.setLogsForDevelopmentMode(locationData: "No server Hit", status: "speed = \(speedValue)ms", hitResponse: "SpeedCheckFailed", latitude: nil, longitude: nil, locationAccuracy: nil)
                        }
                    }
                } else {
                    self.setLogsForDevelopmentMode(locationData: "No server Hit", status: "Accuracy Check Failed", hitResponse: "Fail", latitude: nil, longitude: nil, locationAccuracy: nil)
                }
            }
           
            
            if(UserDefaults.standard.value(forKey: USER_DEFAULT.applicationMode) != nil && UserDefaults.standard.value(forKey: USER_DEFAULT.applicationMode) as! String == "Background") {
                if(UserDefaults.standard.bool(forKey: USER_DEFAULT.isHitInProgress) == false) {
                    if locationArray.count >= 5 {
                        let locationString = locationArray.jsonString
                        sendRequestToServer(locationString, status: "DirectFromDidUpdateLocation", getLocationLatitude: 0.0, getLocationLongitude: 0.0, getLocationAccuracy: 0.0)
                    }
                } else {
                    self.setLogsForDevelopmentMode(locationData: "No server Hit", status: "HitInProgressCheckFailed", hitResponse: "Fail", latitude: nil, longitude: nil, locationAccuracy: nil)
                }
            }
        }
    }
    
    func addFilteredLocationToLocationArray(_ myLocationToSend:[String:Any]) {
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil) {
            if(UserDefaults.standard.integer(forKey: USER_DEFAULT.userAvailableStatus) == DutyStatus.onDuty){
                var locationArray = [Any]()
                if let array = UserDefaults.standard.object(forKey: USER_DEFAULT.locationArray) as? [Any] {
                    locationArray = array
                }
                if(locationArray.count >= 1000) {
                    locationArray.remove(at: 0)
                }
                locationArray.append(myLocationToSend)
                 UserDefaults.standard.set(locationArray, forKey: USER_DEFAULT.locationArray)
            }
        }
    }
    
    func updateLocationToServer() {
        var locationArray = [Any]()
        if let array = UserDefaults.standard.object(forKey: USER_DEFAULT.locationArray) as? [Any] {
            locationArray = array
        }
        if IJReachability.isConnectedToNetwork(){
            if(UserDefaults.standard.bool(forKey: USER_DEFAULT.isHitInProgress) == false) {
                if locationArray.count > 0 {
                    let locationString = locationArray.jsonString
                    sendRequestToServer(locationString, status: "Timer", getLocationLatitude: 0.0, getLocationLongitude: 0.0, getLocationAccuracy: 0.0)
                }
            }
        }
    }
    
    func sendRequestToServer(_ locationString:String, status:String, getLocationLatitude:CLLocationDegrees, getLocationLongitude:CLLocationDegrees, getLocationAccuracy:CLLocationAccuracy) {
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil) {
             if(UserDefaults.standard.integer(forKey: USER_DEFAULT.userAvailableStatus) == DutyStatus.onDuty){
                var locationArray = [Any]()
                if let array = UserDefaults.standard.object(forKey: USER_DEFAULT.locationArray) as? [Any]{
                    locationArray = array
                }
                UserDefaults.standard.set(true, forKey: USER_DEFAULT.isHitInProgress)
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                NetworkingHelper.sharedInstance.updatingFleetLocation(locationString, status: status, locationUpdateMode: UserDefaults.standard.value(forKey: USER_DEFAULT.applicationMode) as! String, getLocationLatitude: getLocationLatitude, getLocationLongitude: getLocationLongitude, getLocationAccuracy: getLocationAccuracy, receivedResponse: { (succeeded, response) -> () in
                    if(succeeded == true) {
                        DispatchQueue.main.async(execute: { () -> Void in
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            
                            if let _ = response["data"] as? NSDictionary {
                                switch(response["status"] as! Int) {
                                case STATUS_CODES.SHOW_DATA:
                                    var sentLocationArray = [Any]()
                                    if let sentLocationString = response["location"] as? String {
                                        sentLocationArray = sentLocationString.jsonObjectArray
                                    }
                                    for i in (0..<sentLocationArray.count) {
                                        let sendDictionaryObject = sentLocationArray[i] as! [String:Any]
                                        if let sendTimeStamp = sendDictionaryObject["tm_stmp"] as? String {
                                            for j in (0..<locationArray.count) {
                                                let locationDictionaryObject = locationArray[j] as! [String:Any]
                                                if let locationTimeStamp = locationDictionaryObject["tm_stmp"] as? String {
                                                    if(sendTimeStamp == locationTimeStamp) {
                                                        locationArray.remove(at: j)
                                                        break
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                    UserDefaults.standard.set(locationArray, forKey: USER_DEFAULT.locationArray)
                                    UserDefaults.standard.synchronize()
                                    break
                                case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                    UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                    self.stopLocationTracking()
                                    DispatchQueue.main.async(execute: { () -> Void in
                                        Auxillary.logoutFromDevice()
                                        NotificationCenter.default.removeObserver(self)
                                        let navigationController = (UIApplication.shared.delegate as! AppDelegate).navigationController as UINavigationController
                                        if navigationController.viewControllers.count >= 2 {
                                            _ = navigationController.popToViewController((navigationController.viewControllers[1]), animated: true)
                                        }
                                    })
                                    break
                                default:
                                    UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                    UserDefaults.standard.set(locationArray, forKey: USER_DEFAULT.locationArray)
                                    UserDefaults.standard.synchronize()
                                    break
                                }
                            } else {
                                UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                                UserDefaults.standard.set(locationArray, forKey: USER_DEFAULT.locationArray)
                                UserDefaults.standard.synchronize()
                            }
                        })
                    } else {
                        DispatchQueue.main.async(execute: { () -> Void in
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            UserDefaults.standard.set(false, forKey: USER_DEFAULT.isHitInProgress)
                        })
                    }
                })
            }
        }
    }
    
    func enterInForegroundFromBackground(){
        if self.checkLocationAuthorization() == true {
           self.checkToStartLocationTracking()
        }
    }
    
    func updateLastSavedLocationOnServer() {
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil) {
            var locationArray = [Any]()
            if let array = UserDefaults.standard.object(forKey: USER_DEFAULT.locationArray) as? [Any]{
                locationArray = array
            }
            self.checkToStartLocationTracking()
            //self.setLocationUpdate()
            if(UserDefaults.standard.bool(forKey: USER_DEFAULT.isHitInProgress) == false) {
                if(locationArray.count > 0) {
                    sendRequestToServer(locationArray.jsonString, status: "Silent", getLocationLatitude: 0.0, getLocationLongitude: 0.0, getLocationAccuracy: 0.0)
                } else {
                    var myLocationToSend = NSMutableDictionary()
                    myLocationToSend = ["bat_lvl" : UIDevice.current.batteryLevel * 100]
                    let highLocationArray = NSMutableArray()
                    highLocationArray.add(myLocationToSend)
                    let locationString = highLocationArray.jsonString
                    sendRequestToServer(locationString, status: "Silent", getLocationLatitude: 0.0, getLocationLongitude: 0.0, getLocationAccuracy: 0.0)
                }
            } else {
                self.setLogsForDevelopmentMode(locationData: "No server Hit", status: "Silent - hitInProgress", hitResponse: "Fail", latitude: nil, longitude: nil, locationAccuracy: nil)
            }
        }
    }
    
    func getSpeed() -> Float {
        if(myLastLocation != nil) {
            let time = self.myLocation.timestamp.timeIntervalSince(myLastLocation.timestamp)
            let distance:CLLocationDistance = myLocation.distance(from: myLastLocation)
            if(distance > 200) {
                self.locationManager.stopUpdatingLocation()
                self.getPathFromGoogle()
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.setLocationUpdate()
                speed = Float(distance) / Float(time)
                if(speed > 0) {
                    return speed
                }
                return 0.0
            } else {
                speed = Float(distance) / Float(time)
                if(speed > 0) {
                    return speed
                }
                return 0.0
            }
        }
        return 0.0
    }
    
    func getPathFromGoogle() {
        if let json = NetworkingHelper.sharedInstance.getLatLongFromDirectionAPI("\(myLastLocation.coordinate.latitude),\(myLastLocation.coordinate.longitude)", destination: "\(myLocation.coordinate.latitude),\(myLocation.coordinate.longitude)") {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let routes = json["routes"] as? [AnyObject] {
                if(routes.count > 0) {
                    if let legs = routes[0]["legs"] as? [AnyObject] {
                        if(legs.count > 0) {
                            if let distance = legs[0]["distance"] as? [String:Any] {
                                if let _ = distance["value"] as? Int {
                                    if let overviewPolyline = routes[0]["overview_polyline"] as? [String:Any] {
                                        if let polyline = overviewPolyline["points"] as? String {
                                            let locations = TaskDataModel().decodePolylineForCoordinates(polyline) as [CLLocation]
                                            for i in (0..<locations.count) {
                                                let locationDict = ["latitude" : "\(locations[i].coordinate.latitude)", "longitude" : "\(locations[i].coordinate.longitude)"]
                                                UserDefaults.standard.set(locationDict, forKey: USER_DEFAULT.lastAccurateUserLocation)
                                                
                                                var myLocationToSend = [String:Any]()
                                                let date = locations[i].timestamp.timeIntervalSince1970 * 1000
                                                // let timestamp = Auxillary.getUTCDateString()
                                                if(self.myLocationAccuracy < 20) {
                                                    myLocationToSend["lat"] = locations[i].coordinate.latitude as Double
                                                    myLocationToSend["lng"] = locations[i].coordinate.longitude as Double
                                                    myLocationToSend["tm_stmp"] = "\(date)"
                                                    myLocationToSend["bat_lvl"] = UIDevice.current.batteryLevel * 100
                                                    myLocationToSend["acc"] = (self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300)
                                                    myLocationToSend["d_acc"] = "1"
                                                    myLocationToSend["status"] = "1"
                                                    myLocationToSend["gps"] = 1
                                                    //myLocationToSend = ["lat" : locations[i].coordinate.latitude as Double,"lng" :locations[i].coordinate.longitude as Double, "tm_stmp" : "\(date)", "bat_lvl" : UIDevice.current.batteryLevel * 100, "gps":1,"acc":(self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300), "d_acc":"1","status":"1"]
                                                } else {
                                                    myLocationToSend["lat"] = locations[i].coordinate.latitude as Double
                                                    myLocationToSend["lng"] = locations[i].coordinate.longitude as Double
                                                    myLocationToSend["tm_stmp"] = "\(date)"
                                                    myLocationToSend["bat_lvl"] = UIDevice.current.batteryLevel * 100
                                                    myLocationToSend["acc"] = (self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300)
                                                    myLocationToSend["d_acc"] = "1"
                                                    myLocationToSend["gps"] = 1
                                                    //myLocationToSend = ["lat" : locations[i].coordinate.latitude as Double,"lng" :locations[i].coordinate.longitude as Double, "tm_stmp" : "\(date)", "bat_lvl" : UIDevice.current.batteryLevel * 100, "gps":1,"acc":(self.myLocationAccuracy != nil ? self.myLocationAccuracy! : 300), "d_acc":"1"]
                                                }
                                                self.addFilteredLocationToLocationArray(myLocationToSend)
                                                self.setLogsForDevelopmentMode(locationData: "GooglePath", status: "Google", hitResponse: "True", latitude: nil, longitude: nil, locationAccuracy: nil)
                                                self.myLastLocation = CLLocation(latitude: locations[i].coordinate.latitude, longitude: locations[i].coordinate.longitude)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func setLogsForDevelopmentMode(locationData:String, status:String, hitResponse:String, latitude:CLLocationDegrees?, longitude:CLLocationDegrees?, locationAccuracy:CLLocationAccuracy?) {
        if(APP_STORE.value == 0) {
            var locationUpdatedOnServerDictionary = [String:Any]()
            locationUpdatedOnServerDictionary["GetLat"] = latitude != nil ? latitude : self.myLocation != nil ? self.myLocation.coordinate.latitude : 0.0
            locationUpdatedOnServerDictionary["GetLong"] = longitude != nil ? longitude : self.myLocation != nil ? self.myLocation.coordinate.longitude : 0.0
            locationUpdatedOnServerDictionary["GetLocationAccuracy"] = locationAccuracy != nil ? locationAccuracy : self.myLocation != nil ? self.myLocationAccuracy! : 0.0
            locationUpdatedOnServerDictionary["locationData"] = locationData
            locationUpdatedOnServerDictionary["TimeStamp"] = Date()
            locationUpdatedOnServerDictionary["Status"] = status
            locationUpdatedOnServerDictionary["Mode"] = UserDefaults.standard.value(forKey: USER_DEFAULT.applicationMode) as! String
            locationUpdatedOnServerDictionary["LocationBatteryUsage"] = UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)
            locationUpdatedOnServerDictionary["HitResponse"] = hitResponse
            locationUpdatedOnServerDictionary["Battery"] = UIDevice.current.batteryLevel * 100
            var locationUpdatedOnServerArray = [Any]()
            if let array = UserDefaults.standard.value(forKey: "SavedLocations") as? [Any]{
                locationUpdatedOnServerArray = array
            }
            locationUpdatedOnServerArray.append(locationUpdatedOnServerDictionary)
            UserDefaults.standard.setValue(locationUpdatedOnServerArray, forKey: "SavedLocations")
        }
    }
    
    
    func appEnterInTerminateState() {
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.high, BATTERY_USAGE.low, BATTERY_USAGE.medium:
            if(self.locationManager != nil) {
                self.locationManager.stopUpdatingLocation()
                guard Singleton.sharedInstance.getAccessToken() != "" else {
                    return
                }
                
                guard UserDefaults.standard.integer(forKey: USER_DEFAULT.userAvailableStatus) == DutyStatus.onDuty else {
                    return
                }
                self.locationManager.startMonitoringSignificantLocationChanges()
            }
            break
        default:
            break
        }
    }
    
    func appEnterInBackground() {
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.high, BATTERY_USAGE.low, BATTERY_USAGE.medium:
            self.checkToStartLocationTracking()
            //self.setLocationUpdate()
            self.shareModel!.bgTask = BackgroundTaskManager.sharedBackgroundTaskManager()
            _ = self.shareModel!.bgTask!.beginNewBackgroundTask()
            break
        case BATTERY_USAGE.foreground:
            if self.locationManager != nil {
                self.locationManager.stopUpdatingLocation()
            }
            break
        default:
            break
        }
    }
    
    func checkAuthorizationStatus(_ launchOptions:[UIApplicationLaunchOptionsKey: Any]?) {
        //We have to make sure that the Background App Refresh is enable for the Location updates to work in the background.
        if self.checkLocationAuthorization() == true {
            // When there is a significant changes of the location,
            // The key UIApplicationLaunchOptionsLocationKey will be returned from didFinishLaunchingWithOptions
            // When the app is receiving the key, it must reinitiate the locationManager and get
            // the latest location updates
            
            // This UIApplicationLaunchOptionsLocationKey key enables the location update even when
            // the app has been killed/terminated (Not in th background) by iOS or the user.
            if(launchOptions != nil) {
                if let launchDictionary = launchOptions {
                    if let remoteNotifications = launchDictionary[UIApplicationLaunchOptionsKey.remoteNotification] as? UIApplicationLaunchOptionsKey {
                       // NSLog("UIApplicationLaunchOptionsLocationKey : \(remoteNotifications.locations)");
                            // This "afterResume" flag is just to show that he receiving location updates
                            // are actually from the key "UIApplicationLaunchOptionsLocationKey"
                        self.checkToStartLocationTracking()
                    }
                }
            } else {
                self.checkToStartLocationTracking()
            }
        }
    }
    
    func checkLocationAuthorization() -> Bool {
        if(UIApplication.shared.backgroundRefreshStatus == UIBackgroundRefreshStatus.denied) {
            DispatchQueue.main.async {
                if(UIApplication.shared.applicationState == UIApplicationState.active) {
//                    UIAlertView(title: "", message: "The app doesn't work without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh", delegate: nil, cancelButtonTitle: "OK").show()
                    self.popupForBackgroundRefresh(message: "The app doesn't work without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh")
                }
            }
            return false
        } else if (UIApplication.shared.backgroundRefreshStatus == UIBackgroundRefreshStatus.restricted) {
            DispatchQueue.main.async {
                if(UIApplication.shared.applicationState == UIApplicationState.active) {
//                    UIAlertView(title: "", message: "The functions of this app are limited because the Background App Refresh is disable.", delegate: nil, cancelButtonTitle: "OK").show()
                    self.popupForBackgroundRefresh(message: "The functions of this app are limited because the Background App Refresh is disable.")
                }
            }
            return false
        } else if CLLocationManager.locationServicesEnabled() == false {
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NotifRequestingResponse.locationServicesDisabled.rawValue), object: self)
            return false
        }
        else {
            let authorizationStatus = CLLocationManager.authorizationStatus()
            if authorizationStatus == CLAuthorizationStatus.denied || authorizationStatus == CLAuthorizationStatus.restricted {
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NotifRequestingResponse.locationServicesDisabled.rawValue), object: self)
                return false
            }
            return true
        }
    }
    
    func getLastLocation() -> CLLocation! {
        if let userlocationDict = UserDefaults.standard.object(forKey: USER_DEFAULT.lastAccurateUserLocation) as? [String:Any] {
            let latitudeString = userlocationDict["latitude"] as! NSString
            let longitudeString = userlocationDict["longitude"] as! NSString
            return CLLocation(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
        }
        return nil
    }
    
    func getLatestLocationForForegroundMode() -> CLLocation! {
        if self.myLocation != nil && self.myLocation.coordinate.latitude > 0.0 {
            return self.myLocation
        } else if let lastLocation = self.getLastLocation() {
            return lastLocation
        } else {
            return nil
        }
     }
    
    func checkToStartLocationTracking() {
        guard Singleton.sharedInstance.getAccessToken() != "" else {
            self.stopLocationTracking()
            return
        }
        
        guard UserDefaults.standard.integer(forKey: USER_DEFAULT.userAvailableStatus) == DutyStatus.onDuty else {
            self.stopLocationTracking()
            return
        }
        self.startLocationTracking()
    }
    
    func popupForBackgroundRefresh(message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK".localized, style: UIAlertActionStyle.default){
            UIAlertAction in
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
        }
        alert.addAction(okAction)
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
                appDelegate.window!.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
    }
}

