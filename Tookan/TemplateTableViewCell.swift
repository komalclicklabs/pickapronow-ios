//
//  TemplateTableViewCell.swift
//  SearchTemplate
//
//  Created by CL-Macmini-110 on 7/27/17.
//  Copyright © 2017 CL-Macmini-110. All rights reserved.
//

import UIKit

class TemplateTableViewCell: UITableViewCell {

    @IBOutlet weak var templateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.templateLabel.numberOfLines = 0
        self.templateLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.templateLabel.textColor = COLOR.TEXT_COLOR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
