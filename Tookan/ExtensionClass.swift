//
//  ExtensionClass.swift
//  Tookan
//
//  Created by Rakesh Kumar on 4/28/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
private var tempColor = UIColor.black
class ExtensionClass: NSObject {

}


extension UILabel {
    func setLetterSpacing(value:CGFloat){
        let attributedString = NSMutableAttributedString(string: text!)
        attributedString.addAttribute(NSKernAttributeName, value: value, range: NSRange(location: 0, length: text!.characters.count))
        attributedText = attributedString
    }
}


//MARK: UITableView
extension UITableView {
    func setMyTableHeaderView(_ headerHeight:CGFloat) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: headerHeight))
        view.backgroundColor = UIColor.clear
        self.tableHeaderView = view
    }
    
    func setMyTableFooterView(_ footerHeight:CGFloat) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: footerHeight))
        view.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
        view.addSubview(Auxillary.drawSingleLine(0, yOrigin: footerHeight - 0.5, lineWidth: self.frame.width, lineHeight: 0.5))
        self.tableFooterView = view
    }
    
    func setMyTableBackgroundView(_ backgroundText:String) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 50))
        label.text = backgroundText
        label.textColor = UIColor().textColor
        label.font = UIFont(name: UIFont().MontserratRegular, size: 14.0)
        label.center = CGPoint(x: view.center.x, y: view.center.y - 70)
        label.textAlignment = NSTextAlignment.center
        
        view.addSubview(label)
        self.backgroundView = view
    }
    
    func isIndexPathExist(indexPath: IndexPath) -> Bool {
        guard self.isSectionExist(section: indexPath.section) == true else {
            return false
        }
        
        guard indexPath.row < self.numberOfRows(inSection: indexPath.section) else {
            return false
        }
        return true
    }
    
    func isSectionExist(section:Int) -> Bool {
        if section < self.numberOfSections {
            return true
        }
        return false
    }
    
    func roundCornerForTable(corners:UIRectCorner, radius: CGFloat, width: CGFloat, height: CGFloat) {
        let path = UIBezierPath(roundedRect: CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y, width: width, height: height), byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

//MARK: NSMutableAttributedString
extension NSMutableAttributedString {
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: 10000)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        return ceil(boundingBox.height)
    }
    
    func setSpacingAttribute(value:CGFloat) {
        self.addAttribute(NSKernAttributeName, value: value, range: NSRange(location: 0, length: self.length))
    }
}

//MARK: CALayer
extension CALayer{
    public var borderUIColor : UIColor!{
        get {
            borderColor = tempColor.cgColor
            return tempColor
        }
        set(newValue){
            tempColor = newValue
            borderColor = tempColor.cgColor
        }
    }
}

//MARK: UIFONT
extension UIFont {
    func sizeOfString (_ string: String, constrainedToWidth width: Double) -> CGFloat {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: DBL_MAX),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: self],
            context: nil).size.height
    }
    
    var MontserratBlack:String {
        return "Montserrat-Black"
    }
    
    var MontserratBold:String {
        return "Montserrat-Bold"
    }

    var MontserratExtraBold:String {
        return "Montserrat-ExtraBold"
    }
    
    var MontserratExtraLight:String {
        return "Montserrat-ExtraLight"
    }
    
    var MontserratLight:String {
        return "Montserrat-Light"
    }

    var MontserratMedium:String {
        return "Montserrat-Medium"
    }
    
    var MontserratRegular:String {
        return "Montserrat-Regular"
    }

    var MontserratSemiBold:String {
        return "Montserrat-SemiBold"
    }
    
    var MontserratThin:String {
        return "Montserrat-Thin"
    }
    
    var BebasNeueRegular:String {
        return"BebasNeueRegular"
    }
}

//MARK: UIVIEW
extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat, width: CGFloat, height: CGFloat) {
        let path = UIBezierPath(roundedRect: CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y, width: width, height: height), byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    var safeAreaInsetsForAllOS: UIEdgeInsets {
        var insets: UIEdgeInsets
        if #available(iOS 11.0, *) {
            insets = safeAreaInsets
        } else {
            insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        return insets
    }
}

//MARK: Bundle
extension Bundle {
    var displayName: String? {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
    }
}

//MARK: String
extension String {
    var asDate:Date! {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.timeZone =  TimeZone(identifier: "UTC")
        styler.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if(styler.date(from: self) == nil) {
            styler.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }
        return styler.date(from: self)
    }
    
    func asDateFormattedWith(_ format:String) -> Date! {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.dateFormat = format
        styler.timeZone = TimeZone(identifier: "UTC")
        return styler.date(from: self)!
    }
    
    func datefromYourInputFormatToOutputFormat(input:String,output:String)-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = input
        //dateFormatter.timeZone = TimeZone(abbreviation:  "UTC")
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = output
        //dateFormatter.timeZone = NSTimeZone.local
        guard date != nil else {
            return ""
        }
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    func getSelectedLocaleDateFormat(input:String,output:String)-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!
        dateFormatter.dateFormat = input
        //dateFormatter.timeZone = TimeZone(abbreviation:  "UTC")
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = output
        //dateFormatter.timeZone = NSTimeZone.local
        guard date != nil else {
            return ""
        }
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    func blank(_ text: String) -> Bool {
        let trimmed = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmed.isEmpty
    }
    
    var trimText:String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var localized: String {
        if let path = Bundle.main.path(forResource: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String, ofType: "lproj") {
            let bundle = Bundle(path: path)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        }
        return self
    }
    
    var length: Int {
        return self.characters.count
    }
    
    var jsonObject: NSMutableArray {
        do {
            let value = try JSONSerialization.jsonObject(with: self.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.mutableLeaves)
            return (NSMutableArray(array: value as! [AnyObject]))
        } catch {
            print("Error")
        }
        return NSMutableArray()
    }
    
    var jsonObjectArray: [Any] {
        do {
            let value = try JSONSerialization.jsonObject(with: self.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? [Any]
            return value!
        } catch {
            print("Error")
        }
        return []
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGRect {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox
    }
    
}

//MARK: NSDate
extension Date {
    var startOfWeek: Date? {
        let cal = Calendar.current
        var comps = cal.dateComponents([.weekOfYear, .yearForWeekOfYear], from: self)
        comps.weekday = 2 // Monday
        comps.timeZone = TimeZone.current
        let mondayInWeek = cal.date(from: comps)!
        return mondayInWeek
    }
    
    var formatted:String {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "dd MMM, yyyy  h:mm a"
        return formatter.string(from: self)
    }
    
    var formattedForDate:String {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "dd MMM, yyyy"
        return formatter.string(from: self)
    }
    
    var formattedForTime:String {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm a"
        return formatter.string(from: self)
    }
    
    func formattedWith(_ format:String) -> String {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.dateFormat = format
        formatter.timeZone = TimeZone.current
        return formatter.string(from: self)
    }
    
    var getLocalDateString:String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: self)
    }
    
    var getDateStringWithUnderscore:String {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.timeZone = TimeZone(abbreviation:  "UTC")
        styler.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        return styler.string(from: Date())
    }
}

//MARK: NSMutableArray
extension NSMutableArray {
    var jsonString:String {
        do {
            let dataObject:Data? = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let data = dataObject {
                let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                if let json = json {
                    return json as String
                }
            }
        } catch {
            print("Error")
        }
        return ""
    }
}

//MARK: Array
extension Array {
    var jsonString:String {
        do {
            let dataObject:Data? = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let data = dataObject {
                let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                if let json = json {
                    return json as String
                }
            }
        } catch {
            print("Error")
        }
        return ""
    }
}


//MARK: UIButton
extension UIButton {
    func setShadow() {
        self.layer.shadowOffset = CGSize(width: 2, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        self.layer.shadowRadius = 3
    }
}


//MARK: NSDictionary
extension Dictionary {
    var jsonString:String {
        do {
            
//            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
//            // here "jsonData" is the dictionary encoded in JSON data
//            
//            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
//            // here "decoded" is an `AnyObject` decoded from JSON data
//            
//            // you can now cast it with the right type
//            if let dictFromJSON = decoded as? [String:String] {
//                // use dictFromJSON
//            }
            
            
            let dataObject:Data? = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let data = dataObject {
                let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                if let json = json {
                    return json as String
                }
            }
        } catch {
            print("Error")
        }
        return ""
    }
}

//MARK: NSLocale
extension NSLocale {
    struct locale {
        let countryCode: String
        let countryName: String
    }
    
    class func locales() -> [locale] {
        var locales = [locale]()
        for localeCode in NSLocale.isoCountryCodes {
            let countryName = Locale.current.localizedString(forRegionCode: localeCode)
           // let countryName = NSLocale().displayName(forKey: NSLocale.Key.countryCode, value: localeCode)!
            let countryCode = localeCode
            let loc = locale(countryCode: countryCode, countryName: countryName!)
            locales.append(loc)
        }
        return locales
    }
}

//MARK: NSURLSession
extension URLSession {
    
    /// Return data from synchronous URL request
    public static func requestSynchronousData(_ request: Foundation.URLRequest) -> Data? {
        var data: Data? = nil
        let semaphore: DispatchSemaphore = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            taskData, _, error -> () in
            data = taskData
            if data == nil, let error = error {print(error)}
            semaphore.signal();
        })
        task.resume()
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        return data
    }
    
    /// Return data synchronous from specified endpoint
    public static func requestSynchronousDataWithURLString(_ requestString: String) -> Data? {
        guard let url = URL(string:requestString) else {return nil}
        let request = Foundation.URLRequest(url: url)
        return URLSession.requestSynchronousData(request)
    }
    
    /// Return JSON synchronous from URL request
    public static func requestSynchronousJSON(_ request: Foundation.URLRequest) -> Any? {
        guard let data = URLSession.requestSynchronousData(request) else {return nil}
        return try? JSONSerialization.jsonObject(with: data, options: [])
    }
    
    /// Return JSON synchronous from specified endpoint
    public static func requestSynchronousJSONWithURLString(_ requestString: String) -> Any? {
        guard let url = URL(string: requestString) else {return nil}
        let request = NSMutableURLRequest(url:url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return URLSession.requestSynchronousJSON(request as URLRequest)
    }
}

//MARK: UIColor
extension UIColor {
    var lineColor:UIColor {
        return UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
    }
    
    var taskTableSectionHeaderColor:UIColor {
        return UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1.0)
    }
    
    var blueLineColor:UIColor {
        return UIColor(red: 21/255, green: 146/255, blue: 206/255, alpha: 1.0)
    }
    
    var textColor:UIColor {
        return UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1.0)
    }
    
    var disabledColor:UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
    }
    
}

//MARK: UIImageView 
extension UIImageView {
    func setImageUrl(urlString:String, placeHolderImage:UIImage, indexPath:IndexPath, view:AnyObject) {
        if let cacheImage = NetworkingHelper.sharedInstance.allImagesCache.object(forKey: "\(urlString)" as NSString) {//image caching
            self.image = cacheImage
            return
        } else {
            // The image isn't cached, download the img data
            // We should perform this in a background thread
            self.downloadImageAsynchronously(urlString: urlString, placeHolderImage: placeHolderImage, indexPath: indexPath, view: view, receivedResponse: { [weak self](isSucceeded, updateIndexPath, updateView, downloadedImage) in
                DispatchQueue.main.async {
                    if isSucceeded == true {
                        guard self != nil else {
                            return
                        }
                        if updateView.isKind(of: UICollectionView.classForCoder()) == true {
                            let collectionView = updateView as! UICollectionView
                            if (collectionView.cellForItem(at: updateIndexPath) as? ImageCollectionCell) != nil {   //collectionView.cellForRow(at: indexPath) {
                                //cellToUpdate.imageView?.image = downloadedImage
                                //collectionView.reloadItems(at: [updateIndexPath])
                                collectionView.reloadData()
                            }
                        }
                        return
                    } else {
                       self?.image = downloadedImage
                       return
                    }
                }
            })
        }
        self.image = placeHolderImage
    }
    
    func downloadImageAsynchronously(urlString:String,placeHolderImage:UIImage, indexPath:IndexPath, view:AnyObject,receivedResponse:@escaping (_ succeeded:Bool, _ indexPath:IndexPath, _ view:AnyObject, _ image:UIImage) -> ()) {
        let updatedURLString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: updatedURLString! as String)
        let request: Foundation.URLRequest = Foundation.URLRequest(url: url!)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue,  completionHandler: { (response, data, error) -> Void in
            if error == nil {
                if updatedURLString == request.url!.absoluteString {
                    let image = UIImage(data: data!)
                    DispatchQueue.main.async(execute: {
                        if(image != nil) {
                            NetworkingHelper.sharedInstance.allImagesCache.setObject(image!, forKey: "\(urlString)" as NSString)
                            receivedResponse(true, indexPath, view, image!)
                        } else {
                            receivedResponse(false, indexPath, view, #imageLiteral(resourceName: "corruptedImage"))
                        }
                    })
                } else {
                    receivedResponse(false, indexPath, view, #imageLiteral(resourceName: "corruptedImage"))
                }
            }
            else {
                receivedResponse(false, indexPath, view, #imageLiteral(resourceName: "corruptedImage"))
            }
        })
    }
    
    func setImageUrl(urlString:String, placeHolderImage:UIImage) {
        guard  urlString.length > 0 else {
            self.image = placeHolderImage
            return
        }
        
        if let cacheImage = NetworkingHelper.sharedInstance.allImagesCache.object(forKey: "\(urlString)" as NSString) {//image caching
            self.image = cacheImage
            return
        } else {
            // The image isn't cached, download the img data
            // We should perform this in a background thread
            let url = URL(string: urlString as String)
            guard url != nil else {
                self.image = placeHolderImage
                return
            }
            let request: Foundation.URLRequest = Foundation.URLRequest(url: url!)
            let mainQueue = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue,  completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    if urlString == request.url!.absoluteString {
                        // Convert the downloaded data in to a UIImage object
                        let image = UIImage(data: data!)
                        // Store the image in to our cache
                        
                        DispatchQueue.main.async(execute: {
                            if(image != nil) {
                                NetworkingHelper.sharedInstance.allImagesCache.setObject(image!, forKey: "\(request.url!.absoluteString)" as NSString)
                                self.image = image
                                return
                            } else {
                                self.image = #imageLiteral(resourceName: "corruptedImage")
                                return
                            }
                        })
                    } else {
                        return
                    }
                }
                else {
                    self.image = #imageLiteral(resourceName: "corruptedImage")
                    return
                }
            })
        }
        self.image = placeHolderImage
    }
    
    func setImageFromDocumentDirectory(imagePath:String, placeHolderImage:UIImage) {
        self.image = placeHolderImage
        if let cacheImage = NetworkingHelper.sharedInstance.allImagesCache.object(forKey: "\(imagePath)" as NSString) {             //image caching
            DispatchQueue.main.async(execute: {
                self.image = cacheImage
                return
            })
        } else {
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                if(Singleton.sharedInstance.isFileExistAtPath(imagePath) == true) {
                    if let imageFromDocumentDirectory = UIImage(contentsOfFile: Auxillary.createPath(imagePath)) {
                        NetworkingHelper.sharedInstance.allImagesCache.setObject(imageFromDocumentDirectory, forKey: "\(imagePath)" as NSString)
                        DispatchQueue.main.async {
                            self.image = imageFromDocumentDirectory
                        }
                    }
                }
            }
        }
    }
}

extension UINavigationController {
    public func pushViewController(viewController: UIViewController,
                                   animated: Bool,
                                   completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
}

