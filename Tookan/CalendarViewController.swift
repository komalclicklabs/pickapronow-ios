//
//  CalendarViewController.swift
//  Tookan
//
//  Created by Click Labs on 7/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit
import Crashlytics
import FirebaseAnalytics
import DGActivityIndicatorView
class CalendarViewController: UIViewController,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate,UIScrollViewDelegate,  NavigationDelegate {
  
    @IBOutlet weak var activityIndicatorView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var backgroundLayerView: UIView!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    
    let scrollViewHeight:CGFloat = 273
    var collectionView1: UICollectionView?
    var collectionView2: UICollectionView?
    var collectionView3: UICollectionView?
    let cellIdentifier = "Cell"
    var nxtMonth = [String]()
    var prvMonth = [String]()
    var curMonth = [String]()
    var tasksDatesArray = [String]()
    let monthData = CalendarDataView()
    var onceLoad = true
    var monthNum: String!
    var yearNum: String!
    var accessToken: String!
    var datesArray = [MonthTasksList]()
    var monthScrollView: UIScrollView!
    /*--------- Set by Rakesh --------*/
    var internetToast:NoInternetConnectionView!
    let taskObjectContext = DatabaseManager.sharedInstance.managedObjectContext
    var previousOffsetValueOfScroll:CGFloat = 0
    var navigation:navigationBar!
    var weekdayArray = [String]()
    var pendingStatuslabel:UILabel!
    var totalTaskLabel:UILabel!
    var selectedIndexPath:IndexPath!
    var selectedMonthNum:String!
    var selectedDateNum:String!
    var selectedYearNum:String!
    var monthCounter = 0
    var todayButton:TodayButton!
    var activityIndicatorMain:DGActivityIndicatorView!
    @IBOutlet var previousButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        /*============= Month Label ==================*/
        self.monthLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
        self.monthLabel.textColor = COLOR.TEXT_COLOR
        self.monthLabel.setLetterSpacing(value: 0.6)
        
        self.backgroundLayerView.transform = CGAffineTransform(translationX: 0, y: -SCREEN_SIZE.height)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        tap.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tap)
        self.setActivityIndicator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        accessToken = Singleton.sharedInstance.getAccessToken()
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!
        weekdayArray = dateFormatter.veryShortWeekdaySymbols as Array
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if onceLoad {
            self.monthContentHandler()
            self.setCollectionsView()
            self.getMonthsDataArray()
            self.onceLoad = false
            self.activityIndicator.stopAnimating()
            self.setNavigationBar()
        } else {
            getMonthsDataArray()
        }
        self.animateView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func backgroundTouch() {
        self.editAction()
    }
    
    func animateView() {
        UIApplication.shared.statusBarStyle = .default
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.navigation.alpha = 1.0
            self.backgroundLayerView.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.backgroundColor = UIColor.white
        navigation.alpha = 0.0
        navigation.delegate = self
        navigation.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:HEIGHT.navigationHeight)
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        navigation.titleLabel.text = TEXT.CALENDAR
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.titleLabel.textColor = COLOR.TEXT_COLOR
        navigation.titleLabel.isUserInteractionEnabled = true
        navigation.backButton.isHidden = true
        navigation.editButton.isHidden = false
        navigation.editButton.setTitle("", for: .normal)
        navigation.editButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.editButton.tintColor = COLOR.TEXT_COLOR
        navigation.editButton.addTarget(self, action: #selector(self.editAction), for: .touchUpInside)
        navigation.bottomLine.isHidden = false
        navigation.editButton.contentHorizontalAlignment = .center
        self.todayButton = UINib(nibName: NIB_NAME.todayButton, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TodayButton
        self.todayButton.frame = CGRect(x: 5, y: Singleton.sharedInstance.getTopInsetofSafeArea() + 20, width: 88, height: 50)
        self.todayButton.delegate = self
        self.todayButton.isHidden = false
//        self.todayButton.backgroundColor = UIColor.yellow
        navigation.addSubview(todayButton)
        self.todayButton.updateDate()
        self.view.addSubview(navigation)
        
        /*=============== Tap gesture ================*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.editAction))
        tap.numberOfTapsRequired = 1
        navigation.titleLabel.addGestureRecognizer(tap)
        
    }
    
    func setActivityIndicator() {
        self.activityIndicatorMain = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballClipRotate, tintColor: COLOR.themeForegroundColor, size: 12.0)
        self.activityIndicatorMain.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        self.activityIndicatorMain.clipsToBounds = true
        self.activityIndicatorView.addSubview(self.activityIndicatorMain)
    }
    
    func backAction() {
        //_ = self.navigationController?.popViewController(animated: true)
    }
    
    func editAction() {
        UIView.animate(withDuration: 0.3, animations: {
            self.backgroundLayerView.transform = CGAffineTransform(translationX: 0, y: -self.heightConstraint.constant)
            self.navigation.alpha = 0.0
        }, completion: { finished in
            self.dismiss(animated: false, completion: { finished in
                if let mapStyle = (UserDefaults.standard.value(forKey: USER_DEFAULT.mapStyle) as? Int){
                    if (mapStyle == MAP_STYLE.dark){
                        UIApplication.shared.statusBarStyle = .lightContent
                    } else {
                        UIApplication.shared.statusBarStyle = .default
                    }
                }
            })
        })
         //_ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func previousMonth(_ sender: UIButton) {
        monthScrollView .scrollRectToVisible(CGRect(x: 0, y: 0, width: monthScrollView.frame.width, height: monthScrollView.frame.height), animated: true)
        swipeLeft()
    }
    
    @IBAction func nextMonth(_ sender: UIButton) {
        monthScrollView .scrollRectToVisible(CGRect(x: monthScrollView.contentOffset.x + monthScrollView.frame.width, y: 0, width: monthScrollView.frame.width, height: monthScrollView.frame.height), animated: true)
        swipeRight()
    }

    func monthContentHandler() {
        let screenSize: CGRect = UIScreen.main.bounds
        let monthCount = 3
        let start = getAspectRatioValue(value: 25)
        let widthOfLabel = getAspectRatioValue(value: 85)
        let heightOfLabel = getAspectRatioValue(value: 16)
        let heightOfView = getAspectRatioValue(value: 8)
        /*================== Scroll View =================*/
        self.monthScrollView = UIScrollView()
        self.monthScrollView.delegate = self
        self.monthScrollView.showsHorizontalScrollIndicator = false
        self.monthScrollView.frame = CGRect(x: 11, y: monthLabel.frame.origin.y + monthLabel.frame.size.height + 20, width: screenSize.width - 22, height: scrollViewHeight)
        self.monthScrollView.contentSize = CGSize(width: monthScrollView.frame.size.width * CGFloat(monthCount), height: scrollViewHeight)
        self.monthScrollView.isPagingEnabled = true
        
        
        /*================== Bottom Line ==================*/
        let line = UIView(frame: CGRect(x: start, y: monthScrollView.frame.origin.y + monthScrollView.frame.size.height + 1, width: SCREEN_SIZE.width - 50, height: 1))
        line.backgroundColor = COLOR.LINE_COLOR
        //self.backgroundLayerView.addSubview(line)
        
        
        /*=============== Task Label ===================*/
        let taskLabel = UILabel(frame: CGRect(x: start, y: monthScrollView.frame.origin.y + monthScrollView.frame.size.height + getAspectRatioValue(value: 5), width: 50, height: heightOfLabel))
        taskLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.medium)
        taskLabel.textColor = COLOR.TEXT_COLOR
        taskLabel.text = "\(TEXT.ORDERS)"// + " " + TEXT.PENDING
        self.backgroundLayerView.addSubview(taskLabel)
        
        
        /*=============== Pending Task View ===================*/
        let viewForPendingTask = UIView(frame: CGRect(x: taskLabel.frame.origin.x + taskLabel.frame.size.width + getAspectRatioValue(value: 15), y: taskLabel.frame.origin.y, width: heightOfView, height: heightOfView))
        viewForPendingTask.backgroundColor = COLOR.themeForegroundColor
        viewForPendingTask.layer.cornerRadius = heightOfView / 2
        self.backgroundLayerView.addSubview(viewForPendingTask)
        viewForPendingTask.center = CGPoint(x: viewForPendingTask.center.x, y:taskLabel.center.y)
        
        /*=============== Pending Task Label ===================*/
        pendingStatuslabel = UILabel(frame: CGRect(x: viewForPendingTask.frame.origin.x + viewForPendingTask.frame.size.width + getAspectRatioValue(value: 4), y: viewForPendingTask.frame.origin.y, width: widthOfLabel, height: heightOfLabel))
        pendingStatuslabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        pendingStatuslabel.textColor = COLOR.TEXT_COLOR
        pendingStatuslabel.text = "0 \(TEXT.PENDING)"// + " " + TEXT.PENDING
        pendingStatuslabel.center = CGPoint(x: pendingStatuslabel.center.x, y: viewForPendingTask.center.y)
        
        
        /*=============== Complete Task View ===================*/
        let viewForCompleteTask = UIView(frame: CGRect(x: pendingStatuslabel.frame.origin.x + pendingStatuslabel.frame.size.width + getAspectRatioValue(value: 10), y: pendingStatuslabel.frame.origin.y, width: heightOfView, height: heightOfView))
        viewForCompleteTask.backgroundColor = COLOR.LITTLE_LIGHT_COLOR
        viewForCompleteTask.layer.cornerRadius = heightOfView / 2
        self.backgroundLayerView.addSubview(viewForCompleteTask)
        viewForCompleteTask.center = CGPoint(x: viewForCompleteTask.center.x, y:taskLabel.center.y)
        
        /*=============== Complete Task Label ===================*/
        totalTaskLabel = UILabel(frame: CGRect(x: viewForCompleteTask.frame.origin.x + viewForCompleteTask.frame.size.width + getAspectRatioValue(value: 4), y: viewForCompleteTask.frame.origin.y, width: getAspectRatioValue(value: 95), height: heightOfLabel))
        totalTaskLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        totalTaskLabel.textColor = COLOR.TEXT_COLOR
        totalTaskLabel.text = "0 \(TEXT.COMPLETE)" // + " " + TEXT.COMPLETE
        totalTaskLabel.center = CGPoint(x: totalTaskLabel.center.x, y: viewForCompleteTask.center.y)
        
        /*=============== View Path Summary ==================*/
        let viewPathSummaryButton = UIButton(type: .custom)
        viewPathSummaryButton.frame = CGRect(x: start, y: taskLabel.frame.origin.y + taskLabel.frame.size.height + getAspectRatioValue(value: 25), width: SCREEN_SIZE.width / 2 - start - 15, height: 40)
        viewPathSummaryButton.backgroundColor = COLOR.themeForegroundColor
        viewPathSummaryButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        viewPathSummaryButton.setTitle(TEXT.VIEW_TRAVEL_SUMMARY, for: .normal)
        viewPathSummaryButton.titleLabel?.numberOfLines = 2
        viewPathSummaryButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        viewPathSummaryButton.layer.cornerRadius = 20.0
        viewPathSummaryButton.titleLabel?.textAlignment = .center
        viewPathSummaryButton.addTarget(self, action: #selector(self.viewTravelSummaryAction), for: .touchUpInside)
        
        /*=============== View Task Button ==============*/
        let viewTaskButton = UIButton(type: .custom)
        viewTaskButton.frame = CGRect(x:SCREEN_SIZE.width - viewPathSummaryButton.frame.width - start, y: viewPathSummaryButton.frame.origin.y, width: viewPathSummaryButton.frame.width, height: 40)
        viewTaskButton.backgroundColor = COLOR.themeForegroundColor
        viewTaskButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
        viewTaskButton.setTitle(TEXT.VIEW_TASK, for: .normal)
        viewTaskButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        viewTaskButton.layer.cornerRadius = 20.0
        viewTaskButton.addTarget(self, action: #selector(self.viewTaskAction), for: .touchUpInside)
        //viewTaskButton.center = CGPoint(x: viewTaskButton.center.x, y: totalTaskLabel.center.y)

        
        /*================== Set Height Constraint =====================*/
        self.heightConstraint.constant = viewTaskButton.frame.origin.y + viewTaskButton.frame.size.height + 20
        
        /*================== Set Shadow BackgroundLayerView =====================*/
        self.backgroundLayerView.layer.masksToBounds = false
        self.backgroundLayerView.layer.shadowOffset = CGSize(width: 5, height: 10)
        self.backgroundLayerView.layer.shadowOpacity = 0.7
        self.backgroundLayerView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        self.backgroundLayerView.layer.shadowRadius = 3
        
        self.backgroundLayerView.addSubview(pendingStatuslabel)
        self.backgroundLayerView.addSubview(totalTaskLabel)
        self.backgroundLayerView.addSubview(monthScrollView)
        self.backgroundLayerView.addSubview(viewTaskButton)
        self.backgroundLayerView.addSubview(viewPathSummaryButton)
    }
    
    func updateTaskCount(complete:String, pending:String) {
        totalTaskLabel.text = "\(complete) \(TEXT.COMPLETE)"// + TEXT.COMPLETE
        pendingStatuslabel.text = "\(pending) \(TEXT.PENDING)"// + TEXT.PENDING
    }
    
    func setCollectionsView() {
        previousOffsetValueOfScroll = monthScrollView.frame.width
        var month:Int = 0
        
        let frame = monthScrollView.bounds
        monthScrollView .scrollRectToVisible(CGRect(x: monthScrollView.frame.size.width, y: frame.origin.y, width: monthScrollView.bounds.width, height: monthScrollView.bounds.height), animated: false)
        
        collectionView1 = UICollectionView(frame: CGRect(x: frame.size.width * CGFloat(month), y: frame.origin.y, width: monthScrollView.bounds.width, height: monthScrollView.bounds.height), collectionViewLayout: self.createLayOut())
        self.createCollectionV(collectionView1!, tag:1)
        monthScrollView.addSubview(collectionView1!)
        
        month = 1
        collectionView2 = UICollectionView(frame: CGRect(x: frame.size.width * CGFloat(month), y: frame.origin.y, width: monthScrollView.bounds.width, height: monthScrollView.bounds.height), collectionViewLayout: self.createLayOut())
        self.createCollectionV(collectionView2!, tag: 2)
        monthScrollView.addSubview(collectionView2!)
        
        month = 2
        collectionView3 = UICollectionView(frame: CGRect(x: frame.size.width * CGFloat(month), y: frame.origin.y, width: monthScrollView.bounds.width, height: monthScrollView.bounds.height), collectionViewLayout: self.createLayOut())
        self.createCollectionV(collectionView3!, tag: 3)
        monthScrollView.addSubview(collectionView3!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewTaskAction() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "d"
        if(monthNum.length < 2) {
            monthNum = "0\(monthNum!)"
        }
        
        guard selectedIndexPath != nil else {
            if IJReachability.isConnectedToNetwork() == true {
                self.editAction()
            } else {
                self.showInternetToast()
            }
            return
        }
        
        var day = curMonth[selectedIndexPath.row - 7]
        if(day.length < 2 && day.length > 0) {
            day = "0\(day)"
        }
        
        if day.length > 0 {
            if IJReachability.isConnectedToNetwork() == true {
                self.editAction()
                if dateFormatter.string(from: date) == curMonth[selectedIndexPath.row - 7]{
                    NetworkingHelper.sharedInstance.currentDate = "\(yearNum!)-\(monthNum!)-\(day)"
                } else {
                    NetworkingHelper.sharedInstance.currentDate = "\(yearNum!)-\(monthNum!)-\(day)"
                }
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotification), object: self)
                Analytics.logEvent(ANALYTICS_KEY.CALENDAR_DATE_SELECTION, parameters: [:])
                Answers.logCustomEvent(withName: ANALYTICS_KEY.CALENDAR_DATE_SELECTION, customAttributes: [:])
            }
            else {
                self.showInternetToast()
            }
        }
    }
    
    func viewTravelSummaryAction() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "d"
        if(monthNum.length < 2) {
            monthNum = "0\(monthNum!)"
        }
        guard selectedIndexPath != nil else {
            self.editAction()
            return
        }
        
        guard self.selectedIndexPath != nil else {
            return
        }
        
        var day = curMonth[selectedIndexPath.row - 7]
        if(day.length < 2 && day.length > 0) {
            day = "0\(day)"
        }
        
        if day.length > 0 {
            self.editAction()
            if dateFormatter.string(from: date) == curMonth[selectedIndexPath.row - 7]{
                NetworkingHelper.sharedInstance.currentDate = "\(yearNum!)-\(monthNum!)-\(day)"
            } else {
                NetworkingHelper.sharedInstance.currentDate = "\(yearNum!)-\(monthNum!)-\(day)"
            }
            Analytics.logEvent(ANALYTICS_KEY.SHOW_PATH_SUMMARY, parameters: [:])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.SHOW_PATH_SUMMARY, customAttributes: [:])
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotification), object: self)
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.viewTravelSummary), object: self)
        }
    }
    
    //MARK: UICollectionView Delegate
    func createLayOut()->UICollectionViewFlowLayout{
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = CGFloat(0)
        layout.minimumLineSpacing = CGFloat(0)
        layout.itemSize = CGSize(width: floor((SCREEN_SIZE.width - 22)/7) , height: 39)
        return layout
    }
    
    func createCollectionV(_ collection:UICollectionView, tag:Int){
        collection.tag = tag
        collection.dataSource = self
        collection.delegate = self
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        collection.isScrollEnabled = false
        collection.backgroundColor = UIColor.white
        collection.register(UINib(nibName: NIB_NAME.calendarCollectionViewCell, bundle: Bundle.main), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag{
        case 1 :
            return prvMonth.count + 7
        case 2 :
            return curMonth.count + 7
        case 3:
            return nxtMonth.count + 7
        default:
            break
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath ) as? CalendarCollectionViewCell
        print(indexPath.item)
        if let cell = collectionViewCell {
            cell.background.backgroundColor = UIColor.white
            cell.dateLabel.textColor = COLOR.TEXT_COLOR
            cell.pendingTaskView.isHidden = true
            cell.bottomLine.isHidden = false
            if (indexPath.row - 7) >= 28 {
                if curMonth[28] == "" {
                    cell.bottomLine.isHidden = true
                } else if (indexPath.row - 7) >= 35 {
                    cell.bottomLine.isHidden = true
                }
            }
            switch collectionView.tag {
            case 1 :
                if indexPath.row < 7 {
                    cell.dateLabel.text = self.weekdayArray[indexPath.row]
                } else {
                    cell.dateLabel.text = prvMonth[indexPath.row - 7]
                }
            case 2 :
                
                if indexPath.row < 7 {
                    cell.dateLabel.text = self.weekdayArray[indexPath.row]
                } else {
                    cell.dateLabel.text = curMonth[indexPath.row - 7]
//                    if self.isDatesArrayEmpty == false {
//                        if curMonth[indexPath.row - 7] == "" {
//                            cell.pendingTaskView.isHidden = true
//                            self.blankCount = indexPath.row
//                        } else if (indexPath.row - blankCount - 1) < datesArray.count && datesArray[indexPath.row - blankCount - 1].incompleteTasks > 0 {
//                            cell.pendingTaskView.isHidden = false
//                        } else {
//                            cell.pendingTaskView.isHidden = true
//                        }
//                        
                        if (indexPath.row - 7) >= 28 {
                            if curMonth[28] == ""{
                                cell.bottomLine.isHidden = true
                            }
                        }
//                    }
                    
                    if tasksDatesArray.contains(curMonth[indexPath.row - 7]) {
                        let num = Int(tasksDatesArray[tasksDatesArray.index(of: curMonth[indexPath.row - 7])!])! - 1
                        if (datesArray[num].incompleteTasks!) > 0 {
                            cell.pendingTaskView.isHidden = false
                        } else {
                            cell.pendingTaskView.isHidden = true
                        }
                        

                    }
                    
                    if self.selectedIndexPath == nil {
                        setCurrentDayBackgroundColor(cell,dateNum: curMonth[indexPath.row - 7],label: cell.dateLabel, row:indexPath.row)
                    } else  if self.selectedIndexPath == indexPath {
                        if self.isMonthDateMatched(dateNum: curMonth[indexPath.row - 7]) == true {
                            cell.background.backgroundColor = COLOR.VEHICLE_BUTTON_COLOR
                            cell.dateLabel.textColor = UIColor.black
                            if tasksDatesArray.contains(curMonth[indexPath.row - 7]) {
                                let num = Int(tasksDatesArray[tasksDatesArray.index(of: curMonth[indexPath.row - 7])!])! - 1
                                self.updateTaskCount(complete: "\(datesArray[num].completedTasks!)", pending: "\(datesArray[num].incompleteTasks!)")
                            } else {
                                self.updateTaskCount(complete: "0", pending: "0")
                            }
                        } else {
                            self.updateTaskCount(complete: "0", pending: "0")
                        }
                    } else {
                        cell.background.backgroundColor = UIColor.white
                        cell.dateLabel.textColor = COLOR.TEXT_COLOR
                    }
                }
            case 3:
                if indexPath.row < 7 {
                    cell.dateLabel.text = self.weekdayArray[indexPath.row]
                } else {
                    cell.dateLabel.text = nxtMonth[indexPath.row - 7]
                }
            default:
                break
            }
        }
        return collectionViewCell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        guard indexPath.row > 6 else {
            return
        }
        
        guard curMonth[indexPath.row - 7] != "" else {
            return
        }
        
        if tasksDatesArray.contains(curMonth[indexPath.row - 7]) {
            let num = Int(tasksDatesArray[tasksDatesArray.index(of: curMonth[indexPath.row - 7])!])! - 1
            self.updateTaskCount(complete: "\(datesArray[num].completedTasks!)", pending: "\(datesArray[num].incompleteTasks!)")
        } else {
            self.updateTaskCount(complete: "0", pending: "0")
        }
        
        if selectedIndexPath != nil {
            if let cell = collectionView.cellForItem(at: selectedIndexPath) as? CalendarCollectionViewCell {
                cell.background.backgroundColor = UIColor.white
                cell.dateLabel.textColor = COLOR.TEXT_COLOR
            }
            self.selectedIndexPath = indexPath
            self.selectedYearNum = yearNum
            self.selectedMonthNum = monthNum
            self.selectedDateNum = curMonth[indexPath.row - 7]
            if let cell = collectionView.cellForItem(at: indexPath) as? CalendarCollectionViewCell {
                cell.background.backgroundColor = COLOR.VEHICLE_BUTTON_COLOR
                cell.dateLabel.textColor = UIColor.black
            }
        } else {
            self.selectedIndexPath = indexPath
            if let cell = collectionView.cellForItem(at: indexPath) as? CalendarCollectionViewCell {
                cell.background.backgroundColor = COLOR.VEHICLE_BUTTON_COLOR
                cell.dateLabel.textColor = UIColor.black
            }
        }
    }
    
    func setCurrentDayBackgroundColor(_ cell: CalendarCollectionViewCell,dateNum: String,label: UILabel, row:Int){
        let date = NetworkingHelper.sharedInstance.currentDate.asDateFormattedWith("yyyy-MM-dd")!
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "M"
        let currentMonthNum = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "d"
        let currentDateNum = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "YYYY"
        let currentYearNum = dateFormatter.string(from: date)
        if monthNum == currentMonthNum && currentDateNum == dateNum && yearNum == currentYearNum{
            cell.background.backgroundColor = COLOR.VEHICLE_BUTTON_COLOR
            cell.dateLabel.textColor = UIColor.black
            self.selectedIndexPath = IndexPath(row: row, section: 0)
            self.selectedDateNum = dateNum
            self.selectedMonthNum = monthNum
            self.selectedYearNum = yearNum
            if tasksDatesArray.contains(curMonth[row - 7]) {
                let num = Int(tasksDatesArray[tasksDatesArray.index(of: curMonth[row - 7])!])! - 1
                self.updateTaskCount(complete: "\(datesArray[num].completedTasks!)", pending: "\(datesArray[num].incompleteTasks!)")
                if (datesArray[num].incompleteTasks!) > 0 {
                    cell.pendingTaskView.isHidden = false
                } else {
                    cell.pendingTaskView.isHidden = true
                }
            } else {
                self.updateTaskCount(complete: "0", pending: "0")
            }
        } else {
            cell.background.backgroundColor = UIColor.white
            cell.dateLabel.textColor = COLOR.TEXT_COLOR
        }
    }
    
    func isMonthDateMatched(dateNum:String)-> Bool {
        if monthNum == selectedMonthNum && selectedDateNum == dateNum && yearNum == selectedYearNum {
            return true
        }
        return false
    }
    
    
    // MARK: - Month Data Presentation Functions
    func swipeRight(){
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.CALENDAR_MONTH_SELECTION, parameters: [:])
        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
        monthCounter += 1
        getMonthsDataArray()
        tasksDatesArray = []
    }
    
    func swipeLeft(){
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.CALENDAR_MONTH_SELECTION, parameters: [:])
        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
        monthCounter -= 1
        getMonthsDataArray()
        tasksDatesArray = []
    }
    
    func getMonthsDataArray(){
        self.previousButton.isEnabled = false
        self.nextButton.isEnabled = false
        monthScrollView.isScrollEnabled = false

        if let responseDict = monthData.currentMonth(monthCounter) as Dictionary! {
            curMonth = responseDict["dates"] as! Array
            monthLabel.text = responseDict["monthYear"] as? String
            monthNum = responseDict["month"] as! String
            yearNum = responseDict["year"] as! String
        }
        prvMonth = monthData.previousMonth(monthCounter-1)
        nxtMonth = monthData.nextMonth(monthCounter+1)
        if IJReachability.isConnectedToNetwork() == true {
            //UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.activityIndicatorMain.startAnimating()
            var param:[String:Any] = ["access_token": accessToken]
            param["month"] = monthNum
            param["year"] = yearNum
//            let param = [
//                "access_token" : accessToken,
//                "month"        : monthNum,
//                "year"         : yearNum
//            ]
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.view_task_month, params: param as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    print(response)
                    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.activityIndicatorMain.stopAnimating()
                    if isSucceeded == true {
                        self.showMonthData(response)
                    } else {
                        self.reloadCollection()
                        Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    }
                }
            })
        } else {
            //UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.showInternetToast()
        }
    }
    
    func showMonthData(_ response: [String:Any]) {
        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
        var monthTasksArray = [MonthTasksList]()
        print(response)
        if let status = response["status"] as? Int {
            switch status {
            case STATUS_CODES.SHOW_DATA:
                if let responseArray = response["data"] as? NSArray {
                    for item in responseArray {
                        monthTasksArray.append(MonthTasksList(json: item as! NSDictionary))
                    }
                }
                datesArray = monthTasksArray
                let filteredDates = Auxillary()
                tasksDatesArray = filteredDates.tasksAssignedDatesFilter(datesArray)
                break
            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                // Author : LOGIO
                let alert = UIAlertController(title: "", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                    DispatchQueue.main.async(execute: { () -> Void in
                        Auxillary.logoutFromDevice()
                        NotificationCenter.default.removeObserver(self)
                    })
                })
                alert.addAction(actionPickup)
                self.present(alert, animated: true, completion: nil)
                break
            default:
                break
            }
            reloadCollection()
        }
    }
    
    func reloadCollection() {
        if(self.collectionView2 != nil) {
            self.collectionView2!.reloadData()
        }
        self.monthScrollView .scrollRectToVisible(CGRect(x: self.monthScrollView.frame.size.width, y: 0, width: self.monthScrollView.frame.width, height: self.monthScrollView.frame.height), animated: false)
        self.previousOffsetValueOfScroll = self.monthScrollView.frame.width
        
        if(self.collectionView1 != nil) {
            self.collectionView1!.reloadData()
        }
        if(self.collectionView3 != nil) {
            self.collectionView3!.reloadData()
        }
        monthScrollView.isScrollEnabled = true
        self.previousButton.isEnabled = true
        self.nextButton.isEnabled = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.x != previousOffsetValueOfScroll) {
            if scrollView.contentOffset.x > previousOffsetValueOfScroll{
                swipeRight()
            }
            else if scrollView.contentOffset.x < previousOffsetValueOfScroll {
                swipeLeft()
            }
            previousOffsetValueOfScroll = scrollView.contentOffset.x
        }
    }
    
    func getCurrentDayTasksList(_ selectedDate: String) {
        if IJReachability.isConnectedToNetwork() == true {
            ActivityIndicator.sharedInstance.showActivityIndicator(self)
            self.sendRequestForOnlineData(selectedDate)
        } else {
            self.saveOfflineData(selectedDate)
        }
    }
    
    func sendRequestForOnlineData(_ selectedDate:String) {
        var params:[String:Any] = ["access_token": self.accessToken]
        params["date"] = selectedDate
        params["version"] = VERSION
//        let params = [
//            "access_token": self.accessToken,
//            "date":selectedDate,
//            "version":VERSION]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.viewTaskForDate, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        // Author : LOGIO
                        let alert = UIAlertController(title: "", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                            DispatchQueue.main.async(execute: { () -> Void in
                                Auxillary.logoutFromDevice()
                                NotificationCenter.default.removeObserver(self)
                            })
                        })
                        alert.addAction(actionPickup)
                        self.present(alert, animated: true, completion: nil)
                        break
                    default:
                        NetworkingHelper.sharedInstance.dayTasksListArray.removeAll(keepingCapacity: false)
                        if let items = response["data"] as? NSArray {
                            for item in items {
                                var innerTaskListArray = [TasksAssignedToDate]()
                                let taskList = item as! [[String:Any]]
                                for task in taskList {
                                    let taskDetails = TasksAssignedToDate(json: task)
                                    innerTaskListArray.append(taskDetails)
                                }
                                NetworkingHelper.sharedInstance.dayTasksListArray.append(innerTaskListArray)
                            }
                        }
                        if let polylines = response["polylines"] as? NSArray {
                            NetworkingHelper.sharedInstance.polylines = polylines
                        }
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        switch(NetworkingHelper.sharedInstance.lastController) {
                        case CONTROLLER.taskList:
                            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.taskListView), object: nil)
                            break
                        case CONTROLLER.mapView:
                            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.mapView), object: nil)
                            break
                        default:
                            break
                        }
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func saveOfflineData(_ selectedDate:String) {
        if(Auxillary.isAppSyncingEnable() == true) {
            DispatchQueue.main.async(execute: { () -> Void in
                let tasksFromDatabase = DatabaseManager.sharedInstance.fetchAssignedTaskFromDatabase(self.taskObjectContext, selectedDate: selectedDate)
                NetworkingHelper.sharedInstance.dayTasksListArray.removeAll(keepingCapacity: false)
                if((tasksFromDatabase?.count)! > 0) {
                    var relationshipIdArray = [String]()
                    for i in (0..<tasksFromDatabase!.count) {
                        var innerArray = [TasksAssignedToDate]()
                        let taskData = NSData(data: tasksFromDatabase?[i].value(forKey: "task_object") as! Data) as Data
                        let taskDetails = NSKeyedUnarchiver.unarchiveObject(with: taskData) as! TasksAssignedToDate
                        
                        if let index = relationshipIdArray.index(of: taskDetails.pickupDeliveryRelationship) {
                            innerArray = NetworkingHelper.sharedInstance.dayTasksListArray[index]
                            innerArray.append(taskDetails)
                            NetworkingHelper.sharedInstance.dayTasksListArray[index] = innerArray
                        } else {
                            relationshipIdArray.append(taskDetails.pickupDeliveryRelationship)
                            innerArray.append(taskDetails)
                            NetworkingHelper.sharedInstance.dayTasksListArray.append(innerArray)
                        }
                    }
                    
                    let polylines = DatabaseManager.sharedInstance.fetchPolylineFromDatabase(self.taskObjectContext, selectedDate: selectedDate)
                    if(polylines != nil) {
                        NetworkingHelper.sharedInstance.polylines = polylines?.jsonObject
                    }
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    switch(NetworkingHelper.sharedInstance.lastController) {
                    case CONTROLLER.taskList:
                        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.taskListView), object: nil)
                        break
                    case CONTROLLER.mapView:
                        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.mapView), object: nil)
                        break
                    default:
                        break
                    }
                } else {
                    //Auxillary.showAlert(STATUS_CODES.NO_INTERNET_CONNECTION)
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                }
            })
        } else {
            self.showInternetToast()
        }
    }
}

extension CalendarViewController:NoInternetConnectionDelegate {
    //MARK: NoInternetConnectionView
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: NIB_NAME.noInternetConnectionView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:HEIGHT.navigationHeight, width: self.view.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.view.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
}

extension CalendarViewController:TodayButtonDelegate {
    func todayTaskAction() {
        self.editAction()
        NetworkingHelper.sharedInstance.currentDate = Date().formattedWith("yyyy-MM-dd")
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OBSERVER.pushNotification), object: self)
        Analytics.logEvent(ANALYTICS_KEY.CALENDAR_DATE_SELECTION, parameters: [:])
    }
}

//MARK: UIGestureRecognizerDelegate method
extension CalendarViewController:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: self.backgroundLayerView) {
             return false
        }
        return true
    }
}

