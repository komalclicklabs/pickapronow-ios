//
//  CustomDatePicker.swift
//  Tookan
//
//  Created by Rakesh Kumar on 1/4/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

protocol CustomPickerDelegate {
    func dismissDatePicker()
    func selectedDateFromPicker(_ selectedDate:String)
}
class CustomDatePicker: UIView {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var delegate:CustomPickerDelegate!
    override func awakeFromNib() {
        datePicker.backgroundColor = UIColor.white
        datePicker.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
        toolbar.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
       // datePicker.datePickerMode = UIDatePickerMode.DateAndTime
       // datePicker.datePickerMode = UIDatePickerMode.Date
        cancelButton.title = "Cancel".localized
        doneButton.title = "Done".localized
    }
    
    func setDatePicker() {
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.datePicker.transform = CGAffineTransform.identity
            self.toolbar.transform = CGAffineTransform.identity
            }, completion: nil)
    }
    
    
    @IBAction func cancelAction(_ sender: AnyObject) {
        self.doneButton.isEnabled = false
        self.cancelButton.isEnabled = false
        self.delegate.dismissDatePicker()
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.datePicker.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
            self.toolbar.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
            }, completion: { finished in
                self.removeFromSuperview()
        })
    }

    @IBAction func doneAction(_ sender: AnyObject) {
        self.cancelButton.isEnabled = false
        self.doneButton.isEnabled = false
        let selectedDate = datePicker.date
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        if(self.datePicker.datePickerMode == UIDatePickerMode.date) {
            dateFormatter.dateFormat = "yyyy-MM-dd"//"MM/dd/yyyy"
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"//"MM/dd/yyyy HH:mm"
        }

        self.delegate.selectedDateFromPicker(dateFormatter.string(from: selectedDate))
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.datePicker.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
            self.toolbar.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
            }, completion: { finished in
                self.removeFromSuperview()
        })
    }
}
