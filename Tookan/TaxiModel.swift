//
//  TaxiModel.swift
//  Tookan
//
//  Created by cl-macmini-45 on 15/05/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

struct TAXI_LABEL {
    static let baseFare = "baseFare"
    static let distanceFare = "distanceFare"
    static let thresholdDistance = "thresholdDistance"
    static let timeFare = "timeFare"
    static let thresholdTime = "thresholdTime"
    static let surgeFactor = "surgeFactor"
    static let estimatedFare = "estimatedFare"
    static let estimatedDistance = "estimatedDistance"
    static let destinationAddress = "destinationAddress"
    static let destinationLatitude = "destinationLatitude"
    static let destinationLongitude = "destinationLongitude"
    static let distanceUnit = "distanceUnit"
    static let currencySymbol = "currencySymbol"
    static let minPickupDistance = "minPickupDistance"
}

protocol TaxiModelDelegate {
    
}


class TaxiModel: NSObject {

    var sections = [DETAIL_SECTION]()
    var detailRows = [DETAIL_ROWS]()
    
    var distanceUnit:String = "km"
    var baseFare:Double = 0.0
    var thresholdDistance:Double = 0.0
    var distanceFare:Double = 0.0
    var thresholdTime:Double = 0.0
    var timeFare:Double = 0.0
    var surgeFactor:Double = 0.0
    var currencySymbol:String? = "$"
    var destinationAddress:String? = ""
    var destinationLatitude:Double? = 0.0
    var destinationLongitude:Double? = 0.0
    var minimumPickupDistance:Double? = 500.0

    
    func setSectionsAndRows() {
        sections = [.jobDetails]
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
        case JOB_STATUS.accepted, JOB_STATUS.assigned:
            detailRows = [.job, .user, .location]
        default:
            detailRows = [.job, .user, .location, .destinationLocation]
        }
        
    }
    
    func getJobTime(jobDetails:TasksAssignedToDate) -> String {
        switch jobDetails.jobType {
        case JOB_TYPE.pickup:
            var jobTypeString = ""
            if jobDetails.vertical == VERTICAL.TAXI.rawValue {
                jobTypeString = TEXT.TAXI
            } else {
                jobTypeString = TEXT.PICKUP
            }

            return "\(jobDetails.jobPickupDateTimeInTwelveHoursFormat!) - \(jobTypeString)"
        case JOB_TYPE.delivery:
            return "\(jobDetails.jobDeliveryDateTimeInTwelveFormat!) - \(TEXT.DELIVERY)"//jobDetails.jobDeliveryDateTimeInTwelveFormat + " - " + TEXT.DELIVERY
        case JOB_TYPE.appointment, JOB_TYPE.fieldWorkforce:
            return "\(jobDetails.jobPickupDateTimeInTwelveHoursFormat!) - \(jobDetails.jobDeliveryDateTimeInTwelveFormat)"//jobDetails.jobPickupDateTimeInTwelveHoursFormat + " - " + jobDetails.jobDeliveryDateTimeInTwelveFormat
        default:
            return "-"
        }
    }
    
    //MARK: Get Customer Name
    func getCustomerName(_ selectedTaskDetails:TasksAssignedToDate) -> String {
        switch(selectedTaskDetails.jobType) {
        case JOB_TYPE.pickup:
            return selectedTaskDetails.jobPickupName
        default:
            return selectedTaskDetails.customerUsername
        }
    }
    
    func getDestinationAddress() -> CLLocationCoordinate2D {
        if(Singleton.sharedInstance.selectedTaskDetails != nil) {
            let latitudeString = Singleton.sharedInstance.selectedTaskDetails.jobLatitude as NSString
            let longitudeString = Singleton.sharedInstance.selectedTaskDetails.jobLongitude as NSString
            switch Singleton.sharedInstance.selectedTaskDetails.jobType {
            case JOB_TYPE.pickup:
                let latitudeString = Singleton.sharedInstance.selectedTaskDetails.jobPickupLatitude as NSString
                let longitudeString = Singleton.sharedInstance.selectedTaskDetails.jobPickupLongitude  as NSString
                return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            case JOB_TYPE.delivery:
                return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            case JOB_TYPE.fieldWorkforce:
                if Singleton.sharedInstance.selectedTaskDetails.jobLatitude != nil{
                    return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
                }else if Singleton.sharedInstance.selectedTaskDetails.jobPickupLatitude != nil{
                    return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
                }
            case JOB_TYPE.appointment:
                return CLLocationCoordinate2D(latitude: latitudeString.doubleValue, longitude: longitudeString.doubleValue)
            default:
                break
            }
        }
        return CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
    //MARK: Get Address
    func getAddress(_ selectedTaskDetails:TasksAssignedToDate) -> String {
        switch(selectedTaskDetails.jobType) {
        case JOB_TYPE.pickup:
            return selectedTaskDetails.jobPickupAddress
        default:
            return selectedTaskDetails.jobAddress
        }
    }
    
    func setTaxiCustomField() {
        for i in 0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray!.count {
            switch Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].label {
            case TAXI_LABEL.distanceUnit:
                self.distanceUnit = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data
                break
            case TAXI_LABEL.baseFare:
                if let value = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                    self.baseFare = value
                }
                break
            case TAXI_LABEL.thresholdDistance:
                if let value = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                    self.thresholdDistance = value
                }
                break
            case TAXI_LABEL.thresholdTime:
                if let value = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                    self.thresholdTime = value
                }
                break
            case TAXI_LABEL.timeFare:
                if let value = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                    self.timeFare = value
                }
                break
            case TAXI_LABEL.distanceFare:
                if let value = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                    self.distanceFare = value
                }
                break
            case TAXI_LABEL.surgeFactor:
                if let value = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                    self.surgeFactor = value
                }
                break
            case TAXI_LABEL.currencySymbol:
                self.currencySymbol = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data
                break
            case TAXI_LABEL.destinationAddress:
                self.destinationAddress = Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data
                break
            case TAXI_LABEL.destinationLatitude:
                if let value = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                    self.destinationLatitude = value
                }
                break
            case TAXI_LABEL.destinationLongitude:
                if let value = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                    self.destinationLongitude = value
                }
                break
            case TAXI_LABEL.minPickupDistance:
                if Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data.length > 0 {
                    if let distance = Double(Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data) {
                        self.minimumPickupDistance = distance
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    func updateValuesForTaxiFromCustomField(location:CLLocation, address:String) {
        for i in 0..<Singleton.sharedInstance.selectedTaskDetails.customFieldArray!.count {
            switch Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].label {
            case TAXI_LABEL.destinationAddress:
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data = address
                break
            case TAXI_LABEL.destinationLatitude:
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data = "\(location.coordinate.latitude)"
                break
            case TAXI_LABEL.destinationLongitude:
                Singleton.sharedInstance.selectedTaskDetails.customFieldArray![i].data = "\(location.coordinate.longitude)"
                break
            default:
                break
            }
        }
    }
    
    func getCoordinate() -> CLLocationCoordinate2D {
        var latitudeString:Double?
        var longitudeString:Double?
        switch Singleton.sharedInstance.selectedTaskDetails.jobType {
        case JOB_TYPE.pickup:
            if let lat = Singleton.sharedInstance.selectedTaskDetails.jobPickupLatitude {
                latitudeString = Double(lat)
            }
            
            if let long = Singleton.sharedInstance.selectedTaskDetails.jobPickupLongitude {
                longitudeString = Double(long)
            }
            let coordinate = CLLocationCoordinate2D(latitude: latitudeString!, longitude: longitudeString!)
            return coordinate
        default:
            if let lat = Singleton.sharedInstance.selectedTaskDetails.jobLatitude {
                latitudeString = Double(lat)
            }
            
            if let long = Singleton.sharedInstance.selectedTaskDetails.jobLongitude {
                longitudeString = Double(long)
            }
            let coordinate = CLLocationCoordinate2D(latitude: latitudeString!, longitude: longitudeString!)
            return coordinate
        }
    }
    
    func getDestinationCoordinates() -> CLLocationCoordinate2D {
        var destinationCoordinate:CLLocationCoordinate2D!
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return CLLocationCoordinate2D(latitude: 0, longitude: 0)
        }
        switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
        case JOB_STATUS.started:
            destinationCoordinate = CLLocationCoordinate2D(latitude: (self.destinationLatitude)!, longitude: (self.destinationLongitude)!)
//        case JOB_STATUS.arrived:
//            destinationCoordinate = CLLocationCoordinate2D(latitude: (self.destinationLatitude)!, longitude: (self.destinationLongitude)!)
//            break
        default:
            destinationCoordinate = self.getDestinationAddress()
            break
        }
        return destinationCoordinate
    }
    
    func convertIntoSeconds(currentTime:String, initialCalculation:Bool) -> String {
        guard Singleton.sharedInstance.selectedTaskDetails != nil else {
            return "00:00:00"
        }
        let timeArray = currentTime.components(separatedBy: ":")
        let hours = Int(timeArray[0])!
        let minutes = Int(timeArray[1])!
        let seconds = Int(timeArray[2])!
        
        var totalSeconds = (hours * 60 * 60) + (minutes * 60) + seconds + 1
        if initialCalculation == true {
            totalSeconds = Int(Singleton.sharedInstance.selectedTaskDetails.travelledTime!)
        }
        
        Singleton.sharedInstance.selectedTaskDetails.travelledTime = Double(totalSeconds)
        
        let newHours = totalSeconds / 3600
        let newMinutes = (totalSeconds % 3600) / 60
        let newSeconds = (totalSeconds % 3600) % 60
        
        let updatedTimeString = "\(self.checkTimeForSingleDigit(time: newHours)):\(self.checkTimeForSingleDigit(time: newMinutes)):\(self.checkTimeForSingleDigit(time: newSeconds))"
        return updatedTimeString
    }
    
    func checkTimeForSingleDigit(time:Int) -> String {
        var timeString:String!
        if time < 10 {
            timeString = "0\(time)"
        } else {
            timeString = "\(time)"
        }
        return timeString
    }
    
    func getDistance(previousLocation:CLLocation, distanceType:String, lastDistance:String, initialCalculation:Bool) -> String {
        
        let currentLocation = LocationTracker.sharedInstance().getLastLocation()
        let distance:CLLocationDistance = (currentLocation?.distance(from: previousLocation))!
        let oldDistanceInMeters = self.convertDistanceIntoMeters(distanceType: distanceType, lastDistance: lastDistance)
        var divisor = 0.0
        var unit = "Mile"
        if distanceType.lowercased().contains("km") == true {
            divisor = 1000.0
            unit = distanceType
        } else {
            divisor = 1609.0
            unit = distanceType
        }
        var totalDistanceInMeters = oldDistanceInMeters + distance
        if initialCalculation == true {
            totalDistanceInMeters = Singleton.sharedInstance.selectedTaskDetails.travelledDistance!
        }
        if Singleton.sharedInstance.selectedTaskDetails != nil {
            Singleton.sharedInstance.selectedTaskDetails.travelledDistance = totalDistanceInMeters
        }
        
        let distanceString =  String.init(format: "%.2f %@(s)", ceil(totalDistanceInMeters) / divisor, unit)
        return distanceString
    }
    
    func convertDistanceIntoMeters(distanceType:String, lastDistance:String) -> Double {
        let distanceArray = lastDistance.components(separatedBy: " ")
        var distance = Double(distanceArray[0])!
        var divisor = 0.0
        if distanceType.lowercased().contains("km") == true {
            divisor = 1000.0
        } else {
            divisor = 1609.0
        }
        distance = distance * divisor
        return distance
    }
    
    func convertTimeIntoMinutes(currentTime:String) -> Int {
        let timeArray = currentTime.components(separatedBy: ":")
        let hours = Int(timeArray[0])!
        let minutes = Int(timeArray[1])!
        
        let totalMinutes = (hours * 60) + minutes
        return totalMinutes
    }
    
    /*------------ Request to change job status --------------*/
    func requestToChangeJobStatus(_ accessToken:String, jobId:Int, jobStatus:Int, reason:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var params:[String:Any] = ["access_token":accessToken]
        params["job_id"] = "\(jobId)"
        params["job_status"] = "\(jobStatus)"
        params["reason"] = "\(reason)"
        params["lat"] = lat
        params["lng"] = lng
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(API_NAME.changeJobStatus, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async(execute: { () -> Void in
                    if(succeeded){
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA, STATUS_CODES.PICKUP_TASK, STATUS_CODES.INVALID_ACCESS_TOKEN:
                            receivedResponse(true, response)
                            break
                        case STATUS_CODES.SHOW_MESSAGE, STATUS_CODES.SHOW_ERROR_MESSAGE:
                            if let message = response["message"] as? String {
                                receivedResponse(false, ["status":STATUS_CODES.SHOW_MESSAGE, "message":message])
                            } else {
                                receivedResponse(false, ["status":STATUS_CODES.SHOW_MESSAGE, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                            }
                            break
                        default:
                            if let message = response["message"] as? String {
                                receivedResponse(false, ["status":response["status"] as! Int, "message":message])
                            } else {
                                receivedResponse(false, ["status":response["status"] as! Int, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                            }
                            break
                        }
                    } else {
                        receivedResponse(false, ["status":STATUS_CODES.SLOW_INTERNET_CONNECTION, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                })
            }
        } else {
            receivedResponse(false, ["status":STATUS_CODES.SLOW_INTERNET_CONNECTION, "message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }


}
