//
//  EditScheduleController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 21/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class EditScheduleController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, ArrowButtonDelegate, SaveCancelDelegate, CopyRepeatDelegate, UnavailableDelegate {

    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var availabilityCollectionView: UICollectionView!
    @IBOutlet var availableColorView: UIView!
    @IBOutlet var timeCollectionView: UICollectionView!
    @IBOutlet var unavailableColorView: UIView!
    @IBOutlet var busyColorView: UIView!
    @IBOutlet var availableLabel: UILabel!
    @IBOutlet var unavailableLabel: UILabel!
    @IBOutlet var busyLabel: UILabel!
    @IBOutlet var headerTitle: UILabel!
    @IBOutlet var headerView: UIView!
    @IBOutlet var availableStatusLabel: UILabel!
    @IBOutlet var navigationBar: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var switchButton: UIButton!
    @IBOutlet var heightOfNavigation: NSLayoutConstraint!
    @IBOutlet var topconstraintOfBottomView: NSLayoutConstraint!
    var selectedDate:String!
    var availabilityStatus = false
    var availableColorViewHeight:CGFloat = 17
    var maximumDays = 1
    var maximumTimeSlots = 96
    var timeCellWidth:CGFloat = 50
    var timeCellHeight:CGFloat = 52
    var availabilityCellWidth:CGFloat = 52
    var availabilityCellHeight:CGFloat = 52
    let repeatCellWidth:CGFloat = 55
    let repeatCellHeight:CGFloat = 75
    let copyRepeatPopupHeight:CGFloat = 380
    let headerHeight:CGFloat = 75
    var currentScrollingView = 0
    var weekday:Int!
    var arrowBottomView:ArrowBottomView!
    var saveCancelButton:SaveCancelButton!
    var copyRepeatView:CopyRepeatView!
    var unavailableView:UnavailableMarkView!
    var isSaveCancelButtonNeedToDisplay = false
    var updatedAvailabilityStatusIndex = [Int]()
    var previousContentOffset:CGFloat = 0.0
    let model = AvailabilityModel()
    var callback : ((String) -> Void)?
    
    struct SCROLL_VIEW {
        static let timeScrollView = 1
        static let availabilityScrollView = 2
    }
    
    struct CURRENT_SCROLLING {
        static let time = 1
        static let availability = 2
    }

    struct BOTTOM_HEIGHT {
        static let defaultHeight:CGFloat = 50.0
        static let heightWithSaveCancel:CGFloat = 133.0
        static let saveCancelHeight:CGFloat = 83.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.setNavigationBar()
        self.setHeaderView()
        self.heightOfNavigation.constant = self.heightOfNavigation.constant + Singleton.sharedInstance.getTopInsetofSafeArea()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.availabilityCellWidth = self.availabilityCollectionView.frame.width
        self.setCollectionView()
    }
    
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        self.navigationBar.backgroundColor = UIColor.black//COLOR.availabilityHeaderColor//COLOR.navigationBackgroundColor
        self.titleLabel.text = selectedDate
        self.titleLabel.textColor = UIColor.white//COLOR.TEXT_COLOR
        self.titleLabel.setLetterSpacing(value: 1.8)
        self.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.switchButton.isSelected = availabilityStatus
        self.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        self.backButton.tintColor = UIColor.white//COLOR.TEXT_COLOR
        self.availableStatusLabel.textColor = UIColor.white//COLOR.TEXT_COLOR
        self.availableStatusLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.setAvailabilityText()
    }

    func setAvailabilityText() {
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [.curveEaseInOut], animations: {
            if self.switchButton.isSelected == true {
                self.availableStatusLabel.text = TEXT.AVAILABLE
            } else {
                self.availableStatusLabel.text = TEXT.UNAVAILABLE
            }
        }, completion: nil)
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.resetPendingDataToUpdate()
        self.callback!("Release")
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: HEADER VIEW
    func setHeaderView() {
        /*------------- Color ------------*/
        headerView.backgroundColor = COLOR.availabilityHeaderColor
        availableColorView.backgroundColor = COLOR.availabilityFreeColor
        unavailableColorView.backgroundColor = COLOR.availabilityUnavailableColor
        busyColorView.backgroundColor = COLOR.availabilityBusyColor
        /*----------- Text -------------*/
        availableLabel.text = TEXT.AVAILABLE
        unavailableLabel.text = TEXT.UNAVAILABLE
        busyLabel.text = TEXT.BUSY
        headerTitle.text = TEXT.TAP_BELOW_AVAILABILITY
        /*----------- Corner radius ------------*/
        availableColorView.layer.cornerRadius = availableColorViewHeight / 2
        unavailableColorView.layer.cornerRadius = availableColorViewHeight / 2
        busyColorView.layer.cornerRadius = availableColorViewHeight / 2
    }
    
    //MARK: SET COLLECTION VIEW
    func setCollectionView() {
        timeCollectionView.register(UINib(nibName: NIB_NAME.timeCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.timeCollectionCell)
        timeCollectionView.delegate = self
        timeCollectionView.dataSource = self
        timeCollectionView.tag = 1
        
        availabilityCollectionView.register(UINib(nibName: NIB_NAME.availbilityCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.availbilityCollectionCell)
        availabilityCollectionView.delegate = self
        availabilityCollectionView.dataSource = self
        availabilityCollectionView.tag = 2

        self.setBottomView()
        self.setCopyRepeatView()
        
        
        if availabilityStatus == false {
            self.setUnavailableView(isTopLabelHidden: false, isBottomLabelHidden: true)
        }
    }
    
    //MARK: SET UNAVAILABLE VIEW
    func setUnavailableView(isTopLabelHidden:Bool, isBottomLabelHidden:Bool) {
        if unavailableView == nil {
            unavailableView = UINib(nibName: NIB_NAME.unavailableMarkView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UnavailableMarkView
            unavailableView.delegate = self
            var height:CGFloat!
            if isBottomLabelHidden == false {
                height = self.view.frame.height - self.headerView.frame.origin.y - BOTTOM_HEIGHT.saveCancelHeight
                self.isSaveCancelButtonNeedToDisplay = true
                self.setSaveCancelButton()
            } else {
                height = self.view.frame.height - self.headerView.frame.origin.y
            }
            unavailableView.frame = CGRect(x: 0, y: self.headerView.frame.origin.y, width: self.view.frame.width, height: height)
            unavailableView.alpha = 0.0
            self.view.addSubview(unavailableView)
        }
        UIView.animate(withDuration: 0.5) { 
            self.unavailableView.alpha = 1.0
            self.unavailableView.topLabel.isHidden = isTopLabelHidden
            self.unavailableView.topArrow.isHidden = isTopLabelHidden
            self.unavailableView.bottomLabel.isHidden = isBottomLabelHidden
        }
    }
    
    func tappedOnUnavailableView() {
        if Singleton.sharedInstance.availabilityData[weekday].lastStatusOfDayBlocked == 0  {
            self.switchButton.isSelected = true
            Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_AVAILABILITY_ON, parameters: [:])
            Singleton.sharedInstance.availabilityData[weekday].isDayBlocked = 0
            self.availabilityStatus = true
            self.removeUnavialableView()
            if updatedAvailabilityStatusIndex.count == 0 && self.copyRepeatView.finalSelectedDays.count == 0 {
                self.resetSaveCancel()
            }
            self.setAvailabilityText()
        }
    }

    func removeUnavialableView() {
        if unavailableView != nil {
            UIView.animate(withDuration: 0.5, animations: {
                self.unavailableView.alpha = 0.0
                }, completion: { finished in
                    self.unavailableView.removeFromSuperview()
                    self.unavailableView = nil
            })
        }
    }
    
    //MARK: SET BOTTOM VIEW
    func setBottomView() {
        if arrowBottomView == nil {
            arrowBottomView = UINib(nibName: NIB_NAME.arrowBottomView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ArrowBottomView
            arrowBottomView.delegate = self
            arrowBottomView.frame = CGRect(x: 0, y: self.view.frame.height - BOTTOM_HEIGHT.defaultHeight - Singleton.sharedInstance.getBottomInsetofSafeArea(), width: self.view.frame.width, height: BOTTOM_HEIGHT.defaultHeight)
            self.view.addSubview(arrowBottomView)
            self.topconstraintOfBottomView.constant = 0
            self.updateBottomConstraint(updatedValue: BOTTOM_HEIGHT.defaultHeight + Singleton.sharedInstance.getBottomInsetofSafeArea())
        }
    }
    
    //MARK: ArrowBottomView Delegate Methods
    func arrowAction() {
        Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_REPEAT, parameters: [:])
        self.showCopyRepeatPopup()
    }
    
    //MARK: SET SAVE CANCEL BUTTON
    func setSaveCancelButton() {
        if saveCancelButton == nil {
            saveCancelButton = UINib(nibName: NIB_NAME.saveCancelButton, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! SaveCancelButton
            saveCancelButton.frame = CGRect(x: 0, y: self.view.frame.height - Singleton.sharedInstance.getBottomInsetofSafeArea(), width: self.view.frame.width, height: BOTTOM_HEIGHT.saveCancelHeight)
            saveCancelButton.delegate = self
            self.view.addSubview(saveCancelButton)
            self.setBottomViewWithSaveCancel()
        } else {
           self.setBottomViewWithSaveCancel()
        }
    }
    
    //MARK: SET COPY/REPEAT VIEW
    func setCopyRepeatView() {
        if copyRepeatView == nil {
            copyRepeatView = UINib(nibName: NIB_NAME.copyRepeatView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CopyRepeatView
            copyRepeatView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            copyRepeatView.copyCollectionView.delegate = self
            copyRepeatView.copyCollectionView.dataSource = self
            copyRepeatView.delegate = self
            // copyRepeatView.popupView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            copyRepeatView.isHidden = true
            self.copyRepeatView.setRepeatData()
            self.view.addSubview(copyRepeatView)
        }
    }
    
    func showCopyRepeatPopup() {
        self.copyRepeatView.setRepeatData()
        self.copyRepeatView.isHidden = false
        self.view.bringSubview(toFront: self.copyRepeatView)
        self.copyRepeatView.popupView.alpha = 0.0
        let translationY = self.arrowBottomView.frame.origin.y - self.copyRepeatView.popupView.frame.origin.y
        self.copyRepeatView.popupView.transform = CGAffineTransform(translationX: 0, y: translationY)
       
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 2.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.copyRepeatView.popupView.alpha = 1.0
            self.copyRepeatView.popupView.transform = CGAffineTransform.identity
          //  self.copyRepeatView.popupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
    }
    
    //MARK: COPY/REPEAT DELEGATE METHODS
    func hideCopyRepeatPopup() {
        Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_REPEAT_CANCEL, parameters: [:])
        UIView.animate(withDuration: 0.35, animations: {
            let translationY = self.arrowBottomView.frame.origin.y - self.copyRepeatView.popupView.frame.origin.y
            self.copyRepeatView.popupView.transform = CGAffineTransform(translationX: 0, y: translationY)
            }, completion: { finished in
                self.copyRepeatView.selectedWeekdays.removeAll()
                self.copyRepeatView.isHidden = true
                self.copyRepeatView.popupView.transform = CGAffineTransform.identity
                if self.updatedAvailabilityStatusIndex.count == 0  && self.copyRepeatView.finalSelectedDays.count == 0 {
                    self.copyRepeatView.resetRepeatData()
                    self.resetSaveCancel()
                }
        })
    }
    
    func doneFromCopyRepeatPopup() {
        self.copyRepeatView.finalSelectedDays = self.copyRepeatView.selectedWeekdays
        Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_REPEAT_DONE, parameters: [:])
        //self.copyRepeatView.isHidden = true
        self.isSaveCancelButtonNeedToDisplay = true
        UIView.animate(withDuration: 0.35, animations: {
            //self.copyRepeatView.popupView.alpha = 0.0
            //self.copyRepeatView.popupView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            let translationY = self.arrowBottomView.frame.origin.y - self.copyRepeatView.popupView.frame.origin.y
            self.copyRepeatView.popupView.transform = CGAffineTransform(translationX: 0, y: translationY)
            }, completion: { finished in
                self.copyRepeatView.isHidden = true
                self.copyRepeatView.popupView.transform = CGAffineTransform.identity
                if self.copyRepeatView.selectedWeekdays.count > 0 {
                    self.setSaveCancelButton()
                    self.arrowBottomView.arrowButton.setTitle(self.copyRepeatView.descriptionLabel.text, for: UIControlState.normal)
                } else {
                    self.copyRepeatView.resetRepeatData()
                    self.arrowBottomView.resetTitle()
                    if self.updatedAvailabilityStatusIndex.count == 0  && self.copyRepeatView.finalSelectedDays.count == 0 {
                        self.resetSaveCancel()
                    }
                }
        })
     }
    
    func updateDescriptionText() {
        self.copyRepeatView.descriptionLabel.text = model.setDescriptionLabel(selectedDays: self.copyRepeatView.selectedWeekdays.sorted(), forever: self.copyRepeatView.forever.isSelected, weekday:weekday + 1)
        self.updateCopyRepeatPopupHeight(text: self.copyRepeatView.descriptionLabel.text!)
        self.copyRepeatView.copyCollectionView.reloadData()
    }
    
    func updateCopyRepeatPopupHeight(text:String) {
        if(text.length > 0) {
            let stringHeight = UIFont(name: UIFont().MontserratMedium, size: 15.0)?.sizeOfString(text, constrainedToWidth: Double(self.copyRepeatView.descriptionLabel.frame.width))
            self.copyRepeatView.popupHeightConstraint.constant = copyRepeatPopupHeight + stringHeight! + 10
        } else {
            self.copyRepeatView.popupHeightConstraint.constant = copyRepeatPopupHeight
        }
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.2) {
            self.copyRepeatView.layoutIfNeeded()
        }
    }
    
    func checkAction(sender:UIButton) {
        sender.isSelected = !sender.isSelected
        let item = (sender.tag - weekday - 1) % 4
        if let cell = self.copyRepeatView.copyCollectionView.cellForItem(at: NSIndexPath(item: item, section: (sender.tag - weekday - 1) > 3 ? 1 : 0) as IndexPath) as? CopyRepeatCell {
            if sender.isSelected == true {
                cell.checkbox.layer.borderColor = cell.selectedColor.cgColor
                cell.dateLabel.textColor = cell.selectedColor
                self.copyRepeatView.selectedWeekdays.append(sender.tag)
            } else {
                cell.checkbox.layer.borderColor = cell.normalColor.cgColor
                cell.dateLabel.textColor = cell.normalColor
                
                if self.copyRepeatView.selectedWeekdays.contains(sender.tag) {
                    if let index = self.copyRepeatView.selectedWeekdays.index(of: sender.tag) {
                        self.copyRepeatView.selectedWeekdays.remove(at: index)
                    }
                }
            }
            let text = self.model.setDescriptionLabel(selectedDays: self.copyRepeatView.selectedWeekdays.sorted(), forever: self.copyRepeatView.forever.isSelected, weekday:weekday + 1)
            self.copyRepeatView.descriptionLabel.text = text
            self.updateCopyRepeatPopupHeight(text: text)
            self.copyRepeatView.setRadioButtons()
        }
    }
    
    func setBottomViewWithSaveCancel() {
        self.topconstraintOfBottomView.constant = 100
        self.updateBottomConstraint(updatedValue: BOTTOM_HEIGHT.heightWithSaveCancel + Singleton.sharedInstance.getBottomInsetofSafeArea())
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 4.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.arrowBottomView.transform = CGAffineTransform(translationX: 0, y: -BOTTOM_HEIGHT.saveCancelHeight)
            self.saveCancelButton.transform = CGAffineTransform(translationX: 0, y:  -BOTTOM_HEIGHT.saveCancelHeight)
            }, completion: { finished in
        })
    }
    
    func resetBottomViewWithSaveCancel() {
        self.topconstraintOfBottomView.constant = 0
        self.updateBottomConstraint(updatedValue: BOTTOM_HEIGHT.defaultHeight + Singleton.sharedInstance.getBottomInsetofSafeArea())
        UIView.animate(withDuration: 0.2, animations: {
            self.arrowBottomView.transform = CGAffineTransform.identity
            self.saveCancelButton.transform = CGAffineTransform.identity
            }, completion: { finished in
                //self.setBottomViewWithCopyRepeat()
        })
    }
    
    func resetSaveCancel() {
        self.topconstraintOfBottomView.constant = 0
        self.updateBottomConstraint(updatedValue: BOTTOM_HEIGHT.defaultHeight + Singleton.sharedInstance.getBottomInsetofSafeArea())
        UIView.animate(withDuration: 0.2, animations: {
            self.arrowBottomView.transform = CGAffineTransform.identity
            if self.saveCancelButton != nil {
                self.saveCancelButton.transform = CGAffineTransform.identity
            }
        }, completion: { finished in
            if self.saveCancelButton != nil {
                self.saveCancelButton.removeFromSuperview()
                self.saveCancelButton = nil
            }
        })
    }
    
    //MARK: UPDATE BOTTOM CONSTRAINT
    func updateBottomConstraint(updatedValue:CGFloat) {
        self.bottomConstraint.constant = updatedValue
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.4) {
            self.view.updateConstraintsIfNeeded()
        }
    }
    
    //MARK: RESET PENDING DATA
    func resetPendingDataToUpdate() {
        guard weekday < Singleton.sharedInstance.availabilityData.count else {
            return
        }
        if(Singleton.sharedInstance.availabilityData[weekday].isDayBlocked != Singleton.sharedInstance.availabilityData[weekday].lastStatusOfDayBlocked) {
            Singleton.sharedInstance.availabilityData[weekday].isDayBlocked = Singleton.sharedInstance.availabilityData[weekday].lastStatusOfDayBlocked
        }
        
        if self.updatedAvailabilityStatusIndex.count > 0 {
            self.model.resetAvailabilityStatus(updatedStatusArray: self.updatedAvailabilityStatusIndex, day: weekday)
            self.availabilityCollectionView.reloadData()
            self.updatedAvailabilityStatusIndex.removeAll()
        }
        self.arrowBottomView.resetTitle()
        self.copyRepeatView.resetRepeatData()
    }
    
    //MARK: SAVE CANCEL DELEGATE METHODS
    func saveSchedule() {
        Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_SAVE_BUTTON, parameters: [:])
        ActivityIndicator.sharedInstance.showActivityIndicator(self)
        if availabilityStatus == false {
            self.sendServerHitToUnavailableStatus()
        } else {
            self.sendServerHitToUpdateSchedule()
        }
    }
    
    func cancelSchedule() {
         Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_RESET_BUTTON, parameters: [:])
        self.isSaveCancelButtonNeedToDisplay = false
        self.resetSaveCancel()
        if availabilityStatus == false {
            Singleton.sharedInstance.availabilityData[weekday].isDayBlocked = 0
            self.switchButton.isSelected = !self.switchButton.isSelected
            self.availabilityStatus = true
            self.removeUnavialableView()
            self.setAvailabilityText()
        }
        self.resetPendingDataToUpdate()
    }
    
    //MARK: SERVER HIT
    func sendServerHitToUpdateSchedule() {
        guard self.weekday < Singleton.sharedInstance.availabilityData.count else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            return
        }
        let locatDateTime = Singleton.sharedInstance.availabilityData[self.weekday].dateTime
        let calendarJson = (model.getTimeSlotJson(indexArray: updatedAvailabilityStatusIndex, day: self.weekday)).jsonString
        let copyRepeatJson = (model.getCopyRepeatJson(indexArray: self.copyRepeatView.finalSelectedDays)).jsonString
        var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["local_date_time"] = locatDateTime
        params["calendar_information_json"] = calendarJson
        params["is_day_blocked"] = 2
        params["copy_to_date_json"] = copyRepeatJson
        params["every_week_status"] = self.copyRepeatView.forever.isSelected == true ? 1 : 0

//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//            "local_date_time":locatDateTime,
//            "calendar_information_json":calendarJson,
//            "is_day_blocked":2,
//            "copy_to_date_json":copyRepeatJson,
//            "every_week_status":self.copyRepeatView.forever.isSelected == true ? 1 : 0
//            ] as [String : Any]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.setDayAvailability, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    case STATUS_CODES.SHOW_DATA:
                        self.model.updateLastAvailabilityStatus(updatedStatusArray: self.updatedAvailabilityStatusIndex, day: self.weekday)
                        self.availabilityCollectionView.reloadData()
                        self.updatedAvailabilityStatusIndex.removeAll()
                        self.copyRepeatView.selectedWeekdays.removeAll()
                        self.copyRepeatView.finalSelectedDays.removeAll()
                        self.arrowBottomView.resetTitle()
                        self.resetSaveCancel()
                        self.callback!("Release")
                        _ = self.navigationController?.popViewController(animated: true)
                        break
                    case STATUS_CODES.UNAUTHORIZED_FOR_AVAILABILITY:
                        if let data = response["data"] as? [String:Any] {
                            if let message = response["message"] as? String {
                                self.showPopupForInvalidAccessForAvailability(message: message, data: data)
                            }
                        }
                        break
                    default:
                        break
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    func sendServerHitToUnavailableStatus() {
        let localDateTime = Singleton.sharedInstance.availabilityData[self.weekday].dateTime
        let calendarJson = (model.getTimeSlotJson(indexArray: updatedAvailabilityStatusIndex, day: self.weekday)).jsonString
        let copyRepeatJson = (model.getCopyRepeatJson(indexArray: [])).jsonString
        var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["local_date_time"] = localDateTime
        params["calendar_information_json"] = calendarJson
        params["is_day_blocked"] = 1
        params["copy_to_date_json"] = copyRepeatJson
        params["every_week_status"] = 0
//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//            "local_date_time":localDateTime,
//            "calendar_information_json":calendarJson,
//            "is_day_blocked":1,
//            "copy_to_date_json":copyRepeatJson,
//            "every_week_status":0
//            ] as [String : Any]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.setDayAvailability, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    case STATUS_CODES.SHOW_DATA:
                        Singleton.sharedInstance.availabilityData[self.weekday].isDayBlocked = 1
                        Singleton.sharedInstance.availabilityData[self.weekday].lastStatusOfDayBlocked = 1
                        self.model.resetAvailabilityStatusToUnavailable(day: self.weekday)
                        self.removeUnavialableView()
                        self.resetPendingDataToUpdate()
                        self.callback!("Release")
                        _ = self.navigationController?.popViewController(animated: true)
                        break
                    case STATUS_CODES.UNAUTHORIZED_FOR_AVAILABILITY:
                        if let data = response["data"] as? [String:Any] {
                            if let message = response["message"] as? String {
                                self.showPopupForInvalidAccessForAvailability(message: message, data: data)
                            }
                        }
                        break
                        
                    default:
                        break
                    }

                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    self.availabilityStatus = true
                    self.switchButton.isSelected = !self.switchButton.isSelected
                    self.setAvailabilityText()
                    self.removeUnavialableView()
                    self.resetPendingDataToUpdate()
                    self.resetSaveCancel()
                }
            }
        }
    }
    
    func sendServerHitToAvailableStatus() {
        let localDateTime = Singleton.sharedInstance.availabilityData[self.weekday].dateTime
        let calendarJson = (model.getTimeSlotJson(indexArray: updatedAvailabilityStatusIndex, day: self.weekday)).jsonString
        let copyRepeatJson = (model.getCopyRepeatJson(indexArray: [])).jsonString
        var params:[String : Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["local_date_time"] = localDateTime
        params["calendar_information_json"] = calendarJson
        params["is_day_blocked"] = 0
        params["copy_to_date_json"] = copyRepeatJson
        params["every_week_status"] = 0
//        let params = [
//            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
//            "local_date_time":localDateTime,
//            "calendar_information_json":calendarJson,
//            "is_day_blocked":0,
//            "copy_to_date_json":copyRepeatJson,
//            "every_week_status":0
//            ] as [String : Any]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.setDayAvailability, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        self.showInvalidAccessTokenPopup(response["message"] as! String)
                        break
                    case STATUS_CODES.SHOW_DATA:
                        Singleton.sharedInstance.availabilityData[self.weekday].isDayBlocked = 0
                        Singleton.sharedInstance.availabilityData[self.weekday].lastStatusOfDayBlocked = 0
                        self.model.resetAvailabilityStatusToAvailable(day: self.weekday)
                        self.availabilityCollectionView.reloadData()
                        self.copyRepeatView.selectedWeekdays.removeAll()
                        self.copyRepeatView.finalSelectedDays.removeAll()
                        self.arrowBottomView.resetTitle()
                        break
                    case STATUS_CODES.UNAUTHORIZED_FOR_AVAILABILITY:
                        if let data = response["data"] as? [String:Any] {
                            if let message = response["message"] as? String {
                                self.showPopupForInvalidAccessForAvailability(message: message, data: data)
                            }
                        }
                        break
                        
                    default:
                        break
                    }

                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                }
            }
        }
    }
    
    @IBAction func switchAction(_ sender: AnyObject) {
        if IJReachability.isConnectedToNetwork() == true {
            guard weekday < Singleton.sharedInstance.availabilityData.count else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
                return
            }
            self.switchButton.isSelected = !self.switchButton.isSelected
            if switchButton.isSelected == true {
                Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_AVAILABILITY_ON, parameters: [:])
                Singleton.sharedInstance.availabilityData[weekday].isDayBlocked = 0
                self.availabilityStatus = true
                self.removeUnavialableView()
                if updatedAvailabilityStatusIndex.count == 0 && self.copyRepeatView.finalSelectedDays.count == 0 {
                    self.resetSaveCancel()
                }
                if Singleton.sharedInstance.availabilityData[weekday].lastStatusOfDayBlocked != 0 {
                    self.sendServerHitToAvailableStatus()
                }
            } else {
                Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_UNAVAILABILITY_OFF, parameters: [:])
                if model.isGivenDayContainsAnyBusyStatus(day: weekday) == false {
                    self.availabilityStatus = false
                    Singleton.sharedInstance.availabilityData[weekday].isDayBlocked = 1
                    self.setUnavailableView(isTopLabelHidden: true, isBottomLabelHidden: false)
                } else {
                    self.showAlertMessageForBusyTask()
                }
            }
            self.setAvailabilityText()
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        }
    }
    
    func showAlertMessageForBusyTask() {
        let alert = UIAlertController(title: "", message: ERROR_MESSAGE.resetToAvailable, preferredStyle: UIAlertControllerStyle.alert)
        let confirmAction = UIAlertAction(title: TEXT.CONFIRM, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.sendServerHitToUnavailableStatus()
            })
        })
        
        let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: UIAlertActionStyle.cancel) { (UIAlertAction) in
            DispatchQueue.main.async(execute: { () -> Void in
                self.availabilityStatus = true
                self.switchButton.isSelected = !self.switchButton.isSelected
                self.setAvailabilityText()
            })
        }
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: UICollection View Delegate...
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        switch collectionView {
        case self.copyRepeatView.copyCollectionView:
            if section == 0 {
                return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 10)
            } else {
                return UIEdgeInsets(top:0, left: 35, bottom: 0, right: 35)
            }
        default:
            return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.copyRepeatView.copyCollectionView:
            return 2
        default:
            return 1
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case timeCollectionView:
            return maximumTimeSlots
        case self.copyRepeatView.copyCollectionView:
            if section == 0 {
                return 4
            } else {
                return 3
            }
        default:
            return maximumDays * maximumTimeSlots
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case timeCollectionView:
            return CGSize(width: timeCellWidth, height: timeCellHeight)
        case copyRepeatView.copyCollectionView:
            return CGSize(width: repeatCellWidth, height: repeatCellHeight)
        default:
            return CGSize(width: availabilityCellWidth, height: availabilityCellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case timeCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.timeCollectionCell, for: indexPath) as! TimeCollectionCell
            cell.timeLabel.text = model.getTimeSlot(indexPath: indexPath.item, totalTimeSlots: maximumTimeSlots)
            return cell
        case self.copyRepeatView.copyCollectionView:
            let tag = (4*indexPath.section + indexPath.item) + weekday + 1
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.copyRepeatCell, for: indexPath) as! CopyRepeatCell
            cell.checkbox.setTitle(model.getDateSlotForRepeatPopup(indexPath: tag, format: "EEE").capitalized, for: UIControlState.normal)
            cell.checkbox.tag = tag
            cell.checkbox.addTarget(self, action: #selector(self.checkAction(sender:)), for: UIControlEvents.touchUpInside)
            cell.dateLabel.text = model.getDateSlotForRepeatPopup(indexPath: tag, format: "MMM dd")
            if self.copyRepeatView.forever.isSelected == true {
                cell.dateLabel.isHidden = true
            } else {
                cell.dateLabel.isHidden = false
            }
            if self.copyRepeatView.selectedWeekdays.contains(tag) == true {
                cell.checkbox.isSelected = true
                cell.checkbox.layer.borderColor = cell.selectedColor.cgColor
                cell.dateLabel.textColor = cell.selectedColor
            } else {
                cell.checkbox.isSelected = false
                cell.checkbox.layer.borderColor = cell.normalColor.cgColor
                cell.dateLabel.textColor = cell.normalColor
            }
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.availbilityCollectionCell, for: indexPath) as! availbilityCollectionCell
            cell.lock.isHidden = true
            cell.centerImageView.isHidden = true
            cell.topMarginView.isHidden = true
            cell.bottomMarginView.isHidden = true
            let currentNodeStatus = model.getAvailabilityStatusForParticularDay(indexPath: indexPath.item, day: weekday)//model.getAvailabilityStatus(indexPath: indexPath.item, maximumDays: self.maximumDays)
            let currentNodeColor = model.currentStatusColor(currentNodeStatus: currentNodeStatus)
            cell.statusView.backgroundColor = currentNodeColor
            
            if currentNodeStatus == AVAILABILITY.busy {
                if model.isTopMarginViewEligibleForCombineView(indexPath: indexPath.item, maximumDays: self.maximumDays, currentNodeStatus: currentNodeStatus) == true {
                    cell.topMarginView.isHidden = false
                    cell.topMarginView.backgroundColor = currentNodeColor
                } else {
                    cell.topMarginView.isHidden = true
                }
                
                if model.isBottomMarginViewEligibleForCombineView(indexPath: indexPath.item, maximumDays: self.maximumDays, currentNodeStatus: currentNodeStatus, maximumSlots: self.maximumTimeSlots) {
                    cell.bottomMarginView.isHidden = false
                    cell.bottomMarginView.backgroundColor = currentNodeColor
                } else {
                    cell.bottomMarginView.isHidden = true
                }
                if cell.topMarginView.isHidden == true {
                    cell.centerImageView.isHidden = false
                    cell.centerImageView.image = UIImage(named: "centerLock")
                }
            } else if currentNodeStatus == AVAILABILITY.available || currentNodeStatus == AVAILABILITY.highlightedAvailable {
                cell.centerImageView.isHidden = false
                cell.centerImageView.image = UIImage(named: "tick")
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case availabilityCollectionView:
            let cell = availabilityCollectionView.cellForItem(at: indexPath) as! availbilityCollectionCell
            var currentNodeStatus = model.getAvailabilityStatusForParticularDay(indexPath: indexPath.item, day: weekday)
           
            if currentNodeStatus != AVAILABILITY.busy {
                switch currentNodeStatus {
                case AVAILABILITY.available:
                    cell.centerImageView.isHidden = true
                    currentNodeStatus = AVAILABILITY.highlightedUnavailable
                    break
                case AVAILABILITY.unavailable:
                    cell.centerImageView.isHidden = false
                    cell.centerImageView.image = UIImage(named: "tick")
                    currentNodeStatus = AVAILABILITY.highlightedAvailable
                    break
                case AVAILABILITY.highlightedAvailable:
                    cell.centerImageView.isHidden = true
                    currentNodeStatus = AVAILABILITY.unavailable
                    break
                case AVAILABILITY.highlightedUnavailable:
                    cell.centerImageView.isHidden = false
                    cell.centerImageView.image = UIImage(named: "tick")
                    currentNodeStatus = AVAILABILITY.available
                    break
                default:
                    break
                }
                
                let currentNodeColor = model.currentStatusColor(currentNodeStatus: currentNodeStatus)
                cell.statusView.backgroundColor = currentNodeColor
                model.setAvailabilityStatusForParticularDay(indexPath: indexPath.item, day: weekday, updatedStatus: currentNodeStatus)
                
                if updatedAvailabilityStatusIndex.contains(indexPath.item) == false {
                    updatedAvailabilityStatusIndex.append(indexPath.item)
                } else {
                    if let index = updatedAvailabilityStatusIndex.index(of: indexPath.item) {
                        updatedAvailabilityStatusIndex.remove(at: index)
                    }
                }
                
                if self.saveCancelButton == nil {
                    if updatedAvailabilityStatusIndex.count > 0 {
                        self.isSaveCancelButtonNeedToDisplay = true
                        self.setSaveCancelButton()
                    }
                } else if updatedAvailabilityStatusIndex.count == 0 && self.copyRepeatView.finalSelectedDays.count == 0 {
                    self.resetSaveCancel()
                }
            }
            break
        default:
            break
            
        }
    }
    
    //MARK: SCROLL VIEW DELEGATE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        if self.topConstraint.constant <= 0 && self.topConstraint.constant >= -headerHeight {
            if offsetY >= 0 {
                if(-offsetY >= -headerHeight) {
                    if(previousContentOffset <= offsetY) {
                        topConstraint.constant = -offsetY
                    } else {
                        if(topConstraint.constant != 0) {
                            topConstraint.constant = -offsetY
                        }
                    }
                } else {
                    if(previousContentOffset <= offsetY) {
                        if(offsetY != -headerHeight) {
                            self.topConstraint.constant = -headerHeight
                            self.view.setNeedsUpdateConstraints()
                            UIView.animate(withDuration: 0.2, animations: {
                                self.view.updateConstraintsIfNeeded()
                            })
                        }
                    } else {
                        if(offsetY != 0) {
                            self.topConstraint.constant = 0
                            self.view.setNeedsUpdateConstraints()
                            UIView.animate(withDuration: 0.2, animations: {
                                self.view.updateConstraintsIfNeeded()
                            })
                        }
                    }
                }
                previousContentOffset = scrollView.contentOffset.y
            } else {
                if(topConstraint.constant != 0) {
                    self.topConstraint.constant = 0
                    self.view.setNeedsUpdateConstraints()
                    UIView.animate(withDuration: 0.2, animations: {
                        self.view.updateConstraintsIfNeeded()
                    })
                }
            }

        }
        
        switch currentScrollingView {
        case CURRENT_SCROLLING.time:
            self.availabilityCollectionView.setContentOffset(CGPoint(x: self.availabilityCollectionView.contentOffset.x, y: scrollView.contentOffset.y) , animated: false)
            break
        case CURRENT_SCROLLING.availability:
            self.timeCollectionView.setContentOffset(CGPoint(x: self.timeCollectionView.contentOffset.x, y: scrollView.contentOffset.y) , animated: false)
            break
        default:
            break
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.setCurrentScrollView(scrollView: scrollView)
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        self.setCurrentScrollView(scrollView: scrollView)
    }
    
    func setCurrentScrollView(scrollView:UIScrollView) {
        switch scrollView.tag {
        case SCROLL_VIEW.timeScrollView:
            self.currentScrollingView = CURRENT_SCROLLING.time
            break
        case SCROLL_VIEW.availabilityScrollView:
            self.currentScrollingView = CURRENT_SCROLLING.availability
            break
        default:
            break
        }
    }
    
    func showInvalidAccessTokenPopup(_ message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                 NotificationCenter.default.removeObserver(self)
                Singleton.sharedInstance.availabilityData = [AvailabilityDay]()
                Auxillary.logoutFromDevice()
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPopupForInvalidAccessForAvailability(message:String, data:[String:Any]) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let status = data["fleet_calendar__view_status"] as? Int {
                    if status == 0 {
                        Singleton.sharedInstance.availabilityViewStatus = 0
                        let controllers = self.navigationController?.viewControllers
                        _ = self.navigationController?.popToViewController((controllers?[(controllers?.count)! - 3])!, animated: true)
                        return
                    }
                }
                
                if let status = data["fleet_calendar__edit_status"] as? Int {
                    if status == 0 {
                        Singleton.sharedInstance.availabilityEditStatus = 0
                        self.callback!("Release")
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
            })
        })
        alert.addAction(actionPickup)
        self.present(alert, animated: true, completion: nil)
    }
 }
