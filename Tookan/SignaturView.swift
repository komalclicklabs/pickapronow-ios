//
//  SignaturView.swift
//  Tookan
//
//  Created by clicklabs106 on 9/8/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

protocol SignatureViewControllerDelegate: NSObjectProtocol {
    func signatureViewController()
}

protocol SignaturePlaceDelegate {
    func drawingHasStarted()
}

class SignaturView: UIView {

    var path = UIBezierPath()
    var pts: [CGPoint] = [CGPoint(),CGPoint(),CGPoint(),CGPoint(),CGPoint()]
    var ctr: Int = 0
    weak var delegate: SignatureViewControllerDelegate?
    var placeDelegate:SignaturePlaceDelegate!
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Capturing Touches
        // DrawTouch
        let pan: UIPanGestureRecognizer = UIPanGestureRecognizer(target:self, action:#selector(SignaturView.pan(_:)))
        pan.maximumNumberOfTouches = 1
        pan.minimumNumberOfTouches = 1
        pan.cancelsTouchesInView = true
        self.addGestureRecognizer(pan)
        
        // Dotting-The-I's-Touches
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(SignaturView.tap(_:)))
        pan.cancelsTouchesInView = true
        self.addGestureRecognizer(tap)
        
        
        self.backgroundColor = COLOR.SIGNATURE_COLOR
        self.layer.cornerRadius = 15.0
        self.layer.masksToBounds = true
        self.path.lineWidth = 4.0
    }
    
    // MARK: - Draw
    override func draw(_ rect: CGRect) {
        UIColor.white.setStroke()
        UIColor.white.setFill()
        self.path.stroke()
    }
    
    // MARK: - Gesture Recognizers
    
    // MARK: Pan - Draws the Signature Curves
    func pan(_ touch: UIPanGestureRecognizer){
        let touchPoint: CGPoint = touch.location(in: self)
        
        if (touch.state == UIGestureRecognizerState.began) {
            self.ctr = 0
            self.pts[0] = touchPoint
            delegate?.signatureViewController()
            placeDelegate?.drawingHasStarted()
        } else if (touch.state == UIGestureRecognizerState.changed) {
            
            self.ctr += 1
            self.pts[self.ctr] = touchPoint
            if (self.ctr == 4) {
                self.pts[3] = CGPoint(x: (self.pts[2].x + self.pts[4].x)/2.0, y: (self.pts[2].y + self.pts[4].y)/2.0)
                self.path.move(to: self.pts[0])
                self.path.addCurve(to: self.pts[3], controlPoint1:self.pts[1], controlPoint2:self.pts[2])
                
                self.setNeedsDisplay()
                self.pts[0] = self.pts[3]
                self.pts[1] = self.pts[4]
                self.ctr = 1
            }
            
            self.setNeedsDisplay()
            
        } else if (touch.state == UIGestureRecognizerState.ended || touch.state == UIGestureRecognizerState.cancelled) {
            
            self.path.addLine(to: touchPoint)
            self.ctr = 0
            self.setNeedsDisplay()
            
        }
        
    }
    
    // MARK: Tap - Draws the Dots
    func tap(_ touch: UITapGestureRecognizer){
        let touchPoint: CGPoint = touch.location(in: self)
        self.path.move(to: CGPoint(x: touchPoint.x-1.0,y: touchPoint.y))
        self.path.addLine(to: CGPoint(x: touchPoint.x+1.0,y: touchPoint.y))
        self.setNeedsDisplay()
    }
    
    // MARK: - Helpers
    
    // MARK: Clear the Signature View
    func clearSignature() {
        self.path.removeAllPoints()
        self.setNeedsDisplay()
    }
    
    // MARK: Save the Signature as an UIImage
    func getSignature() -> UIImage! {
        if(path.isEmpty == false) {
            UIGraphicsBeginImageContext(CGSize(width: self.bounds.size.width, height: self.bounds.size.height))
            self.layer.render(in: UIGraphicsGetCurrentContext()!)
            let signature: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return signature
        } else {
            return nil
        }
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
