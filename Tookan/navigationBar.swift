//
//  navigationBar.swift
//  Tookan
//
//  Created by Rakesh Kumar on 12/30/15.
//  Copyright © 2015 Click Labs. All rights reserved.
//

import UIKit
protocol NavigationDelegate {
    func backAction()
}
class navigationBar: UIView {

    @IBOutlet var bottomLine: UIView!
    @IBOutlet var hamburgerButton: HamburgerButton!
    @IBOutlet var rightButtonWithImage: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var editButtonTrailingConstraint: NSLayoutConstraint!
    var delegate:NavigationDelegate!
    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
    var blurEffectView:UIVisualEffectView!
    
    override func awakeFromNib() {
        self.backgroundColor = COLOR.navigationBackgroundColor
        self.titleLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
        self.titleLabel.textColor = UIColor.white
        
        self.editButton.titleLabel?.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
        self.editButton.setTitleColor(COLOR.TEXT_COLOR, for: .normal)
        
        self.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        self.backButton.tintColor = COLOR.TEXT_COLOR
        
        /*================= Blur effect ===================*/
        
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        delegate.backAction()
    }
    
    @IBAction func hamburgerAction(_ sender: Any) {
        delegate.backAction()
    }
    
    func changeHamburgerButton() {
        self.hamburgerButton.showsMenu = !self.hamburgerButton.showsMenu
    }
    
    
    func setAlphaForAllItems(_ alphaValue:CGFloat) {
        self.editButton.alpha = alphaValue
        self.backButton.alpha = alphaValue
        self.titleLabel.alpha = alphaValue
    }

}
