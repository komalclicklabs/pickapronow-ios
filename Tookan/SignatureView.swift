//
//  SignatureView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 27/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Crashlytics


protocol SignatureDelegate {
    func hideSignatureView()
    func addSignatureAction(imagePath:String, image:UIImage)
}
class SignatureView: UIView {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var signaturePlace: SignaturView!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var signatureImageView: UIImageView!
    var delegate:SignatureDelegate!
    
    override func awakeFromNib() {
        /*===================   Title Label   ==================================*/
        self.titleLabel.font = UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.regular)
        self.titleLabel.textColor = UIColor.white
        self.titleLabel.text = TEXT.PLEASE_SIGN
        self.titleLabel.setLetterSpacing(value: 2.0)
        
        /*===================   Signature Place   ==================================*/
        self.signaturePlace.layer.cornerRadius = 15.0
        self.signaturePlace.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5).cgColor
        self.signaturePlace.layer.borderWidth = 1.0
        self.signaturePlace.layer.masksToBounds = true
        self.signaturePlace.backgroundColor = COLOR.SIGNATURE_COLOR
        self.signaturePlace.placeDelegate = self
        
        /*===================   Signature Image View   ==================================*/
        self.signatureImageView.layer.cornerRadius = 15.0
        self.signatureImageView.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5).cgColor
        self.signatureImageView.layer.borderWidth = 1.0
        self.signatureImageView.layer.masksToBounds = true
        self.signatureImageView.isHidden = true
        
        /*===================   Reset button   ==================================*/
        self.resetButton.setTitle(TEXT.RESET_UPPER, for: .normal)
        self.resetButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.resetButton.setTitleColor(UIColor.white, for: .normal)
        self.resetButton.titleLabel?.setLetterSpacing(value: 1.8)
        self.resetButton.alpha = 0.0
        
        /*===================   Cancel button   ==================================*/
        self.cancelButton.setTitle(TEXT.CANCEL_UPPER, for: .normal)
        self.cancelButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.cancelButton.setTitleColor(UIColor.white, for: .normal)
        self.cancelButton.titleLabel?.setLetterSpacing(value: 1.8)
        self.cancelButton.alpha = 0.0
        
        /*===================   Done button   ==================================*/
        self.doneButton.setTitle(TEXT.CANCEL_UPPER, for: .normal)
        self.doneButton.setImage(nil, for: .normal)
        self.doneButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.doneButton.setTitleColor(UIColor.white, for: .normal)
        self.doneButton.titleLabel?.setLetterSpacing(value: 1.8)
        self.doneButton.isSelected = false
    }
    
    func setSignatureImageView() {
        if Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray!.count != 0 {
            if Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.last!.taskDescription.range(of: "http", options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil) != nil {
                signatureImageView.setImageUrl(urlString: (Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.last!.taskDescription)!, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"))
            } else {
                let imagePath = (Singleton.sharedInstance.selectedTaskDetails.addedSignatureArray?.last!.taskDescription)!
                if(Singleton.sharedInstance.isFileExistAtPath(imagePath) == true) {
                    let imageFromDocumentDirectory = UIImage(contentsOfFile: Auxillary.createPath(imagePath))
                    NetworkingHelper.sharedInstance.allImagesCache.setObject(imageFromDocumentDirectory!, forKey: "\(imagePath)" as NSString)
                    signatureImageView.image = imageFromDocumentDirectory
                }
            }
            self.signaturePlace.isUserInteractionEnabled = false
            self.signatureImageView.isHidden = false
            self.doneButton.alpha = 0.0
            if Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true {
                self.resetButton.alpha = 0.0
                self.cancelButton.alpha = 0.0
            } else {
                self.resetButton.alpha = 1.0
                self.cancelButton.alpha = 1.0
            }
       }
    }
    
    func resetDoneButton(status:Bool) {
       // self.doneButton.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            if status == true {
                self.doneButton.setTitle(TEXT.CANCEL_UPPER, for: .normal)
                self.doneButton.setImage(nil, for: .normal)
                self.doneButton.titleLabel?.setLetterSpacing(value: 1.8)
                self.doneButton.isSelected = false
                self.cancelButton.alpha = 0.0
                self.resetButton.alpha = 0.0
                self.doneButton.alpha = 1.0
            } else {
                self.doneButton.setImage(#imageLiteral(resourceName: "doneSignature"), for: .normal)
                self.doneButton.setTitle("", for: .normal)
                self.doneButton.isSelected = true
                self.cancelButton.alpha = 1.0
                self.resetButton.alpha = 1.0
                self.doneButton.alpha = 1.0
            }
        }, completion: { finished in
        })
    }
    
    
    @IBAction func doneAction(_ sender: UIButton) {
        guard  sender.isSelected == true else {
            delegate.hideSignatureView()
            return
        }
        
        if let signatureImage = self.signaturePlace.getSignature() {
            let imagePath = Singleton.sharedInstance.saveImageToDocumentDirectory("\(Singleton.sharedInstance.selectedTaskDetails.jobId)_Signature.png", image: signatureImage)
            self.delegate.addSignatureAction(imagePath: imagePath, image: signatureImage)
        }
    }
    
    @IBAction func resetAction(_ sender: UIButton) {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.SIGNATURE_RESET, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.SIGNATURE_RESET, customAttributes: [:])
        self.signaturePlace.clearSignature()
        self.signatureImageView.isHidden = true
        self.signaturePlace.isUserInteractionEnabled = true
        self.resetDoneButton(status: true)
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        delegate.hideSignatureView()
    }
}

extension SignatureView: SignaturePlaceDelegate {
    func drawingHasStarted() {
        self.resetDoneButton(status: false)
    }
}
