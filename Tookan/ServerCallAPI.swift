//
//  ServerCallAPI.swift
//  Butlers
//
//  Created by Rakesh Kumar on 5/14/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAnalytics
import Crashlytics

func sendRequestToServer(_ url: String, params: [String:AnyObject], httpMethod: String, isZipped:Bool, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()){

    let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    var request = URLRequest(url: URL(string: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) + urlString!)!)
    request.httpMethod = httpMethod as String
    request.timeoutInterval = 20
    if(httpMethod == "POST")
    {
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue(VERSION, forHTTPHeaderField: "version")
        if isZipped == false {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        } else {
            request.addValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
            request.addValue("application/octet-stream", forHTTPHeaderField: "Content-Encoding: gzip")
        }
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    }
    let task = URLSession.shared.dataTask(with: request) {data, response, error in
        if(response != nil && data != nil) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                    //     let success = json["success"] as? Int                                  // Okay, the `json` is here, let's get the value for 'success' out of it
                    // print("Success: \(success)")
                    Analytics.logEvent(ANALYTICS_KEY.API_SUCCESS, parameters: ["API":urlString! as NSObject])
                    Answers.logCustomEvent(withName: ANALYTICS_KEY.API_SUCCESS, customAttributes: ["API":urlString ?? ""])
                    receivedResponse(true, json)
                } else {
                    Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":urlString! as NSObject])
                    Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":urlString ?? ""])

                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                    print("Error could not parse JSON: \(jsonStr ?? "")")
                    receivedResponse(false, [:])
                }
            } catch let parseError {
                print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr ?? "")'")
                Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":urlString! as NSObject])
                Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":urlString ?? ""])
                receivedResponse(false, [:])
            }
        } else {
            Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":urlString! as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":urlString ?? ""])
            receivedResponse(false, [:])
        }
    }
    task.resume()
}


func uploadingMultipleTask(_ url:String, params: [String:AnyObject], isImage:Bool, imageData:[UIImage], imageKey:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ())
{
    let boundary:NSString = "----WebKitFormBoundarycC4YiaUFwM44F6rT"
    let body:NSMutableData = NSMutableData()
    let paramsArray = params.keys
    for item in paramsArray {
        body.append(("--\(boundary)\r\n" as String).data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append("Content-Disposition: form-data; name=\"\(item)\"\r\n\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append("\(params[item]!)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    if(isImage) {
        for i in (0..<imageData.count) {
            body.append(("--\(boundary)\r\n" as String).data(using: String.Encoding.utf8, allowLossyConversion: true)!)
            body.append("Content-Disposition: form-data; name=\"\(imageKey)\"; filename=\"photoName.jpeg\"\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
            body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
            if let dataVaue = UIImageJPEGRepresentation(imageData[i], 0.5) {
                body.append(dataVaue)
            }
            body.append("\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        }
    }

    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    var request = URLRequest(url: URL(string: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) + (url as String))!)
    request.httpMethod = "POST"
    request.httpBody = body as Data
    request.timeoutInterval = 60
    request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    request.addValue(VERSION, forHTTPHeaderField: "version")
    
    let task = URLSession.shared.dataTask(with: request, completionHandler: {data, response, error -> Void in
        if(response != nil) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String:Any] {
                    //let success = json["success"] as? Int                                  // Okay, the `json` is here, let's get the value for 'success' out of it
                    Analytics.logEvent(ANALYTICS_KEY.API_SUCCESS, parameters: ["API":url as NSObject])
                    Answers.logCustomEvent(withName: ANALYTICS_KEY.API_SUCCESS, customAttributes: ["API":url])
                    receivedResponse(true, json)
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                    print("Error could not parse JSON: \(jsonStr ?? "")")
                    Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
                    Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
                    receivedResponse(false, [:])
                }
            } catch let parseError {
                print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr ?? "")'")
                Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
                Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
                receivedResponse(false, [:])
            }
        } else {
            Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
            receivedResponse(false, [:])
        }
    })
    task.resume()
}

func sendSynchronousRequestToUploadImage(_ url:String, params:[String:AnyObject], imageData:Data!,imageKey:String) -> [String:Any]! {
    let boundary:NSString = "----WebKitFormBoundarycC4YiaUFwM44F6rT"
    let body:NSMutableData = NSMutableData()
    
    let paramsArray = params.keys
    for item in paramsArray {
        body.append(("--\(boundary)\r\n" as String).data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append("Content-Disposition: form-data; name=\"\(item)\"\r\n\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append("\(params[item]!)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    if(imageData != nil) {
        body.append(("--\(boundary)\r\n" as String).data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append("Content-Disposition: form-data; name=\"\(imageKey)\"; filename=\"photoName.jpeg\"\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append(imageData!)
        body.append("\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    var request = URLRequest(url: URL(string: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) + urlString!)!)
    request.httpMethod = "POST"
    request.httpBody = body as Data
    request.timeoutInterval = 60
    request.addValue(VERSION, forHTTPHeaderField: "version")
    request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    let response:[String:Any]!
    if let data = URLSession.requestSynchronousData(request as URLRequest){//NSURLSession.requestSynchronousJSON(request) {
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                Analytics.logEvent(ANALYTICS_KEY.API_SUCCESS, parameters: ["API":url as NSObject])
                Answers.logCustomEvent(withName: ANALYTICS_KEY.API_SUCCESS, customAttributes: ["API":url])
                response = json
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                let jsonStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                print("Error could not parse JSON: \(jsonStr ?? "")")
                Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
                Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
                return nil
            }
        } catch let parseError {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
            let jsonStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            print("Error could not parse JSON: '\(jsonStr ?? "")'")
            Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
            Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
            return nil
        }
    } else {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        Analytics.logEvent(ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
        return nil
    }
    return response
}


func downloadImageAsynchronously(_ imageURL:String, imageView:UIImageView, placeHolderImage:UIImage, contentMode:UIViewContentMode) -> UIImage {
    imageView.contentMode = contentMode
    imageView.clipsToBounds = true
    if let cacheImage = NetworkingHelper.sharedInstance.allImagesCache.object(forKey: "\(imageURL)" as NSString) {             //image caching
        return cacheImage
    } else {
        // The image isn't cached, download the img data
        // We should perform this in a background thread
        let updatedURLString = imageURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: updatedURLString! as String)
        let request: Foundation.URLRequest = Foundation.URLRequest(url: url!)
        let mainQueue = OperationQueue.main
        
        NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                // Convert the downloaded data in to a UIImage object
                let image = UIImage(data: data!)
                // Store the image in to our cache
                if(image != nil) {
                    NetworkingHelper.sharedInstance.allImagesCache.setObject(image!, forKey: "\(imageURL)" as NSString)
                    // Update the cell
                    DispatchQueue.main.async(execute: {
                        imageView.image = image
                    })
                } else {
                    print("corrupted image")
                    imageView.image = UIImage(named: "corruptedImage")
                }
            }
            else {
                 print("Error")
            }
        })
    }
    return placeHolderImage
}

func getImageFromDocumentDirectory(_ imagePath:String, imageView:UIImageView, placeHolderImage:UIImage, contentMode:UIViewContentMode) {
    imageView.image = placeHolderImage
    imageView.contentMode = contentMode
    imageView.clipsToBounds = true
    if let cacheImage = NetworkingHelper.sharedInstance.allImagesCache.object(forKey: "\(imagePath)" as NSString) {             //image caching
        DispatchQueue.main.async(execute: {
            imageView.image = cacheImage
        })
    } else {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            if(Auxillary.isFileExistAtPath(imagePath) == true) {
                if let imageFromDocumentDirectory = UIImage(contentsOfFile: Auxillary.createPath(imagePath)) {
                    NetworkingHelper.sharedInstance.allImagesCache.setObject(imageFromDocumentDirectory, forKey: "\(imagePath)" as NSString)
                    imageView.image = imageFromDocumentDirectory
                    //})
                }
            }
        }
    }
}

