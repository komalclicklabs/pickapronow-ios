//
//  LoaderView.swift
//  Tookan
//
//  Created by Rakesh Kumar on 2/22/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

class LoaderView: UIView {

    @IBOutlet weak var loaderText: UILabel!
    var activityIndicator:DGActivityIndicatorView!
    
    override func awakeFromNib() {
        self.activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballClipRotate, tintColor: UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 1.0), size: 40.0)
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        self.activityIndicator.clipsToBounds = true
        self.activityIndicator.center = CGPoint(x: SCREEN_SIZE.width/2, y: SCREEN_SIZE.height/2 - 100)
        self.addSubview(self.activityIndicator)
    }
    
    func setLoader() {
        loaderText.text = "Please wait...".localized
        self.activityIndicator.startAnimating()
    }

    
    
}
