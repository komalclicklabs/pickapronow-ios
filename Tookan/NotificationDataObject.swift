//
//  NotificationDataObject.swift
//  Tookan
//
//  Created by CL-macmini45 on 6/23/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class NotificationDataObject: NSObject, NSCoding {
    var end_time: String?
    var start_time: String?
    var start_address: String?
    var job_id: Int? = -1
    var end_address: String?
    var d: String?
    var cust_name: String?
    var message: String?
    var accept_button: Int? = -1
    var flag: Int? = -1
    var job_type: Int?
    var timer: String?
    var m:String?
    var pickupName:String?
    var isAvailable:Int?
    var isTaxi:Int?
    var incomplete_data:Int?
    var jobs:[[String:Any]]? = [[String:Any]]()
    var battery_usage:Int? = 0
    var vertical:Int? = 0
    var registrationStatus:Int? = 0
    var admin_action_message = ""
    var channelId:Int? = 0
    var fuguBody:String? = ""
    var fuguTitle: String? = ""
    var jsonObject = [String: Any]()
    
    required init(coder aDecoder: NSCoder) {
        end_time = aDecoder.decodeObject(forKey: "end_time") as? String
        start_time = aDecoder.decodeObject(forKey: "start_time") as? String
        start_address = aDecoder.decodeObject(forKey: "start_address") as? String
        job_id = aDecoder.decodeObject(forKey: "job_id") as? Int
        end_address = aDecoder.decodeObject(forKey: "end_address") as? String
        d = aDecoder.decodeObject(forKey: "d") as? String
        cust_name = aDecoder.decodeObject(forKey: "cust_name") as? String
        message = aDecoder.decodeObject(forKey: "message") as? String
        accept_button = aDecoder.decodeObject(forKey: "accept_button") as? Int
        flag = aDecoder.decodeObject(forKey: "flag") as? Int
        job_type = aDecoder.decodeObject(forKey: "job_type") as? Int
        timer = aDecoder.decodeObject(forKey: "timer") as? String
        m = aDecoder.decodeObject(forKey: "m") as? String
        pickupName = aDecoder.decodeObject(forKey: "pickupName") as? String
        isAvailable = aDecoder.decodeObject(forKey: "isAvailable") as? Int
        isTaxi = aDecoder.decodeObject(forKey: "isTaxi") as? Int
        incomplete_data = aDecoder.decodeObject(forKey: "incomplete_data") as? Int
        battery_usage = aDecoder.decodeObject(forKey: "battery_usage") as? Int
        jobs = aDecoder.decodeObject(forKey: "jobs") as? [[String:Any]]
        registrationStatus = aDecoder.decodeObject(forKey: "registrationStatus") as? Int
        admin_action_message = aDecoder.decodeObject(forKey: "admin_action_message") as! String
        channelId = aDecoder.decodeObject(forKey: "channelId") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(end_time, forKey: "end_time")
        aCoder.encode(start_time, forKey: "start_time")
        aCoder.encode(start_address, forKey: "start_address")
        aCoder.encode(job_id, forKey: "job_id")
        aCoder.encode(end_address, forKey: "end_address")
        aCoder.encode(d, forKey: "d")
        aCoder.encode(cust_name, forKey: "cust_name")
        aCoder.encode(message, forKey: "message")
        aCoder.encode(accept_button, forKey: "accept_button")
        aCoder.encode(flag, forKey: "flag")
        aCoder.encode(job_type, forKey: "job_type")
        aCoder.encode(timer, forKey: "timer")
        aCoder.encode(m, forKey: "m")
        aCoder.encode(pickupName, forKey: "pickupName")
        aCoder.encode(isAvailable, forKey: "isAvailable")
        aCoder.encode(isTaxi, forKey: "isTaxi")
        aCoder.encode(incomplete_data, forKey: "incomplete_data")
        aCoder.encode(jobs, forKey: "jobs")
        aCoder.encode(battery_usage, forKey: "battery_usage")
        aCoder.encode(registrationStatus, forKey: "registrationStatus")
        aCoder.encode(admin_action_message, forKey: "admin_action_message")
        aCoder.encode(registrationStatus, forKey: "channelId")
    }
    
    
    init(json: [String:Any]){
        jsonObject = json
        if let endTime = json["end_time"] as? String {
            end_time = endTime
        } else {
            end_time = ""
        }
        
        if let startTime = json["start_time"] as? String {
            start_time = startTime
        } else {
            start_time = ""
        }
        
        if let startAddress = json["start_address"] as? String {
            start_address = startAddress
        } else {
            start_address = ""
        }
        
        if let jobId = json["job_id"] as? Int {
            job_id = jobId
        } else if let jobId = json["job_id"] as? String {
            job_id = Int(jobId)
        } else {
            job_id = -1
        }
        
        if let endAddress = json["end_address"] as? String {
            end_address = endAddress
        } else {
            end_address = ""
        }
        
        if let date = json["d"] as? String {
            d = date
        } else {
            d = ""
        }
        
        if let mm = json["m"] as? String {
            m = mm
        } else {
            m = ""
        }
        
        if let custName = json["cust_name"] as? String {
            cust_name = custName
        } else {
            cust_name = ""
        }
        
        if let pickName = json["pick_name"] as? String {
            self.pickupName = pickName
        } else {
            self.pickupName = ""
        }
        
        
        if let msg = json["message"] as? String {
            message = msg
        } else {
            message = ""
        }
        
        if let acceptButton = json["accept_button"] as? Int {
            accept_button = acceptButton
        } else if let acceptButton = json["accept_button"] as? String {
            if let value = Int(acceptButton) {
                accept_button = value
            } else {
                accept_button = -1
            }
        }else {
            accept_button = -1
        }
        
        if let flagType = json["flag"] as? Int {
            flag = flagType
        } else if let flagType = json["flag"] as? String {
            if let value = Int(flagType) {
                flag = value
            } else {
              flag = -1
            }
        } else {
            flag = -1
        }
        
        if let jobType = json["job_type"] as? Int {
            job_type = jobType
        } else if let jobType = json["job_type"] as? String {
            if let value = Int(jobType) {
                job_type = value
            } else {
                job_type = -1
            }
        } else {
            job_type = -1
        }
        
        if let time = json["timer"] as? String {
            if time == "-" {
                timer = "-1"
            } else {
                timer = time
            }
        } else if let time = json["timer"] as? Int {
            timer = "\(time)"
        } else {
            timer = "-1"
        }
        
        if let value = json["is_available"] as? Int {
            isAvailable = value
        } else if let value = json["is_available"] as? String {
            if let intValue = Int(value) {
                isAvailable = intValue
            }
            
        }
        
        if let value = json["vertical"] as? Int {
            isTaxi = value
        } else if let value = json["vertical"] as? String {
            if let intValue = Int(value) {
                isTaxi = intValue
            }
        } else {
            isTaxi = 0
        }
        
        if let value = json["incomplete_data"] as? Int {
            incomplete_data = value
        } else if let value = json["incomplete_data"] as? String {
            if let intValue = Int(value) {
                incomplete_data = intValue
            }
        } else {
            incomplete_data = 0
        }
        
        if let value = json["jobs"] as? [[String:Any]] {
            self.jobs = value
        }
        
        if let value = json["battery_usage"] as? Int {
            self.battery_usage = value
        } else if let value = json["battery_usage"] as? String {
            if let intValue = Int(value) {
                battery_usage = intValue
            }
        }
        
        if let registrationStatus = json["registration_status"] as? Int {
            self.registrationStatus = registrationStatus
        } else if let registrationStatus = json["registration_status"] as? String {
            if let intValue = Int(registrationStatus) {
                self.registrationStatus = intValue
            }
        } else {
            self.registrationStatus = 0
        }
        
        if let value = json["admin_action_message"] as? String {
            self.admin_action_message = value
        }
        
        if let registrationStatus = json["channel_id"] as? Int {
            self.channelId = registrationStatus
        } else if let registrationStatus = json["channel_id"] as? String {
            if let intValue = Int(registrationStatus) {
                self.channelId = intValue
            }
        } else {
            self.channelId = 0
        }
    }
}
