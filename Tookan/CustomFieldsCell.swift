//
//  CustomFieldsCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 01/03/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
protocol CustomFieldDelegate {
    func customFieldShouldBeginEditing(textView:UITextView) -> Bool
    func customFieldShouldReturn(textView:UITextView) -> Bool
    func editAction(sender:UIButton)
    func updateTableHeight(descriptionText: String)
}

class CustomFieldsCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet var icon: UIImageView!
    @IBOutlet var detailView: UITextView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var placeholderLabel: UILabel!
   
    var delegate:CustomFieldDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*------------- Placeholder label -----------*/
        placeholderLabel.textColor = COLOR.LIGHT_COLOR
        placeholderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        
        /*---------- Text View -------------*/
        detailView.textColor = COLOR.TEXT_COLOR
        detailView.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        detailView.delegate = self
        detailView.tintColor = COLOR.themeForegroundColor
        
        self.actionButton.setImage(#imageLiteral(resourceName: "doneSmall").withRenderingMode(.alwaysTemplate), for: .normal)
        self.actionButton.setImage(#imageLiteral(resourceName: "edit").withRenderingMode(.alwaysTemplate), for: .selected)
        self.actionButton.tintColor = COLOR.themeForegroundColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        delegate.editAction(sender:sender)
    }
    
    //MARK: UITEXTVIEW DELEGATE
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty == true {
            placeholderLabel.isHidden = false
        } else {
            placeholderLabel.isHidden = true
        }
        delegate.updateTableHeight(descriptionText: textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            return delegate.customFieldShouldReturn(textView: textView)
        }
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return delegate.customFieldShouldBeginEditing(textView: textView)
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return delegate.customFieldShouldReturn(textView: textView)
    }
}
