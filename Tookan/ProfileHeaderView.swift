//
//  ProfileHeaderView.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 6/14/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class ProfileHeaderView: UIView {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var profileHeaderView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var transView: UIView!
    @IBOutlet weak var profilePicActivityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        self.setCameraPic()
        self.setTransView()
        self.setEditButton()
        self.setHeaderLabel()
        self.profileHeaderView.backgroundColor = COLOR.LINE_COLOR
        self.profileHeaderView.layer.borderColor = COLOR.EXTRA_LIGHT_COLOR.cgColor
        self.profileHeaderView.layer.borderWidth = 0.5
    }
    
    func setProfilePic()  {
        /*===============Setting round Profile Pic=================*/
        self.profilePic.layer.cornerRadius = getAspectRatioValue(value: self.profilePic.frame.height/2)
        self.profilePic.layer.masksToBounds = false
        self.profilePic.clipsToBounds = true
        self.profilePic.setImageUrl(urlString: Singleton.sharedInstance.fleetDetails.fleetImage!, placeHolderImage: #imageLiteral(resourceName: "img_placeholder"))
        self.profilePic.layer.borderWidth = 1.0
        self.profilePic.layer.borderColor = COLOR.EXTRA_LIGHT_COLOR.cgColor
    }
    
    func setCameraPic() {
        /*===============Setting round Camera Pic Image=================*/
        self.cameraImage.layer.cornerRadius = self.cameraImage.frame.height/2
        self.cameraImage.layer.masksToBounds = false
        self.cameraImage.clipsToBounds = true
        self.cameraImage.backgroundColor = COLOR.TABLE_HEADER_COLOR
        self.cameraImage.image = #imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate)
        self.cameraImage.tintColor = COLOR.themeForegroundColor
        self.cameraImage.isUserInteractionEnabled = true
        self.cameraImage.layer.masksToBounds = false
        self.cameraImage.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.cameraImage.layer.shadowOpacity = 0.7
        self.cameraImage.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        self.cameraImage.layer.shadowRadius = 3
        
    }
    
    func setHeaderLabel()  {
        /*===============Setting Header Label=================*/
        self.headerLabel.text = TEXT.ACCOUNT_DETAILS
        self.headerLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.headerLabel.setLetterSpacing(value: CGFloat(1.8))
        self.headerLabel.textColor = COLOR.LIGHT_COLOR
    }
    
    func setEditButton() {
        /*===============Setting Edit Button Design=================*/
        self.editButton?.setTitle(TEXT.EDIT, for: .normal)
        self.editButton.setTitle(TEXT.CANCEL, for: .selected)
        self.editButton?.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.small)
        self.editButton.setTitleColor(COLOR.themeForegroundColor, for: .normal)
        self.editButton.setTitleColor(COLOR.TEXT_COLOR, for: .selected)
    }
    
    func setTransView()  {
        /*===============Setting Trans View=================*/
        self.transView.isHidden = true
        self.transView.layer.cornerRadius = getAspectRatioValue(value: self.transView.frame.height/2)
        self.transView.layer.masksToBounds = false
        self.transView.clipsToBounds = true
        self.profilePicActivityIndicator.isHidden = true
        self.transView.backgroundColor = COLOR.imagePreviewBackgroundColor
    }
}
