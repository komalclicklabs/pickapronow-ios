//
//  SignupImageCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 05/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol SignupImageDelegate {
    func imageTapped(imageType:ImageType, index:Int, collectionTag:Int)
    func deleteImage(section:Int, row:Int)
}

class SignupImageCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imageCollectionView: UICollectionView!
    @IBOutlet var bottomLine: UIView!
    @IBOutlet var placeholderLabel: UILabel!
    var delegate:SignupImageDelegate!
    var previewMode = false
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = COLOR.TEXT_COLOR
        titleLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.bottomLine.backgroundColor = COLOR.LIGHT_COLOR
        /*------------- Placeholder label -----------*/
        placeholderLabel.textColor = COLOR.LIGHT_COLOR
        placeholderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.setImageCollectionCell()
    }

    
    func setImageCollectionCell() {
        self.imageCollectionView.register(UINib(nibName: NIB_NAME.imageCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.imageCollectionCell)
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: UICollection View Delegate...
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (Singleton.sharedInstance.fleetDetails.signup_template_data![collectionView.tag].imageArray.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width:CGFloat = 82
        let height:CGFloat = 82
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.imageCollectionCell, for: indexPath) as! ImageCollectionCell
        cell.imageView.contentMode = UIViewContentMode.scaleAspectFill
        cell.imageView.layer.cornerRadius = 3
        let signupTemplateData = Singleton.sharedInstance.fleetDetails.signup_template_data![collectionView.tag]
        if indexPath.item < signupTemplateData.imageArray.count {
            cell.removeButton.tag = collectionView.tag * 1000 + indexPath.item
            cell.removeButton.addTarget(self, action: #selector(self.deleteImage(sender:)), for: .touchUpInside)
            if((signupTemplateData.imageArray.object(at: indexPath.item) as AnyObject).isKind(of: UIImage.classForCoder()) == true) {
                cell.transLayer.isHidden = true
                cell.activityIndicator.stopAnimating()
                cell.imageView.image = signupTemplateData.imageArray.object(at: (indexPath as NSIndexPath).item) as? UIImage
            } else {
                if(Auxillary.isFileExistAtPath(signupTemplateData.imageArray.object(at:indexPath.item) as! String) == true) {
                    cell.removeButton.isHidden = true
                    cell.transLayer.isHidden = false
                    cell.activityIndicator.startAnimating()
                    cell.imageView.setImageFromDocumentDirectory(imagePath: signupTemplateData.imageArray.object(at: indexPath.item) as! String, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"))
                } else {
                    cell.removeButton.isHidden = false
                    cell.transLayer.isHidden = true
                    cell.activityIndicator.stopAnimating()
                    cell.imageView.setImageUrl(urlString: signupTemplateData.imageArray.object(at: indexPath.item) as! String, placeHolderImage: #imageLiteral(resourceName: "imagePlaceholder"), indexPath: indexPath, view: collectionView)
                }
            }
        }
        
        if self.previewMode == true{
            cell.removeButton.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.imageTapped(imageType: .signupImage, index: indexPath.item, collectionTag: collectionView.tag)
    }

    func deleteImage(sender:UIButton) {
        print(sender.tag)
        self.delegate.deleteImage(section: sender.tag / 1000, row: sender.tag % 1000)
    }
}
