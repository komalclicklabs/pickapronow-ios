//
//  ActivityIndicator.swift
//  Butlers
//
//  Created by Rakesh Kumar on 5/20/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import UIKit
import DGActivityIndicatorView

class ActivityIndicator:UIView {
    
    struct Static {
        
        static var instance: ActivityIndicator?
        static var token: Int = 0
    }
    
    private static var __once: () = {
            Static.instance = ActivityIndicator(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        }()
    
    class var sharedInstance: ActivityIndicator {
        _ = ActivityIndicator.__once
        return Static.instance!
    }
   
    var activityIndicator:DGActivityIndicatorView!
   // var activityIndicator:UIActivityIndicatorView!
  //  var imageArray = NSMutableArray()
  //  var imageView = UIImageView()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.backgroundColor = COLOR.TRANS_COLOR
        self.clipsToBounds = true
//        for(var i = 1; i <= 8; i++)
//        {
//            imageArray.addObject(UIImage(named: "frame\(i)")!)
//        }
//        
//        imageView.frame = CGRectMake(0, 0, 50, 50)
//        imageView.image = UIImage(named: "frame1")
//        imageView.center = CGPointMake(self.frame.width/2, self.frame.height/2)
//        self.addSubview(imageView)
        
        
       // self.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
    }

    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showActivityIndicator(_ controller:UIViewController){
         DispatchQueue.main.async(execute: { () -> Void in
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.1))
            if(self.activityIndicator != nil)
            {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.removeFromSuperview()
                self.activityIndicator = nil
            }
//            self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//            self.activityIndicator.center = self.center
//            self.activityIndicator.clipsToBounds = true
//            self.activityIndicator.hidesWhenStopped = true
//            self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
//            self.activityIndicator.color = UIColor.white
//            self.activityIndicator.startAnimating()
//            self.addSubview(self.activityIndicator)
            
//            self.imageView.animationImages = self.imageArray as [AnyObject]
//            self.imageView.animationDuration = 1.0
//            self.imageView.startAnimating()
            
            self.activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballClipRotate, tintColor: .white, size: 40.0)
            self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
            self.activityIndicator.center = self.center
            self.activityIndicator.clipsToBounds = true
            self.activityIndicator.startAnimating()
            self.addSubview(self.activityIndicator)
            
            
            (UIApplication.shared.delegate as! AppDelegate).window?.addSubview(self)
            //controller.view.addSubview(self)
            //controller.view.bringSubviewToFront(self)
        })
     }
    
    func showActivityIndicator(){
        DispatchQueue.main.async(execute: { () -> Void in
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.1))
            if(self.activityIndicator != nil)
            {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.removeFromSuperview()
                self.activityIndicator = nil
            }
            
            self.activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballClipRotate, tintColor: .white, size: 40.0)
            self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
            self.activityIndicator.center = self.center
            self.activityIndicator.clipsToBounds = true
            self.activityIndicator.startAnimating()
            self.addSubview(self.activityIndicator)
            (UIApplication.shared.delegate as! AppDelegate).window?.addSubview(self)
        })
    }
    
    func hideActivityIndicator(){
        DispatchQueue.main.async(execute: { () -> Void in
            
//            self.imageView.stopAnimating()
//            self.removeFromSuperview()
            if(self.activityIndicator != nil)
            {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.removeFromSuperview()
                self.activityIndicator = nil
                self.removeFromSuperview()
            }
        })
    }
}
