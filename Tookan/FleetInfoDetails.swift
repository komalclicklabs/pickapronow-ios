//
//  FleetInfoDetails.swift
//  Tookan
//
//  Created by Click Labs on 7/8/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import Crashlytics

class FleetInfoDetails: NSObject, NSCoding {
    
    struct AppWebViewToken {
        var name:String? = ""
        var url:String? = ""
        
        init(json:[String:Any]) {
            if let value = json["name"] as? String {
                self.name = value
            }
            
            if let value = json["url"] as? String {
                self.url = value
            }
        }
    }
    
    var fleetId:Int? = 0
    var fleetImage:String? = ""
    var accessToken:String? = ""
    var email:String? = ""
    var phone:String? = ""
    var password:String? = ""
    var transportType:String? = ""
    var transportDesc:String? = ""
    var license:String? = ""
    var color:String? = ""
    var userId:Int? = 0
    var registrationStatus:Int? = 0
    var lastLoginDatetime:String? = ""
    var isActive:Int? = 0
    var isAvailable:Int? = 0
    var name:String? = ""
    var teams:AssignedTeamDetails? = AssignedTeamDetails(json: [:])
    var hasRouting:Int? = 0
    var notiTone:String? = ""
    var layoutType:Int? = 0
    var isCreateTaskAvailable:Int? = 0
    var batteryUsage:Int? = 0
    var appCaching:Int? = 0
    var calendarEditStatus:Int? = 0
    var calendarViewStatus:Int? = 0
    var taxi_rides:[TasksAssignedToDate]? = [TasksAssignedToDate]()
    var isUDPEnabled:Int? = 0
    var oldPassword = ""
    var newPassword = ""
    var retypePassword = ""
    var availability_days_limit:Int? = 0
    var signup_template_data:[CustomFields]? = [CustomFields]()
    var admin_action_message:String? = ""
    var fleet_signup_info :String? = ""
    var fleet_signup:Int? = 0
    var isEarning:Int? = 0
    var rating_comments:Int? = 0
    var fugu_chat:Int? = 0
    var fugu_token:String? = ""
    var has_invoicing_module:Int? = 0
    var app_web_view:Int? = 0
    var app_web_view_token:[AppWebViewToken]? = [AppWebViewToken]()
    var isScanToAssignAvailable:Int? = 0
    var tagsArray:[String]? = [String]()
    var teamsArray:[String]? = [String]()
    var selectedTags:[String]? = [String]()
    var isTagsEnabled:Int? = 0
    var isTeamEnabled:Int? = 0
    var showTagsDropDown:Bool? = false
    var showTeams: Int? = 0
    var showTags: Int? = 0
    var callFuguAs: String? = ""
    
    required init(coder aDecoder: NSCoder) {
        fleetId = aDecoder.decodeObject(forKey: "fleetId") as? Int
        fleetImage = aDecoder.decodeObject(forKey: "fleetImage") as? String
        accessToken = aDecoder.decodeObject(forKey: "accessToken") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        password = aDecoder.decodeObject(forKey: "password") as? String
        transportType = aDecoder.decodeObject(forKey: "transportType") as? String
        transportDesc = aDecoder.decodeObject(forKey: "transportDesc") as? String
        license = aDecoder.decodeObject(forKey: "license") as? String
        color = aDecoder.decodeObject(forKey: "color") as? String
        userId = aDecoder.decodeObject(forKey: "userId") as? Int
        registrationStatus = aDecoder.decodeObject(forKey: "registrationStatus") as? Int
        lastLoginDatetime = aDecoder.decodeObject(forKey: "lastLoginDatetime") as? String
        isActive = aDecoder.decodeObject(forKey: "isActive") as? Int
        isAvailable = aDecoder.decodeObject(forKey: "isAvailable") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        teams = aDecoder.decodeObject(forKey: "teams") as? AssignedTeamDetails
        hasRouting = aDecoder.decodeObject(forKey: "hasRouting") as? Int
        notiTone = aDecoder.decodeObject(forKey: "notiTone") as? String
        layoutType = aDecoder.decodeObject(forKey: "layoutType") as? Int
        isCreateTaskAvailable = aDecoder.decodeObject(forKey: "isCreateTaskAvailable") as? Int
        batteryUsage = aDecoder.decodeObject(forKey: "batteryUsage") as? Int
        appCaching = aDecoder.decodeObject(forKey: "appCaching") as? Int
        calendarEditStatus = aDecoder.decodeObject(forKey: "calendarEditStatus") as? Int
        calendarViewStatus = aDecoder.decodeObject(forKey: "calendarViewStatus") as? Int
        taxi_rides = aDecoder.decodeObject(forKey: "taxi_rides") as? [TasksAssignedToDate]
        isUDPEnabled = aDecoder.decodeObject(forKey: "isUDPEnabled") as? Int
        availability_days_limit = aDecoder.decodeObject(forKey: "availability_days_limit") as? Int
        signup_template_data = (aDecoder.decodeObject(forKey: "signup_template_data") as? [CustomFields])
        admin_action_message = aDecoder.decodeObject(forKey: "admin_action_message") as? String
        fleet_signup_info = aDecoder.decodeObject(forKey: "fleet_signup_info") as? String
        isEarning = aDecoder.decodeObject(forKey: "isEarning") as? Int
        rating_comments = aDecoder.decodeObject(forKey: "rating_comments") as? Int
        fugu_chat = aDecoder.decodeObject(forKey: "fugu_chat") as? Int
        fugu_token = aDecoder.decodeObject(forKey: "fugu_token") as? String
        has_invoicing_module = aDecoder.decodeObject(forKey: "has_invoicing_module") as? Int
        app_web_view = aDecoder.decodeObject(forKey: "app_web_view") as? Int
        fleet_signup = aDecoder.decodeObject(forKey: "fleet_signup") as? Int
        isScanToAssignAvailable = aDecoder.decodeObject(forKey: "isScanToAssignAvailable") as? Int
        tagsArray = aDecoder.decodeObject(forKey: "tagsArray") as? [String]
        teamsArray = aDecoder.decodeObject(forKey: "teamsArray") as? [String]
        isTagsEnabled = aDecoder.decodeObject(forKey: "isTagsEnabled") as? Int
        isTeamEnabled = aDecoder.decodeObject(forKey: "isTeamEnabled") as? Int
        showTagsDropDown = aDecoder.decodeObject(forKey: "showTagsDropDown") as? Bool
        selectedTags = aDecoder.decodeObject(forKey: "selectedTags") as? [String]
        showTeams = aDecoder.decodeObject(forKey: "showTeams") as? Int
        showTags = aDecoder.decodeObject(forKey: "showTags") as? Int
        callFuguAs = aDecoder.decodeObject(forKey: "callFuguAs") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(fleetId, forKey: "fleetId")
        aCoder.encode(fleetImage, forKey: "fleetImage")
        aCoder.encode(accessToken, forKey: "accessToken")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(password, forKey: "password")
        aCoder.encode(transportType, forKey: "transportType")
        aCoder.encode(transportDesc, forKey: "transportDesc")
        aCoder.encode(license, forKey: "license")
        aCoder.encode(color, forKey: "color")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(registrationStatus, forKey: "registrationStatus")
        aCoder.encode(lastLoginDatetime, forKey: "lastLoginDatetime")
        aCoder.encode(isActive, forKey: "isActive")
        aCoder.encode(isAvailable, forKey: "isAvailable")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(teams, forKey: "teams")
        aCoder.encode(hasRouting, forKey: "hasRouting")
        aCoder.encode(notiTone, forKey: "notiTone")
        aCoder.encode(layoutType, forKey: "layoutType")
        aCoder.encode(isCreateTaskAvailable, forKey: "isCreateTaskAvailable")
        aCoder.encode(batteryUsage, forKey: "batteryUsage")
        aCoder.encode(appCaching, forKey: "appCaching")
        aCoder.encode(calendarViewStatus, forKey: "calendarViewStatus")
        aCoder.encode(calendarEditStatus, forKey: "calendarEditStatus")
        aCoder.encode(taxi_rides, forKey: "taxi_rides")
        aCoder.encode(isUDPEnabled, forKey: "isUDPEnabled")
        aCoder.encode(availability_days_limit, forKey: "availability_days_limit")
        aCoder.encode(signup_template_data, forKey: "signup_template_data")
        aCoder.encode(fleet_signup_info, forKey: "fleet_signup_info")
        aCoder.encode(admin_action_message, forKey: "admin_action_message")
        aCoder.encode(isEarning, forKey: "isEarning")
        aCoder.encode(rating_comments, forKey: "rating_comments")
        aCoder.encode(fugu_chat, forKey: "fugu_chat")
        aCoder.encode(fugu_token, forKey: "fugu_token")
        aCoder.encode(has_invoicing_module, forKey: "has_invoicing_module")
        aCoder.encode(app_web_view, forKey: "app_web_view")
        aCoder.encode(fleet_signup, forKey: "fleet_signup")
        aCoder.encode(isScanToAssignAvailable, forKey: "isScanToAssignAvailable")
        aCoder.encode(tagsArray, forKey: "tagsArray")
        aCoder.encode(teamsArray, forKey: "teamsArray")
        aCoder.encode(isTeamEnabled, forKey: "isTeamEnabled")
        aCoder.encode(isTagsEnabled, forKey: "isTagsEnabled")
        aCoder.encode(showTagsDropDown, forKey: "showTagsDropDown")
        aCoder.encode(selectedTags, forKey: "selectedTags")
        aCoder.encode(showTeams, forKey: "showTeams")
        aCoder.encode(showTags, forKey: "showTags")
        aCoder.encode(callFuguAs, forKey: "callFuguAs")
    }
    
    init(json: NSDictionary) {
        if let batteryData = json["battery_usage"] as? NSNumber {
            self.batteryUsage = Int(batteryData)
        } else if let batteryData = json["battery_usage"] as? String {
            if let intValue = Int(batteryData){
                self.batteryUsage = intValue
            }
        }

        if let fleetId = json["fleet_id"] as? NSNumber {
            self.fleetId = Int(fleetId)
            Crashlytics.sharedInstance().setUserIdentifier("\(fleetId)")
        } else if let fleetId = json["fleet_id"] as? String{
            if let intValue = Int(fleetId){
                self.fleetId = intValue
                Crashlytics.sharedInstance().setUserIdentifier("\(intValue)")
            }
        }
        
        if let fleetImage = json["fleet_image"] as? String {
            self.fleetImage = fleetImage
        }
        
        if let accessToken = json["access_token"] as? String {
            self.accessToken = accessToken
        }
        
        if let email = json["email"] as? String {
            self.email = email
            Crashlytics.sharedInstance().setUserEmail(email)
        }
        if let phone = json["phone"] as? String {
            self.phone = phone
        }
        if let password = json["password"] as? String {
            self.password = password
        }
        
        if let transportType = json["transport_type"] as? Int {
            self.transportType = String("\(transportType)")
        }else if let transportType = json["transport_type"] as? String {
            if transportType.length > 0 {
                self.transportType = transportType
            }else {
                self.transportType = "4"
            }
        }else if let transportType = json["transport_type"] as? Double {
            self.transportType = "\(transportType)"
        }else if let transportType = json["transport_type"] as? CGFloat {
            self.transportType = "\(transportType)"
        } else {
            self.transportType = "4"
        }
        
        if let transportDesc = json["transport_desc"] as? String {
            self.transportDesc = transportDesc
        }
        
        if let license = json["license"] as? String {
            self.license = license
        }
        
        if let color = json["color"] as? String {
            self.color = color
        }
        
        if let userId = json["user_id"] as? NSNumber {
            self.userId = Int(userId)
        } else if let userId = json["user_id"] as? String {
            if let intValue = Int(userId) {
                self.userId = intValue
            }
        }
        
        if let registrationStatus = json["registration_status"] as? NSNumber {
            self.registrationStatus = Int(registrationStatus)
        } else if let registrationStatus = json["registration_status"] as? String {
            if let intValue = Int(registrationStatus) {
                self.registrationStatus = intValue
            }
        }
        
        if let lastLoginDatetime = json["last_login_datetime"] as? String {
            self.lastLoginDatetime = lastLoginDatetime
        }
        
        if let isActive = json["is_acive"] as? NSNumber {
            self.isActive = Int(isActive)
        } else if let isActive = json["is_acive"] as? String{
            if let intValue = Int(isActive) {
                self.isActive = intValue
            }
        }
        
        if let isAvailable = json["is_available"] as? NSNumber {
            self.isAvailable = Int(isAvailable)
            UserDefaults.standard.set(self.isAvailable, forKey: USER_DEFAULT.userAvailableStatus)
        } else if let isAvailable = json["is_available"] as? String {
            if let intValue = Int(isAvailable) {
                self.isAvailable = intValue
            }
        }
        
        if let hasRouting = json["has_routing"] as? NSNumber {
            self.hasRouting = Int(hasRouting)
        } else if let hasRouting = json["has_routing"] as? String {
            if let intValue = Int(hasRouting) {
                self.hasRouting = intValue
            }
        }
        
        if let name = json["name"] as? String {
            self.name = name
            Crashlytics.sharedInstance().setUserIdentifier("\(name)")
        }
        
        if let notiTone = json["noti_tone"] as? String {
            self.notiTone = notiTone
        }
        
        if let items = json["teams"] as? NSArray {
            for item in items{
               teams = AssignedTeamDetails(json: item as! NSDictionary)
            }
        }
        
        if let layout = json["layout_type"] as? NSNumber {
            self.layoutType = Int(layout)
        } else if let value = json["layout_type"] as? String {
            if value.length > 0 {
                if let valueInt = Int(value) {
                    self.layoutType = valueInt
                }
            }
        }
        
        UserDefaults.standard.set(self.layoutType, forKey: USER_DEFAULT.layoutType)
        
        if let createTask = json["create_task"] as? Int {
            self.isCreateTaskAvailable = createTask
        } else if let createTask = json["create_task"] as? String {
            if let intValue = Int(createTask) {
                self.isCreateTaskAvailable = intValue
            }
            
        }
        
        if let barCode = json["barcode"] as? Int {
            self.isScanToAssignAvailable = barCode
        } else if let barCode = json["barcode"] as? String{
            if let intValue = Int(barCode) {
                self.isScanToAssignAvailable = intValue
            }
        }
        
        if let caching = json["app_caching"] as? NSNumber {
            self.appCaching = Int(caching)
        } else if let caching = json["app_caching"] as? String{
            if let intValue = Int(caching) {
                self.appCaching = intValue
            }
        }
        UserDefaults.standard.set(self.appCaching, forKey: USER_DEFAULT.appCaching)
        
        if let calendarEdit = json["fleet_calendar__edit_status"] as? NSNumber {
            self.calendarEditStatus = Int(calendarEdit)
            Singleton.sharedInstance.availabilityEditStatus = Int(calendarEdit)
        } else if let value = json["fleet_calendar__edit_status"] as? String {
            if let intValue = Int(value) {
                self.calendarEditStatus = intValue
                Singleton.sharedInstance.availabilityEditStatus = intValue
            }
        }
        
        if let calendarView = json["fleet_calendar__view_status"] as? NSNumber {
            self.calendarViewStatus = Int(calendarView)
            Singleton.sharedInstance.availabilityViewStatus = Int(calendarView)
        } else if let value = json["fleet_calendar__view_status"] as? String {
            if let intValue = Int(value) {
                self.calendarViewStatus = intValue
                Singleton.sharedInstance.availabilityViewStatus = intValue
            }
        }
        
        if let value = json["enable_UDP"] as? NSNumber {
            self.isUDPEnabled = Int(value)
        } else if let value = json["enable_UDP"] as? String {
            if let intValue = Int(value) {
                self.isUDPEnabled = intValue
            }
        }
        
        if let value = json["availability_days_limit"] as? NSNumber {
            self.availability_days_limit = Int(value)
        } else if let value = json["availability_days_limit"] as? String {
            if let intValue = Int(value) {
                self.availability_days_limit = intValue
            }
        }
        
        if let taxiRides = json["taxi_rides"] as? [Any] {
            for task in taxiRides {
                let taxiTask = TasksAssignedToDate(json: task as! [String:Any])
                self.taxi_rides?.append(taxiTask)
                Singleton.sharedInstance.selectedTaskDetails = taxiTask
            }
            
            if (self.taxi_rides?.count)! > 0 {
                switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
                case JOB_STATUS.successful, JOB_STATUS.canceled, JOB_STATUS.failed, JOB_STATUS.declined:
                    Singleton.sharedInstance.isTaxiTask = false
                    Singleton.sharedInstance.selectedTaskDetails = nil
                    break
                default:
                    Singleton.sharedInstance.isTaxiTask = true
                    break
                }
            } else {
                Singleton.sharedInstance.isTaxiTask = false
            }
        }
        
        if let signupData = json["signup_template_data"] as? [[String:Any]] {
            for item in signupData {
                if let customField = CustomFields(json: item) as CustomFields!{
                    self.signup_template_data?.append(customField)
                }
            }
        }
        
        if let value = json["admin_action_message"] as? String {
            self.admin_action_message = value
        }
        
        if let value = json["fleet_signup_info"] as? String {
            self.fleet_signup_info = value
        }
        
        if let value = json["earnings"] as? NSNumber {
            self.isEarning = Int(value)
        } else if let value = json["earnings"] as? String {
            self.isEarning = Int(value)!
        }
        
        if let value = json["rating_comments"] as? NSNumber {
            self.rating_comments = Int(value)
        } else if let value = json["rating_comments"] as? String {
            self.rating_comments = Int(value)!
        }
        
        if let value = json["fugu_token"] as? String {
            self.fugu_token = value
        }
        
        if let value = json["fugu_chat"] as? NSNumber {
            self.fugu_chat = Int(value)
        } else if let value = json["fugu_chat"] as? String {
            self.fugu_chat = Int(value)!
        }
        
        if let value = json["has_invoicing_module"] as? NSNumber {
            self.has_invoicing_module = Int(value)
        } else if let value = json["has_invoicing_module"] as? String {
            self.has_invoicing_module = Int(value)!
        }
        
        if let value = json["app_web_view"] as? NSNumber {
            self.app_web_view = Int(value)
        } else if let value = json["app_web_view"] as? String {
            if let intValue = Int(value) {
                self.app_web_view = intValue
            }
        }
        
        if let values = json["app_web_view_token"] as? [[String:Any]] {
            self.app_web_view_token?.removeAll()
            for value in values {
                let appWebToken = AppWebViewToken(json: value)
                self.app_web_view_token?.append(appWebToken)
            }
        }
        
        if let value = json["fleet_signup"] as? NSNumber{
            self.fleet_signup = Int(value)
        } else if let value = json["fleet_signup"] as? String{
            self.fleet_signup = Int(value)!
        }
        
        if let value = json["is_tags_enabled"] as? NSNumber{
            self.isTagsEnabled = Int(value)
        } else if let value = json["is_tags_enabled"] as? String {
            if let intValue = Int(value) {
                self.isTagsEnabled = intValue
            }
        }
        
        if let value = json["is_team_enabled"] as? NSNumber{
            self.isTeamEnabled = Int(value)
        } else if let value = json["is_team_enabled"] as? String {
            if let intValue = Int(value) {
                self.isTeamEnabled = intValue
            }
        }
        
        if let value = json["show_team"] as? NSNumber {
            self.showTeams = Int(value)
        } else if let value = json["show_team"] as? String {
            if let intValue = Int(value) {
                self.showTeams = intValue
            }
        }
        
        if let value = json["show_tags"] as? NSNumber {
            self.showTags = Int(value)
        } else if let value = json["show_tags"] as? String {
            if let intValue = Int(value) {
                self.showTags = intValue
            }
        }
        
        if let value = json["call_fugu_as"] as? String {
            self.callFuguAs = value
        }
    }
}
