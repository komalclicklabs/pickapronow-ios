//
//  ReloadView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 18/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class ReloadView: UIView {

    @IBOutlet var textLabel: UILabel!
    override func awakeFromNib() {
        textLabel.text = "Tap to reload again".localized
        
    }
}
