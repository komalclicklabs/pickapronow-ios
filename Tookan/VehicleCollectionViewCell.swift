//
//  VehicleCollectionViewCell.swift
//  Tookan
//
//  Created by CL-Macmini-110 on 3/31/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class VehicleCollectionViewCell: UICollectionViewCell {

   
    @IBOutlet weak var cellButton: UIButton!
    var buttonCornerRadius = getAspectRatioValue(value:5.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cellButton.backgroundColor = COLOR.VEHICLE_BUTTON_COLOR
        self.cellButton.isUserInteractionEnabled = false
        self.cellButton.layer.cornerRadius = buttonCornerRadius
        self.cellButton.layer.masksToBounds = true
        self.cellButton.clipsToBounds = true
        
    }

}
