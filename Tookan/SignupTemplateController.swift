//
//  SignupTemplateController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 02/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseAnalytics
import Crashlytics
import AssetsLibrary

class SignupTemplateController: UIViewController {

    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var signupView: UIView!
    @IBOutlet var signupTable: UITableView!
    @IBOutlet var tableTopConstraint: NSLayoutConstraint!
    
    let headerHeight:CGFloat = 80.0
    let footerHeight:CGFloat = 110.0
    var signupModel = SignupModel()
    var keyboardToolbar:KeyboardToolbar!
    var imagesPreview:ImagesPreview!
    var customPicker:CustomDatePicker!
    var drowDownWithSearch:TemplateController!
    let defaultBottomConstraintValue:CGFloat = 50.0
    let continueButton = UIButton(type: .custom)
    var navigation:navigationBar!
    let headerHeightForSection:CGFloat = 80.0
    var notesDelegate:NotesModelDelegate!
    var scanView:ScanView!
    var fromProfileVC = false
    var headerHeightForTags:CGFloat = 50.0
    var isSearching = false
    var filteredData = [String]()
    let getImagePicker = GetImageFromPicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
        self.signupView.layer.cornerRadius = 20.0
        self.signupModel.setSectionsAndRows()
        self.setTableView()
        self.setTableFooterView()
        if fromProfileVC == false {
            self.setTableHeaderView()
            //self.clearFleetData()
            self.continueButton.isHidden = false
        } else {
            self.tableTopConstraint.constant = 10.0
            self.setNavigationBar()
            self.continueButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        keyboardToolbar = KeyboardToolbar()
        keyboardToolbar.keyboardDelegate = self
        keyboardToolbar.addButtons()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(statusBarHeightChanged), name: NSNotification.Name.UIApplicationWillChangeStatusBarFrame, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillChangeStatusBarFrame, object: nil)
    }
    
    func statusBarHeightChanged() {
        guard self.navigation != nil else {
            return
        }
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height - 20.0
        UIView.animate(withDuration: 0.35) {
            self.navigation.frame.origin.y = statusBarHeight
        }
    }
    
    func clearFleetData() {
        for field in Singleton.sharedInstance.fleetDetails.signup_template_data! {
            field.fleetData = ""
        }
    }
    
    
    func setTableView() {
        self.signupTable.delegate = self
        self.signupTable.dataSource = self
        self.signupTable.register(UINib(nibName: NIB_NAME.signupTemplateCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.signupTemplateCell)
        self.signupTable.register(UINib(nibName: NIB_NAME.signupImageCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.signupImageCell)
        self.signupTable.register(UINib(nibName: NIB_NAME.customBarCodeCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.customBarCodeCell)
        self.signupTable.register(UINib(nibName:NIB_NAME.checklistCell, bundle:nil), forCellReuseIdentifier: NIB_NAME.checklistCell)
        self.signupTable.rowHeight = UITableViewAutomaticDimension
        
    }
    
    func setTableHeaderView() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width - 70, height: headerHeight))
        let titleLabel = UILabel(frame: CGRect(x: 8, y: 0, width: headerView.frame.width - 8, height: headerView.frame.height))
        titleLabel.font = UIFont(name: UIFont().MontserratExtraLight, size: FONT_SIZE.large)
        titleLabel.textColor =  COLOR.LIGHT_COLOR
        titleLabel.text = "\(TEXT.ENTER) \(TEXT.ADDITIONAL_INFORMATION)"
        titleLabel.setLetterSpacing(value: 0.5)
        titleLabel.numberOfLines = 0
        headerView.addSubview(titleLabel)
        self.signupTable.tableHeaderView = headerView
    }
    
    func setTableFooterView() {
        if fromProfileVC == true {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width - 70, height: 20))
            footerView.backgroundColor = UIColor.white
            self.signupTable.tableFooterView = footerView
        } else {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width - 70, height: footerHeight))
            continueButton.frame = CGRect(x: 8, y: 0, width: footerView.frame.width, height: 50.0)
            continueButton.backgroundColor = COLOR.themeForegroundColor
            continueButton.setTitle(TEXT.CONTINUE, for: .normal)
            continueButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
            continueButton.titleLabel?.font = UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.timer)
            continueButton.layer.cornerRadius = 3.0
            continueButton.center = CGPoint(x: continueButton.center.x, y: footerView.center.y)
            continueButton.addTarget(self, action: #selector(self.continueAction), for: .touchUpInside)
            footerView.addSubview(continueButton)
            self.signupTable.tableFooterView = footerView
        }
        
    }
    
    func addMandatoryFieldStar(_ title:String, required:Bool, color:UIColor) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: title, attributes: [NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!, NSForegroundColorAttributeName:color])
        let star = NSMutableAttributedString(string: " *", attributes: [NSForegroundColorAttributeName:UIColor.red,NSFontAttributeName:UIFont(name: UIFont().MontserratBold, size: FONT_SIZE.regular)!])
        if(required == true) {
            attributedString.append(star)
        }
        return attributedString
    }

    func continueAction() {
        self.view.endEditing(true)
        guard self.signupModel.showMandatoryCheck() == true else {
            return
        }
        
        var isPathExist = false
        for field in Singleton.sharedInstance.fleetDetails.signup_template_data! {
            if field.dataType == CUSTOM_FIELD_DATA_TYPE.image {
                for image in field.imageArray {
                    if (Auxillary.isFileExistAtPath(image as! String) == true) {
                        isPathExist = true
                        break
                    }
                }
            }
        }
        
        guard isPathExist == false else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_UPLOADING, isError: .error)
            return
        }
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        var fields = [[String:Any]]()
        for field in Singleton.sharedInstance.fleetDetails.signup_template_data! {
            var data = [String:Any]()
            data["label"] = field.label
            if field.dataType == CUSTOM_FIELD_DATA_TYPE.image {
                data["data"] = field.imageArray.jsonString
            } else {
                switch field.dataType {
                case CUSTOM_FIELD_DATA_TYPE.dropDown:
                    data["data"] = field.fleetData
                default:
                    if field.fleetData == "" {
                        data["data"] = field.data
                    } else {
                        data["data"] = field.fleetData
                    }
                }
                
            }
            fields.append(data)
        }
        
        print(fields)
        var params:[String:Any] = ["access_token": Singleton.sharedInstance.getAccessToken()]
        params["fields"] = fields
        if (Singleton.sharedInstance.fleetDetails.signup_template_data?.count)! > 0{
            params["template_name"] = Singleton.sharedInstance.fleetDetails.signup_template_data?[0].templateName
        } else {
            params["template_name"] = ""
        }
        params["team"] = Singleton.sharedInstance.fleetDetails.teams?.teamName
        
//        if Singleton.sharedInstance.fleetDetails.teams.count > 0{
//            if Singleton.sharedInstance.fleetDetails.teams[0].teamName == "" {
//                params["team"] = ""
//            } else {
//                
//            }
//        } else {
//            params["team"] = ""
//        }
        
        if (Singleton.sharedInstance.fleetDetails.selectedTags?.count)! > 0 {
            params["tags"] = Singleton.sharedInstance.fleetDetails.selectedTags?.joined(separator: ", ")
        } else {
            params["tags"] = ""
        }
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.submit_signup_template, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    if let status = response["status"] as? Int {
                        switch status {
                        case STATUS_CODES.SHOW_DATA:
                            if let data = response["data"] as? [String:Any] {
                                if let registration_status = data["registration_status"] as? String {
                                    Singleton.sharedInstance.fleetDetails.registrationStatus = Int(registration_status)!
                                } else if let registration_status = data["registration_status"] as? Int {
                                    Singleton.sharedInstance.fleetDetails.registrationStatus = registration_status
                                }
                                _ = self.navigationController?.popViewController(animated: true)
                            }

                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            let alert = UIAlertController(title: "", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                            let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                                DispatchQueue.main.async(execute: { () -> Void in
                                    Auxillary.logoutFromDevice()
                                    NotificationCenter.default.removeObserver(self)
                                })
                            })
                            alert.addAction(actionPickup)
                            self.present(alert, animated: true, completion: nil)
                            break
                        default:
                            Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                        }
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                }
            }
        }
    }
    
    func addImageAction(sender:UIButton) {
        if IJReachability.isConnectedToNetwork() == false {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        } else {
            getImagePicker.setImagePicker(imagePickerType:getImagePicker.getPickerTypeForSignupTemplate(tag:sender.tag), tag: sender.tag)
            getImagePicker.imageCallBack = { [weak self] (result) in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let image, let index):
                        self?.setImage(image: image, tag: index)
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                     case .error(let message):
                        Singleton.sharedInstance.showErrorMessage(error: message, isError: .error)
                    }
                }
            }
        }
    }
    
    func checkboxAction(tag: Int, isSelected: Bool) {
        if(isSelected == true) {
           // Singleton.sharedInstance.fleetDetails.signup_template_data![tag].data = "true"
            Singleton.sharedInstance.fleetDetails.signup_template_data![tag].fleetData = "true"
        } else {
            //Singleton.sharedInstance.fleetDetails.signup_template_data![tag].data = "false"
            Singleton.sharedInstance.fleetDetails.signup_template_data![tag].fleetData = "false"
        }
        UIView.setAnimationsEnabled(false)
        self.signupTable.reloadSections(IndexSet(integer:tag), with: .none)
        UIView.setAnimationsEnabled(true)
    }
    
    func checkAction(sender:UIButton) {
        sender.isSelected = !sender.isSelected
        self.checkboxAction(tag: sender.tag, isSelected: sender.isSelected)
    }
    
    func showCustomDatePicker(customField:CustomFields, tag:Int) {
        self.view.endEditing(true)
        customPicker = UINib(nibName: NIB_NAME.customDatePicker, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomDatePicker
        customPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        customPicker.delegate = self
        customPicker.tag = tag
        self.view.addSubview(customPicker)
        customPicker.setDatePicker()
        customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        switch(customField.dataType) {
        case CUSTOM_FIELD_DATA_TYPE.date:
            customPicker.datePicker.datePickerMode = UIDatePickerMode.date
            break
        case CUSTOM_FIELD_DATA_TYPE.datePast:
            customPicker.datePicker.datePickerMode = UIDatePickerMode.date
            customPicker.datePicker.maximumDate = Date()
            break
        case CUSTOM_FIELD_DATA_TYPE.dateFuture:
            customPicker.datePicker.datePickerMode = UIDatePickerMode.date
            customPicker.datePicker.minimumDate = Date()
            break
        case CUSTOM_FIELD_DATA_TYPE.dateTime:
            customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
            break
        case CUSTOM_FIELD_DATA_TYPE.dateTimeFuture:
            customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
            customPicker.datePicker.minimumDate = Date()
            break
        case CUSTOM_FIELD_DATA_TYPE.dateTimePast:
            customPicker.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
            customPicker.datePicker.maximumDate = Date()
            break
        default:
            break
        }
    }
    
    func showDropdown(customField: CustomFields, tag: Int) {
        self.view.endEditing(true)
        guard Singleton.sharedInstance.fleetDetails.signup_template_data![tag].dropdown.count > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
            return
        }
        drowDownWithSearch = TemplateController()
        drowDownWithSearch.itemArray = Singleton.sharedInstance.fleetDetails.signup_template_data![tag].dropdown
        drowDownWithSearch.placeholderValue = Singleton.sharedInstance.fleetDetails.signup_template_data![tag].displayLabelName
        drowDownWithSearch.delegate = self
        drowDownWithSearch.view.tag = tag
        drowDownWithSearch.modalPresentationStyle = .overCurrentContext
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.present(drowDownWithSearch, animated: false, completion: nil)
    }
    
    func showTeams(tag: Int) {
        self.view.endEditing(true)
        drowDownWithSearch = TemplateController()
        drowDownWithSearch.itemArray = Singleton.sharedInstance.fleetDetails.teamsArray!
        drowDownWithSearch.placeholderValue = TEXT.TEAMS
        drowDownWithSearch.delegate = self
        drowDownWithSearch.view.tag = tag
        drowDownWithSearch.modalPresentationStyle = .overCurrentContext
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.present(drowDownWithSearch, animated: false, completion: nil)
    }
    func keyboardWillShow(_ notification : Foundation.Notification){
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        Singleton.sharedInstance.keyboardSize = value.cgRectValue.size
        let keyboardSize = value.cgRectValue.size
        self.bottomConstraint.constant = self.defaultBottomConstraintValue + keyboardSize.height
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        self.bottomConstraint.constant = self.defaultBottomConstraintValue
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scanCustomBarcode(sender:UIButton) {
        let indexPath = IndexPath(row: 0, section: sender.tag)
        if let cell = self.signupTable.cellForRow(at: indexPath) as? CustomBarCodeCell {
            guard cell.actionButton.isSelected == false else {
                self.scannerAction(index: 0)
                return
            }
            self.view.endEditing(true)
            cell.actionButton.isSelected = true
        }
    }
}

extension SignupTemplateController:NavigationDelegate {
    func setNavigationBar() {
        navigation = UINib(nibName: NIB_NAME.navigationBar, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! navigationBar
        navigation.delegate = self
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height - 20.0
        navigation.frame = CGRect(x: 0, y: statusBarHeight, width: self.view.frame.width, height: HEIGHT.navigationHeight - statusBarHeight)
        navigation.titleLabel.text = TEXT.ADDITIONAL_INFORMATION
        navigation.titleLabel.setLetterSpacing(value: 1.8)
        navigation.backButton.setImage(#imageLiteral(resourceName: "back_btn").withRenderingMode(.alwaysTemplate), for: .normal)
        navigation.backButton.tintColor = UIColor.white
        navigation.titleLabel.textColor = UIColor.white
        navigation.titleLabel.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.medium)
        self.view.addSubview(navigation)
    }
    
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func editAction() {
        
    }
}

extension SignupTemplateController:UITableViewDelegate, UITableViewDataSource {
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.signupModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.signupModel.sections[section] {
        case .checklist:
            if Singleton.sharedInstance.fleetDetails.signup_template_data![section].showChecklistRows == false {
                return 0
            } else {
                return Singleton.sharedInstance.fleetDetails.signup_template_data![section].checklistArray.count
            }
        case .searchTags:
            return 1
        case .tags:
            guard (Singleton.sharedInstance.fleetDetails.tagsArray?.count)! > 0 else {
                return 0
            }
            
            guard self.isSearching == false else {
                return filteredData.count
            }
            
            if Singleton.sharedInstance.fleetDetails.showTagsDropDown == true {
                return (Singleton.sharedInstance.fleetDetails.tagsArray?.count)!
            } else {
                return 0
            }
        default:
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.section < self.signupModel.sections.count else {
            return 0
        }
        switch self.signupModel.sections[indexPath.section] {
        case .date, .datePast, .dateFuture, .dateTime, .dateTimePast, .dateTimeFuture,.email,.number, .text, .telephone, .url,.dropdown, .checkbox, .customImage, .checklist, .customBarcode:
            if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                return 0.01
            }
            return UITableViewAutomaticDimension
        case .tags, .teams:
            return UITableViewAutomaticDimension
        default:
            return 0.01
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var signupTemplate = CustomFields(json: [:])
        guard indexPath.section < self.signupModel.sections.count else {
            return UITableViewCell()
        }
        if fromProfileVC == true {
            if (Singleton.sharedInstance.fleetDetails.signup_template_data?.count)! > indexPath.section{
                Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide = APP_SIDE.read
            }
        }
        if (Singleton.sharedInstance.fleetDetails.signup_template_data?.count)! > indexPath.section {
            signupTemplate = (Singleton.sharedInstance.fleetDetails.signup_template_data?[indexPath.section])!
        }
        
        switch self.signupModel.sections[indexPath.section] {
        case .date, .datePast, .dateFuture, .dateTime, .dateTimePast, .dateTimeFuture, .email, .number, .text, .telephone, .url, .dropdown, .checkbox, .teams:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.signupTemplateCell) as! SignupTemplateCell
            cell.delegate = self
            cell.detailTextView.tag = indexPath.section
            cell.placeholderLabel.attributedText = self.addMandatoryFieldStar((signupTemplate.label.replacingOccurrences(of: "_", with: " ")), required: (signupTemplate.required)!, color: COLOR.LIGHT_COLOR)
            //cell.placeholderLabel.transform = CGAffineTransform(translationX: 0, y: cell.detailTextView.center.y - 21)
            cell.actionButton.isHidden = false
            cell.actionButton.isUserInteractionEnabled = false
            cell.detailTextView.isUserInteractionEnabled = true
            cell.actionButton.tag = indexPath.section
            cell.actionButton.isSelected = false
            cell.detailTextView.text = ""
            cell.placeholderLabel.isHidden = false
            switch self.signupModel.sections[indexPath.section] {
            case .date, .datePast, .dateFuture, .dateTime, .dateTimePast, .dateTimeFuture:
                cell.actionButton.setImage(#imageLiteral(resourceName: "customCalendar"), for: .normal)
                var updatedDate = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section])
                if updatedDate.range(of: "/") != nil {
                    updatedDate = updatedDate.replacingOccurrences(of: "/", with: "-")
                }
                switch Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].dataType {
                case CUSTOM_FIELD_DATA_TYPE.date, CUSTOM_FIELD_DATA_TYPE.datePast, CUSTOM_FIELD_DATA_TYPE.dateFuture:
                    cell.detailTextView.text! = updatedDate.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd", output: "dd MMM yyyy")
                    break
                default:
                    cell.detailTextView.text! = updatedDate.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd hh:mm a", output: "dd MMM yyyy hh:mm a")
                    break
                }
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                    cell.isUserInteractionEnabled = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
                } else {
                    cell.isUserInteractionEnabled = true
                }
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                    cell.isHidden = true
                }
                
                break
            case .email:
                cell.actionButton.setImage(#imageLiteral(resourceName: "customEmail"), for: .normal)
                let updatedData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section])
                cell.detailTextView.text = updatedData
                
                cell.detailTextView.keyboardType = UIKeyboardType.emailAddress
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                    cell.isUserInteractionEnabled = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
                } else {
                    cell.isUserInteractionEnabled = true
                }
                
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                    cell.isHidden = true
                }
                
                break
            case .number, .text:
                cell.actionButton.isHidden = true
                let updatedData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section])
                cell.detailTextView.text = updatedData

                if self.signupModel.sections[indexPath.section] == .number {
                    cell.detailTextView.keyboardType = UIKeyboardType.numberPad
                } else {
                    cell.detailTextView.keyboardType = UIKeyboardType.default
                }
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                    cell.isUserInteractionEnabled = false
                    cell.actionButton.isHidden = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
                } else {
                    cell.isUserInteractionEnabled = true
                }
                
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                    cell.isHidden = true
                }
                
                break
            case .telephone:
                cell.actionButton.setImage(#imageLiteral(resourceName: "telephone"), for: .normal)
                let updatedData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section])
                cell.detailTextView.text = updatedData

                cell.detailTextView.keyboardType = UIKeyboardType.phonePad
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                    cell.isUserInteractionEnabled = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
                } else {
                    cell.isUserInteractionEnabled = true
                }
                
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                    cell.isHidden = true
                }
                
                break
            case .url:
                cell.actionButton.setImage(#imageLiteral(resourceName: "web"), for: .normal)
                let updatedData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section])
                cell.detailTextView.text = updatedData

                cell.detailTextView.keyboardType = UIKeyboardType.URL
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                    cell.isUserInteractionEnabled = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
                } else {
                    cell.isUserInteractionEnabled = true
                }
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                    cell.isHidden = true
                }
                
                break
            case .dropdown:
                cell.actionButton.setImage(#imageLiteral(resourceName: "smallDownArrow"), for: .normal)
                let updatedData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section])
                cell.detailTextView.text = updatedData
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                    cell.isUserInteractionEnabled = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
                } else {
                    cell.isUserInteractionEnabled = true
                }
                
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                    cell.isHidden = true
                }
                break
            case .teams:
                cell.actionButton.setImage(#imageLiteral(resourceName: "smallDownArrow"), for: .normal)
                //if Singleton.sharedInstance.fleetDetails.teams.count > 0 {
                    if Singleton.sharedInstance.fleetDetails.teams?.teamName == "" {
                        cell.detailTextView.text = Singleton.sharedInstance.fleetDetails.teamsArray?.joined(separator: ", ")
                    } else {
                        cell.detailTextView.text = Singleton.sharedInstance.fleetDetails.teams?.teamName
                    }
//                } else {
//                    cell.detailTextView.text = Singleton.sharedInstance.fleetDetails.teamsArray.joined(separator: ", ")
//                }
                if fromProfileVC == true{
                    cell.isUserInteractionEnabled = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
                }
                cell.detailTextView.textColor = COLOR.TEXT_COLOR
                cell.placeholderLabel.text = TEXT.TEAMS
                break
            case .checkbox:
                if fromProfileVC == true{
                    Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide = APP_SIDE.tableEdit
                }
                cell.placeholderLabel.isHidden = true
                cell.actionButton.setImage(#imageLiteral(resourceName: "checkboxEmpty"), for: .normal)
                cell.actionButton.setImage(#imageLiteral(resourceName: "checkboxTicked"), for: .selected)
                cell.actionButton.isUserInteractionEnabled = true
                cell.detailTextView.isUserInteractionEnabled = false
                cell.detailTextView.attributedText = self.addMandatoryFieldStar((signupTemplate.label.replacingOccurrences(of: "_", with: " ")), required: (signupTemplate.required)!, color: COLOR.TEXT_COLOR)//signupTemplate?.label.replacingOccurrences(of: "_", with: " ")
                cell.actionButton.addTarget(self, action: #selector(self.checkAction(sender:)), for: .touchUpInside)
                let updatedData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section])
                if updatedData == "" || updatedData == "false" || updatedData == "0" {
                    cell.actionButton.isSelected = false
                } else {
                    cell.actionButton.isSelected = true
                }
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                    cell.isUserInteractionEnabled = false
                    cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
                } else {
                    cell.isUserInteractionEnabled = true
                }
                if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                    cell.isHidden = true
                }
                if fromProfileVC == true{
                    cell.isUserInteractionEnabled = false
                }
   
            default:
                break
            }
            if self.signupModel.sections[indexPath.section] != .checkbox {
                if cell.detailTextView.text != ""{
                    cell.tranlatePlaceholderLabel(status: true)
                }else{
                    cell.tranlatePlaceholderLabel(status: false)
                }
            }
            return cell
        case .customImage:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.signupImageCell) as! SignupImageCell
            cell.delegate = self
            cell.imageCollectionView.tag = indexPath.section
            cell.titleLabel.attributedText = self.addMandatoryFieldStar((signupTemplate.label.replacingOccurrences(of: "_", with: " ")), required: (signupTemplate.required)!, color: COLOR.LIGHT_COLOR)//"\((signupTemplate?.label.replacingOccurrences(of: "_", with: " "))!)"
            cell.actionButton.tag = indexPath.section
            cell.actionButton.addTarget(self, action: #selector(self.addImageAction(sender:)), for: .touchUpInside)
            cell.imageCollectionView.reloadData()
            
            if (signupTemplate.imageArray.count) > 0 {
                cell.imageCollectionView.isHidden = false
                cell.placeholderLabel.isHidden = true
                if (signupTemplate.imageArray.count) >= 6 {
                    cell.actionButton.isHidden = true
                } else {
                    cell.actionButton.isHidden = false
                }
            } else {
                cell.imageCollectionView.isHidden = true
                cell.placeholderLabel.isHidden = false
                cell.placeholderLabel.text = "\(TEXT.NO) \(TEXT.IMAGE)"
            }
            
            if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                cell.isUserInteractionEnabled = false
                cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive"), for: .normal)
            } else {
                cell.isUserInteractionEnabled = true
            }
            if fromProfileVC == true{
                cell.previewMode = true
                cell.isUserInteractionEnabled = true
                cell.actionButton.isUserInteractionEnabled = false
            }
            if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                cell.isHidden = true
            }

            return cell
        case .checklist:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.checklistCell) as! ChecklistCell
            let checklistValues = Singleton.sharedInstance.fleetDetails.signup_template_data?[indexPath.section].checklistArray[indexPath.row]
            cell.checklistTitle.text = checklistValues?.value?.trimText
            if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                cell.isUserInteractionEnabled = false
            } else {
                cell.isUserInteractionEnabled = true
            }
            if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                cell.isHidden = true
            }
            cell.checklistButton.isSelected = (checklistValues?.check)!
            return cell
        case .customBarcode:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customBarCodeCell) as! CustomBarCodeCell
            cell.descriptionView.delegate = self
            let fleetData = Singleton.sharedInstance.getFleetData(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section])
            cell.descriptionView.text = fleetData
            cell.placeholderLabel.text = "\(TEXT.ADD) \(Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].displayLabelName) \(TEXT.HERE)"
            cell.actionButton.setImage(#imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate), for: .selected)
            cell.actionButton.setImage(#imageLiteral(resourceName: "doneSmall").withRenderingMode(.alwaysTemplate), for: .normal)
            cell.actionButton.tintColor = COLOR.themeForegroundColor
            cell.actionButton.tag = indexPath.section
            cell.actionButton.addTarget(self, action: #selector(self.scanCustomBarcode(sender:)), for: .touchUpInside)
            cell.actionButton.isSelected = true
            cell.headerLabel.attributedText = self.addMandatoryFieldStar(TEXT.BARCODE_SMALL, required: (Singleton.sharedInstance.fleetDetails.signup_template_data?[indexPath.section].required)!, color: COLOR.LIGHT_COLOR)
            if (cell.descriptionView.text.length) == 0 {
                cell.placeholderLabel.isHidden = false
            } else {
                cell.placeholderLabel.isHidden = true
            }
            if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.read {
                cell.isUserInteractionEnabled = false
                cell.actionButton.setImage(#imageLiteral(resourceName: "iconPasswordActive").withRenderingMode(.alwaysTemplate), for: .normal)
                cell.actionButton.tintColor = UIColor.black
                cell.actionButton.isSelected = false
                cell.actionButtonTrailing.constant = -10.0
            } else {
                cell.isUserInteractionEnabled = true
            }
            if Singleton.sharedInstance.fleetDetails.signup_template_data![indexPath.section].appSide == APP_SIDE.hidden {
                cell.isHidden = true
            }
            return cell
            
        case .tags:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.checklistCell) as! ChecklistCell
            //let checklistValues = Singleton.sharedInstance.fleetDetails.tagsArray[indexPath.row]
            
            
            let tags = Singleton.sharedInstance.fleetDetails.tagsArray
            if Singleton.sharedInstance.fleetDetails.selectedTags?.contains(tags![indexPath.row]) == true {
                cell.checklistButton.isSelected = true
            } else {
                cell.checklistButton.isSelected = false
            }
            guard isSearching == false else {
                cell.checklistTitle.text = filteredData[indexPath.row]
                return cell
            }
            cell.checklistTitle.text = Singleton.sharedInstance.fleetDetails.tagsArray![indexPath.row].trimText
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.signupModel.sections[indexPath.section] {
        case .checkbox:
            if Singleton.sharedInstance.fleetDetails.signup_template_data?[indexPath.section].fleetData == "" || Singleton.sharedInstance.fleetDetails.signup_template_data?[indexPath.section].fleetData == "false" {
                self.checkboxAction(tag: indexPath.section, isSelected: true)
            } else {
                self.checkboxAction(tag: indexPath.section, isSelected: false)
            }
            break
        case .checklist:
            let checklistValues = Singleton.sharedInstance.fleetDetails.signup_template_data?[indexPath.section].checklistArray[indexPath.row]
            if(checklistValues?.check == true) {
                checklistValues?.check = false
            } else {
                checklistValues?.check = true
            }
            self.setChecklistDictionary(section: indexPath.section, row:indexPath.row)
            tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: indexPath.section)], with: .none)
        case .customBarcode:
            if let cell = self.signupTable.cellForRow(at: IndexPath(row: indexPath.row, section: indexPath.section)){
                cell.selectionStyle = .none
            }
        case .tags:
//            if isSearching == true {
//                selectedValue = filteredData[indexPath.row]
//            } else {
//                selectedValue = itemArray[indexPath.row]
//            }
            let tags = Singleton.sharedInstance.fleetDetails.tagsArray
            
            
            if Singleton.sharedInstance.fleetDetails.selectedTags?.contains(tags![indexPath.row]) == true {
                let index = Singleton.sharedInstance.fleetDetails.selectedTags?.index(of: tags![indexPath.row])
                Singleton.sharedInstance.fleetDetails.selectedTags?.remove(at: index!)
            } else {
                Singleton.sharedInstance.fleetDetails.selectedTags?.append(tags![indexPath.row])
            }
            tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: indexPath.section)], with: .none)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section < self.signupModel.sections.count else {
            return nil
        }
        switch self.signupModel.sections[section] {
        case .checklist:
            let headerView = UINib(nibName: NIB_NAME.detailHeaderView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! DetailHeaderView
            headerView.tag = section
            headerView.backgroundColor = UIColor.white
            headerView.requiredLabel.isHidden = true
            headerView.bottomLine.isHidden = true
            
            headerView.headerLabelLeading.constant = 6.0
            headerView.headerLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
            headerView.headerLabel.textColor = COLOR.TEXT_COLOR
            headerView.headerLabel.attributedText = self.addMandatoryFieldStar((Singleton.sharedInstance.fleetDetails.signup_template_data?[section].displayLabelName)!, required: (Singleton.sharedInstance.fleetDetails.signup_template_data?[section].required)!, color: COLOR.TEXT_COLOR)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.checklistHeaderTapAction(gesture:)))
            tap.numberOfTapsRequired = 1
            headerView.addGestureRecognizer(tap)
            headerView.addButtonTrailing.constant = -10.0
            headerView.addButton.setImage(#imageLiteral(resourceName: "smallDownArrow").withRenderingMode(.alwaysTemplate), for: .normal)
            headerView.addButton.isUserInteractionEnabled = false
            if Singleton.sharedInstance.fleetDetails.signup_template_data![section].showChecklistRows == true {
                headerView.addButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
            } else {
                headerView.addButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) * 180.0)
            }
            return headerView
            
        case .searchTags:
            let headerView = UINib(nibName: NIB_NAME.tagsHeader, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TagsHeader
            headerView.tag = section
            headerView.searchTextField.tag = section
            headerView.searchTextField.delegate = self
            headerView.searchButton.tag = section
            headerView.rightTagView.tag = (section*10) + 0
            headerView.middleTagView.tag = (section*10) + 1
            headerView.leftTagView.tag = (section*10) + 2
            headerView.backgroundColor = UIColor.white
            headerView.searchButton.addTarget(self, action: #selector(self.searchAction(_:)), for: .touchUpInside)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tagsHeaderTapAction(gesture:)))
            tap.numberOfTapsRequired = 1
            headerView.addGestureRecognizer(tap)
            headerView.listImageView.image = #imageLiteral(resourceName: "smallDownArrow").withRenderingMode(.alwaysTemplate)
            headerView.searchTextField.isUserInteractionEnabled = false
            //UIView.animate(withDuration: 0.3) {
                if Singleton.sharedInstance.fleetDetails.showTagsDropDown == true {
                    headerView.searchButton.isHidden = false
                    headerView.listImageView.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
                } else {
                    headerView.searchButton.isHidden = true
                    headerView.listImageView.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) * 180.0)
                }
            //}
            
            
            
            //headerView.listButtonTrailing.constant = -15
            headerView.rightTagView.isHidden = true
            headerView.middleTagView.isHidden = true
            headerView.leftTagView.isHidden = true
            let tapOnLeftTag = UITapGestureRecognizer(target: self, action: #selector(self.tagViewAction(gesture:)))
            tapOnLeftTag.numberOfTapsRequired = 1
            headerView.leftTagView.addGestureRecognizer(tapOnLeftTag)
            let tapOnMiddleTag = UITapGestureRecognizer(target: self, action: #selector(self.tagViewAction(gesture:)))
            tapOnMiddleTag.numberOfTapsRequired = 1
            headerView.middleTagView.addGestureRecognizer(tapOnMiddleTag)
            let tapOnRightTag = UITapGestureRecognizer(target: self, action: #selector(self.tagViewAction(gesture:)))
            tapOnRightTag.numberOfTapsRequired = 1
            headerView.rightTagView.addGestureRecognizer(tapOnRightTag)
            let selectedTags = Singleton.sharedInstance.fleetDetails.selectedTags
            if Singleton.sharedInstance.fleetDetails.showTagsDropDown == false {
                if (selectedTags?.count)! > 0 {
                    switch (selectedTags?.count)! {
                    case 1:
                        headerView.rightTagView.isHidden = false
                        headerView.middleTagView.isHidden = true
                        headerView.leftTagView.isHidden = true
                        headerView.rightTagLabel.text = Singleton.sharedInstance.fleetDetails.selectedTags?[0]
                    case 2:
                        headerView.rightTagView.isHidden = false
                        headerView.middleTagView.isHidden = false
                        headerView.leftTagView.isHidden = true
                        headerView.middleTagLabel.text = Singleton.sharedInstance.fleetDetails.selectedTags?[0]
                        headerView.rightTagLabel.text = Singleton.sharedInstance.fleetDetails.selectedTags?[1]
                    default:
                        headerView.rightTagView.isHidden = false
                        headerView.middleTagView.isHidden = false
                        headerView.leftTagView.isHidden = false
                        headerView.leftTagLabel.text = Singleton.sharedInstance.fleetDetails.selectedTags?[0]
                        headerView.middleTagLabel.text = Singleton.sharedInstance.fleetDetails.selectedTags?[1]
                        headerView.rightTagLabel.text = "\(TEXT.MORE)..."
                    }
                    
                }
            }
            return headerView
        default:
            break
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch self.signupModel.sections[section] {
        case .checklist:
            let footerView = UIView(frame: CGRect(x: 6, y: 0, width: SCREEN_SIZE.width, height: 1))
            let bottomLine = UIView(frame: CGRect(x: 6, y: 0, width: SCREEN_SIZE.width, height: 1))
            bottomLine.backgroundColor = COLOR.LIGHT_COLOR
            footerView.addSubview(bottomLine)
            return footerView
        case .tags:
            let footerView = UIView(frame: CGRect(x: 6, y: 0, width: SCREEN_SIZE.width, height: 1))
            let bottomLine = UIView(frame: CGRect(x: 6, y: 0, width: SCREEN_SIZE.width, height: 1))
            bottomLine.backgroundColor = COLOR.LIGHT_COLOR
            footerView.addSubview(bottomLine)
            return footerView
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch self.signupModel.sections[section] {
        case .checklist:
            guard Singleton.sharedInstance.fleetDetails.signup_template_data?[section].appSide != APP_SIDE.hidden else {
                return 0.01
            }
            return headerHeightForSection
        case .searchTags:
            return headerHeightForTags
        default:
            return 0.01
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch self.signupModel.sections[section] {
        case .checklist:
            guard Singleton.sharedInstance.fleetDetails.signup_template_data?[section].appSide != APP_SIDE.hidden else {
                return 0.01
            }
            return 1
        case .tags:
            return 1
        default:
            return 0.01
        }
    }
    
    func setChecklistDictionary(section:Int, row:Int) {
        var checklistValues = [[String:Any]]()
        for checklist in (Singleton.sharedInstance.fleetDetails.signup_template_data?[section].checklistArray)! {
            let checklistDictionary = ["check":checklist.check ?? false,"id":checklist.check_id ?? 0, "value":checklist.value ?? 0] as [String : Any]
            checklistValues.append(checklistDictionary)
        }
        Singleton.sharedInstance.fleetDetails.signup_template_data?[section].fleetData = checklistValues.jsonString
        Singleton.sharedInstance.fleetDetails.signup_template_data?[section].data = checklistValues.jsonString
    }
    
    func checklistHeaderTapAction(gesture:UITapGestureRecognizer) {

        guard Singleton.sharedInstance.fleetDetails.signup_template_data![(gesture.view?.tag)!].checklistDictionary.count != 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_DATA_FOUND, isError: .error)
            return
        }
        Singleton.sharedInstance.fleetDetails.signup_template_data?[(gesture.view?.tag)!].showChecklistRows = !(Singleton.sharedInstance.fleetDetails.signup_template_data?[(gesture.view?.tag)!].showChecklistRows)!
        if Singleton.sharedInstance.fleetDetails.signup_template_data![(gesture.view?.tag)!].showChecklistRows == true {
            var rows = [IndexPath]()
            for i in 0..<Singleton.sharedInstance.fleetDetails.signup_template_data![(gesture.view?.tag)!].checklistDictionary.count {
                rows.append(IndexPath(row: i, section: (gesture.view?.tag)!))
            }
            self.signupTable.insertRows(at: rows, with: .bottom)
        } else {
            var rows = [IndexPath]()
            for i in 0..<Singleton.sharedInstance.fleetDetails.signup_template_data![(gesture.view?.tag)!].checklistDictionary.count {
                rows.append(IndexPath(row: i, section: (gesture.view?.tag)!))
            }
            self.signupTable.deleteRows(at: rows, with: .top)
        }
        if let headerView = self.signupTable.viewWithTag( (gesture.view?.tag)!) as? DetailHeaderView {
            UIView.animate(withDuration: 0.3) {
                if Singleton.sharedInstance.fleetDetails.signup_template_data![(gesture.view?.tag)!].showChecklistRows == true {
                    headerView.addButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
                } else {
                    headerView.addButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) * 180.0)
                }
            }
        }
    }
    
    func tagsHeaderTapAction(gesture:UITapGestureRecognizer) {
        Singleton.sharedInstance.fleetDetails.showTagsDropDown = !(Singleton.sharedInstance.fleetDetails.showTagsDropDown!)
        self.isSearching = false
        self.signupTable.reloadSections([self.signupModel.sections.index(of: .searchTags)!,self.signupModel.sections.index(of: .tags)!], with: .top)
        //self.signupTable.scrollRectToVisible(self.signupTable.rect(forSection: self.signupModel.sections.index(of: .searchTags)!), animated: true)
        //self.signupTable.reloadSections(IndexSet(integer: self.signupModel.sections.index(of: .tags)!), with: .none)
    }
    
    func searchAction(_ sender: UIButton) {
        if let headerView = self.signupTable.viewWithTag(sender.tag) as? TagsHeader {
            headerView.searchTextField.isUserInteractionEnabled = true
            headerView.searchTextField.attributedPlaceholder = NSAttributedString(string: TEXT.TAGS, attributes: [NSForegroundColorAttributeName : COLOR.LIGHT_COLOR])
            headerView.searchTextField.becomeFirstResponder()
            if self.isSearching == true {
                //headerView.searchTextField.isUserInteractionEnabled = true
                //headerView.searchTextField.becomeFirstResponder()
                self.signupTable.scrollToRow(at: IndexPath(row: 0, section: self.signupModel.sections.index(of: .searchTags)!), at: .top, animated: true)
                //headerView.searchTextField.attributedPlaceholder = NSAttributedString(string: TEXT.TAGS, attributes: [NSForegroundColorAttributeName : COLOR.LIGHT_COLOR])
            } else {
                
                
            }
        }
    }
    
    func tagViewAction(gesture:UITapGestureRecognizer)  {
        //if let headerView = self.signupTable.viewWithTag(((gesture.view?.tag)! / 10)) as? TagsHeader {
            switch (gesture.view?.tag)! % 10 {
            case 0:
                if (Singleton.sharedInstance.fleetDetails.selectedTags?.count)! > 2{
                    return
                } else {
                    Singleton.sharedInstance.fleetDetails.selectedTags?.removeLast()
                }
            case 1:
                if (Singleton.sharedInstance.fleetDetails.selectedTags?.count)! == 2 {
                    Singleton.sharedInstance.fleetDetails.selectedTags?.removeFirst()
                } else {
                    Singleton.sharedInstance.fleetDetails.selectedTags?.remove(at: 1)
                }
            default:
                Singleton.sharedInstance.fleetDetails.selectedTags?.removeFirst()
            }
            self.signupTable.reloadSections(IndexSet(integer: ((gesture.view?.tag)! / 10)), with: .none)
            //}
        
    }
}

extension SignupTemplateController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedText = textField.text!
        var updatedString:NSString = "\(updatedText)" as NSString
        updatedString = updatedString.replacingCharacters(in: range, with: string) as NSString
//        if updatedString == "\n"{
//            self.view.endEditing(true)
//            return false
//        }
        
        if updatedString == "" {
            self.isSearching = false
        } else {
            self.isSearching = true
            self.filteredData = (Singleton.sharedInstance.fleetDetails.tagsArray?.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: updatedString as String, options: .caseInsensitive)
                return range.location != NSNotFound
            }))!
        }
//        var index = [IndexPath]()
//        for i in 0..<self.filteredData.count {
//            index.append(IndexPath(row: i, section: textField.tag))
//        }
        
        self.signupTable.reloadSections(IndexSet(integer: self.signupModel.sections.index(of: .tags)!), with: .none)
       // DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            if let headerView = self.signupTable.viewWithTag(self.signupModel.sections.index(of: .tags)!) as? TagsHeader {
//                headerView.searchTextField.isUserInteractionEnabled = true
//                headerView.searchTextField.becomeFirstResponder()
//            }
        //}
        
        //self.signupTable.reloadRows(at: index, with: .automatic)
        return true
    }
}

//MARK: SCANNER
extension SignupTemplateController:ScannerDelegate {
    func scannerAction(index: Int) {
        self.view.endEditing(true)
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
            self.startScanning()
        } else {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                DispatchQueue.main.async {
                    if granted == true {
                        self.startScanning()
                    } else {
                        Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.REJECTED_CAMERA_SUPPORT, isError: .error)
                    }
                }
            })
        }
    }
    
    func startScanning() {
        /*------------- Firebase ---------------*/
        Analytics.logEvent(ANALYTICS_KEY.SIGNUP_BARCODE_SCAN, parameters: [:])
        Answers.logCustomEvent(withName: ANALYTICS_KEY.SIGNUP_BARCODE_SCAN, customAttributes: [:])
        guard self.scanView == nil else {
            return
        }
        scanView = ScanView(frame: self.view.frame)
        scanView.delegate = self
        self.view.addSubview(scanView)
        self.view.endEditing(true)
        scanView.setScanView()
    }
    
    //MARK: Scanner Delegate
    func decodedOutput(_ value: String) {
        if let section = self.signupModel.sections.index(of: .customBarcode) {
            let indexPath = IndexPath(row: 0, section: section)
            if let cell = self.signupTable.cellForRow(at: indexPath) as? CustomBarCodeCell {
                cell.descriptionView.text = value
                Singleton.sharedInstance.fleetDetails.signup_template_data![self.signupModel.sections.index(of: .customBarcode)!].data = value
                Singleton.sharedInstance.fleetDetails.signup_template_data![self.signupModel.sections.index(of: .customBarcode)!].fleetData = value
                if value.isEmpty == false {
                    cell.placeholderLabel.isHidden = true
                    cell.actionButton.isSelected = false
                } else {
                    cell.placeholderLabel.isHidden = false
                    cell.actionButton.isSelected = true
                }
            }
        }
    }
    
    func dismissScannerView() {
        guard scanView != nil else {
            return
        }
        self.scanView.removeFromSuperview()
        self.scanView = nil
    }
}

extension SignupTemplateController:UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            if let cell = self.signupTable.cellForRow(at: IndexPath(row: 0, section: self.signupModel.sections.index(of: DETAIL_SECTION.customBarcode)!)) as? CustomBarCodeCell{
                cell.actionButton.isSelected = true
            }
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        UIView.setAnimationsEnabled(false) // Disable animations
        self.signupTable.beginUpdates()
        self.signupTable.endUpdates()
        UIView.setAnimationsEnabled(true)
        self.decodedOutput(textView.text)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if let section = self.signupModel.sections.index(of: .customBarcode) {
            let indexPath = IndexPath(row: 0, section: section)
            if let cell = self.signupTable.cellForRow(at: indexPath) as? CustomBarCodeCell {
                cell.actionButton.isSelected = true
            }
        }
    }
    
}

extension SignupTemplateController:SignupTemplateDelegate {
    
    func customFieldShouldBeginEditing(textView: UITextView) -> Bool {
        textView.inputAccessoryView = keyboardToolbar
        keyboardToolbar.currentTextField = textView
        switch self.signupModel.sections[textView.tag] {
        case .date, .datePast, .dateFuture, .dateTime, .dateTimePast, .dateTimeFuture:
            self.showCustomDatePicker(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![textView.tag], tag: textView.tag)
            return false
        case .email,.number, .text,.telephone,.url:
            return true
        case .dropdown:
            self.showDropdown(customField: Singleton.sharedInstance.fleetDetails.signup_template_data![textView.tag], tag: textView.tag)
            return false
        case .checkbox:
            return false
        case .teams:
            self.showTeams(tag: textView.tag)
            return false
        default:
            return false
        }
    }
    
    func customFieldShouldReturn(textView: UITextView) -> Bool {
        return true
    }
    
    func customFieldDidEndEditing(textView: UITextView) {
        let text = textView.text!
        Singleton.sharedInstance.fleetDetails.signup_template_data?[textView.tag].fleetData = text
    }
    
    func updateTableHeight(descriptionText: String) {
        UIView.setAnimationsEnabled(false) // Disable animations
        self.signupTable.beginUpdates()
        self.signupTable.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
}

extension SignupTemplateController:KeyboardDelegate {
    func nextFromKeyboard(_ nextField: AnyObject) {
        let textField = nextField as! UITextView
        guard textField.tag + 1 < (Singleton.sharedInstance.fleetDetails.signup_template_data?.count)! else {
            self.view.endEditing(true)
            return
        }
        self.signupTable.scrollToRow(at: IndexPath(row: 0, section: textField.tag + 1), at: .middle, animated: true)
        if Singleton.sharedInstance.fleetDetails.signup_template_data?[textField.tag + 1].appSide == APP_SIDE.write {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                if let cell = self.signupTable.cellForRow(at: IndexPath(row: 0, section: textField.tag + 1)) as? SignupTemplateCell {
                    cell.detailTextView.becomeFirstResponder()
                } else if let cell = self.signupTable.cellForRow(at: IndexPath(row: 0, section: textField.tag + 1)) as? CustomBarCodeCell{
                    cell.descriptionView.becomeFirstResponder()
                }
            })
        } else {
            self.view.endEditing(true)
        }
    }
    
    func prevFromKeyboard(_ prevField: AnyObject) {
        let textField = prevField as! UITextView
        guard textField.tag - 1 >= 0 else {
            self.view.endEditing(true)
            return
        }
        self.signupTable.scrollToRow(at: IndexPath(row: 0, section: textField.tag - 1), at: .middle, animated: true)
        if Singleton.sharedInstance.fleetDetails.signup_template_data?[textField.tag - 1].appSide == APP_SIDE.write {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                if let cell = self.signupTable.cellForRow(at: IndexPath(row: 0, section: textField.tag - 1)) as? SignupTemplateCell {
                    cell.detailTextView.becomeFirstResponder()
                }
            })
        } else {
            self.view.endEditing(true)
        }
    }
    
    func doneFromKeyboard() {
        self.view.endEditing(true)
    }
    
    func setImage(image:UIImage, tag:Int) {
        if IJReachability.isConnectedToNetwork() == true {
            let currentDateTime = Date().getDateStringWithUnderscore
            let imagePath = Auxillary.saveImageToDocumentDirectory("\(Singleton.sharedInstance.fleetDetails.signup_template_data![tag].label)_\(currentDateTime).png", image: image)
            Singleton.sharedInstance.fleetDetails.signup_template_data![tag].imageArray.add(imagePath)
            let caption = Caption(json: [:])
            Singleton.sharedInstance.fleetDetails.signup_template_data![tag].captionArray?.append(caption)
            UIView.setAnimationsEnabled(false)
            self.signupTable.reloadSections(IndexSet(integer: tag), with: .none)
            UIView.setAnimationsEnabled(true)
            Singleton.sharedInstance.fleetDetails.signup_template_data![tag].fleetData = "Uploading"
            self.sendRequestToUploadCustomFieldImage(tag, imagePath: imagePath, image: image, index: Singleton.sharedInstance.fleetDetails.signup_template_data![tag].imageArray.count - 1)
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        }
    }
    func sendRequestToUploadCustomFieldImage(_ tag:Int,imagePath:String, image:UIImage, index:Int) {
        NetworkingHelper.sharedInstance.uploadImageToServer(image, imagePath: imagePath, tag: tag) { (isSucceeded, response) in
            DispatchQueue.main.async {
                print(response)
                if isSucceeded == true {
                    if let responseData = response["responseData"] as? [String:Any] {
                        if let status = responseData["status"] as? Int {
                            switch status {
                            case STATUS_CODES.SHOW_DATA:
                                if let data = responseData["data"] as? [String:Any] {
                                    if let ref_image = data["ref_image"] as? String {
                                        guard index < Singleton.sharedInstance.fleetDetails.signup_template_data![tag].imageArray.count else {
                                            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.SOMETHING_WRONG, isError: .error)
                                            return
                                        }
                                        if let storedImagePath = response["imagePath"] as? String {
                                            if let storedTag = response["tag"] as? Int {
                                                if let storedIndex = self.getImageIndexFromImageName(storedImagePath, imageArray: Singleton.sharedInstance.fleetDetails.signup_template_data![storedTag].imageArray) {
                                                    Singleton.sharedInstance.fleetDetails.signup_template_data![storedTag].fleetData = "ImageAdded"
                                                    Singleton.sharedInstance.fleetDetails.signup_template_data![storedTag].imageArray.replaceObject(at: storedIndex, with: ref_image)
                                                    NetworkingHelper.sharedInstance.allImagesCache.setObject(image, forKey: "\(ref_image)" as NSString)
                                                    //self.reloadImageSection()
                                                    if let cell = self.signupTable.cellForRow(at: IndexPath(row: 0, section: storedTag)) as? SignupImageCell {
                                                        cell.imageCollectionView.reloadData()
                                                        Auxillary.deleteImageFromDocumentDirectory(imagePath)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                break
                            default:
                                Singleton.sharedInstance.showErrorMessage(error: responseData["message"] as! String, isError: .error)
                            }
                        }
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                    if let storedImagePath = response["imagePath"] as? String {
                        if let storedTag = response["tag"] as? Int {
                            if let storedIndex = self.getImageIndexFromImageName(storedImagePath, imageArray: Singleton.sharedInstance.fleetDetails.signup_template_data![storedTag].imageArray) {
                                Singleton.sharedInstance.fleetDetails.signup_template_data?[storedTag].imageArray.removeObject(at: storedIndex)
                                Singleton.sharedInstance.fleetDetails.signup_template_data![storedTag].fleetData = ""
                                if let _ = self.signupTable.cellForRow(at: IndexPath(row: 0, section: storedTag)) as? SignupImageCell {
                                    self.signupTable.reloadSections(IndexSet(integer: storedTag), with: .none)
                                    Auxillary.deleteImageFromDocumentDirectory(imagePath)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getImageIndexFromImageName(_ imageName:String, imageArray:NSMutableArray) -> Int! {
        for i in (0..<imageArray.count) {
            if(imageArray.object(at: i) as! String == imageName) {
                return i
            }
        }
        return nil
    }
}

extension SignupTemplateController:SignupImageDelegate {
    func deleteImage(section: Int, row: Int) {
        
        guard row < Singleton.sharedInstance.fleetDetails.signup_template_data![section].imageArray.count else {
            return
        }
 
        if(Singleton.sharedInstance.fleetDetails.signup_template_data![section].fleetData == "Uploading") {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_UPLOADING, isError: .message)
        } else if(Singleton.sharedInstance.fleetDetails.signup_template_data![section].fleetData == "Deleting") {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.IMAGE_DELETING, isError: .message)
        } else if((Singleton.sharedInstance.fleetDetails.signup_template_data![section].imageArray.object(at: row) as AnyObject).isKind(of: UIImage.classForCoder()) == true) {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.PLEASE_TRY_AGAIN, isError: .error)
        }else {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            let deleteAction = UIAlertAction(title: TEXT.DELETE, style: UIAlertActionStyle.destructive){
                UIAlertAction in
                DispatchQueue.main.async(execute: { () -> Void in
                    if self.imagesPreview != nil {
                        self.imagesPreview.confirmationDoneForDeletion()
                    }
                    Singleton.sharedInstance.fleetDetails.signup_template_data![section].fleetData = ""
                    Singleton.sharedInstance.fleetDetails.signup_template_data![section].imageArray.removeObject(at: row)
                    if row < (Singleton.sharedInstance.fleetDetails.signup_template_data![section].captionArray?.count)! {
                        Singleton.sharedInstance.fleetDetails.signup_template_data![section].captionArray?.remove(at: row)
                    }
                    Singleton.sharedInstance.fleetDetails.signup_template_data![section].fleetData = ""
                    UIView.setAnimationsEnabled(false)
                    self.signupTable.reloadSections(IndexSet(integer: section), with: .none)
                    UIView.setAnimationsEnabled(true)
                })
            }
            let cancelAction = UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.cancel){
                UIAlertAction in
            }
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func imageTapped(imageType: ImageType, index: Int, collectionTag: Int) {
        self.showImagesPreview(index, collectionTag: collectionTag, imageType: imageType)
    }
    
}
extension SignupTemplateController:ImagePreviewDelegate {
    func showImagesPreview(_ indexItem:Int, collectionTag:Int, imageType:ImageType) {
        self.view.endEditing(true)
        if(imagesPreview == nil) {
            imagesPreview = UINib(nibName: NIB_NAME.imagesPreview, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ImagesPreview
            imagesPreview.frame = UIScreen.main.bounds
            imagesPreview.imageCollectionView.frame = CGRect(x: imagesPreview.imageCollectionView.frame.origin.x, y: imagesPreview.imageCollectionView.frame.origin.y, width: self.view.frame.width, height: imagesPreview.imageCollectionView.frame.height)
            
            imagesPreview.delegate = self
            switch imageType {
            case .customFieldImage, .signupImage:
                imagesPreview.imageArray = NSMutableArray(array: Singleton.sharedInstance.fleetDetails.signup_template_data![collectionTag].imageArray)
                var captionArray = [Caption]()
                for _ in 0..<Singleton.sharedInstance.fleetDetails.signup_template_data![collectionTag].imageArray.count{
                    let caption = Caption(json: [:])
                    caption.image_url = ""
                    caption.caption_data = ""
                    captionArray.append(caption)
                }
                Singleton.sharedInstance.fleetDetails.signup_template_data![collectionTag].captionArray = captionArray
                imagesPreview.captionArray = captionArray
                imagesPreview.imageType = ImageType.signupImage.hashValue
                imagesPreview.collectionViewTag = collectionTag
            default:
                break
            }
            
            imagesPreview.backgroundColor = UIColor.clear
            self.imagesPreview.setCollectionViewWithPage(indexItem)
            self.view.addSubview(imagesPreview)
            
            imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                self.imagesPreview.transform = CGAffineTransform.identity
            }, completion: { finished in
                self.imagesPreview.backgroundColor = COLOR.imagePreviewBackgroundColor
            })
        }
        
        if fromProfileVC == true{
            imagesPreview.deleteButton.isHidden = true
        }
    }
    
    func dismissImagePreview() {
        imagesPreview.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: { finished in
            self.imagesPreview.removeFromSuperview()
            self.imagesPreview = nil
        })
    }
    
    func deleteImageActionFromImages(_ index:Int) {
    }
    
    func deleteImageActionFromCustomField(_ indexItem: Int, collectionViewIndex: Int) {
        self.deleteImage(section: collectionViewIndex, row: indexItem)
    }
}

//MARK: CustomDatePicker Delegate Methods
extension SignupTemplateController:CustomPickerDelegate {
    func dismissDatePicker() {
        self.updateValueForDatePicker("",customPickerTag: customPicker.tag)
    }
    
    func selectedDateFromPicker(_ selectedDate: String) {
        self.view.endEditing(true)
        self.updateValueForDatePicker(selectedDate, customPickerTag: customPicker.tag)
    }
    
    func updateValueForDatePicker(_ updatedDate:String, customPickerTag:Int) {
        if IJReachability.isConnectedToNetwork() == false {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        } else {
            //Singleton.sharedInstance.fleetDetails.signup_template_data![customPicker.tag].data = updatedDate
            Singleton.sharedInstance.fleetDetails.signup_template_data![customPicker.tag].fleetData = updatedDate
            UIView.setAnimationsEnabled(false) // Disable animations
            self.signupTable.reloadSections(IndexSet(integer:customPickerTag), with: .none)
            UIView.setAnimationsEnabled(true)
        }
        customPicker = nil
    }
}

//MARK: TemplateControllerDelegate Methods
extension SignupTemplateController:TemplateControllerDelegate {
    func selectedValue(value: String, tag:Int, isDirectDismiss: Bool) {
        if isDirectDismiss == false {
            self.updateValue(value, tagValue: self.drowDownWithSearch.view.tag)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.drowDownWithSearch = nil
    }
    
    func updateValue(_ updatedValue:String, tagValue:Int) {
        if IJReachability.isConnectedToNetwork() == false && Auxillary.isAppSyncingEnable() == false {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        } else {
            //if Singleton.sharedInstance.fleetDetails.teams.count > 0 {
                if self.signupModel.sections.index(of: .teams) == tagValue {
                    Singleton.sharedInstance.fleetDetails.teams?.teamName = updatedValue
                } else {
                    Singleton.sharedInstance.fleetDetails.signup_template_data![tagValue].fleetData = updatedValue
                }
//            } else {
//                let team = AssignedTeamDetails(json: [:])
//                Singleton.sharedInstance.fleetDetails.teams.append(team)
//                Singleton.sharedInstance.fleetDetails.teams[0].teamName = updatedValue
//            }
            UIView.setAnimationsEnabled(false)
            self.signupTable.reloadSections(IndexSet(integer:tagValue), with: .none)
            UIView.setAnimationsEnabled(true)
        }
    }
}
