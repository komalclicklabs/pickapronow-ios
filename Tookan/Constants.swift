//
//  Constants.swift
//  Tookan
//
//  Created by Click Labs on 7/7/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import UIKit

// MARK : Networking Constants
let APIKeyForGoogleMaps = "AIzaSyDqZn_rsqd_ZMQUITSKB5FGerbn6DwtdLg"//"AIzaSyDX32ar9JhpTYR1W3vGAAjzQTGeOx7MWm4"//"AIzaSyBFXsZ_biHbFbNkY05VRZ59YnhS6FWSYyg"
//let FLURRY_KEY = "B4PWSZNNMMQ2ZXSQXK7J"
//MARK: UDP
let IP_ADDRESS = "test.tookanapp.com"
let PORT = 8015
let TAG_LINE = "Empowering fieldforce"
let CONTACT_NUMBER = "123456789"
let CONTACT_ID = "contact@tookanapp.com"
let TERM_CONDITIONS_LINK = "http://google.com"
let app_version = "319"
var DEVICE_TYPE = "3"// 3 for app store, 1 for enterprise build
let SHOW_TUTORIAL = 1
let SHOW_SIGNUP = 0 //0 for AppStore
let SCREEN_SIZE = UIScreen.main.bounds
let VERSION = "v2"
let APP_NAME = "TOOKAN"
let MENU_LOGO = 1
let TAXI_TEMPLATE = "taxi-template"
let FESTIVE_SPLASH = "splash"
let FESTIVE_IN_APP_LOGO = "in_app_logo"
let SPLASH_IMAGE = "splash"
let LOGIN_BACKGROUND_IMAGE = "splash"



struct APP_STORE {
    static let value = 1
}

struct FESTIVE_SEASON {
    static let DIWALI = "2016-10-30"
    static let HALLOWEEN = "2016-10-31"
    static let FESTIVE = ""
}

func getAspectRatioValue(value:CGFloat) -> CGFloat {
    return (value / 375) * SCREEN_SIZE.width
}

struct FONT_SIZE {
    static let regular:CGFloat = getAspectRatioValue(value: 16.0) //16.0
    static let large:CGFloat = getAspectRatioValue(value: 32.0)//32.0
    static let timer:CGFloat = getAspectRatioValue(value: 18.0)//18.0
    static let twenty:CGFloat = getAspectRatioValue(value: 20.0)
    static let medium:CGFloat = getAspectRatioValue(value: 14.0)//14.0
    static let small:CGFloat = getAspectRatioValue(value: 12.0)//11.0
    static let extraSmall:CGFloat = getAspectRatioValue(value: 10.0)
    static let extra_large:CGFloat = getAspectRatioValue(value: 36.0)//36.0
    static let tutorial:CGFloat = getAspectRatioValue(value: 24.0) //24.0
}

struct CONTROLLER {
    static let taskList = 1
    static let mapView = 2
}

struct USER_TYPE {
    static let agent = 0
    static let admin = 1
}

struct FONT {
    
    //static let regular = "Graphik-Regular"
    //static let light = "Graphik-Light"
    //static let medium = "Graphik-Medium"
}

enum ImageType {
    case image
    case customFieldImage
    case createTaskImage
    case taskDescription
    case signatureImage
    case signupImage
}

enum ERROR_TYPE {
    case error
    case success
    case message
}

struct DatabaseImageType {
    static let image = 1
    static let signature = 2
    static let customField = 3
}

struct SECTION_NAME {
    static let customer = 0
    static let taskDetails = 1
    static let note = 2
    static let signature = 3
    static let image = 4
    static let invoice = 5
    static let scanner = 6
    static let customField = 7
}

enum DETAIL_SECTION {
    case jobDetails
    case notes
    case signature
    case image
    case barcode
    case invoice
    case dropdown
    case text
    case number
    case customImage
    case date
    case checkbox
    case telephone
    case email
    case url
    case checklist
    case dateFuture
    case datePast
    case dateTime
    case dateTimeFuture
    case dateTimePast
    case table
    case customBarcode
    case earnings
    case teams
    case tags
    case searchTags
}

enum ADDED_DETAILS_TYPE:String {
    case text_added = "text_added"
    case text_updated = "text_updated"
    case signature_image_added = "signature_image_added"
    case image_added = "image_added"
    case barcode_added = "barcode_added"
    case barcode_updated = "barcode_updated"
    case text_deleted = "text_deleted"
    case barcode_deleted = "barcode_deleted"
    case custom_image_added = "custom_image_added"
    case custom_image_deleted = "custom_image_deleted"
}

enum DETAIL_ROWS {
    case job
    case user
    case location
    case description
    case destinationLocation
}


struct COLOR {
    static let TRANS_COLOR = UIColor(red: 0, green: 0.0, blue: 0.0, alpha: 0.5)
    static let imagePreviewBackgroundColor = UIColor(red: 0, green: 0.0, blue: 0.0, alpha: 0.8)
    static var lowerNavigationBackgroundColor = UIColor(red: 20/255, green: 49/255, blue: 66/255, alpha: 1.0)
    static let alertPositiveColor = UIColor(red: 99/255, green: 174/255, blue: 12/255, alpha: 1.0)
    static let alertNegativeColor = UIColor(red: 214/255, green: 78/255, blue: 71/255, alpha: 1.0)
    static let cancelButtonColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)//TaskTableButton Color
    
    static let halloweenNavigationBackgroundColor = UIColor(red: 27/255, green: 12/255, blue: 38/255, alpha: 1.0)
    static let halloweenLowerBarColor = UIColor(red: 21/255, green: 5/255, blue: 32/255, alpha: 1.0)
    static let diwaliNavigationColor = UIColor(red: 224/255, green: 64/255, blue: 94/255, alpha: 1.0)
    static let diwaliLowerBarColor = UIColor(red: 193/255, green: 37/255, blue: 70/255, alpha: 1.0)
    static var navigationBackgroundColorForFestive = UIColor(red: 12/255, green: 87/255, blue: 123/255, alpha: 1.0) //UIColor(red: 9/255, green: 41/255, blue: 58/255, alpha: 1.0)
    static var lowerNavigationBackgroundColorForFestive = UIColor(red: 20/255, green: 49/255, blue: 66/255, alpha: 1.0)
    static let ERROR_COLOR = UIColor(red: 226/255, green: 87/255, blue: 76/255, alpha: 1.0)
    static let SUCCESS_COLOR = UIColor(red: 102/255, green: 208/255, blue: 42/255, alpha: 1.0)
    static let MESSAGE_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.9)
    static let UNASSIGNED_COLOR = UIColor(red: 252/255, green: 131/255, blue: 68/255, alpha: 1)
    static let ASSIGNED_COLOR = UIColor(red: 252/255, green: 131/255, blue: 68/255, alpha: 1)
    static let STARTED_COLOR = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
    static let ARRIVED_COLOR = UIColor(red: 63/255, green: 81/255, blue: 181/255, alpha: 1)
    static let SUCCESSFUL_COLOR = UIColor(red: 44/255, green: 159/255, blue: 44/255, alpha: 1)
    static let FAILED_COLOR = UIColor(red: 229/255, green: 57/255, blue: 53/255, alpha: 1)
    static let CANCELED_COLOR = UIColor(red: 229/255, green: 57/255, blue: 53/255, alpha: 1)
    static let ACCEPTED_COLOR = UIColor(red: 186/255, green: 104/255, blue: 200/255, alpha: 1)
    static let DECLINED_COLOR = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
    
   
    static var navigationBackgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    static let TEXT_COLOR = UIColor.black
    static let LITTLE_LIGHT_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
    static let LIGHT_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    static let EXTRA_LIGHT_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    static let themeBackgroundColor = UIColor.white
    static let negativeButtonBackgroundColor = UIColor.white
    static let negativeButtonTitleColor = UIColor.black
    static let positiveButtonTitleColor = UIColor.white
    static let DECLINE_TITLE_COLOR = UIColor(red: 229/255, green: 19/255, blue: 19/255, alpha: 1.0)
    static let ACCEPT_BACKGROUND_COLOR = UIColor(red: 15/255, green: 189/255, blue: 52/255, alpha: 1.0)
    static let DECLINE_BACKGROUND_COLOR = UIColor.white
    static let ACCEPT_TITLE_COLOR = UIColor.white
    static let LINE_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
    static let CALL_BUTTON_COLOR = UIColor(red: 15/255, green: 189/255, blue: 52/255, alpha: 1.0)
    static let TABLE_HEADER_COLOR = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
    static let REQUIRED_COLOR = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
    static let HEADER_LABEL_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    static let SIGNATURE_COLOR =  UIColor(red: 35/255, green: 35/255, blue: 35/255, alpha: 1.0)
    static let MENU_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
    static let LOGIN_LINE_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15)
    static let VEHICLE_BUTTON_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
    static let VEHICLE_CELL_SELECTION_COLOR = UIColor(red: 108/255, green: 185/255, blue: 85/255, alpha: 1.0)
    static let PROFILE_LINE_ERROR_COLOR = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
    static let PROFILE_LINE_COLOR = UIColor(red: 5/255, green: 179/255, blue: 70/255, alpha: 1.0)
    static let TABLE_BACKGROUND_COLOR = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
    static let FILTER_CLEAR_COLOR = UIColor(red: 236/255, green: 3/255, blue: 3/255, alpha: 1)
    static let CREATE_TASK_BORDER_COLOR = UIColor(red: 36/255, green: 36/255, blue: 36/255, alpha: 0.1)
    static let mapStrokeColor = UIColor(red: 70/255, green: 149/255, blue: 246/255, alpha: 1.0)
    static let NEW_USER_COLOR = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1.0)
    static let CELL_HIGHLIGHT_COLOR = #colorLiteral(red: 0.2745098039, green: 0.5843137255, blue: 1, alpha: 0.1049604024)
    static let SUCCESSFUL_VERIFICATION_COLOR = #colorLiteral(red: 0.4235294118, green: 0.7764705882, blue: 0.3019607843, alpha: 1)
    static let FAILED_VERIFICATION_COLOR = #colorLiteral(red: 0.8196078431, green: 0.1960784314, blue: 0.2078431373, alpha: 1)
    static let STAR_COLOR = #colorLiteral(red: 0.9725490196, green: 0.9058823529, blue: 0.1098039216, alpha: 1)
    static let availabilityHeaderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.9)
    static let availabilityBusyColor = #colorLiteral(red: 0.831372549, green: 0.3843137255, blue: 0.5411764706, alpha: 1)
    static let availabilityFreeColor = #colorLiteral(red: 0.4862745098, green: 0.8196078431, blue: 0.3882352941, alpha: 1)
    static let availabilityUnavailableColor = #colorLiteral(red: 0.3254901961, green: 0.3254901961, blue: 0.3254901961, alpha: 1)
    static let availabilityHighlightedFreeColor = UIColor(red: 122/255, green: 177/255, blue: 82/255, alpha: 1.0)
    static let availabilityHighlightedUnavailableColor = UIColor(red: 197/255, green: 197/255, blue: 197/255, alpha: 1.0)
    static let warningColor = UIColor(red: 212/255, green: 52/255, blue: 52/255, alpha: 1.0)
    /*=============== WhiteLabelColor ================*/
    static let themeForegroundColor = UIColor(red: 70/255, green: 149/255, blue: 246/255, alpha: 1.0)
    
}

//MARK: IMAGES
struct IMAGE {
    static let placeholderImage = UIImage(named: "imagePlaceholder")!
}

//MARK: STORYBOARD
struct STORYBOARD_ID {
    static let taxiController = "TaxiController"
    static let homeNavigation = "HomeNavigation"
    static let homeBaseController = "HomeBaseController"
    static let newTasksController = "NewTasksController"
    static let detailController = "DetailController"
    static let splashController = "SplashViewController"
    static let relatedTaskController = "RelatedTaskController"
    static let loginNavigation = "LoginNavigation"
    static let allNotificationController = "AllNotificationsController"
    static let signinViewController = "SigninViewController"
    static let forgotPasswordViewController = "ForgotPasswordViewController"
    static let settingViewController = "SettingViewController"
    static let profileViewController = "ProfileViewController"
    static let calendarViewController = "calendarViewController"
    static let filterViewController = "FilterViewController"
    static let tutorialController = "TutorialController"
    static let invoiceController = "InvoiceController"
    static let createNewOrder = "CreateNewOrder"
    static let addDeliveryPoint = "AddDeliveryPoint"
    static let customServerController = "CustomServerController"
    static let signupViewController = "SignupViewController"
    static let otpController = "OTPController"
    static let verificationStateController = "VerificationStateController"
    static let signupTemplateController = "SignupTemplateController"
    static let earningController = "EarningController"
    static let earningDetailController = "EarningDetailController"
    static let completedTaskEarningController = "CompletedTaskEarningController"
    static let editScheduleController = "EditScheduleController"
}

//MARK: STORYBOARD
struct STORYBOARD_NAME {
    static let main = "Main"
    static let home = "Home"
}

//MARK: NIB NAME
struct NIB_NAME {
    static let errorView = "ErrorView"
    static let loaderView = "LoaderView"
    static let jobListTable = "JobListTable"
    static let jobListCell = "JobListCell"
    static let navigationBar = "navigationBar"
    static let noInternetConnectionView = "NoInternetConnectionView"
    static let newTaskCell = "NewTaskCell"
    static let timerButton = "TimerButton"
    static let jobDetailView = "JobDetailView"
    static let jobDetailCell = "JobDetailCell"
    static let userDetailCell = "UserDetailCell"
    static let jobDescriptionCell = "JobDescriptionCell"
    static let imageCollectionCell = "ImageCollectionCell"
    static let detailHeaderView = "DetailHeaderView"
    static let notesCell = "NotesCell"
    static let signatureView = "SignatureView"
    static let imageTableCell = "ImageTableCell"
    static let imagesPreview = "ImagesPreview"
    static let customFieldsCell = "CustomFieldsCell"
    static let customDatePicker = "CustomDatePicker"
    static let customDropdown = "CustomDropdown"
    static let menuList = "MenuList"
    static let menuListCell = "MenuListCell"
    static let pushBanner = "PushBanner"
    static let todayButton = "TodayButton"
    static let calendarCollectionViewCell = "CalendarCollectionViewCell"
    static let loadingTaskView = "LoadingTaskView"
    static let settingViewCell = "SettingViewCell"
    static let settingDropDown = "SettingDropDown"
    static let dropDownCell = "DropDownCell"
    static let VehicleCollectionViewCell = "VehicleCollectionViewCell"
    static let VehicleViewController = "VehicleViewController"
    static let profileViewCell = "ProfileViewCell"
    static let filterSectionHeader = "FilterSectionHeader"
    static let filterViewCell = "FilterViewCell"
    static let keyWordSectionHeader = "KeyWordSectionHeader"
    static let customSlider = "CustomSlider"
    static let checklistCell = "ChecklistCell"
    static let addAnotherPickupAndDelivery = "AddAnotherPickupAndDelivery"
    static let createTaskCell = "CreateTaskCell"
    static let taxiDetailView = "TaxiDetailView"
    static let signupCell = "SignupCell"
    static let signupFooterView = "SignupFooterView"
    static let userStateView = "UserStateView"
    static let signupTemplateCell = "SignupTemplateCell"
    static let signupImageCell = "SignupImageCell"
    static let profileHeaderView = "ProfileHeaderView"
    static let earningCell = "EarningCell"
    static let earningHeaderView = "EarningHeaderView"
    static let earningDetailCell = "EarningDetailCell"
    static let weekEarningDetailCell = "WeekEarningDetailCell"
    static let barGraphCell = "BarGraphCell"
    static let rupeesScaleCollectionCell = "rupeesScaleCollectionCell"
    static let templateController = "TemplateController"
    static let templateTableViewCell = "TemplateTableViewCell"
    static let pathSummaryView = "PathSummaryView"
    static let dayCollectionCell = "DayCollectionCell"
    static let timeCollectionCell = "TimeCollectionCell"
    static let availbilityCollectionCell = "availbilityCollectionCell"
    static let availabilityDotCell = "AvailabilityDotCell"
    static let availabilityView = "AvailabilityView"
    static let reloadView = "ReloadView"
    static let unavailableMarkView = "UnavailableMarkView"
    static let arrowBottomView = "ArrowBottomView"
    static let saveCancelButton = "SaveCancelButton"
    static let copyRepeatView = "CopyRepeatView"
    static let copyRepeatCell = "CopyRepeatCell"
    static let customBarCodeCell = "CustomBarCodeCell"
    static let imageCaptionViewController = "ImageCaptionViewController"
    static let tagsHeader = "TagsHeader"
    static let syncView = "SyncView"
    static let topWarningView = "TopWarningView"
}

struct USER_DEFAULT {
    static let selectedServer = "selectedServer"
    static let accessToken = "accessToken"
    static let layoutType = "layoutType"
    static let selectedLocale = "locale"
    //static let isCreateTaskAvailable = "createTask"
    static let locationArray = "locationArray"
    static let serverLocationArray = "serverLocationArray"
    static let lastHitTimeOfLocation = "lastHitTime"
    static let batteryUsage = "batteryUsage"
    static let filterDictionary = "filterDictionary"
    static let sortingDictionary = "sortingDictionary"
    static let templateDictionary = "templateDictionary"
    static let updatingLocationPathArray = "updatingPathLocationArray"
    static let applicationMode = "applicationMode"
    static let isHitInProgress = "isHitInProgress"
    static let teamId = "teamId"
    static let appCaching = "appCaching"
    static let navigationMap = "navigationMap"
    static let vibration = "vibration"
    static let trafficLayer = "trafficlayer"
    static let batterySaver = "batterysaver"
    static let countForPathSummaryButton = "pathSummaryButtonCount"
    static let lastAccurateUserLocation = "lastUserLocation"
    static let buttonCountOnTaskDetailToShow = "buttonCountOnTaskDetailToShow"
    static let allClearTutorial = "allClearTutorial"
    static let fullTutorial = "fullTutorial"
    static let onlyAssignedTutorial = "onlyAssignedTutorial"
    static let onlyNewTaskTutorial = "onlyNewTaskTutorial"
    static let newTaskTutorial = "newTaskTutorial"
    static let afterAcceptTutorial = "afterAcceptTutorial"
    static let AppleLanguages = "AppleLanguages"
    static let useremail = "useremail"
    static let deviceToken = "deviceToken"
    static let mapStyle = "mapStyle"
    static let appVersion = "appVersion"
    static let userAvailableStatus = "userAvailableStatus"
    static let searchTextForFilter = "searchTextForFilter"
}


struct HEIGHT {
    static let navigationHeight:CGFloat = 70.0 + Singleton.sharedInstance.getTopInsetofSafeArea()
    static let headerHeight:CGFloat = 45.0
    static let errorMessageHeight:CGFloat = 43.0
    static let pushBannerHeight:CGFloat = getAspectRatioValue(value: 75.0)
    static let warningViewHeight:CGFloat = getAspectRatioValue(value: 40.0)
    
}

struct OBSERVER {
    static let mapView = "mapView"
    static let taskListView = "taskList"
    static let filterData = "filterData"
    static let pushNotification = "PushNotification"
    static let changeJobStatusResponse = "jobResponse"
    static let updatePickupDeliveryTask = "pickupDeliveryTask"
    static let startTaskNow = "startTaskNow"
    static let dismissSearch = "dismissSearch"
    static let updatePath = "updatePath"
    static let putOffDuty = "putOffDuty"
    static let reloadTaskTableForImageSection = "reloadTaskTableForImageSection"
    static let reloadTaskTableForNoteSection = "reloadTaskTableForNoteSection"
    static let pushNotificationCountIncrease = "pushNotificationCountIncrease"
    static let pushNotificationCountDecrease = "pushNotificationCountDecrease"
    static let pushNotificationCount = "pushNotificationCount"
    static let getAllNotification = "getAllNotification"
    static let showNotificationAfterSync = "showNotificationAfterSync"
    static let reloadDetailTable = "reloadDetailTable"
    static let signupVerificationPushReceived = "signupVerificationPushReceived"
    static let viewTravelSummary = "viewTravelSummary"
}

struct ANALYTICS_KEY {
    static let FORGOT_PASSWORD = "sign_in_view_forgot_password"
    static let FORGOT_PASSWORD_SUBMIT = "sign_in_forgot_password_submit"
    static let SIGN_IN = "sign_in_view_sign_in"
    static let ADMIN_LOGIN = "sign_in_view_login"
    static let ADMIN_REGISTER = "sign_in_view_register"
    static let DUTY_TOGGLE = "task_list_duty_toggle"
    static let LIST_MAP_TOGGLE = "task_list_map_view_toggle"
    static let CREATE_TASK = "task_list_new_task"
    static let CREATE_TASK_PICKUP = "task_list_add_task_create_pickup"
    static let CREATE_TASK_DELIVERY = "task_list_create_task_delivery"
    static let CREATE_TASK_BOTH = "task_list_add_task_create_both"
    static let CREATE_TASK_CANCEL = "task_list_add_task_cancel"
    static let CREATE_TASK_BACK = "create_task_back"
    static let CREATE_TASK_IMAGES = "create_task_image_added"
    static let CREATE_TASK_IMAGE_GALLERY = "create_task_gallery_image"
    static let CREATE_TASK_IMAGE_CAMERA = "create_task_camera_image"
    static let CREATE_TASK_ADD_IMAGE = "create_task_image_picker"
    static let CREATE_TASK_NOW = "create_task_now_btn"
    static let CREATE_TASK_LATER = "create_task_later_btn"
    static let CREATE_TASK_BUTTON = "press_create_task_button"
    static let TASK_CREATED = "create_task_created"
    static let NO_TASK = "task_list_no_tasks_refresh"
    static let NOTIFICATION_ICON = "task_list_notification_icon"
    static let CALENDAR_ICON = "task_list_calendar_icon"
    static let SEARCH_ICON = "task_list_search_icon"
    static let FILTER_ICON = "task_list_filter_icon"
    static let TASK_LIST_CARD = "task_list_task_card"
    static let MENU_ACTION = "task_list_action_icon"
    static let PROFILE_ACTION = "task_list_action_icon_profile"
    static let SUPPORT_ACTION = "task_list_action_icon_support"
    static let TUTORIAL = "task_list_action_icon_tutorials"
    static let SETTINGS = "task_list_action_icon_settings"
    static let LOGOUT = "task_list_action_icon_logout"
    static let LOGOUT_CONFIRM = "task_list_action_icon_logout_yes"
    static let LOGOUT_CANCEL = "task_list_action_icon_logout_no"
    static let LOGOUT_SYNC = "task_list_logout_sync"
    static let TASK_LIST_REFRESH = "task_list_refresh_task_list"
    static let LANGUAGE_ACTION = "setting_select_language"
    static let LANGUAGE_SELECTED = "setting_select_language_selected"
    static let NAVIGATION_SELECTED = "setting_navigation_selected"
    static let VIBRATION = "notification_vibration_selected"
    static let NOTIFICATION_TONE = "setting_notifications_tone"
    static let SHOW_TRAFFIC = "setting_map_show_traffic"
    static let POWER_SAVING_MODE = "setting_battery_power_saving"
    static let SETTING_BACK = "action_settings_back"
    static let CALENDAR_DATE_SELECTION = "task_calendar_view_select_date"
    static let CALENDAR_MONTH_SELECTION = "task_calendar_view_change_month"
    static let FILTER_STATUS = "filter_by_status_status_option"
    static let FILTER_TEMPLATE = "filter_by_status_template_option"
    static let FILTER_SORTING = "filter_by_status_sorting_option"
    static let FILTER_APPLY = "task_list_filter_by_apply_filter"
    static let FILTER_CLEAR = "task_list_filter_by_clear_button"
    static let FILTER_CLOSE = "task_list_filter_by_close_button"
    static let TASK_DETAIL_MESSAGE = "task_detail_view_message"
    static let TASK_DETAIL_CALL = "task_detail_view_call"
    static let TASK_DETAIL_MASKING_CALL = "task_detail_masking_call"
    static let TASK_DETAIL_NAVIGATION = "task_detail_view_map_navigation"
    static let NAVIGATION_START = "task_detail_map_navigation_start"
    static let NAVIGATION_BACK = "task_detail_map_navigation_back"
    static let TASK_DETAIL_BARCODE = "task_detail_view_barcode_view"
    static let BARCODE_BACK = "task_barcode_view_back"
    static let CHANGE_JOB_STATUS = "task_detail_view_task_status"
    static let TASK_DETAIL_NOTE = "task_detail_view_add_notes"
    static let TASK_DETAIL_SIGNATURE = "task_detail_view_add_signature"
    static let TASK_DETAIL_IMAGE_CAMERA = "task_detail_add_image_camera"
    static let TASK_DETAIL_IMAGE_GALLERY = "task_detail_add_image_gallery"
    static let TASK_DETAIL_INVOICE = "task_detail_view_Invoice"
    static let GOTO_DELIVERY = "task_detail_view_go_to_delivery"
    static let TASK_FAILED_SUBMITTED = "task_failed_reason_submitted"
    static let TASK_FAILED_DISCARD = "task_failed_reason_cancelled"
    static let TASK_MAP_VIEW_SWIPE = "task_map_view_task_swipe"
    static let TASK_MAP_VIEW_DETAIL = "task_map_view_task_detail"
    static let TASK_DETAIL_DESCRIPTION = "task_detail_task_description"
    static let PROFILE_CHANGE_PASSWORD = "profile_change_password"
    static let PROFILE_EDIT = "profile_edit"
    static let PROFILE_SAVE = "profile_edit_save"
    static let PROFILE_ADD_IMAGE = "open_profile_image_choser_dialog"
    static let PROFILE_IMAGE_CAMERA = "profile_image_camera"
    static let PROFILE_IMAGE_GALLERY = "profile_image_gallery"
    static let CHANGE_PASSWORD_SUBMIT = "change_password_submit"
    static let CHANGE_PASSWORD_BACK = "change_password_back"
    static let CHECKLIST = "custom_field_checklist"
    static let LOGO_BUTTON = "today_task_icon"
    static let MAP_VIEW = "map_view"
    static let LIST_VIEW = "list_view"
    static let NOTE_MORE_OPTION = "note_more_option"
    static let NOTE_ADD = "note_added"
    static let NOTE_DELETED = "note_deleted"
    static let NOTE_EDITED = "note_edited"
    static let BARCODE_ADD = "barcode_add"
    static let BARCODE_ADD_SCAN = "barcode_add_scan"
    static let BARCODE_MANUAL = "barcode_add_manual"
    static let BARCODE_CANCEL = "barcode_add_cancel"
    static let BARCODE_MORE_OPTIONS = "barcode_more_options"
    static let BARCODE_EDIT = "barcode_edit"
    static let BARCODE_DELETE = "barcode_delete"
    static let BARCODE_MORE_CANCEL = "barcode_more_option_cancel"
    static let SIGNATURE_DONE = "signature_done"
    static let SIGNATURE_RESET = "signature_reset"
    static let SIGNATURE_BACK = "signature_back"
    static let SIGNUP_COMPLETED = "signup_completed"
    static let SIGNUP_BACK = "signup_back"
    static let CUSTOM_FIELD_BARCODE_SCAN = "custom_field_barcode_scan"
    static let CUSTOM_FIELD_BARCODE_MANUAL = "custom_field_barcode_manual"
    static let CUSTOM_FIELD_DATE = "custom_field_date"
    static let CUSTOM_FIELD_DROPDOWN = "custom_field_dropdown"
    static let CUSTOM_FIELD_CHECKBOX = "custom_field_checkbox"
    static let CUSTOM_FIELD_IMAGE_DELETE = "custom_field_image_delete"
    static let TABLE_CALL = "custom_field_table_call"
    static let TABLE_NAVIGATION = "custom_field_table_navigation"
    static let TABLE_STATUS = "custom_field_table_status"
    static let TABLE_EDIT = "custom_field_table_edit"
    static let TABLE_BACK = "custom_field_table_back"
    static let TASK_DETAIL_BACK = "task_detail_view_ui_back"
    static let VEHICLE_BACK = "vehicle_back"
    static let SHOW_PATH_SUMMARY = "show_path_summary"
    static let HIDE_PATH_SUMMARY = "hide_path_summary"
    static let CALENDAR_AVAILABILITY_BUTTON = "calendar_availability_button"
    static let CALENDAR_EDIT_SCHEDULE = "calendar_edit_schedule"
    static let SCHEDULE_ADD_BUTTON = "schedule_add_button"
    static let SCHEDULE_DELETE_BUTTON = "schedule_delete_button"
    static let SCHEDULE_RESET_BUTTON = "schedule_reset_button"
    static let SCHEDULE_SAVE_BUTTON = "schedule_save_button"
    static let SCHEDULE_AVAILABILITY_ON = "schedule_availability_on"
    static let SCHEDULE_UNAVAILABILITY_OFF = "schedule_unavailability_off"
    static let SCHEDULE_REPEAT = "schedule_repeat"
    static let SCHEDULE_REPEAT_CANCEL = "schedule_repeat_cancel"
    static let SCHEDULE_REPEAT_DONE = "schedule_repeat_done"
    static let SCHEDULE_REPEAT_SEVEN_DAYS = "schedule_repeat_sever_days"
    static let SCHEDULE_REPEAT_FOREVER = "schedule_repeat_forever"
    static let API_SUCCESS = "API_SUCCESS"
    static let API_FAILURE = "API_FAILURE"
    static let FILTER_KEYWORD = "filter_keyword"
    static let MAP_STYLE = "Hamburger_Settings_MapStyle_light/dark"
    static let DONE_EDITING = "Hamburger_Profile_Done_Editing"
    static let PROFILE_EDIT_FIELDS = "Hamburger_Profile_editfields"
    static let SIGNUP_BARCODE_SCAN = "Signup_Barcode_Scan"
}

struct DeviceInfo {
    static var oSVersion  =  UIDevice.current.systemVersion
    static var deviceType = "IOS"
    static var deviceName = UIDevice.current.name
}

enum NotifRequestingResponse: String {
    case currentLocationOfUser = "getCurentLocationCoordinates"
    case locationServicesDisabled = "locationDisabled"
}
