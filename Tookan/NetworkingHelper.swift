//
//  Networking Helper.swift
//  Tookan
//
//  Created by Click Labs on 7/7/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class NetworkingHelper: NSObject {
    static let sharedInstance = NetworkingHelper()
    var allImagesCache = NSCache<NSString,UIImage>()  //Cache<AnyObject,AnyObject>()
    var currentDate:String!
    var lastController = 0
    var dayTasksListArray = [[TasksAssignedToDate]]()
    var newTaskListArray = [[TasksAssignedToDate]]()
    var polylines:NSArray!
    var notificationArray = [NotificationDataObject]()
    var allNotificationDataArray = [NotificationDataObject]()
    var serverTime:String!
    var syncView:SyncView!
    var notificationCount = 0
    var notificationJobId = -1
    var deviceToken =  UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) != nil ? UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) as? String : "No deviceToken"
    
    override init() {}
    
    func showSyncView() {
        if(syncView == nil) {
            syncView = UINib(nibName: NIB_NAME.syncView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! SyncView
        }
        syncView.startLoader(TEXT.DATA_SYNCING_PROGRESS, subtitle: TEXT.TAKE_FEW_MINUTES)
    }
    
    func hideSyncView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if(self.syncView != nil) {
                self.syncView.stopLoader()
                self.syncView = nil
                if(NetworkingHelper.sharedInstance.notificationArray.count > 0) {
                    Singleton.sharedInstance.handlingPushAfterGettingAllNotification()
                }
            }
        }
    }
    
    func updateProgressValue(_ value:Float) {
        if(syncView != nil) {
            syncView.updateProgressValue(value)
        }
    }
    
    func updatingFleetLocation(_ location: String, status:String, locationUpdateMode:String, getLocationLatitude:CLLocationDegrees, getLocationLongitude:CLLocationDegrees, getLocationAccuracy:CLLocationAccuracy, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()){
        var params:[String:Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["location"] = "\(location)"
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(API_NAME.update_fleet_location, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                if(succeeded){
                    DispatchQueue.main.async(execute: { () -> Void in
                        var hitStatus = ""
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            hitStatus = "Success"
                        default:
                            hitStatus = "Fail"
                        }
                        if(APP_STORE.value == 0) {
                            LocationTracker.sharedInstance().setLogsForDevelopmentMode(locationData: location, status: status, hitResponse: hitStatus, latitude: getLocationLatitude, longitude: getLocationLongitude, locationAccuracy: getLocationAccuracy)
                        }
                        if(hitStatus == "Success") {
                            receivedResponse(true, ["data":response["data"] as AnyObject,"status":response["status"] as! Int, "location":location])
                        } else {
                            if let message = response["message"] as? String {
                                receivedResponse(false, ["status":response["status"] as! Int, "message":message])
                            } else {
                                receivedResponse(false, ["status":response["status"] as! Int, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                            }
                        }
                    })
                } else {
                    if(APP_STORE.value == 0) {
                         LocationTracker.sharedInstance().setLogsForDevelopmentMode(locationData: location, status: status, hitResponse: "Fail", latitude: getLocationLatitude, longitude: getLocationLongitude, locationAccuracy: getLocationAccuracy)
                    }
                    receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                }
            }
        } else {
            if(APP_STORE.value == 0) {
                LocationTracker.sharedInstance().setLogsForDevelopmentMode(locationData: location, status: status, hitResponse: "No Internet Connection", latitude: getLocationLatitude, longitude: getLocationLongitude, locationAccuracy: getLocationAccuracy)
            }
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    func addTaskDetails(_ accessToken: String, text: String, type: String, jobId : Int, image: UIImage, caption:String, timeStamp:String, imagePath:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any])  -> ()) {
        var imageData = UIImageJPEGRepresentation(image, 0.1)
        var isImage = false
        if imageData == nil {
            imageData = Data()
            isImage = false
        } else {
            isImage = true
        }
        var lat:String = ""
        var lng:String = ""
        
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var param = [String:Any]()
        param["job_id"] = jobId
        param["type"] = type
        param["text"] = text
        param["access_token"] = accessToken
        param["lat"] = lat
        param["lng"] = lng
        param["extra_fields"] = caption
        
        if IJReachability.isConnectedToNetwork() == true {
            uploadingMultipleTask(API_NAME.add_task_detail, params: param as [String:AnyObject], isImage: isImage, imageData: [image], imageKey: "image", receivedResponse: { (succeeded, response) -> () in
                if(succeeded == true) {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA, STATUS_CODES.INVALID_ACCESS_TOKEN:
                        DispatchQueue.main.async(execute: { () -> Void in
                            receivedResponse(true, ["responseData":response, "text":text, "timeStamp":timeStamp, "type":type,"imagePath":imagePath, "jobId":jobId])
                        })
                        break
                    default:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["message":message, "text":text, "timeStamp":timeStamp,"status":STATUS_CODES.SHOW_MESSAGE, "type":type, "imagePath":imagePath, "jobId":jobId])
                        } else {
                            receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "text":text, "timeStamp":timeStamp,"status":STATUS_CODES.SHOW_MESSAGE, "type":type, "imagePath":imagePath, "jobId":jobId])
                        }
                    }
                } else {
                    receivedResponse(false, ["text":text, "timeStamp":timeStamp,"status":STATUS_CODES.SLOW_INTERNET_CONNECTION, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "type":type, "imagePath":imagePath, "jobId":jobId])
                }
            })
        } else {
            receivedResponse(false, ["status":0,"message":ERROR_MESSAGE.NO_INTERNET_CONNECTION, "imagePath":imagePath, "type":type, "text":text, "timeStamp":timeStamp, "jobId":jobId])
        }
    }
    
    func deleteTaskDetails(_ type: String, jobId: Int, id: Int, imagePath:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var param:[String:Any] = ["id":id]
        param["job_id"] = jobId
        param["type"] = type
        param["access_token"] = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String
        param["lat"] = lat
        param["lng"] = lng
        
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(API_NAME.delete_task_detail, params: param as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                if(succeeded){
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA, STATUS_CODES.INVALID_ACCESS_TOKEN:
                        DispatchQueue.main.async(execute: { () -> Void in
                            receivedResponse(true, ["response":response, "imagePath" :imagePath, "jobId":jobId])
                        })
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["response":response, "imagePath" :imagePath, "message":message, "jobId":jobId])
                        } else {
                            receivedResponse(false, ["response":response, "imagePath" :imagePath, "message":ERROR_MESSAGE.INVALID_ACCESS_TOKEN, "jobId":jobId])
                        }
                    default:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["imagePath":imagePath,"status":STATUS_CODES.SHOW_MESSAGE, "message":message, "jobId":jobId])
                        } else {
                            receivedResponse(false, ["imagePath":imagePath,"status":STATUS_CODES.SHOW_MESSAGE, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "jobId":jobId])
                        }
                    }
                } else {
                    receivedResponse(false, ["imagePath":imagePath,"status":0, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "jobId":jobId])
                }
            }
        } else {
           receivedResponse(false, ["imagePath":imagePath,"status":0, "message":ERROR_MESSAGE.NO_INTERNET_CONNECTION, "jobId":jobId])
        }
    }
    
    func updateCustomFieldWithImage(_ access_token: String, custom_field_label : String, job_id : Int , data : String, image: UIImage, caption:String, customFieldValue:Int, imageArray:NSMutableArray, imagePath:String, receivedResponse:@escaping (_ succeeded:Bool,_ response:[String:Any]) ->()){
        var imageData = UIImageJPEGRepresentation(image, 0.1)
        var isImage = false
        if imageData == nil {
            imageData = Data()
            isImage = false
        } else {
            isImage = true
        }
        
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var param:[String:Any] = ["custom_field_label":custom_field_label]
        param["job_id"] = job_id
        param["data"] = data
        param["access_token"] = access_token
        param["lat"] = lat
        param["lng"] = lng
        param["caption"] = caption
        if IJReachability.isConnectedToNetwork() == true {
            uploadingMultipleTask(API_NAME.updateCustomField, params: param as [String:AnyObject], isImage: isImage, imageData: [image], imageKey: "custom_image", receivedResponse: { (succeeded, response) -> () in
                if(succeeded == true) {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        DispatchQueue.main.async(execute: { () -> Void in
                            receivedResponse(true, ["CustomField":custom_field_label, "Index":customFieldValue, "json":response, "imagePath":imagePath, "jobId":job_id])
                        })
                    case STATUS_CODES.INVALID_ACCESS_TOKEN:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["CustomField":custom_field_label, "Index":customFieldValue, "imagePath":imagePath,"status":STATUS_CODES.INVALID_ACCESS_TOKEN, "message":message, "jobId":job_id])
                        } else {
                            receivedResponse(false, ["CustomField":custom_field_label, "Index":customFieldValue, "imagePath":imagePath,"status":STATUS_CODES.INVALID_ACCESS_TOKEN, "message":ERROR_MESSAGE.INVALID_ACCESS_TOKEN, "jobId":job_id])
                        }
                    case STATUS_CODES.SHOW_MESSAGE:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["CustomField":custom_field_label, "Index":customFieldValue, "imagePath":imagePath,"status":STATUS_CODES.SHOW_MESSAGE, "message":message, "jobId":job_id])
                        } else {
                            receivedResponse(false, ["CustomField":custom_field_label, "Index":customFieldValue, "imagePath":imagePath,"status":STATUS_CODES.SHOW_MESSAGE, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "jobId":job_id])
                        }
                    default:
                        imageArray.remove(imagePath)
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["CustomField":custom_field_label, "Index":customFieldValue, "imagePath":imagePath,"status":0, "message":message, "jobId":job_id])
                        } else {
                            receivedResponse(false, ["CustomField":custom_field_label, "Index":customFieldValue, "imagePath":imagePath,"status":0, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "jobId":job_id])
                        }
                    }
                } else {
                    receivedResponse(false, ["CustomField":custom_field_label, "Index":customFieldValue, "imagePath":imagePath,"status":STATUS_CODES.SLOW_INTERNET_CONNECTION, "jobId":job_id])
                }
            })
            
        } else {
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION, "CustomField":custom_field_label, "Index":customFieldValue, "imagePath":imagePath,"status":0, "jobId":job_id])
        }
    }
    
    func updateCustomFieldWithOutImage(_ custom_field_label : String, job_id : Int , data : String,status:String, imageName:String, collectionIndex:Int, receivedResponse:@escaping (_ succeeded:Bool,_ response:[String:Any]) ->()){
        var lat:String = ""
        var lng:String = ""
        /*======= SET MODE ==========*/
        switch(UserDefaults.standard.integer(forKey: USER_DEFAULT.batteryUsage)) {
        case BATTERY_USAGE.foreground:
            if let location = LocationTracker.sharedInstance().getLatestLocationForForegroundMode() {
                lat = "\(location.coordinate.latitude)"
                lng = "\(location.coordinate.longitude)"
            }
            break
        default:
            break
        }
        /*==========================================*/
        var param:[String:Any] = ["custom_field_label":custom_field_label]
        param["job_id"] = job_id
        param["data"] = data
        param["access_token"] = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String
        param["status"] = status
        param["lat"] = lat
        param["lng"] = lng
        
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(API_NAME.updateCustomField, params: param as [String:AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                if(succeeded){
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA, STATUS_CODES.INVALID_ACCESS_TOKEN:
                        DispatchQueue.main.async(execute: { () -> Void in
                            receivedResponse(true, ["response":response, "imagePath":imageName, "collectionIndex":collectionIndex, "jobId":job_id])
                        })
                    case STATUS_CODES.SHOW_MESSAGE:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["collectionIndex":collectionIndex,"status":STATUS_CODES.SHOW_MESSAGE, "message":message, "jobId":job_id])
                        } else {
                            receivedResponse(false, ["collectionIndex":collectionIndex,"status":STATUS_CODES.SHOW_MESSAGE, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "jobId":job_id])
                        }
                    default:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["collectionIndex":collectionIndex,"status":0, "message":message, "jobId":job_id])
                        } else {
                            receivedResponse(false, ["collectionIndex":collectionIndex,"status":0, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "jobId":job_id])
                        }
                    }
                } else {
                    receivedResponse(false, ["collectionIndex":collectionIndex,"status":STATUS_CODES.SLOW_INTERNET_CONNECTION, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "jobId":job_id])
                }
            }
        } else {
            receivedResponse(false, ["status":STATUS_CODES.SLOW_INTERNET_CONNECTION, "message":ERROR_MESSAGE.NO_INTERNET_CONNECTION, "jobId":job_id])
        }
    }
    
    func commonServerCall(apiName:String,params: [String : AnyObject]?,httpMethod:String,receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(apiName, params: params! , httpMethod: httpMethod, isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async {
                    print(response)
                    if(succeeded){
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA, STATUS_CODES.UNAUTHORIZED_FOR_AVAILABILITY:
                                receivedResponse(true, response)
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                if let message = response["message"] as? String {
                                    receivedResponse(true, ["data":response["data"] as! [String:Any],"status":status, "message":message])
                                } else {
                                    receivedResponse(true, ["data":response["data"] as! [String:Any],"status":status, "message":ERROR_MESSAGE.INVALID_ACCESS_TOKEN])
                                }
                            case STATUS_CODES.SHOW_MESSAGE:
                                if let message = response["message"] as? String {
                                    receivedResponse(false, ["status":status, "message":message])
                                } else {
                                    receivedResponse(false, ["status":status, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                                }
                            default:
                                if let message = response["message"] as? String {
                                    receivedResponse(false, ["status":status, "message":message])
                                } else {
                                    receivedResponse(false, ["status":status, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                                }
                            }
                        } else {
                            receivedResponse(false, ["status":0,"message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        }
                    } else {
                        receivedResponse(false, ["status":0, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                }
            }
        } else {
            receivedResponse(false, ["status":0, "message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }

    /*----------------- Auto complete API ------------*/
    func autoCompleteSearch(_ searchKeyword:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any])->()) {
        if IJReachability.isConnectedToNetwork() == true {
            let session = URLSession.shared
            let formattedSearchKey = searchKeyword.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(formattedSearchKey)&radius=500&key=\(APIKeyForGoogleMaps)"
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            session.dataTask(with: URL(string: urlString)!) {data, response, error in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                var encodedRoute: [String:Any]?
                if(data != nil) {
                    do {
                        if let json = (try JSONSerialization.jsonObject(with: data!, options:[])) as? [String:Any] {
                            encodedRoute = json
                        }
                        DispatchQueue.main.async {
                            if(encodedRoute != nil) {
                                receivedResponse(true, encodedRoute!)
                            } else {
                                receivedResponse(false, [:])
                            }
                        }
                    } catch {
                        receivedResponse(false, [:])
                    }
                } else {
                    receivedResponse(false, [:])
                }
                }.resume()
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            receivedResponse(false, [:])
        }
    }
    
    /*------------ Latitude and Longitude from Place Id --------------*/
    func getLatLongFromPlaceId(_ placeId:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any])-> ()) {
        if IJReachability.isConnectedToNetwork() == true {
            let session = URLSession.shared
            let urlString =  "https://maps.googleapis.com/maps/api/place/details/json?input=bar&placeid=\(placeId)&key=\(APIKeyForGoogleMaps)"
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            session.dataTask(with: URL(string: urlString)!) {data, response, error in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                guard data != nil else {
                    receivedResponse(false, [:])
                    return
                }
                var encodedRoute: [String:Any]?
                do {
                    if let json = (try JSONSerialization.jsonObject(with: data!, options:[])) as? [String:Any] {
                        encodedRoute = json
                    }
                    DispatchQueue.main.async {
                        if(encodedRoute != nil) {
                            receivedResponse(true, encodedRoute!)
                        } else {
                            receivedResponse(false, [:])
                        }
                    }
                } catch {
                     receivedResponse(false, [:])
                }
                }.resume()
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            receivedResponse(false, [:])
        }
    }
    
    /*------------ Get Address from Latitude, Longitude --------------*/
    func getAddressFromLatLong(_ latLong:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any])-> ()) {
        if IJReachability.isConnectedToNetwork() == true {
            let session = URLSession.shared
            let urlString =  "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latLong)&key=\(APIKeyForGoogleMaps)"
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            session.dataTask(with: URL(string: urlString)!) {data, response, error in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                var encodedRoute:[String:Any]?
                do {
                    if let json = (try JSONSerialization.jsonObject(with: data!, options:[])) as? [String:Any] {
                        encodedRoute = json
                    }
                    DispatchQueue.main.async {
                        if(encodedRoute != nil) {
                            receivedResponse(true, encodedRoute!)
                        } else {
                            receivedResponse(false, [:])
                        }
                    }
                } catch {
                    receivedResponse(false, [:])
                }
                }.resume()
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            receivedResponse(false, [:])
        }
    }

    func getLatLongFromDirectionAPI(_ origin:String, destination:String) -> [String:Any]! {
        var encodedRoute = [String:Any]()
        if IJReachability.isConnectedToNetwork() == true {
          //  let session = NSURLSession()
            // "https://maps.googleapis.com/maps/api/directions/json?origin=30.7342187,76.78088307&destination=30.74571777,76.78635478&sensor=false&mode=driving&alternatives=false"
            let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=false&mode=driving&alternatives=false"
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            if let json = URLSession.requestSynchronousJSONWithURLString(urlString) {
                encodedRoute = json as! [String:Any]
            }
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
            encodedRoute = [:]
        }
        return encodedRoute
    }
    
    /*---------- Upload Image to Server ------------*/
    func uploadImageToServer(_ refImage:UIImage,imagePath:String,tag:Int, receivedResponse:@escaping (_ succeeded:Bool,_ response:[String:Any])-> ()) {
         if IJReachability.isConnectedToNetwork() == true {
            var imageData = UIImageJPEGRepresentation(refImage, 0.1)
            var isImage = false
            if imageData == nil {
                imageData = Data()
                isImage = false
            } else {
                isImage = true
            }
            
            uploadingMultipleTask(API_NAME.uploadReferenceImage, params: [:], isImage: isImage, imageData: [refImage], imageKey: "ref_image", receivedResponse: { (succeeded, response) -> () in
                if(succeeded == true) {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        DispatchQueue.main.async(execute: { () -> Void in
                            receivedResponse(true, ["responseData":response, "imagePath":imagePath, "tag":tag])
                        })
                    default:
                        if let message = response["message"] as? String {
                            receivedResponse(false, ["status":response["status"] as! Int, "message":message, "imagePath":imagePath, "tag":tag])
                        } else {
                            receivedResponse(false, ["status":response["status"] as! Int, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "imagePath":imagePath, "tag":tag])
                        }
                    }
                } else {
                    receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING, "imagePath":imagePath, "tag":tag])
                }
            })
        } else {
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION, "imagePath":imagePath, "tag":tag])
        }
    }
    
    /*---------------- Upload Offline Data ------------------*/
    func sendRequestToUploadOfflineData(_ jobID:NSNumber, dataJson:[String:Any]) -> [String:Any]! {
        var params:[String:Any] = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String]
        params["fleet_id"] = Singleton.sharedInstance.fleetDetails.fleetId!
        params["job_id"] = jobID
        params["data"] = dataJson.jsonString
        var response:[String:Any]!
        if IJReachability.isConnectedToNetwork() == true {
            let urlString = API_NAME.uploadOfflineData.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            var request = URLRequest(url: URL(string: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) + urlString!)!)
            request.httpMethod = "POST"
            request.timeoutInterval = 20
            request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            if let data = URLSession.requestSynchronousData(request){//NSURLSession.requestSynchronousJSON(request) {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        response = json
                    } else {
                         UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let jsonStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                        print("Error could not parse JSON: \(jsonStr ?? "nil")")
                        return nil
                    }
                } catch let parseError {
                    print(parseError)
                     UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    // Log the error thrown by `JSONObjectWithData`
                    let jsonStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print("Error could not parse JSON: '\(jsonStr ?? "nil")'")
                    return nil
                }
            } else {
               return nil
            }
        } else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: .error)
        }
        return response
    }
    
    func fetchPathPoints(_ from: CLLocationCoordinate2D, to: CLLocationCoordinate2D, completion: @escaping ((NSDictionary?) -> Void)) -> () {
        let session = URLSession.shared
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(from.latitude),\(from.longitude)&destination=\(to.latitude),\(to.longitude)&mode=driving"
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        session.dataTask(with: URL(string: urlString)!) {data, response, error in
            var encodedRoute: NSDictionary?
            if(data != nil) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options:[]) as? [String:AnyObject]
                    if let jsonData = json {
                        encodedRoute = jsonData as NSDictionary
                    }
                } catch {
                    print("Error")
                }
            }
            DispatchQueue.main.async {
                if(encodedRoute != nil) {
                    completion(encodedRoute)
                } else {
                    completion([:])
                }
            }
            }.resume()
    }
}
