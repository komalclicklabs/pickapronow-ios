//
//  ArrowBottomView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

protocol ArrowButtonDelegate {
    func arrowAction()
}

class ArrowBottomView: UIView {

    @IBOutlet var arrowButton: UIButton!
    var delegate:ArrowButtonDelegate!
    
    override func awakeFromNib() {
        arrowButton.setTitle("Repeat this day on", for: UIControlState.normal)
        arrowButton.setTitleColor(COLOR.themeForegroundColor, for: UIControlState.normal)
    }
    
    
    func resetTitle() {
        arrowButton.setTitle("Repeat this day on", for: UIControlState.normal)
    }
    
    @IBAction func arrowAction(_ sender: AnyObject) {
        delegate.arrowAction()
    }
}
