//
//  CreateTaskModel.swift
//  Tookan
//
//  Created by cl-macmini-150 on 11/04/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class CreateTaskModel: NSObject {

    //Delivery
    var email = ""
    var phone = ""
    var name = ""
    var address = ""
    var startTime = ""
    var endTime = ""
    var template = ""
    var latitude = ""
    var longitude = ""
    var taskDescription = ""
    var has_delivery = 0
    var has_pickup = 0 //self.hasPickup,
    var jobType = 0
    
    func setJsonForCreateTaskRequest() -> [String:Any] {
        let accessToken = Singleton.sharedInstance.getAccessToken()
        let fleetId = Singleton.sharedInstance.fleetDetails.fleetId!
        var pickupTasks = [[String:Any]]()
        var deliveryTasks = [[String:Any]]()
        var appointmentTasks = [[String:Any]]()
        var hasPickup = 0
        var hasDelivery = 0
        
        for task in Singleton.sharedInstance.createTaskArray {
            switch task.jobType {
            case JOB_TYPE.pickup:
                let data = self.getPickupDeliveryTaskJson(task: task)
                pickupTasks.append(data)
                hasPickup = 1
                break
            case JOB_TYPE.delivery:
                let data = self.getPickupDeliveryTaskJson(task: task)
                deliveryTasks.append(data)
                hasDelivery = 1
                break
            default:
                let data = self.getAppointmentFOSTaskJson(task: task)
                appointmentTasks.append(data)
                break
            }
        }
        
        var params:[String:Any] = ["access_token":accessToken]
        params["fleet_id"] = fleetId
        params["pickups"] = pickupTasks
        params["deliveries"] = deliveryTasks
        params["appointments"] = appointmentTasks
        params["has_pickup"] = hasPickup
        params["has_delivery"] = hasDelivery
        params["layout_type"] = UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType)
        params["team_id"] = UserDefaults.standard.integer(forKey: USER_DEFAULT.teamId)
//        let params:[String:Any] = [
//            "access_token":accessToken,
//            "fleet_id":fleetId,
//            "pickups":pickupTasks,
//            "deliveries":deliveryTasks,
//            "appointments":appointmentTasks,
//            "has_pickup":hasPickup,
//            "has_delivery":hasDelivery,
//            "layout_type":UserDefaults.standard.integer(forKey: USER_DEFAULT.layoutType),
//            "team_id":UserDefaults.standard.integer(forKey: USER_DEFAULT.teamId)
//        ]
        
        return params
    }
    
    func getPickupDeliveryTaskJson(task:CreateTaskModel) -> [String:Any] {
        var params:[String:Any] = ["email":task.email]
        params["phone"] = task.phone
        params["name"] = task.name
        params["address"] = task.address
        params["latitude"] = task.latitude
        params["longitude"] = task.longitude
        params["time"] = task.startTime
        params["template_name"] = task.template
        params["description"] = task.taskDescription
//        let params:[String:Any] = [
//            "email":task.email,
//            "phone":task.phone,
//            "name":task.name,
//            "address":task.address,
//            "latitude": task.latitude,
//            "longitude": task.longitude,
//            "time": task.startTime,
//            "template_name":task.template,
//            "description":task.taskDescription,
//        ]
        return params
    }
    
    func getAppointmentFOSTaskJson(task:CreateTaskModel) -> [String:Any] {
        var params:[String:Any] = ["email":task.email]
        params["phone"] = task.phone
        params["name"] = task.name
        params["address"] = task.address
        params["latitude"] = task.latitude
        params["longitude"] = task.longitude
        params["start_time"] = task.startTime
        params["end_time"] = task.endTime
        params["template_name"] = task.template
        params["description"] = task.taskDescription
//        let params:[String:Any] = [
//            "email":task.email,
//            "phone":task.phone,
//            "name":task.name,
//            "address":task.address,
//            "latitude": task.latitude,
//            "longitude": task.longitude,
//            "start_time": task.startTime,
//            "end_time": task.endTime,
//            "template_name":task.template,
//            "description":task.taskDescription,
//            ]
        return params
    }
}
