//
//  CopyRepeatView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/10/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import FirebaseAnalytics

protocol CopyRepeatDelegate {
    func hideCopyRepeatPopup()
    func doneFromCopyRepeatPopup()
    func updateDescriptionText()
}


class CopyRepeatView: UIView  {

    @IBOutlet var popupHeightConstraint: NSLayoutConstraint!
    @IBOutlet var popupView: UIView!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var forever: UIButton!
    @IBOutlet var onlyOnce: UIButton!
    @IBOutlet var frequencyLabel: UILabel!
    @IBOutlet var repeatLabel: UILabel!
    @IBOutlet var copyCollectionView: UICollectionView!
    
    var delegate:CopyRepeatDelegate!
    var selectedWeekdays = [Int]()
    var finalSelectedDays = [Int]()
    var lastStateOfForeverButton = false
    let normalColor = UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 1.0)

    override func awakeFromNib() {
        copyCollectionView.register(UINib(nibName: "CopyRepeatCell", bundle: nil), forCellWithReuseIdentifier: "CopyRepeatCell")
        repeatLabel.text = "Repeat on".localized
        frequencyLabel.text = "Frequency".localized
        onlyOnce.setTitle("Only once".localized, for: UIControlState.normal)
        forever.setTitle("Forever".localized, for: UIControlState.normal)
        doneButton.setTitle("Done".localized, for: UIControlState.normal)
        //descriptionLabel.text = "Repeats on ".localized
        self.popupView.layer.cornerRadius = 8.0
        self.popupHeightConstraint.constant = 380
        doneButton.backgroundColor = COLOR.themeForegroundColor
        doneButton.isEnabled = true
    }
    
    func setRepeatData() {
        self.selectedWeekdays = self.finalSelectedDays
        self.setRadioButtons()
        delegate.updateDescriptionText()
        self.copyCollectionView.reloadData()
    }
    
    func resetRepeatData() {
        descriptionLabel.text = ""
        self.selectedWeekdays.removeAll()
        self.finalSelectedDays.removeAll()
        self.popupHeightConstraint.constant = 380
        self.setRadioButtons()
    }
    
    func setRadioButtons() {
        if self.selectedWeekdays.count > 0 {
            forever.isSelected = lastStateOfForeverButton
            onlyOnce.isSelected = !lastStateOfForeverButton
            onlyOnce.isEnabled = true
            forever.isEnabled = true
        } else {
            forever.isEnabled = false
            onlyOnce.isEnabled = false
            forever.isSelected = false
            onlyOnce.isSelected = false
        }
    }
    
    @IBAction func doneAction(_ sender: AnyObject) {
        delegate.doneFromCopyRepeatPopup()
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        delegate.hideCopyRepeatPopup()
    }
    
    @IBAction func sevenDaysAction(_ sender: AnyObject) {
        Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_REPEAT_SEVEN_DAYS, parameters: [:])
        onlyOnce.isSelected = !onlyOnce.isSelected
        forever.isSelected = !forever.isSelected
        delegate.updateDescriptionText()
        lastStateOfForeverButton = forever.isSelected
    }
    
    @IBAction func foreverAction(_ sender: AnyObject) {
        Analytics.logEvent(ANALYTICS_KEY.SCHEDULE_REPEAT_FOREVER, parameters: [:])
        onlyOnce.isSelected = !onlyOnce.isSelected
        forever.isSelected = !forever.isSelected
        delegate.updateDescriptionText()
        lastStateOfForeverButton = forever.isSelected
    }
}
