//
//  ImagesPreview.swift
//  Tookan
//
//  Created by Rakesh Kumar on 1/27/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

@objc protocol ImagePreviewDelegate {
    func dismissImagePreview()
    @objc optional func deleteImageActionFromImages(_ index:Int)
    @objc optional func deleteImageActionFromCustomField(_ indexItem:Int, collectionViewIndex:Int)
}

class ImagesPreview: UIView, UICollectionViewDataSource, UICollectionViewDelegate, NoInternetConnectionDelegate {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pageNumber: UILabel!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    var imageArray = NSMutableArray()
    var delegate:ImagePreviewDelegate!
    var isTopBottomViewHidden = false
    var imageType:Int!
    var currentIndex:Int!
    var collectionViewTag:Int!
    var completeStatus:Int!
    var internetToast:NoInternetConnectionView!
    var captionArray = [Caption]()
    
    override func awakeFromNib() {
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        imageCollectionView.register(UINib(nibName: "ImagePreviewCell", bundle: nil), forCellWithReuseIdentifier: "ImagePreviewCell")
        
        /*------------- Tap Gesture ----------------*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(ImagesPreview.hideTopBottomView))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    @IBAction func deleteAction(_ sender: AnyObject) {
        if IJReachability.isConnectedToNetwork() == false && Auxillary.isAppSyncingEnable() == false {
            self.showInternetToast()
        } else {
            if(self.imageType != nil) {
                switch(self.imageType) {
                case ImageType.image.hashValue:
                    delegate.deleteImageActionFromImages!(currentIndex)
                    break
                case ImageType.customFieldImage.hashValue, ImageType.signupImage.hashValue:
                    delegate.deleteImageActionFromCustomField!(currentIndex, collectionViewIndex: self.collectionViewTag)
                    break
                case ImageType.createTaskImage.hashValue:
                    break
                case ImageType.taskDescription.hashValue:
                    break
                default:
                    break
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        delegate.dismissImagePreview()
    }
    
    func confirmationDoneForDeletion() {
        imageArray.removeObject(at: currentIndex)
        if captionArray.count > self.currentIndex {
            captionArray.remove(at: currentIndex)
        }
        if(imageArray.count == 0){
            delegate.dismissImagePreview()
        } else if(currentIndex >= imageArray.count) {
            self.setCollectionViewWithPage(imageArray.count - 1)
        } else {
            self.setCollectionViewWithPage(currentIndex)
        }
    }
    
    func setCollectionViewWithPage(_ index:Int) {
        self.layoutIfNeeded()
        if IJReachability.isConnectedToNetwork() == false {
            self.showInternetToast()
        }
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = headerView.bounds
        headerView.addSubview(blurView)
        headerView.sendSubview(toBack: blurView)
        
        
        let darkBlurForPageNumber = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurViewForPageNumber = UIVisualEffectView(effect: darkBlurForPageNumber)
        blurViewForPageNumber.frame = bottomView.bounds
        bottomView.addSubview(blurViewForPageNumber)
        bottomView.sendSubview(toBack: blurViewForPageNumber)
        
        if self.imageType == ImageType.signupImage.hashValue {
            self.deleteButton.isHidden = false
        } else if(self.imageType == ImageType.taskDescription.hashValue || Singleton.sharedInstance.hasTaskCompleted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus) == true) {
            self.deleteButton.isHidden = true
        } else if Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == false {
            self.deleteButton.isHidden = true
        }
        
        
        
        currentIndex = index
        pageNumber.text = "\(index + 1)/\(imageArray.count)"
        imageCollectionView.reloadData()
        if(index < imageArray.count) {
            imageCollectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: UICollectionViewScrollPosition.right, animated: false)
        }
    }
    
    //MARK: UICollection View Delegate...
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width:CGFloat = collectionView.frame.width
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagePreviewCell", for: indexPath) as! ImagePreviewCell
        cell.imageView.layer.borderWidth = 0.0
        cell.imageScrollView.delegate = self
        cell.imageScrollView.minimumZoomScale = 1.0
        cell.imageScrollView.maximumZoomScale = 10.0
        cell.imageScrollView.zoomScale = 1.0
        //cell.captionTextView.isHidden = true
        let zoomTap = UITapGestureRecognizer(target: self, action: #selector(self.setZoom))
        zoomTap.numberOfTapsRequired = 2
        cell.addGestureRecognizer(zoomTap)
        
        if((imageArray.object(at: (indexPath as NSIndexPath).item) as AnyObject).isKind(of: UIImage.classForCoder()) == true) {
            cell.imageView.image = imageArray.object(at: (indexPath as NSIndexPath).item) as? UIImage
        } else {
            if(Auxillary.isFileExistAtPath(imageArray.object(at: (indexPath as NSIndexPath).item) as! NSString as String) == true) {
                getImageFromDocumentDirectory(imageArray.object(at: (indexPath as NSIndexPath).item) as! String, imageView: cell.imageView, placeHolderImage:  UIImage(named: "imagePlaceholder")!, contentMode: UIViewContentMode.scaleAspectFit)
            } else {
                cell.imageView.image = downloadImageAsynchronously(imageArray.object(at: (indexPath as NSIndexPath).item) as! String, imageView: cell.imageView, placeHolderImage: UIImage(named: "imagePlaceholder")!, contentMode: UIViewContentMode.scaleAspectFit)
            }
        }
        cell.imageView.contentMode = UIViewContentMode.scaleAspectFit
        
//        cell.captionTextView.isScrollEnabled = false
//        cell.captionTextView.isHidden = false
//        cell.captionTextView.text = captionArray[indexPath.item].caption_data
//        self.setCaptionView(caption: captionArray[indexPath.item].caption_data!, cell: cell)
//        if (captionArray[indexPath.item].caption_data?.heightWithConstrainedWidth(width: cell.captionTextView.frame.width, font: cell.captionTextView.font!
//            ).size.height)! > cell.captionTextView.frame.height {
//            cell.captionTextView.isScrollEnabled = true
//        } else {
//            cell.captionTextView.isScrollEnabled = false
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let imageCell = cell as? ImagePreviewCell {
            imageCell.captionTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10)
            //imageCell.captionTextView.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
            imageCell.captionTextView.isScrollEnabled = false
            imageCell.captionTextView.isHidden = false
            let captionText = self.captionArrayValue(index: indexPath.item)
            imageCell.captionTextView.text = captionText
            self.setCaptionView(caption: captionText, cell: imageCell)
            if (captionText.heightWithConstrainedWidth(width: imageCell.captionTextView.frame.width, font: UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)!).size.height) >= 150 {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
                    self.updateCaptionView(imageCell: imageCell)
                }
            } else {
                imageCell.captionTextView.isScrollEnabled = false
            }
            
        }
    }
    
    func updateCaptionView(imageCell:ImagePreviewCell) {
        imageCell.captionTextView.isScrollEnabled = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.imageCollectionView {
            let index = Int(scrollView.contentOffset.x / scrollView.frame.width)
            currentIndex = index
            pageNumber.text = "\(index + 1)/\(imageArray.count)"
            
//            if let cell = self.imageCollectionView.cellForItem(at: IndexPath(item: index, section: 0)) as? ImagePreviewCell {
//                if cell.captionTextView.frame.origin.y > SCREEN_SIZE.height - 60.0 {
//                    self.imageCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
//                }
//            }
        }
    }
    
   
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if let cell = imageCollectionView.cellForItem(at: NSIndexPath(item: currentIndex, section: 0) as IndexPath) as? ImagePreviewCell {
            if scale == 1.0 {
                self.imageCollectionView.isScrollEnabled = true
            } else {
                self.imageCollectionView.isScrollEnabled = false
            }
            self.setCaptionView(caption: self.captionArrayValue(index: self.currentIndex), cell: cell)
        }
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        self.imageCollectionView.isScrollEnabled = false
        if let cell = imageCollectionView.cellForItem(at: NSIndexPath(item: currentIndex, section: 0) as IndexPath) as? ImagePreviewCell {
            cell.captionTextView.isHidden = true
        }
    }
    
    /*-------- Dismiss ---------*/
    func hideTopBottomView() {
        if(isTopBottomViewHidden == false) {
            isTopBottomViewHidden = true
            UIApplication.shared.isStatusBarHidden = true
            self.imageCollectionView.backgroundColor = UIColor.black
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.headerView.transform = CGAffineTransform(translationX: 0.0, y: -self.headerView.frame.height)
                self.bottomView.transform = CGAffineTransform(translationX: 0.0, y: self.bottomView.frame.height)
            })
        } else {
            isTopBottomViewHidden = false
             UIApplication.shared.isStatusBarHidden = false
            self.imageCollectionView.backgroundColor = UIColor.white
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.headerView.transform = CGAffineTransform.identity
                self.bottomView.transform = CGAffineTransform.identity
            })
        }
        
        if let cell = imageCollectionView.cellForItem(at: NSIndexPath(item: currentIndex, section: 0) as IndexPath) as? ImagePreviewCell {
            self.setCaptionView(caption: self.captionArrayValue(index: self.currentIndex), cell: cell)
        }
    }
    
    //MARK: NoInternetConnectionView
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: "NoInternetConnectionView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:HEIGHT.navigationHeight, width: self.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        guard scrollView != self.imageCollectionView else {
            return nil
        }
        
        guard currentIndex != nil else {
            return self
        }

        if let cell = imageCollectionView.cellForItem(at: NSIndexPath(item: currentIndex, section: 0) as IndexPath) as? ImagePreviewCell {
            if scrollView == cell.imageScrollView {
                return cell.imageView
            }
        }
        return self
    }
    
    func setZoom() {
        guard currentIndex != nil else {
            return
        }
        
        if let cell = imageCollectionView.cellForItem(at: NSIndexPath(item: currentIndex, section: 0) as IndexPath) as? ImagePreviewCell {
            UIView.animate(withDuration: 0.3, animations: {
                if cell.imageScrollView.zoomScale == 1.0 {
                    cell.imageScrollView.zoomScale = 2.0
                    self.imageCollectionView.isScrollEnabled = false
                } else {
                    cell.imageScrollView.zoomScale = 1.0
                    self.imageCollectionView.isScrollEnabled = true
                }
                self.setCaptionView(caption: self.captionArrayValue(index: self.currentIndex), cell: cell)
            })
            
        }
    }
    
    func setCaptionView(caption:String, cell:ImagePreviewCell) {
        print(caption)
        guard caption.length > 0 else {
            cell.captionTextView.isHidden = true
            return
        }
        cell.captionTextView.isHidden = true
        if self.isTopBottomViewHidden == false {
            if cell.imageScrollView.zoomScale == 1.0 {
                cell.captionTextView.isHidden = false
            }
        }
    }
    
    func captionArrayValue(index: Int) -> String {
        guard captionArray.count > 0 else {
            return ""
        }
        guard index < captionArray.count else {
            return ""
        }
        return captionArray[index].caption_data!
    }
    

}
