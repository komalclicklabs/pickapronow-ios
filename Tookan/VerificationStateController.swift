//
//  VerificationStateController.swift
//  Tookan
//
//  Created by cl-macmini-45 on 31/05/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class VerificationStateController: UIViewController {
    
    @IBOutlet var processStepsTextView: UITextView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subTitleLabel: UILabel!
    
    var userStateView:UserStateView!
    let userStateOrigin:CGFloat = SCREEN_SIZE.height - 220
    var currentUserState:DRIVER_STATE = DRIVER_STATE.otp_verified
    var stateTitle = ""
    var stateSubtitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUserStateView), name: NSNotification.Name(rawValue: OBSERVER.signupVerificationPushReceived), object: nil)
        self.updateUserStateView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBSERVER.signupVerificationPushReceived), object: nil)
    }
    
    func updateUserStateView() {
        currentUserState = DRIVER_STATE(rawValue: Singleton.sharedInstance.fleetDetails.registrationStatus!)!
        self.processStepsTextView.isHidden = true
        self.view.backgroundColor = UIColor.black
        self.setUserStateView()
        self.setTitleLabels()
    }
    
    
    func setTitleLabels() {
        /*================ Sign up title Label Design======================*/
        self.titleLabel.font = UIFont(name: UIFont().MontserratExtraLight, size: FONT_SIZE.large)
        self.titleLabel.textColor =  UIColor.white
        self.titleLabel.text = stateTitle
        self.titleLabel.setLetterSpacing(value: 0.5)
        
        self.subTitleLabel.font = UIFont(name: UIFont().MontserratExtraLight, size: FONT_SIZE.regular)
        self.subTitleLabel.textColor =  UIColor.white
        self.subTitleLabel.text = stateSubtitle
    }
    
    func setTextView() {
        self.processStepsTextView.isHidden = false
        self.processStepsTextView.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        self.processStepsTextView.textColor = UIColor.white
        var processSteps = ""
        for i in 0..<(Singleton.sharedInstance.fleetDetails.signup_template_data?.count)! {
            let signupTemplateData = (Singleton.sharedInstance.fleetDetails.signup_template_data?[i])! as CustomFields
            if signupTemplateData.appSide == APP_SIDE.write {
                let step = "-\t\(signupTemplateData.label.replacingOccurrences(of: "_", with: " "))\n"
                processSteps = "\(processSteps)\(step)"
            }
        }
        self.processStepsTextView.text = processSteps
        self.processStepsTextView.scrollsToTop = true
    }
    
    func setUserStateView() {
        if userStateView != nil  {
            self.userStateView.removeFromSuperview()
            self.userStateView = nil
        }
        
        self.userStateView = UINib(nibName: NIB_NAME.userStateView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UserStateView
        self.userStateView.frame = CGRect(x: 5, y: userStateOrigin, width: SCREEN_SIZE.width - 10, height: SCREEN_SIZE.height)
        self.userStateView.layer.cornerRadius = 20.0
        self.userStateView.transform = CGAffineTransform(translationX: 0, y: SCREEN_SIZE.height / 2)
        self.view.addSubview(userStateView)
        self.setUserState()
    }
    
    func setUserState() {
        currentUserState = DRIVER_STATE(rawValue: Singleton.sharedInstance.fleetDetails.registrationStatus!)!
        switch currentUserState {
        case .otp_verified:
            self.setStartVerificationProcess()
            break
        case .resubmit_verificaton:
            self.setReVerficationProcess()
        case .verified:
            self.setVerifiedState()
        case .rejected:
            self.setVerficationFailedState()
        case .verification_pending:
            self.setBeingVerifiedState()
        case .acknowledged:
            UserDefaults.standard.set(Singleton.sharedInstance.getVersion(), forKey: USER_DEFAULT.appVersion)
            Singleton.sharedInstance.gotoHomeStoryboard()
        default:
            break
        }
        self.animateUserStateView()
    }
    
    func setStartVerificationProcess() {
        self.setTextView()
        stateTitle = TEXT.ADDITIONAL_INFORMATION
        stateSubtitle = TEXT.VERIFICATION_PROCESS_TEXT
        self.userStateView.contactLabel.isHidden = true
        self.userStateView.setStartVerificationProcess()
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.successfulVerification), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.restartVerificationProcess), for: .touchUpInside)
        self.userStateView.firstButton.addTarget(self, action: #selector(self.continueVerficationProcess), for: .touchUpInside)
        self.userStateView.secondButton.addTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
    }
    
    func setReVerficationProcess() {
        self.userStateView.firstButton.isEnabled = true
        self.processStepsTextView.isHidden = true
        stateTitle = TEXT.REVERIFICATION_TEXT
        if (Singleton.sharedInstance.fleetDetails.admin_action_message?.length)! > 0 {
            stateSubtitle = Singleton.sharedInstance.fleetDetails.admin_action_message!
        } else {
            stateSubtitle = TEXT.REVERIFICATION_MESSAGE
        }
        self.userStateView.contactLabel.isHidden = true
        self.userStateView.setReVerificationProcess()
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.successfulVerification), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.continueVerficationProcess), for: .touchUpInside)
        self.userStateView.firstButton.addTarget(self, action: #selector(self.restartVerificationProcess), for: .touchUpInside)
        self.userStateView.secondButton.addTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
    }
    
    func setVerifiedState() {
        self.userStateView.firstButton.isEnabled = true
        self.processStepsTextView.isHidden = true
        stateTitle = TEXT.SUCCESSFULL_ACCOUNT_VERIFICATION
        stateSubtitle = TEXT.SUCCESSFULL_ACCOUNT_MESSAGE
        self.userStateView.contactLabel.isHidden = true
        self.userStateView.setVerifiedProcess()
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.continueVerficationProcess), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.restartVerificationProcess), for: .touchUpInside)
        self.userStateView.firstButton.addTarget(self, action: #selector(self.successfulVerification), for: .touchUpInside)
        self.userStateView.secondButton.addTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
    }
    
    func setVerficationFailedState() {
        self.userStateView.firstButton.isEnabled = true
        self.processStepsTextView.isHidden = true
        stateTitle = TEXT.VERIFICATION_FAILED
        if (Singleton.sharedInstance.fleetDetails.admin_action_message?.length)! > 0 {
            stateSubtitle = Singleton.sharedInstance.fleetDetails.admin_action_message!
        } else {
            stateSubtitle = TEXT.VERIFICATION_FAILED_MESSAGE
        }
        
        self.userStateView.setFailedProcess()
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.successfulVerification), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.continueVerficationProcess), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.restartVerificationProcess), for: .touchUpInside)
        self.userStateView.firstButton.addTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
    }
    
    func setBeingVerifiedState() {
        self.userStateView.firstButton.isEnabled = false
        self.userStateView.refreshButton.isHidden = true
        self.processStepsTextView.isHidden = true
        stateTitle = TEXT.BEING_VERIFIED
        stateSubtitle = ""
        self.userStateView.setBeingProcessed()
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.successfulVerification), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.continueVerficationProcess), for: .touchUpInside)
        self.userStateView.firstButton.removeTarget(self, action: #selector(self.restartVerificationProcess), for: .touchUpInside)
        self.userStateView.firstButton.addTarget(self, action: #selector(self.logoutAction), for: .touchUpInside)
        self.perform(#selector(self.showRefreshButton), with: nil, afterDelay: 3.0)
    }
    
    func showRefreshButton() {
        self.userStateView.refreshButton.setTitle(TEXT.REFRESH, for: .normal)
        self.userStateView.refreshButton.isHidden = false
        self.userStateView.activityIndicator.stopAnimating()
        self.userStateView.firstButton.isEnabled = true
        self.userStateView.refreshButton.addTarget(self, action: #selector(self.refreshAction), for: .touchUpInside)
    }
    
    func refreshAction() {
        self.userStateView.refreshButton.isHidden = true
        self.userStateView.activityIndicator.startAnimating()
        self.userStateView.firstButton.isEnabled = false
        let deviceToken =  UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) != nil ? UserDefaults.standard.object(forKey: USER_DEFAULT.deviceToken) as? String : "No deviceToken"
        let location = LocationTracker.sharedInstance().getLastLocation()
        var params = [String:Any]()
        params["access_token"] = Singleton.sharedInstance.getAccessToken()
        params["device_type"] = DEVICE_TYPE
        params["device_token"] = "\(deviceToken!)"
        params["latitude"] = "\((location?.coordinate.latitude)!)"
        params["longitude"] = "\((location?.coordinate.longitude)!)"
        params["device_os"] = "iOS"
        params["store_version"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        params["device_desc"] = "\(DeviceInfo.oSVersion) \(DeviceInfo.deviceType) \((DeviceInfo.deviceName as String))" //+ DeviceInfo.deviceType + " " + (DeviceInfo.deviceName as String)
        params["app_version"] = app_version
        params["bat_lvl"] = "\(UIDevice.current.batteryLevel * 100)"
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.access_token_login, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                
                if isSucceeded == true {
                    if let status = response["status"] as? Int {
                        switch status {
                        case STATUS_CODES.SHOW_DATA:
                            if let data = response["data"] as? [String:Any] {
                                if let items = data["fleet_info"] as? NSArray {
                                    for item in items {
                                        if let fleetInfo = item as? NSDictionary{
                                            Singleton.sharedInstance.fleetDetails = FleetInfoDetails(json: fleetInfo)
                                        }
                                    }
                                }
                                self.setUserState()
                                self.setTitleLabels()
                            }
                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            self.userStateView.activityIndicator.stopAnimating()
                            self.userStateView.refreshButton.isHidden = false
                            self.userStateView.firstButton.isEnabled = true
                            let alert = UIAlertController(title: "", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                            let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                                DispatchQueue.main.async(execute: { () -> Void in
                                    Auxillary.logoutFromDevice()
                                    NotificationCenter.default.removeObserver(self)
                                })
                            })
                            alert.addAction(actionPickup)
                            self.present(alert, animated: true, completion: nil)
                            break
                        default:
                            self.userStateView.activityIndicator.stopAnimating()
                            self.userStateView.refreshButton.isHidden = false
                            self.userStateView.firstButton.isEnabled = true
                            Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                        }
                    }
                } else {
                    self.userStateView.activityIndicator.stopAnimating()
                    self.userStateView.refreshButton.isHidden = false
                    self.userStateView.firstButton.isEnabled = true
                    Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                }
            }
        }
    }
    
    func animateUserStateView() {
        UIView.animate(withDuration: 1.0, delay: 0.5, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveLinear, animations: {
            self.userStateView.transform = CGAffineTransform.identity
        }, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func continueVerficationProcess() {
        if Singleton.sharedInstance.fleetDetails.isTeamEnabled == 1 {
            self.gotoSignupTemplateController()
        } else {
            self.gotoSignupTemplateControllerVC()
        }
//        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.signupTemplateController) as! SignupTemplateController
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func restartVerificationProcess() {
        if Singleton.sharedInstance.fleetDetails.isTeamEnabled == 1 {
            self.gotoSignupTemplateController()
        } else {
            self.gotoSignupTemplateControllerVC()
        }
        
//        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.signupTemplateController) as! SignupTemplateController
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoSignupTemplateController() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        let params:[String:Any] = ["access_token": Singleton.sharedInstance.getAccessToken()]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.get_teams_tags_onsignup, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    print(response)
                    if let status = response["status"] as? Int {
                        switch status {
                        case STATUS_CODES.SHOW_DATA:
                            //self.refreshAction()
                            if let data = response["data"] as? [String:Any] {
                                if let tags = data["tags"] as? [String] {
                                    Singleton.sharedInstance.fleetDetails.tagsArray = tags
                                }
                                if let teams = data["teams"] as? [String] {
                                    Singleton.sharedInstance.fleetDetails.teamsArray = teams
                                }
                                self.gotoSignupTemplateControllerVC()
                                
                            }
                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            let alert = UIAlertController(title: "", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                            let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                                DispatchQueue.main.async(execute: { () -> Void in
                                    Auxillary.logoutFromDevice()
                                    NotificationCenter.default.removeObserver(self)
                                })
                            })
                            alert.addAction(actionPickup)
                            self.present(alert, animated: true, completion: nil)
                            break
                        default:
                            Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                        }
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                }
            }
        }
    }
    
    func gotoSignupTemplateControllerVC() {
        let controller  = self.storyboard?.instantiateViewController(withIdentifier:STORYBOARD_ID.signupTemplateController) as! SignupTemplateController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func successfulVerification() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        let params:[String:Any] = ["access_token": Singleton.sharedInstance.getAccessToken()]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.acknowledge_signup_verification, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSucceeded, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSucceeded == true {
                    if let status = response["status"] as? Int {
                        switch status {
                        case STATUS_CODES.SHOW_DATA:
                            if let data = response["data"] as? [String:Any] {
                                if let items = data["fleet_info"] as? NSArray {
                                    for item in items {
                                        if let fleetInfo = item as? NSDictionary{
                                            Singleton.sharedInstance.fleetDetails = FleetInfoDetails(json: fleetInfo)
                                        }
                                    }
                                }
                                if let registration_status = data["registration_status"] as? String {
                                    Singleton.sharedInstance.fleetDetails.registrationStatus = Int(registration_status)!
                                } else if let registration_status = data["registration_status"] as? Int {
                                    Singleton.sharedInstance.fleetDetails.registrationStatus = registration_status
                                }
                                UserDefaults.standard.set(Singleton.sharedInstance.getVersion(), forKey: USER_DEFAULT.appVersion)
                                Singleton.sharedInstance.gotoHomeStoryboard()
                            }

                            break
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            let alert = UIAlertController(title: "", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                            let actionPickup = UIAlertAction(title: TEXT.OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                                DispatchQueue.main.async(execute: { () -> Void in
                                    Auxillary.logoutFromDevice()
                                    NotificationCenter.default.removeObserver(self)
                                })
                            })
                            alert.addAction(actionPickup)
                            self.present(alert, animated: true, completion: nil)
                            break
                        default:
                            Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                        }
                    }
                } else {
                    Singleton.sharedInstance.showErrorMessage(error: (response["message"] as? String)!, isError: .error)
                }
            }
        }
    }
    
    func logoutAction() {
        self.sendRequestForLogout()
    }
    
    func sendRequestForLogout() {
        if let accessToken = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as? String {
            ActivityIndicator.sharedInstance.showActivityIndicator((self.navigationController?.visibleViewController)!)
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.logout, params: [
                "access_token":accessToken as AnyObject], httpMethod: HTTP_METHOD.POST, receivedResponse: { (isSucceeded, response) in
                    DispatchQueue.main.async {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                        if isSucceeded == true {
                            switch(response["status"] as! Int) {
                            case STATUS_CODES.SHOW_DATA, STATUS_CODES.INVALID_ACCESS_TOKEN:
                                NotificationCenter.default.removeObserver(self)
                                Auxillary.logoutFromDevice()
                                break
                            default:
                                Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                                break
                            }
                        } else {
                            Singleton.sharedInstance.showErrorMessage(error: response["message"] as! String, isError: .error)
                        }
                    }
            })
        } else {
            NotificationCenter.default.removeObserver(self)
            Auxillary.logoutFromDevice()
        }
    }

}
