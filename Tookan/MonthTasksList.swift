//
//  MonthTasksList.swift
//  Tookan
//
//  Created by Click Labs on 7/31/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
class MonthTasksList: NSObject {
    var date: String!
    var incompleteTasks: Int!
    var totalTasks: Int!
    var completedTasks:Int!
    
    init(json: NSDictionary){
        
        if let date = json["date"] as? String {
            if date.isEmpty {
                self.date = nil
            }
            self.date = date
        }
        if let incompleteTasks = json["incomplete_tasks"] as? Int {
            if incompleteTasks == 0 {
                self.incompleteTasks = nil
            }
            self.incompleteTasks = incompleteTasks
        }
        if let totalTasks = json["total_tasks"] as? Int {
            if totalTasks == 0 {
                self.totalTasks = nil
            }
            self.totalTasks = totalTasks
        }
        
        if let value = json["completed_tasks"] as? Int {
            self.completedTasks = value
        } else if let value = json["completed_tasks"] as? String {
            self.completedTasks = Int(value)
        } else {
            self.completedTasks = 0
        }
    }
}
