//
//  NotesCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 24/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol NotesDelegate {
    func updateDescriptionCellHeight(descriptionText:String, section:Int, index:Int)
    func notesViewShouldReturn(index:Int, section:Int)
    func notesShouldBeginEditing(textView:UITextView, section:Int)
    func notesShouldStartEditing(textView:UITextView, section:Int)
}

class NotesCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var descriptionView: UITextView!
    @IBOutlet var placeholderLabel: UILabel!
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var line: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var delegate:NotesDelegate!
    var sectionTag:Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*--------- Header Label -------------*/
        headerLabel.textColor = COLOR.HEADER_LABEL_COLOR
        headerLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.small)
        
        /*------------- Placeholder label -----------*/
        placeholderLabel.textColor = COLOR.LIGHT_COLOR
        placeholderLabel.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        
        /*---------- Text View -------------*/
        descriptionView.textColor = COLOR.TEXT_COLOR
        descriptionView.font = UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)
        descriptionView.delegate = self
        descriptionView.tintColor = COLOR.themeForegroundColor
        
        /*--------- UnderLine ------------*/
        line.backgroundColor = COLOR.LINE_COLOR
        
        self.actionButton.setImage(#imageLiteral(resourceName: "doneSmall").withRenderingMode(.alwaysTemplate), for: .normal)
        self.actionButton.tintColor = COLOR.themeForegroundColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: UITEXTVIEW DELEGATE
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty == true {
            placeholderLabel.isHidden = false
            if self.sectionTag == BARCODE_TAG {
                self.actionButton.setImage(#imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate), for: .normal)
            } else if self.sectionTag == SECTION_TAG {
                self.actionButton.setImage(#imageLiteral(resourceName: "remove"), for: .selected)
                self.actionButton.isSelected = true
            } else {
                self.actionButton.setImage(#imageLiteral(resourceName: "barcodeCamera").withRenderingMode(.alwaysTemplate), for: .normal)
                self.actionButton.isSelected = false
            }
        } else {
            placeholderLabel.isHidden = true
            self.actionButton.setImage(#imageLiteral(resourceName: "doneSmall").withRenderingMode(.alwaysTemplate), for: .normal)
            if self.sectionTag == BARCODE_TAG {
                
            } else {
                self.actionButton.isSelected = false
            }
        }
        delegate.updateDescriptionCellHeight(descriptionText: textView.text, section: self.sectionTag, index: textView.tag)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            //delegate.notesViewShouldReturn(index:textView.tag, section: self.sectionTag)
            return false
        }
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        guard Singleton.sharedInstance.hasTaskStarted(jobStatus: Singleton.sharedInstance.selectedTaskDetails.jobStatus, arrivedValue: Singleton.sharedInstance.selectedTaskDetails.arrivedOptionalField.value!) == true else {
            return false
        }
        if textView.text.length > 0 {
            delegate.notesShouldBeginEditing(textView: textView, section: self.sectionTag)
        } else{
            delegate.notesShouldStartEditing(textView: textView, section: self.sectionTag)
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard textView.text.length > 0 else {
            return
        }
        delegate.notesViewShouldReturn(index:textView.tag, section: self.sectionTag)
    }
    
}
