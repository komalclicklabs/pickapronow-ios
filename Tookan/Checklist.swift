//
//  Checklist.swift
//  Tookan
//
//  Created by cl-macmini-45 on 11/10/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class ChecklistData:NSObject, NSCoding {
    var check:Bool? = false
    var check_id:Int? = 0
    var value:String? = ""
    var uploading:Bool? = false
    
    required init(coder aDecoder: NSCoder) {
        check = aDecoder.decodeObject(forKey: "check") as? Bool
        check_id = aDecoder.decodeObject(forKey: "check_id") as? Int
        value = aDecoder.decodeObject(forKey: "value") as? String
        uploading = aDecoder.decodeObject(forKey: "uploading") as? Bool
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(check, forKey: "check")
        aCoder.encode(check_id, forKey: "check_id")
        aCoder.encode(value, forKey: "value")
        aCoder.encode(uploading, forKey: "uploading")
    }
    
    init(json:[String:Any]) {
        if let data = json["check"] as? String {
            if data == "true" || data == "1" {
                self.check = true
            } else {
                self.check = false
            }
        } else if let data = json["check"] as? Int {
            if data == 1 {
                self.check = true
            } else {
                self.check = false
            }
        } else if let data = json["check"] as? Bool {
            self.check = data
        }
        
        if let data = json["value"] as? String {
            self.value = data
        }
        
        if let data = json["id"] as? String {
            self.check_id = Int(data)!
        } else if let data = json["id"] as? Int {
            self.check_id = data
        }
    }
}
