//
//  JobListCell.swift
//  Tookan
//
//  Created by cl-macmini-45 on 15/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class JobListCell: UITableViewCell {

    
    @IBOutlet var topPatternLine: UIView!
    @IBOutlet var rightArrow: UIImageView!
    @IBOutlet var dot: UIView!
    @IBOutlet var patternLine: UIView!
    @IBOutlet var firstLine: UILabel!
    @IBOutlet var secondLine: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        patternLine.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "iconPattern"))
        topPatternLine.isHidden = true
        
    }

    func setDotView(indexPath:IndexPath, filteredTaskListArray:[[TasksAssignedToDate]], isRelatedTask:Bool) {
        var dotColor:UIColor!
        var borderColor:UIColor!
        var isPatterHidden:Bool!
        if isRelatedTask == true {
            let statusColor = Singleton.sharedInstance.getColor(jobStatus: filteredTaskListArray[indexPath.section][indexPath.row].jobStatus)
            if indexPath.row == 0 {
                dotColor = statusColor
                borderColor = statusColor
                isPatterHidden = false
                topPatternLine.isHidden = true
                self.topPatternLine.backgroundColor = statusColor
            } else if indexPath.row == filteredTaskListArray[indexPath.section].count - 1 {
                dotColor = statusColor
                borderColor = statusColor
                isPatterHidden = true
                topPatternLine.isHidden = false
                self.topPatternLine.backgroundColor = Singleton.sharedInstance.getColor(jobStatus: filteredTaskListArray[indexPath.section][indexPath.row - 1].jobStatus)
            }  else {
                dotColor = UIColor.white
                borderColor = statusColor
                isPatterHidden = false
                topPatternLine.isHidden = false
                self.topPatternLine.backgroundColor = Singleton.sharedInstance.getColor(jobStatus: filteredTaskListArray[indexPath.section][indexPath.row - 1].jobStatus)
            }
            self.patternLine.backgroundColor = statusColor
            
        } else {
            if filteredTaskListArray[indexPath.section].count > 1 {
                if indexPath.row == 0 {
                    dotColor = UIColor.black
                    borderColor = UIColor.black
                    isPatterHidden = false
                } else if indexPath.row == filteredTaskListArray[indexPath.section].count - 1 {
                    dotColor = UIColor.white
                    borderColor = UIColor.black
                    isPatterHidden = true
                } else {
                    dotColor = UIColor.black
                    borderColor = UIColor.black
                    isPatterHidden = false
                }
            } else {
                dotColor = UIColor.black
                borderColor = UIColor.black
                isPatterHidden = true
            }
        }
        self.dot.backgroundColor = dotColor
        self.dot.layer.cornerRadius = 4.5
        self.dot.layer.borderWidth = 1.0
        self.dot.layer.borderColor = borderColor.cgColor
        self.dot.layer.masksToBounds = true
        self.patternLine.isHidden = isPatterHidden
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
