//
//  TeamDetails.swift
//  Tookan
//
//  Created by Click Labs on 7/8/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation

class AssignedTeamDetails: NSObject, NSCoding {
    
    var teamId: Int? = 0
    var teamName: String! = ""
    
    required init(coder aDecoder: NSCoder) {
        teamId = aDecoder.decodeObject(forKey: "teamId") as? Int
        teamName = aDecoder.decodeObject(forKey: "teamName") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(teamId, forKey: "teamId")
        aCoder.encode(teamName, forKey: "teamName")
    }
    
    
    init(json: NSDictionary){
        
        if let teamId = json["team_id"] as? NSNumber {
            //if "\(teamId)".isEmpty {
            // self.teamId = 0
            //}
            self.teamId = Int(teamId)
        } else if let teamId = json["team_id"] as? String {
            if let intValue = Int(teamId) {
                self.teamId = intValue
            }
        }
        
        if let teamName = json["team_name"] as? String {
//            if teamName.isEmpty{
//                self.teamName = nil
//            }
            self.teamName = teamName
        }
    }
}
