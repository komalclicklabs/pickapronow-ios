//
//  JobListTable.swift
//  Tookan
//
//  Created by cl-macmini-45 on 15/02/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

protocol JobListDelegate {
    func gotoTaskDetailPage(index:Int, startStatus:Int, section:Int)
    func gotoTaxiController(index:Int)
    func slideButtonAction()
}

class JobListTable: UIView,UITableViewDelegate,UITableViewDataSource, CAAnimationDelegate {

    @IBOutlet var noTaskImageView: UIImageView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var createTaskButton: UIButton!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var slideUpIcon: UIButton!
    @IBOutlet var backgroundLabel: UILabel!
    @IBOutlet var taskListTable: UITableView!
   
    var filteredTasksListArray = [[TasksAssignedToDate]]()
    var delegate:JobListDelegate!
    let defaultBottomConstraint:CGFloat = 120.0
    var gradientLayer:CAGradientLayer!
    var toColors = [CGColor]()//: [AnyObject] = [UIColor.cyan.cgColor, UIColor.blue.cgColor, UIColor.yellow.cgColor, UIColor.green.cgColor, UIColor.red.cgColor, UIColor.orange.cgColor]
    var fromColor = [CGColor]()//[UIColor.blue.cgColor, UIColor.yellow.cgColor, UIColor.green.cgColor, UIColor.red.cgColor, UIColor.orange.cgColor, UIColor.cyan.cgColor]
    var stopGradientAnimation = false
    let footerHeight:CGFloat = 15.0
    
    override func awakeFromNib() {
        self.taskListTable.delegate = self
        self.taskListTable.dataSource = self
        self.taskListTable.register(UINib(nibName: NIB_NAME.jobListCell, bundle: nil), forCellReuseIdentifier: NIB_NAME.jobListCell)
        self.taskListTable.rowHeight = UITableViewAutomaticDimension
        self.taskListTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    
    func setGradientLayer() {
        
       self.fromColor = [UIColor(red: 70/255, green: 148/255, blue: 246/255, alpha: 1.0).cgColor,
                         UIColor(red: 88/255, green: 160/255, blue: 245/255, alpha: 1.0).cgColor,
                         UIColor(red: 107/255, green: 170/255, blue: 249/255, alpha: 1.0).cgColor,
                         UIColor(red: 125/255, green: 180/255, blue: 247/255, alpha: 1.0).cgColor,
                         UIColor(red: 145/255, green: 191/255, blue: 250/255, alpha: 1.0).cgColor,
                         UIColor(red: 162/255, green: 202/255, blue: 251/255, alpha: 1.0).cgColor,
                         UIColor(red: 181/255, green: 213/255, blue: 251/255, alpha: 1.0).cgColor,
                         UIColor(red: 199/255, green: 222/255, blue: 253/255, alpha: 1.0).cgColor]
       
 //       var hue = 0.0
//        while hue <= 360.0 {
//            let value = 1.0 * Double(hue)
//            let updatedValue = Double(value / 360)
//            let color = UIColor(hue: CGFloat(updatedValue), saturation: 1.0, brightness: 1.0, alpha: 1.0)
//            self.fromColor.append(color.cgColor)
//            hue += 5
//        }
        var shiftedColors = self.fromColor
        shiftedColors.removeLast()
        shiftedColors.insert(self.fromColor.last!, at: 0)
        self.toColors = shiftedColors
        
        if self.gradientLayer != nil {
            self.gradientLayer.removeAllAnimations()
            self.gradientLayer.removeFromSuperlayer()
            self.gradientLayer = nil
        }
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: self.slideUpIcon.frame.height, width: SCREEN_SIZE.width - 10, height: 3)
        gradientLayer.colors = self.fromColor
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5) //CGPointMake(0.0, 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5) //CGPointMake(1.0, 0.5)
        self.layer.addSublayer(gradientLayer)
        self.animateLayer()
    }
    
    func animateLayer(){
        self.gradientLayer.colors = self.toColors
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = self.fromColor
        animation.toValue = toColors
        animation.duration = 0.08
        animation.isRemovedOnCompletion = true
        animation.fillMode = kCAFillModeForwards
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.delegate = self
        self.gradientLayer.add(animation, forKey:"animateGradient")
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if self.stopGradientAnimation == false {
            var shiftedColors = self.fromColor
            shiftedColors.removeLast()
            shiftedColors.insert(self.fromColor.last!, at: 0)
            self.toColors = shiftedColors
            self.fromColor = self.gradientLayer.colors as! [CGColor]
            animateLayer()
        } else {
            guard self.gradientLayer != nil else {
                return
            }
            self.gradientLayer.removeAllAnimations()
            self.gradientLayer.removeFromSuperlayer()
            self.gradientLayer = nil
            self.fromColor.removeAll(keepingCapacity: false)
            self.toColors.removeAll(keepingCapacity: false)
        }
    }
    
    
    func setBackgroundView() {
        self.backgroundView.layer.cornerRadius = 14.0
        self.backgroundView.isHidden = false
        self.slideUpIcon.isHidden = true
        
        var headingText = ""
        var subText = ""
        if Singleton.sharedInstance.isFilterApplied() == true {
            self.noTaskImageView.image = #imageLiteral(resourceName: "sadFace")
            headingText = TEXT.NO_RESULT_FOUND
            subText = TEXT.NO_FILTER_TASK
        } else {
            headingText = TEXT.ALL_CLEAR
            subText = TEXT.NO_TASK_TODAY
            self.noTaskImageView.image = #imageLiteral(resourceName: "sun")
        }
        
        /*==================== Label =====================*/
        let attributedString = NSMutableAttributedString(string: headingText, attributes: [NSFontAttributeName:UIFont(name: UIFont().MontserratSemiBold, size: FONT_SIZE.large)!])
        let subAttributedText = NSMutableAttributedString(string: "\n\(subText)", attributes: [NSFontAttributeName:UIFont(name: UIFont().MontserratLight, size: FONT_SIZE.regular)!])
        attributedString.append(subAttributedText)
        attributedString.setSpacingAttribute(value: 0.4)
        self.backgroundLabel.attributedText = attributedString
        
        /*==================== Create Task Button ===================*/
//        self.createTaskButton.backgroundColor = COLOR.themeForegroundColor
//        self.createTaskButton.setTitleColor(COLOR.positiveButtonTitleColor, for: .normal)
//        self.createTaskButton.titleLabel?.font = UIFont(name: UIFont().MontserratRegular, size: FONT_SIZE.regular)
//        self.createTaskButton.setTitle("+ " + TEXT.CREATE_TASK, for: .normal)
//        self.createTaskButton.layer.cornerRadius = self.createTaskButton.frame.height / 2
//        self.createTaskButton.layer.masksToBounds = true
//        if(UserDefaults.standard.integer(forKey: USER_DEFAULT.isCreateTaskAvailable) == 1) {
//            self.createTaskButton.isHidden = false
//        } else {
//            self.createTaskButton.isHidden = true
//        }
    }
    @IBAction func slideUpAction(_ sender: Any) {
        delegate.slideButtonAction()
    }
    
    //MARK: UITableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if filteredTasksListArray.count > 0 {
            self.taskListTable.backgroundView = nil
            self.backgroundView.isHidden = true
        }
        return filteredTasksListArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredTasksListArray[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width - 10, height: footerHeight))
        let line = Auxillary.drawSingleLine(15, yOrigin: 0, lineWidth: footerView.frame.width - 15, lineHeight: 1)
        footerView.addSubview(line)
        line.center = CGPoint(x: line.center.x, y: footerView.center.y)
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return footerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.jobListCell) as! JobListCell
        
        let model = JobModel()
        cell.firstLine.attributedText = model.setFirstLine(jobDetails: filteredTasksListArray[indexPath.section][indexPath.row], forNewTask:false)
        cell.secondLine.attributedText = model.setSecondLine(jobDetails: filteredTasksListArray[indexPath.section][indexPath.row])
        cell.setDotView(indexPath: indexPath, filteredTaskListArray: filteredTasksListArray, isRelatedTask: false)
        if cell.isSelected == true {
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor.red //UIColor(red: 70/255, green: 149/255, blue: 246/255, alpha: 0.1)
            cell.selectedBackgroundView = backgroundView
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section < filteredTasksListArray.count else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
            return
        }
        
        guard indexPath.row < filteredTasksListArray[indexPath.section].count else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
            return
        }
        
        guard filteredTasksListArray[indexPath.section][indexPath.row].appOptionalFieldArray!.count > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
            return
        }
        guard filteredTasksListArray[indexPath.section][indexPath.row].appOptionalFieldArray!.count > 0 else {
            Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
            return
        }
        if filteredTasksListArray[indexPath.section][indexPath.row].vertical == VERTICAL.TAXI.rawValue {
            Singleton.sharedInstance.selectedTaskDetails = filteredTasksListArray[indexPath.section][indexPath.row]
            
            guard Singleton.sharedInstance.selectedTaskDetails.customFieldArray != nil && (Singleton.sharedInstance.selectedTaskDetails.customFieldArray?.count)! > 0 else {
                Singleton.sharedInstance.showErrorMessage(error: ERROR_MESSAGE.INSUFFICIENT_DATA, isError: .error)
                return
            }
            switch Singleton.sharedInstance.selectedTaskDetails.jobStatus {
            case JOB_STATUS.successful, JOB_STATUS.canceled, JOB_STATUS.failed, JOB_STATUS.declined:
                _ = delegate.gotoTaskDetailPage(index:indexPath.row, startStatus: 0, section: indexPath.section)
                break
            default:
                delegate.gotoTaxiController(index: 0)
                break
            }
        } else {
            _ = delegate.gotoTaskDetailPage(index:indexPath.row, startStatus: 0, section: indexPath.section)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if let cell  = tableView.cellForRow(at: indexPath) as? JobListCell {
            cell.contentView.backgroundColor = COLOR.CELL_HIGHLIGHT_COLOR
        }
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if let cell  = tableView.cellForRow(at: indexPath) as? JobListCell {
            cell.contentView.backgroundColor = UIColor.clear
        }
    }
}
