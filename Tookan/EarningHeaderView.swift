//
//  EarningHeaderView.swift
//  Tookan
//
//  Created by cl-macmini-45 on 20/06/17.
//  Copyright © 2017 Click Labs. All rights reserved.
//

import UIKit

class EarningHeaderView: UIView {

    @IBOutlet var rightArrow: UIImageView!
    @IBOutlet var totalPayoutButton: UIButton!
    @IBOutlet weak var barGraphCollectionView: UICollectionView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var rupeesScaleCollectionView: UICollectionView!
    
    let model = AvailabilityModel()
    let heightForCollectionView:CGFloat = 233.0
    let numberOfPriceSlots = 5
    var ceilValueForGraphWidth:CGFloat!
    var selectedGraphIndex = -1
    
    let minPriceValue:Float = 0
    var valueForBarGraph:Float = 100
    var leadingForCollectionView:CGFloat = 45.0
    var trailingForCollectionView:CGFloat = 10.0
    var currentWeekDate:Date!

    override func awakeFromNib() {
        self.rightArrow.isHidden = true
        self.setLeftButton()
        self.setRightButton()
    }
    
    func setCenterLabel(startDate:String, endDate:String,currentDateIndex:Int)   {
        let month = model.getDateSlotForRepeatPopup(indexPath: currentDateIndex + 6, format: "MMMM")
        let year = model.getDateSlotForRepeatPopup(indexPath: currentDateIndex + 6, format: "YYYY")
        self.centerLabel.text = "\(startDate)-\(endDate) \(month) \(year)"
        self.centerLabel.font = UIFont(name: UIFont().MontserratMedium, size: FONT_SIZE.medium)
        self.centerLabel.textColor = COLOR.LITTLE_LIGHT_COLOR
    }
    
    func setLeftButton() {
        self.leftButton.setImage(#imageLiteral(resourceName: "left_arrow_dark"), for: .normal)
    }
    
    func setRightButton() {
        self.rightButton.setImage(#imageLiteral(resourceName: "right_arrow_dark"), for: .normal)
    }
    
    func getDateSlotForNextPrevious(currentDateIndex:Int) -> String {
        print(currentDateIndex)
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
        let currentDate = self.currentWeekDate
        let newDate:Date = (calendar as NSCalendar).date(byAdding: .day, value: currentDateIndex, to: currentDate!, options: [])!
        dateFormatter.locale = NSLocale(localeIdentifier: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as! String) as Locale!//NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation:"UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: newDate)
    }
    
}


