//
//  SyncView.swift
//  Tookan
//
//  Created by CL-macmini45 on 6/3/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

class SyncView: UIView {

    @IBOutlet weak var syncImageView: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
  
    func startLoader(_ title:String, subtitle:String) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.alpha = 0
        DispatchQueue.main.async {
            (UIApplication.shared.delegate as! AppDelegate).window?.addSubview(self)
        }
        self.progressView.transform = CGAffineTransform(scaleX: 1.0, y: 3.0)
      //  self.activityIndicator.startAnimating()
        titleLabel.text = title
        subtitleLabel.text = subtitle
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1.0
        }
        RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.1))
    }
    
    func updateProgressValue(_ value:Float) {
        print("final Value = \(value)")
        self.progressView.setProgress(value, animated: true)
      //  dispatch_async(dispatch_get_main_queue()) {
            UIView.animate(withDuration: 1.0, animations: {
                self.progressView.layoutIfNeeded()
                self.layoutIfNeeded()
                RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.2))
            })
      //  }
    }
    
    func stopLoader() {
        //self.activityIndicator.stopAnimating()
        self.removeFromSuperview()
    }
    
    
}
