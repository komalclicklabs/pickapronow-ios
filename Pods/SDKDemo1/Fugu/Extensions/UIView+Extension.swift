//
//  UIViewController+Extension.swift
//  BRYXBanner
//
//  Created by cl-macmini-117 on 01/11/17.
//

import UIKit

extension UIView {
   var safeAreaInsetsForAllOS: UIEdgeInsets {
      var insets: UIEdgeInsets
      if #available(iOS 11.0, *) {
         insets = safeAreaInsets
      } else {
         insets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
      }
      return insets
   }
   
   static var safeAreaInsetOfKeyWindow: UIEdgeInsets {
      return UIApplication.shared.keyWindow?.safeAreaInsetsForAllOS ?? UIEdgeInsets()
   }
}
