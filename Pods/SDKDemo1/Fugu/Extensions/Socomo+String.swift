//
//  Socomo+String.swift
//  Fugu
//
//  Created by Gagandeep Arora on 08/09/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import Foundation

//class So_String: CustomStringConvertible {
//
//    var description: String = ""
//
//
//}
extension String {
//    func randomString(OfLength length: Int) -> String {
//        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
//        let len = UInt32(letters.length)
//
//        var randomString = ""
//
//        for _ in 0 ..< length {
//            let rand = arc4random_uniform(len)
//            var nextChar = letters.character(at: Int(rand))
//            randomString += NSString(characters: &nextChar, length: 1) as String
//        }
//        return randomString
//    }
//
//    func changeDateFormat(sourceFormat: String? = "yyyy-MM-dd HH:mm:ss", toFormat: String) -> String {
//        var dateString = self.replacingOccurrences(of: ".000Z", with: "")
//        if dateString.contains(".") == true {
//            dateString = (dateString.components(separatedBy: "."))[0]
//        }
//        dateString = dateString.replacingOccurrences(of: "T", with: " ")
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = sourceFormat
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        let date = dateFormatter.date(from: dateString)
//
//        dateFormatter.dateFormat = toFormat
//        dateFormatter.timeZone = NSTimeZone.local
//        if date != nil {
//            return dateFormatter.string(from: date!)
//        }
//
//        return ""
//    }
//
   func trimWhiteSpacesAndNewLine() -> String {
      let trimmedString = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
      return trimmedString
   }
   
   func removeNewLine() -> String {
      let newString = self.replacingOccurrences(of: "\n", with: " ")
      return newString
   }
   
}
