//
//  OutgoingImageCell.swift
//  Fugu
//
//  Created by Gagandeep Arora on 31/07/17.
//  Copyright © 2017 Socomo Technologies Private Limited. All rights reserved.
//

import UIKit

class OutgoingImageCell: So_TableViewCell {
    @IBOutlet weak var shadowView: So_UIView!
    @IBOutlet weak var mainContentView: So_UIView!
    @IBOutlet weak var thumbnailImageView: So_UIImageView!
    @IBOutlet weak var timeLabel: So_CustomLabel!
    @IBOutlet weak var readUnreadImageView: So_UIImageView!
    @IBOutlet weak var customIndicator: So_UIImageView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    let rotationAnimationKey = String().randomString(OfLength: 36)
    
    override func awakeFromNib() { super.awakeFromNib() }

    override func setSelected(_ selected: Bool,
                              animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        adjustShadow()
    }
    
    deinit { stopIndicatorAnimation() }
}

//MARK: - Configure Cell
extension OutgoingImageCell {
    func setupBoxBackground(messageType: Int) {
        mainContentView.backgroundColor = FuguConfig.shared.theme.outgoingChatBoxColor
        
//        switch messageType {
//        case MessageType.privateNote.rawValue:
//            mainContentView.backgroundColor = UIColor.privateMessageBoxColor
//        default:
//            mainContentView.backgroundColor = UIColor.outGoingMessageBoxColor
//        }
    }
    
    func adjustShadow() {
        shadowView.layoutIfNeeded()
        mainContentView.layoutIfNeeded()
        
        shadowView.layer.cornerRadius = self.mainContentView.layer.cornerRadius
        shadowView.clipsToBounds = true
        shadowView.backgroundColor = FuguConfig.shared.theme.chatBoxBorderColor//UIColor.makeColor(red: 234, green: 234, blue: 234, alpha: 0.3)
        //self.shadowView.roundCorner(cornerRect: .allCorners, cornerRadius: self.shadowView.layer.cornerRadius)
    }
    
    func resetPropertiesOfOutgoingCell() {
        backgroundColor = .clear
        selectionStyle = .none
        timeLabel.text = ""
        mainContentView.layer.cornerRadius = 5
        
        thumbnailImageView.image = nil
        thumbnailImageView.layer.cornerRadius = mainContentView.layer.cornerRadius
        thumbnailImageView.clipsToBounds = true
        
        readUnreadImageView.image = UIImage(named: "unreadMsgTick",
                                            in: bundle,
                                            compatibleWith: nil)
    }
    
    func configureCellOfOutGoingImageCell(resetProperties: Bool,
                                          userSubscribedValue: Int,
                                          chatMessageObject: [String: Any]) -> OutgoingImageCell {
        if resetProperties { resetPropertiesOfOutgoingCell() }
        
        let messageType = (chatMessageObject["message_type"] as? Int) ?? 1
        setupBoxBackground(messageType: messageType)
        
        readUnreadImageView.tintColor = nil
        if let messageReadStatus = chatMessageObject["message_status"] as? Int {
            if messageReadStatus == ReadUnReadStatus.none.rawValue {
                readUnreadImageView.image = FuguConfig.shared.theme.unsentMessageIcon
                if let tintColor = FuguConfig.shared.theme.unsentMessageTintColor {
                    readUnreadImageView.tintColor = tintColor
                }
            } else if messageReadStatus == ReadUnReadStatus.read.rawValue {
                readUnreadImageView.image = UIImage(named: "readMsgTick",
                                                    in: bundle,
                                                    compatibleWith: nil)
                if let tintColor = FuguConfig.shared.theme.readMessageTintColor {
                    readUnreadImageView.tintColor = tintColor
                }
            }
        }
        
        if let thumbnailUrl = chatMessageObject["thumbnail_url"] as? String,
            thumbnailUrl.characters.count > 0,
            let url = NSURL(string: thumbnailUrl) {
            setupIndicatorView(true)
            let placeHolderImage = UIImage(named: "placeholderImg",
                                           in: bundle,
                                           compatibleWith: nil)
            thumbnailImageView.so_setImage(withURL: url,
                                           placeHolder: placeHolderImage,
                                           completion: { (image, error) in
                                            self.setupIndicatorView(false)
                                            
            })
        } else if let imageFile = chatMessageObject["image_file"] as? String,
            imageFile.isEmpty == false,
            let fuguImagePath = FuguDefaults.fuguImagesDirectory()?.path {
            fuguDelay(0.1,
                      completion: { self.setupIndicatorView(true) })
            
            let imagePath = fuguImagePath + "/" + imageFile
            thumbnailImageView.image = UIImage(contentsOfFile: imagePath)
        }
        
        if let dateTime = chatMessageObject["date_time"] as? String,
            dateTime.characters.count > 0 {
            let timeOfMessage = dateTime.changeDateFormat(toFormat: "h:mm a")
            timeLabel.text = "\(timeOfMessage)"
        }
        
        return self
    }
}

//MARK: - HELPERS
extension OutgoingImageCell {
    func setupIndicatorView(_ show: Bool) {
        customIndicator.image = UIImage(named: "app_loader_shape",
                                        in: bundle,
                                        compatibleWith: nil)
        if show {
            startIndicatorAnimation()
            customIndicator.isHidden = false
        } else {
            stopIndicatorAnimation()
            customIndicator.isHidden = true
        }
    }
    
    func startIndicatorAnimation() {
        startFuguRotation(ofView: customIndicator,
                          forKey: rotationAnimationKey)
    }
    
    func stopIndicatorAnimation() {
        stopFuguRotation(ofView: customIndicator,
                         forKey: rotationAnimationKey)
    }
}

private extension String {
    func randomString(OfLength length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    func changeDateFormat(sourceFormat: String? = "yyyy-MM-dd HH:mm:ss",
                          toFormat: String) -> String {
        var dateString = self.replacingOccurrences(of: ".000Z",
                                                   with: "")
        if dateString.contains(".") == true {
            dateString = (dateString.components(separatedBy: "."))[0]
        }
        dateString = dateString.replacingOccurrences(of: "T",
                                                     with: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = toFormat
        dateFormatter.timeZone = NSTimeZone.local
        if date != nil { return dateFormatter.string(from: date!) }
        return ""
    }
    
}
