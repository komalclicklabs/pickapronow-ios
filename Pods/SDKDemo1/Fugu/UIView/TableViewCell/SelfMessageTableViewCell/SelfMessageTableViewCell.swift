//
//  SelfMessageTableViewCell.swift
//  Fugu
//
//  Created by Gagandeep Arora on 29/09/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit

class SelfMessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var shadowView: So_UIView!
    @IBOutlet weak var selfMessageTextView: UITextView!
    @IBOutlet var bgView: UIView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var readUnreadImageView: UIImageView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        selfMessageTextView.backgroundColor = .clear
        selfMessageTextView.textContainer.lineFragmentPadding = 0
        selfMessageTextView.textContainerInset = .zero
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        adjustShadow()
    }
    
    func setupBoxBackground(messageType: Int) {
        bgView.backgroundColor = FuguConfig.shared.theme.outgoingChatBoxColor
        
        //        switch messageType {
        //        case MessageType.privateNote.rawValue:
        //            bgView.backgroundColor = UIColor.privateMessageBoxColor
        //        default:
        //            bgView.backgroundColor = UIColor.outGoingMessageBoxColor
        //        }
    }
    
    func adjustShadow() {
        //        self.shadowView.layoutIfNeeded()
        //        self.bgView.layoutIfNeeded()
        //        self.shadowView.layer.cornerRadius = self.bgView.layer.cornerRadius
        //        self.shadowView.backgroundColor = UIColor.makeColor(red: 234, green: 234, blue: 234, alpha: 0.3)
        //        self.shadowView.roundCorner(cornerRect: .allCorners, cornerRadius: self.shadowView.layer.cornerRadius)
    }
    
    func resetPropertiesOfOutgoingMessage() {
        selectionStyle = .none
        
        selfMessageTextView.text = ""
        selfMessageTextView.isEditable = false
        selfMessageTextView.dataDetectorTypes = UIDataDetectorTypes.all
        selfMessageTextView.textAlignment = .left
        selfMessageTextView.font =  FuguConfig.shared.theme.inOutChatTextFont
        selfMessageTextView.textColor = FuguConfig.shared.theme.outgoingMsgColor
        selfMessageTextView.backgroundColor = .clear
        
        timeLabel.text = ""
        timeLabel.font = FuguConfig.shared.theme.dateTimeFontSize
        timeLabel.textAlignment = .right
        timeLabel.textColor = FuguConfig.shared.theme.outgoingMsgDateTextColor
        
        //chatImageView.image = nil
       // chatImageView.layer.cornerRadius = 4.0
        
       // heightImageView.constant = 0
        
        bgView.layer.cornerRadius = 10
        bgView.backgroundColor = FuguConfig.shared.theme.outgoingChatBoxColor
        bgView.layer.borderWidth = FuguConfig.shared.theme.chatBoxBorderWidth
        bgView.layer.borderColor = FuguConfig.shared.theme.chatBoxBorderColor.cgColor
        
        readUnreadImageView.image = FuguConfig.shared.theme.unreadMessageTick
        if let tintColor = FuguConfig.shared.theme.unreadMessageTintColor {
            readUnreadImageView.tintColor = tintColor
        }
    }
    
    func configureIncomingMessageCell(resetProperties: Bool,
                                      userSubscribedValue: Int,
                                      chatMessageObject: [String: Any]) -> SelfMessageTableViewCell {
        if resetProperties { resetPropertiesOfOutgoingMessage() }
        let messageType = (chatMessageObject["message_type"] as? Int) ?? 1
        setupBoxBackground(messageType: messageType)

        readUnreadImageView.tintColor = nil
        if let messageReadStatus = chatMessageObject["message_status"] as? Int {
            if messageReadStatus == ReadUnReadStatus.none.rawValue {
                readUnreadImageView.image = FuguConfig.shared.theme.unsentMessageIcon
                if let tintColor = FuguConfig.shared.theme.unsentMessageTintColor {
                    readUnreadImageView.tintColor = tintColor
                }
            } else if messageReadStatus == ReadUnReadStatus.read.rawValue {
                readUnreadImageView.image = FuguConfig.shared.theme.readMessageTick
                if let tintColor = FuguConfig.shared.theme.readMessageTintColor {
                    readUnreadImageView.tintColor = tintColor
                }
            }
        }
        
        if let messageString = chatMessageObject["message"] as? String, messageString.characters.count > 0 {
            selfMessageTextView.text = messageString
        }
        
        if let dateTime = chatMessageObject["date_time"] as? String,
            dateTime.characters.count > 0 {
            let timeOfMessage = dateTime.changeDateFormat(toFormat: "h:mm a")
            timeLabel.text = "\(timeOfMessage)"
        }
        return self
    }
}

private extension String {
    func changeDateFormat(sourceFormat: String? = "yyyy-MM-dd HH:mm:ss",
                          toFormat: String) -> String {
        var dateString = self.replacingOccurrences(of: ".000Z",
                                                   with: "")
        if dateString.contains(".") == true {
            dateString = (dateString.components(separatedBy: "."))[0]
        }
        dateString = dateString.replacingOccurrences(of: "T",
                                                     with: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = toFormat
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        dateFormatter.timeZone = NSTimeZone.local
        if date != nil { return dateFormatter.string(from: date!) }
        return ""
    }
}
