//
//  FuguTheme.swift
//  Fugu
//
//  Created by Gagandeep Arora on 28/08/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit

@objc public class FuguTheme: NSObject {
    public class func defaultTheme() -> FuguTheme { return FuguTheme() }
    
    open var backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9764705882, blue: 1, alpha: 1)
    
    open var headerBackgroundColor = #colorLiteral(red: 0.3843137255, green: 0.4901960784, blue: 0.8823529412, alpha: 1)
    open var headerTextColor = #colorLiteral(red: 0.9882352941, green: 0.9882352941, blue: 0.9882352941, alpha: 1)
    open var headerText = "Support"
    open var headerTextFont: UIFont? = UIFont.boldSystemFont(ofSize: 18.0)
    
    open var leftBarButtonImage: UIImage? = UIImage(named: "whiteBackButton", in: bundle, compatibleWith: nil)
    open var leftBarButtonFont: UIFont? = UIFont.systemFont(ofSize: 13.0)
    open var leftBarButtonTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    open var leftBarButtonText = String()
    
    open var conversationTitleColor = #colorLiteral(red: 0.1725490196, green: 0.137254902, blue: 0.2, alpha: 1)
    open var conversationLastMsgColor = #colorLiteral(red: 0.1725490196, green: 0.137254902, blue: 0.2, alpha: 1)
    
    open var sendBtnIcon: UIImage? = UIImage(named: "sendMessageIcon", in: bundle, compatibleWith: nil)
    open var sendBtnIconTintColor: UIColor?
    
    open var addButtonIcon: UIImage? = UIImage(named: "addButtonIcon", in: bundle, compatibleWith: nil)
    open var addBtnTintColor: UIColor?
    
    open var readMessageTick: UIImage? = UIImage(named: "readMsgTick", in: bundle, compatibleWith: nil)
    open var readMessageTintColor: UIColor?
    
    open var unreadMessageTick: UIImage? = UIImage(named: "unreadMsgTick", in: bundle, compatibleWith: nil)
    open var unreadMessageTintColor: UIColor?
    
    open var unsentMessageIcon: UIImage? = UIImage(named: "unsent_watch_icon", in: bundle, compatibleWith: nil)
    open var unsentMessageTintColor: UIColor?
    
    open var dateTimeTextColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5568627451, alpha: 1)
    open var dateTimeFontSize: UIFont? = UIFont.systemFont(ofSize: 12.0)
    
    open var senderNameColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5568627451, alpha: 1)
    open var senderNameFont: UIFont? = UIFont.systemFont(ofSize: 12.0)
    
    open var incomingMsgColor = #colorLiteral(red: 0.1725490196, green: 0.137254902, blue: 0.2, alpha: 1)
    open var incomingMsgFont: UIFont? = UIFont.systemFont(ofSize: 15.0)
    open var incomingChatBoxColor = #colorLiteral(red: 0.9098039216, green: 0.9176470588, blue: 0.9882352941, alpha: 1)
    open var incomingMsgDateTextColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5568627451, alpha: 1)
    
    open var outgoingMsgColor = #colorLiteral(red: 0.1725490196, green: 0.137254902, blue: 0.2, alpha: 1)
    open var outgoingChatBoxColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    open var outgoingMsgDateTextColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5568627451, alpha: 1)
    
    open var chatBoxBorderWidth = CGFloat(0.5)
    open var chatBoxBorderColor = #colorLiteral(red: 0.862745098, green: 0.8784313725, blue: 0.9019607843, alpha: 1)
    
    open var inOutChatTextFont: UIFont? = UIFont.systemFont(ofSize: 15.0)
    
    open var timeTextColor = #colorLiteral(red: 0.1725490196, green: 0.137254902, blue: 0.2, alpha: 1)
    
    open var typingTextFont: UIFont? = UIFont.systemFont(ofSize: 15.0)
    open var typingTextColor = #colorLiteral(red: 0.1725490196, green: 0.137254902, blue: 0.2, alpha: 1)
}
