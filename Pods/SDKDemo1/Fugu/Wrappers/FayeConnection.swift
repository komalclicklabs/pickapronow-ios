//
//  FayeConnection.swift
//  Fugu
//
//  Created by Gagandeep Arora on 21/06/17.
//  Copyright © 2017 Socomo Technologies Private Limited. All rights reserved.
//

import UIKit
import MZFayeClient

let fayeConnection = FayeConnection()
//let faye
class FayeConnection: NSObject, MZFayeClientDelegate {
   
   fileprivate var subscribedChannels = [String]()
    fileprivate var isSendingCachedMessages = false
    var localFaye = MZFayeClient(url: URL(string: FuguConfig.shared.fayeBaseURLString))
   
   fileprivate override init() {
      super.init()
   }
    
    func setupFayeClient(_ successHandler: MZFayeClientSuccessHandler?,
                         failure failureHandler: MZFayeClientFailureHandler?) {
        localFaye?.delegate = self
        
        localFaye?.connect({
            print("successfuly connected")
            // self.subscribeFayeClient()
            if successHandler != nil { successHandler!() }
        }, failure: { (error) in
            if error != nil {
                print("failure: { (error)==> \(error!.localizedDescription)")
            }
            if failureHandler != nil {
                failureHandler!(error)
            }
            fuguDelay(1.5,
                      completion: {
                     self.setupFayeClient(successHandler, failure: failureHandler)
            })
        })
    }
    
    func setupConnectionAndSubscribeAllFayeEvents(event: @escaping (_ messageReceived: [AnyHashable: Any]?) -> Void) {
        if localFaye?.isConnected == true {
            subscribeAllFayeEvents(event: event)
        } else {
            setupFayeClient({
                self.subscribeAllFayeEvents(event: event)
            }, failure: { (error) in
                
            })
        }
    }
    
    func subscribeAllFayeEvents(event: @escaping (_ messageReceived: [AnyHashable: Any]?) -> Void) {
        if FuguConfig.shared.appSecretKey.isEmpty == false {
            localFaye?.subscribe(toChannel: "/\(FuguConfig.shared.appSecretKey)/**", success: { }, failure: { (error) in
            }, receivedMessage: { (messageReceived) in
                event(messageReceived)
            })
        }
    }
    
    func fayeClient(_ client: MZFayeClient!,
                    didConnectTo url: URL!) {
        print("didConnectTo==>\(url.absoluteString)")
    }
   
   func isChannelSubscribed(channelID: String) -> Bool {
      let updatedChannel = channelID.replacingOccurrences(of: "/", with: "")
      return subscribedChannels.contains(updatedChannel)
   }
    func fayeClient(_ client: MZFayeClient!,
                    didDisconnectWithError error: Error!) {
       // print("didDisconnectWithError==>\(error.localizedDescription)")
    }
    
    func fayeClient(_ client: MZFayeClient!,
                    didUnsubscribeFromChannel channel: String!) {
      
      guard channel != nil else {
         return
      }
      
      let updatedChannel = channel.replacingOccurrences(of: "/", with: "")
        print("didUnsubscribeFromChannel==>\(channel)")
      if isChannelSubscribed(channelID: updatedChannel)  {
         subscribedChannels = subscribedChannels.filter {return updatedChannel != $0}
      }
    }
    
    func fayeClient(_ client: MZFayeClient!,
                    didSubscribeToChannel channel: String!) {
        print("didSubscribeToChannel==>\(channel)")
      guard channel != nil else {
         return
      }
      
      let updatedChannel = channel.replacingOccurrences(of: "/", with: "")
      guard !isChannelSubscribed(channelID: updatedChannel) else {
         return
      }
      subscribedChannels.append(updatedChannel)
    }
    
    func fayeClient(_ client: MZFayeClient!,
                    didFailWithError error: Error!) {
        print("didFailWithError==>\(error.localizedDescription)")
    }
    
    func fayeClient(_ client: MZFayeClient!,
                    didFailDeserializeMessage message: [AnyHashable : Any]!,
                    withError error: Error!) {
        print("didFailDeserializeMessage==>\(message) \n and error==>\(error.localizedDescription)")
    }
    
    func fayeClient(_ client: MZFayeClient!,
                    didReceiveMessage messageData: [AnyHashable : Any]!,
                    fromChannel channel: String!) {
        print("didReceiveMessage==>\(messageData)")
    }
}

extension FayeConnection {
    func subscribeToSendCachedMessages(channelId: String,
                                       onSuccess: ((_ connected: Bool) -> Void)? = nil) {
        if connectedToNetwork() == true {
            if localFaye?.isConnected == true {
               guard isChannelSubscribed(channelID: channelId) == false
                  else {
                     if onSuccess != nil { onSuccess!(true) }
                     return
               }
                localFaye?.subscribe(toChannel: channelId, success: {
                    print("fayeClient?.subscrethgvri")
                    if onSuccess != nil { onSuccess!(true) }
                }, failure: { (error) in
                    if error != nil {
                        print("errorakdsnmrwt==>\(error!.localizedDescription)")
                    }
                    fuguDelay(1,
                              completion: {
                        self.subscribeToSendCachedMessages(channelId: channelId, onSuccess: onSuccess)
                    })
                }, receivedMessage: { (messageInfo) in })
            } else {
                setupFayeClient({
                    if onSuccess != nil { onSuccess!(false) }
                }, failure: { (error) in })
            }
        } else { isSendingCachedMessages = false }
    }
    
    func sendCachedMessages() {
        if let messagesInfo = FuguDefaults.object(forKey: DefaultName.unsentMessagesData.rawValue) as? [String: Any],
            messagesInfo.keys.count > 0 {
            for (key_channelId, value) in messagesInfo {
                if let chatBoxVC = fuguLastVisibleController() as? ConversationsViewController,
                    "\(chatBoxVC.channelId)" == key_channelId {
                    //code here
                } else {
                    if let cachedMessagesArray = value as? [[String: Any]] {
                        send(cachedMessagesArray: cachedMessagesArray, ofchannelId: key_channelId)
                        break
                    }
                } 
            }
        } else {
            isSendingCachedMessages = false
        }
    }
    
    func send(cachedMessagesArray: [[String: Any]],
              ofchannelId channelId: String) {
        if cachedMessagesArray.count > 0 {
            //if isSendingCachedMessages { return }
            
            isSendingCachedMessages = true
            
            var chatMessagesArray = cachedMessagesArray
            subscribeToSendCachedMessages(channelId: "/\(channelId)",
                onSuccess: { (connected) in
                    if connected {
                        self.sendCachedMessagesToFaye(toChannelId: channelId,
                                                      messageInfoArray: chatMessagesArray,
                                                      onSuccess: { (unsentMessages) in
                                                        if unsentMessages.count == 0 {
                                                            chatMessagesArray.removeAll()
                                                            self.send(cachedMessagesArray: unsentMessages,
                                                                      ofchannelId: channelId)
                                                        }
                        })
                    } else {
                        fuguDelay(0.1, completion: {
                            self.send(cachedMessagesArray: chatMessagesArray,
                                      ofchannelId: channelId)
                        })
                    }
            })
        } else {
            unsubscribe(fromChannelId: "/\(channelId)")
            if var cachedMessagesInfo = FuguDefaults.object(forKey: DefaultName.unsentMessagesData.rawValue) as? [String: Any] {
                cachedMessagesInfo.removeValue(forKey: channelId)
                FuguDefaults.set(value: cachedMessagesInfo, forKey: DefaultName.unsentMessagesData.rawValue)
            }
            sendCachedMessages()
        }
    }
    
    func sendCachedMessagesToFaye(toChannelId channelId: String,
                                  messageInfoArray: [[String: Any]],
                                  onSuccess:((_ unsentMessages: [[String: Any]]) -> Void)? = nil) {
        var cachedArray = messageInfoArray
        if cachedArray.count > 0 {
            var cachedMessageInfo = cachedArray[0]
            if let imageFile = cachedMessageInfo["image_file"] as? String,
                imageFile.isEmpty == false,
                let fuguImagePath = FuguDefaults.fuguImagesDirectory()?.path {
                let imagePath = fuguImagePath + "/" + imageFile
                getImageURL(fromImageInfo: imagePath,
                            fayeMessageInfo: cachedMessageInfo,
                            onCompletion: { (imageURLInfo, success) in
                                var when: Double = 0
                                if success {
                                    cachedMessageInfo.removeValue(forKey: "image_file")
                                    if imageURLInfo != nil {
                                        for (key, value) in imageURLInfo! { cachedMessageInfo[key] = value }
                                    }
                                    cachedArray[0] = cachedMessageInfo
                                } else {
                                    when = 5
                                    if var cachedMessagesInfo = FuguDefaults.object(forKey: DefaultName.unsentMessagesData.rawValue) as? [String: Any] {
                                        cachedMessagesInfo["\(channelId)"] = cachedArray
                                        FuguDefaults.set(value: cachedMessagesInfo, forKey: DefaultName.unsentMessagesData.rawValue)
                                    }
                                }
                                fuguDelay(when, completion: {
                                    self.sendCachedMessagesToFaye(toChannelId: channelId, messageInfoArray: cachedArray, onSuccess: onSuccess)
                                })
                })
            } else {
                fayeConnection.localFaye?.sendMessage(cachedArray[0], toChannel: "/\(channelId)", success: {
                    cachedArray.remove(at: 0)
                    self.sendCachedMessagesToFaye(toChannelId: channelId, messageInfoArray: cachedArray, onSuccess: onSuccess)
                }, failure: { (error) in
                    if error != nil { print("failure:\(error!.localizedDescription)") }
                    fuguDelay(0.1, completion: {
                        self.sendCachedMessagesToFaye(toChannelId: channelId, messageInfoArray: cachedArray, onSuccess: onSuccess)
                    })
                })
            }
            
        } else { if onSuccess != nil { onSuccess!(cachedArray) } }
    }
    
    func unsubscribe(fromChannelId channelId: String) {
        localFaye?.unsubscribe(fromChannel: channelId,
            success: {}, failure: { (error) in
            fuguDelay(0.1, completion: {
                self.unsubscribe(fromChannelId: channelId)
            })
        })
    }
    
    func getImageURL(fromImageInfo imagePath: String,
                     fayeMessageInfo: [String: Any],
                     onCompletion:@escaping (_ imageURLInfo: [String: Any]?, _ success: Bool) -> Void) {
        if connectedToNetwork() == false || FuguConfig.shared.appSecretKey.isEmpty == true {
            isSendingCachedMessages = false
            return
        }
        
        let params = ["app_secret_key": FuguConfig.shared.appSecretKey,
                      "file_type": "image"]
        
        let imageList = ["file": imagePath]
        
        HTTPClient.makeMultiPartRequestWith(method: .POST,
                                            showAlert: false,
                                            showAlertInDefaultCase: false,
                                            showActivityIndicator: false,
                                            para: params,
                                            baseUrl: FuguConfig.shared.baseUrl,
                                            extendedUrl: FuguEndPoints.API_UPLOAD_FILE.rawValue,
                                            imageList: imageList) { (responseObject, error, tag, statusCode) in
                                                switch (statusCode ?? -1) {
                                                case StatusCodeValue.Authorized_Min.rawValue..<StatusCodeValue.Authorized_Max.rawValue:
                                                    let response = responseObject as? [String: Any]
                                                    
                                                    if let data = response?["data"] as? [String: Any],
                                                        let imageUrl = data["image_url"] as? String,
                                                        let thumbnailUrl = data["thumbnail_url"] as? String {
                                                        let imageURLInfo = ["image_url": imageUrl,
                                                                            "thumbnail_url": thumbnailUrl]
                                                        if let cachedImage = UIImage(contentsOfFile: imagePath),
                                                            let url = NSURL(string: thumbnailUrl) {
                                                            imageCacheFugu.setObject(cachedImage, forKey: url)
                                                        }
                                                        onCompletion(imageURLInfo, true)
                                                    } else { onCompletion(nil, false) }
                                                default: onCompletion(nil, false)
                                                }
        }
    }
}

//func delay(_ inSeconds: Double, completion: @escaping () -> ()) {
//    DispatchQueue.main.asyncAfter(deadline: .now() + inSeconds) {
//        completion()
//    }
//}

