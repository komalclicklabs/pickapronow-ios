//
//  Helpers.swift
//  Fugu
//
//  Created by CL-macmini-88 on 5/11/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import Foundation
import UIKit
import ImageIO
import AVFoundation

//MARK: - Navigation
var isHandlingNotification: Bool = false
let FUGU_USER_ID = "fuguUserId"
let Fugu_AppSecret_Key = "fugu_app_secret_key"

var FUGU_SCREEN_WIDTH: CGFloat { return UIScreen.main.bounds.width }
var FUGU_SCREEN_HEIGHT: CGFloat { return UIScreen.main.bounds.height }

let reachability = FuguConnection()!

var bundle:Bundle? {
    let podBundle = Bundle(for: ShowAllConersationsViewController.self)
    guard
        let bundleURL = podBundle.url(forResource: "SDKDemo1",
                                      withExtension: "bundle"),
        let fetchBundle = Bundle(url: bundleURL)
    else { return nil }

    return fetchBundle
}

var isModuleRunning: Bool {
    guard let bundleIdentifier = Bundle.main.bundleIdentifier else { return true }
    return bundleIdentifier != "com.socomo.Fugu"
}

func presentAllChatsViewController(animation: Bool = true) {
    if validateFuguCredential() == false { return }
    
    let storyboard = UIStoryboard(name: "FuguUnique",
                                  bundle: bundle)
    if let navigationController = storyboard.instantiateViewController(withIdentifier: "FuguNavigationController") as? UINavigationController,
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController {
        var controllersInStack = navigationController.viewControllers
        if isHandlingNotification {
            isHandlingNotification = false
            
            guard let showAllConversationVC = controllersInStack.first as? ShowAllConersationsViewController else {
                return
            }
            if let conversationVC = storyboard.instantiateViewController(withIdentifier: "ConversationsViewController") as? ConversationsViewController {
                guard
                    let pushChannelId = FuguConfig.shared.pushInfo["channel_id"] as? Int,
                    let pushMessage = FuguConfig.shared.pushInfo["new_message"] as? String
                    else { return }
                
                if let tempChatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]] {
                    var chatCachedArray = tempChatCachedArray
                    for channel in 0 ..< chatCachedArray.count {
                        let channelObj = chatCachedArray[channel]
                        
                        guard let channelId = channelObj["channel_id"] as? Int else {
                            return
                        }
                        
                        if channelId == pushChannelId {
                            var updatedChannelObj = channelObj
                            updatedChannelObj["message"] = pushMessage
                            
//                            if let unreadCount = FuguConfig.shared.pushInfo["unread_count"] as? Int {
//                                updatedChannelObj["unread_count"] = unreadCount
//                            }
                            updatedChannelObj["unread_count"] = 0
                            if let dateTime = FuguConfig.shared.pushInfo["date_time"] as? String, dateTime.characters.count > 0 {
                                updatedChannelObj["date_time"] = dateTime
                            }
                            
                            chatCachedArray[channel] = updatedChannelObj
                            FuguDefaults.set(value: chatCachedArray,
                                             forKey: DefaultName.conversationData.rawValue)
                            _ = checkUnreadChatCount()
                            conversationVC.delegate = showAllConversationVC
                            break
                        }
                    }
                }
                
                if let userIdPush = FuguConfig.shared.pushInfo["user_id"] as? Int {
                    UserDefaults.standard.set(userIdPush,
                                              forKey: FUGU_USER_ID)
                }
                conversationVC.channelId = pushChannelId//"/\(pushChannelId)"
                FuguConfig.shared.pushInfo = [:]
                controllersInStack.append(conversationVC)
            }
            navigationController.viewControllers = controllersInStack
        } else {
            if let tempChatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]],
                tempChatCachedArray.count < 2,
                let conversationVC = setupChatBoxController(withChatArray: tempChatCachedArray) {
                controllersInStack.removeAll()
                controllersInStack.append(conversationVC)
                navigationController.viewControllers = controllersInStack
               getUserDetailsAndConversation()
            }
        }
        navigationController.modalPresentationStyle = .overCurrentContext
        if rootViewController.presentedViewController != nil {
            rootViewController.dismiss(animated: true, completion: {
                rootViewController.present(navigationController,
                                           animated: animation,
                                           completion: nil)
            })
        } else {
            rootViewController.present(navigationController,
                                       animated: animation,
                                       completion: nil)
        }
    }
}

func setupChatBoxController(withChatArray chatArray: [[String: Any]]) -> ConversationsViewController? {
    let storyboard = UIStoryboard(name: "FuguUnique",
                                  bundle: bundle)
    if let conversationVC = storyboard.instantiateViewController(withIdentifier: "ConversationsViewController") as? ConversationsViewController {
        if chatArray.count == 1 {
            let channelObj = chatArray[0]
            if let channelId = channelObj["channel_id"] as? Int {
                conversationVC.channelId = channelId
            }
        }
        return conversationVC
    }
    return nil
}
 func openChatViewController(labelId: Int,
                             message: String = "") {
    if validateFuguCredential() == false { return }
    
    let storyboard = UIStoryboard(name: "FuguUnique",
                                  bundle: bundle)
    if let pushToFilterViewController = storyboard.instantiateViewController(withIdentifier: "ConversationsViewController") as? ConversationsViewController,
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController {
        fuguDelay(1,
                  completion: {
            pushToFilterViewController.openChatBasedOnSpecificLabel(labelId,
                                                                    pageStart: 1,
                                                                    message: message)
        })
        if rootViewController.presentedViewController != nil {
            rootViewController.dismiss(animated: true,
                                       completion: {
                rootViewController.present(pushToFilterViewController,
                                           animated: true,
                                           completion: nil)
            })
        } else {
            rootViewController.present(pushToFilterViewController,
                                       animated: true,
                                       completion: nil)
        }
    }
}

func showFuguChat(withTransationId transactionId: String,
                  tags: [String]? = nil,
                  channelName: String,
                  message: String = "") {
    if validateFuguCredential() == false { return }
    
    let storyboard = UIStoryboard(name: "FuguUnique",
                                  bundle: bundle)
    if let chatBoxVC = storyboard.instantiateViewController(withIdentifier: "ConversationsViewController") as? ConversationsViewController,
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController {
        chatBoxVC.channelName = channelName
        chatBoxVC.directChatDetail = DirectChat(transactionId: transactionId,
                                                tags: tags,
                                                channelName: channelName, preMessage: message)
        
        if rootViewController.presentedViewController != nil {
            rootViewController.dismiss(animated: true,
                                       completion: {
                rootViewController.present(chatBoxVC,
                                           animated: true,
                                           completion: nil)
            })
        } else {
            rootViewController.present(chatBoxVC,
                                       animated: true,
                                       completion: nil)
        }
    }
}

func convertCurrentDateTimeToUTC() -> String {
    var utcDateTimeString = String(describing: Date())
    utcDateTimeString = utcDateTimeString.replacingOccurrences(of: " +0000",
                                                               with: "")
    
    let formatterUTC = DateFormatter()
    formatterUTC.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    formatterUTC.timeZone = TimeZone(secondsFromGMT: 0)
    
    return formatterUTC.string(from: Date())
}


func attributedStringForLabel(_ firstString: String,
                              secondString: String,
                              thirdString: String,
                              colorOfFirstString: UIColor,
                              colorOfSecondString: UIColor,
                              colorOfThirdString: UIColor,
                              fontOfFirstString: UIFont?,
                              fontOfSecondString: UIFont?,
                              fontOfThirdString: UIFont,
                              textAlighnment: NSTextAlignment,
                              dateAlignment: NSTextAlignment) -> NSMutableAttributedString {
    
    let combinedString = "\(firstString)\(secondString)\(thirdString)" as NSString
    
    let rangeOfFirstString = combinedString.range(of: firstString)
    let rangeOfSecondString = combinedString.range(of: secondString)
    let rangeOfThirdString = combinedString.range(of: thirdString)
    
    let firstStringStyle = NSMutableParagraphStyle()
    firstStringStyle.alignment = textAlighnment
    
    let thirdStringStyle = NSMutableParagraphStyle()
    thirdStringStyle.alignment = dateAlignment
    
    let attributedTitle = NSMutableAttributedString(string: combinedString as String)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName,
                                 value: colorOfFirstString,
                                 range: rangeOfFirstString)
    if let sendeNameFont = fontOfFirstString {
        attributedTitle.addAttribute(NSFontAttributeName,
                                     value: sendeNameFont,
                                     range: rangeOfFirstString)
    }
    
    attributedTitle.addAttribute(NSParagraphStyleAttributeName,
                                 value: firstStringStyle,
                                 range: rangeOfFirstString)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName,
                                 value: colorOfSecondString,
                                 range: rangeOfSecondString)
    
    if let incomingMsgFont = fontOfSecondString {
        attributedTitle.addAttribute(NSFontAttributeName,
                                     value: incomingMsgFont,
                                     range: rangeOfSecondString)
    }
    
    attributedTitle.addAttribute(NSParagraphStyleAttributeName,
                                 value: firstStringStyle,
                                 range: rangeOfSecondString)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName,
                                 value: colorOfThirdString,
                                 range: rangeOfThirdString)
    
    attributedTitle.addAttribute(NSFontAttributeName,
                                 value: fontOfThirdString,
                                 range: rangeOfThirdString)
    attributedTitle.addAttribute(NSParagraphStyleAttributeName,
                                 value: thirdStringStyle,
                                 range: rangeOfThirdString)    
    return attributedTitle
}

func attributedStringForLabelForTwoStrings(_ firstString: String,
                                           secondString: String,
                                           colorOfFirstString: UIColor,
                                           colorOfSecondString: UIColor,
                                           fontOfFirstString: UIFont,
                                           fontOfSecondString: UIFont,
                                           textAlighnment: NSTextAlignment,
                                           dateAlignment: NSTextAlignment) -> NSMutableAttributedString {
    let combinedString = "\(firstString)\(secondString)" as NSString
    
    let rangeOfFirstString = combinedString.range(of: firstString)
    let rangeOfSecondString = combinedString.range(of: secondString)
    
    let firstStringStyle = NSMutableParagraphStyle()
    firstStringStyle.alignment = textAlighnment
    
    let thirdStringStyle = NSMutableParagraphStyle()
    thirdStringStyle.alignment = dateAlignment
    
    let attributedTitle = NSMutableAttributedString(string: combinedString as String)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName,
                                 value: colorOfFirstString,
                                 range: rangeOfFirstString)
    attributedTitle.addAttribute(NSFontAttributeName,
                                 value: fontOfFirstString,
                                 range: rangeOfFirstString)
    attributedTitle.addAttribute(NSParagraphStyleAttributeName,
                                 value: firstStringStyle,
                                 range: rangeOfFirstString)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName,
                                 value: colorOfSecondString,
                                 range: rangeOfSecondString)
    attributedTitle.addAttribute(NSFontAttributeName,
                                 value: fontOfSecondString,
                                 range: rangeOfSecondString)
    attributedTitle.addAttribute(NSParagraphStyleAttributeName,
                                 value: firstStringStyle,
                                 range: rangeOfSecondString)
    
    return attributedTitle
}

func changeDateToParticularFormat(_ dateTobeConverted: Date,
                                  dateFormat: String,
                                  showInFormat: Bool) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = dateFormat
    formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.timeZone = TimeZone.current
    formatter.amSymbol = "am"
    formatter.pmSymbol = "pm"
    
    if !showInFormat {
        let comparisonResult = Calendar.current.compare(dateTobeConverted,
                                                        to: Date(),
                                                        toGranularity: .day)
        switch comparisonResult {
        case .orderedSame: return "Today"
        default:
            let calendar = NSCalendar.current
            let dateOfMsg = calendar.startOfDay(for: dateTobeConverted)
            let currentDate = calendar.startOfDay(for: Date())
            let dateDifference = calendar.dateComponents([.day],
                                                         from: dateOfMsg,
                                                         to: currentDate).day
            if dateDifference == 1 { return "Yesterday" }
            return formatter.string(from: dateTobeConverted)
        }
    }
    return formatter.string(from: dateTobeConverted)
}

func connectedToNetwork() -> Bool { return reachability.connection != .none }

func fuguDelay(_ withDuration: Double,
               completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + withDuration) { completion() }
}

func startFuguRotation(ofView rotateView: UIView,
                       forKey rotationAnimationKey: String) {
    if rotateView.layer.animation(forKey: rotationAnimationKey) == nil {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = Float(.pi * 2.0)
        rotationAnimation.duration = 1
        rotationAnimation.repeatCount = Float.infinity
        
        rotateView.layer.add(rotationAnimation,
                             forKey: rotationAnimationKey)
    }
}

func stopFuguRotation(ofView rotateView: UIView,
                      forKey rotationAnimationKey: String) {
    if rotateView.layer.animation(forKey: rotationAnimationKey) != nil {
        rotateView.layer.removeAnimation(forKey: rotationAnimationKey)
    }
}

func getUserDetailsAndConversation(callback: HTTPClient.ServiceResponse? = nil) {
    var endPointName = FuguEndPoints.API_PUT_USER_DETAILS.rawValue
    let params = FuguConfig.shared.userDetail.toJson()
    
    if params["user_unique_key"] == nil { return }
    
    switch FuguConfig.shared.credentialType {
    case FuguCredentialType.reseller:
        endPointName = FuguEndPoints.API_Reseller_Put_User.rawValue
        if params["reseller_token"] == nil || params["reference_id"] == nil { return }
    default: if params["app_secret_key"] == nil { return }
    }
    /*["device_details": "{  \"ios_os_version\" : \"10.0.2\",  \"ios_model\" : \"iPhone\",  \"ios_operating_system\" : \"IOS\",  \"ios_manufacturer\" : \"APPLE\"}", "device_id": "383B3CC2-0963-413C-B8D8-7E584E0D130B", "user_unique_key": "f593c696ca8103", "device_type": 2, "app_secret_key": "dff8ab31b54372e9d183e16774604e34"]
     
     */
    HTTPClient.makeConcurrentConnectionWith(method: .POST,
                                                  showAlert: false,
                                                  showAlertInDefaultCase: false,
                                                  showActivityIndicator: false,
                                                  para: params,
                                                  extendedUrl: endPointName) { (responseObject, error, tag, statusCode) in
                                                    //print("error putuser==>\(error?.localizedDescription)")
                                                    let response = (responseObject as? [String: Any]) ?? [:]
                                                    if statusCode == STATUS_CODE_SUCCESS {
                                                        if let data = response["data"] as? [String: Any] {
                                                            userDetailData = data
                                                            
                                                            if let appSecretKey = userDetailData["app_secret_key"] as? String {
                                                                FuguConfig.shared.appSecretKey = appSecretKey
                                                            }
                                                            
                                                            if let userId = userDetailData["user_id"] as? Int {
                                                                UserDefaults.standard.set(userId, forKey: FUGU_USER_ID)
                                                            }
                                                            
                                                            if let botChannelsArray = userDetailData["conversations"] as? [[String: Any]] {
                                                                FuguDefaults.set(value: botChannelsArray, forKey: DefaultName.conversationData.rawValue)
                                                            }
                                                            _ = checkUnreadChatCount()
                                                        }
                                                    }
                                                    if callback != nil { callback!(responseObject, error, tag, statusCode) }
    }
}

func checkUnreadChatCount() -> Int {
    var chatCounter = 0
    if let chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]] {
        for conversationInfo in chatCachedArray {
            if let conversationCounter = conversationInfo["unread_count"] as? Int {
                chatCounter += conversationCounter
            }
        }
    }
    if FuguConfig.shared.unreadCount != nil { FuguConfig.shared.unreadCount!(chatCounter) }
    return chatCounter
}

 func toShowInAppNotification(userInfo: [String: Any]) -> Bool {
    if validateFuguCredential() == false { return false }
    
    if UIApplication.shared.applicationState == .active {
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController,
            let presentedViewController = rootViewController.presentedViewController {
            var visibleController = presentedViewController
            if let presentedNavigationController = presentedViewController as? UINavigationController,
                let lastVisibleCtrl = presentedNavigationController.viewControllers.last {
                visibleController = lastVisibleCtrl
            }
            
            if let lastVisibleCtrl = visibleController as? ShowAllConersationsViewController {
                lastVisibleCtrl.updateChannelsWithrespectToPush(pushInfo: userInfo,
                                                                moveToChatController: false)
                return true
            } else if let conversationVC = visibleController as? ConversationsViewController {
                if let existingViewControllers = conversationVC.navigationController?.viewControllers {
                    for existingController in existingViewControllers {
                        if let lastVisibleCtrl = existingController as? ShowAllConersationsViewController {
                            lastVisibleCtrl.updateChannelsWithrespectToPush(pushInfo: userInfo,
                                                                            moveToChatController: false)
                            break
                        }
                    }
                }
                
                if let channelId = userInfo["channel_id"] as? Int,
                    conversationVC.channelId == channelId {
                    return false
                }
            }
        }
        return true
    } else {
        var visibleController: UIViewController?
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController, let presentedViewController = rootViewController.presentedViewController {
            visibleController = presentedViewController
            if let presentedNavigationController = presentedViewController as? UINavigationController, let lastVisibleCtrl = presentedNavigationController.viewControllers.last {
                visibleController = lastVisibleCtrl
            }
        }
        
        if let lastVisibleCtrl = visibleController as? ShowAllConersationsViewController {
            lastVisibleCtrl.updateChannelsWithrespectToPush(pushInfo: userInfo,
                                                            moveToChatController: true)
        } else if let lastVisibleCtrl = visibleController as? ConversationsViewController {
            guard let channelId = userInfo["channel_id"] as? Int,
                lastVisibleCtrl.channelId != channelId else {
                return false
            }
            
            DispatchQueue.main.async {
                lastVisibleCtrl.isChatDeactivated = false
                lastVisibleCtrl.getMessagesBasedOnChannel(channelId, pageStart: 1)
            }
        } else {
            isHandlingNotification = true
            FuguConfig.shared.pushInfo = userInfo
            presentAllChatsViewController()
        }
    }
    return false
}

func fuguLastVisibleController() -> UIViewController? {
    if  isModuleRunning == true,
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController,
        let presentedViewController = rootViewController.presentedViewController {
        var visibleController = presentedViewController
        if let presentedNavigationController = presentedViewController as? UINavigationController,
            let lastVisibleCtrl = presentedNavigationController.viewControllers.last {
            visibleController = lastVisibleCtrl
        }
        return visibleController
    } else {
        if isModuleRunning == false,
            let presentedNavigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            return presentedNavigationController.viewControllers.last
        }
    }
    return nil
}

func logoutFromFugu() {
    if FuguConfig.shared.appSecretKey.isEmpty { return }
    
    var params: [String: Any] = ["app_secret_key": FuguConfig.shared.appSecretKey]
    
    if let savedUserId = UserDefaults.standard.value(forKey: FUGU_USER_ID) as? Int {
        params["user_id"] = savedUserId
    }
    
    HTTPClient.makeConcurrentConnectionWith(method: .POST,
                                                  showAlert: false,
                                                  showAlertInDefaultCase: false,
                                                  showActivityIndicator: false,
                                                  para: params,
                                                  extendedUrl: FuguEndPoints.API_CLEAR_USER_DATA_LOGOUT.rawValue) { (responseObject, error, tag, statusCode) in
                                                    FuguDefaults.removeAllPersistingData()
                                                    
                                                    //FuguConfig.shared.deviceToken = ""
                                                    FuguConfig.shared.appSecretKey = ""
                                                    FuguConfig.shared.resellerToken = ""
                                                    FuguConfig.shared.referenceId = -1
                                                    FuguConfig.shared.appType = nil
                                                    FuguConfig.shared.userDetail = FuguUserDetail()
                                                    userDetailData = [String: Any]()
                                                    
                                                    FuguDefaults.removeObject(forKey: DefaultName.conversationData.rawValue)
                                                    FuguDefaults.removeAllPersistingData()
                                                    
                                                    let defaults = UserDefaults.standard
                                                    defaults.removeObject(forKey: FUGU_USER_ID)
                                                    defaults.synchronize()
    }
}

func updateDeviceToken(deviceToken: Data) {
    let tokenData = NSData(data: deviceToken)
    let trimEnds = tokenData.description.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
    let pushToken = trimEnds.replacingOccurrences(of: " ",
                                                  with: "")
    
    if pushToken.isEmpty == false {
        FuguConfig.shared.deviceToken = pushToken
        getUserDetailsAndConversation()
    }
}

func fuguConnectionChangesStartNotifier() {
    //declare this property where it won't go out of scope relative to your listener
    reachability.whenReachable = { fuguConnection in
//        if fuguConnection.connection == .wifi { print("Reachable via WiFi") }
//        else { print("Reachable via Cellular") }
        errorMessage(needToBeHidden: true)
      if fayeConnection.localFaye?.isConnected == true {
         
      } else {
         fayeConnection.setupFayeClient({
            
         }, failure: { (error) in
            
         })
      }
        sendAllCachedMessages()
    }
    
    reachability.whenUnreachable = { _ in
        //print("Not reachable")
        errorMessage(needToBeHidden: false)
    }
    
    do { try reachability.startNotifier() }
    catch { print("Unable to start notifier") }
}

func errorMessage(needToBeHidden hidden: Bool) {
    let erorMessage = "No Internet Connection"
    if let chatBoxVC = fuguLastVisibleController() as? ConversationsViewController {
        if hidden == false { chatBoxVC.errorLabel.text = erorMessage }
        chatBoxVC.updateErrorLabelView(isHiding: hidden)
    } else if let conversationVC = fuguLastVisibleController() as? ShowAllConersationsViewController {
        if hidden == false {
            
            guard
                let chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]],
                chatCachedArray.isEmpty == false
                else {
                    fuguDelay(1,
                              completion: {
                                conversationVC.tableViewDefaultText = "No Internet Connection\n Tap to retry"
                                conversationVC.showConversationsTableView.reloadData()
                    })
                    return
            }
            
            conversationVC.refreshControl.endRefreshing()
            conversationVC.isGetAllConversations = false
            
            conversationVC.errorLabel.text = erorMessage
            conversationVC.updateErrorLabelView(isHiding: false)
        } else {
            conversationVC.putUserDetailsAndGetConversations()
            conversationVC.updateErrorLabelView(isHiding: hidden)
        }
    }
}

func sendAllCachedMessages() {
    fayeConnection.sendCachedMessages()
    if let chatBoxVC = fuguLastVisibleController() as? ConversationsViewController {
        chatBoxVC.startSendingCachedMessagesWhenInternetAvailable()
    }
}

func validateFuguRemoteNotification(withUserInfo userInfo: [String: Any]) -> Bool {
    if let pushSource = userInfo["push_source"] as? String,
        pushSource == "FUGU" {
        return true
    }
    return false
}

func validateFuguCredential() -> Bool {
    switch FuguConfig.shared.credentialType {
    case FuguCredentialType.reseller:
        if FuguConfig.shared.resellerToken.isEmpty ||
            FuguConfig.shared.referenceId < 0 { return false }
    default: if FuguConfig.shared.appSecretKey.isEmpty { return false }
    }
    return true
}

//MARK: Extentions
extension String {
    
    var toDate: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        guard let date = formatter.date(from: self) else {
            return nil
        }
        
        return date
    }
    
    func checkNA() -> String {
        let str = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if str.characters.count == 0 {
            return "--"
        } else {
            return self
        }
    }
}

extension Date {
    var toString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy, hh:mm a"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.current
        
        let comparisonResult = Calendar.current.compare(self,
                                                        to: Date(),
                                                        toGranularity: .day)

        switch comparisonResult {
            
        case .orderedSame:
            formatter.amSymbol = "am"
            formatter.pmSymbol = "pm"
            formatter.dateFormat = "h:mm a"
            return formatter.string(from: self)
        default:
            let calendar = NSCalendar.current
            let dateOfMsg = calendar.startOfDay(for: self)
            let currentDate = calendar.startOfDay(for: Date())
            
            let dateDifference = calendar.dateComponents([.day],
                                                         from: dateOfMsg,
                                                         to: currentDate).day ?? -1
            
            switch dateDifference {
            case 1:
                //formatter.dateFormat = "hh:mm a"
                return "Yesterday"
            case let difference where (difference > 1 && difference < 8):
                return "\(dateDifference) days ago"
            default:
                formatter.dateFormat = "d/MM/yyyy"
                return formatter.string(from: self)
            }
        }
    }
}

extension UIImageView {
    func downloadedFrom(url: URL,
                        contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse,
                httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType,
                mimeType.hasPrefix("image"),
                let data = data,
                error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String,
                        contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
