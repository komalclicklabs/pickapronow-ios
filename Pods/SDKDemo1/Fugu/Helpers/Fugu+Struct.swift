//
//  FileStructClasses.swift
//  Fugu
//
//  Created by Gagandeep Arora on 13/10/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import Foundation

struct DirectChat {
    var transactionId: String?
    var userUniqueKey: String?
    var otherUniqueKey: [String]?
    var tags: [String]?
    var channelName: String?
   var preMessage = ""
   
    init(transactionId: String,
         userUniqueKey: String? = nil,
         otherUniqueKey: [String]? = nil,
         tags: [String]? = nil,
         channelName: String? = nil, preMessage: String = "") {
        self.transactionId = transactionId
        self.userUniqueKey = userUniqueKey
        self.otherUniqueKey = otherUniqueKey
        self.tags = tags
        self.channelName = channelName
      self.preMessage = preMessage
    }
    
    func toJSONObj() -> [String: Any] {
        var params = [String: Any]()
        
        if let tempTransactionId = transactionId {
            params["transaction_id"] = tempTransactionId
        }
        
        if let tempUserUniqueKey = userUniqueKey {
            params["user_unique_key"] = tempUserUniqueKey
        }
        
        if let tempOtherUniquekey = otherUniqueKey {
            params["other_user_unique_key"] = tempOtherUniquekey
        }
        
        if let tempTags = tags {
            params["tags"] = tempTags
        }
        
        if let tempChannelName = channelName {
            params["custom_label"] = tempChannelName
        }
      
      if isPreMessageValid() {
         let messageToSend = [preMessage]
         params["user_first_messages"] = messageToSend
      }
        
        return params
    }
   
   private func isPreMessageValid() -> Bool {
      let messageAfterRemovingWhiteSpaces =  preMessage.replacingOccurrences(of: " ", with: "")
      let messageAfterTrimmingWhiteSpaces = messageAfterRemovingWhiteSpaces.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
      return messageAfterTrimmingWhiteSpaces.characters.count != 0
   }
}
